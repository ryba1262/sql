
-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------USR_HistoriaLogowan---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_USR_HistoriaLogowan]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_USR_HistoriaLogowan] (
   [Log_USR_HistoriaLogowanID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_HistoriaLogowan] [int] 
  ,[Org_Login] [varchar] (50)
  ,[Org_AdresZdalny] [varchar] (50)
  ,[Org_Powodzenie] [bit] 
  ,[Org_DataAkcji] [datetime] 
  ,[Org_UserAgent] [varchar] (256)
  ,[Org_SessionToken] [varchar] (32)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DadaModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_USR_HistoriaLogowan] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_USR_HistoriaLogowan] PRIMARY KEY  CLUSTERED
(
   [Log_USR_HistoriaLogowanID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_USR_HistoriaLogowan] (
   [Log_USR_HistoriaLogowanID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_HistoriaLogowan] [int] 
  ,[Org_Login] [varchar] (50)
  ,[Org_AdresZdalny] [varchar] (50)
  ,[Org_Powodzenie] [bit] 
  ,[Org_DataAkcji] [datetime] 
  ,[Org_UserAgent] [varchar] (256)
  ,[Org_SessionToken] [varchar] (32)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DadaModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_HistoriaLogowan ON

IF EXISTS (SELECT * FROM dbo.LOG_USR_HistoriaLogowan)
EXEC('INSERT INTO dbo.Tmp_LOG_USR_HistoriaLogowan (Log_USR_HistoriaLogowanID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_HistoriaLogowan, Org_Login, Org_AdresZdalny, Org_Powodzenie, Org_DataAkcji, Org_UserAgent, Org_SessionToken, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DadaModyfikacji)
  SELECT Log_USR_HistoriaLogowanID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_HistoriaLogowan, Org_Login, Org_AdresZdalny, Org_Powodzenie, Org_DataAkcji, Org_UserAgent, Org_SessionToken, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DadaModyfikacji FROM dbo.Log_USR_HistoriaLogowan WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_HistoriaLogowan OFF

DROP TABLE dbo.LOG_USR_HistoriaLogowan

EXECUTE sp_rename N'dbo.Tmp_LOG_USR_HistoriaLogowan', N'LOG_USR_HistoriaLogowan', 'OBJECT'

ALTER TABLE [dbo].[LOG_USR_HistoriaLogowan] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_USR_HistoriaLogowan] PRIMARY KEY  CLUSTERED
(
   [Log_USR_HistoriaLogowanID]
) ON [PRIMARY]

END
GO

