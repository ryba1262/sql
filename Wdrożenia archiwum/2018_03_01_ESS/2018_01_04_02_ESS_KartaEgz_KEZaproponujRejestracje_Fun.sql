
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

/*
Funkcja zwraca propozycj� rejestracji dla danej karty egzaminacyjnej
2 - studenci z prawem do rejestracji pe�nej
3 - studenci z prawem do rejestracji warunkowej
4 - studenci bez prawa do rejestracji
*/
DROP Function [dbo].[KEZaproponujRejestracjeDlaKartyEgz]
go

CREATE function [dbo].[KEZaproponujRejestracjeDlaKartyEgz] (@KartaEgzID int, @RejestracjaWarunkowaKryteriumID int)  
returns @result table(IndeksID int, IDKartaEgz int,KartaPoczatkowa int, RodzajRejestracji int, UzyskanaLP int, BrakujacaLP int,UzyskanaLPrzedm int, BrakujacaLPrzedm int,KryteriumRejestracjiWarunkowej varchar(100))
begin	


    declare @CzyNoweKartyEgz bit, @KERokAkadOd int, @KESemestrIDOd int
	select @KERokAkadOd = Wartosc from UstawieniaProgramu where Nazwa = 'KERokAkadOd'
	select @KESemestrIDOd = Wartosc from UstawieniaProgramu where Nazwa = 'KESemestrOdID'
	
	select @CzyNoweKartyEgz = (case when COUNT(*)>0 then 1 else 0 end) from KartaEgz where IDKartaEgz = @KartaEgzID and RokAkad*10+SemestrID>=@KERokAkadOd*10+@KESemestrIDOd
	
	
	;with DaneWejsciowe as
	(
		select SemestrRealizacjiID, ke.SemestrStudiow, LiczbaPunktow, CzyPoprawkowy, CzyZalicza, KartaPoczatkowa, IndeksID, IDKartaEgz, TylkoKartaGlowna,
		(select COUNT(*) from KartaEgzForma 
		left join SkalaOcenPoz on OcenaID = IDSkalaOcenPoz
		JOIN FormaWymiar ON KartaEgzForma.FormaWymiarID = FormaWymiar.IDFormaWymiar
        LEFT JOIN FormaZajec ON FormaWymiar.FormaZajecID = FormaZajec.IDFormaZajec
		where KartaEgzPozID =kep.IDKartaEgzPoz	and CzyDrukowacNaKarcieEgz = 1 and (CzyZalicza = 0 or CzyZalicza  is null)) NiezaliczonePrzedmioty
		from KartaEgz ke		  
		join KartaEgzPoz kep on ke.IDKartaEgz = kep.KartaEgzID
								and ke.IDKartaEgz = @KartaEgzID
		left join SkalaOcenPoz on OcenaKoncowaID = IDSkalaOcenPoz
		join SemestrRealizacji on SemestrRealizacjiID = IDSemestrRealizacji			
		left join RejestracjaWarunkowaKryterium on IDRejestracjaWarunkowaKryterium = @RejestracjaWarunkowaKryteriumID 
												and CzyZatwierdzono = 1
		left join RejestracjaWarunkowaKryteriumPoz rwkp on IDRejestracjaWarunkowaKryterium = RejestracjaWarunkowaKryteriumID and rwkp.SemestrStudiow = ke.SemestrStudiow					  	
	),
	DaneWejsciowe2 (SemestrRealizacjiID, SemestrStudiow, LiczbaPunktow, CzyPoprawkowy, CzyZalicza, KartaPoczatkowa, IndeksID, IDKartaEgz, TylkoKartaGlowna)
	as
	(
	select SemestrRealizacjiID, SemestrStudiow, LiczbaPunktow, CzyPoprawkowy, 
	(case when @CzyNoweKartyEgz = 1 then case when NiezaliczonePrzedmioty>0 then 0 else 1 end  else CzyZalicza end), 
	KartaPoczatkowa, IndeksID, IDKartaEgz, TylkoKartaGlowna 
	from DaneWejsciowe
	)	
	, 
	PunktyDlaKartyStudenta(IndeksID, IDKartaEgz, SemestrStudiow, KartaPoczatkowa, NominalnaLP, UzyskanaLP, BrakujacaLP, BrakujacePrzedmiotyZaZero, NominalnaLPrzedm, UzyskanaLPrzedm, BrakujacaLPrzedm) as
	(
		select  IndeksID, 
				IDKartaEgz,
				SemestrStudiow,
				KartaPoczatkowa,
				sum(
				  case 
					when
						(coalesce(CzyPoprawkowy, 0) = 0 or (CzyPoprawkowy = 1 and coalesce(TylkoKartaGlowna, 0) <> 1))
					then
						LiczbaPunktow
					else
						0
				  end) as NominalnaLP,
				sum(
				  case 
					when
						 (coalesce(CzyPoprawkowy, 0) = 0 or (CzyPoprawkowy = 1 and coalesce(TylkoKartaGlowna, 0) <> 1))
						 and coalesce(CzyZalicza, 0) = 1
					then
						LiczbaPunktow
					else
						0					 
				  end) as UzyskanaLP,
				sum(
				  case 
					when
						 (coalesce(CzyPoprawkowy, 0) = 0 or (CzyPoprawkowy = 1 and coalesce(TylkoKartaGlowna, 0) <> 1))
						 and coalesce(CzyZalicza, 0) = 0 and LiczbaPunktow > 0
					then
						LiczbaPunktow
					else
						0					 
				  end) as BrakujacaLP,
				sum(
				  case 
					when
						 (coalesce(CzyPoprawkowy, 0) = 0 or (CzyPoprawkowy = 1 and coalesce(TylkoKartaGlowna, 0) <> 1))
						 and coalesce(CzyZalicza, 0) = 0 and LiczbaPunktow = 0
					then
						1
					else
						0					 
				  end) as BrakujacePrzedmiotyZaZero,
				count(
				  case 
					when
						(coalesce(CzyPoprawkowy, 0) = 0 or (CzyPoprawkowy = 1 and coalesce(TylkoKartaGlowna, 0) <> 1))
					then
						SemestrRealizacjiID					
				  end) as NominalnaLPrzedm,
				count(
				  case 
					when
						 (coalesce(CzyPoprawkowy, 0) = 0 or (CzyPoprawkowy = 1 and coalesce(TylkoKartaGlowna, 0) <> 1))
						 and coalesce(CzyZalicza, 0) = 1
					then
						SemestrRealizacjiID										 
				  end) as UzyskanaLPrzedm,
				count(
				  case 
					when
						 (coalesce(CzyPoprawkowy, 0) = 0 or (CzyPoprawkowy = 1 and coalesce(TylkoKartaGlowna, 0) <> 1))
						 and coalesce(CzyZalicza, 0) = 0
					then
						SemestrRealizacjiID										 
				  end) as BrakujacaLPrzedm
		from DaneWejsciowe2
		group by IndeksID, IDKartaEgz, SemestrStudiow, KartaPoczatkowa
	)
    insert into @result
	select  IndeksID,
			IDKartaEgz,
			KartaPoczatkowa,
			case 
				when 
					PunktyMinimalne is null and PunktyBrakujaceMaksymalnie is null and PrzedmiotyBrakujaceMaksymalnie is null
				then
					null
				when 
					(PunktyMinimalne is not null or PunktyBrakujaceMaksymalnie is not null) and PrzedmiotyBrakujaceMaksymalnie is null
				then	 
					case 
						when
							UzyskanaLP = NominalnaLP and BrakujacaLP = 0 and BrakujacePrzedmiotyZaZero = 0 --sprawdzenie czy jest to rejestracja pe�na
						then
							2
						else
						  case 
							when 
								 (PunktyMinimalne is not null and UzyskanaLP >= PunktyMinimalne)
								 or (PunktyBrakujaceMaksymalnie is not null and BrakujacaLP <= PunktyBrakujaceMaksymalnie) --sprawdzenie warunkow i wpisanie wiersza z odpowiednia wartoscia do kolumny RodzajRejestracji
							then
								3
							else
								4
						  end
					end 
				when
					PunktyMinimalne is null and PunktyBrakujaceMaksymalnie is null and PrzedmiotyBrakujaceMaksymalnie is not null
				then
					case 
						when
							UzyskanaLPrzedm = NominalnaLPrzedm and BrakujacaLPrzedm = 0 --sprawdzenie czy jest to rejestracja pe�na
						then
							2
						else
						  case 
							when 								 
								BrakujacaLPrzedm <= PrzedmiotyBrakujaceMaksymalnie --sprawdzenie warunkow i wpisanie wiersza z odpowiednia wartoscia do kolumny RodzajRejestracji
							then
								3
							else
								4
						  end
					end				
			end as RodzajRejestracji,
			UzyskanaLP,
			BrakujacaLP,
			UzyskanaLPrzedm,
			BrakujacaLPrzedm,
			case 
				when 
					PunktyMinimalne is null and PunktyBrakujaceMaksymalnie is null and PrzedmiotyBrakujaceMaksymalnie is null
				then
					'Brak kryterium'
				when 
					(PunktyMinimalne is not null or PunktyBrakujaceMaksymalnie is not null) and PrzedmiotyBrakujaceMaksymalnie is null
				then
					'Punkty'
				when
					PunktyMinimalne is null and PunktyBrakujaceMaksymalnie is null and PrzedmiotyBrakujaceMaksymalnie is not null
				then
					'Przedmioty'
			end as KryteriumRejestracjiWarunkowej
	from PunktyDlaKartyStudenta as pdks
	left join RejestracjaWarunkowaKryterium on IDRejestracjaWarunkowaKryterium = @RejestracjaWarunkowaKryteriumID 
											and CzyZatwierdzono = 1
	left join RejestracjaWarunkowaKryteriumPoz rwkp on IDRejestracjaWarunkowaKryterium = RejestracjaWarunkowaKryteriumID and rwkp.SemestrStudiow = pdks.SemestrStudiow
	
	
	return
end
go