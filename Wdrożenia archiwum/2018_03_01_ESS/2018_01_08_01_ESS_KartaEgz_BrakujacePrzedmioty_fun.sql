IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KEBrakujacaLiczbaPrzedmiotow]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[KEBrakujacaLiczbaPrzedmiotow]
GO

SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[KEBrakujacaLiczbaPrzedmiotow] (@IDKartaEgz int)  
RETURNS @Output table (Flaga int, BrakujacePrzedmioty int, BrakujacePrzedmiotyCzesne int, BrakujacePrzedmiotyHistoria int, BrakujacePrzedmiotyCzesneHistoria int)  
BEGIN

	declare @BrakujacePrzedmioty int,
			@BrakujacePrzedmiotyCzesne int, 
			@ZaliczonePrzedmioty int, 
			@NominalnaLiczbaPrzedmiotow int, @KERokAkadOd int, @KESemestrIDOd int
	 
	set @BrakujacePrzedmioty = 0
	set @BrakujacePrzedmiotyCzesne = 0
	set @ZaliczonePrzedmioty = 0
	set @NominalnaLiczbaPrzedmiotow = 0

	declare @RokAkad int,			
			@SemestrID int,
			@IndeksID int,
			@Zatwierdzona int

	declare @NastepnaRejestracjaID int,
			@BrakujacePrzedmiotyHistoria int,
			@BrakujacePrzedmiotyCzesneHistoria int,
			@Flaga int

	select @Zatwierdzona = Zatwierdzona, @RokAkad = RokAkad, @SemestrID = SemestrID, @IndeksID = IndeksID
	from KartaEgz where IDKartaEgz = @IDKartaEgz

	declare @ZRokAkad int, @ZSemestrID int
	set @ZRokAkad = 0
	set @ZSemestrID = 0
	select top 1 @ZRokAkad = RokAkademicki, @ZSemestrID = SemestrAkadID
	from HistoriaStudenta where ZdarzenieID = 32 and IndeksID = @IndeksID and (@RokAkad * 100 + @SemestrID) >= (RokAkademicki * 100 + SemestrAkadID) and Anulowany = 0
	order by RokAkademicki desc, SemestrAkadID desc

	if (@Zatwierdzona = 1)
	  begin

		select  @NastepnaRejestracjaID = nast_rej.IDHistoriaStudenta,
				@BrakujacePrzedmiotyHistoria = cast(substring(nast_rej.OldParams, 15, 2) as int),
				@BrakujacePrzedmiotyCzesneHistoria = cast(substring(nast_rej.OldParams, 17, 2) as int)
		from KartaEgz ke
		cross apply
		(
			select top 1 IDHistoriaStudenta, OldParams
			from HistoriaStudenta
			where IndeksID = ke.IndeksID
			and ZdarzenieID = 1
			and Anulowany = 0
			and (RokAkademicki * 100 + SemestrAkadID) > (ke.RokAkad * 100 + ke.SemestrID)
			and cast(substring(OldParams, 1, 1) as int) = ke.SemestrStudiow		
			order by DataDodania	
		) nast_rej
		where ke.IDKartaEgz = @IDKartaEgz	
		
		if (@NastepnaRejestracjaID is null)
		  begin
			set @BrakujacePrzedmiotyHistoria = -1
			set @BrakujacePrzedmiotyCzesneHistoria = -1
		  end
	 

		
		select @KERokAkadOd = Wartosc from UstawieniaProgramu where Nazwa = 'KERokAkadOd'
	    select @KESemestrIDOd = Wartosc from UstawieniaProgramu where Nazwa = 'KESemestrOdID'
		select @NominalnaLiczbaPrzedmiotow = dbo.KENominalnaLiczbaPrzedmiotow(@IDKartaEgz)

		;with ListaZaliczonychPrzedmiotow
		as
		(
			select  ke.IndeksID,
					ke.IDKartaEgz,
					kep.IDKartaEgzPoz,
					hr.IDSemReal,
					sop.CzyZalicza,
					sop.Wartosc,
					case when (select COUNT(*) from KartaEgzForma 
		                       left join SkalaOcenPoz on OcenaID = IDSkalaOcenPoz
		                       JOIN FormaWymiar ON KartaEgzForma.FormaWymiarID = FormaWymiar.IDFormaWymiar
                                LEFT JOIN FormaZajec ON FormaWymiar.FormaZajecID = FormaZajec.IDFormaZajec
		                        where KartaEgzPozID =kep.IDKartaEgzPoz	and CzyDrukowacNaKarcieEgz = 1 and (CzyZalicza = 0 or CzyZalicza  is null) and CzyPoprawkowy = 0)>0 then 0 else 1 end CzyZaliczaNowe, 
		            case when RokAkad*10+SemestrID >=@KERokAkadOd*10+@KESemestrIDOd then 1 else 0 end CzyNoweKarty,
					row_number() over (partition by IDSemReal order by kep.IDKartaEgzPoz desc) as NrWiersza
			from dbo.KartaEgzHistoriaRozliczen(@IndeksID, coalesce(@ZRokAkad, 0), coalesce(@ZSemestrID, 0), @RokAkad, @SemestrID, 1, null) hr
			join KartaEgzPoz kep on hr.IDSemReal = kep.SemestrRealizacjiID
			join KartaEgz ke on kep.KartaEgzID = ke.IDKartaEgz
							 and ke.IndeksID = @IndeksID							  
							 and (ke.RokAkad * 100 + ke.SemestrID) <= (@RokAkad * 100 + @SemestrID)  
							 and (ke.RokAkad * 100 + ke.SemestrID) >= (coalesce(@ZRokAkad, 0) * 100 + coalesce(@ZSemestrID, 0))
			left join SkalaOcenPoz sop on kep.OcenaKoncowaID = sop.IDSkalaOcenPoz
								  
		)
		
		select @ZaliczonePrzedmioty = count(IDSemReal)
		from ListaZaliczonychPrzedmiotow
		where NrWiersza = 1	and ((CzyZalicza = 1 and CzyNoweKarty = 0) or (CzyZaliczaNowe = 1 and CzyNoweKarty = 1))		  	

		set @BrakujacePrzedmioty = coalesce(@NominalnaLiczbaPrzedmiotow, 0) - coalesce(@ZaliczonePrzedmioty, 0)



		if @BrakujacePrzedmioty < 0
			set @BrakujacePrzedmioty = 0

		set @BrakujacePrzedmiotyCzesne = @BrakujacePrzedmioty

		if (@BrakujacePrzedmiotyHistoria = -1 and @BrakujacePrzedmiotyCzesneHistoria = -1) 
		  begin	
			set @Flaga = 0
			set @BrakujacePrzedmiotyHistoria = @BrakujacePrzedmioty
			set @BrakujacePrzedmiotyCzesneHistoria = @BrakujacePrzedmiotyCzesne
		  end
		else
		  begin
			if ((@BrakujacePrzedmioty - @BrakujacePrzedmiotyHistoria) = 0 and (@BrakujacePrzedmiotyCzesne - @BrakujacePrzedmiotyCzesneHistoria) = 0)
				set @Flaga = 0 
			else 
			  begin
				if ((@BrakujacePrzedmioty - @BrakujacePrzedmiotyHistoria) <> 0 and (@BrakujacePrzedmiotyCzesne - @BrakujacePrzedmiotyCzesneHistoria) <> 0) 
					set @Flaga = 3
				else if ((@BrakujacePrzedmioty - @BrakujacePrzedmiotyHistoria) <> 0)
					set @Flaga = 1
				else if ((@BrakujacePrzedmiotyCzesne - @BrakujacePrzedmiotyCzesneHistoria) <> 0)
					set @Flaga = 2			
			  end
		  end
		
	  end

	insert into @Output (Flaga, BrakujacePrzedmioty, BrakujacePrzedmiotyCzesne, BrakujacePrzedmiotyHistoria, BrakujacePrzedmiotyCzesneHistoria)
	select  @Flaga,
			@BrakujacePrzedmioty, 
			@BrakujacePrzedmiotyCzesne,
			@BrakujacePrzedmiotyHistoria,
			@BrakujacePrzedmiotyCzesneHistoria

	RETURN

END


GO


