IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_USR_UprawnianiaToRola_USR_Rola]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_UprawnianiaToRola]'))
ALTER TABLE [dbo].[USR_UprawnianiaToRola] DROP CONSTRAINT [FK_USR_UprawnianiaToRola_USR_Rola]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_USR_UprawnianiaToRola_USR_UprawnianiaDefinicje]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_UprawnianiaToRola]'))
ALTER TABLE [dbo].[USR_UprawnianiaToRola] DROP CONSTRAINT [FK_USR_UprawnianiaToRola_USR_UprawnianiaDefinicje]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[USR_UprawnianiaToRola]') AND name = N'PK_USR_UprawnianiaToRola')
ALTER TABLE [dbo].[USR_UprawnianiaToRola] DROP CONSTRAINT [PK_USR_UprawnianiaToRola]




--------------------------------------
----------------USR_UprawnianiaToRola---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[USR_UprawnianiaToRola]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[USR_UprawnianiaToRola]
GO

CREATE TABLE [dbo].[USR_UprawnianiaToRola] (
   [IDUSR_UprawnianiaToRola] [int]  IDENTITY(1,1) NOT NULL
  ,[USR_RolaID] [int]  NOT NULL
  ,[USR_UprawnieniaDefinicjeID] [int]  NOT NULL
  ,[CzyMozeWykonac] [bit]  NOT NULL DEFAULT ((0))
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO




ALTER TABLE [dbo].[USR_UprawnianiaToRola] ADD  CONSTRAINT [PK_USR_UprawnianiaToRola] PRIMARY KEY CLUSTERED( IDUSR_UprawnianiaToRola ) ON [PRIMARY]

ALTER TABLE [dbo].[USR_UprawnianiaToRola] WITH NOCHECK ADD CONSTRAINT [FK_USR_UprawnianiaToRola_USR_UprawnianiaDefinicje] FOREIGN KEY([USR_UprawnieniaDefinicjeID]) REFERENCES [dbo].[USR_UprawnianiaDefinicje] ([IDUSR_UprawnianiaDefinicje])

ALTER TABLE [dbo].[USR_UprawnianiaToRola] WITH NOCHECK ADD CONSTRAINT [FK_USR_UprawnianiaToRola_USR_Rola] FOREIGN KEY([USR_RolaID]) REFERENCES [dbo].[USR_Rola] ([IDUSR_Rola])
