------------wdro1-------------2018_02_05_01_FormaZajecNowaKolumna
------------wdro2-------------2018_02_05_05_ESS_PlanZajec_tab
------------wdro3-------------2018_02_05_10_ESS_WPD_WPWPlanySemestralny_prc
------------wdro4-------------2018_02_05_11_ESS_WPD_WPWPlanySzczegolowe_prc
------------wdro5-------------2018_02_06_01_TypAktywnosci
------------wdro6-------------2018_02_06_05_ZajeciaTypAktywnosci
------------wdro7-------------2018_02_07_01_ESS_WPD_WPWPlanySzczegolowe_prc
------------wdro8-------------2018_02_13_01_ESS_WPD_WPWProfil_tab
------------wdro9-------------2018_02_13_02_ESS_WPD_WPWProfilToRola_tab
------------wdro10------------2018_02_14_01_KE_ListaFormPrzedmiotow_fun
------------wdro11------------2018_02_15_01_ESS_WPD_WPWPlanySemestralny_prc
------------wdro12------------2018_02_21_01_ESS_WPD_USR_UprawnieniaDefinicje_tab
------------wdro13------------2018_02_21_04_ESS_WPD_USR_UprawnieniaToRola_tab
------------wdro14------------2018_02_26_01_ESS_WPD_USR_Rola_CzyPolitykaHasel_tab.sql



-----------------------------------------------------------------------------wdro1-------------------------------------------------------------------


-- =============================================
-- Author:		Agata Stelcer-Matuszczak
-- Create date: 2018-02-05
-- Description:	Dodanie kolumny "Kolor" do tabeli FormaZajec
-- =============================================



--------------------------------------
----------------FormaZajec---------------
-- Nale�y zweryfikowa� constrainty--
--------------------------------------
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RPRozliczenieKolejnosc_FormaZajec]') AND parent_object_id = OBJECT_ID(N'[dbo].[RPRozliczenieKolejnosc]'))
ALTER TABLE [dbo].[RPRozliczenieKolejnosc] DROP CONSTRAINT [FK_RPRozliczenieKolejnosc_FormaZajec]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RPWniosekPoz_FormaZajec]') AND parent_object_id = OBJECT_ID(N'[dbo].[RPWniosekPoz]'))
ALTER TABLE [dbo].[RPWniosekPoz] DROP CONSTRAINT [FK_RPWniosekPoz_FormaZajec]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[FormaZajec]') AND name = N'PK_FormaZajec')
ALTER TABLE [dbo].[FormaZajec] DROP CONSTRAINT [PK_FormaZajec]


if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[FormaZajec]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[FormaZajec] (
   [IDFormaZajec] [int]  IDENTITY(1,1) NOT NULL
  ,[Symbol] [varchar] (2) NULL
  ,[Nazwa] [varchar] (50) NOT NULL
  ,[Opis] [varchar] (200) NULL
  ,[Symbol6Znakow] [varchar] (6) NULL
  ,[CzyDrukowacNaKarcieEgz] [bit]  NOT NULL DEFAULT ((1))
  ,[Kolor] [varchar] (10) NULL
  ,[DataModyfikacji] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Dodal] [int]  NOT NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[FormaZajec] WITH NOCHECK ADD 
CONSTRAINT [PK_FormaZajec] PRIMARY KEY  CLUSTERED
(
  IDFormaZajec
) ON [PRIMARY]


End
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_FormaZajec] (
   [IDFormaZajec] [int]  IDENTITY(1,1) NOT NULL
  ,[Symbol] [varchar] (2) NULL
  ,[Nazwa] [varchar] (50) NOT NULL
  ,[Opis] [varchar] (200) NULL
  ,[Symbol6Znakow] [varchar] (6) NULL
  ,[CzyDrukowacNaKarcieEgz] [bit]  NOT NULL DEFAULT ((1))
  ,[Kolor] [varchar] (10) NULL
  ,[DataModyfikacji] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Dodal] [int]  NOT NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[Tmp_FormaZajec] WITH NOCHECK ADD 
CONSTRAINT [PK_FormaZajec] PRIMARY KEY  CLUSTERED
(
  IDFormaZajec
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_FormaZajec ON

IF EXISTS (SELECT * FROM dbo.FormaZajec)
EXEC('INSERT INTO dbo.Tmp_FormaZajec (IDFormaZajec, Symbol, Nazwa, Opis, Symbol6Znakow, CzyDrukowacNaKarcieEgz, DataModyfikacji, Zmodyfikowal, DataDodania, Dodal)
  SELECT IDFormaZajec, Symbol, Nazwa, Opis, Symbol6Znakow, CzyDrukowacNaKarcieEgz, DataModyfikacji, Zmodyfikowal, DataDodania, Dodal FROM dbo.FormaZajec WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_FormaZajec OFF

DROP TABLE dbo.FormaZajec

EXECUTE sp_rename N'dbo.Tmp_FormaZajec', N'FormaZajec', 'OBJECT'


END
ALTER TABLE [dbo].[RPWniosekPoz] WITH NOCHECK ADD CONSTRAINT [FK_RPWniosekPoz_FormaZajec] FOREIGN KEY([FormaZajecID]) REFERENCES [dbo].[FormaZajec] ([IDFormaZajec])

ALTER TABLE [dbo].[RPRozliczenieKolejnosc] WITH NOCHECK ADD CONSTRAINT [FK_RPRozliczenieKolejnosc_FormaZajec] FOREIGN KEY([FormaZajecID]) REFERENCES [dbo].[FormaZajec] ([IDFormaZajec])


go


-----------------------------------------------------------wdro2------------------------------------------------------------------

--------------------------------------
----------------PlanZajec---------------
-- Nale�y zweryfikowa� constrainty--
--------------------------------------
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GrupaZajeciowa_PlanZajec]') AND parent_object_id = OBJECT_ID(N'[dbo].[GrupaZajeciowa]'))
ALTER TABLE [dbo].[GrupaZajeciowa] DROP CONSTRAINT [FK_GrupaZajeciowa_PlanZajec]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ZajeciaWspolnePoz_PlanZajec]') AND parent_object_id = OBJECT_ID(N'[dbo].[ZajeciaWspolnePoz]'))
ALTER TABLE [dbo].[ZajeciaWspolnePoz] DROP CONSTRAINT [FK_ZajeciaWspolnePoz_PlanZajec]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PlanZajec]') AND name = N'PK_PlanZajec')
ALTER TABLE [dbo].[PlanZajec] DROP CONSTRAINT [PK_PlanZajec]


if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[PlanZajec]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[PlanZajec] (
   [IDPlanZajec] [int]  IDENTITY(1,1) NOT NULL
  ,[SiatkaID] [int]  NOT NULL
  ,[SekcjaID] [int]  NOT NULL
  ,[Opis] [varchar] (100) NULL
  ,[RokAkad] [smallint]  NULL
  ,[SemestrStudiow] [tinyint]  NULL
  ,[ZjazdID] [int]  NULL
  ,[SemestrID] [int]  NULL
  ,[PrzerwaPoXGodz] [tinyint]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataModyfikacji] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NOT NULL
  ,[Dokladnosc] [int]  NOT NULL
  ,[GodzOd] [int]  NOT NULL
  ,[GodzDo] [int]  NOT NULL
  ,[CzasPrzerwy] [int]  NOT NULL
  ,[CzasZajec] [int]  NOT NULL
  ,[StatusAktywny] [bit]  NOT NULL DEFAULT (0)
  ,[StatusAktywnyDydaktyk] [bit]  NOT NULL DEFAULT 0
  ,[Przypisy] [text]  NULL
  ,[EdycjaSesjaID] [int]  NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[PlanZajec] WITH NOCHECK ADD 
CONSTRAINT [PK_PlanZajec] PRIMARY KEY  CLUSTERED
(
  IDPlanZajec
) ON [PRIMARY]


End
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_PlanZajec] (
   [IDPlanZajec] [int]  IDENTITY(1,1) NOT NULL
  ,[SiatkaID] [int]  NOT NULL
  ,[SekcjaID] [int]  NOT NULL
  ,[Opis] [varchar] (100) NULL
  ,[RokAkad] [smallint]  NULL
  ,[SemestrStudiow] [tinyint]  NULL
  ,[ZjazdID] [int]  NULL
  ,[SemestrID] [int]  NULL
  ,[PrzerwaPoXGodz] [tinyint]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataModyfikacji] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NOT NULL
  ,[Dokladnosc] [int]  NOT NULL
  ,[GodzOd] [int]  NOT NULL
  ,[GodzDo] [int]  NOT NULL
  ,[CzasPrzerwy] [int]  NOT NULL
  ,[CzasZajec] [int]  NOT NULL
  ,[StatusAktywny] [bit]  NOT NULL DEFAULT (0)
  ,[StatusAktywnyDydaktyk] [bit]  NOT NULL DEFAULT 0
  ,[Przypisy] [text]  NULL
  ,[EdycjaSesjaID] [int]  NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[Tmp_PlanZajec] WITH NOCHECK ADD 
CONSTRAINT [PK_PlanZajec] PRIMARY KEY  CLUSTERED
(
  IDPlanZajec
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_PlanZajec ON

IF EXISTS (SELECT * FROM dbo.PlanZajec)
EXEC('INSERT INTO dbo.Tmp_PlanZajec (IDPlanZajec, SiatkaID, SekcjaID, Opis, RokAkad, SemestrStudiow, ZjazdID, SemestrID, PrzerwaPoXGodz, DataDodania, Dodal, DataModyfikacji, Zmodyfikowal, Dokladnosc, GodzOd, GodzDo, CzasPrzerwy, CzasZajec, StatusAktywny, Przypisy, EdycjaSesjaID)
  SELECT IDPlanZajec, SiatkaID, SekcjaID, Opis, RokAkad, SemestrStudiow, ZjazdID, SemestrID, PrzerwaPoXGodz, DataDodania, Dodal, DataModyfikacji, Zmodyfikowal, Dokladnosc, GodzOd, GodzDo, CzasPrzerwy, CzasZajec, StatusAktywny, Przypisy, EdycjaSesjaID FROM dbo.PlanZajec WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_PlanZajec OFF

DROP TABLE dbo.PlanZajec

EXECUTE sp_rename N'dbo.Tmp_PlanZajec', N'PlanZajec', 'OBJECT'


END
ALTER TABLE [dbo].[ZajeciaWspolnePoz] WITH NOCHECK ADD CONSTRAINT [FK_ZajeciaWspolnePoz_PlanZajec] FOREIGN KEY([PlanZajecID]) REFERENCES [dbo].[PlanZajec] ([IDPlanZajec])

ALTER TABLE [dbo].[PlanZajec] WITH NOCHECK ADD CONSTRAINT [FK_PlanZajec_Zjazd] FOREIGN KEY([ZjazdID]) REFERENCES [dbo].[Zjazd] ([IDZjazd])

ALTER TABLE [dbo].[PlanZajec] WITH NOCHECK ADD CONSTRAINT [FK_PlanZajec_Siatka] FOREIGN KEY([SiatkaID]) REFERENCES [dbo].[Siatka] ([IDSiatka])

ALTER TABLE [dbo].[PlanZajec] WITH NOCHECK ADD CONSTRAINT [FK_PlanZajec_LOG_SesjeUzytkownika] FOREIGN KEY([EdycjaSesjaID]) REFERENCES [dbo].[LOG_SesjeUzytkownika] ([IDSesja])

ALTER TABLE [dbo].[GrupaZajeciowa] WITH NOCHECK ADD CONSTRAINT [FK_GrupaZajeciowa_PlanZajec] FOREIGN KEY([PlanZajecID]) REFERENCES [dbo].[PlanZajec] ([IDPlanZajec])

go



---------------------------------------------------------------wdro3-----------------------------------------------------------------------------

-- ******************* -- WPWPlanySemestralny -- ******************* --

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WPWPlanySemestralny]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WPWPlanySemestralny]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[WPWPlanySemestralny] @pracownikID INT, @Sem INT, @Rok INT
AS


declare @table table(id int, liczbaKier int, liczbaSpec int, kier int, spec int)
  declare @TabDaty table (id int identity(8,1), Daty varchar(1000))

	insert into @table
		select distinct idplanzajecpoz,count( distinct spec.kierunekid), count(distinct SpecjalnoscID), max(spec.kierunekID), max(specjalnoscID)
		from PlanZajecPoz pzp
 	  join GrupaZajeciowa gz on pzp.GrupaZajeciowaID = IDgrupaZajeciowa
		left join PlanPozPracownik ppp on pzp.IDplanZajecPoz = ppp.planZajecpozid
	  join PlanZajec pz on pz.IDplanZajec = gz.PlanZajecID or
		pz.IDplanZajec in (select PlanZajecID from ZajeciaWspolnePoz where ZajeciaWspolneID = gz.ZajeciaWspolneID)
		join siatka on siatkaid = idsiatka
		join program on programid = idprogram
		join Specjalnosc spec on specjalnoscid = idspecjalnosc

		where ppp.pracownikid = @PracownikID
		and pz.RokAkad = @Rok
		and pz.SemestrID = @Sem
		group by idplanzajecpoz

   insert into @TabDaty
    select dbo.DatyZKalendarza(idplanzajecpoz)
    from PlanZajecPoz
    join @table tab on idplanzajecpoz = tab.id
    join zajeciaStatus on ZajeciaStatusID = IDZajeciaStatus 
    where CzyKalendarz = 1

		select
			distinct
			IDPlanZajecPoz,
			gz.IDGrupaZajeciowa,
			gz.FormaWymiarID,
      pz.SemestrStudiow Sem,
			prz.Nazwa + coalesce(' - ' + nullif(fw.NazwaFormy,''),'') PrzedmiotNazwa,
			fz.Symbol6Znakow FormaZajec,
			sr.Numer as SRNumer,
			coalesce(bud.Symbol + ' ' + cast(sala.Numer as varchar(6)),'<bez sali>') Gdzie,
			dbo.FormatCzas(OdGodz) OdGodziny,
			dbo.FormatCzas(OdGodz + LGodz*pz.CzasZajec + CzasPrzerw) +
			case when zs.CoXtyg = 2 and CzyKalendarz = 0 then case when zs.NzX = 1 then +' (*)' else +' (**)' end else '' end as DoGodziny,
      odGodz as OdMinuty,
			case when zs.CzyKalendarz = 0 then dt.Nazwa else dbo.DatyZKalendarza(pzp.IDPlanZajecPoz) end Kiedy,
			case when zs.CzyKalendarz = 0 then 1+(6+(pzp.dzienTygodniaID-1)%7)%7 else
        ( select top 1 ID from @tabDaty where Daty = dbo.DatyZKalendarza(pzp.IDPlanZajecPoz) ) end DzienID,

			prz.Kod + ' - ' +ku.koduzupelniajacy Kod,

			case when ( select liczbaKier from @table where id = pzp.idplanzajecpoz ) = 1 then
			(
				select '<'+rtrim(w.Symbol)+'> '+k.Symbol from Kierunek   k
        join Wydzial w on WydzialID = IDWydzial
				where IDkierunek = (select kier from @table where id = pzp.idplanzajecpoz )

			)
			else
			 '' end KierSymbol,

			case when ( select LiczbaSpec from @table where id = pzp.idplanzajecpoz	) = 1 then
			(
				select '<' +ss.Symbol + '> ' +spec.Nazwa from Specjalnosc spec

				join SystemStudiow SS on SystemStudiowID = IDSystemStudiow
				where IDSpecjalnosc = (select spec from @table where id = pzp.idplanzajecpoz )
			)
			else '' end SpecNazwa,

			tryb.symbol as TrybSymbol,
	    dbo.GrupyStudenckieStr(pzp.GrupaZajeciowaID) Grupy,
      Zjazd.Nazwa as ZjazdNazwa,
      Sekcja.Nazwa as SekcjaNazwa,
      Sekcja.Symbol as SekcjaSymbol,
	  fz.Kolor as FormaKolor
      
		from PlanZajecPoz pzp
 	  join GrupaZajeciowa gz on pzp.GrupaZajeciowaID = IDgrupaZajeciowa
	  join ZajeciaStatus zs on ZajeciaStatusid = idZajeciaStatus

		-- do grupy
		left join PlanPozGrupa ppg on ppg.GrupaZajeciowaId = gz.IdGrupazajeciowa

		-- rodzaj studi�w
	  join PlanZajec pz on pz.IDplanZajec = gz.PlanZajecID or
		pz.IDplanZajec in (select PlanZajecID from ZajeciaWspolnePoz where ZajeciaWspolneID = gz.ZajeciaWspolneID)
		join siatka on siatkaID = IDSiatka
		join program pr on siatka.programid = idprogram
 	  join TrybStudiow tryb on siatka.TrybstudiowID = IDtrybStudiow
    join Zjazd on pz.ZjazdID = IDZjazd
    join Sekcja on SekcjaID = IDSekcja

		-- do przedmiotu
		left join FormaWymiar fw on FormaWymiarID = IDformaWymiar
		left join SemestrRealizacji sr on IDSemestrRealizacji = fw.SemestrRealizacjiID
		left join KartaUzup ku on sr.KartaUzupID = IDkartaUzup
		left join Przedmiot prz on ku.PrzedmiotID = IDprzedmiot
		left join FormaZajec fz on fw.FormaZajecID = IDFormaZajec

		-- do sali
		left join Sala on pzp.SalaID = IDsala
		left join Budynek bud on budynekid = idbudynek

		-- dzienTygodnia
		left join DzienTygodnia dt on pzp.DzienTygodniaID = IDDzienTygodnia

		where pzp.idPlanZajecPoz in (select ID from @Table)

    order by DzienID, odGodz
	



GO


----------------------------------------------------------wdro4----------------------------------------------------------------------


-- ******************* -- WPWPlanySzczegolowe -- ******************* --

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WPWPlanySzczegolowe]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WPWPlanySzczegolowe]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[WPWPlanySzczegolowe] @pracownikID INT, @DataOD DATE, @DataDO DATE, @SprawdzAktywnosc BIT
AS
select distinct 
  [gz].[IDGrupaZajeciowa]
, DATEADD(minute, [ppd].[OdGodz], [ppd].[Data]) AS [DataOD]
, DATEADD(minute, [ppd].[DoGodz], [ppd].[Data]) AS [DataDO]
, [b].[Symbol] + [s].[Numer] AS [Sala]
, [b].[Adres] AS [Adres]
, CASE
	WHEN [fw].[NazwaFormy] IS NOT NULL THEN [p].[Nazwa] + ': ' + [fw].[NazwaFormy]
	WHEN [fw].[NazwaFormy] IS NULL THEN [p].[Nazwa]
end as [PNazwa]
, [fz].[Symbol6Znakow] as [TypZajecSkrot]
, [fz].[Nazwa] as [TypZajec]
, [fz].[Kolor] as [FormaKolor]
from
			[PlanPozDef] [ppd]
JOIN		[PlanZajecPoz] [pzp]		on [ppd].[PlanZajecPozIDNr] = [pzp].[IDPlanZajecPoz]
LEFT JOIN	[GrupaZajeciowa] [gz]		on [pzp].[GrupaZajeciowaID] = [gz].[IDGrupaZajeciowa]
JOIN		[FormaWymiar] [fw]			on [gz].[FormaWymiarID] = [fw].[IDFormaWymiar]
JOIN		[SemestrRealizacji] [sr]	on [sr].[IDSemestrRealizacji] = [FW].[SemestrRealizacjiID]
JOIN		[KartaUzup] [ku]			on [ku].[IDKartaUzup] = [sr].[KartaUzupID]
JOIN		[Przedmiot] [p]				on [p].[IDPrzedmiot] = [ku].[PrzedmiotID]
LEFT JOIN	[PlanZajec] [pz]			on [ppd].[PlanZajecPozIDNr] = [pz].[IDPlanZajec]
JOIN		[Sala] [s]					on [s].[IDSala] = [ppd].[SalaIDNr]
JOIN		[Budynek] [b]				on [b].[IDBudynek] = [s].[BudynekID]
LEFT JOIN	[PakietZaj] [pakiet]		on [ppd].[PakietIDNr] = [pakiet].[IDPakietZaj]
LEFT JOIN	[FormaZajec] [fz]			on [fz].[IDFormaZajec] = [fw].[FormaZajecID]
where 
[ppd].[PracownikIDNr] = @pracownikID
and  (@SprawdzAktywnosc = 0 OR ([pz].[StatusAktywny] = 1 OR [pz].[StatusAktywny] is null))
and [ppd].Data >= @DataOD
and ppd.Data <= @DataDO 
group by [gz].[IDGrupaZajeciowa], [P].[Nazwa], [ppd].[Data], [ppd].[OdGodz], [ppd].[DoGodz], [FW].[NazwaFormy], [B].[Symbol], [S].[Numer], [B].[Adres], [fz].[Symbol6Znakow], [fz].[Nazwa], [fz].[Kolor]
order by [DataOd] desc


GO


-----------------------------------------------------------------wdro5----------------------------------------------------------------



IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Tmp_TypUc__CzyFo__5F4BB751]') AND parent_object_id = OBJECT_ID(N'[dbo].[TypUczestnictwa]'))
ALTER TABLE [dbo].[TypUczestnictwa] DROP CONSTRAINT [DF__Tmp_TypUc__CzyFo__5F4BB751]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ZajeciaTypUczestnictwa_TypUczestnictwa]') AND parent_object_id = OBJECT_ID(N'[dbo].[ZajeciaTypUczestnictwa]'))
ALTER TABLE [dbo].[ZajeciaTypUczestnictwa] DROP CONSTRAINT [FK_ZajeciaTypUczestnictwa_TypUczestnictwa]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TypUczestnictwa]') AND name = N'PK_TypUczestnictwa')
ALTER TABLE [dbo].[TypUczestnictwa] DROP CONSTRAINT [PK_TypUczestnictwa]
--------------------------------------
----------------TypUczestnictwa---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[TypUczestnictwa]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[TypUczestnictwa]
GO

CREATE TABLE [dbo].[TypUczestnictwa] (
   [IDTypUczestnictwa] [int]  IDENTITY(1,1) NOT NULL
  ,[NazwaTypu] [varchar] (256) NOT NULL
  ,[Opis] [varchar] (500) NULL
  ,[CzyFormaWymiar] [bit]  NOT NULL DEFAULT ((0))
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO




ALTER TABLE [dbo].[TypUczestnictwa] ADD  CONSTRAINT [PK_TypUczestnictwa] PRIMARY KEY CLUSTERED( IDTypUczestnictwa ) ON [PRIMARY]

go

---------------------------------------------------wdro6---------------------------------------------------------------


IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ZajeciaTypUczestnictwa_FormaWymiar]') AND parent_object_id = OBJECT_ID(N'[dbo].[ZajeciaTypUczestnictwa]'))
ALTER TABLE [dbo].[ZajeciaTypUczestnictwa] DROP CONSTRAINT [FK_ZajeciaTypUczestnictwa_FormaWymiar]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ZajeciaTypUczestnictwa_TypUczestnictwa]') AND parent_object_id = OBJECT_ID(N'[dbo].[ZajeciaTypUczestnictwa]'))
ALTER TABLE [dbo].[ZajeciaTypUczestnictwa] DROP CONSTRAINT [FK_ZajeciaTypUczestnictwa_TypUczestnictwa]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ZajeciaTypUczestnictwa]') AND name = N'PK_ZajeciaTypUczestnictwa')
ALTER TABLE [dbo].[ZajeciaTypUczestnictwa] DROP CONSTRAINT [PK_ZajeciaTypUczestnictwa]


--------------------------------------
----------------ZajeciaTypUczestnictwa---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[ZajeciaTypUczestnictwa]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[ZajeciaTypUczestnictwa]
GO

CREATE TABLE [dbo].[ZajeciaTypUczestnictwa] (
   [IDZajeciaTypUczestnictwa] [int]  IDENTITY(1,1) NOT NULL
  ,[TypUczestnictwaID] [int]  NOT NULL
  ,[Podmiot] [varchar] (100) NOT NULL
  ,[PodmiotID] [int]  NOT NULL
  ,[FormaWymiarID] [int]  NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO




ALTER TABLE [dbo].[ZajeciaTypUczestnictwa] ADD  CONSTRAINT [PK_ZajeciaTypUczestnictwa] PRIMARY KEY CLUSTERED( IDZajeciaTypUczestnictwa ) ON [PRIMARY]

ALTER TABLE [dbo].[ZajeciaTypUczestnictwa] WITH NOCHECK ADD CONSTRAINT [FK_ZajeciaTypUczestnictwa_TypUczestnictwa] FOREIGN KEY([TypUczestnictwaID]) REFERENCES [dbo].[TypUczestnictwa] ([IDTypUczestnictwa])

ALTER TABLE [dbo].[ZajeciaTypUczestnictwa] WITH NOCHECK ADD CONSTRAINT [FK_ZajeciaTypUczestnictwa_FormaWymiar] FOREIGN KEY([FormaWymiarID]) REFERENCES [dbo].[FormaWymiar] ([IDFormaWymiar])


go

----------------------------------------------------------------wdro7----------------------------------------------------------------------


-- ******************* -- WPWPlanySzczegolowe -- ******************* --

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WPWPlanySzczegolowe]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WPWPlanySzczegolowe]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[WPWPlanySzczegolowe] @pracownikID INT, @DataOD DATE, @DataDO DATE, @SprawdzAktywnosc BIT
AS
select distinct 
  [gz].[IDGrupaZajeciowa]
, DATEADD(minute, [ppd].[OdGodz], [ppd].[Data]) AS [DataOD]
, DATEADD(minute, [ppd].[DoGodz], [ppd].[Data]) AS [DataDO]
, [b].[Symbol] + [s].[Numer] AS [Sala]
, [b].[Adres] AS [Adres]
, CASE
	WHEN [fw].[NazwaFormy] IS NOT NULL THEN [p].[Nazwa] + ': ' + [fw].[NazwaFormy]
	WHEN [fw].[NazwaFormy] IS NULL THEN [p].[Nazwa]
end as [PNazwa]
, [fz].[Symbol6Znakow] as [TypZajecSkrot]
, [fz].[Nazwa] as [TypZajec]
, [fz].[Kolor] as [FormaKolor]
from
			[PlanPozDef] [ppd]
JOIN		[PlanZajecPoz] [pzp]		on [ppd].[PlanZajecPozIDNr] = [pzp].[IDPlanZajecPoz]
LEFT JOIN	[GrupaZajeciowa] [gz]		on [pzp].[GrupaZajeciowaID] = [gz].[IDGrupaZajeciowa]
JOIN		[FormaWymiar] [fw]			on [gz].[FormaWymiarID] = [fw].[IDFormaWymiar]
JOIN		[SemestrRealizacji] [sr]	on [sr].[IDSemestrRealizacji] = [FW].[SemestrRealizacjiID]
JOIN		[KartaUzup] [ku]			on [ku].[IDKartaUzup] = [sr].[KartaUzupID]
JOIN		[Przedmiot] [p]				on [p].[IDPrzedmiot] = [ku].[PrzedmiotID]
JOIN		[PlanZajec] [pz]			on [ppd].[PlanZajecPozIDNr] = [pz].[IDPlanZajec]
LEFT JOIN	[Sala] [s]					on [s].[IDSala] = [ppd].[SalaIDNr]
LEFT JOIN	[Budynek] [b]				on [b].[IDBudynek] = [s].[BudynekID]
LEFT JOIN	[PakietZaj] [pakiet]		on [ppd].[PakietIDNr] = [pakiet].[IDPakietZaj]
LEFT JOIN	[FormaZajec] [fz]			on [fz].[IDFormaZajec] = [fw].[FormaZajecID]
where
[ppd].[PracownikIDNr] = @pracownikID
and  (@SprawdzAktywnosc = 0 OR ([pz].[StatusAktywnyDydaktyk] = 1))
and [ppd].Data >= @DataOD
and ppd.Data <= @DataDO 
group by [gz].[IDGrupaZajeciowa], [P].[Nazwa], [ppd].[Data], [ppd].[OdGodz], [ppd].[DoGodz], [FW].[NazwaFormy], [B].[Symbol], [S].[Numer], [B].[Adres], [fz].[Symbol6Znakow], [fz].[Nazwa], [fz].[Kolor]

UNION

select distinct 
  [gz].[IDGrupaZajeciowa]
, DATEADD(minute, [ppd].[OdGodz], [ppd].[Data]) AS [DataOD]
, DATEADD(minute, [ppd].[DoGodz], [ppd].[Data]) AS [DataDO]
, [b].[Symbol] + [s].[Numer] AS [Sala]
, [b].[Adres] AS [Adres]
, CASE
	WHEN [fw].[NazwaFormy] IS NOT NULL THEN [p].[Nazwa] + ': ' + [fw].[NazwaFormy]
	WHEN [fw].[NazwaFormy] IS NULL THEN [p].[Nazwa]
end as [PNazwa]
, [fz].[Symbol6Znakow] as [TypZajecSkrot]
, [fz].[Nazwa] as [TypZajec]
, [fz].[Kolor] as [FormaKolor]
from
			[PlanPozDef] [ppd]
JOIN		[PlanZajecPoz] [pzp]		on [ppd].[PlanZajecPozIDNr] = [pzp].[IDPlanZajecPoz]
LEFT JOIN	[GrupaZajeciowa] [gz]		on [pzp].[GrupaZajeciowaID] = [gz].[IDGrupaZajeciowa]
JOIN		[FormaWymiar] [fw]			on [gz].[FormaWymiarID] = [fw].[IDFormaWymiar]
JOIN		[SemestrRealizacji] [sr]	on [sr].[IDSemestrRealizacji] = [FW].[SemestrRealizacjiID]
JOIN		[KartaUzup] [ku]			on [ku].[IDKartaUzup] = [sr].[KartaUzupID]
JOIN		[Przedmiot] [p]				on [p].[IDPrzedmiot] = [ku].[PrzedmiotID]
LEFT JOIN	[ZajeciaWspolne] [zw]		on [ppd].[ZajeciaWspolneIDNr] = [zw].[IDZajeciaWspolne]
LEFT JOIN		[ZajeciaWspolnePoz] [zwp]	on [zw].[IDZajeciaWspolne] = [zwp].[ZajeciaWspolneID]
LEFT JOIN	[PlanZajec] [pz]			on [zwp].[PlanZajecID] = [pz].[IDPlanZajec]
LEFT JOIN	[Sala] [s]					on [s].[IDSala] = [ppd].[SalaIDNr]
LEFT JOIN	[Budynek] [b]				on [b].[IDBudynek] = [s].[BudynekID]
LEFT JOIN	[PakietZaj] [pakiet]		on [ppd].[PakietIDNr] = [pakiet].[IDPakietZaj]
LEFT JOIN	[FormaZajec] [fz]			on [fz].[IDFormaZajec] = [fw].[FormaZajecID]
LEFT JOIN	[PlanPozPracownik] [ppp]	on [pzp].[IDplanZajecPoz] = [ppp].[planZajecpozid]
where 
([ppp].[PracownikID] = @pracownikID OR [ppd].[PracownikIDNr] = @pracownikID)
and  (@SprawdzAktywnosc = 0 OR ([pz].[StatusAktywnyDydaktyk] = 1 OR [pz].[StatusAktywnyDydaktyk] is null))
and [ppd].Data >= @DataOD
and ppd.Data <= @DataDO 
group by [gz].[IDGrupaZajeciowa], [P].[Nazwa], [ppd].[Data], [ppd].[OdGodz], [ppd].[DoGodz], [FW].[NazwaFormy], [B].[Symbol], [S].[Numer], [B].[Adres], [fz].[Symbol6Znakow], [fz].[Nazwa], [fz].[Kolor]
order by [DataOd] desc


GO

--------------------------------------------------------wdro8--------------------------------------------------------


--------------------------------------
----------------USR_Profil---------------
-- Nale�y zweryfikowa� constrainty--
--------------------------------------
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__USR_Histo__USR_P__6FE211CD]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_HistoriaAkcji]'))
ALTER TABLE [dbo].[USR_HistoriaAkcji] DROP CONSTRAINT [FK__USR_Histo__USR_P__6FE211CD]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__USR_Profi__USR_P__6B1D5CB0]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_ProfilToRola]'))
ALTER TABLE [dbo].[USR_ProfilToRola] DROP CONSTRAINT [FK__USR_Profi__USR_P__6B1D5CB0]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_USR_HistoriaHasla_USR_Profil]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_HistoriaHasla]'))
ALTER TABLE [dbo].[USR_HistoriaHasla] DROP CONSTRAINT [FK_USR_HistoriaHasla_USR_Profil]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[USR_Profil]') AND name = N'PK_USR_Profil')
ALTER TABLE [dbo].[USR_Profil] DROP CONSTRAINT [PK_USR_Profil]


if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[USR_Profil]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[USR_Profil] (
   [IDUSR_Profil] [int]  IDENTITY(1000000,1) NOT NULL
  ,[Email] [varchar] (254) NULL
  ,[Login] [varchar] (50) NULL
  ,[HasloPlain] [varchar] (128) NULL
  ,[HasloHash] [varchar] (160) NULL
  ,[ZiarnoHasla] [varchar] (32) NULL
  ,[Imie] [varchar] (50) NOT NULL
  ,[DrugieImie] [varchar] (50) NULL
  ,[NickName] [varchar] (50) NULL
  ,[Nazwisko] [varchar] (50) NOT NULL
  ,[PrefiksTelefonu] [varchar] (5) NULL
  ,[Telefon] [varchar] (20) NULL
  ,[CzyHasloJednorazowe] [bit]  NULL
  ,[CzyKontoAktywne] [bit]  NOT NULL DEFAULT ((0))
  ,[DataOstatniejZmianyHasla] [datetime]  NULL
  ,[Token] [uniqueidentifier]  NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[USR_Profil] WITH NOCHECK ADD 
CONSTRAINT [PK_USR_Profil] PRIMARY KEY  CLUSTERED
(
  IDUSR_Profil
) ON [PRIMARY]


End
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_USR_Profil] (
   [IDUSR_Profil] [int]  IDENTITY(1000000,1) NOT NULL
  ,[Email] [varchar] (254) NULL
  ,[Login] [varchar] (50) NULL
  ,[HasloPlain] [varchar] (128) NULL
  ,[HasloHash] [varchar] (160) NULL
  ,[ZiarnoHasla] [varchar] (32) NULL
  ,[Imie] [varchar] (50) NOT NULL
  ,[DrugieImie] [varchar] (50) NULL
  ,[NickName] [varchar] (50) NULL
  ,[Nazwisko] [varchar] (50) NOT NULL
  ,[PrefiksTelefonu] [varchar] (5) NULL
  ,[Telefon] [varchar] (20) NULL
  ,[CzyHasloJednorazowe] [bit]  NULL
  ,[CzyKontoAktywne] [bit]  NOT NULL DEFAULT ((0))
  ,[DataOstatniejZmianyHasla] [datetime]  NULL
  ,[Token] [uniqueidentifier]  NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[Tmp_USR_Profil] WITH NOCHECK ADD 
CONSTRAINT [PK_USR_Profil] PRIMARY KEY  CLUSTERED
(
  IDUSR_Profil
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_USR_Profil ON

IF EXISTS (SELECT * FROM dbo.USR_Profil)
EXEC('INSERT INTO dbo.Tmp_USR_Profil (IDUSR_Profil, Email, Login, HasloPlain, HasloHash, ZiarnoHasla, Imie, DrugieImie, NickName, Nazwisko, PrefiksTelefonu, Telefon, CzyHasloJednorazowe, CzyKontoAktywne, DataOstatniejZmianyHasla, Token, Dodal, DataDodania, Zmodyfikowal, DataModyfikacji)
  SELECT IDUSR_Profil, Email, Login, HasloPlain, HasloHash, ZiarnoHasla, Imie, DrugieImie, NickName, Nazwisko, PrefiksTelefonu, Telefon, CzyHasloJednorazowe, CzyKontoAktywne, DataOstatniejZmianyHasla, Token, Dodal, DataDodania, Zmodyfikowal, DataModyfikacji FROM dbo.USR_Profil WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_USR_Profil OFF

DROP TABLE dbo.USR_Profil

EXECUTE sp_rename N'dbo.Tmp_USR_Profil', N'USR_Profil', 'OBJECT'


END
ALTER TABLE [dbo].[USR_HistoriaHasla] WITH NOCHECK ADD CONSTRAINT [FK_USR_HistoriaHasla_USR_Profil] FOREIGN KEY([USR_ProfilID]) REFERENCES [dbo].[USR_Profil] ([IDUSR_Profil])

ALTER TABLE [dbo].[USR_ProfilToRola] WITH NOCHECK ADD CONSTRAINT [FK__USR_Profi__USR_P__6B1D5CB0] FOREIGN KEY([USR_ProfilID]) REFERENCES [dbo].[USR_Profil] ([IDUSR_Profil])

ALTER TABLE [dbo].[USR_HistoriaAkcji] WITH NOCHECK ADD CONSTRAINT [FK__USR_Histo__USR_P__6FE211CD] FOREIGN KEY([USR_ProfilID]) REFERENCES [dbo].[USR_Profil] ([IDUSR_Profil])

go

----------------------------------------------------------------wdro9-----------------------------------------------------------------------


--------------------------------------
----------------USR_ProfilToRola---------------
-- Nale�y zweryfikowa� constrainty--
--------------------------------------
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[USR_ProfilToRola]') AND name = N'PK_USR_ProfilToRola')
ALTER TABLE [dbo].[USR_ProfilToRola] DROP CONSTRAINT [PK_USR_ProfilToRola]


if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[USR_ProfilToRola]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[USR_ProfilToRola] (
   [IDUSR_ProfilToRola] [int]  IDENTITY(1000000,1) NOT NULL
  ,[USR_ProfilID] [int]  NOT NULL
  ,[USR_RolaID] [int]  NOT NULL
  ,[KluczZdalny] [int]  NULL
  ,[Aktywny] [bit]  NOT NULL DEFAULT ((1))
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[USR_ProfilToRola] WITH NOCHECK ADD 
CONSTRAINT [PK_USR_ProfilToRola] PRIMARY KEY  CLUSTERED
(
  IDUSR_ProfilToRola
) ON [PRIMARY]


End
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_USR_ProfilToRola] (
   [IDUSR_ProfilToRola] [int]  IDENTITY(1000000,1) NOT NULL
  ,[USR_ProfilID] [int]  NOT NULL
  ,[USR_RolaID] [int]  NOT NULL
  ,[KluczZdalny] [int]  NULL
  ,[Aktywny] [bit]  NOT NULL DEFAULT ((1))
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[Tmp_USR_ProfilToRola] WITH NOCHECK ADD 
CONSTRAINT [PK_USR_ProfilToRola] PRIMARY KEY  CLUSTERED
(
  IDUSR_ProfilToRola
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_USR_ProfilToRola ON

IF EXISTS (SELECT * FROM dbo.USR_ProfilToRola)
EXEC('INSERT INTO dbo.Tmp_USR_ProfilToRola (IDUSR_ProfilToRola, USR_ProfilID, USR_RolaID, KluczZdalny, Aktywny, Dodal, DataDodania, Zmodyfikowal, DataModyfikacji)
  SELECT IDUSR_ProfilToRola, USR_ProfilID, USR_RolaID, KluczZdalny, Aktywny, Dodal, DataDodania, Zmodyfikowal, DataModyfikacji FROM dbo.USR_ProfilToRola WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_USR_ProfilToRola OFF

DROP TABLE dbo.USR_ProfilToRola

EXECUTE sp_rename N'dbo.Tmp_USR_ProfilToRola', N'USR_ProfilToRola', 'OBJECT'


END
ALTER TABLE [dbo].[USR_ProfilToRola] WITH NOCHECK ADD CONSTRAINT [FK__USR_Profi__USR_R__6C1180E9] FOREIGN KEY([USR_RolaID]) REFERENCES [dbo].[USR_Rola] ([IDUSR_Rola])

ALTER TABLE [dbo].[USR_ProfilToRola] WITH NOCHECK ADD CONSTRAINT [FK__USR_Profi__USR_P__6B1D5CB0] FOREIGN KEY([USR_ProfilID]) REFERENCES [dbo].[USR_Profil] ([IDUSR_Profil])

go

--------------------------------------------------------------------------------wdro10------------------------------------------------------------------------





IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KE_ListaFormPrzedmiotu]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[KE_ListaFormPrzedmiotu]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[KE_ListaFormPrzedmiotu] (@IndeksID int, @RokAkad int, @SemestrID int, @CzyPoprawkowy bit)  
RETURNS @Output table (SemestrRealizacjiID int, IDFormaWymiar int, NazwaFormy varchar(500), Przedmiot varchar(500), FormaZajec varchar(10))
BEGIN

	
 declare @WyzerowanieRokAkad int, @WyzerowanieSemAkad int
  select @WyzerowanieRokAkad=RokAkademicki,
         @WyzerowanieSemAkad=SemestrAkadID
  from HistoriaStudenta
  where IndeksID=@IndeksID and ZdarzenieID=32 and Anulowany=0
  order by RokAkademicki desc,  SemestrAkadID desc


  -- szukamy poprzedniej karty egz.
  declare @KartaEgzPoprzID int, @SemStudenta int
  select @SemStudenta=Semestr from Indeks where idindeks=@IndeksID


    declare @OldRok int, @oldSemLp int, @oldSem int
    select top 1 @KartaEgzPoprzID = IDKartaEgz, @OldRok=RokAkad, @OldSemLp=Lp, @OldSem=SemestrStudiow  from KartaEgz
    join OkresRozliczeniowyPoz on SemestrID=IDOkresRozliczeniowyPoz
    where IndeksID=@IndeksID and ((RokAkad*10+Lp < @RokAkad*10+@SemestrID) or
                                  (RokAkad*10+Lp <= @RokAkad*10+@SemestrID) and KartaPoczatkowa=1) 
          and RokAkad*10+Lp >= coalesce(@WyzerowanieRokAkad,0)*10+coalesce(@WyzerowanieSemAkad,0)
    order by RokAkad desc, Lp desc, KartaPoczatkowa asc

  
   insert into @Output
    select KartaEgzPoz.SemestrRealizacjiID, FormaWymiarID, NazwaFormy, Przedmiot.Nazwa as Przedmiot, FormaZajec.Symbol6Znakow
		from KartaEgzPoz
    join KartaEgzForma on KartaEgzPozID = IDKartaEgzPoz
    left join SkalaOcenPoz on OcenaID=IDSkalaOcenPoz
    left join FormaWymiar on FormaWymiarID = IDFormaWymiar
    left join SemestrRealizacji on KartaEgzPoz.SemestrRealizacjiID = IDSemestrRealizacji
    left join KartaUzup on KartaUzupID = IDKartaUzup
    left join Przedmiot on PrzedmiotID = IDPrzedmiot
    left join FormaZajec on FormaZajecID = IDFormaZajec
    where KartaEgzID=@KartaEgzPoprzID and coalesce(CzyZalicza,0) = 0 and @CzyPoprawkowy = 1 and CzyDrukowacNaKarcieEgz = 1
    UNION
    select krpp.SemestrRealizacjiID, IDFormaWymiar, NazwaFormy, Przedmiot.Nazwa as Przedmiot, FormaZajec.Symbol6Znakow from KartaRoznicProgramowych
    left join KartaRoznicProgramowychPoz krpp on KartaRoznicProgramowychID = IDKartaRoznicProgramowych
    left join SemestrRealizacji on SemestrRealizacjiID = IDSemestrRealizacji
    left join KartaUzup on KartaUzupID = IDKartaUzup
    left join Przedmiot on PrzedmiotID = IDPrzedmiot
    join FormaWymiar on FormaWymiar.SemestrRealizacjiID = IDSemestrRealizacji
    left join FormaZajec on FormaZajecID = IDFormaZajec
    where IndeksID = @IndeksID and @CzyPoprawkowy = 0 and CzyDrukowacNaKarcieEgz = 1

	return

END


GO


----------------------------------------------------------------------------wdro11-----------------------------------------------------------------------


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WPWPlanySemestralny]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WPWPlanySemestralny]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[WPWPlanySemestralny] @pracownikID INT, @Sem INT, @Rok INT
AS


declare @table table(id int, liczbaKier int, liczbaSpec int, kier int, spec int)
  declare @TabDaty table (id int identity(8,1), Daty varchar(1000))

	insert into @table
		select distinct idplanzajecpoz,count( distinct spec.kierunekid), count(distinct SpecjalnoscID), max(spec.kierunekID), max(specjalnoscID)
		from PlanZajecPoz pzp
 	  join GrupaZajeciowa gz on pzp.GrupaZajeciowaID = IDgrupaZajeciowa
		left join PlanPozPracownik ppp on pzp.IDplanZajecPoz = ppp.planZajecpozid
	  join PlanZajec pz on pz.IDplanZajec = gz.PlanZajecID or
		pz.IDplanZajec in (select PlanZajecID from ZajeciaWspolnePoz where ZajeciaWspolneID = gz.ZajeciaWspolneID)
		join siatka on siatkaid = idsiatka
		join program on programid = idprogram
		join Specjalnosc spec on specjalnoscid = idspecjalnosc

		where ppp.pracownikid = @PracownikID
		and pz.RokAkad = @Rok
		and pz.SemestrID = @Sem
		and pz.StatusAktywnyDydaktyk = 1
		group by idplanzajecpoz

   insert into @TabDaty
    select dbo.DatyZKalendarza(idplanzajecpoz)
    from PlanZajecPoz
    join @table tab on idplanzajecpoz = tab.id
    join zajeciaStatus on ZajeciaStatusID = IDZajeciaStatus 
    where CzyKalendarz = 1

		select
			distinct
			IDPlanZajecPoz,
			gz.IDGrupaZajeciowa,
			gz.FormaWymiarID,
      pz.SemestrStudiow Sem,
			prz.Nazwa + coalesce(' - ' + nullif(fw.NazwaFormy,''),'') PrzedmiotNazwa,
			fz.Symbol6Znakow FormaZajec,
			sr.Numer as SRNumer,
			coalesce(bud.Symbol + ' ' + cast(sala.Numer as varchar(6)),'<bez sali>') Gdzie,
			dbo.FormatCzas(OdGodz) OdGodziny,
			dbo.FormatCzas(OdGodz + LGodz*pz.CzasZajec + CzasPrzerw) +
			case when zs.CoXtyg = 2 and CzyKalendarz = 0 then case when zs.NzX = 1 then +' (*)' else +' (**)' end else '' end as DoGodziny,
      odGodz as OdMinuty,
			case when zs.CzyKalendarz = 0 then dt.Nazwa else dbo.DatyZKalendarza(pzp.IDPlanZajecPoz) end Kiedy,
			case when zs.CzyKalendarz = 0 then 1+(6+(pzp.dzienTygodniaID-1)%7)%7 else
        ( select top 1 ID from @tabDaty where Daty = dbo.DatyZKalendarza(pzp.IDPlanZajecPoz) ) end DzienID,

			prz.Kod + ' - ' +ku.koduzupelniajacy Kod,

			case when ( select liczbaKier from @table where id = pzp.idplanzajecpoz ) = 1 then
			(
				select '<'+rtrim(w.Symbol)+'> '+k.Symbol from Kierunek   k
        join Wydzial w on WydzialID = IDWydzial
				where IDkierunek = (select kier from @table where id = pzp.idplanzajecpoz )

			)
			else
			 '' end KierSymbol,

			case when ( select LiczbaSpec from @table where id = pzp.idplanzajecpoz	) = 1 then
			(
				select '<' +ss.Symbol + '> ' +spec.Nazwa from Specjalnosc spec

				join SystemStudiow SS on SystemStudiowID = IDSystemStudiow
				where IDSpecjalnosc = (select spec from @table where id = pzp.idplanzajecpoz )
			)
			else '' end SpecNazwa,

			tryb.symbol as TrybSymbol,
	    dbo.GrupyStudenckieStr(pzp.GrupaZajeciowaID) Grupy,
      Zjazd.Nazwa as ZjazdNazwa,
      Sekcja.Nazwa as SekcjaNazwa,
      Sekcja.Symbol as SekcjaSymbol,
	  fz.Kolor as FormaKolor
      
		from PlanZajecPoz pzp
 	  join GrupaZajeciowa gz on pzp.GrupaZajeciowaID = IDgrupaZajeciowa
	  join ZajeciaStatus zs on ZajeciaStatusid = idZajeciaStatus

		-- do grupy
		left join PlanPozGrupa ppg on ppg.GrupaZajeciowaId = gz.IdGrupazajeciowa

		-- rodzaj studi�w
	  join PlanZajec pz on pz.IDplanZajec = gz.PlanZajecID or
		pz.IDplanZajec in (select PlanZajecID from ZajeciaWspolnePoz where ZajeciaWspolneID = gz.ZajeciaWspolneID)
		join siatka on siatkaID = IDSiatka
		join program pr on siatka.programid = idprogram
 	  join TrybStudiow tryb on siatka.TrybstudiowID = IDtrybStudiow
    join Zjazd on pz.ZjazdID = IDZjazd
    join Sekcja on SekcjaID = IDSekcja

		-- do przedmiotu
		left join FormaWymiar fw on FormaWymiarID = IDformaWymiar
		left join SemestrRealizacji sr on IDSemestrRealizacji = fw.SemestrRealizacjiID
		left join KartaUzup ku on sr.KartaUzupID = IDkartaUzup
		left join Przedmiot prz on ku.PrzedmiotID = IDprzedmiot
		left join FormaZajec fz on fw.FormaZajecID = IDFormaZajec

		-- do sali
		left join Sala on pzp.SalaID = IDsala
		left join Budynek bud on budynekid = idbudynek

		-- dzienTygodnia
		left join DzienTygodnia dt on pzp.DzienTygodniaID = IDDzienTygodnia

		where pzp.idPlanZajecPoz in (select ID from @Table)
		 and pz.StatusAktywnyDydaktyk = 1

    order by DzienID, odGodz


GO


-------------------------------------------------------------------------------wdro12---------------------------------------------------------------


IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_USR_UprawnianiaToRola_USR_UprawnianiaDefinicje]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_UprawnianiaToRola]'))
ALTER TABLE [dbo].[USR_UprawnianiaToRola] DROP CONSTRAINT [FK_USR_UprawnianiaToRola_USR_UprawnianiaDefinicje]

--------------------------------------
----------------USR_UprawnianiaDefinicje---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[USR_UprawnianiaDefinicje]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[USR_UprawnianiaDefinicje]
GO

CREATE TABLE [dbo].[USR_UprawnianiaDefinicje] (
   [IDUSR_UprawnianiaDefinicje] [int]  IDENTITY(1,1) NOT NULL
  ,[Adres] [varchar] (250) NOT NULL
  ,[Opis] [text]  NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO



ALTER TABLE [dbo].[USR_UprawnianiaDefinicje] ADD  CONSTRAINT [PK_USR_UprawnianiaDefinicje] PRIMARY KEY CLUSTERED( IDUSR_UprawnianiaDefinicje ) ON [PRIMARY]


go


-----------------------------------------------------------------------wdro13-------------------------------------------------------------------------


IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_USR_UprawnianiaToRola_USR_Rola]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_UprawnianiaToRola]'))
ALTER TABLE [dbo].[USR_UprawnianiaToRola] DROP CONSTRAINT [FK_USR_UprawnianiaToRola_USR_Rola]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_USR_UprawnianiaToRola_USR_UprawnianiaDefinicje]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_UprawnianiaToRola]'))
ALTER TABLE [dbo].[USR_UprawnianiaToRola] DROP CONSTRAINT [FK_USR_UprawnianiaToRola_USR_UprawnianiaDefinicje]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[USR_UprawnianiaToRola]') AND name = N'PK_USR_UprawnianiaToRola')
ALTER TABLE [dbo].[USR_UprawnianiaToRola] DROP CONSTRAINT [PK_USR_UprawnianiaToRola]




--------------------------------------
----------------USR_UprawnianiaToRola---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[USR_UprawnianiaToRola]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[USR_UprawnianiaToRola]
GO

CREATE TABLE [dbo].[USR_UprawnianiaToRola] (
   [IDUSR_UprawnianiaToRola] [int]  IDENTITY(1,1) NOT NULL
  ,[USR_RolaID] [int]  NOT NULL
  ,[USR_UprawnieniaDefinicjeID] [int]  NOT NULL
  ,[CzyMozeWykonac] [bit]  NOT NULL DEFAULT ((0))
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO




ALTER TABLE [dbo].[USR_UprawnianiaToRola] ADD  CONSTRAINT [PK_USR_UprawnianiaToRola] PRIMARY KEY CLUSTERED( IDUSR_UprawnianiaToRola ) ON [PRIMARY]

ALTER TABLE [dbo].[USR_UprawnianiaToRola] WITH NOCHECK ADD CONSTRAINT [FK_USR_UprawnianiaToRola_USR_UprawnianiaDefinicje] FOREIGN KEY([USR_UprawnieniaDefinicjeID]) REFERENCES [dbo].[USR_UprawnianiaDefinicje] ([IDUSR_UprawnianiaDefinicje])

ALTER TABLE [dbo].[USR_UprawnianiaToRola] WITH NOCHECK ADD CONSTRAINT [FK_USR_UprawnianiaToRola_USR_Rola] FOREIGN KEY([USR_RolaID]) REFERENCES [dbo].[USR_Rola] ([IDUSR_Rola])


go


--------------------------------------------------------------wdro14----------------------------------------------------------------

--------------------------------------
----------------USR_Rola---------------
-- Nale�y zweryfikowa� constrainty--
--------------------------------------
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__USR_Profi__USR_R__6C1180E9]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_ProfilToRola]'))
ALTER TABLE [dbo].[USR_ProfilToRola] DROP CONSTRAINT [FK__USR_Profi__USR_R__6C1180E9]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_USR_UprawnianiaToRola_USR_Rola]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_UprawnianiaToRola]'))
ALTER TABLE [dbo].[USR_UprawnianiaToRola] DROP CONSTRAINT [FK_USR_UprawnianiaToRola_USR_Rola]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_USR_Uprawnienia_USR_Rola]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_Uprawnienia]'))
ALTER TABLE [dbo].[USR_Uprawnienia] DROP CONSTRAINT [FK_USR_Uprawnienia_USR_Rola]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[USR_Rola]') AND name = N'PK_USR_Rola')
ALTER TABLE [dbo].[USR_Rola] DROP CONSTRAINT [PK_USR_Rola]


if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[USR_Rola]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[USR_Rola] (
   [IDUSR_Rola] [int]  IDENTITY(1,1) NOT NULL
  ,[Nazwa] [varchar] (128) NOT NULL
  ,[Podmiot] [varchar] (128) NULL
  ,[Klucz] [varchar] (128) NULL
  ,[CzyPolitykaHasel] [bit]  NOT NULL DEFAULT 0
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[USR_Rola] WITH NOCHECK ADD 
CONSTRAINT [PK_USR_Rola] PRIMARY KEY  CLUSTERED
(
  IDUSR_Rola
) ON [PRIMARY]


End
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_USR_Rola] (
   [IDUSR_Rola] [int]  IDENTITY(1,1) NOT NULL
  ,[Nazwa] [varchar] (128) NOT NULL
  ,[Podmiot] [varchar] (128) NULL
  ,[Klucz] [varchar] (128) NULL
  ,[CzyPolitykaHasel] [bit]  NOT NULL DEFAULT 0
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[Tmp_USR_Rola] WITH NOCHECK ADD 
CONSTRAINT [PK_USR_Rola] PRIMARY KEY  CLUSTERED
(
  IDUSR_Rola
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_USR_Rola ON

IF EXISTS (SELECT * FROM dbo.USR_Rola)
EXEC('INSERT INTO dbo.Tmp_USR_Rola (IDUSR_Rola, Nazwa, Podmiot, Klucz, Dodal, DataDodania, Zmodyfikowal, DataModyfikacji)
  SELECT IDUSR_Rola, Nazwa, Podmiot, Klucz, Dodal, DataDodania, Zmodyfikowal, DataModyfikacji FROM dbo.USR_Rola WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_USR_Rola OFF

DROP TABLE dbo.USR_Rola

EXECUTE sp_rename N'dbo.Tmp_USR_Rola', N'USR_Rola', 'OBJECT'


END
ALTER TABLE [dbo].[USR_UprawnianiaToRola] WITH NOCHECK ADD CONSTRAINT [FK_USR_UprawnianiaToRola_USR_Rola] FOREIGN KEY([USR_RolaID]) REFERENCES [dbo].[USR_Rola] ([IDUSR_Rola])

ALTER TABLE [dbo].[USR_ProfilToRola] WITH NOCHECK ADD CONSTRAINT [FK__USR_Profi__USR_R__6C1180E9] FOREIGN KEY([USR_RolaID]) REFERENCES [dbo].[USR_Rola] ([IDUSR_Rola])






