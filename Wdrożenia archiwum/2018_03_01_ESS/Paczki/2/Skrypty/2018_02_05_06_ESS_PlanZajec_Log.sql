--------------------------------------
----------------PlanZajec---------------
--------------------------------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_PlanZajec]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_PlanZajec] (
   [Log_PlanZajecID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDPlanZajec] [int] 
  ,[Org_SiatkaID] [int] 
  ,[Org_SekcjaID] [int] 
  ,[Org_Opis] [varchar] (100)
  ,[Org_RokAkad] [smallint] 
  ,[Org_SemestrStudiow] [tinyint] 
  ,[Org_ZjazdID] [int] 
  ,[Org_SemestrID] [int] 
  ,[Org_PrzerwaPoXGodz] [tinyint] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Dodal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_Dokladnosc] [int] 
  ,[Org_GodzOd] [int] 
  ,[Org_GodzDo] [int] 
  ,[Org_CzasPrzerwy] [int] 
  ,[Org_CzasZajec] [int] 
  ,[Org_StatusAktywny] [bit] 
  ,[Org_StatusAktywnyDydaktyk] [bit] 
  ,[Org_EdycjaSesjaID] [int] 
) ON [PRIMARY]
ALTER TABLE [dbo].[LOG_PlanZajec] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_PlanZajec] PRIMARY KEY  CLUSTERED
(
   [Log_PlanZajecID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_PlanZajec] (
   [Log_PlanZajecID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDPlanZajec] [int] 
  ,[Org_SiatkaID] [int] 
  ,[Org_SekcjaID] [int] 
  ,[Org_Opis] [varchar] (100)
  ,[Org_RokAkad] [smallint] 
  ,[Org_SemestrStudiow] [tinyint] 
  ,[Org_ZjazdID] [int] 
  ,[Org_SemestrID] [int] 
  ,[Org_PrzerwaPoXGodz] [tinyint] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Dodal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_Dokladnosc] [int] 
  ,[Org_GodzOd] [int] 
  ,[Org_GodzDo] [int] 
  ,[Org_CzasPrzerwy] [int] 
  ,[Org_CzasZajec] [int] 
  ,[Org_StatusAktywny] [bit] 
  ,[Org_StatusAktywnyDydaktyk] [bit] 
  ,[Org_EdycjaSesjaID] [int] 
) ON [PRIMARY]
SET IDENTITY_INSERT dbo.Tmp_LOG_PlanZajec ON

IF EXISTS (SELECT * FROM dbo.LOG_PlanZajec)
EXEC('INSERT INTO dbo.Tmp_LOG_PlanZajec (Log_PlanZajecID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDPlanZajec, Org_SiatkaID, Org_SekcjaID, Org_Opis, Org_RokAkad, Org_SemestrStudiow, Org_ZjazdID, Org_SemestrID, Org_PrzerwaPoXGodz, Org_DataDodania, Org_Dodal, Org_DataModyfikacji, Org_Zmodyfikowal, Org_Dokladnosc, Org_GodzOd, Org_GodzDo, Org_CzasPrzerwy, Org_CzasZajec, Org_StatusAktywny, Org_EdycjaSesjaID)
  SELECT Log_PlanZajecID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDPlanZajec, Org_SiatkaID, Org_SekcjaID, Org_Opis, Org_RokAkad, Org_SemestrStudiow, Org_ZjazdID, Org_SemestrID, Org_PrzerwaPoXGodz, Org_DataDodania, Org_Dodal, Org_DataModyfikacji, Org_Zmodyfikowal, Org_Dokladnosc, Org_GodzOd, Org_GodzDo, Org_CzasPrzerwy, Org_CzasZajec, Org_StatusAktywny, Org_EdycjaSesjaID FROM dbo.Log_PlanZajec WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_PlanZajec OFF

DROP TABLE dbo.LOG_PlanZajec

EXECUTE sp_rename N'dbo.Tmp_LOG_PlanZajec', N'LOG_PlanZajec', 'OBJECT'

ALTER TABLE [dbo].[LOG_PlanZajec] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_PlanZajec] PRIMARY KEY  CLUSTERED
(
   [Log_PlanZajecID]
) ON [PRIMARY]

END
GO


