--------------------------------------
----------------PlanZajec---------------
-- Nale�y zweryfikowa� constrainty--
--------------------------------------
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GrupaZajeciowa_PlanZajec]') AND parent_object_id = OBJECT_ID(N'[dbo].[GrupaZajeciowa]'))
ALTER TABLE [dbo].[GrupaZajeciowa] DROP CONSTRAINT [FK_GrupaZajeciowa_PlanZajec]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ZajeciaWspolnePoz_PlanZajec]') AND parent_object_id = OBJECT_ID(N'[dbo].[ZajeciaWspolnePoz]'))
ALTER TABLE [dbo].[ZajeciaWspolnePoz] DROP CONSTRAINT [FK_ZajeciaWspolnePoz_PlanZajec]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[PlanZajec]') AND name = N'PK_PlanZajec')
ALTER TABLE [dbo].[PlanZajec] DROP CONSTRAINT [PK_PlanZajec]


if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[PlanZajec]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[PlanZajec] (
   [IDPlanZajec] [int]  IDENTITY(1,1) NOT NULL
  ,[SiatkaID] [int]  NOT NULL
  ,[SekcjaID] [int]  NOT NULL
  ,[Opis] [varchar] (100) NULL
  ,[RokAkad] [smallint]  NULL
  ,[SemestrStudiow] [tinyint]  NULL
  ,[ZjazdID] [int]  NULL
  ,[SemestrID] [int]  NULL
  ,[PrzerwaPoXGodz] [tinyint]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataModyfikacji] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NOT NULL
  ,[Dokladnosc] [int]  NOT NULL
  ,[GodzOd] [int]  NOT NULL
  ,[GodzDo] [int]  NOT NULL
  ,[CzasPrzerwy] [int]  NOT NULL
  ,[CzasZajec] [int]  NOT NULL
  ,[StatusAktywny] [bit]  NOT NULL DEFAULT (0)
  ,[StatusAktywnyDydaktyk] [bit]  NOT NULL DEFAULT 0
  ,[Przypisy] [text]  NULL
  ,[EdycjaSesjaID] [int]  NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[PlanZajec] WITH NOCHECK ADD 
CONSTRAINT [PK_PlanZajec] PRIMARY KEY  CLUSTERED
(
  IDPlanZajec
) ON [PRIMARY]


End
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_PlanZajec] (
   [IDPlanZajec] [int]  IDENTITY(1,1) NOT NULL
  ,[SiatkaID] [int]  NOT NULL
  ,[SekcjaID] [int]  NOT NULL
  ,[Opis] [varchar] (100) NULL
  ,[RokAkad] [smallint]  NULL
  ,[SemestrStudiow] [tinyint]  NULL
  ,[ZjazdID] [int]  NULL
  ,[SemestrID] [int]  NULL
  ,[PrzerwaPoXGodz] [tinyint]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataModyfikacji] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NOT NULL
  ,[Dokladnosc] [int]  NOT NULL
  ,[GodzOd] [int]  NOT NULL
  ,[GodzDo] [int]  NOT NULL
  ,[CzasPrzerwy] [int]  NOT NULL
  ,[CzasZajec] [int]  NOT NULL
  ,[StatusAktywny] [bit]  NOT NULL DEFAULT (0)
  ,[StatusAktywnyDydaktyk] [bit]  NOT NULL DEFAULT 0
  ,[Przypisy] [text]  NULL
  ,[EdycjaSesjaID] [int]  NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[Tmp_PlanZajec] WITH NOCHECK ADD 
CONSTRAINT [PK_PlanZajec] PRIMARY KEY  CLUSTERED
(
  IDPlanZajec
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_PlanZajec ON

IF EXISTS (SELECT * FROM dbo.PlanZajec)
EXEC('INSERT INTO dbo.Tmp_PlanZajec (IDPlanZajec, SiatkaID, SekcjaID, Opis, RokAkad, SemestrStudiow, ZjazdID, SemestrID, PrzerwaPoXGodz, DataDodania, Dodal, DataModyfikacji, Zmodyfikowal, Dokladnosc, GodzOd, GodzDo, CzasPrzerwy, CzasZajec, StatusAktywny, Przypisy, EdycjaSesjaID)
  SELECT IDPlanZajec, SiatkaID, SekcjaID, Opis, RokAkad, SemestrStudiow, ZjazdID, SemestrID, PrzerwaPoXGodz, DataDodania, Dodal, DataModyfikacji, Zmodyfikowal, Dokladnosc, GodzOd, GodzDo, CzasPrzerwy, CzasZajec, StatusAktywny, Przypisy, EdycjaSesjaID FROM dbo.PlanZajec WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_PlanZajec OFF

DROP TABLE dbo.PlanZajec

EXECUTE sp_rename N'dbo.Tmp_PlanZajec', N'PlanZajec', 'OBJECT'


END
ALTER TABLE [dbo].[ZajeciaWspolnePoz] WITH NOCHECK ADD CONSTRAINT [FK_ZajeciaWspolnePoz_PlanZajec] FOREIGN KEY([PlanZajecID]) REFERENCES [dbo].[PlanZajec] ([IDPlanZajec])

ALTER TABLE [dbo].[PlanZajec] WITH NOCHECK ADD CONSTRAINT [FK_PlanZajec_Zjazd] FOREIGN KEY([ZjazdID]) REFERENCES [dbo].[Zjazd] ([IDZjazd])

ALTER TABLE [dbo].[PlanZajec] WITH NOCHECK ADD CONSTRAINT [FK_PlanZajec_Siatka] FOREIGN KEY([SiatkaID]) REFERENCES [dbo].[Siatka] ([IDSiatka])

ALTER TABLE [dbo].[PlanZajec] WITH NOCHECK ADD CONSTRAINT [FK_PlanZajec_LOG_SesjeUzytkownika] FOREIGN KEY([EdycjaSesjaID]) REFERENCES [dbo].[LOG_SesjeUzytkownika] ([IDSesja])

ALTER TABLE [dbo].[GrupaZajeciowa] WITH NOCHECK ADD CONSTRAINT [FK_GrupaZajeciowa_PlanZajec] FOREIGN KEY([PlanZajecID]) REFERENCES [dbo].[PlanZajec] ([IDPlanZajec])


