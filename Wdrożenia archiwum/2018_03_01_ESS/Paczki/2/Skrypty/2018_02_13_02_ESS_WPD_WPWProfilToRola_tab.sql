--------------------------------------
----------------USR_ProfilToRola---------------
-- Należy zweryfikować constrainty--
--------------------------------------
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[USR_ProfilToRola]') AND name = N'PK_USR_ProfilToRola')
ALTER TABLE [dbo].[USR_ProfilToRola] DROP CONSTRAINT [PK_USR_ProfilToRola]


if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[USR_ProfilToRola]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[USR_ProfilToRola] (
   [IDUSR_ProfilToRola] [int]  IDENTITY(1000000,1) NOT NULL
  ,[USR_ProfilID] [int]  NOT NULL
  ,[USR_RolaID] [int]  NOT NULL
  ,[KluczZdalny] [int]  NULL
  ,[Aktywny] [bit]  NOT NULL DEFAULT ((1))
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[USR_ProfilToRola] WITH NOCHECK ADD 
CONSTRAINT [PK_USR_ProfilToRola] PRIMARY KEY  CLUSTERED
(
  IDUSR_ProfilToRola
) ON [PRIMARY]


End
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_USR_ProfilToRola] (
   [IDUSR_ProfilToRola] [int]  IDENTITY(1000000,1) NOT NULL
  ,[USR_ProfilID] [int]  NOT NULL
  ,[USR_RolaID] [int]  NOT NULL
  ,[KluczZdalny] [int]  NULL
  ,[Aktywny] [bit]  NOT NULL DEFAULT ((1))
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[Tmp_USR_ProfilToRola] WITH NOCHECK ADD 
CONSTRAINT [PK_USR_ProfilToRola] PRIMARY KEY  CLUSTERED
(
  IDUSR_ProfilToRola
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_USR_ProfilToRola ON

IF EXISTS (SELECT * FROM dbo.USR_ProfilToRola)
EXEC('INSERT INTO dbo.Tmp_USR_ProfilToRola (IDUSR_ProfilToRola, USR_ProfilID, USR_RolaID, KluczZdalny, Aktywny, Dodal, DataDodania, Zmodyfikowal, DataModyfikacji)
  SELECT IDUSR_ProfilToRola, USR_ProfilID, USR_RolaID, KluczZdalny, Aktywny, Dodal, DataDodania, Zmodyfikowal, DataModyfikacji FROM dbo.USR_ProfilToRola WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_USR_ProfilToRola OFF

DROP TABLE dbo.USR_ProfilToRola

EXECUTE sp_rename N'dbo.Tmp_USR_ProfilToRola', N'USR_ProfilToRola', 'OBJECT'


END
ALTER TABLE [dbo].[USR_ProfilToRola] WITH NOCHECK ADD CONSTRAINT [FK__USR_Profi__USR_R__6C1180E9] FOREIGN KEY([USR_RolaID]) REFERENCES [dbo].[USR_Rola] ([IDUSR_Rola])

ALTER TABLE [dbo].[USR_ProfilToRola] WITH NOCHECK ADD CONSTRAINT [FK__USR_Profi__USR_P__6B1D5CB0] FOREIGN KEY([USR_ProfilID]) REFERENCES [dbo].[USR_Profil] ([IDUSR_Profil])


