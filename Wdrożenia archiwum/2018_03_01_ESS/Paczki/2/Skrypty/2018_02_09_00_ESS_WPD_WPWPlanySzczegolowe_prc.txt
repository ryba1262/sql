-- ******************* -- WPWPlanySzczegolowe -- ******************* --

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WPWPlanySzczegolowe]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WPWPlanySzczegolowe]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[WPWPlanySzczegolowe] @pracownikID INT, @DataOD DATE, @DataDO DATE, @SprawdzAktywnosc BIT
AS
select distinct
  [gz].[IDGrupaZajeciowa]
, DATEADD(minute, [ppd].[OdGodz], [ppd].[Data]) AS [DataOD]
, DATEADD(minute, [ppd].[DoGodz], [ppd].[Data]) AS [DataDO]
, [b].[Symbol] + [s].[Numer] AS [Sala]
, [b].[Adres] AS [Adres]
, CASE
	WHEN ([fw].[NazwaFormy] IS NOT NULL AND NOT [fw].[NazwaFormy] = '') THEN [p].[Nazwa] + ': ' + [fw].[NazwaFormy]
	ELSE [p].[Nazwa]
end as [PNazwa]
, [fz].[Symbol6Znakow] as [TypZajecSkrot]
, [fz].[Nazwa] as [TypZajec]
, [fz].[Kolor] as [FormaKolor]
from
					[PlanPozDef] [ppd]
JOIN				[PlanZajecPoz] [pzp]		on [ppd].[PlanZajecPozIDNr] = [pzp].[IDPlanZajecPoz]
LEFT JOIN			[GrupaZajeciowa] [gz]		on [pzp].[GrupaZajeciowaID] = [gz].[IDGrupaZajeciowa]
JOIN				[FormaWymiar] [fw]			on [gz].[FormaWymiarID] = [fw].[IDFormaWymiar]
JOIN				[SemestrRealizacji] [sr]	on [sr].[IDSemestrRealizacji] = [FW].[SemestrRealizacjiID]
JOIN				[KartaUzup] [ku]			on [ku].[IDKartaUzup] = [sr].[KartaUzupID]
JOIN				[Przedmiot] [p]				on [p].[IDPrzedmiot] = [ku].[PrzedmiotID]
LEFT OUTER JOIN		[PlanZajec] [pz]			on [gz].[PlanZajecID] = [pz].[IDPlanZajec]
LEFT JOIN			[Sala] [s]					on [s].[IDSala] = [ppd].[SalaIDNr]
LEFT JOIN			[Budynek] [b]				on [b].[IDBudynek] = [s].[BudynekID]
LEFT JOIN			[PakietZaj] [pakiet]		on [ppd].[PakietIDNr] = [pakiet].[IDPakietZaj]
LEFT JOIN			[FormaZajec] [fz]			on [fz].[IDFormaZajec] = [fw].[FormaZajecID]
WHERE
[ppd].[PracownikIDNr] = @pracownikID
and pz.IDPlanZajec is not null
and  (@SprawdzAktywnosc = 0 OR ([pz].[StatusAktywnyDydaktyk] = 1 OR [pz].[StatusAktywnyDydaktyk] is null))
and [ppd].Data >= @DataOD
and ppd.Data <= @DataDO 
group by [gz].[IDGrupaZajeciowa], [P].[Nazwa], [ppd].[Data], [ppd].[OdGodz], [ppd].[DoGodz], [FW].[NazwaFormy], [B].[Symbol], [S].[Numer], [B].[Adres], [fz].[Symbol6Znakow], [fz].[Nazwa], [fz].[Kolor]

UNION

select distinct
  [gz].[IDGrupaZajeciowa]
, DATEADD(minute, [ppd].[OdGodz], [ppd].[Data]) AS [DataOD]
, DATEADD(minute, [ppd].[DoGodz], [ppd].[Data]) AS [DataDO]
, [b].[Symbol] + [s].[Numer] AS [Sala]
, [b].[Adres] AS [Adres]
, CASE
	WHEN ([fw].[NazwaFormy] IS NOT NULL AND NOT [fw].[NazwaFormy] = '') THEN [p].[Nazwa] + ': ' + [fw].[NazwaFormy]
	ELSE [p].[Nazwa]
end as [PNazwa]
, [fz].[Symbol6Znakow] as [TypZajecSkrot]
, [fz].[Nazwa] as [TypZajec]
, [fz].[Kolor] as [FormaKolor]
from
				[PlanPozDef] [ppd]
JOIN			[PlanZajecPoz] [pzp]			on [ppd].[PlanZajecPozIDNr] = [pzp].[IDPlanZajecPoz]
LEFT JOIN		[GrupaZajeciowa] [gz]			on [pzp].[GrupaZajeciowaID] = [gz].[IDGrupaZajeciowa]
JOIN			[FormaWymiar] [fw]				on [gz].[FormaWymiarID] = [fw].[IDFormaWymiar]
JOIN			[SemestrRealizacji] [sr]		on [sr].[IDSemestrRealizacji] = [FW].[SemestrRealizacjiID]
JOIN			[KartaUzup] [ku]				on [ku].[IDKartaUzup] = [sr].[KartaUzupID]
JOIN			[Przedmiot] [p]					on [p].[IDPrzedmiot] = [ku].[PrzedmiotID]
FULL OUTER JOIN	[ZajeciaWspolne] [zw]			on [gz].IDGrupaZajeciowa = [zw].[IDZajeciaWspolne]
FULL OUTER JOIN	[ZajeciaWspolnePoz] [zwp]		on [zw].[IDZajeciaWspolne] = [zwp].[ZajeciaWspolneID]
FULL OUTER JOIN	[PlanZajec] [pz]				on [zwp].[PlanZajecID] = [pz].[IDPlanZajec]
LEFT JOIN		[Sala] [s]						on [s].[IDSala] = [ppd].[SalaIDNr]
LEFT JOIN		[Budynek] [b]					on [b].[IDBudynek] = [s].[BudynekID]
LEFT JOIN		[PakietZaj] [pakiet]			on [ppd].[PakietIDNr] = [pakiet].[IDPakietZaj]
LEFT JOIN		[FormaZajec] [fz]				on [fz].[IDFormaZajec] = [fw].[FormaZajecID]
LEFT JOIN		[PlanPozPracownik] [ppp]		on [pzp].[IDplanZajecPoz] = [ppp].[planZajecpozid]
where
[ppd].[PracownikIDNr] = @pracownikID
and [pz].[IDPlanZajec] is not null
and  (@SprawdzAktywnosc = 0 OR ([pz].[StatusAktywnyDydaktyk] = 1 OR [pz].[StatusAktywnyDydaktyk] is null))
and [ppd].Data >= @DataOD
and ppd.Data <= @DataDO 
group by [gz].[IDGrupaZajeciowa], [P].[Nazwa], [ppd].[Data], [ppd].[OdGodz], [ppd].[DoGodz], [FW].[NazwaFormy], [B].[Symbol], [S].[Numer], [B].[Adres], [fz].[Symbol6Znakow], [fz].[Nazwa], [fz].[Kolor]

order by [DataOd] desc

GO


