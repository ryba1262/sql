delete from DWOPole where IDDWOPole between 84000 and 84010
delete from DWOWariant where IDDWOWariant between 84000 and 84010
delete from DWOJoin where IDDWOJoin between 84000 and 84012
delete from DWOWystepowanie where IDDWOWystepowanie = 84000


insert into DWOWystepowanie (IDDWOWystepowanie,MiejsceWystepowania,RegKeyName,MaxLiczbaKolumn,LP,CzySprawdzacUprawnienia,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84000,'Kursy','Kursy',30,1,0,452,getdate(),NULL,NULL)


insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84001,84000,'Kurs','from Kurs',NULL,0,452,getdate(),NULL,NULL) 

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84002,84000,'Sekcja','left join Sekcja on Kurs.SekcjaID=IDSekcja',84001,1,452,getdate(),NULL,NULL) 

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84003,84000,'Siatka','left join Siatka on Kurs.SiatkaID = IDSiatka',84001,1,452,getdate(),NULL,NULL) 

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84004,84000,'TrybStudiow','left join TrybStudiow on Siatka.TrybStudiowID = IDTrybStudiow',84003,2,452,getdate(),NULL,NULL) 

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84005,84000,'Program','left join Program on Siatka.ProgramID = IDProgram',84003,2,452,getdate(),NULL,NULL) 
  
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84006,84000,'Kierunek','left join Kierunek on Program.KierunekID = IDKierunek',84005,3,452,getdate(),NULL,NULL) 
 
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84007,84000,'SystemStudiow','left join SystemStudiow on Program.SystemStudiowID = IDSystemStudiow',84005,3,452,getdate(),NULL,NULL) 
 
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84009,84000,'Wydzial','left join Wydzial on Kierunek.WydzialID = IDWydzial',84006,4,452,getdate(),NULL,NULL)  
 
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84010,84000,'SemestrAkad','left join OkresRozliczeniowyPoz SemestrAkad on Kurs.SemestrID = SemestrAkad.IDOkresRozliczeniowyPoz',84001,1,452,getdate(),NULL,NULL)  

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84011,84000,'KursPoz','join KursPoz on KursPoz.KursID = Kurs.IDKurs',84001,1,452,getdate(),NULL,NULL)  
 

INSERT INTO DWOWariant(IDDWOWariant,DWOJoinID,Nazwa,Opis,Warunek,CzyPelnyWarunek,CzySprawdzacUprawnienia,LP,Wymagany,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
  values(84001,84007,'Bez podyplomowych','Bez podyplomowych','SystemStudiow.CzyPodyplomowe = 0 ',1,0,1,0,452,GetDate(),NULL,NULL)

insert into DWOWariant (IDDWOWariant,DWOJoinID,Nazwa,Opis,Warunek,CzyPelnyWarunek,CzySprawdzacUprawnienia,LP,Wymagany,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84002,84001,'Wszystkie kursy','Wszystkie kursy','1 = 1',1,0,2,0,452,getdate(),NULL,NULL) 
 
INSERT INTO DWOWariant(IDDWOWariant,DWOJoinID,Nazwa,Opis,Warunek,CzyPelnyWarunek,CzySprawdzacUprawnienia,LP,Wymagany,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
  values(84003,84007,'Podyplomowe','Podyplomowe','SystemStudiow.CzyPodyplomowe = 1 ',1,0,3,0,452,GetDate(),NULL,NULL)


insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84001,84001,'IDKurs','IDKURS','Kurs.IDKurs',NULL,'Kurs.IDKurs',NULL,1,0,1,1,1,NULL,NULL,452,getdate(),NULL,NULL) 

insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84002,84001,'Rok akad',NULL,'cast(Kurs.RokAkad as varchar(4)) + ''/'' + cast((Kurs.RokAkad+1) as varchar(4))',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL) 
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84003,84010,'Semestr','SEMESTRID','SemestrAkad.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)  

insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84004,84002,'Sekcja','SEKCJA','Sekcja.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL) 

insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84005,84004,'Tryb studiów','TRYBSTUDIOW','TrybStudiow.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84006,84007,'System studiów','SYSTEMSTUDIOW','SystemStudiow.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84007,84009,'Kierunek','KIERUNEK','''<'' + Wydzial.Symbol  + ''> '' + Kierunek.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)     
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84008,84011,'Semestr studiów','SEMESTRSTUDIOW','KursPoz.Semestr',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)       
 
  