--------------------------------------
----------------USR_Rola---------------
-- Należy zweryfikować constrainty--
--------------------------------------
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__USR_Profi__USR_R__6C1180E9]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_ProfilToRola]'))
ALTER TABLE [dbo].[USR_ProfilToRola] DROP CONSTRAINT [FK__USR_Profi__USR_R__6C1180E9]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_USR_UprawnianiaToRola_USR_Rola]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_UprawnianiaToRola]'))
ALTER TABLE [dbo].[USR_UprawnianiaToRola] DROP CONSTRAINT [FK_USR_UprawnianiaToRola_USR_Rola]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_USR_Uprawnienia_USR_Rola]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_Uprawnienia]'))
ALTER TABLE [dbo].[USR_Uprawnienia] DROP CONSTRAINT [FK_USR_Uprawnienia_USR_Rola]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[USR_Rola]') AND name = N'PK_USR_Rola')
ALTER TABLE [dbo].[USR_Rola] DROP CONSTRAINT [PK_USR_Rola]


if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[USR_Rola]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[USR_Rola] (
   [IDUSR_Rola] [int]  IDENTITY(1,1) NOT NULL
  ,[Nazwa] [varchar] (128) NOT NULL
  ,[Podmiot] [varchar] (128) NULL
  ,[Klucz] [varchar] (128) NULL
  ,[CzyPolitykaHasel] [bit]  NOT NULL DEFAULT 0
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[USR_Rola] WITH NOCHECK ADD 
CONSTRAINT [PK_USR_Rola] PRIMARY KEY  CLUSTERED
(
  IDUSR_Rola
) ON [PRIMARY]


End
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_USR_Rola] (
   [IDUSR_Rola] [int]  IDENTITY(1,1) NOT NULL
  ,[Nazwa] [varchar] (128) NOT NULL
  ,[Podmiot] [varchar] (128) NULL
  ,[Klucz] [varchar] (128) NULL
  ,[CzyPolitykaHasel] [bit]  NOT NULL DEFAULT 0
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[Tmp_USR_Rola] WITH NOCHECK ADD 
CONSTRAINT [PK_USR_Rola] PRIMARY KEY  CLUSTERED
(
  IDUSR_Rola
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_USR_Rola ON

IF EXISTS (SELECT * FROM dbo.USR_Rola)
EXEC('INSERT INTO dbo.Tmp_USR_Rola (IDUSR_Rola, Nazwa, Podmiot, Klucz, Dodal, DataDodania, Zmodyfikowal, DataModyfikacji)
  SELECT IDUSR_Rola, Nazwa, Podmiot, Klucz, Dodal, DataDodania, Zmodyfikowal, DataModyfikacji FROM dbo.USR_Rola WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_USR_Rola OFF

DROP TABLE dbo.USR_Rola

EXECUTE sp_rename N'dbo.Tmp_USR_Rola', N'USR_Rola', 'OBJECT'


END
ALTER TABLE [dbo].[USR_UprawnianiaToRola] WITH NOCHECK ADD CONSTRAINT [FK_USR_UprawnianiaToRola_USR_Rola] FOREIGN KEY([USR_RolaID]) REFERENCES [dbo].[USR_Rola] ([IDUSR_Rola])

ALTER TABLE [dbo].[USR_ProfilToRola] WITH NOCHECK ADD CONSTRAINT [FK__USR_Profi__USR_R__6C1180E9] FOREIGN KEY([USR_RolaID]) REFERENCES [dbo].[USR_Rola] ([IDUSR_Rola])


