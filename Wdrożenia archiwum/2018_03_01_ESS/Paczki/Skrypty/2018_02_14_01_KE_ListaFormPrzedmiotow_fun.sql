

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KE_ListaFormPrzedmiotu]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[KE_ListaFormPrzedmiotu]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[KE_ListaFormPrzedmiotu] (@IndeksID int, @RokAkad int, @SemestrID int, @CzyPoprawkowy bit)  
RETURNS @Output table (SemestrRealizacjiID int, IDFormaWymiar int, NazwaFormy varchar(500), Przedmiot varchar(500), FormaZajec varchar(10))
BEGIN

	
 declare @WyzerowanieRokAkad int, @WyzerowanieSemAkad int
  select @WyzerowanieRokAkad=RokAkademicki,
         @WyzerowanieSemAkad=SemestrAkadID
  from HistoriaStudenta
  where IndeksID=@IndeksID and ZdarzenieID=32 and Anulowany=0
  order by RokAkademicki desc,  SemestrAkadID desc


  -- szukamy poprzedniej karty egz.
  declare @KartaEgzPoprzID int, @SemStudenta int
  select @SemStudenta=Semestr from Indeks where idindeks=@IndeksID


    declare @OldRok int, @oldSemLp int, @oldSem int
    select top 1 @KartaEgzPoprzID = IDKartaEgz, @OldRok=RokAkad, @OldSemLp=Lp, @OldSem=SemestrStudiow  from KartaEgz
    join OkresRozliczeniowyPoz on SemestrID=IDOkresRozliczeniowyPoz
    where IndeksID=@IndeksID and ((RokAkad*10+Lp < @RokAkad*10+@SemestrID) or
                                  (RokAkad*10+Lp <= @RokAkad*10+@SemestrID) and KartaPoczatkowa=1) 
          and RokAkad*10+Lp >= coalesce(@WyzerowanieRokAkad,0)*10+coalesce(@WyzerowanieSemAkad,0)
    order by RokAkad desc, Lp desc, KartaPoczatkowa asc

  
   insert into @Output
    select KartaEgzPoz.SemestrRealizacjiID, FormaWymiarID, NazwaFormy, Przedmiot.Nazwa as Przedmiot, FormaZajec.Symbol6Znakow
		from KartaEgzPoz
    join KartaEgzForma on KartaEgzPozID = IDKartaEgzPoz
    left join SkalaOcenPoz on OcenaID=IDSkalaOcenPoz
    left join FormaWymiar on FormaWymiarID = IDFormaWymiar
    left join SemestrRealizacji on KartaEgzPoz.SemestrRealizacjiID = IDSemestrRealizacji
    left join KartaUzup on KartaUzupID = IDKartaUzup
    left join Przedmiot on PrzedmiotID = IDPrzedmiot
    left join FormaZajec on FormaZajecID = IDFormaZajec
    where KartaEgzID=@KartaEgzPoprzID and coalesce(CzyZalicza,0) = 0 and @CzyPoprawkowy = 1 and CzyDrukowacNaKarcieEgz = 1
    UNION
    select krpp.SemestrRealizacjiID, IDFormaWymiar, NazwaFormy, Przedmiot.Nazwa as Przedmiot, FormaZajec.Symbol6Znakow from KartaRoznicProgramowych
    left join KartaRoznicProgramowychPoz krpp on KartaRoznicProgramowychID = IDKartaRoznicProgramowych
    left join SemestrRealizacji on SemestrRealizacjiID = IDSemestrRealizacji
    left join KartaUzup on KartaUzupID = IDKartaUzup
    left join Przedmiot on PrzedmiotID = IDPrzedmiot
    join FormaWymiar on FormaWymiar.SemestrRealizacjiID = IDSemestrRealizacji
    left join FormaZajec on FormaZajecID = IDFormaZajec
    where IndeksID = @IndeksID and @CzyPoprawkowy = 0 and CzyDrukowacNaKarcieEgz = 1

	return

END


GO


