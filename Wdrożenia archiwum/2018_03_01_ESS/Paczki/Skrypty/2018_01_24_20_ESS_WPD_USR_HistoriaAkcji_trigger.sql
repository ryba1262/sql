DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------USR_HistoriaAkcji-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_HistoriaAkcji_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER USR_HistoriaAkcji_INSERT
')

EXEC('
CREATE TRIGGER USR_HistoriaAkcji_INSERT ON ['+@BazaDanych+'].[dbo].[USR_HistoriaAkcji] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_HistoriaAkcji'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_HistoriaAkcji]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_HistoriaAkcji]
  ,[Org_Uri]
  ,[Org_USR_ProfilID]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDUSR_HistoriaAkcji
 ,ins.Uri
 ,ins.USR_ProfilID
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_HistoriaAkcji_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER USR_HistoriaAkcji_UPDATE
')

EXEC('
CREATE TRIGGER USR_HistoriaAkcji_UPDATE ON ['+@BazaDanych+'].[dbo].[USR_HistoriaAkcji] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_HistoriaAkcji'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_HistoriaAkcji]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_HistoriaAkcji]
  ,[Org_Uri]
  ,[Org_USR_ProfilID]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDUSR_HistoriaAkcji
 ,ins.Uri
 ,ins.USR_ProfilID
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_HistoriaAkcji_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER USR_HistoriaAkcji_DELETE
')

EXEC('
CREATE TRIGGER USR_HistoriaAkcji_DELETE ON ['+@BazaDanych+'].[dbo].[USR_HistoriaAkcji] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_HistoriaAkcji'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_HistoriaAkcji]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_HistoriaAkcji]
  ,[Org_Uri]
  ,[Org_USR_ProfilID]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDUSR_HistoriaAkcji
 ,del.Uri
 ,del.USR_ProfilID
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'USR_HistoriaAkcji')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('USR_HistoriaAkcji', 1, 0,GetDate())

GO