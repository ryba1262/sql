IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__USR_Histo__USR_P__6FE211CD]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_HistoriaAkcji]'))
ALTER TABLE [dbo].[USR_HistoriaAkcji] DROP CONSTRAINT [FK__USR_Histo__USR_P__6FE211CD]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[USR_HistoriaAkcji]') AND name = N'PK_USR_HistoriaAkcji')
ALTER TABLE [dbo].[USR_HistoriaAkcji] DROP CONSTRAINT [PK_USR_HistoriaAkcji]

--------------------------------------
----------------USR_HistoriaAkcji---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[USR_HistoriaAkcji]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[USR_HistoriaAkcji]
GO

CREATE TABLE [dbo].[USR_HistoriaAkcji] (
   [IDUSR_HistoriaAkcji] [int]  IDENTITY(1,1) NOT NULL
  ,[Uri] [varchar] (256) NOT NULL
  ,[DaneWejsciowe] [text]  NULL
  ,[DaneWyjsciowe] [text]  NULL
  ,[USR_ProfilID] [int]  NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[USR_HistoriaAkcji] ADD  CONSTRAINT [PK_USR_HistoriaAkcji] PRIMARY KEY CLUSTERED( IDUSR_HistoriaAkcji ) ON [PRIMARY]

ALTER TABLE [dbo].[USR_HistoriaAkcji] WITH NOCHECK ADD CONSTRAINT [FK__USR_Histo__USR_P__6FE211CD] FOREIGN KEY([USR_ProfilID]) REFERENCES [dbo].[USR_Profil] ([IDUSR_Profil])


GO