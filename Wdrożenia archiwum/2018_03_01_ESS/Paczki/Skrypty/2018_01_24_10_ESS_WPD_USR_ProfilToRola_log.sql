
-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------USR_ProfilToRola---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_USR_ProfilToRola]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_USR_ProfilToRola] (
   [Log_USR_ProfilToRolaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_ProfilToRola] [int] 
  ,[Org_USR_ProfilID] [int] 
  ,[Org_USR_RolaID] [int] 
  ,[Org_KluczZdalny] [int] 
  ,[Org_Aktywny] [bit] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_USR_ProfilToRola] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_USR_ProfilToRola] PRIMARY KEY  CLUSTERED
(
   [Log_USR_ProfilToRolaID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_USR_ProfilToRola] (
   [Log_USR_ProfilToRolaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_ProfilToRola] [int] 
  ,[Org_USR_ProfilID] [int] 
  ,[Org_USR_RolaID] [int] 
  ,[Org_KluczZdalny] [int] 
  ,[Org_Aktywny] [bit] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_ProfilToRola ON

IF EXISTS (SELECT * FROM dbo.LOG_USR_ProfilToRola)
EXEC('INSERT INTO dbo.Tmp_LOG_USR_ProfilToRola (Log_USR_ProfilToRolaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_ProfilToRola, Org_USR_ProfilID, Org_USR_RolaID, Org_KluczZdalny, Org_Aktywny, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_USR_ProfilToRolaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_ProfilToRola, Org_USR_ProfilID, Org_USR_RolaID, Org_KluczZdalny, Org_Aktywny, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_USR_ProfilToRola WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_ProfilToRola OFF

DROP TABLE dbo.LOG_USR_ProfilToRola

EXECUTE sp_rename N'dbo.Tmp_LOG_USR_ProfilToRola', N'LOG_USR_ProfilToRola', 'OBJECT'

ALTER TABLE [dbo].[LOG_USR_ProfilToRola] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_USR_ProfilToRola] PRIMARY KEY  CLUSTERED
(
   [Log_USR_ProfilToRolaID]
) ON [PRIMARY]

END
GO

