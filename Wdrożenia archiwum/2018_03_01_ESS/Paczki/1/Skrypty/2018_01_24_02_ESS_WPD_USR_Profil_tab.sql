
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__USR_Profi__CzyKo__3ED4C61E]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_Profil]'))
ALTER TABLE [dbo].[USR_Profil] DROP CONSTRAINT [DF__USR_Profi__CzyKo__3ED4C61E]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__USR_Histo__USR_P__6FE211CD]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_HistoriaAkcji]'))
ALTER TABLE [dbo].[USR_HistoriaAkcji] DROP CONSTRAINT [FK__USR_Histo__USR_P__6FE211CD]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__USR_Profi__USR_P__6B1D5CB0]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_ProfilToRola]'))
ALTER TABLE [dbo].[USR_ProfilToRola] DROP CONSTRAINT [FK__USR_Profi__USR_P__6B1D5CB0]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_USR_HistoriaHasla_USR_Profil]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_HistoriaHasla]'))
ALTER TABLE [dbo].[USR_HistoriaHasla] DROP CONSTRAINT [FK_USR_HistoriaHasla_USR_Profil]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[USR_Profil]') AND name = N'PK_USR_Profil')
ALTER TABLE [dbo].[USR_Profil] DROP CONSTRAINT [PK_USR_Profil]

--------------------------------------
----------------USR_Profil---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[USR_Profil]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[USR_Profil]
GO

CREATE TABLE [dbo].[USR_Profil] (
   [IDUSR_Profil] [int]  IDENTITY(1,1) NOT NULL
  ,[Email] [varchar] (254) NULL
  ,[Login] [varchar] (50) NULL
  ,[HasloPlain] [varchar] (128) NULL
  ,[HasloHash] [varchar] (160) NULL
  ,[ZiarnoHasla] [varchar] (32) NULL
  ,[Imie] [varchar] (50) NOT NULL
  ,[DrugieImie] [varchar] (50) NULL
  ,[NickName] [varchar] (50) NULL
  ,[Nazwisko] [varchar] (50) NOT NULL
  ,[PrefiksTelefonu] [varchar] (5) NULL
  ,[Telefon] [varchar] (20) NULL
  ,[CzyHasloJednorazowe] [bit]  NULL
  ,[CzyKontoAktywne] [bit]  NOT NULL
  ,[DataOstatniejZmianyHasla] [datetime]  NULL
  ,[Token] [uniqueidentifier]  NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO



ALTER TABLE [dbo].[USR_Profil] ADD  CONSTRAINT [PK_USR_Profil] PRIMARY KEY CLUSTERED( IDUSR_Profil ) ON [PRIMARY]

ALTER TABLE [dbo].[USR_Profil] ADD  CONSTRAINT [DF__USR_Profi__CzyKo__3ED4C61E]  DEFAULT ((0)) FOR [CzyKontoAktywne]
GO