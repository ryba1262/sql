-- ******************* -- WPWPlanySzczegolowe -- ******************* --

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WPWPlanySzczegolowe]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WPWPlanySzczegolowe]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[WPWPlanySzczegolowe] @pracownikID INT, @DataOD DATE, @DataDO DATE, @SprawdzAktywnosc BIT
AS
select distinct 
  [gz].[IDGrupaZajeciowa]
, DATEADD(minute, [ppd].[OdGodz], [ppd].[Data]) AS [DataOD]
, DATEADD(minute, [ppd].[DoGodz], [ppd].[Data]) AS [DataDO]
, [B].[Symbol] + [S].[Numer] AS [Sala]
, [B].[Adres] AS [Adres]
, CASE
	WHEN [FW].[NazwaFormy] IS NOT NULL THEN [P].[Nazwa] + ': ' + [FW].[NazwaFormy]
	WHEN [FW].[NazwaFormy] IS NULL THEN [P].[Nazwa]
end as [PNazwa]
, [fz].[Symbol6Znakow] as [TypZajecSkrot]
, [fz].[Nazwa] as [TypZajec]
from
			[PlanPozDef] [ppd]
JOIN		[PlanZajecPoz] [pzp]		on [ppd].[PlanZajecPozIDNr] = [pzp].[IDPlanZajecPoz]
LEFT JOIN	[GrupaZajeciowa] [gz]		on [pzp].[GrupaZajeciowaID] = [gz].[IDGrupaZajeciowa]
JOIN		[FormaWymiar] [fw]			on [gz].[FormaWymiarID] = [fw].[IDFormaWymiar]
JOIN		[SemestrRealizacji] [SR]	on [SR].[IDSemestrRealizacji] = [FW].[SemestrRealizacjiID]
JOIN		[KartaUzup] [KU]			on [KU].[IDKartaUzup] = [SR].[KartaUzupID]
JOIN		[Przedmiot] [P]				on [P].[IDPrzedmiot] = [KU].[PrzedmiotID]
LEFT JOIN	[PlanZajec] [pz]			on [ppd].[PlanZajecPozIDNr] = [pz].[IDPlanZajec]
JOIN		[Sala] [S]					on [S].[IDSala] = [ppd].[SalaIDNr]
JOIN		[Budynek] [B]				on [B].[IDBudynek] = [S].[BudynekID]
LEFT JOIN	[PakietZaj] [pakiet]		on [ppd].[PakietIDNr] = [pakiet].[IDPakietZaj]
LEFT JOIN	[FormaZajec] [FZ]			on [FZ].[IDFormaZajec] = [FW].[FormaZajecID]
where 
[ppd].[PracownikIDNr] = @pracownikID
and  (@SprawdzAktywnosc = 0 OR ([pz].[StatusAktywny] = 1 OR [pz].[StatusAktywny] is null))
and [ppd].Data >= @DataOD
and ppd.Data <= @DataDO 
group by [gz].[IDGrupaZajeciowa], [P].[Nazwa], [ppd].[Data], [ppd].[OdGodz], [ppd].[DoGodz], [FW].[NazwaFormy], [B].[Symbol], [S].[Numer], [B].[Adres], [FZ].[Symbol6Znakow], [Fz].[Nazwa]
order by [DataOd] desc



GO
