
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KartaEgzBrakujacePrzedmiotyZaZero]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[KartaEgzBrakujacePrzedmiotyZaZero]
GO

SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE FUNCTION [dbo].[KartaEgzBrakujacePrzedmiotyZaZero] (@IDKartaEgz int)  
RETURNS int AS  
BEGIN 

	declare @BrakujacePrzedmioty int, @Zatwierdzona int , @KERokAkadOd int, @KESemestrIDOd int

	set @BrakujacePrzedmioty = 0
	select @Zatwierdzona = Zatwierdzona from KartaEgz where IDKartaEgz = @IDKartaEgz
			
	select @KERokAkadOd = Wartosc from UstawieniaProgramu where Nazwa = 'KERokAkadOd'
	select @KESemestrIDOd = Wartosc from UstawieniaProgramu where Nazwa = 'KESemestrOdID'

	if (@Zatwierdzona = 1)
	  begin

		select @BrakujacePrzedmioty = count(distinct IDSemestrRealizacji)  
		from KartaEgzPoz
		join KartaEgz on KartaEgzID = IDKartaEgz
		join SemestrRealizacji on IDSemestrRealizacji = SemestrRealizacjiID
		left join SkalaOcenPoz sop on OcenaKoncowaID = IDSkalaOcenPoz
		join KartaEgzForma kef on KartaEgzPozID = IDKartaEgzPoz
		left join FormaWymiar on kef.FormaWymiarID = IDFormaWymiar
		left join FormaZajec on FormaZajecID = IDFormaZajec
		left join SkalaOcenPoz sopcz on sopcz.IDSkalaOcenPoz = kef.OcenaID
		where IDKartaEgz = @IDKartaEgz 
		and LiczbaPunktow = 0 
		and ((coalesce(sop.CzyZalicza, 0) = 0 and RokAkad*10+SemestrID<(@KERokAkadOd*10 + @KESemestrIDOd)) or (FormaZajec.CzyDrukowacNaKarcieEgz = 1 and coalesce(sopcz.CzyZalicza,0) = 0 and RokAkad*10+SemestrID>=(@KERokAkadOd*10 + @KESemestrIDOd)))

		if (@BrakujacePrzedmioty is null)
			set @BrakujacePrzedmioty = 0

	  end
	else 
		set @BrakujacePrzedmioty = null

	return @BrakujacePrzedmioty

END



GO


grant execute on [dbo].[KartaEgzBrakujacePrzedmiotyZaZero] to [user]