IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__USR_Profi__USR_R__6C1180E9]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_ProfilToRola]'))
ALTER TABLE [dbo].[USR_ProfilToRola] DROP CONSTRAINT [FK__USR_Profi__USR_R__6C1180E9]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[USR_Rola]') AND name = N'PK_USR_Rola')
ALTER TABLE [dbo].[USR_Rola] DROP CONSTRAINT [PK_USR_Rola]


--------------------------------------
----------------USR_Rola---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[USR_Rola]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[USR_Rola]
GO

CREATE TABLE [dbo].[USR_Rola] (
   [IDUSR_Rola] [int]  IDENTITY(1,1) NOT NULL
  ,[Nazwa] [varchar] (128) NOT NULL
  ,[Podmiot] [varchar] (128) NULL
  ,[Klucz] [varchar] (128) NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[USR_Rola] ADD  CONSTRAINT [PK_USR_Rola] PRIMARY KEY CLUSTERED( IDUSR_Rola ) ON [PRIMARY]

GO