DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------USR_HistoriaHasla-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_HistoriaHasla_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER USR_HistoriaHasla_INSERT
')

EXEC('
CREATE TRIGGER USR_HistoriaHasla_INSERT ON ['+@BazaDanych+'].[dbo].[USR_HistoriaHasla] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_HistoriaHasla'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_HistoriaHasla]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_HistoriaHasla]
  ,[Org_USR_ProfilID]
  ,[Org_HasloPlain]
  ,[Org_HasloHash]
  ,[Org_ZiarnoHasla]
  ,[Org_IP]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDUSR_HistoriaHasla
 ,ins.USR_ProfilID
 ,ins.HasloPlain
 ,ins.HasloHash
 ,ins.ZiarnoHasla
 ,ins.IP
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_HistoriaHasla_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER USR_HistoriaHasla_UPDATE
')

EXEC('
CREATE TRIGGER USR_HistoriaHasla_UPDATE ON ['+@BazaDanych+'].[dbo].[USR_HistoriaHasla] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_HistoriaHasla'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_HistoriaHasla]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_HistoriaHasla]
  ,[Org_USR_ProfilID]
  ,[Org_HasloPlain]
  ,[Org_HasloHash]
  ,[Org_ZiarnoHasla]
  ,[Org_IP]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDUSR_HistoriaHasla
 ,ins.USR_ProfilID
 ,ins.HasloPlain
 ,ins.HasloHash
 ,ins.ZiarnoHasla
 ,ins.IP
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_HistoriaHasla_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER USR_HistoriaHasla_DELETE
')

EXEC('
CREATE TRIGGER USR_HistoriaHasla_DELETE ON ['+@BazaDanych+'].[dbo].[USR_HistoriaHasla] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_HistoriaHasla'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_HistoriaHasla]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_HistoriaHasla]
  ,[Org_USR_ProfilID]
  ,[Org_HasloPlain]
  ,[Org_HasloHash]
  ,[Org_ZiarnoHasla]
  ,[Org_IP]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDUSR_HistoriaHasla
 ,del.USR_ProfilID
 ,del.HasloPlain
 ,del.HasloHash
 ,del.ZiarnoHasla
 ,del.IP
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'USR_HistoriaHasla')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('USR_HistoriaHasla', 1, 0,GetDate())

GO