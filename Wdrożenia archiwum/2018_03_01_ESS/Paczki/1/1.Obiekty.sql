----------wdro1------2018_01_04_02_ESS_KartaEgz_KEZaproponujRejestracje_Fun
----------wdro2------2018_01_05_01_ESS_KartaEgz_Flagi_Fun
----------wdro3------2018_01_08_01_ESS_KartaEgz_BrakujacePrzedmioty_fun
----------wdro4------2018_01_08_02_ESS_KartaEgz_BrakujacePrzedmiotyZaZero_fun
----------wdro5------2018_01_10_01_ESS_KartaEgz_SredniaArytmetyczna_fun
----------wdro6------2018_01_16_02_ESS_Raporty_GetTerminUkonczenia_fun

----------wdro7------2018_01_24_02_ESS_WPD_USR_Profil_tab
----------wdro8------2018_01_24_05_ESS_WPD_USR_Rola_tab
----------wdro9------2018_01_24_09_ESS_WPD_USR_ProfiToRola_tab
----------wdro10------2018_01_24_12_ESS_WPD_USR_HistoriaLogowan_tab
----------wdro11------2018_01_24_15_ESS_WPD_USR_HistoriaHasla_tab
----------wdro12------2018_01_24_18_ESS_WPD_USR_HistoriaAkcji_tab
----------wdro13------2018_01_26_01_ESS_WPD_WPWPlanySzczegolowe_prc
----------wdro14------2018_01_26_02_ESS_WPD_WPWPlanySemestralny_prc
----------wdro15------2018_01_29_01_ESS_WPD_WPWPlanySzczegolowe_prc



--------------------------------------------------------wdro1-----------------------------------------------------


-- ******************* -- KartaEgz_KEZaproponujRejestracje -- ******************* --


SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

/*
Funkcja zwraca propozycj� rejestracji dla danej karty egzaminacyjnej
2 - studenci z prawem do rejestracji pe�nej
3 - studenci z prawem do rejestracji warunkowej
4 - studenci bez prawa do rejestracji
*/
DROP Function [dbo].[KEZaproponujRejestracjeDlaKartyEgz]
go

CREATE function [dbo].[KEZaproponujRejestracjeDlaKartyEgz] (@KartaEgzID int, @RejestracjaWarunkowaKryteriumID int)  
returns @result table(IndeksID int, IDKartaEgz int,KartaPoczatkowa int, RodzajRejestracji int, UzyskanaLP int, BrakujacaLP int,UzyskanaLPrzedm int, BrakujacaLPrzedm int,KryteriumRejestracjiWarunkowej varchar(100))
begin	


    declare @CzyNoweKartyEgz bit, @KERokAkadOd int, @KESemestrIDOd int
	select @KERokAkadOd = Wartosc from UstawieniaProgramu where Nazwa = 'KERokAkadOd'
	select @KESemestrIDOd = Wartosc from UstawieniaProgramu where Nazwa = 'KESemestrOdID'
	
	select @CzyNoweKartyEgz = (case when COUNT(*)>0 then 1 else 0 end) from KartaEgz where IDKartaEgz = @KartaEgzID and RokAkad*10+SemestrID>=@KERokAkadOd*10+@KESemestrIDOd
	
	
	;with DaneWejsciowe as
	(
		select SemestrRealizacjiID, ke.SemestrStudiow, LiczbaPunktow, CzyPoprawkowy, CzyZalicza, KartaPoczatkowa, IndeksID, IDKartaEgz, TylkoKartaGlowna,
		(select COUNT(*) from KartaEgzForma 
		left join SkalaOcenPoz on OcenaID = IDSkalaOcenPoz
		JOIN FormaWymiar ON KartaEgzForma.FormaWymiarID = FormaWymiar.IDFormaWymiar
        LEFT JOIN FormaZajec ON FormaWymiar.FormaZajecID = FormaZajec.IDFormaZajec
		where KartaEgzPozID =kep.IDKartaEgzPoz	and CzyDrukowacNaKarcieEgz = 1 and (CzyZalicza = 0 or CzyZalicza  is null)) NiezaliczonePrzedmioty
		from KartaEgz ke		  
		join KartaEgzPoz kep on ke.IDKartaEgz = kep.KartaEgzID
								and ke.IDKartaEgz = @KartaEgzID
		left join SkalaOcenPoz on OcenaKoncowaID = IDSkalaOcenPoz
		join SemestrRealizacji on SemestrRealizacjiID = IDSemestrRealizacji			
		left join RejestracjaWarunkowaKryterium on IDRejestracjaWarunkowaKryterium = @RejestracjaWarunkowaKryteriumID 
												and CzyZatwierdzono = 1
		left join RejestracjaWarunkowaKryteriumPoz rwkp on IDRejestracjaWarunkowaKryterium = RejestracjaWarunkowaKryteriumID and rwkp.SemestrStudiow = ke.SemestrStudiow					  	
	),
	DaneWejsciowe2 (SemestrRealizacjiID, SemestrStudiow, LiczbaPunktow, CzyPoprawkowy, CzyZalicza, KartaPoczatkowa, IndeksID, IDKartaEgz, TylkoKartaGlowna)
	as
	(
	select SemestrRealizacjiID, SemestrStudiow, LiczbaPunktow, CzyPoprawkowy, 
	(case when @CzyNoweKartyEgz = 1 then case when NiezaliczonePrzedmioty>0 then 0 else 1 end  else CzyZalicza end), 
	KartaPoczatkowa, IndeksID, IDKartaEgz, TylkoKartaGlowna 
	from DaneWejsciowe
	)	
	, 
	PunktyDlaKartyStudenta(IndeksID, IDKartaEgz, SemestrStudiow, KartaPoczatkowa, NominalnaLP, UzyskanaLP, BrakujacaLP, BrakujacePrzedmiotyZaZero, NominalnaLPrzedm, UzyskanaLPrzedm, BrakujacaLPrzedm) as
	(
		select  IndeksID, 
				IDKartaEgz,
				SemestrStudiow,
				KartaPoczatkowa,
				sum(
				  case 
					when
						(coalesce(CzyPoprawkowy, 0) = 0 or (CzyPoprawkowy = 1 and coalesce(TylkoKartaGlowna, 0) <> 1))
					then
						LiczbaPunktow
					else
						0
				  end) as NominalnaLP,
				sum(
				  case 
					when
						 (coalesce(CzyPoprawkowy, 0) = 0 or (CzyPoprawkowy = 1 and coalesce(TylkoKartaGlowna, 0) <> 1))
						 and coalesce(CzyZalicza, 0) = 1
					then
						LiczbaPunktow
					else
						0					 
				  end) as UzyskanaLP,
				sum(
				  case 
					when
						 (coalesce(CzyPoprawkowy, 0) = 0 or (CzyPoprawkowy = 1 and coalesce(TylkoKartaGlowna, 0) <> 1))
						 and coalesce(CzyZalicza, 0) = 0 and LiczbaPunktow > 0
					then
						LiczbaPunktow
					else
						0					 
				  end) as BrakujacaLP,
				sum(
				  case 
					when
						 (coalesce(CzyPoprawkowy, 0) = 0 or (CzyPoprawkowy = 1 and coalesce(TylkoKartaGlowna, 0) <> 1))
						 and coalesce(CzyZalicza, 0) = 0 and LiczbaPunktow = 0
					then
						1
					else
						0					 
				  end) as BrakujacePrzedmiotyZaZero,
				count(
				  case 
					when
						(coalesce(CzyPoprawkowy, 0) = 0 or (CzyPoprawkowy = 1 and coalesce(TylkoKartaGlowna, 0) <> 1))
					then
						SemestrRealizacjiID					
				  end) as NominalnaLPrzedm,
				count(
				  case 
					when
						 (coalesce(CzyPoprawkowy, 0) = 0 or (CzyPoprawkowy = 1 and coalesce(TylkoKartaGlowna, 0) <> 1))
						 and coalesce(CzyZalicza, 0) = 1
					then
						SemestrRealizacjiID										 
				  end) as UzyskanaLPrzedm,
				count(
				  case 
					when
						 (coalesce(CzyPoprawkowy, 0) = 0 or (CzyPoprawkowy = 1 and coalesce(TylkoKartaGlowna, 0) <> 1))
						 and coalesce(CzyZalicza, 0) = 0
					then
						SemestrRealizacjiID										 
				  end) as BrakujacaLPrzedm
		from DaneWejsciowe2
		group by IndeksID, IDKartaEgz, SemestrStudiow, KartaPoczatkowa
	)
    insert into @result
	select  IndeksID,
			IDKartaEgz,
			KartaPoczatkowa,
			case 
				when 
					PunktyMinimalne is null and PunktyBrakujaceMaksymalnie is null and PrzedmiotyBrakujaceMaksymalnie is null
				then
					null
				when 
					(PunktyMinimalne is not null or PunktyBrakujaceMaksymalnie is not null) and PrzedmiotyBrakujaceMaksymalnie is null
				then	 
					case 
						when
							UzyskanaLP = NominalnaLP and BrakujacaLP = 0 and BrakujacePrzedmiotyZaZero = 0 --sprawdzenie czy jest to rejestracja pe�na
						then
							2
						else
						  case 
							when 
								 (PunktyMinimalne is not null and UzyskanaLP >= PunktyMinimalne)
								 or (PunktyBrakujaceMaksymalnie is not null and BrakujacaLP <= PunktyBrakujaceMaksymalnie) --sprawdzenie warunkow i wpisanie wiersza z odpowiednia wartoscia do kolumny RodzajRejestracji
							then
								3
							else
								4
						  end
					end 
				when
					PunktyMinimalne is null and PunktyBrakujaceMaksymalnie is null and PrzedmiotyBrakujaceMaksymalnie is not null
				then
					case 
						when
							UzyskanaLPrzedm = NominalnaLPrzedm and BrakujacaLPrzedm = 0 --sprawdzenie czy jest to rejestracja pe�na
						then
							2
						else
						  case 
							when 								 
								BrakujacaLPrzedm <= PrzedmiotyBrakujaceMaksymalnie --sprawdzenie warunkow i wpisanie wiersza z odpowiednia wartoscia do kolumny RodzajRejestracji
							then
								3
							else
								4
						  end
					end				
			end as RodzajRejestracji,
			UzyskanaLP,
			BrakujacaLP,
			UzyskanaLPrzedm,
			BrakujacaLPrzedm,
			case 
				when 
					PunktyMinimalne is null and PunktyBrakujaceMaksymalnie is null and PrzedmiotyBrakujaceMaksymalnie is null
				then
					'Brak kryterium'
				when 
					(PunktyMinimalne is not null or PunktyBrakujaceMaksymalnie is not null) and PrzedmiotyBrakujaceMaksymalnie is null
				then
					'Punkty'
				when
					PunktyMinimalne is null and PunktyBrakujaceMaksymalnie is null and PrzedmiotyBrakujaceMaksymalnie is not null
				then
					'Przedmioty'
			end as KryteriumRejestracjiWarunkowej
	from PunktyDlaKartyStudenta as pdks
	left join RejestracjaWarunkowaKryterium on IDRejestracjaWarunkowaKryterium = @RejestracjaWarunkowaKryteriumID 
											and CzyZatwierdzono = 1
	left join RejestracjaWarunkowaKryteriumPoz rwkp on IDRejestracjaWarunkowaKryterium = RejestracjaWarunkowaKryteriumID and rwkp.SemestrStudiow = pdks.SemestrStudiow
	
	
	return
end
go


--------------------------------------------------------wdro2-----------------------------------------------------


-- ******************* -- KartaEgz_Flagi -- ******************* --


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KartaEgzFlagi]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[KartaEgzFlagi]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[KartaEgzFlagi] (@IDKartaEgz int)  
RETURNS @Output table (Flaga int, BrakujacePunkty int, Czesne int, BrakPkt int, BrakCzesne int)
BEGIN

	declare @Flaga int, @BrakujacePunkty int, @ZdobytePunkty int, @BrakujacePunktyCzesne int, @DodatkowePunkty int,
			@RokAkad int, @SemestrID int, @IndeksID int, @BrakujacePrzedmiotyZaZero int,
			@Zatwierdzona int, @DecyzjaDziekanaPunkty int, @NastepnaRejestracjaID int, @BrakujacePunktyHistoria int,
			@BrakujacePunktyCzesneHistoria int, @NominalnaLiczbaPunktow int, @KERokAkadOd int, @KESemestrIDOd int
	
	set @Flaga = 0	 
	set @BrakujacePunkty = 0 
	set @ZdobytePunkty = 0 
	set @BrakujacePunktyCzesne = 0 
	set @DodatkowePunkty = 0
	set @BrakujacePrzedmiotyZaZero = 0
	set @NominalnaLiczbaPunktow = 0

	select @Zatwierdzona = Zatwierdzona, @RokAkad = RokAkad, @SemestrID = SemestrID, @IndeksID = IndeksID
	from KartaEgz where IDKartaEgz = @IDKartaEgz

	declare @ZRokAkad int, @ZSemestrID int
	set @ZRokAkad = 0
	set @ZSemestrID = 0

	select top 1 @ZRokAkad = RokAkademicki, @ZSemestrID = SemestrAkadID
	from HistoriaStudenta where ZdarzenieID = 32 and IndeksID = @IndeksID and (@RokAkad * 100 + @SemestrID) >= (RokAkademicki * 100 + SemestrAkadID) and Anulowany = 0
	order by RokAkademicki desc, SemestrAkadID desc

	if (@Zatwierdzona = 1)
	begin

		select  @NastepnaRejestracjaID = nast_rej.IDHistoriaStudenta,
				@BrakujacePunktyHistoria = cast(substring(nast_rej.OldParams, 10, 2) as int),
				@BrakujacePunktyCzesneHistoria = cast(substring(nast_rej.OldParams, 12, 2) as int)
		from KartaEgz ke
		cross apply
		(
			select top 1 IDHistoriaStudenta, OldParams
			from HistoriaStudenta
			where IndeksID = ke.IndeksID
			and ZdarzenieID = 1
			and Anulowany = 0
			and (RokAkademicki * 100 + SemestrAkadID) > (ke.RokAkad * 100 + ke.SemestrID)
			and cast(substring(OldParams, 1, 1) as int) = ke.SemestrStudiow		
			order by DataDodania	
		) nast_rej
		where ke.IDKartaEgz = @IDKartaEgz

		if (@NastepnaRejestracjaID is null)
		  begin
			set @BrakujacePunktyHistoria = -1
			set @BrakujacePunktyCzesneHistoria = -1
		  end

		
		select @KERokAkadOd = Wartosc from UstawieniaProgramu where Nazwa = 'KERokAkadOd'
	    select @KESemestrIDOd = Wartosc from UstawieniaProgramu where Nazwa = 'KESemestrOdID'
		set @NominalnaLiczbaPunktow = dbo.KartaEgzNominalnaLiczbaPkt(@IDKartaEgz)		

		;with ListaZaliczonychPrzedmiotow
		as
		(
			select  ke.IndeksID,
					ke.IDKartaEgz,
					kep.IDKartaEgzPoz,
					hr.IDSemReal,
					sr.LiczbaPunktow,
					sop.CzyZalicza,
					sop.Wartosc,
					case when (select COUNT(*) from KartaEgzForma 
		                       left join SkalaOcenPoz on OcenaID = IDSkalaOcenPoz
		                       JOIN FormaWymiar ON KartaEgzForma.FormaWymiarID = FormaWymiar.IDFormaWymiar
                                LEFT JOIN FormaZajec ON FormaWymiar.FormaZajecID = FormaZajec.IDFormaZajec
		                        where KartaEgzPozID =kep.IDKartaEgzPoz	and CzyDrukowacNaKarcieEgz = 1 and (CzyZalicza = 0 or CzyZalicza  is null) and CzyPoprawkowy = 0)>0 then 0 else 1 end CzyZaliczaNowe, 
		            case when RokAkad*10+SemestrID >=@KERokAkadOd*10+@KESemestrIDOd then 1 else 0 end CzyNoweKarty,
					row_number() over (partition by IDSemReal order by kep.IDKartaEgzPoz desc) as NrWiersza
			from dbo.KartaEgzHistoriaRozliczen(@IndeksID, coalesce(@ZRokAkad, 0), coalesce(@ZSemestrID, 0), @RokAkad, @SemestrID, 1, null) hr
			join SemestrRealizacji sr on hr.IDSemReal = sr.IDSemestrRealizacji
			join KartaEgzPoz kep on hr.IDSemReal = kep.SemestrRealizacjiID
			join KartaEgz ke on kep.KartaEgzID = ke.IDKartaEgz
							 and ke.IndeksID = @IndeksID							  
							 and (ke.RokAkad * 100 + ke.SemestrID) <= (@RokAkad * 100 + @SemestrID)  
							 and (ke.RokAkad * 100 + ke.SemestrID) >= (coalesce(@ZRokAkad, 0) * 100 + coalesce(@ZSemestrID, 0))
			left join SkalaOcenPoz sop on kep.OcenaKoncowaID = sop.IDSkalaOcenPoz
								 
		)

		select @ZdobytePunkty = sum(LiczbaPunktow)
		from ListaZaliczonychPrzedmiotow
		where NrWiersza = 1 and ((CzyZalicza = 1 and CzyNoweKarty = 0) or (CzyZaliczaNowe = 1 and CzyNoweKarty = 1))

		set @BrakujacePunkty = coalesce(@NominalnaLiczbaPunktow, 0) - coalesce(@ZdobytePunkty, 0)



		;with KorektyLiczbyPunktow as
		( 
			select  klpp.ObowiazujacaLPunktow,
					sr.LiczbaPunktow,
					row_number() over (partition by hr.IDSemReal order by hs.RokAkademicki desc, hs.SemestrAkadID desc) as NrWiersza
			from dbo.KartaEgzHistoriaRozliczen(@IndeksID, coalesce(@ZRokAkad, 0), coalesce(@ZSemestrID, 0), @RokAkad, @SemestrID, 1, null) hr
			join SemestrRealizacji sr on hr.IDSemReal = sr.IDSemestrRealizacji
			join KorektaLiczbyPunktowPoz as klpp on hr.IDSemReal = klpp.SemestrRealizacjiID
			join KorektaLiczbyPunktow klp on klpp.KorektaLiczbyPunktowID = klp.IDKorektaLiczbyPunktow 
											 and klp.IndeksID = @IndeksID 
											 and (klp.RokAkad * 100 + klp.SemestrID) <= (@RokAkad * 100 + @SemestrID)
											 and (klp.RokAkad * 100 + klp.SemestrID) >= (coalesce(@ZRokAkad, 0) * 100 + coalesce(@ZSemestrID, 0))
			join HistoriaStudenta hs on hs.TabelaID = klp.IDKorektaLiczbyPunktow 
									 and hs.ZdarzenieID = 33 
									 and hs.IndeksID = @IndeksID
									 and hs.Anulowany = 0
		)

		select @DecyzjaDziekanaPunkty = sum(ObowiazujacaLPunktow - LiczbaPunktow)
		from KorektyLiczbyPunktow
		where NrWiersza = 1

		set @BrakujacePunkty = coalesce(@BrakujacePunkty, 0) - coalesce(@DecyzjaDziekanaPunkty, 0)



		select top 1 @DodatkowePunkty = PoprzSemPkt
		from KartaEgz ke	
		where ke.IndeksID = @IndeksID				 
		and (ke.RokAkad * 100 + ke.SemestrID) <= (@RokAkad * 100 + @SemestrID)
		and (ke.RokAkad * 100 + ke.SemestrID) >= (coalesce(@ZRokAkad, 0) * 100 + coalesce(@ZSemestrID, 0))
		order by ke.RokAkad, ke.SemestrID, ke.KartaPoczatkowa desc

		set @BrakujacePunkty = coalesce(@BrakujacePunkty, 0) - coalesce(@DodatkowePunkty, 0)



		select @BrakujacePrzedmiotyZaZero = dbo.KartaEgzBrakujacePrzedmiotyZaZero(@IDKartaEgz)
		
		if(@BrakujacePunkty < 0)
			set @BrakujacePunkty = 0
		set @BrakujacePunktyCzesne = @BrakujacePunkty + (coalesce(@BrakujacePrzedmiotyZaZero, 0) * 3)

		if(@BrakujacePunktyCzesne >= 3)
			set @BrakujacePunktyCzesne = @BrakujacePunktyCzesne - 3
		else 
			set @BrakujacePunktyCzesne = 0



		if (@BrakujacePunktyHistoria = -1 and @BrakujacePunktyCzesneHistoria = -1) 
		  begin	
			set @Flaga = 0
			set @BrakujacePunktyHistoria = @BrakujacePunkty
			set @BrakujacePunktyCzesneHistoria = @BrakujacePunktyCzesne
		  end
		else
		  begin
			if ((@BrakujacePunkty - @BrakujacePunktyHistoria) = 0 and (@BrakujacePunktyCzesne - @BrakujacePunktyCzesneHistoria) = 0)
				set @Flaga = 0 
			else 
			  begin
				if ((@BrakujacePunkty - @BrakujacePunktyHistoria) <> 0 and (@BrakujacePunktyCzesne - @BrakujacePunktyCzesneHistoria) <> 0) 
					set @Flaga = 3
				else if ((@BrakujacePunkty - @BrakujacePunktyHistoria) <> 0)
					set @Flaga = 1
				else if ((@BrakujacePunktyCzesne - @BrakujacePunktyCzesneHistoria) <> 0)
					set @Flaga = 2			
			  end
		  end
	end

	insert into @Output (Flaga, BrakujacePunkty, Czesne, BrakPkt, BrakCzesne)  
	select @Flaga, @BrakujacePunkty, @BrakujacePunkty, @BrakujacePunktyHistoria, @BrakujacePunktyCzesneHistoria

	return

END

GO


--------------------------------------------------------wdro3-----------------------------------------------------


-- ******************* -- KartaEgz_BrakujacePrzedmioty -- ******************* --


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KEBrakujacaLiczbaPrzedmiotow]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[KEBrakujacaLiczbaPrzedmiotow]
GO

SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[KEBrakujacaLiczbaPrzedmiotow] (@IDKartaEgz int)  
RETURNS @Output table (Flaga int, BrakujacePrzedmioty int, BrakujacePrzedmiotyCzesne int, BrakujacePrzedmiotyHistoria int, BrakujacePrzedmiotyCzesneHistoria int)  
BEGIN

	declare @BrakujacePrzedmioty int,
			@BrakujacePrzedmiotyCzesne int, 
			@ZaliczonePrzedmioty int, 
			@NominalnaLiczbaPrzedmiotow int, @KERokAkadOd int, @KESemestrIDOd int
	 
	set @BrakujacePrzedmioty = 0
	set @BrakujacePrzedmiotyCzesne = 0
	set @ZaliczonePrzedmioty = 0
	set @NominalnaLiczbaPrzedmiotow = 0

	declare @RokAkad int,			
			@SemestrID int,
			@IndeksID int,
			@Zatwierdzona int

	declare @NastepnaRejestracjaID int,
			@BrakujacePrzedmiotyHistoria int,
			@BrakujacePrzedmiotyCzesneHistoria int,
			@Flaga int

	select @Zatwierdzona = Zatwierdzona, @RokAkad = RokAkad, @SemestrID = SemestrID, @IndeksID = IndeksID
	from KartaEgz where IDKartaEgz = @IDKartaEgz

	declare @ZRokAkad int, @ZSemestrID int
	set @ZRokAkad = 0
	set @ZSemestrID = 0
	select top 1 @ZRokAkad = RokAkademicki, @ZSemestrID = SemestrAkadID
	from HistoriaStudenta where ZdarzenieID = 32 and IndeksID = @IndeksID and (@RokAkad * 100 + @SemestrID) >= (RokAkademicki * 100 + SemestrAkadID) and Anulowany = 0
	order by RokAkademicki desc, SemestrAkadID desc

	if (@Zatwierdzona = 1)
	  begin

		select  @NastepnaRejestracjaID = nast_rej.IDHistoriaStudenta,
				@BrakujacePrzedmiotyHistoria = cast(substring(nast_rej.OldParams, 15, 2) as int),
				@BrakujacePrzedmiotyCzesneHistoria = cast(substring(nast_rej.OldParams, 17, 2) as int)
		from KartaEgz ke
		cross apply
		(
			select top 1 IDHistoriaStudenta, OldParams
			from HistoriaStudenta
			where IndeksID = ke.IndeksID
			and ZdarzenieID = 1
			and Anulowany = 0
			and (RokAkademicki * 100 + SemestrAkadID) > (ke.RokAkad * 100 + ke.SemestrID)
			and cast(substring(OldParams, 1, 1) as int) = ke.SemestrStudiow		
			order by DataDodania	
		) nast_rej
		where ke.IDKartaEgz = @IDKartaEgz	
		
		if (@NastepnaRejestracjaID is null)
		  begin
			set @BrakujacePrzedmiotyHistoria = -1
			set @BrakujacePrzedmiotyCzesneHistoria = -1
		  end
	 

		
		select @KERokAkadOd = Wartosc from UstawieniaProgramu where Nazwa = 'KERokAkadOd'
	    select @KESemestrIDOd = Wartosc from UstawieniaProgramu where Nazwa = 'KESemestrOdID'
		select @NominalnaLiczbaPrzedmiotow = dbo.KENominalnaLiczbaPrzedmiotow(@IDKartaEgz)

		;with ListaZaliczonychPrzedmiotow
		as
		(
			select  ke.IndeksID,
					ke.IDKartaEgz,
					kep.IDKartaEgzPoz,
					hr.IDSemReal,
					sop.CzyZalicza,
					sop.Wartosc,
					case when (select COUNT(*) from KartaEgzForma 
		                       left join SkalaOcenPoz on OcenaID = IDSkalaOcenPoz
		                       JOIN FormaWymiar ON KartaEgzForma.FormaWymiarID = FormaWymiar.IDFormaWymiar
                                LEFT JOIN FormaZajec ON FormaWymiar.FormaZajecID = FormaZajec.IDFormaZajec
		                        where KartaEgzPozID =kep.IDKartaEgzPoz	and CzyDrukowacNaKarcieEgz = 1 and (CzyZalicza = 0 or CzyZalicza  is null) and CzyPoprawkowy = 0)>0 then 0 else 1 end CzyZaliczaNowe, 
		            case when RokAkad*10+SemestrID >=@KERokAkadOd*10+@KESemestrIDOd then 1 else 0 end CzyNoweKarty,
					row_number() over (partition by IDSemReal order by kep.IDKartaEgzPoz desc) as NrWiersza
			from dbo.KartaEgzHistoriaRozliczen(@IndeksID, coalesce(@ZRokAkad, 0), coalesce(@ZSemestrID, 0), @RokAkad, @SemestrID, 1, null) hr
			join KartaEgzPoz kep on hr.IDSemReal = kep.SemestrRealizacjiID
			join KartaEgz ke on kep.KartaEgzID = ke.IDKartaEgz
							 and ke.IndeksID = @IndeksID							  
							 and (ke.RokAkad * 100 + ke.SemestrID) <= (@RokAkad * 100 + @SemestrID)  
							 and (ke.RokAkad * 100 + ke.SemestrID) >= (coalesce(@ZRokAkad, 0) * 100 + coalesce(@ZSemestrID, 0))
			left join SkalaOcenPoz sop on kep.OcenaKoncowaID = sop.IDSkalaOcenPoz
								  
		)
		
		select @ZaliczonePrzedmioty = count(IDSemReal)
		from ListaZaliczonychPrzedmiotow
		where NrWiersza = 1	and ((CzyZalicza = 1 and CzyNoweKarty = 0) or (CzyZaliczaNowe = 1 and CzyNoweKarty = 1))		  	

		set @BrakujacePrzedmioty = coalesce(@NominalnaLiczbaPrzedmiotow, 0) - coalesce(@ZaliczonePrzedmioty, 0)



		if @BrakujacePrzedmioty < 0
			set @BrakujacePrzedmioty = 0

		set @BrakujacePrzedmiotyCzesne = @BrakujacePrzedmioty

		if (@BrakujacePrzedmiotyHistoria = -1 and @BrakujacePrzedmiotyCzesneHistoria = -1) 
		  begin	
			set @Flaga = 0
			set @BrakujacePrzedmiotyHistoria = @BrakujacePrzedmioty
			set @BrakujacePrzedmiotyCzesneHistoria = @BrakujacePrzedmiotyCzesne
		  end
		else
		  begin
			if ((@BrakujacePrzedmioty - @BrakujacePrzedmiotyHistoria) = 0 and (@BrakujacePrzedmiotyCzesne - @BrakujacePrzedmiotyCzesneHistoria) = 0)
				set @Flaga = 0 
			else 
			  begin
				if ((@BrakujacePrzedmioty - @BrakujacePrzedmiotyHistoria) <> 0 and (@BrakujacePrzedmiotyCzesne - @BrakujacePrzedmiotyCzesneHistoria) <> 0) 
					set @Flaga = 3
				else if ((@BrakujacePrzedmioty - @BrakujacePrzedmiotyHistoria) <> 0)
					set @Flaga = 1
				else if ((@BrakujacePrzedmiotyCzesne - @BrakujacePrzedmiotyCzesneHistoria) <> 0)
					set @Flaga = 2			
			  end
		  end
		
	  end

	insert into @Output (Flaga, BrakujacePrzedmioty, BrakujacePrzedmiotyCzesne, BrakujacePrzedmiotyHistoria, BrakujacePrzedmiotyCzesneHistoria)
	select  @Flaga,
			@BrakujacePrzedmioty, 
			@BrakujacePrzedmiotyCzesne,
			@BrakujacePrzedmiotyHistoria,
			@BrakujacePrzedmiotyCzesneHistoria

	RETURN

END


GO


--------------------------------------------------------wdro4-----------------------------------------------------


-- ******************* -- KartaEgz_BrakujacePrzedmiotyZaZero -- ******************* --


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KartaEgzBrakujacePrzedmiotyZaZero]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[KartaEgzBrakujacePrzedmiotyZaZero]
GO

SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO


CREATE FUNCTION [dbo].[KartaEgzBrakujacePrzedmiotyZaZero] (@IDKartaEgz int)  
RETURNS int AS  
BEGIN 

	declare @BrakujacePrzedmioty int, @Zatwierdzona int , @KERokAkadOd int, @KESemestrIDOd int

	set @BrakujacePrzedmioty = 0
	select @Zatwierdzona = Zatwierdzona from KartaEgz where IDKartaEgz = @IDKartaEgz
			
	select @KERokAkadOd = Wartosc from UstawieniaProgramu where Nazwa = 'KERokAkadOd'
	select @KESemestrIDOd = Wartosc from UstawieniaProgramu where Nazwa = 'KESemestrOdID'

	if (@Zatwierdzona = 1)
	  begin

		select @BrakujacePrzedmioty = count(distinct IDSemestrRealizacji)  
		from KartaEgzPoz
		join KartaEgz on KartaEgzID = IDKartaEgz
		join SemestrRealizacji on IDSemestrRealizacji = SemestrRealizacjiID
		left join SkalaOcenPoz sop on OcenaKoncowaID = IDSkalaOcenPoz
		join KartaEgzForma kef on KartaEgzPozID = IDKartaEgzPoz
		left join FormaWymiar on kef.FormaWymiarID = IDFormaWymiar
		left join FormaZajec on FormaZajecID = IDFormaZajec
		left join SkalaOcenPoz sopcz on sopcz.IDSkalaOcenPoz = kef.OcenaID
		where IDKartaEgz = @IDKartaEgz 
		and LiczbaPunktow = 0 
		and ((coalesce(sop.CzyZalicza, 0) = 0 and RokAkad*10+SemestrID<(@KERokAkadOd*10 + @KESemestrIDOd)) or (FormaZajec.CzyDrukowacNaKarcieEgz = 1 and coalesce(sopcz.CzyZalicza,0) = 0 and RokAkad*10+SemestrID>=(@KERokAkadOd*10 + @KESemestrIDOd)))

		if (@BrakujacePrzedmioty is null)
			set @BrakujacePrzedmioty = 0

	  end
	else 
		set @BrakujacePrzedmioty = null

	return @BrakujacePrzedmioty

END



GO


grant execute on [dbo].[KartaEgzBrakujacePrzedmiotyZaZero] to [user]


--------------------------------------------------------wdro5-----------------------------------------------------


-- ******************* -- KartaEgz_SredniaArytmetyczna -- ******************* --



/****** Object:  UserDefinedFunction [dbo].[KartaEgzSredniaArytmetyczna]    Script Date: 01/10/2018 09:55:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KartaEgzSredniaArytmetyczna]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[KartaEgzSredniaArytmetyczna]
GO

SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

-- =============================================
-- Author:		Katarzyna Goc�awska
-- Create date: 2015-08-18
-- Description:	Funkcja do obliczania �redniej arytmetycznej semestralnej, rocznej i �redniej ze studi�w
-- =============================================

CREATE FUNCTION [dbo].[KartaEgzSredniaArytmetyczna] (@IndeksID int, @RokAkad int, @SemestrID int, @SemestrStudiow int, @CzyRoczna bit, @CzyPoczatkowa bit, @CzyDecyzja bit, @CzyKartaPoczatkowa bit)
RETURNS float
AS
BEGIN

  DECLARE @Srednia float, @KERokAkadOd int, @KESemestrIDOd int

  select @KERokAkadOd = Wartosc from UstawieniaProgramu where Nazwa = 'KERokAkadOd'
  select @KESemestrIDOd = Wartosc from UstawieniaProgramu where Nazwa = 'KESemestrOdID'
  
  IF(@CzyKartaPoczatkowa = 1)
  BEGIN
      SET @Srednia = NULL
      RETURN @Srednia
  END  

  DECLARE @CzyZatwierdzona bit
  DECLARE @RozliczonaWarunkowo int

  IF(@RokAkad is not NULL) -- �rednia sem lub roczna
   BEGIN
       SELECT @CzyZatwierdzona = Zatwierdzona, @RozliczonaWarunkowo = RozliczonaWarunkowo
       FROM KartaEgz WHERE RokAkad = @RokAkad AND SemestrID = @SemestrID AND SemestrStudiow = @SemestrStudiow AND IndeksID = @IndeksID
   END
  ELSE
   BEGIN  -- srednia ze studi�w
       SELECT @CzyZatwierdzona = Zatwierdzona, @RozliczonaWarunkowo = RozliczonaWarunkowo
       FROM KartaEgz 
       LEFT JOIN OkresRozliczeniowyPoz orp on SemestrID = IDOkresRozliczeniowyPoz
       WHERE IndeksID = @IndeksID and (RokAkad*10+orp.LP) = (SELECT Max(RokAkad*10+okp.LP) FROM KartaEgz LEFT JOIN OkresRozliczeniowyPoz okp on SemestrID = IDOkresRozliczeniowyPoz WHERE IndeksID = @IndeksID)
   END


  DECLARE @RokOd int , @RokDo int, @WhrRokAkad int, @WhrSemestrID int
  DECLARE @SemestrOd int, @SemestrDo int
  DECLARE @SemS int  -- semestr studiow w przypadku sredniej semestralnej ustawiony, w innych przypadkach NULL
  SET @SemS = NULL
  
  	-- ostatnie wyzerowanie historii rozlicze�
	select  top (1)
			@WhrRokAkad = RokAkademicki,
			@WhrSemestrID = SemestrAkadID
	from HistoriaStudenta
	where ZdarzenieID = 32
	and Anulowany = 0
	and IndeksID = @IndeksID
	and ((@RokAkad is null and @SemestrID is null) or (RokAkademicki * 100 + SemestrAkadID <= @RokAkad * 100 + @SemestrID))
	order by RokAkademicki * 100 + SemestrAkadID desc 

  IF(@RozliczonaWarunkowo = 0 AND @CzyZatwierdzona = 1)
    BEGIN  -- rejestracja pe�na - karta zatwierdzona
     
       -- Wyznaczenie zakresow pobierania przedmiotow
       IF(@CzyRoczna = 0 AND @RokAkad is not NULL) --srednia semestralna
         BEGIN
            SET @RokDo = @RokAkad
            SET @SemestrDo = @SemestrID
            SET @SemS = @SemestrStudiow
            
            SELECT TOP 1 @RokOd = RokAkad, @SemestrOd = SemestrID FROM KartaEgz
            LEFT JOIN OkresRozliczeniowyPoz orp on SemestrID = IDOkresRozliczeniowyPoz            
            WHERE SemestrStudiow = @SemestrStudiow AND IndeksID = @IndeksID
            and RokAkad * 100 + SemestrID >= coalesce(@WhrRokAkad, 0) * 100 + coalesce(@WhrSemestrID, 0)
            ORDER BY RokAkad*10+orp.LP ASC 
         END
       ELSE IF(@CzyRoczna = 1 AND @RokAkad is not NULL)  --srednia roczna
         BEGIN
            IF(@SemestrStudiow %2=0)   
              BEGIN
                  SET @RokDo = @RokAkad
                  SET @SemestrDo = @SemestrID
                  
                  SELECT TOP 1 @RokOd = RokAkad, @SemestrOd = SemestrID FROM KartaEgz 
                  LEFT JOIN OkresRozliczeniowyPoz orp on SemestrID = IDOkresRozliczeniowyPoz
                  WHERE SemestrStudiow = (@SemestrStudiow-1) AND IndeksID = @IndeksID
                  and RokAkad * 100 + SemestrID >= coalesce(@WhrRokAkad, 0) * 100 + coalesce(@WhrSemestrID, 0)
                  ORDER BY RokAkad*10+orp.LP ASC 
                  
                  IF(((@SemestrStudiow-2)/2)+((@SemestrStudiow-2)%2) = 0) 
                  BEGIN
                     SELECT TOP 1 @RokOd = RokAkad, @SemestrOd = SemestrID FROM KartaEgz 
                     LEFT JOIN OkresRozliczeniowyPoz orp on SemestrID = IDOkresRozliczeniowyPoz
                     WHERE SemestrStudiow = (@SemestrStudiow-2) AND IndeksID = @IndeksID
                     and RokAkad * 100 + SemestrID >= coalesce(@WhrRokAkad, 0) * 100 + coalesce(@WhrSemestrID, 0)
                     ORDER BY RokAkad*10+orp.LP ASC 
                  END 
              END
            ELSE  -- srednia roczna z semestru nieparzystego = NULL
              BEGIN
                 SET @Srednia = NULL              
                 RETURN @Srednia
              END
         END
       ELSE  --srednia ze studiow
         BEGIN
            SELECT TOP 1 @RokOd = RokAkad, @SemestrOd = SemestrID FROM KartaEgz
            LEFT JOIN OkresRozliczeniowyPoz orp on SemestrID = IDOkresRozliczeniowyPoz
            WHERE IndeksID = @IndeksID
            and RokAkad * 100 + SemestrID >= coalesce(@WhrRokAkad, 0) * 100 + coalesce(@WhrSemestrID, 0)
            ORDER BY RokAkad*10+orp.LP ASC 

            SELECT TOP 1 @RokDo = RokAkad, @SemestrDo = SemestrID FROM KartaEgz 
            LEFT JOIN OkresRozliczeniowyPoz orp on SemestrID = IDOkresRozliczeniowyPoz
            WHERE IndeksID = @IndeksID
            and RokAkad * 100 + SemestrID >= coalesce(@WhrRokAkad, 0) * 100 + coalesce(@WhrSemestrID, 0)
            ORDER BY RokAkad*10+orp.LP DESC
         END  
      
      -- pobiera przedmioty z kart w zawezonym okresie
      DECLARE @TabelaPrzedmiotow TABLE (IDSemReal int ) 
      INSERT @TabelaPrzedmiotow  
      SELECT IDSemReal FROM [dbo].[KartaEgzHistoriaRozliczen] (@IndeksID, @RokOd,@SemestrOd, @RokDo, @SemestrDo,@CzyPoczatkowa, @SemS)      

      DECLARE @TabelaObliczenia TABLE(IDSemReal int, Punkty int, Ocena float, CzyZalicza bit, CzyOcena bit, NumerWiersza int, RokAkad int, SemestrID int, FormaWymiarID int, 
      CzyZaliczaNowe bit, CzyNoweKarty bit, OcenaCz float) 
      
      IF(@CzyPoczatkowa = 1)
      BEGIN
        INSERT @TabelaObliczenia
         SELECT IDSemestrRealizacji, LiczbaPunktow, SkalaOcenPoz.Wartosc,SkalaOcenPoz.CzyZalicza, SkalaOcenPoz.CzyOcena, ROW_NUMBER() over (partition by IDSemestrRealizacji order by FormaWymiarID asc), RokAkad, SemestrID, FormaWymiarID,
        case when (select COUNT(*) from KartaEgzForma 
		           left join SkalaOcenPoz on OcenaID = IDSkalaOcenPoz
		           JOIN FormaWymiar ON KartaEgzForma.FormaWymiarID = FormaWymiar.IDFormaWymiar
                   LEFT JOIN FormaZajec ON FormaWymiar.FormaZajecID = FormaZajec.IDFormaZajec
		           where KartaEgzForma.IDKartaEgzForma = kaf.IDKartaEgzForma	and CzyDrukowacNaKarcieEgz = 1 and (CzyZalicza <> 0 or CzyZalicza  is not null) and CzyPoprawkowy = 0)>0 then 1 else 0 end CzyZaliczaNowe, 
		case when RokAkad*10+SemestrID >=@KERokAkadOd*10+@KESemestrIDOd then 1 else 0 end CzyNoweKarty, skocz.Wartosc        
        FROM KartaEgzPoz
        join KartaEgzForma kaf on kaf.KartaEgzPozID = IDKartaEgzPoz
        LEFT JOIN KartaEgz ON KartaEgzID = IDKartaEgz
        LEFT JOIN SemestrRealizacji  ON SemestrRealizacjiID = IDSemestrRealizacji
        LEFT JOIN SkalaOcenPoz ON OcenaKoncowaID = IDSkalaOcenPoz
        left join SkalaOcenPoz skocz on kaf.OcenaID = skocz.IDSkalaOcenPoz
        WHERE IndeksID = @IndeksID AND Zatwierdzona = 1 /*AND CzyZalicza = 1 AND CzyOcena=1*/ AND SemestrRealizacjiID IN (SELECT IDSemReal FROM @TabelaPrzedmiotow) AND 
              ((RokAkad*10 + SemestrID) BETWEEN (@RokOd * 10 + @SemestrOd) AND (@RokDo * 10 + @SemestrDo))           
      END
      ELSE
      BEGIN
        INSERT @TabelaObliczenia
         SELECT IDSemestrRealizacji, LiczbaPunktow, SkalaOcenPoz.Wartosc,SkalaOcenPoz.CzyZalicza, SkalaOcenPoz.CzyOcena, ROW_NUMBER() over (partition by IDSemestrRealizacji order by FormaWymiarID asc), RokAkad, SemestrID, FormaWymiarID ,  
        case when (select COUNT(*) from KartaEgzForma 
		           left join SkalaOcenPoz on OcenaID = IDSkalaOcenPoz
		           JOIN FormaWymiar ON KartaEgzForma.FormaWymiarID = FormaWymiar.IDFormaWymiar
                   LEFT JOIN FormaZajec ON FormaWymiar.FormaZajecID = FormaZajec.IDFormaZajec
		           where KartaEgzForma.IDKartaEgzForma = kaf.IDKartaEgzForma and CzyDrukowacNaKarcieEgz = 1 and (CzyZalicza<> 0 or CzyZalicza  is not null) and CzyPoprawkowy = 0)>0 then 1 else 0 end CzyZaliczaNowe, 
		case when RokAkad*10+SemestrID >=@KERokAkadOd*10+@KESemestrIDOd then 1 else 0 end CzyNoweKarty, skocz.Wartosc     
        FROM KartaEgzPoz
        join KartaEgzForma kaf on kaf.KartaEgzPozID = IDKartaEgzPoz
        LEFT JOIN KartaEgz ON KartaEgzID = IDKartaEgz
        LEFT JOIN SemestrRealizacji  ON SemestrRealizacjiID = IDSemestrRealizacji
        LEFT JOIN SkalaOcenPoz ON OcenaKoncowaID = IDSkalaOcenPoz
        left join SkalaOcenPoz skocz on kaf.OcenaID = skocz.IDSkalaOcenPoz
        WHERE IndeksID = @IndeksID AND Zatwierdzona = 1 /*AND CzyZalicza = 1 AND CzyOcena=1*/ AND SemestrRealizacjiID IN (SELECT IDSemReal FROM @TabelaPrzedmiotow) AND 
              ((RokAkad*10 + SemestrID) BETWEEN (@RokOd * 10 + @SemestrOd) AND (@RokDo * 10 + @SemestrDo)) AND              
              (KartaPoczatkowa is NULL or KartaPoczatkowa = 0)
      END 

      IF(@CzyDecyzja = 1)
      BEGIN
         DECLARE @Korekta TABLE(ObowiazujacaLPunktow int, SemestrRealizacjiID int, KRokAkad int, KSemestrID int, Data datetime)
         INSERT @Korekta
         SELECT ObowiazujacaLPunktow,SemestrRealizacjiID,RokAkad, SemestrID, Data FROM KorektaLiczbyPunktowPoz
         JOIN KorektaLiczbyPunktow ON (KorektaLiczbyPunktowID=IDKorektaLiczbyPunktow AND KorektaLiczbyPunktow.IndeksID = @IndeksID)
         JOIN HistoriaStudenta ON (TabelaID=IDKorektaLiczbyPunktow AND ZdarzenieID=33 AND HistoriaStudenta.IndeksID = @IndeksID)
         WHERE SemestrRealizacjiID IN (SELECT IDSemReal FROM @TabelaPrzedmiotow) AND HistoriaStudenta.Anulowany = 0 
		 AND ((RokAkad*10 + SemestrID) BETWEEN (@RokOd * 10 + @SemestrOd) AND (@RokDo * 10 + @SemestrDo))

         UPDATE @TabelaObliczenia 
		 SET Punkty =
		 (
			SELECT TOP 1 ObowiazujacaLPunktow 
			FROM @Korekta 
            WHERE SemestrRealizacjiID = IDSemReal AND KRokAkad = RokAkad AND KSemestrID = SemestrID 
            ORDER BY Data DESC
		 )
         WHERE IDSemReal in (select SemestrRealizacjiID from @Korekta) 
      END

	  declare @SumaOceny float, @SumaIlosc int
	  select @SumaOceny = SUM(Ocena) from @TabelaObliczenia where CzyZalicza = 1 and CzyOcena = 1 and CzyNoweKarty = 0 and NumerWiersza = 1
	  select @SumaIlosc = Count(distinct IDSemReal) from @TabelaObliczenia where CzyZalicza = 1 and CzyOcena = 1 and CzyNoweKarty = 0

      declare @SumaOcenyN float, @SumaIloscN int
	  select @SumaOcenyN = SUM(OcenaCz) from @TabelaObliczenia where CzyNoweKarty = 1 and CzyZaliczaNowe = 1
	  select @SumaIloscN = Count(distinct FormaWymiarID) from @TabelaObliczenia where CzyNoweKarty = 1 and CzyZaliczaNowe = 1

	  set @Srednia = case when (@SumaIlosc + @SumaIloscN) > 0 then (coalesce(@SumaOceny,0)+ coalesce(@SumaOcenyN,0)) / (@SumaIlosc+ @SumaIloscN) else 0 end                
   END
  ELSE
   BEGIN
      SET @Srednia = NULL
   END

	
  RETURN ROUND(COALESCE(@Srednia,0),3)

END



GO


grant execute on dbo.KartaEgzSredniaArytmetyczna to [user]



--------------------------------------------------------wdro6-----------------------------------------------------


-- ******************* -- Raporty_GetTerminUkonczenia -- ******************* --


SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

ALTER function Raporty_GetTerminUkonczenia(@IDIndeks int, @IDUczelnia int)
returns varchar(50)
as 
begin

    declare @TerminUkonczenia varchar(50)
  declare @OkresRozliczeniowyID int, @RokAkad int
  declare @Aktual int
  declare @DataAktualna datetime
  select @DataAktualna = AktualnaData from dbo.AktualnaData
  select  @Aktual = dbo.DajAktualnySemestr(@DataAktualna)
  set @OkresRozliczeniowyID = @Aktual - (@Aktual/10)*10
  set @RokAkad = @Aktual/10

    declare @CzasTrwania int, @Semestr int
    declare @RokWyrownawczy int

    SELECT
    @CzasTrwania = LiczbaSemestrow, @RokWyrownawczy = RokWyrownawczy, @Semestr=Semestr
    FROM Student join Indeks on StudentID=IDStudent
    left join Studia on StudiaID = IDStudia
    left join SystemStudiow on Studia.SystemStudiowID = IDSystemStudiow
    WHERE IDIndeks=  @IDIndeks;


    declare @OkresRozlLiczba int
    SELECT @OkresRozlLiczba = OkresRozlLiczba FROM WSHE WHERE IDWSHE = @IDUczelnia

    declare @IleZostalo int

    if(@RokWyrownawczy <> 0 and @RokWyrownawczy is not null) 
      set @IleZostalo = @CzasTrwania +@OkresRozlLiczba - @Semestr
    else
      set @IleZostalo = @CzasTrwania - @Semestr;

    declare @deltaOkresRozl int, @deltaRok int
    set @deltaOkresRozl  = @IleZostalo % @OkresRozlLiczba;
    set @deltaRok        = @IleZostalo / @OkresRozlLiczba;

    declare @Lp int
    SELECT @Lp = Lp FROM OkresRozliczeniowyPoz WHERE IDOkresRozliczeniowyPoz = @OkresRozliczeniowyID

    if (@Lp +  @deltaOkresRozl >@OkresRozlLiczba )
       set @deltaRok = @deltaRok+1

    declare @LpUkonczenia int
    set @LpUkonczenia  = (@Lp + @deltaOkresRozl) %@OkresRozlLiczba
    if(@LpUkonczenia = 0)
      set @LpUkonczenia = @OkresRozlLiczba

   declare @DeltaRokDo int, @UkStudiowDzien int,@UkStudiowMiesiac int
   SELECT @UkStudiowDzien=UkStudiowDzien,@UkStudiowMiesiac=UkStudiowMiesiac,@DeltaRokDo=DeltaRokDo FROM OkresRozliczeniowyPoz
   WHERE UczelniaID = @IDUczelnia  AND  Lp = @LpUkonczenia

    declare @RokUkonczenia int
    set @RokUkonczenia =@RokAkad + @deltaRok +@DeltaRokDo

    set @TerminUkonczenia = cast(@RokUkonczenia as varchar) +'-'

    if(@UkStudiowMiesiac < 10)
      set @TerminUkonczenia = @TerminUkonczenia +'0'

    set  @TerminUkonczenia =@TerminUkonczenia + cast(@UkStudiowMiesiac as varchar) + '-'

    if (@UkStudiowMiesiac = 2) and (@UkStudiowDzien > 28) and ((@RokUkonczenia%4)<>0) -- korekta lat nieprzest�pnych
      set @UkStudiowDzien = 28

    if(@UkStudiowDzien < 10)
      set @TerminUkonczenia = @TerminUkonczenia +'0'
    set @TerminUkonczenia = @TerminUkonczenia + cast(@UkStudiowDzien as varchar)

   return @TerminUkonczenia

end

go










--------------------------------------------------------wdro7-----------------------------------------------------


-- ******************* -- 2018_01_24_02_ESS_WPD_USR_Profil_tab -- ******************* --



IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__USR_Profi__CzyKo__3ED4C61E]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_Profil]'))
ALTER TABLE [dbo].[USR_Profil] DROP CONSTRAINT [DF__USR_Profi__CzyKo__3ED4C61E]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__USR_Histo__USR_P__6FE211CD]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_HistoriaAkcji]'))
ALTER TABLE [dbo].[USR_HistoriaAkcji] DROP CONSTRAINT [FK__USR_Histo__USR_P__6FE211CD]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__USR_Profi__USR_P__6B1D5CB0]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_ProfilToRola]'))
ALTER TABLE [dbo].[USR_ProfilToRola] DROP CONSTRAINT [FK__USR_Profi__USR_P__6B1D5CB0]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_USR_HistoriaHasla_USR_Profil]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_HistoriaHasla]'))
ALTER TABLE [dbo].[USR_HistoriaHasla] DROP CONSTRAINT [FK_USR_HistoriaHasla_USR_Profil]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[USR_Profil]') AND name = N'PK_USR_Profil')
ALTER TABLE [dbo].[USR_Profil] DROP CONSTRAINT [PK_USR_Profil]

--------------------------------------
----------------USR_Profil---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[USR_Profil]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[USR_Profil]
GO

CREATE TABLE [dbo].[USR_Profil] (
   [IDUSR_Profil] [int]  IDENTITY(1,1) NOT NULL
  ,[Email] [varchar] (254) NULL
  ,[Login] [varchar] (50) NULL
  ,[HasloPlain] [varchar] (128) NULL
  ,[HasloHash] [varchar] (160) NULL
  ,[ZiarnoHasla] [varchar] (32) NULL
  ,[Imie] [varchar] (50) NOT NULL
  ,[DrugieImie] [varchar] (50) NULL
  ,[NickName] [varchar] (50) NULL
  ,[Nazwisko] [varchar] (50) NOT NULL
  ,[PrefiksTelefonu] [varchar] (5) NULL
  ,[Telefon] [varchar] (20) NULL
  ,[CzyHasloJednorazowe] [bit]  NULL
  ,[CzyKontoAktywne] [bit]  NOT NULL
  ,[DataOstatniejZmianyHasla] [datetime]  NULL
  ,[Token] [uniqueidentifier]  NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO



ALTER TABLE [dbo].[USR_Profil] ADD  CONSTRAINT [PK_USR_Profil] PRIMARY KEY CLUSTERED( IDUSR_Profil ) ON [PRIMARY]

ALTER TABLE [dbo].[USR_Profil] ADD  CONSTRAINT [DF__USR_Profi__CzyKo__3ED4C61E]  DEFAULT ((0)) FOR [CzyKontoAktywne]
GO





--------------------------------------------------------wdro8-----------------------------------------------------


-- ******************* -- 2018_01_24_05_ESS_WPD_USR_Rola_tab -- ******************* --

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__USR_Profi__USR_R__6C1180E9]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_ProfilToRola]'))
ALTER TABLE [dbo].[USR_ProfilToRola] DROP CONSTRAINT [FK__USR_Profi__USR_R__6C1180E9]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[USR_Rola]') AND name = N'PK_USR_Rola')
ALTER TABLE [dbo].[USR_Rola] DROP CONSTRAINT [PK_USR_Rola]


--------------------------------------
----------------USR_Rola---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[USR_Rola]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[USR_Rola]
GO

CREATE TABLE [dbo].[USR_Rola] (
   [IDUSR_Rola] [int]  IDENTITY(1,1) NOT NULL
  ,[Nazwa] [varchar] (128) NOT NULL
  ,[Podmiot] [varchar] (128) NULL
  ,[Klucz] [varchar] (128) NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[USR_Rola] ADD  CONSTRAINT [PK_USR_Rola] PRIMARY KEY CLUSTERED( IDUSR_Rola ) ON [PRIMARY]

GO



--------------------------------------------------------wdro9-----------------------------------------------------


-- ******************* -- 2018_01_24_09_ESS_WPD_USR_ProfiToRola_tab -- ******************* --



IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__USR_Profi__Aktyw__40BD0E90]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_ProfilToRola]'))
ALTER TABLE [dbo].[USR_ProfilToRola] DROP CONSTRAINT [DF__USR_Profi__Aktyw__40BD0E90]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__USR_Profi__USR_P__6B1D5CB0]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_ProfilToRola]'))
ALTER TABLE [dbo].[USR_ProfilToRola] DROP CONSTRAINT [FK__USR_Profi__USR_P__6B1D5CB0]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__USR_Profi__USR_R__6C1180E9]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_ProfilToRola]'))
ALTER TABLE [dbo].[USR_ProfilToRola] DROP CONSTRAINT [FK__USR_Profi__USR_R__6C1180E9]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[USR_ProfilToRola]') AND name = N'PK_USR_ProfilToRola')
ALTER TABLE [dbo].[USR_ProfilToRola] DROP CONSTRAINT [PK_USR_ProfilToRola]



--------------------------------------
----------------USR_ProfilToRola---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[USR_ProfilToRola]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[USR_ProfilToRola]
GO

CREATE TABLE [dbo].[USR_ProfilToRola] (
   [IDUSR_ProfilToRola] [int]  IDENTITY(1,1) NOT NULL
  ,[USR_ProfilID] [int]  NOT NULL
  ,[USR_RolaID] [int]  NOT NULL
  ,[KluczZdalny] [int]  NULL
  ,[Aktywny] [bit]  NOT NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO



ALTER TABLE [dbo].[USR_ProfilToRola] ADD  CONSTRAINT [PK_USR_ProfilToRola] PRIMARY KEY CLUSTERED( IDUSR_ProfilToRola ) ON [PRIMARY]

ALTER TABLE [dbo].[USR_ProfilToRola] WITH NOCHECK ADD CONSTRAINT [FK__USR_Profi__USR_R__6C1180E9] FOREIGN KEY([USR_RolaID]) REFERENCES [dbo].[USR_Rola] ([IDUSR_Rola])

ALTER TABLE [dbo].[USR_ProfilToRola] WITH NOCHECK ADD CONSTRAINT [FK__USR_Profi__USR_P__6B1D5CB0] FOREIGN KEY([USR_ProfilID]) REFERENCES [dbo].[USR_Profil] ([IDUSR_Profil])

ALTER TABLE [dbo].[USR_ProfilToRola] ADD  CONSTRAINT [DF__USR_Profi__Aktyw__40BD0E90]  DEFAULT ((1)) FOR [Aktywny]
GO


--------------------------------------------------------wdro10-----------------------------------------------------


-- ******************* -- 2018_01_24_12_ESS_WPD_USR_HistoriaLogowan_tab -- ******************* --




IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[USR_HistoriaLogowan]') AND name = N'PK_USR_HistoriaLogowan')
ALTER TABLE [dbo].[USR_HistoriaLogowan] DROP CONSTRAINT [PK_USR_HistoriaLogowan]

--------------------------------------
----------------USR_HistoriaLogowan---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[USR_HistoriaLogowan]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[USR_HistoriaLogowan]
GO

CREATE TABLE [dbo].[USR_HistoriaLogowan] (
   [IDUSR_HistoriaLogowan] [int]  IDENTITY(1,1) NOT NULL
  ,[Login] [varchar] (50) NOT NULL
  ,[AdresZdalny] [varchar] (50) NULL
  ,[Powodzenie] [bit]  NOT NULL
  ,[DataAkcji] [datetime]  NOT NULL
  ,[UserAgent] [varchar] (256) NULL
  ,[SessionToken] [varchar] (32) NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DadaModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[USR_HistoriaLogowan] ADD  CONSTRAINT [PK_USR_HistoriaLogowan] PRIMARY KEY CLUSTERED( IDUSR_HistoriaLogowan ) ON [PRIMARY]
GO



--------------------------------------------------------wdro11-----------------------------------------------------


-- ******************* -- 2018_01_24_15_ESS_WPD_USR_HistoriaHasla_tab -- ******************* --



IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_USR_HistoriaHasla_USR_Profil]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_HistoriaHasla]'))
ALTER TABLE [dbo].[USR_HistoriaHasla] DROP CONSTRAINT [FK_USR_HistoriaHasla_USR_Profil]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[USR_HistoriaHasla]') AND name = N'PK_USR_HistoriaHasla')
ALTER TABLE [dbo].[USR_HistoriaHasla] DROP CONSTRAINT [PK_USR_HistoriaHasla]

--------------------------------------
----------------USR_HistoriaHasla---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[USR_HistoriaHasla]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[USR_HistoriaHasla]
GO

CREATE TABLE [dbo].[USR_HistoriaHasla] (
   [IDUSR_HistoriaHasla] [int]  IDENTITY(1,1) NOT NULL
  ,[USR_ProfilID] [int]  NULL
  ,[HasloPlain] [varchar] (128) NOT NULL
  ,[HasloHash] [varchar] (160) NOT NULL
  ,[ZiarnoHasla] [varchar] (32) NOT NULL
  ,[IP] [varchar] (39) NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO



ALTER TABLE [dbo].[USR_HistoriaHasla] ADD  CONSTRAINT [PK_USR_HistoriaHasla] PRIMARY KEY CLUSTERED( IDUSR_HistoriaHasla ) ON [PRIMARY]

ALTER TABLE [dbo].[USR_HistoriaHasla] WITH NOCHECK ADD CONSTRAINT [FK_USR_HistoriaHasla_USR_Profil] FOREIGN KEY([USR_ProfilID]) REFERENCES [dbo].[USR_Profil] ([IDUSR_Profil])
GO



--------------------------------------------------------wdro12-----------------------------------------------------


-- ******************* -- 2018_01_24_18_ESS_WPD_USR_HistoriaAkcji_tab -- ******************* --



IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__USR_Histo__USR_P__6FE211CD]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_HistoriaAkcji]'))
ALTER TABLE [dbo].[USR_HistoriaAkcji] DROP CONSTRAINT [FK__USR_Histo__USR_P__6FE211CD]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[USR_HistoriaAkcji]') AND name = N'PK_USR_HistoriaAkcji')
ALTER TABLE [dbo].[USR_HistoriaAkcji] DROP CONSTRAINT [PK_USR_HistoriaAkcji]

--------------------------------------
----------------USR_HistoriaAkcji---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[USR_HistoriaAkcji]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[USR_HistoriaAkcji]
GO

CREATE TABLE [dbo].[USR_HistoriaAkcji] (
   [IDUSR_HistoriaAkcji] [int]  IDENTITY(1,1) NOT NULL
  ,[Uri] [varchar] (256) NOT NULL
  ,[DaneWejsciowe] [text]  NULL
  ,[DaneWyjsciowe] [text]  NULL
  ,[USR_ProfilID] [int]  NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[USR_HistoriaAkcji] ADD  CONSTRAINT [PK_USR_HistoriaAkcji] PRIMARY KEY CLUSTERED( IDUSR_HistoriaAkcji ) ON [PRIMARY]

ALTER TABLE [dbo].[USR_HistoriaAkcji] WITH NOCHECK ADD CONSTRAINT [FK__USR_Histo__USR_P__6FE211CD] FOREIGN KEY([USR_ProfilID]) REFERENCES [dbo].[USR_Profil] ([IDUSR_Profil])


GO




--------------------------------------------------------wdro13-----------------------------------------------------


-- ******************* -- 2018_01_26_01_ESS_WPD_WPWPlanySzczegolowe_prc -- ******************* --


-- ******************* -- WPWPlanySzczegolowe -- ******************* --

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WPWPlanySzczegolowe]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WPWPlanySzczegolowe]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[WPWPlanySzczegolowe] @pracownikID INT, @DataOD DATE, @DataDO DATE, @SprawdzAktywnosc BIT
AS
select distinct 
  MAX([gz].[IDGrupaZajeciowa]) as [IDGrupaZajeciowa]
, [IDPakietZaj]
, DATEADD(minute, [ppd].[OdGodz], [ppd].[Data]) AS [DataOD]
, DATEADD(minute, [ppd].[DoGodz], [ppd].[Data]) AS [DataDO]
, [B].[Symbol] + [S].[Numer] AS [Sala]
, [B].[Adres] AS [Adres]
, CASE
	WHEN [FW].[NazwaFormy] IS NOT NULL THEN min([P].[Nazwa]) + ': ' + [FW].[NazwaFormy]
	WHEN [FW].[NazwaFormy] IS NULL THEN min([P].[Nazwa])
end as [PNazwa]
, [fz].[Symbol6Znakow] as [TypZajecSkrot]
, [fz].[Nazwa] as [TypZajec]
from
			[PlanPozDef] [ppd]
JOIN		[PlanZajecPoz] [pzp]		on [ppd].[PlanZajecPozIDNr] = [pzp].[IDPlanZajecPoz]
LEFT JOIN	[GrupaZajeciowa] [gz]		on [pzp].[GrupaZajeciowaID] = [gz].[IDGrupaZajeciowa]
JOIN		[FormaWymiar] [fw]			on [gz].[FormaWymiarID] = [fw].[IDFormaWymiar]
JOIN		[SemestrRealizacji] [SR]	on [SR].[IDSemestrRealizacji] = [FW].[SemestrRealizacjiID]
JOIN		[KartaUzup] [KU]			on [KU].[IDKartaUzup] = [SR].[KartaUzupID]
JOIN		[Przedmiot] [P]				on [P].[IDPrzedmiot] = [KU].[PrzedmiotID]
LEFT JOIN	[PlanZajec] [pz]			on [ppd].[PlanZajecPozIDNr] = [pz].[IDPlanZajec]
JOIN		[Sala] [S]					on [S].[IDSala] = [ppd].[SalaIDNr]
JOIN		[Budynek] [B]				on [B].[IDBudynek] = [S].[BudynekID]
LEFT JOIN	[PakietZaj] [pakiet]		on [ppd].[PakietIDNr] = [pakiet].[IDPakietZaj]
LEFT JOIN	[FormaZajec] [FZ]			on [FZ].[IDFormaZajec] = [FW].[FormaZajecID]
where 
[ppd].[PracownikIDNr] = @pracownikID
and  (@SprawdzAktywnosc = 0 OR ([pz].[StatusAktywny] = 1 OR [pz].[StatusAktywny] is null))
and [ppd].Data >= @DataOD
and ppd.Data <= @DataDO 
group by [IDPakietZaj], [ppd].[Data], [ppd].[OdGodz], [ppd].[DoGodz], [FW].[NazwaFormy], [B].[Symbol], [S].[Numer], [B].[Adres], [FZ].[Symbol6Znakow], [Fz].[Nazwa]
order by [DataOd] desc

GO



--------------------------------------------------------wdro14-----------------------------------------------------


-- ******************* -- 2018_01_26_02_ESS_WPD_WPWPlanySemestralny_prc -- ******************* --



-- ******************* -- WPWPlanySemestralny -- ******************* --

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WPWPlanySemestralny]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WPWPlanySemestralny]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[WPWPlanySemestralny] @pracownikID INT, @Sem INT, @Rok INT
AS


declare @table table(id int, liczbaKier int, liczbaSpec int, kier int, spec int)
  declare @TabDaty table (id int identity(8,1), Daty varchar(1000))

	insert into @table
		select distinct idplanzajecpoz,count( distinct spec.kierunekid), count(distinct SpecjalnoscID), max(spec.kierunekID), max(specjalnoscID)
		from PlanZajecPoz pzp
 	  join GrupaZajeciowa gz on pzp.GrupaZajeciowaID = IDgrupaZajeciowa
		left join PlanPozPracownik ppp on pzp.IDplanZajecPoz = ppp.planZajecpozid
	  join PlanZajec pz on pz.IDplanZajec = gz.PlanZajecID or
		pz.IDplanZajec in (select PlanZajecID from ZajeciaWspolnePoz where ZajeciaWspolneID = gz.ZajeciaWspolneID)
		join siatka on siatkaid = idsiatka
		join program on programid = idprogram
		join Specjalnosc spec on specjalnoscid = idspecjalnosc

		where ppp.pracownikid = @PracownikID
		and pz.RokAkad = @Rok
		and pz.SemestrID = @Sem
		group by idplanzajecpoz

   insert into @TabDaty
    select dbo.DatyZKalendarza(idplanzajecpoz)
    from PlanZajecPoz
    join @table tab on idplanzajecpoz = tab.id
    join zajeciaStatus on ZajeciaStatusID = IDZajeciaStatus 
    where CzyKalendarz = 1

		select
			distinct
			IDPlanZajecPoz,
			gz.IDGrupaZajeciowa,
			gz.FormaWymiarID,
      pz.SemestrStudiow Sem,
			prz.Nazwa + coalesce(' - ' + nullif(fw.NazwaFormy,''),'') PrzedmiotNazwa,
			fz.Symbol6Znakow FormaZajec,
			sr.Numer as SRNumer,
			coalesce(bud.Symbol + ' ' + cast(sala.Numer as varchar(6)),'<bez sali>') Gdzie,
			dbo.FormatCzas(OdGodz) OdGodziny,
			dbo.FormatCzas(OdGodz + LGodz*pz.CzasZajec + CzasPrzerw) +
			case when zs.CoXtyg = 2 and CzyKalendarz = 0 then case when zs.NzX = 1 then +' (*)' else +' (**)' end else '' end as DoGodziny,
      odGodz as OdMinuty,
			case when zs.CzyKalendarz = 0 then dt.Nazwa else dbo.DatyZKalendarza(pzp.IDPlanZajecPoz) end Kiedy,
			case when zs.CzyKalendarz = 0 then 1+(6+(pzp.dzienTygodniaID-1)%7)%7 else
        ( select top 1 ID from @tabDaty where Daty = dbo.DatyZKalendarza(pzp.IDPlanZajecPoz) ) end DzienID,

			prz.Kod + ' - ' +ku.koduzupelniajacy Kod,

			case when ( select liczbaKier from @table where id = pzp.idplanzajecpoz ) = 1 then
			(
				select '<'+rtrim(w.Symbol)+'> '+k.Symbol from Kierunek   k
        join Wydzial w on WydzialID = IDWydzial
				where IDkierunek = (select kier from @table where id = pzp.idplanzajecpoz )

			)
			else
			 '' end KierSymbol,

			case when ( select LiczbaSpec from @table where id = pzp.idplanzajecpoz	) = 1 then
			(
				select '<' +ss.Symbol + '> ' +spec.Nazwa from Specjalnosc spec

				join SystemStudiow SS on SystemStudiowID = IDSystemStudiow
				where IDSpecjalnosc = (select spec from @table where id = pzp.idplanzajecpoz )
			)
			else '' end SpecNazwa,

			tryb.symbol as TrybSymbol,
	    dbo.GrupyStudenckieStr(pzp.GrupaZajeciowaID) Grupy,
      Zjazd.Nazwa as ZjazdNazwa,
      Sekcja.Nazwa as SekcjaNazwa,
      Sekcja.Symbol as SekcjaSymbol
      
		from PlanZajecPoz pzp
 	  join GrupaZajeciowa gz on pzp.GrupaZajeciowaID = IDgrupaZajeciowa
	  join ZajeciaStatus zs on ZajeciaStatusid = idZajeciaStatus

		-- do grupy
		left join PlanPozGrupa ppg on ppg.GrupaZajeciowaId = gz.IdGrupazajeciowa

		-- rodzaj studi�w
	  join PlanZajec pz on pz.IDplanZajec = gz.PlanZajecID or
		pz.IDplanZajec in (select PlanZajecID from ZajeciaWspolnePoz where ZajeciaWspolneID = gz.ZajeciaWspolneID)
		join siatka on siatkaID = IDSiatka
		join program pr on siatka.programid = idprogram
 	  join TrybStudiow tryb on siatka.TrybstudiowID = IDtrybStudiow
    join Zjazd on pz.ZjazdID = IDZjazd
    join Sekcja on SekcjaID = IDSekcja

		-- do przedmiotu
		left join FormaWymiar fw on FormaWymiarID = IDformaWymiar
		left join SemestrRealizacji sr on IDSemestrRealizacji = fw.SemestrRealizacjiID
		left join KartaUzup ku on sr.KartaUzupID = IDkartaUzup
		left join Przedmiot prz on ku.PrzedmiotID = IDprzedmiot
		left join FormaZajec fz on fw.FormaZajecID = IDFormaZajec

		-- do sali
		left join Sala on pzp.SalaID = IDsala
		left join Budynek bud on budynekid = idbudynek

		-- dzienTygodnia
		left join DzienTygodnia dt on pzp.DzienTygodniaID = IDDzienTygodnia

		where pzp.idPlanZajecPoz in (select ID from @Table)

    order by DzienID, odGodz
	

GO



--------------------------------------------------------wdro15-----------------------------------------------------


-- ******************* -- 2018_01_29_01_ESS_WPD_WPWPlanySzczegolowe_prc -- ******************* --




-- ******************* -- WPWPlanySzczegolowe -- ******************* --

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WPWPlanySzczegolowe]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WPWPlanySzczegolowe]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[WPWPlanySzczegolowe] @pracownikID INT, @DataOD DATE, @DataDO DATE, @SprawdzAktywnosc BIT
AS
select distinct 
  [gz].[IDGrupaZajeciowa]
, DATEADD(minute, [ppd].[OdGodz], [ppd].[Data]) AS [DataOD]
, DATEADD(minute, [ppd].[DoGodz], [ppd].[Data]) AS [DataDO]
, [B].[Symbol] + [S].[Numer] AS [Sala]
, [B].[Adres] AS [Adres]
, CASE
	WHEN [FW].[NazwaFormy] IS NOT NULL THEN [P].[Nazwa] + ': ' + [FW].[NazwaFormy]
	WHEN [FW].[NazwaFormy] IS NULL THEN [P].[Nazwa]
end as [PNazwa]
, [fz].[Symbol6Znakow] as [TypZajecSkrot]
, [fz].[Nazwa] as [TypZajec]
from
			[PlanPozDef] [ppd]
JOIN		[PlanZajecPoz] [pzp]		on [ppd].[PlanZajecPozIDNr] = [pzp].[IDPlanZajecPoz]
LEFT JOIN	[GrupaZajeciowa] [gz]		on [pzp].[GrupaZajeciowaID] = [gz].[IDGrupaZajeciowa]
JOIN		[FormaWymiar] [fw]			on [gz].[FormaWymiarID] = [fw].[IDFormaWymiar]
JOIN		[SemestrRealizacji] [SR]	on [SR].[IDSemestrRealizacji] = [FW].[SemestrRealizacjiID]
JOIN		[KartaUzup] [KU]			on [KU].[IDKartaUzup] = [SR].[KartaUzupID]
JOIN		[Przedmiot] [P]				on [P].[IDPrzedmiot] = [KU].[PrzedmiotID]
LEFT JOIN	[PlanZajec] [pz]			on [ppd].[PlanZajecPozIDNr] = [pz].[IDPlanZajec]
JOIN		[Sala] [S]					on [S].[IDSala] = [ppd].[SalaIDNr]
JOIN		[Budynek] [B]				on [B].[IDBudynek] = [S].[BudynekID]
LEFT JOIN	[PakietZaj] [pakiet]		on [ppd].[PakietIDNr] = [pakiet].[IDPakietZaj]
LEFT JOIN	[FormaZajec] [FZ]			on [FZ].[IDFormaZajec] = [FW].[FormaZajecID]
where 
[ppd].[PracownikIDNr] = @pracownikID
and  (@SprawdzAktywnosc = 0 OR ([pz].[StatusAktywny] = 1 OR [pz].[StatusAktywny] is null))
and [ppd].Data >= @DataOD
and ppd.Data <= @DataDO 
group by [gz].[IDGrupaZajeciowa], [P].[Nazwa], [ppd].[Data], [ppd].[OdGodz], [ppd].[DoGodz], [FW].[NazwaFormy], [B].[Symbol], [S].[Numer], [B].[Adres], [FZ].[Symbol6Znakow], [Fz].[Nazwa]
order by [DataOd] desc



GO



