IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_USR_UprawnianiaToRola_USR_UprawnianiaDefinicje]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_UprawnianiaToRola]'))
ALTER TABLE [dbo].[USR_UprawnianiaToRola] DROP CONSTRAINT [FK_USR_UprawnianiaToRola_USR_UprawnianiaDefinicje]

--------------------------------------
----------------USR_UprawnianiaDefinicje---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[USR_UprawnianiaDefinicje]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[USR_UprawnianiaDefinicje]
GO

CREATE TABLE [dbo].[USR_UprawnianiaDefinicje] (
   [IDUSR_UprawnianiaDefinicje] [int]  IDENTITY(1,1) NOT NULL
  ,[Adres] [varchar] (250) NOT NULL
  ,[Opis] [text]  NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO



ALTER TABLE [dbo].[USR_UprawnianiaDefinicje] ADD  CONSTRAINT [PK_USR_UprawnianiaDefinicje] PRIMARY KEY CLUSTERED( IDUSR_UprawnianiaDefinicje ) ON [PRIMARY]


