DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------USR_Rola-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_Rola_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER USR_Rola_INSERT
')

EXEC('
CREATE TRIGGER USR_Rola_INSERT ON ['+@BazaDanych+'].[dbo].[USR_Rola] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_Rola'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_Rola]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_Rola]
  ,[Org_Nazwa]
  ,[Org_Podmiot]
  ,[Org_Klucz]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDUSR_Rola
 ,ins.Nazwa
 ,ins.Podmiot
 ,ins.Klucz
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_Rola_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER USR_Rola_UPDATE
')

EXEC('
CREATE TRIGGER USR_Rola_UPDATE ON ['+@BazaDanych+'].[dbo].[USR_Rola] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_Rola'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_Rola]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_Rola]
  ,[Org_Nazwa]
  ,[Org_Podmiot]
  ,[Org_Klucz]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDUSR_Rola
 ,ins.Nazwa
 ,ins.Podmiot
 ,ins.Klucz
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_Rola_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER USR_Rola_DELETE
')

EXEC('
CREATE TRIGGER USR_Rola_DELETE ON ['+@BazaDanych+'].[dbo].[USR_Rola] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_Rola'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_Rola]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_Rola]
  ,[Org_Nazwa]
  ,[Org_Podmiot]
  ,[Org_Klucz]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDUSR_Rola
 ,del.Nazwa
 ,del.Podmiot
 ,del.Klucz
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'USR_Rola')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('USR_Rola', 1, 0,GetDate())

GO