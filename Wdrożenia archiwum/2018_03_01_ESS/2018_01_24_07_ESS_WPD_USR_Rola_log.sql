
-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------USR_Profil---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_USR_Profil]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_USR_Profil] (
   [Log_USR_ProfilID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_Profil] [int] 
  ,[Org_Email] [varchar] (254)
  ,[Org_Login] [varchar] (50)
  ,[Org_HasloPlain] [varchar] (128)
  ,[Org_HasloHash] [varchar] (160)
  ,[Org_ZiarnoHasla] [varchar] (32)
  ,[Org_Imie] [varchar] (50)
  ,[Org_DrugieImie] [varchar] (50)
  ,[Org_NickName] [varchar] (50)
  ,[Org_Nazwisko] [varchar] (50)
  ,[Org_PrefiksTelefonu] [varchar] (5)
  ,[Org_Telefon] [varchar] (20)
  ,[Org_CzyHasloJednorazowe] [bit] 
  ,[Org_CzyKontoAktywne] [bit] 
  ,[Org_DataOstatniejZmianyHasla] [datetime] 
  ,[Org_Token] [uniqueidentifier] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_USR_Profil] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_USR_Profil] PRIMARY KEY  CLUSTERED
(
   [Log_USR_ProfilID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_USR_Profil] (
   [Log_USR_ProfilID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_Profil] [int] 
  ,[Org_Email] [varchar] (254)
  ,[Org_Login] [varchar] (50)
  ,[Org_HasloPlain] [varchar] (128)
  ,[Org_HasloHash] [varchar] (160)
  ,[Org_ZiarnoHasla] [varchar] (32)
  ,[Org_Imie] [varchar] (50)
  ,[Org_DrugieImie] [varchar] (50)
  ,[Org_NickName] [varchar] (50)
  ,[Org_Nazwisko] [varchar] (50)
  ,[Org_PrefiksTelefonu] [varchar] (5)
  ,[Org_Telefon] [varchar] (20)
  ,[Org_CzyHasloJednorazowe] [bit] 
  ,[Org_CzyKontoAktywne] [bit] 
  ,[Org_DataOstatniejZmianyHasla] [datetime] 
  ,[Org_Token] [uniqueidentifier] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_Profil ON

IF EXISTS (SELECT * FROM dbo.LOG_USR_Profil)
EXEC('INSERT INTO dbo.Tmp_LOG_USR_Profil (Log_USR_ProfilID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_Profil, Org_Email, Org_Login, Org_HasloPlain, Org_HasloHash, Org_ZiarnoHasla, Org_Imie, Org_DrugieImie, Org_NickName, Org_Nazwisko, Org_PrefiksTelefonu, Org_Telefon, Org_CzyHasloJednorazowe, Org_CzyKontoAktywne, Org_DataOstatniejZmianyHasla, Org_Token, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_USR_ProfilID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_Profil, Org_Email, Org_Login, Org_HasloPlain, Org_HasloHash, Org_ZiarnoHasla, Org_Imie, Org_DrugieImie, Org_NickName, Org_Nazwisko, Org_PrefiksTelefonu, Org_Telefon, Org_CzyHasloJednorazowe, Org_CzyKontoAktywne, Org_DataOstatniejZmianyHasla, Org_Token, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_USR_Profil WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_Profil OFF

DROP TABLE dbo.LOG_USR_Profil

EXECUTE sp_rename N'dbo.Tmp_LOG_USR_Profil', N'LOG_USR_Profil', 'OBJECT'

ALTER TABLE [dbo].[LOG_USR_Profil] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_USR_Profil] PRIMARY KEY  CLUSTERED
(
   [Log_USR_ProfilID]
) ON [PRIMARY]

END
GO


-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------USR_Rola---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_USR_Rola]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_USR_Rola] (
   [Log_USR_RolaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_Rola] [int] 
  ,[Org_Nazwa] [varchar] (128)
  ,[Org_Podmiot] [varchar] (128)
  ,[Org_Klucz] [varchar] (128)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_USR_Rola] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_USR_Rola] PRIMARY KEY  CLUSTERED
(
   [Log_USR_RolaID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_USR_Rola] (
   [Log_USR_RolaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_Rola] [int] 
  ,[Org_Nazwa] [varchar] (128)
  ,[Org_Podmiot] [varchar] (128)
  ,[Org_Klucz] [varchar] (128)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_Rola ON

IF EXISTS (SELECT * FROM dbo.LOG_USR_Rola)
EXEC('INSERT INTO dbo.Tmp_LOG_USR_Rola (Log_USR_RolaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_Rola, Org_Nazwa, Org_Podmiot, Org_Klucz, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_USR_RolaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_Rola, Org_Nazwa, Org_Podmiot, Org_Klucz, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_USR_Rola WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_Rola OFF

DROP TABLE dbo.LOG_USR_Rola

EXECUTE sp_rename N'dbo.Tmp_LOG_USR_Rola', N'LOG_USR_Rola', 'OBJECT'

ALTER TABLE [dbo].[LOG_USR_Rola] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_USR_Rola] PRIMARY KEY  CLUSTERED
(
   [Log_USR_RolaID]
) ON [PRIMARY]

END
GO

