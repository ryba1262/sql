-- =============================================
-- Author:		Agata Stelcer-Matuszczak
-- Create date: 2018-02-05
-- Description:	Wypelnienie kolumny Kolor danymi
-- =============================================

  UPDATE FormaZajec  SET Kolor='#ADEAEA'  where IDFormaZajec=1
  UPDATE FormaZajec  SET Kolor='#D8BFD8'  where IDFormaZajec=2
  UPDATE FormaZajec  SET Kolor='#DB9370'  where IDFormaZajec=3
  UPDATE FormaZajec  SET Kolor='#D9D9F3'  where IDFormaZajec=4
  UPDATE FormaZajec  SET Kolor='#EAEAAE'  where IDFormaZajec=5
  UPDATE FormaZajec  SET Kolor='#cfcfcf'  where IDFormaZajec=6
  UPDATE FormaZajec  SET Kolor='#ffcc99'  where IDFormaZajec=7
  UPDATE FormaZajec  SET Kolor='#ff6666'  where IDFormaZajec=8
  UPDATE FormaZajec  SET Kolor='#cc9999'  where IDFormaZajec=9
  UPDATE FormaZajec  SET Kolor='#ff9966'  where IDFormaZajec=10
  UPDATE FormaZajec  SET Kolor='#ff9933'  where IDFormaZajec=11
  UPDATE FormaZajec  SET Kolor='#cccc99'  where IDFormaZajec=12
  UPDATE FormaZajec  SET Kolor='#cccc66'  where IDFormaZajec=14
  UPDATE FormaZajec  SET Kolor='#cccc33'  where IDFormaZajec=15
  UPDATE FormaZajec  SET Kolor='#ccff99'  where IDFormaZajec=16
  UPDATE FormaZajec  SET Kolor='#99ffcc'  where IDFormaZajec=17
  UPDATE FormaZajec  SET Kolor='#66ffcc'  where IDFormaZajec=18
  UPDATE FormaZajec  SET Kolor='#99cccc'  where IDFormaZajec=19
  UPDATE FormaZajec  SET Kolor='#00ccff'  where IDFormaZajec=20
  UPDATE FormaZajec  SET Kolor='#66ccff'  where IDFormaZajec=22
  UPDATE FormaZajec  SET Kolor='#6699cc'  where IDFormaZajec=23
  UPDATE FormaZajec  SET Kolor='#9999cc'  where IDFormaZajec=26
  UPDATE FormaZajec  SET Kolor='#9999ff'  where IDFormaZajec=27
  UPDATE FormaZajec  SET Kolor='#ccccff'  where IDFormaZajec=32
  UPDATE FormaZajec  SET Kolor='#cc66ff'  where IDFormaZajec=33
  UPDATE FormaZajec  SET Kolor='#cc33ff'  where IDFormaZajec=34
  UPDATE FormaZajec  SET Kolor='#ff9999'  where IDFormaZajec=36
  UPDATE FormaZajec  SET Kolor='#ff99ff'  where IDFormaZajec=37
  UPDATE FormaZajec  SET Kolor='#ffccff'  where IDFormaZajec=38
  UPDATE FormaZajec  SET Kolor='#ffffcc'  where IDFormaZajec=39
  UPDATE FormaZajec  SET Kolor='#999966'  where IDFormaZajec=40
  UPDATE FormaZajec  SET Kolor='#F0CFCC'  where IDFormaZajec=41
  UPDATE FormaZajec  SET Kolor='#E38981'  where IDFormaZajec=42
  UPDATE FormaZajec  SET Kolor='#E3C481'  where IDFormaZajec=43
  UPDATE FormaZajec  SET Kolor='#81E3C6'  where IDFormaZajec=44
  UPDATE FormaZajec  SET Kolor='#9CA8FF'  where IDFormaZajec=45
  UPDATE FormaZajec  SET Kolor='#8795FF'  where IDFormaZajec=46
  UPDATE FormaZajec  SET Kolor='#87F7FF'  where IDFormaZajec=47
  UPDATE FormaZajec  SET Kolor='#FFC7D5'  where IDFormaZajec=48
  UPDATE FormaZajec  SET Kolor='#FDC7FF'  where IDFormaZajec=49
  UPDATE FormaZajec  SET Kolor='#C7FFDF'  where IDFormaZajec=50
  UPDATE FormaZajec  SET Kolor='#FFF9C7'  where IDFormaZajec=51