
-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------ZajeciaTypUczestnictwa---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_ZajeciaTypUczestnictwa]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_ZajeciaTypUczestnictwa] (
   [Log_ZajeciaTypUczestnictwaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDZajeciaTypUczestnictwa] [int] 
  ,[Org_TypUczestnictwaID] [int] 
  ,[Org_Podmiot] [varchar] (100)
  ,[Org_PodmiotID] [int] 
  ,[Org_FormaWymiarID] [int] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_ZajeciaTypUczestnictwa] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_ZajeciaTypUczestnictwa] PRIMARY KEY  CLUSTERED
(
   [Log_ZajeciaTypUczestnictwaID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_ZajeciaTypUczestnictwa] (
   [Log_ZajeciaTypUczestnictwaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDZajeciaTypUczestnictwa] [int] 
  ,[Org_TypUczestnictwaID] [int] 
  ,[Org_Podmiot] [varchar] (100)
  ,[Org_PodmiotID] [int] 
  ,[Org_FormaWymiarID] [int] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_ZajeciaTypUczestnictwa ON

IF EXISTS (SELECT * FROM dbo.LOG_ZajeciaTypUczestnictwa)
EXEC('INSERT INTO dbo.Tmp_LOG_ZajeciaTypUczestnictwa (Log_ZajeciaTypUczestnictwaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDZajeciaTypUczestnictwa, Org_TypUczestnictwaID, Org_Podmiot, Org_PodmiotID, Org_FormaWymiarID, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_ZajeciaTypUczestnictwaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDZajeciaTypUczestnictwa, Org_TypUczestnictwaID, Org_Podmiot, Org_PodmiotID, Org_FormaWymiarID, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_ZajeciaTypUczestnictwa WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_ZajeciaTypUczestnictwa OFF

DROP TABLE dbo.LOG_ZajeciaTypUczestnictwa

EXECUTE sp_rename N'dbo.Tmp_LOG_ZajeciaTypUczestnictwa', N'LOG_ZajeciaTypUczestnictwa', 'OBJECT'

ALTER TABLE [dbo].[LOG_ZajeciaTypUczestnictwa] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_ZajeciaTypUczestnictwa] PRIMARY KEY  CLUSTERED
(
   [Log_ZajeciaTypUczestnictwaID]
) ON [PRIMARY]

END
GO

