IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ZajeciaTypUczestnictwa_FormaWymiar]') AND parent_object_id = OBJECT_ID(N'[dbo].[ZajeciaTypUczestnictwa]'))
ALTER TABLE [dbo].[ZajeciaTypUczestnictwa] DROP CONSTRAINT [FK_ZajeciaTypUczestnictwa_FormaWymiar]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ZajeciaTypUczestnictwa_TypUczestnictwa]') AND parent_object_id = OBJECT_ID(N'[dbo].[ZajeciaTypUczestnictwa]'))
ALTER TABLE [dbo].[ZajeciaTypUczestnictwa] DROP CONSTRAINT [FK_ZajeciaTypUczestnictwa_TypUczestnictwa]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ZajeciaTypUczestnictwa]') AND name = N'PK_ZajeciaTypUczestnictwa')
ALTER TABLE [dbo].[ZajeciaTypUczestnictwa] DROP CONSTRAINT [PK_ZajeciaTypUczestnictwa]


--------------------------------------
----------------ZajeciaTypUczestnictwa---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[ZajeciaTypUczestnictwa]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[ZajeciaTypUczestnictwa]
GO

CREATE TABLE [dbo].[ZajeciaTypUczestnictwa] (
   [IDZajeciaTypUczestnictwa] [int]  IDENTITY(1,1) NOT NULL
  ,[TypUczestnictwaID] [int]  NOT NULL
  ,[Podmiot] [varchar] (100) NOT NULL
  ,[PodmiotID] [int]  NOT NULL
  ,[FormaWymiarID] [int]  NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO




ALTER TABLE [dbo].[ZajeciaTypUczestnictwa] ADD  CONSTRAINT [PK_ZajeciaTypUczestnictwa] PRIMARY KEY CLUSTERED( IDZajeciaTypUczestnictwa ) ON [PRIMARY]

ALTER TABLE [dbo].[ZajeciaTypUczestnictwa] WITH NOCHECK ADD CONSTRAINT [FK_ZajeciaTypUczestnictwa_TypUczestnictwa] FOREIGN KEY([TypUczestnictwaID]) REFERENCES [dbo].[TypUczestnictwa] ([IDTypUczestnictwa])

ALTER TABLE [dbo].[ZajeciaTypUczestnictwa] WITH NOCHECK ADD CONSTRAINT [FK_ZajeciaTypUczestnictwa_FormaWymiar] FOREIGN KEY([FormaWymiarID]) REFERENCES [dbo].[FormaWymiar] ([IDFormaWymiar])


