wdro1-----------------------------Dodanie kategorii niepełnosprawności---------------------------------


INSERT INTO [dbo].[ESNiepelnosprawnoscKategoria]
           ([Nazwa]
           ,[RodzajKlasyfikacji]
           ,[Dodal]
           ,[DataDodania]
           ,[Zmodyfikowal]
           ,[DataModyfikacji])
     VALUES
('12-C - całościowe zaburzenia rozwojowe',
1,
1611,
GETDATE(),
NULL,
NULL)
GO

wdro2-----------------------------------ZmianaSposobuRozliczania--------------------------------


UPDATE [dbo].[FormaWymiar]
   SET 
      [SposobRozliczeniaID] = 8
      
 WHERE IDFormaWymiar in (98907, 98908, 98909, 98910, 91048, 91049, 91050, 91051, 91052)
GO


