declare @IDDef int
INSERT INTO [SAK_DefinicjaDokumentu](Nazwa,Symbol,TypDokumentuID,Szablon,DataSzablonu,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values('PK RK rozrachunkowe bilansowe','PKRK',32,'DokumentZEwidencji.Symbol/BRO/autoNr:6/DokumentZEwidencji.DataEwidencji:MM/DokumentZEwidencji.DataEwidencji:YYYY','DokumentZEwidencji.DataEwidencji',452,GETDATE(),null,null)
select @IDDef = SCOPE_IDENTITY()

INSERT INTO [SAK_EwidencjaDokumentowKonfiguracjaEdycji](DefinicjaDokumentuID,TworzonyZEwidencji,LP,CzyKontrahentWidoczny,CzyKontrahentEdycja,CzyDokumentEdycja,CzyPlatnosciWidoczne,
CzyPlatnosciEdycja,CzyDekretyWidoczne,CzyVATWidoczny,CzyRozniceKursoweWidoczne,CzyEdycjaWartosc,CzyWartoscWyliczanaZDekretow,CzyKsiegowanieJednostronne,CzyWartoscPowiazanaRownaWartosciZapisu,
CzyRozliczacPlatnosc,CzyJedenKontrahent,CzyNrEwidencjiPrzenosicDoPlatnosci,CzyNrEwidencjiPrzenosicDoDokumentu,CzyBilansowe,CzyDekretBilansowy,CzyRozrachunkowe,DomyslnaEwidencjaSP,
DomyslnySposobPlatnosci,DomyslnyOpisPlatnosci,DomyslnyOpisEwidencji,DomyslnyOpisDekretu,DomyslnyOpisZapisu,DomyslnaDataWplywu,DomyslnaDataEwidencji,DomyslnaDataWystawienia,
CzyWymuszacTerminPlatnosci,CzyWidocznaDataWplywu,CzyZaplatyWidoczne,TerminPlatnosciMniejszyOdDatyDokumentu,CzyNaliczacOdsetki,CzyBlokadaZmianyStanuNaliczaniaOdsetek,CzyEdycjaDatyPlatnosci
,Opis,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)

 values(@IDDef,1,21,0,0,1,1,1,1,0,0,0,1,0,1,1,1,1,1,1,1,1,1,2,NULL,'Różnice kursowe','Dekret z PK','Zapis Z PK',NULL,NULL,NULL,0,0,0,0,0,1,0,NULL,452,GETDATE(),null,null)
 
 INSERT INTO [SAK_RozliczeniaKonfiguracja](DefinicjaDokumentuID,DokumentyDoPowiazania,CzyJakoZaplata,CzyRozliczacAutomatycznie,PriorytetRozliczeniaAutomatycznego,DokumentyDoPowiazaniaAutomatycznego,
 Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
 values(@IDDef,' 11,  83, 84, 85, 86, 87, 88, 89, 90, 91, 145',0,0,0,'',452,GETDATE(),NULL,NULL)
 
 
 update SAK_RozliczeniaKonfiguracja
 set DokumentyDoPowiazania = DokumentyDoPowiazania + ', ' + cast(@IDDef as varchar(10))
 where DefinicjaDokumentuID in ( 11,  83, 84, 85, 86, 87, 88, 89, 90, 91, 145)
