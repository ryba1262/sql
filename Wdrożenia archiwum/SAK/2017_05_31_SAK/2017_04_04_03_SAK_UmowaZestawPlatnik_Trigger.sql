﻿DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''SAK_SLOUmowaZestawPlatnik_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER SAK_SLOUmowaZestawPlatnik_INSERT
')

EXEC('
CREATE TRIGGER SAK_SLOUmowaZestawPlatnik_INSERT ON ['+@BazaDanych+'].[dbo].[SAK_SLOUmowaZestawPlatnik] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''SAK_SLOUmowaZestawPlatnik'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_SAK_SLOUmowaZestawPlatnik]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDSAK_SLOUmowaZestawPlatnik]
  ,[Org_SAK_SLOKontrahentID]
  ,[Org_SAK_SLOKontrahentOdbiorcaID]
  ,[Org_SAK_SLOUmowaZestawID]
  ,[Org_LP]
  ,[Org_StawkaBrutto]
  ,[Org_StawkaNetto]
  ,[Org_StawkaSymbol]
  ,[Org_SAK_SLOStawkaVATID]
  ,[Org_Uwagi]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
  ,@ID
 ,ins.IDSAK_SLOUmowaZestawPlatnik
 ,ins.SAK_SLOKontrahentID
 ,ins.SAK_SLOKontrahentOdbiorcaID
 ,ins.SAK_SLOUmowaZestawID
 ,ins.LP
 ,ins.StawkaBrutto
 ,ins.StawkaNetto
 ,ins.StawkaSymbol
 ,ins.SAK_SLOStawkaVATID
 ,ins.Uwagi
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''SAK_SLOUmowaZestawPlatnik_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER SAK_SLOUmowaZestawPlatnik_UPDATE
')

EXEC('
CREATE TRIGGER SAK_SLOUmowaZestawPlatnik_UPDATE ON ['+@BazaDanych+'].[dbo].[SAK_SLOUmowaZestawPlatnik] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''SAK_SLOUmowaZestawPlatnik'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_SAK_SLOUmowaZestawPlatnik]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDSAK_SLOUmowaZestawPlatnik]
  ,[Org_SAK_SLOKontrahentID]
  ,[Org_SAK_SLOKontrahentOdbiorcaID]
  ,[Org_SAK_SLOUmowaZestawID]
  ,[Org_LP]
  ,[Org_StawkaBrutto]
  ,[Org_StawkaNetto]
  ,[Org_StawkaSymbol]
  ,[Org_SAK_SLOStawkaVATID]
  ,[Org_Uwagi]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
  ,@ID
 ,ins.IDSAK_SLOUmowaZestawPlatnik
 ,ins.SAK_SLOKontrahentID
 ,ins.SAK_SLOKontrahentOdbiorcaID
 ,ins.SAK_SLOUmowaZestawID
 ,ins.LP
 ,ins.StawkaBrutto
 ,ins.StawkaNetto
 ,ins.StawkaSymbol
 ,ins.SAK_SLOStawkaVATID
 ,ins.Uwagi
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''SAK_SLOUmowaZestawPlatnik_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER SAK_SLOUmowaZestawPlatnik_DELETE
')

EXEC('
CREATE TRIGGER SAK_SLOUmowaZestawPlatnik_DELETE ON ['+@BazaDanych+'].[dbo].[SAK_SLOUmowaZestawPlatnik] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''SAK_SLOUmowaZestawPlatnik'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_SAK_SLOUmowaZestawPlatnik]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDSAK_SLOUmowaZestawPlatnik]
  ,[Org_SAK_SLOKontrahentID]
  ,[Org_SAK_SLOKontrahentOdbiorcaID]
  ,[Org_SAK_SLOUmowaZestawID]
  ,[Org_LP]
  ,[Org_StawkaBrutto]
  ,[Org_StawkaNetto]
  ,[Org_StawkaSymbol]
  ,[Org_SAK_SLOStawkaVATID]
  ,[Org_Uwagi]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
  ,@ID
 ,del.IDSAK_SLOUmowaZestawPlatnik
 ,del.SAK_SLOKontrahentID
 ,del.SAK_SLOKontrahentOdbiorcaID
 ,del.SAK_SLOUmowaZestawID
 ,del.LP
 ,del.StawkaBrutto
 ,del.StawkaNetto
 ,del.StawkaSymbol
 ,del.SAK_SLOStawkaVATID
 ,del.Uwagi
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')

if not exists (select * from LOG_LogowanaTabela where Tabela = 'SAK_SLOUmowaZestawPlatnik')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('SAK_SLOUmowaZestawPlatnik', 1, 0,GetDate())
GO

