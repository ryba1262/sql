﻿DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------SAK_FlagaPodmiot-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''SAK_FlagaPodmiot_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER SAK_FlagaPodmiot_INSERT
')

EXEC('
CREATE TRIGGER SAK_FlagaPodmiot_INSERT ON ['+@BazaDanych+'].[dbo].[SAK_FlagaPodmiot] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''SAK_FlagaPodmiot'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_SAK_FlagaPodmiot]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDSAK_FlagaPodmiot]
  ,[Org_SAK_FlagaID]
  ,[Org_Podmiot]
  ,[Org_PodmiotID]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDSAK_FlagaPodmiot
 ,ins.SAK_FlagaID
 ,ins.Podmiot
 ,ins.PodmiotID
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''SAK_FlagaPodmiot_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER SAK_FlagaPodmiot_UPDATE
')

EXEC('
CREATE TRIGGER SAK_FlagaPodmiot_UPDATE ON ['+@BazaDanych+'].[dbo].[SAK_FlagaPodmiot] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''SAK_FlagaPodmiot'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_SAK_FlagaPodmiot]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDSAK_FlagaPodmiot]
  ,[Org_SAK_FlagaID]
  ,[Org_Podmiot]
  ,[Org_PodmiotID]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDSAK_FlagaPodmiot
 ,ins.SAK_FlagaID
 ,ins.Podmiot
 ,ins.PodmiotID
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''SAK_FlagaPodmiot_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER SAK_FlagaPodmiot_DELETE
')

EXEC('
CREATE TRIGGER SAK_FlagaPodmiot_DELETE ON ['+@BazaDanych+'].[dbo].[SAK_FlagaPodmiot] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''SAK_FlagaPodmiot'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_SAK_FlagaPodmiot]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDSAK_FlagaPodmiot]
  ,[Org_SAK_FlagaID]
  ,[Org_Podmiot]
  ,[Org_PodmiotID]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDSAK_FlagaPodmiot
 ,del.SAK_FlagaID
 ,del.Podmiot
 ,del.PodmiotID
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'SAK_FlagaPodmiot')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('SAK_FlagaPodmiot', 1, 0,GetDate())

