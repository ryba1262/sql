﻿DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------SAK_Flaga-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''SAK_Flaga_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER SAK_Flaga_INSERT
')

EXEC('
CREATE TRIGGER SAK_Flaga_INSERT ON ['+@BazaDanych+'].[dbo].[SAK_Flaga] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''SAK_Flaga'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_SAK_Flaga]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDSAK_Flaga]
  ,[Org_Symbol]
  ,[Org_Nazwa]
  ,[Org_Opis]
  ,[Org_Lp]
  ,[Org_Wystepowanie]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDSAK_Flaga
 ,ins.Symbol
 ,ins.Nazwa
 ,ins.Opis
 ,ins.Lp
 ,ins.Wystepowanie
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''SAK_Flaga_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER SAK_Flaga_UPDATE
')

EXEC('
CREATE TRIGGER SAK_Flaga_UPDATE ON ['+@BazaDanych+'].[dbo].[SAK_Flaga] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''SAK_Flaga'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_SAK_Flaga]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDSAK_Flaga]
  ,[Org_Symbol]
  ,[Org_Nazwa]
  ,[Org_Opis]
  ,[Org_Lp]
  ,[Org_Wystepowanie]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDSAK_Flaga
 ,ins.Symbol
 ,ins.Nazwa
 ,ins.Opis
 ,ins.Lp
 ,ins.Wystepowanie
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''SAK_Flaga_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER SAK_Flaga_DELETE
')

EXEC('
CREATE TRIGGER SAK_Flaga_DELETE ON ['+@BazaDanych+'].[dbo].[SAK_Flaga] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''SAK_Flaga'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_SAK_Flaga]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDSAK_Flaga]
  ,[Org_Symbol]
  ,[Org_Nazwa]
  ,[Org_Opis]
  ,[Org_Lp]
  ,[Org_Wystepowanie]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDSAK_Flaga
 ,del.Symbol
 ,del.Nazwa
 ,del.Opis
 ,del.Lp
 ,del.Wystepowanie
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'SAK_Flaga')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('SAK_Flaga', 1, 0,GetDate())

