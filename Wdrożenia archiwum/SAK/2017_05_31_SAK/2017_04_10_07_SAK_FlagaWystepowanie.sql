﻿IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SAK_Flaga_SAK_FlagaWystepowanie]') AND parent_object_id = OBJECT_ID(N'[dbo].[SAK_Flaga]'))
ALTER TABLE [dbo].[SAK_Flaga] DROP CONSTRAINT [FK_SAK_Flaga_SAK_FlagaWystepowanie]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SAK_FlagaWystepowanie]') AND name = N'PK__SAK_Flag__A55DC6112A78C7B6')
ALTER TABLE [dbo].[SAK_FlagaWystepowanie] DROP CONSTRAINT [PK__SAK_Flag__A55DC6112A78C7B6]

--------------------------------------
----------------SAK_FlagaWystepowanie---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[SAK_FlagaWystepowanie]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[SAK_FlagaWystepowanie]
GO

CREATE TABLE [dbo].[SAK_FlagaWystepowanie] (
   [IDSAK_FlagaWystepowanie] [int]  NOT NULL
  ,[MiejsceWystepowania] [varchar] (50) NOT NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO





ALTER TABLE [dbo].[SAK_FlagaWystepowanie] ADD  CONSTRAINT [PK__SAK_Flag__A55DC6112A78C7B6] PRIMARY KEY CLUSTERED( IDSAK_FlagaWystepowanie ) ON [PRIMARY]

ALTER TABLE [dbo].[SAK_Flaga] WITH NOCHECK ADD CONSTRAINT [FK_SAK_Flaga_SAK_FlagaWystepowanie] FOREIGN KEY([Wystepowanie]) REFERENCES [dbo].[SAK_FlagaWystepowanie] ([IDSAK_FlagaWystepowanie])


