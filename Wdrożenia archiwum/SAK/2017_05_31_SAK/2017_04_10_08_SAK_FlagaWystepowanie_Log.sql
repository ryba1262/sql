﻿
-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------SAK_FlagaWystepowanie---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_SAK_FlagaWystepowanie]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_SAK_FlagaWystepowanie] (
   [Log_SAK_FlagaWystepowanieID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDSAK_FlagaWystepowanie] [int] 
  ,[Org_MiejsceWystepowania] [varchar] (50)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_SAK_FlagaWystepowanie] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_SAK_FlagaWystepowanie] PRIMARY KEY  CLUSTERED
(
   [Log_SAK_FlagaWystepowanieID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_SAK_FlagaWystepowanie] (
   [Log_SAK_FlagaWystepowanieID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDSAK_FlagaWystepowanie] [int] 
  ,[Org_MiejsceWystepowania] [varchar] (50)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_SAK_FlagaWystepowanie ON

IF EXISTS (SELECT * FROM dbo.LOG_SAK_FlagaWystepowanie)
EXEC('INSERT INTO dbo.Tmp_LOG_SAK_FlagaWystepowanie (Log_SAK_FlagaWystepowanieID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDSAK_FlagaWystepowanie, Org_MiejsceWystepowania, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_SAK_FlagaWystepowanieID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDSAK_FlagaWystepowanie, Org_MiejsceWystepowania, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_SAK_FlagaWystepowanie WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_SAK_FlagaWystepowanie OFF

DROP TABLE dbo.LOG_SAK_FlagaWystepowanie

EXECUTE sp_rename N'dbo.Tmp_LOG_SAK_FlagaWystepowanie', N'LOG_SAK_FlagaWystepowanie', 'OBJECT'

ALTER TABLE [dbo].[LOG_SAK_FlagaWystepowanie] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_SAK_FlagaWystepowanie] PRIMARY KEY  CLUSTERED
(
   [Log_SAK_FlagaWystepowanieID]
) ON [PRIMARY]

END
GO

