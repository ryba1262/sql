-------------wdro1------2017_08_16_SAK_PobierzListeRozrachunkow





CREATE function [dbo].[SAK_ListaRozrachunkowDlaDokZEwid](@IDEwidencjiDokumentow int)
returns @result table(IDSAK_Rozrachunki int, Numer varchar(256), ElementKsiegowalnyID int, ElementKsiegowalnyTyp varchar(256),Kwota money, Waluta varchar(5), TypRozrachunku int, KontrahentNazwa varchar(255), Opis varchar(255))
as
BEGIN
	declare @DokumentTyp varchar(256), @DokumentID int
	
	select @DokumentID = Dokument, @DokumentTyp = DokumentTyp from SAK_EwidencjaDokumentow where IDSAK_EwidencjaDokumentow = @IDEwidencjiDokumentow

	if( @DokumentTyp = 'SAK_ESPRaport' )
	begin
		insert into @result
		select r.IDSAK_Rozrachunki, r.NumerDokumentu, r.DokumentID, r.DokumentTyp, r.KwotaWartosc, r.KwotaSymbol, r.RozrachunekTyp, ka.KontrahentNazwa, z.Opis
		from SAK_Rozrachunki r 
		join SAK_SLOKOntrahentAdres ka on r.SAK_SLOKontrahentAdresID = ka.IDSAK_SLOKontrahentAdres
		join dbo.SAK_ESPZaplaty z on r.DokumentID = z.IDSAK_ESPZaplaty
		where r.DokumentTyp = 'SAK_ESPZaplaty' 
			and r.DokumentID in (
				select IDSAK_ESPZaplaty from SAK_ESPZaplaty where SAK_ESPRaportID = @DokumentID)
	end
	else
	begin
		insert into @result
		select r.IDSAK_Rozrachunki, r.NumerDokumentu, r.DokumentID, r.DokumentTyp, r.KwotaWartosc, r.KwotaSymbol, r.RozrachunekTyp, ka.KontrahentNazwa, p.Opis
		from SAK_Rozrachunki r 
		join SAK_SLOKOntrahentAdres ka on r.SAK_SLOKontrahentAdresID = ka.IDSAK_SLOKontrahentAdres
		join SAK_Platnosci p on r.DokumentID = p.IDSAK_Platnosci
		where r.DokumentTyp = 'SAK_Platnosci'
			and r.DokumentID in (
				select IDSAK_Platnosci from SAK_Platnosci where DOkumentTyp = coalesce(@DokumentTyp, 'SAK_EwidencjaDokumentow') and DokumentID = coalesce(@DokumentID,@IDEwidencjiDokumentow))
	end

return
END