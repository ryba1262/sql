-------------wdro1---------2017_03_01_01_SAK_SSAK_SchematyDekretacyjneKorekty_dane.sql
---------------wdro2----------------------------------2017_04_10_01_SAK_Flaga_dane.sql
----------------wdro3---------------------2017_04_27_03_SAK_PoleDoDrzewekUmow_dane.sql
-----------------wdro4---------------------2017_05_18_01_SAK_EwidencjaWysylek_dane.sql
------------------wdro5---------------------2017_05_18_02_SAK_POWKonfiguracja_dane.sql





----------------------------------------------wdro1-----------------------------------------------------

set xact_abort on
set nocount on
begin tran

declare @IDSAK_SchematDekretacyjny int ,@Nazwa varchar(100), @Sak_DefinicjaDokumentuID int, @DokumentTyp varchar(50), @RokObrachunkowyID int, @Opis varchar(255),
        @Aktywny bit, @Domyslny bit, @Dane varchar(500), @CzyGrupowaDekretacja bit
declare @nowySchematDekretacyjnyID int, @DefinicjaDokumentuKorektyID int

declare schematDekteracyjny_cursor cursor for
select IDSAK_SchematDekretacyjny,Nazwa, Sak_DefinicjaDokumentuID, DokumentTyp, RokObrachunkowyID, Opis, Aktywny, Domyslny, Dane, CzyGrupowaDekretacja 
from SAK_SchematDekretacyjny where Nazwa like 'Faktura%' and RokObrachunkowyID = 3

OPEN schematDekteracyjny_cursor;
FETCH NEXT FROM schematDekteracyjny_cursor INTO @IDSAK_SchematDekretacyjny,@Nazwa, @Sak_DefinicjaDokumentuID, @DokumentTyp, @RokObrachunkowyID, @Opis, @Aktywny, @Domyslny, @Dane, @CzyGrupowaDekretacja
WHILE @@FETCH_STATUS = 0
   BEGIN

      select @DefinicjaDokumentuKorektyID = SAK_DefinicjaDokumentuKorektyID from SAK_HandelDefinicjaDokumentu where SAK_DefinicjaDokumentuID = @Sak_DefinicjaDokumentuID
      --dodanie schematu dekretacyjnego
      INSERT INTO SAK_SchematDekretacyjny
      (Nazwa, SAK_DefinicjaDokumentuID, DokumentTyp, RokObrachunkowyID, Opis, Aktywny, Domyslny, Dane, CzyGrupowaDekretacja, Dodal, DataDodania)
      SELECT 'Korekta sprzeda�y ' + SubString(Nazwa,PATINDEX('%-%',Nazwa), Len(Nazwa)- PATINDEX('%-%',Nazwa)+1), @DefinicjaDokumentuKorektyID, DokumentTyp, @RokObrachunkowyID, 
      'Korekta sprzeda�y ' + SubString(Nazwa,PATINDEX('%-%',Nazwa), Len(Nazwa)- PATINDEX('%-%',Nazwa)+1), Aktywny, Domyslny, Dane, CzyGrupowaDekretacja, 452, getdate() 
      FROM SAK_SchematDekretacyjny where IDSAK_SchematDekretacyjny = @IDSAK_SchematDekretacyjny
      
      select @nowySchematDekretacyjnyID = scope_identity()
      
      --dodanie pozycji schematu dekretacyjnego
      INSERT INTO SAK_SchematDekretacyjnyPoz
      (SAK_SchematDekretacyjnyID ,Konto, Strona, Dane, Warunek, Dodal, DataDodania)
      SELECT @nowySchematDekretacyjnyID, Konto, Strona, Dane, '[HandelDokument.SumaBrutto]>0', 452, getdate()
      FROM SAK_SchematDekretacyjnyPoz WHERE SAK_SchematDekretacyjnyID = @IDSAK_SchematDekretacyjny
     
      INSERT INTO SAK_SchematDekretacyjnyPoz
      (SAK_SchematDekretacyjnyID ,Konto, Strona, Dane, Warunek, Dodal, DataDodania)
      SELECT @nowySchematDekretacyjnyID, Konto, case when Strona ='Wn' then 'Ma' else 'Wn' end, Dane, '[HandelDokument.SumaBrutto]<0', 452, getdate()
      FROM SAK_SchematDekretacyjnyPoz WHERE SAK_SchematDekretacyjnyID = @IDSAK_SchematDekretacyjny
     
      
      FETCH NEXT FROM schematDekteracyjny_cursor INTO  @IDSAK_SchematDekretacyjny,@Nazwa, @Sak_DefinicjaDokumentuID, @DokumentTyp, @RokObrachunkowyID, @Opis, @Aktywny, @Domyslny, @Dane, @CzyGrupowaDekretacja
   END;
CLOSE schematDekteracyjny_cursor;
DEALLOCATE schematDekteracyjny_cursor;


commit tran



go




-------------------------------------------------------wdro2------------------------------------------------------


insert into SAK_Flaga (IDSAK_Flaga, Symbol, Nazwa, Opis, Lp,Wystepowanie, Dodal, DataDodania) 
values (1,'A','Ucze� aktywny', 'Flaga okre�laj�ca status ucznia' ,1,1, 452, GETDATE())

go




-------------------------------------------------------wdro3--------------------------------------------------------



delete from DWOJoin where IDDWOJoin = 13061
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
  values (13061,13000,'SAK_FlagaPodmiot','left join SAK_FlagaPodmiot on PodmiotID = IDSAK_SLOUmowa and Podmiot = ''SAK_SLOUmowa'' and SAK_FlagaID = 1',13001,1,452,getdate(),NULL,NULL)  
  
  delete from DWOPole where IDDWOPole = 13110
insert into DWOPole (IDDWOPole, DWOJoinID, NazwaPola, Sygnatura, 
    DefinicjaPola, NazwaPolaSzablon, Klucz, OrderByField, 
    Drzewko, WymaganeDrzewko, Wyswietlane, WymaganeWyswietlane, Dodal, DataDodania)
values (13110, 13061, 'Czy ucze� aktywny','UCZEN_AKTYWNY',
    'case when IDSAK_FlagaPodmiot IS null then ''Nie'' else ''Tak'' end', null, null, null,
    1, 0, 1, 0, 452, getdate())    

	go


	-----------------------------------------------wdro4----------------------------------------------------


	delete from DWOWyszukiwanie where IDDWOWyszukiwanie between 6000 and 6009
delete from DWOPole where IDDWOPole between 6000 and 6027
delete from DWOWariant where IDDWOWariant between 6000 and 6007
delete from DWOJoin where IDDWOJoin between 6001 and 6005
delete from DWOWystepowanie where IDDWOWystepowanie = 6000

INSERT INTO [DWOWystepowanie](IDDWOWystepowanie,MiejsceWystepowania,RegKeyName,MaxLiczbaKolumn,LP,CzySprawdzacUprawnienia,DomyslneDrzewko,DomyslnePola,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(6000,'EwidencjaWysylek','EwidencjaWysylek',30,1,0,NULL,NULL,452,getdate(),NULL,NULL)

INSERT INTO [DWOJoin](IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6004,6000,'POWPowiadomienie','from POWPowiadomienie POWPowiadomienie',NULL,0,452,getdate(),NULL,NULL)
INSERT INTO [DWOJoin](IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6001,6000,'POWWysylka','join POWWysylka POWWysylka on POWPowiadomienie.POWWysylkaID = POWWysylka.IDPOWWysylka',6004,1,452,getdate(),NULL,NULL)
INSERT INTO [DWOJoin](IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6003,6000,'POWKanal','right join POWKanal POWKanal on POWWysylka.POWKanalID = POWKanal.IDPOWKanal',6001,3,452,getdate(),NULL,NULL)
INSERT INTO [DWOJoin](IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6002,6000,'POWGrupaDocelowa','join POWGrupaDocelowa POWGrupaDocelowa on POWKanal.POWGrupaDocelowaID = POWGrupaDocelowa.IDPOWGrupaDocelowa',6003,4,452,getdate(),NULL,NULL)
INSERT INTO [DWOJoin](IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6005,6000,'Uzytkownik','join Uzytkownik Uzytkownik on POWWysylka.Dodal = Uzytkownik.IDUzytkownik',6001,2,452,getdate(),NULL,NULL)


INSERT INTO [DWOWariant](IDDWOWariant,DWOJoinID,Nazwa,Opis,Warunek,CzyPelnyWarunek,CzySprawdzacUprawnienia,LP,Wymagany,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6001,6001,'Wszystkie wysy�ki','Wszystkie wysy�ki','1 = 1',1,0,1,0,452,getdate(),NULL,NULL)
INSERT INTO [DWOWariant](IDDWOWariant,DWOJoinID,Nazwa,Opis,Warunek,CzyPelnyWarunek,CzySprawdzacUprawnienia,LP,Wymagany,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6002,6001,'Wysy�ki wys�ane','Wysy�ki wys�ane','POWWysylka.CzyWyslana = 1',1,0,2,0,452,getdate(),NULL,NULL)
INSERT INTO [DWOWariant](IDDWOWariant,DWOJoinID,Nazwa,Opis,Warunek,CzyPelnyWarunek,CzySprawdzacUprawnienia,LP,Wymagany,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6003,6001,'Wysy�ki niewys�ane','Wysy�ki niewys�ane','POWWysylka.CzyWyslana = 0',1,0,3,0,452,getdate(),NULL,NULL)
INSERT INTO [DWOWariant](IDDWOWariant,DWOJoinID,Nazwa,Opis,Warunek,CzyPelnyWarunek,CzySprawdzacUprawnienia,LP,Wymagany,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6004,6001,'Wysy�ki zatwierdzone','Wysy�ki zatwierdzone','POWWysylka.CzyZatwierdzona = 1',1,0,4,0,452,getdate(),NULL,NULL)
INSERT INTO [DWOWariant](IDDWOWariant,DWOJoinID,Nazwa,Opis,Warunek,CzyPelnyWarunek,CzySprawdzacUprawnienia,LP,Wymagany,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6005,6001,'Wysy�ki niezatwierdzone','Wysy�ki niezatwierdzone','POWWysylka.CzyZatwierdzona = 0',1,0,5,0,452,getdate(),NULL,NULL)
INSERT INTO [DWOWariant](IDDWOWariant,DWOJoinID,Nazwa,Opis,Warunek,CzyPelnyWarunek,CzySprawdzacUprawnienia,LP,Wymagany,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6006,6001,'Wysy�ki zako�czone','Wysy�ki zako�czone','POWWysylka.CzyZakonczona = 1',1,0,6,0,452,getdate(),NULL,NULL)
INSERT INTO [DWOWariant](IDDWOWariant,DWOJoinID,Nazwa,Opis,Warunek,CzyPelnyWarunek,CzySprawdzacUprawnienia,LP,Wymagany,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6007,6001,'Wysy�ki niezako�czone','Wysy�ki niezako�czone','POWWysylka.CzyZakonczona = 0',1,0,7,0,452,getdate(),NULL,NULL)



INSERT INTO [DWOPole](IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6001,6002,'Grupa docelowa',NULL,'POWGrupaDocelowa.Nazwa',NULL,NULL,NULL,1,0,0,0,1,NULL,NULL,452,getdate(),NULL,NULL)
INSERT INTO [DWOPole](IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6002,6003,'Kana� komunikacyjny',NULL,'POWKanal.Nazwa',NULL,NULL,NULL,1,0,0,0,1,NULL,NULL,452,getdate(),NULL,NULL)
INSERT INTO [DWOPole](IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6003,6001,'Identyfikator wysy�ki','IDWYSYLKA','coalesce(POWWysylka.IDPOWWysylka, 0)',NULL,NULL,NULL,0,0,1,1,0,NULL,NULL,452,getdate(),NULL,NULL)
INSERT INTO [DWOPole](IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6004,6001,'Sygnatura (wysy�ka)','SYGNATURAW','POWWysylka.Sygnatura',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)
INSERT INTO [DWOPole](IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6005,6001,'Data rozpocz�cia wysy�ki',NULL,'convert(varchar(10), POWWysylka.DataRozpoczecia, 121)',NULL,NULL,NULL,0,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)
INSERT INTO [DWOPole](IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6006,6001,'Data wa�no�ci wysy�ki',NULL,'convert(varchar(10), POWWysylka.WaznaDo, 121)',NULL,NULL,NULL,0,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)
INSERT INTO [DWOPole](IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6007,6001,'Opis wysy�ki',NULL,'POWWysylka.Opis',NULL,NULL,NULL,0,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)
INSERT INTO [DWOPole](IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6008,6001,'Czy wysy�ka wys�ana','CZYWYSLANA','case when POWWysylka.CzyWyslana = 1 then ''Tak'' else ''Nie'' end',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)
INSERT INTO [DWOPole](IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6009,6001,'Czy wysy�ka zatwierdzona','CZYZATWIERDZONA','case when POWWysylka.CzyZatwierdzona = 1 then ''Tak'' else ''Nie'' end',NULL,NULL,NULL,1,0,1,5,0,NULL,NULL,452,getdate(),NULL,NULL)
INSERT INTO [DWOPole](IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6010,6001,'Czy wysy�ka zako�czona','CZYZAKONCZONA','case when POWWysylka.CzyZakonczona = 1 then ''Tak'' else ''Nie'' end',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)
INSERT INTO [DWOPole](IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6011,6001,'Data zako�czenia wysy�ki',NULL,'convert(varchar(10), POWWysylka.DataZakonczenia, 121)',NULL,NULL,NULL,0,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)
INSERT INTO [DWOPole](IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6012,6004,'Identyfikator powiadomienia','IDPOWIADOMIENIE','POWPowiadomienie.IDPOWPowiadomienie',NULL,NULL,NULL,0,0,1,2,0,NULL,NULL,452,getdate(),NULL,NULL)
INSERT INTO [DWOPole](IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6013,6004,'Sygnatura (powiadomienie)','SYGNATURAP','POWPowiadomienie.Sygnatura',NULL,NULL,NULL,0,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)
INSERT INTO [DWOPole](IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6014,6004,'IdentyfikatorOdbiorcy',NULL,'POWPowiadomienie.IdentyfikatorOdbiorcy',NULL,NULL,NULL,0,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)
INSERT INTO [DWOPole](IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6015,6004,'NazwaOdbiorcy1',NULL,'POWPowiadomienie.NazwaOdbiorcy1',NULL,NULL,NULL,0,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)
INSERT INTO [DWOPole](IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6016,6004,'NazwaOdbiorcy2',NULL,'POWPowiadomienie.NazwaOdbiorcy2',NULL,NULL,NULL,0,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)
INSERT INTO [DWOPole](IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6017,6004,'AdresOdbiorcy',NULL,'POWPowiadomienie.AdresOdbiorcy',NULL,NULL,NULL,0,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)
INSERT INTO [DWOPole](IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6018,6004,'Nadawca powiadomienia',NULL,'POWPowiadomienie.Nadawca',NULL,NULL,NULL,0,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)
INSERT INTO [DWOPole](IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6019,6004,'Temat powiadomienia',NULL,'POWPowiadomienie.Temat',NULL,NULL,NULL,0,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)
INSERT INTO [DWOPole](IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6020,6004,'Tre�� powiadomienia',NULL,'POWPowiadomienie.Tresc',NULL,NULL,NULL,0,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)
INSERT INTO [DWOPole](IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6021,6004,'Informacja1',NULL,'POWPowiadomienie.Informacja1',NULL,NULL,NULL,0,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)
INSERT INTO [DWOPole](IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6022,6002,'Identyfikator grupy docelowej','IDGRUPA','POWGrupaDocelowa.IDPOWGrupaDocelowa',NULL,NULL,NULL,0,0,1,3,0,NULL,NULL,452,getdate(),NULL,NULL)
INSERT INTO [DWOPole](IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6023,6005,'Tw�rca wysy�ki',NULL,'coalesce(Uzytkownik.Imie + '' '', '''') + coalesce(Uzytkownik.Nazwisko, '''')',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)
INSERT INTO [DWOPole](IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6024,6001,'Data utworzenia wysy�ki',NULL,'convert(varchar(10), POWWysylka.DataDodania, 121)',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)
INSERT INTO [DWOPole](IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6025,6001,'Czy wysy�ka w trakcie przetwarzania','CZYPRZETWARZANA','case when POWWysylka.WTrakciePrzetwarzania = 1 then ''Tak'' else ''Nie'' end',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)
INSERT INTO [DWOPole](IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6026,6001,'Ostatnio przetwarzane powiadomienie','OSTATNIOPRZETWPOW','POWWysylka.OstatnioPrzetwarzanePowID',NULL,NULL,NULL,0,0,1,4,0,NULL,NULL,452,getdate(),NULL,NULL)
INSERT INTO [DWOPole](IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6027,6003,'Identyfikator kana�u','IDKANAL','POWKanal.IDPOWKanal',NULL,NULL,NULL,0,0,1,6,0,NULL,NULL,452,getdate(),NULL,NULL)




INSERT INTO [DWOWyszukiwanie](IDDWOWyszukiwanie,DWOPoleID,Nazwa,TypZmiennej,RodzajWarunku,Opis,LP,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6001,6005,'Data rozpocz�cia wysy�ki','date','between','Zakres dat planowanego rozpocz�cia wysy�ki',1,452,getdate(),NULL,NULL)
INSERT INTO [DWOWyszukiwanie](IDDWOWyszukiwanie,DWOPoleID,Nazwa,TypZmiennej,RodzajWarunku,Opis,LP,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6002,6006,'Data wa�no�ci wysy�ki','date','between','Zakres dat wa�no�ci wysy�ki',2,452,getdate(),NULL,NULL)
INSERT INTO [DWOWyszukiwanie](IDDWOWyszukiwanie,DWOPoleID,Nazwa,TypZmiennej,RodzajWarunku,Opis,LP,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6003,6011,'Data zako�czenia wysy�ki','date','between','Zakres dat zako�czenia wysy�ki',3,452,getdate(),NULL,NULL)
INSERT INTO [DWOWyszukiwanie](IDDWOWyszukiwanie,DWOPoleID,Nazwa,TypZmiennej,RodzajWarunku,Opis,LP,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6004,6014,'Identyfikator odbiorcy','text','like','Numer jednoznacznie identyfikuj�cy odbiorc� (znak % zast�puje dowolny ci�g znak�w)',4,452,getdate(),NULL,NULL)
INSERT INTO [DWOWyszukiwanie](IDDWOWyszukiwanie,DWOPoleID,Nazwa,TypZmiennej,RodzajWarunku,Opis,LP,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6005,6016,'Nazwa odbiorcy','text','like','Nazwa w�asna odbiorcy, np. nazwisko (znak % zast�puje dowolny ci�g znak�w)',5,452,getdate(),NULL,NULL)
INSERT INTO [DWOWyszukiwanie](IDDWOWyszukiwanie,DWOPoleID,Nazwa,TypZmiennej,RodzajWarunku,Opis,LP,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6006,6018,'Nadawca powiadomienia','text','like','Nadawca powiadomienia (znak % zast�puje dowolny ci�g znak�w)',6,452,getdate(),NULL,NULL)
INSERT INTO [DWOWyszukiwanie](IDDWOWyszukiwanie,DWOPoleID,Nazwa,TypZmiennej,RodzajWarunku,Opis,LP,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6007,6019,'Temat powiadomienia','text','like','Temat powiadomienia (znak % zast�puje dowolny ci�g znak�w)',7,452,getdate(),NULL,NULL)
INSERT INTO [DWOWyszukiwanie](IDDWOWyszukiwanie,DWOPoleID,Nazwa,TypZmiennej,RodzajWarunku,Opis,LP,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(6008,6020,'Tre�� powiadomienia','text','like','Tre�� powiadomienia (znak % zast�puje dowolny ci�g znak�w)',8,452,getdate(),NULL,NULL)



go


----------------------------------------------------------wdro5------------------------------------------------

delete from DWOPole where IDDWOPole between 11074 and 11075
delete from DWOJoin where IDDWOJoin between 11016 and 11017

INSERT INTO [DWOJoin](IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(11016,11000,'SAK_SLOKontrahentEmail','left join SAK_SLOKontrahentEmail on SAK_SLOKontrahent.IDSAK_SLOKontrahent = SAK_SLOKontrahentEmail.SAK_SLOKontrahentID',11001,1,452,getdate(),NULL,NULL)

INSERT INTO [DWOJoin](IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(11017,11000,'SAK_SLOKontrahentTelefon','left join SAK_SLOKontrahentTelefon on SAK_SLOKontrahent.IDSAK_SLOKontrahent = SAK_SLOKontrahentTelefon.SAK_SLOKontrahentID and SAK_SLOKontrahentTelefon.CzyTelefonSMS=1 ',11001,1,452,getdate(),NULL,NULL)
    
insert into DWOPole (IDDWOPole, DWOJoinID, NazwaPola, Sygnatura, 
    DefinicjaPola, NazwaPolaSzablon, Klucz, OrderByField, 
    Drzewko, WymaganeDrzewko, Wyswietlane, WymaganeWyswietlane, Dodal, DataDodania)
values (11074, 11016, 'Email', 'KONTRAHENT_EMAIL',
    'SAK_SLOKontrahentEmail.Email', null, null, null,
    1, 0, 1, 0, 452, getdate())         
    
insert into DWOPole (IDDWOPole, DWOJoinID, NazwaPola, Sygnatura, 
    DefinicjaPola, NazwaPolaSzablon, Klucz, OrderByField, 
    Drzewko, WymaganeDrzewko, Wyswietlane, WymaganeWyswietlane, Dodal, DataDodania)
values (11075, 11017, 'Telefon kom�rkowy (SMS)', 'TELEFON_SMS',
    'SAK_SLOKontrahentTelefon.NumerTelefonu', null, null, null,
    1, 0, 1, 0, 452, getdate())          

GO

update DWOPole 
set Sygnatura = 'IMIE'
where IDDWOPole = 11012

update DWOPole 
set Sygnatura = 'NAZWISKO'
where IDDWOPole = 11013



update DWOPole
set Wyswietlane = 1 
where IDDWOPole = 13013



declare @idGrupa int
select @idGrupa = IDPOWGrupaDocelowa from POWGrupaDocelowa where Nazwa = 'Kontrahenci'
if(@idGrupa is not null)
begin
  delete from POWMapaPol where IDPOWMapaPol between 54 and 78
  delete from POWKanal where POWGrupaDocelowaID = @idGrupa
  delete from POWGrupaDocelowa where IDPOWGrupaDocelowa = @idGrupa
end

insert into POWGrupaDocelowa (IDPOWGrupaDocelowa,Nazwa,Sygnatura,Dodal,DataDodania)
values (4,'Kontrahenci', 'KONTRAHENCI',452, getdate())

select @idGrupa = IDPOWGrupaDocelowa from POWGrupaDocelowa where Nazwa = 'Kontrahenci'

insert into POWKanal (IDPOWKanal,POWGrupaDocelowaID,DWOWystepowanieID,Nazwa,TypAdresu,MaxDlugoscTresci,CzyDozwolonePolskieZnaki,CzyWymaganyNadawca,CzyWymaganyTemat,Dodal,DataDodania)
values (10,@idGrupa,11000,'D�ugi SMS z polskimi znakami','SMS',603,1,0,0,452, getdate() )
insert into POWKanal (IDPOWKanal,POWGrupaDocelowaID,DWOWystepowanieID,Nazwa,TypAdresu,MaxDlugoscTresci,CzyDozwolonePolskieZnaki,CzyWymaganyNadawca,CzyWymaganyTemat,Dodal,DataDodania)
values (11,@idGrupa,11000,'D�ugi SMS bez polskich znak�w','SMS',1377,0,0,0,452, getdate() )
insert into POWKanal (IDPOWKanal,POWGrupaDocelowaID,DWOWystepowanieID,Nazwa,TypAdresu,MaxDlugoscTresci,CzyDozwolonePolskieZnaki,CzyWymaganyNadawca,CzyWymaganyTemat,Dodal,DataDodania)
values (12,@idGrupa,11000,'Kr�tki SMS z polskimi znakami','SMS',70,1,0,0,452, getdate() )
insert into POWKanal (IDPOWKanal,POWGrupaDocelowaID,DWOWystepowanieID,Nazwa,TypAdresu,MaxDlugoscTresci,CzyDozwolonePolskieZnaki,CzyWymaganyNadawca,CzyWymaganyTemat,Dodal,DataDodania)
values (13,@idGrupa,11000,'Kr�tki SMS bez polskich znak�w','SMS',160,0,0,0,452, getdate() )
insert into POWKanal (IDPOWKanal,POWGrupaDocelowaID,DWOWystepowanieID,Nazwa,TypAdresu,MaxDlugoscTresci,CzyDozwolonePolskieZnaki,CzyWymaganyNadawca,CzyWymaganyTemat,Dodal,DataDodania)
values (14,@idGrupa,11000,'Wiadomo�� e-mail','Email',8000,1,1,1,452, getdate() )


GO




delete from POWMapaPol where IDPOWMapaPol between 54 and 78

INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(54,10,'ID_KONTRAHENT','ID',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(55,10,'TELEFON_SMS','TelefonSMS',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(56,10,'IMIE','Imie',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(57,10,'NAZWISKO','Nazwisko',0,0,452,getdate(),NULL,NULL)


INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(59,11,'ID_KONTRAHENT','ID',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(60,11,'TELEFON_SMS','TelefonSMS',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(61,11,'IMIE','Imie',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(62,11,'NAZWISKO','Nazwisko',0,0,452,getdate(),NULL,NULL)


INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(64,12,'ID_KONTRAHENT','ID',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(65,12,'TELEFON_SMS','TelefonSMS',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(66,12,'IMIE','Imie',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(67,12,'NAZWISKO','Nazwisko',0,0,452,getdate(),NULL,NULL)


INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(69,13,'ID_KONTRAHENT','ID',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(70,13,'TELEFON_SMS','TelefonSMS',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(71,13,'IMIE','Imie',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(72,13,'NAZWISKO','Nazwisko',0,0,452,getdate(),NULL,NULL)


INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(74,14,'ID_KONTRAHENT','ID',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(75,14,'EMAIL','Email',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(76,14,'IMIE','Imie',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(77,14,'NAZWISKO','Nazwisko',0,0,452,getdate(),NULL,NULL)



GO