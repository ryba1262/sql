﻿
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Łukasz Wizner
-- Create date: 2015-03-13
-- Description:	Pobiera listę dokumentów handlowych.
-- =============================================
ALTER PROCEDURE [dbo].[SAK_HandelDokument_DajListe]
	
	@typDokumentu INT,
	@handelDefinicjaID INT,
	@dataWystawieniaOD DATETIME,
	@dataWystawieniaDO DATETIME,
	@kontrahentID INT
AS
BEGIN

	declare 
	@typDokumentuPRC INT,
	@handelDefinicjaIDPRC INT,
	@dataWystawieniaODPRC DATETIME,
	@dataWystawieniaDOPRC DATETIME,
	@kontrahentIDPRC INT
	
	set @typDokumentuPRC = @typDokumentu
	set @handelDefinicjaIDPRC = @handelDefinicjaID
	set @dataWystawieniaODPRC = @dataWystawieniaOD
	set @dataWystawieniaDOPRC = @dataWystawieniaDO
	set @kontrahentIDPRC = @kontrahentID

		;with HandelCTE (IDHandelDokument, HandelDefinicjaID, PlatnoscID)
		as
		(
			SELECT hd.IDSAK_HandelDokument, hdd.IDSAK_HandelDefinicjaDokumentu, p.IDSAK_Platnosci
			FROM SAK_HandelDokument hd
			JOIN SAK_HandelDefinicjaDokumentu hdd ON hdd.IDSAK_HandelDefinicjaDokumentu = hd.SAK_HandelDefinicjaDokumentuID
			LEFT JOIN SAK_SLOKontrahentAdres AS ka ON ka.IDSAK_SLOKontrahentAdres = hd.SAK_SLOKontrahentAdresID
			LEFT JOIN SAK_Platnosci p ON p.DokumentTyp = 'SAK_HandelDokument' AND hd.IDSAK_HandelDokument = p.DokumentID
			WHERE 
				hdd.TypDokumentu = @typDokumentuPRC 
				AND (@handelDefinicjaIDPRC IS NULL OR hdd.IDSAK_HandelDefinicjaDokumentu = @handelDefinicjaIDPRC)
				AND (@kontrahentIDPRC IS NULL OR ka.SAK_SLOKontrahentID = @kontrahentIDPRC)
				AND (@dataWystawieniaODPRC IS NULL OR CONVERT(DATE, hd.DataWystawienia) >= CONVERT(DATE, @dataWystawieniaODPRC))
				AND (@dataWystawieniaDOPRC IS NULL OR CONVERT(DATE, hd.DataWystawienia) <= CONVERT(DATE, @dataWystawieniaDOPRC))
		)
		, HandelNotaOdsetkowaCTE (IDHandelDokument)
		as
		(
			SELECT DISTINCT h.IDHandelDokument
			FROM HandelCTE h
			JOIN SAK_DokumentyRozliczeniowePoz AS drp ON h.PlatnoscID = drp.SAK_PlatnosciID
			JOIN SAK_DokumentyRozliczeniowe dr on dr.IDSAK_DokumentyRozliczeniowe = drp.SAK_DokumentyRozliczenioweID
			GROUP BY h.IDHandelDokument
		)

		SELECT 
			hd.[IDSAK_HandelDokument] AS [IDHandelDokument], 
			hd.[SAK_HandelDefinicjaDokumentuID] AS [HandelDefinicjaID], 
			hdd.[Nazwa] AS [HandelDefinicjaNazwa], 
			hdd.[Symbol] AS [HandelDefinicjaSymbol], 
			hdd.[TypDokumentu], 
			hd.[NumerSymbol], 
			hd.[Identyfikator], 
			hd.[NumerPelny], 
			hd.[NumerData], 
			hd.[Stan], 
			hd.[Kierunek], 
			hd.[KierunekPrzeliczania],
			hd.[Korekta],
			hd.[DataWystawienia], 
			hd.[DataOperacji], 
			hd.[DataOtrzymania], 
			hd.[Opis], 
			hd.[SAK_SLOKontrahentAdresID] AS [KontrahentAdresID], 
			k.IDSAK_SLOKontrahent AS [KontrahentID],
			ka.KontrahentNazwa AS [KontrahentNazwa],
			k.NIP AS [KontrahentNIP],
			hd.[SAK_SLOSposobZaplatyID] AS [SposobZaplatyID], 
			hd.[SAK_ESPEwidencjaID] AS [EwidencjaID], 
			hd.[TerminPlatnosci], 
			hd.[NumerObcy], 
			hd.[SumaNetto], 
			hd.[SumaVAT], 
			hd.[SumaBrutto], 
			hd.[Wartosc], 
			hd.[WartocSymbol] AS [WartoscSymbol], 
			hd.[WartoscWWalucieOperacji], 
			hd.[WartoscWWalucieOperacjiSymbol], 
			hd.[SAK_ESPTabelaKursowaID] AS [TabelaKursowaID], 
			hd.[SAK_ESPKursWalutyID] AS [KursWalutID], 
			hd.[Kurs], 
			hd.[RD] AS [KodRD], 
			hd.[RZ] AS [KodRZ], 
			hd.[SAK_SLOUmowaID] AS [UmowaID], 
			hd.[SAK_DecyzjaID] AS [DecyzjaID], 
			hd.[NrRachunku], hd.[ZrodloRachunku], 
			p.CzyUgoda AS [CzyUgoda], 
			CAST(hno_cte.IDHandelDokument AS BIT) AS [NotaOdsetkowa],
			roz.[IDSAK_Rozrachunki] AS [RozrachunekID], 
			ud.[Imie] + ' ' + ud.[Nazwisko] AS [Dodal], 
			hd.[DataDodania], 
			uz.[Imie] + ' ' + uz.[Nazwisko] AS [Zmodyfikowal], 
			hd.[DataModyfikacji],
			case when rejestW.DokumentID IS not null then 1 else 0 end CzyWydrukowano
	FROM SAK_HandelDokument hd
	LEFT JOIN HandelNotaOdsetkowaCTE hno_cte ON hno_cte.IDHandelDokument = hd.IDSAK_HandelDokument
	LEFT JOIN SAK_Platnosci p ON p.DokumentTyp = 'SAK_HandelDokument' AND hd.IDSAK_HandelDokument = p.DokumentID
	LEFT JOIN SAK_Rozrachunki AS roz ON roz.DokumentTyp = 'SAK_Platnosci' AND p.IDSAK_Platnosci = roz.DokumentID
	INNER JOIN SAK_HandelDefinicjaDokumentu AS hdd ON hdd.IDSAK_HandelDefinicjaDokumentu = hd.SAK_HandelDefinicjaDokumentuID
	LEFT JOIN SAK_SLOKontrahentAdres AS ka ON ka.IDSAK_SLOKontrahentAdres = hd.SAK_SLOKontrahentAdresID
	LEFT JOIN SAK_SLOKontrahent AS k ON k.IDSAK_SLOKontrahent = ka.SAK_SLOKontrahentID
	INNER JOIN Uzytkownik AS ud ON ud.IDUzytkownik = hd.Dodal
	LEFT JOIN Uzytkownik AS uz ON uz.IDUzytkownik = hd.Zmodyfikowal
	LEFT JOIN (select DokumentID, COUNT(*) as ilosc from SAK_WURejestrWydrukow rw where rw.DokumentTyp = 'SAK_HandelDokument' and rw.Duplikat = 0 and rw.CzyWydrukowanoPoprawnie = 1 group by DokumentID) rejestW on rejestW.DokumentID = IDSAK_HandelDokument
	WHERE 
		hdd.TypDokumentu = @typDokumentuPRC 
		AND (@handelDefinicjaIDPRC IS NULL OR hdd.IDSAK_HandelDefinicjaDokumentu = @handelDefinicjaIDPRC)
		AND (@kontrahentIDPRC IS NULL OR ka.SAK_SLOKontrahentID = @kontrahentIDPRC)
		AND (@dataWystawieniaODPRC IS NULL OR CONVERT(DATE, hd.DataWystawienia) >= CONVERT(DATE, @dataWystawieniaODPRC))
		AND (@dataWystawieniaDOPRC IS NULL OR CONVERT(DATE, hd.DataWystawienia) <= CONVERT(DATE, @dataWystawieniaDOPRC))
END

