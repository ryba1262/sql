﻿--------------------------------------
----------------SAK_SLOUmowaZestawPlatnik---------------
--------------------------------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_SAK_SLOUmowaZestawPlatnik]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_SAK_SLOUmowaZestawPlatnik] (
   [Log_SAK_SLOUmowaZestawPlatnikID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDSAK_SLOUmowaZestawPlatnik] [int] 
  ,[Org_SAK_SLOKontrahentID] [int] 
  ,[Org_SAK_SLOKontrahentOdbiorcaID] [int] 
  ,[Org_SAK_SLOUmowaZestawID] [int] 
  ,[Org_LP] [int] 
  ,[Org_StawkaBrutto] [money] 
  ,[Org_StawkaNetto] [money] 
  ,[Org_StawkaSymbol] [varchar] (10)
  ,[Org_SAK_SLOStawkaVATID] [int] 
  ,[Org_Uwagi] [varchar] (255)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]
ALTER TABLE [dbo].[LOG_SAK_SLOUmowaZestawPlatnik] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_SAK_SLOUmowaZestawPlatnik] PRIMARY KEY  CLUSTERED
(
   [Log_SAK_SLOUmowaZestawPlatnikID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_SAK_SLOUmowaZestawPlatnik] (
   [Log_SAK_SLOUmowaZestawPlatnikID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDSAK_SLOUmowaZestawPlatnik] [int] 
  ,[Org_SAK_SLOKontrahentID] [int] 
  ,[Org_SAK_SLOKontrahentOdbiorcaID] [int] 
  ,[Org_SAK_SLOUmowaZestawID] [int] 
  ,[Org_LP] [int] 
  ,[Org_StawkaBrutto] [money] 
  ,[Org_StawkaNetto] [money] 
  ,[Org_StawkaSymbol] [varchar] (10)
  ,[Org_SAK_SLOStawkaVATID] [int] 
  ,[Org_Uwagi] [varchar] (255)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]
SET IDENTITY_INSERT dbo.Tmp_LOG_SAK_SLOUmowaZestawPlatnik ON

IF EXISTS (SELECT * FROM dbo.LOG_SAK_SLOUmowaZestawPlatnik)
EXEC('INSERT INTO dbo.Tmp_LOG_SAK_SLOUmowaZestawPlatnik (Log_SAK_SLOUmowaZestawPlatnikID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDSAK_SLOUmowaZestawPlatnik, Org_SAK_SLOKontrahentID, Org_SAK_SLOUmowaZestawID, Org_LP, Org_StawkaBrutto, Org_StawkaNetto, Org_StawkaSymbol, Org_SAK_SLOStawkaVATID, Org_Uwagi, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_SAK_SLOUmowaZestawPlatnikID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDSAK_SLOUmowaZestawPlatnik, Org_SAK_SLOKontrahentID, Org_SAK_SLOUmowaZestawID, Org_LP, Org_StawkaBrutto, Org_StawkaNetto, Org_StawkaSymbol, Org_SAK_SLOStawkaVATID, Org_Uwagi, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_SAK_SLOUmowaZestawPlatnik WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_SAK_SLOUmowaZestawPlatnik OFF

DROP TABLE dbo.LOG_SAK_SLOUmowaZestawPlatnik

EXECUTE sp_rename N'dbo.Tmp_LOG_SAK_SLOUmowaZestawPlatnik', N'LOG_SAK_SLOUmowaZestawPlatnik', 'OBJECT'

ALTER TABLE [dbo].[LOG_SAK_SLOUmowaZestawPlatnik] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_SAK_SLOUmowaZestawPlatnik] PRIMARY KEY  CLUSTERED
(
   [Log_SAK_SLOUmowaZestawPlatnikID]
) ON [PRIMARY]

END
GO


