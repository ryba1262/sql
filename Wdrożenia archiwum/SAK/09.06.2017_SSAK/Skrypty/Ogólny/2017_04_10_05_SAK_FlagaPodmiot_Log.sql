﻿
-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------SAK_FlagaPodmiot---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_SAK_FlagaPodmiot]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_SAK_FlagaPodmiot] (
   [Log_SAK_FlagaPodmiotID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDSAK_FlagaPodmiot] [int] 
  ,[Org_SAK_FlagaID] [int] 
  ,[Org_Podmiot] [varchar] (150)
  ,[Org_PodmiotID] [int] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_SAK_FlagaPodmiot] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_SAK_FlagaPodmiot] PRIMARY KEY  CLUSTERED
(
   [Log_SAK_FlagaPodmiotID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_SAK_FlagaPodmiot] (
   [Log_SAK_FlagaPodmiotID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDSAK_FlagaPodmiot] [int] 
  ,[Org_SAK_FlagaID] [int] 
  ,[Org_Podmiot] [varchar] (150)
  ,[Org_PodmiotID] [int] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_SAK_FlagaPodmiot ON

IF EXISTS (SELECT * FROM dbo.LOG_SAK_FlagaPodmiot)
EXEC('INSERT INTO dbo.Tmp_LOG_SAK_FlagaPodmiot (Log_SAK_FlagaPodmiotID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDSAK_FlagaPodmiot, Org_SAK_FlagaID, Org_Podmiot, Org_PodmiotID, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_SAK_FlagaPodmiotID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDSAK_FlagaPodmiot, Org_SAK_FlagaID, Org_Podmiot, Org_PodmiotID, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_SAK_FlagaPodmiot WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_SAK_FlagaPodmiot OFF

DROP TABLE dbo.LOG_SAK_FlagaPodmiot

EXECUTE sp_rename N'dbo.Tmp_LOG_SAK_FlagaPodmiot', N'LOG_SAK_FlagaPodmiot', 'OBJECT'

ALTER TABLE [dbo].[LOG_SAK_FlagaPodmiot] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_SAK_FlagaPodmiot] PRIMARY KEY  CLUSTERED
(
   [Log_SAK_FlagaPodmiotID]
) ON [PRIMARY]

END
GO

