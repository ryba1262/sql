﻿declare @ID int
insert into EU_EdytorUniwersalny (Tabela,Nazwa, KolumnaIDNazwa, SygnaturaObiektu, Opis, CzyBlokowacPowiazane, BlokowaneKolumny,Dodal, DataDodania )
values ('SAK_Flaga','Flagi','IDSAK_Flaga', 'SAK_Flaga','Flagi wykorzystywane w modułach SAK.',1,'Nazwa;Wystepowanie',452,GETDATE())

select @ID = SCOPE_IDENTITY()

insert into EU_EdytorUniwersalnyPoz(EU_EdytorUniwersalnyID, NazwaPolaPowiazanego,Tabela,TabelaKlucz,PoleWyswietlane, Dodal, DataDodania)
values (@ID,'Wystepowanie','SAK_FlagaWystepowanie','IDSAK_FlagaWystepowanie','MiejsceWystepowania', 452, GETDATE())


declare @IDDWOObiekt int

select @IDDWOObiekt = IDDWOObiekt from DWOObiekt where Sygnatura = 'SAK_Flaga'

if(@IDDWOObiekt>0)
exec [dbo].[DWOUsunUprawnieniaDoObiektu] @IDDWOObiekt

delete from DWOObiekt where Sygnatura = 'SAK_Flaga'

declare @IDDWOObiektNadrzedny int
select @IDDWOObiektNadrzedny = IDDWOObiekt from DWOObiekt where Sygnatura = 'SAK_EdytorUniwersalny'

declare @UserID int
select @UserID = IDUzytkownik from Uzytkownik where Login = 'ktybinkowska'

INSERT INTO [DWOObiekt](ObiektNadrzednyID,DWOZestawUprawnienID,Sygnatura,Nazwa,Opis,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values(@IDDWOObiektNadrzedny,4,'SAK_Flaga','SAK_Flaga',NULL,@UserID,getdate(),NULL,NULL)

select @IDDWOObiekt = scope_identity() 

exec [dbo].[DWODodajUprawnieniaDoObiektu] @IDDWOObiekt, 1

