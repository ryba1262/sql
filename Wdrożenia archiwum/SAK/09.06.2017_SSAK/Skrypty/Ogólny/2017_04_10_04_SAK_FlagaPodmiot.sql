﻿IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SAK_FlagaPodmiot_SAK_Flaga]') AND parent_object_id = OBJECT_ID(N'[dbo].[SAK_FlagaPodmiot]'))
ALTER TABLE [dbo].[SAK_FlagaPodmiot] DROP CONSTRAINT [FK_SAK_FlagaPodmiot_SAK_Flaga]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SAK_FlagaPodmiot]') AND name = N'PK_SAK_FlagaPodmiot')
ALTER TABLE [dbo].[SAK_FlagaPodmiot] DROP CONSTRAINT [PK_SAK_FlagaPodmiot]

--------------------------------------
----------------SAK_FlagaPodmiot---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[SAK_FlagaPodmiot]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[SAK_FlagaPodmiot]
GO

CREATE TABLE [dbo].[SAK_FlagaPodmiot] (
   [IDSAK_FlagaPodmiot] [int]  IDENTITY(1,1) NOT NULL
  ,[SAK_FlagaID] [int]  NOT NULL
  ,[Podmiot] [varchar] (150) NOT NULL
  ,[PodmiotID] [int]  NOT NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO



ALTER TABLE [dbo].[SAK_FlagaPodmiot] ADD  CONSTRAINT [PK_SAK_FlagaPodmiot] PRIMARY KEY CLUSTERED( IDSAK_FlagaPodmiot ) ON [PRIMARY]

ALTER TABLE [dbo].[SAK_FlagaPodmiot] WITH NOCHECK ADD CONSTRAINT [FK_SAK_FlagaPodmiot_SAK_Flaga] FOREIGN KEY([SAK_FlagaID]) REFERENCES [dbo].[SAK_Flaga] ([IDSAK_Flaga])


