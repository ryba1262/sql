﻿--------------------------------------
----------------SAK_SLOUmowaZestawPlatnik---------------
-- Należy zweryfikować constrainty--
--------------------------------------
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SAK_SLOUmowaZestawPlatnik]') AND name = N'IX_SAK_SLOUmowaZestawPlatnik_UmowaZestawID_Kontrah')
DROP INDEX [IX_SAK_SLOUmowaZestawPlatnik_UmowaZestawID_Kontrah] ON [dbo].[SAK_SLOUmowaZestawPlatnik] WITH ( ONLINE = OFF )

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SAK_SLOUmowaZestawPlatnik]') AND name = N'IX_Zestaw_Kontrahent_Netto_Brutto_VAT')
DROP INDEX [IX_Zestaw_Kontrahent_Netto_Brutto_VAT] ON [dbo].[SAK_SLOUmowaZestawPlatnik] WITH ( ONLINE = OFF )

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SAK_SLOUmowaZestawPlatnik]') AND name = N'PK_SAK_SLOUmowaZestawPlatnik')
ALTER TABLE [dbo].[SAK_SLOUmowaZestawPlatnik] DROP CONSTRAINT [PK_SAK_SLOUmowaZestawPlatnik]


if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[SAK_SLOUmowaZestawPlatnik]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[SAK_SLOUmowaZestawPlatnik] (
   [IDSAK_SLOUmowaZestawPlatnik] [int]  IDENTITY(1,1) NOT NULL
  ,[SAK_SLOKontrahentID] [int]  NOT NULL
  ,[SAK_SLOKontrahentOdbiorcaID] [int]  NULL
  ,[SAK_SLOUmowaZestawID] [int]  NOT NULL
  ,[LP] [int]  NULL
  ,[StawkaBrutto] [money]  NULL
  ,[StawkaNetto] [money]  NULL
  ,[StawkaSymbol] [varchar] (10) NULL
  ,[SAK_SLOStawkaVATID] [int]  NULL
  ,[Uwagi] [varchar] (255) NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[SAK_SLOUmowaZestawPlatnik] WITH NOCHECK ADD 
CONSTRAINT [PK_SAK_SLOUmowaZestawPlatnik] PRIMARY KEY  CLUSTERED
(
  IDSAK_SLOUmowaZestawPlatnik
) ON [PRIMARY]


End
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_SAK_SLOUmowaZestawPlatnik] (
   [IDSAK_SLOUmowaZestawPlatnik] [int]  IDENTITY(1,1) NOT NULL
  ,[SAK_SLOKontrahentID] [int]  NOT NULL
  ,[SAK_SLOKontrahentOdbiorcaID] [int]  NULL
  ,[SAK_SLOUmowaZestawID] [int]  NOT NULL
  ,[LP] [int]  NULL
  ,[StawkaBrutto] [money]  NULL
  ,[StawkaNetto] [money]  NULL
  ,[StawkaSymbol] [varchar] (10) NULL
  ,[SAK_SLOStawkaVATID] [int]  NULL
  ,[Uwagi] [varchar] (255) NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[Tmp_SAK_SLOUmowaZestawPlatnik] WITH NOCHECK ADD 
CONSTRAINT [PK_SAK_SLOUmowaZestawPlatnik] PRIMARY KEY  CLUSTERED
(
  IDSAK_SLOUmowaZestawPlatnik
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_SAK_SLOUmowaZestawPlatnik ON

IF EXISTS (SELECT * FROM dbo.SAK_SLOUmowaZestawPlatnik)
EXEC('INSERT INTO dbo.Tmp_SAK_SLOUmowaZestawPlatnik (IDSAK_SLOUmowaZestawPlatnik, SAK_SLOKontrahentID, SAK_SLOUmowaZestawID, LP, StawkaBrutto, StawkaNetto, StawkaSymbol, SAK_SLOStawkaVATID, Uwagi, Dodal, DataDodania, Zmodyfikowal, DataModyfikacji)
  SELECT IDSAK_SLOUmowaZestawPlatnik, SAK_SLOKontrahentID, SAK_SLOUmowaZestawID, LP, StawkaBrutto, StawkaNetto, StawkaSymbol, SAK_SLOStawkaVATID, Uwagi, Dodal, DataDodania, Zmodyfikowal, DataModyfikacji FROM dbo.SAK_SLOUmowaZestawPlatnik WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_SAK_SLOUmowaZestawPlatnik OFF

DROP TABLE dbo.SAK_SLOUmowaZestawPlatnik

EXECUTE sp_rename N'dbo.Tmp_SAK_SLOUmowaZestawPlatnik', N'SAK_SLOUmowaZestawPlatnik', 'OBJECT'


END
CREATE NONCLUSTERED INDEX [IX_Zestaw_Kontrahent_Netto_Brutto_VAT] ON [dbo].[SAK_SLOUmowaZestawPlatnik] (  [SAK_SLOStawkaVATID] ASC )WITH (PAD_INDEX  = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [IX_SAK_SLOUmowaZestawPlatnik_UmowaZestawID_Kontrah] ON [dbo].[SAK_SLOUmowaZestawPlatnik] (  [SAK_SLOStawkaVATID] ASC )WITH (PAD_INDEX  = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY]

ALTER TABLE [dbo].[SAK_SLOUmowaZestawPlatnik] WITH NOCHECK ADD CONSTRAINT [FK_SAK_SLOUmowaPlatnik_SAK_SLOUmowaZestaw] FOREIGN KEY([SAK_SLOUmowaZestawID]) REFERENCES [dbo].[SAK_SLOUmowaZestaw] ([IDSAK_SLOUmowaZestaw])

ALTER TABLE [dbo].[SAK_SLOUmowaZestawPlatnik] WITH NOCHECK ADD CONSTRAINT [FK_SAK_SLOUmowaPlatnik_SAK_SLOStawkaVAT] FOREIGN KEY([SAK_SLOStawkaVATID]) REFERENCES [dbo].[SAK_SLOStawkaVAT] ([IDSAK_SLOStawkaVAT])

ALTER TABLE [dbo].[SAK_SLOUmowaZestawPlatnik] WITH NOCHECK ADD CONSTRAINT [FK_SAK_SLOUmowaPlatnik_SAK_SLOKontrahent] FOREIGN KEY([SAK_SLOKontrahentID]) REFERENCES [dbo].[SAK_SLOKontrahent] ([IDSAK_SLOKontrahent])


