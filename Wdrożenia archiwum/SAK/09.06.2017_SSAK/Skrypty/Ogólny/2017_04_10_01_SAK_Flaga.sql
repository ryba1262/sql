﻿IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SAK_FlagaPodmiot_SAK_Flaga]') AND parent_object_id = OBJECT_ID(N'[dbo].[SAK_FlagaPodmiot]'))
ALTER TABLE [dbo].[SAK_FlagaPodmiot] DROP CONSTRAINT [FK_SAK_FlagaPodmiot_SAK_Flaga]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SAK_Flaga]') AND name = N'PK_SAKFlaga')
ALTER TABLE [dbo].[SAK_Flaga] DROP CONSTRAINT [PK_SAKFlaga]

--------------------------------------
----------------SAK_Flaga---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[SAK_Flaga]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[SAK_Flaga]
GO

CREATE TABLE [dbo].[SAK_Flaga] (
   [IDSAK_Flaga] [int]  NOT NULL
  ,[Symbol] [varchar] (20) NOT NULL
  ,[Nazwa] [varchar] (50) NOT NULL
  ,[Opis] [varchar] (300) NULL
  ,[Lp] [int]  NOT NULL
  ,[Wystepowanie] [int]  NOT NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO




ALTER TABLE [dbo].[SAK_Flaga] ADD  CONSTRAINT [PK_SAKFlaga] PRIMARY KEY CLUSTERED( IDSAK_Flaga ) ON [PRIMARY]



