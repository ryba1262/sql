﻿
-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------SAK_Flaga---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_SAK_Flaga]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_SAK_Flaga] (
   [Log_SAK_FlagaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDSAK_Flaga] [int] 
  ,[Org_Symbol] [varchar] (20)
  ,[Org_Nazwa] [varchar] (50)
  ,[Org_Opis] [varchar] (300)
  ,[Org_Lp] [int] 
  ,[Org_Wystepowanie] [int] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_SAK_Flaga] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_SAK_Flaga] PRIMARY KEY  CLUSTERED
(
   [Log_SAK_FlagaID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_SAK_Flaga] (
   [Log_SAK_FlagaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDSAK_Flaga] [int] 
  ,[Org_Symbol] [varchar] (20)
  ,[Org_Nazwa] [varchar] (50)
  ,[Org_Opis] [varchar] (300)
  ,[Org_Lp] [int] 
  ,[Org_Wystepowanie] [int] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_SAK_Flaga ON

IF EXISTS (SELECT * FROM dbo.LOG_SAK_Flaga)
EXEC('INSERT INTO dbo.Tmp_LOG_SAK_Flaga (Log_SAK_FlagaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDSAK_Flaga, Org_Symbol, Org_Nazwa, Org_Opis, Org_Lp, Org_Wystepowanie, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_SAK_FlagaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDSAK_Flaga, Org_Symbol, Org_Nazwa, Org_Opis, Org_Lp, Org_Wystepowanie, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_SAK_Flaga WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_SAK_Flaga OFF

DROP TABLE dbo.LOG_SAK_Flaga

EXECUTE sp_rename N'dbo.Tmp_LOG_SAK_Flaga', N'LOG_SAK_Flaga', 'OBJECT'

ALTER TABLE [dbo].[LOG_SAK_Flaga] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_SAK_Flaga] PRIMARY KEY  CLUSTERED
(
   [Log_SAK_FlagaID]
) ON [PRIMARY]

END
GO

