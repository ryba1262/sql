﻿
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SAK_WyliczSaldoDlaUmowy]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[SAK_WyliczSaldoDlaUmowy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[SAK_WyliczSaldoDlaUmowy] ( @UmowaID int, @IDDefDok int, @Typ int, @DataOd datetime, @DataDo datetime)
RETURNS float
AS
BEGIN

  declare @Suma float
  ;with salda(naleznosc,zobowiazanie) as
  (
  select case when (RozrachunekTyp = 1 OR RozrachunekTyp = 4) 
              then coalesce(KwotaPoKorekcieWartosc,0) else coalesce(KwotaRozliczonaWartosc,0) end,
         case when (RozrachunekTyp=2 OR RozrachunekTyp =3) 
              then coalesce(KwotaPoKorekcieWartosc,0) else coalesce(KwotaRozliczonaWartosc,0) end
  from SAK_Rozrachunki 
  JOIN SAK_SLOUmowa U on U.IDSAK_SLOUmowa = SAK_Rozrachunki.SAK_SLOUmowaID
  LEFT JOIN SAK_DefinicjaDokumentu dd on dd.IDSAK_DefinicjaDokumentu = SAK_Rozrachunki.SAK_DefinicjaDokumentuID
  where  U.IDSAK_SLOUmowa = @UmowaID
  and ((dd.TypDokumentuID = @typ and @IDDefDok = 0) or (@Typ = 0 and SAK_Rozrachunki.SAK_DefinicjaDokumentuID = @IDDefDok)) 
  and SAK_Rozrachunki.DataDokumentu >= @DataOd and SAK_Rozrachunki.DataDokumentu<=@DataDo
  )
	
  select @Suma = SUM(naleznosc) -SUM(zobowiazanie) from salda	
  return ROUND(COALESCE(@Suma,0),3)

END

GO


