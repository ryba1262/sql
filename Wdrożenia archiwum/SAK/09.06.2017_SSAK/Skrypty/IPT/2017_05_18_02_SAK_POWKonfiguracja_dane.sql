delete from DWOPole where IDDWOPole between 11074 and 11075
delete from DWOJoin where IDDWOJoin between 11016 and 11017

INSERT INTO [DWOJoin](IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(11016,11000,'SAK_SLOKontrahentEmail','left join SAK_SLOKontrahentEmail on SAK_SLOKontrahent.IDSAK_SLOKontrahent = SAK_SLOKontrahentEmail.SAK_SLOKontrahentID',11001,1,452,getdate(),NULL,NULL)

INSERT INTO [DWOJoin](IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(11017,11000,'SAK_SLOKontrahentTelefon','left join SAK_SLOKontrahentTelefon on SAK_SLOKontrahent.IDSAK_SLOKontrahent = SAK_SLOKontrahentTelefon.SAK_SLOKontrahentID and SAK_SLOKontrahentTelefon.CzyTelefonSMS=1 ',11001,1,452,getdate(),NULL,NULL)
    
insert into DWOPole (IDDWOPole, DWOJoinID, NazwaPola, Sygnatura, 
    DefinicjaPola, NazwaPolaSzablon, Klucz, OrderByField, 
    Drzewko, WymaganeDrzewko, Wyswietlane, WymaganeWyswietlane, Dodal, DataDodania)
values (11074, 11016, 'Email', 'KONTRAHENT_EMAIL',
    'SAK_SLOKontrahentEmail.Email', null, null, null,
    1, 0, 1, 0, 452, getdate())         
    
insert into DWOPole (IDDWOPole, DWOJoinID, NazwaPola, Sygnatura, 
    DefinicjaPola, NazwaPolaSzablon, Klucz, OrderByField, 
    Drzewko, WymaganeDrzewko, Wyswietlane, WymaganeWyswietlane, Dodal, DataDodania)
values (11075, 11017, 'Telefon kom�rkowy (SMS)', 'TELEFON_SMS',
    'SAK_SLOKontrahentTelefon.NumerTelefonu', null, null, null,
    1, 0, 1, 0, 452, getdate())          

GO

update DWOPole 
set Sygnatura = 'IMIE'
where IDDWOPole = 11012

update DWOPole 
set Sygnatura = 'NAZWISKO'
where IDDWOPole = 11013



update DWOPole
set Wyswietlane = 1 
where IDDWOPole = 13013



declare @idGrupa int
select @idGrupa = IDPOWGrupaDocelowa from POWGrupaDocelowa where Nazwa = 'Kontrahenci'
if(@idGrupa is not null)
begin
  delete from POWMapaPol where IDPOWMapaPol between 54 and 78
  delete from POWKanal where POWGrupaDocelowaID = @idGrupa
  delete from POWGrupaDocelowa where IDPOWGrupaDocelowa = @idGrupa
end

insert into POWGrupaDocelowa (IDPOWGrupaDocelowa,Nazwa,Sygnatura,Dodal,DataDodania)
values (4,'Kontrahenci', 'KONTRAHENCI',452, getdate())

select @idGrupa = IDPOWGrupaDocelowa from POWGrupaDocelowa where Nazwa = 'Kontrahenci'

insert into POWKanal (IDPOWKanal,POWGrupaDocelowaID,DWOWystepowanieID,Nazwa,TypAdresu,MaxDlugoscTresci,CzyDozwolonePolskieZnaki,CzyWymaganyNadawca,CzyWymaganyTemat,Dodal,DataDodania)
values (10,@idGrupa,11000,'D�ugi SMS z polskimi znakami','SMS',603,1,0,0,452, getdate() )
insert into POWKanal (IDPOWKanal,POWGrupaDocelowaID,DWOWystepowanieID,Nazwa,TypAdresu,MaxDlugoscTresci,CzyDozwolonePolskieZnaki,CzyWymaganyNadawca,CzyWymaganyTemat,Dodal,DataDodania)
values (11,@idGrupa,11000,'D�ugi SMS bez polskich znak�w','SMS',1377,0,0,0,452, getdate() )
insert into POWKanal (IDPOWKanal,POWGrupaDocelowaID,DWOWystepowanieID,Nazwa,TypAdresu,MaxDlugoscTresci,CzyDozwolonePolskieZnaki,CzyWymaganyNadawca,CzyWymaganyTemat,Dodal,DataDodania)
values (12,@idGrupa,11000,'Kr�tki SMS z polskimi znakami','SMS',70,1,0,0,452, getdate() )
insert into POWKanal (IDPOWKanal,POWGrupaDocelowaID,DWOWystepowanieID,Nazwa,TypAdresu,MaxDlugoscTresci,CzyDozwolonePolskieZnaki,CzyWymaganyNadawca,CzyWymaganyTemat,Dodal,DataDodania)
values (13,@idGrupa,11000,'Kr�tki SMS bez polskich znak�w','SMS',160,0,0,0,452, getdate() )
insert into POWKanal (IDPOWKanal,POWGrupaDocelowaID,DWOWystepowanieID,Nazwa,TypAdresu,MaxDlugoscTresci,CzyDozwolonePolskieZnaki,CzyWymaganyNadawca,CzyWymaganyTemat,Dodal,DataDodania)
values (14,@idGrupa,11000,'Wiadomo�� e-mail','Email',8000,1,1,1,452, getdate() )


GO




delete from POWMapaPol where IDPOWMapaPol between 54 and 78

INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(54,10,'ID_KONTRAHENT','ID',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(55,10,'TELEFON_SMS','TelefonSMS',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(56,10,'IMIE','Imie',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(57,10,'NAZWISKO','Nazwisko',0,0,452,getdate(),NULL,NULL)


INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(59,11,'ID_KONTRAHENT','ID',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(60,11,'TELEFON_SMS','TelefonSMS',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(61,11,'IMIE','Imie',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(62,11,'NAZWISKO','Nazwisko',0,0,452,getdate(),NULL,NULL)


INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(64,12,'ID_KONTRAHENT','ID',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(65,12,'TELEFON_SMS','TelefonSMS',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(66,12,'IMIE','Imie',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(67,12,'NAZWISKO','Nazwisko',0,0,452,getdate(),NULL,NULL)


INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(69,13,'ID_KONTRAHENT','ID',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(70,13,'TELEFON_SMS','TelefonSMS',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(71,13,'IMIE','Imie',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(72,13,'NAZWISKO','Nazwisko',0,0,452,getdate(),NULL,NULL)


INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(74,14,'ID_KONTRAHENT','ID',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(75,14,'EMAIL','Email',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(76,14,'IMIE','Imie',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(77,14,'NAZWISKO','Nazwisko',0,0,452,getdate(),NULL,NULL)



GO