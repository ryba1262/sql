set xact_abort on
set nocount on
begin tran

declare @IDSAK_SchematDekretacyjny int ,@Nazwa varchar(100), @Sak_DefinicjaDokumentuID int, @DokumentTyp varchar(50), @RokObrachunkowyID int, @Opis varchar(255),
        @Aktywny bit, @Domyslny bit, @Dane varchar(500), @CzyGrupowaDekretacja bit
declare @nowySchematDekretacyjnyID int, @DefinicjaDokumentuKorektyID int

declare schematDekteracyjny_cursor cursor for
select IDSAK_SchematDekretacyjny,Nazwa, Sak_DefinicjaDokumentuID, DokumentTyp, RokObrachunkowyID, Opis, Aktywny, Domyslny, Dane, CzyGrupowaDekretacja 
from SAK_SchematDekretacyjny where Nazwa like 'Faktura%' and RokObrachunkowyID = 3

OPEN schematDekteracyjny_cursor;
FETCH NEXT FROM schematDekteracyjny_cursor INTO @IDSAK_SchematDekretacyjny,@Nazwa, @Sak_DefinicjaDokumentuID, @DokumentTyp, @RokObrachunkowyID, @Opis, @Aktywny, @Domyslny, @Dane, @CzyGrupowaDekretacja
WHILE @@FETCH_STATUS = 0
   BEGIN

      select @DefinicjaDokumentuKorektyID = SAK_DefinicjaDokumentuKorektyID from SAK_HandelDefinicjaDokumentu where SAK_DefinicjaDokumentuID = @Sak_DefinicjaDokumentuID
      --dodanie schematu dekretacyjnego
      INSERT INTO SAK_SchematDekretacyjny
      (Nazwa, SAK_DefinicjaDokumentuID, DokumentTyp, RokObrachunkowyID, Opis, Aktywny, Domyslny, Dane, CzyGrupowaDekretacja, Dodal, DataDodania)
      SELECT 'Korekta sprzedaży ' + SubString(Nazwa,PATINDEX('%-%',Nazwa), Len(Nazwa)- PATINDEX('%-%',Nazwa)+1), @DefinicjaDokumentuKorektyID, DokumentTyp, @RokObrachunkowyID, 
      'Korekta sprzedaży ' + SubString(Nazwa,PATINDEX('%-%',Nazwa), Len(Nazwa)- PATINDEX('%-%',Nazwa)+1), Aktywny, Domyslny, Dane, CzyGrupowaDekretacja, 452, getdate() 
      FROM SAK_SchematDekretacyjny where IDSAK_SchematDekretacyjny = @IDSAK_SchematDekretacyjny
      
      select @nowySchematDekretacyjnyID = scope_identity()
      
      --dodanie pozycji schematu dekretacyjnego
      INSERT INTO SAK_SchematDekretacyjnyPoz
      (SAK_SchematDekretacyjnyID ,Konto, Strona, Dane, Warunek, Dodal, DataDodania)
      SELECT @nowySchematDekretacyjnyID, Konto, Strona, Dane, '[HandelDokument.SumaBrutto]>0', 452, getdate()
      FROM SAK_SchematDekretacyjnyPoz WHERE SAK_SchematDekretacyjnyID = @IDSAK_SchematDekretacyjny
     
      INSERT INTO SAK_SchematDekretacyjnyPoz
      (SAK_SchematDekretacyjnyID ,Konto, Strona, Dane, Warunek, Dodal, DataDodania)
      SELECT @nowySchematDekretacyjnyID, Konto, case when Strona ='Wn' then 'Ma' else 'Wn' end, Dane, '[HandelDokument.SumaBrutto]<0', 452, getdate()
      FROM SAK_SchematDekretacyjnyPoz WHERE SAK_SchematDekretacyjnyID = @IDSAK_SchematDekretacyjny
     
      
      FETCH NEXT FROM schematDekteracyjny_cursor INTO  @IDSAK_SchematDekretacyjny,@Nazwa, @Sak_DefinicjaDokumentuID, @DokumentTyp, @RokObrachunkowyID, @Opis, @Aktywny, @Domyslny, @Dane, @CzyGrupowaDekretacja
   END;
CLOSE schematDekteracyjny_cursor;
DEALLOCATE schematDekteracyjny_cursor;


commit tran