﻿
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SAK_MiesiaceWgDat]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[SAK_MiesiaceWgDat]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[SAK_MiesiaceWgDat] ( @DataOd datetime, @DataDo datetime)
RETURNS 
@result table (rok int,miesiac int)
AS
BEGIN
 insert into @result
  SELECT TOP (DATEDIFF(MONTH,@DataOd,@DataDo)+1)
EventYear=DATEPART(YEAR,DATEADD(MONTH,ROW_NUMBER()OVER(ORDER BY message_id)-1,@DataOd))
, EventMonth=DATEPART(MONTH,DATEADD(MONTH,ROW_NUMBER()OVER(ORDER BY message_id)-1,@DataOd))
 
FROM sys.messages m 

return
END

GO


