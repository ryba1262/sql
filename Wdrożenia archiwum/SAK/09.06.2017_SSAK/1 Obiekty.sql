-------------wdro1--------------2017_04_26_01_SAK_SaldoDlaUmowy_fun.sql
-------------wdro2--------------2017_04_26_02_SAK_MiesiąceZPrzedzialu_fun.sql
-------------wdro3--------------2017_04_27_01_SAK_Naliczenia_Lista_sald_dla_IPT.sql




-------------------------------------------------------wdro1----------------------------------------------


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SAK_WyliczSaldoDlaUmowy]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[SAK_WyliczSaldoDlaUmowy]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[SAK_WyliczSaldoDlaUmowy] ( @UmowaID int, @IDDefDok int, @Typ int, @DataOd datetime, @DataDo datetime)
RETURNS float
AS
BEGIN

  declare @Suma float
  ;with salda(naleznosc,zobowiazanie) as
  (
  select case when (RozrachunekTyp = 1 OR RozrachunekTyp = 4) 
              then coalesce(KwotaPoKorekcieWartosc,0) else coalesce(KwotaRozliczonaWartosc,0) end,
         case when (RozrachunekTyp=2 OR RozrachunekTyp =3) 
              then coalesce(KwotaPoKorekcieWartosc,0) else coalesce(KwotaRozliczonaWartosc,0) end
  from SAK_Rozrachunki 
  JOIN SAK_SLOUmowa U on U.IDSAK_SLOUmowa = SAK_Rozrachunki.SAK_SLOUmowaID
  LEFT JOIN SAK_DefinicjaDokumentu dd on dd.IDSAK_DefinicjaDokumentu = SAK_Rozrachunki.SAK_DefinicjaDokumentuID
  where  U.IDSAK_SLOUmowa = @UmowaID
  and ((dd.TypDokumentuID = @typ and @IDDefDok = 0) or (@Typ = 0 and SAK_Rozrachunki.SAK_DefinicjaDokumentuID = @IDDefDok)) 
  and SAK_Rozrachunki.DataDokumentu >= @DataOd and SAK_Rozrachunki.DataDokumentu<=@DataDo
  )
	
  select @Suma = SUM(naleznosc) -SUM(zobowiazanie) from salda	
  return ROUND(COALESCE(@Suma,0),3)

END

GO


-------------------------------------------------------wdro2----------------------------------------------



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SAK_MiesiaceWgDat]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[SAK_MiesiaceWgDat]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[SAK_MiesiaceWgDat] ( @DataOd datetime, @DataDo datetime)
RETURNS 
@result table (rok int,miesiac int)
AS
BEGIN
 insert into @result
  SELECT TOP (DATEDIFF(MONTH,@DataOd,@DataDo)+1)
EventYear=DATEPART(YEAR,DATEADD(MONTH,ROW_NUMBER()OVER(ORDER BY message_id)-1,@DataOd))
, EventMonth=DATEPART(MONTH,DATEADD(MONTH,ROW_NUMBER()OVER(ORDER BY message_id)-1,@DataOd))
 
FROM sys.messages m 

return
END

GO


-------------------------------------------------------wdro3----------------------------------------------

declare @UserID int, @GrupaID int, @PodgrupaID int, @RaportID int
declare @Grupa varchar(50), @GrupaOpis varchar(200)
declare @Podgrupa varchar(50), @PodgrupaOpis varchar(200)
declare @Raport varchar(50), @RaportOpis varchar(200)
declare @Kwerenda varchar(MAX), @KwerendaUpdate int, @ParametryUpdate int
declare @LiczbaBand int, @Timeout int

set @Grupa = 'SAK'
set @GrupaOpis = 'Raporty dotyczące SAKa.'
set @Podgrupa = 'Należności'
set @PodgrupaOpis = 'Raporty prezentujące należności w opłatach.'
set @Raport = 'Zestawienie naliczeń i wpłat'
set @RaportOpis = 'Zestawienie naliczeń i wpłat.'
set @LiczbaBand = 16
set @Timeout = 60 * 30 --sekundy

set @ParametryUpdate = 1
set @KwerendaUpdate = 1
set @Kwerenda = '
declare @Kontrahent3ID int, @GrupaUmowID int, @dataOd datetime, @dataDo  datetime
set @Kontrahent3ID = :Kontrahent3
set @GrupaUmowID = :GrupaUmow
set @dataOd = :DataOd
set @dataDo = :DataDo

;with Okresy( DataOd, DataDo)
as
(
select
case when (MONTH(@dataOd)=Miesiac and YEAR(@DataOd) = Rok) then @dataOd else cast((CAST(Rok as varchar) +''-''+ CAST(Miesiac as varchar) + ''-01'')as datetime) end,
case when (MONTH(@dataDo)=Miesiac and YEAR(@DataDo) = Rok) then @dataDo else  DATEADD(month, ((Rok - 1900) * 12) + Miesiac, -1) end  

from dbo.SAK_MiesiaceWgDat(@dataOd,@dataDo)
),
 Umowy(IDUmowa, NazwaUmowy, CzyAktywny)
as
(
select distinct IDSAK_SLOUmowa, Nazwa, case when SAK_FlagaID IS null then ''Nie'' else ''Tak'' end from SAK_SLOUmowa
left join SAK_FlagaPodmiot on Podmiot = ''SAK_SLOUmowa'' and SAK_FlagaID = 1 and PodmiotID = IDSAK_SLOUmowa
where SAK_SLOKontrahent3ID = @Kontrahent3ID and SAK_SLOUmowaGrupaID = @GrupaUmowID and SAK_FlagaID = 1
), UmowaOkres(IDUmowa,NazwaUmowy,CzyAktywny,DataOd, DataDo)
as
(
select  IDUmowa,NazwaUmowy,CzyAktywny,DataOd, DataDo
from
Okresy 
cross join Umowy 
),
SaldaDlaUmow(IDUmowa, NazwaUmowy,CZyAktywny,DataOd, DataDo, SaldoFaktury, SaldoPK, SaldoNK, SaldoNotOdsetkowych, SaldoBO, SaldoWplata, SaldoWyplat, SaldoKP, SaldoKW)
as
(
select IDUmowa, NazwaUmowy, CzyAktywny,DataOd, DataDo, dbo.SAK_WyliczSaldoDlaUmowy(IDUmowa,0,9,DataOd, DataDo), 
 dbo.SAK_WyliczSaldoDlaUmowy(IDUmowa,0,12,DataOd, DataDo), 
 dbo.SAK_WyliczSaldoDlaUmowy(IDUmowa,0,27,DataOd, DataDo),
 dbo.SAK_WyliczSaldoDlaUmowy(IDUmowa,0,13,DataOd, DataDo), 
 dbo.SAK_WyliczSaldoDlaUmowy(IDUmowa,0,29,DataOd, DataDo), 
 dbo.SAK_WyliczSaldoDlaUmowy(IDUmowa,71,0,DataOd, DataDo), 
 dbo.SAK_WyliczSaldoDlaUmowy(IDUmowa,72,0,DataOd, DataDo), 
 dbo.SAK_WyliczSaldoDlaUmowy(IDUmowa,57,0,DataOd, DataDo),
 dbo.SAK_WyliczSaldoDlaUmowy(IDUmowa,58,0,DataOd, DataDo)
from UmowaOkres
), 
SaldaLp(IDUmowa, NazwaUmowy,CzyAktywny,DataOd, DataDo, SaldoFaktury, SaldoPK, SaldoNK, SaldoNotOdsetkowych, SaldoBO,  SaldoKP, SaldoKW,SaldoWplata, SaldoWyplat, SaldoRazem,LP)
as
(
select  IDUmowa, NazwaUmowy, CzyAktywny,DataOd, DataDo,
	SaldoFaktury, SaldoPK,SaldoNK, SaldoNotOdsetkowych, 
 SaldoBO,SaldoKP, SaldoKW,SaldoWplata,SaldoWyplat,
(SaldoFaktury+ SaldoPK+ SaldoNotOdsetkowych + SaldoBO+ SaldoNK +SaldoWplata+SaldoWyplat), ROW_NUMBER() over (partition by IDUmowa order by IDUmowa,DataOd )
 from SaldaDlaUmow --order by IDUmowa, DataOd
 )
 
 select IDUmowa, NazwaUmowy,CzyAktywny,DataOd, DataDo, SaldoFaktury, SaldoPK, SaldoNK, SaldoNotOdsetkowych, SaldoBO,  SaldoKP, SaldoKW,SaldoWplata, SaldoWyplat, SaldoRazem,LP
 from SaldaLp
 order by IDUmowa, DataOd
 
 /*,
 SaldaNarastajaco (IDUmowa, NazwaUmowy,CzyAktywny,DataOd, DataDo, SaldoFaktury, SaldoPK, SaldoNK, SaldoNotOdsetkowych, SaldoBO,  SaldoKP, SaldoKW,SaldoWplata, SaldoWyplat, SaldoRazem,LP, SaldoN)
 as
 (
 select IDUmowa, NazwaUmowy,CzyAktywny,DataOd, DataDo, SaldoFaktury, SaldoPK, SaldoNK, SaldoNotOdsetkowych, SaldoBO,  SaldoKP, SaldoKW,SaldoWplata, SaldoWyplat, SaldoRazem,LP+1, SaldoRazem
 from SaldaLp where LP = 1
 Union all
 select SaldaLp.IDUmowa, SaldaLp.NazwaUmowy,SaldaLp.CzyAktywny,SaldaLp.DataOd, SaldaLp.DataDo, 
 SaldaLp.SaldoFaktury, SaldaLp.SaldoPK, SaldaLp.SaldoNK, SaldaLp.SaldoNotOdsetkowych, SaldaLp.SaldoBO,  SaldaLp.SaldoKP,
  SaldaLp.SaldoKW,SaldaLp.SaldoWplata, SaldaLp.SaldoWyplat, SaldaLp.SaldoRazem,SaldaLp.LP+1, 
 SaldaNarastajaco.SaldoN + SaldaLp.SaldoRazem
 from 
 SaldaNarastajaco
 join SaldaLp on SaldaLP.IDUmowa = SaldaNarastajaco.IDUmowa and SaldaLp.Lp = SaldaNarastajaco.LP
 where SaldaLp.Lp>1
 
 )
 
 select IDUmowa, NazwaUmowy,CzyAktywny,DataOd, DataDo, SaldoFaktury, SaldoPK, SaldoNK, SaldoNotOdsetkowych, SaldoBO,  SaldoKP, SaldoKW,SaldoWplata, SaldoWyplat, SaldoRazem,LP-1, SaldoN as [Saldo Narastająco]
 from SaldaNarastajaco
 order by IDUmowa, DataOd*/
'

set @UserID = coalesce((select IDUzytkownik from Uzytkownik where Login = 'ktybinkowska'),0)
select @GrupaID = IDRAPRaportGrupa from RAPRaportGrupa where Nazwa = @Grupa and RAPRaportGrupaID is NULL

if coalesce(@GrupaID,0) = 0
begin
  insert into RAPRaportGrupa(RAPRaportGrupaID, Nazwa, Opis, Dodal, DataDodania)
    values(NULL, @Grupa, @GrupaOpis, @UserID, GetDate())
  select @GrupaID = scope_identity()
end

select @PodgrupaID = IDRAPRaportGrupa from RAPRaportGrupa where Nazwa = @Podgrupa and RAPRaportGrupaID = @GrupaID

if coalesce(@PodgrupaID,0) = 0
begin
  insert into RAPRaportGrupa(RAPRaportGrupaID, Nazwa, Opis, Dodal, DataDodania)
    values(@GrupaID, @Podgrupa, @PodgrupaOpis, @UserID, GetDate())
  select @PodgrupaID = scope_identity()
end

select @RaportID = IDRAPRaport from RAPRaport where Nazwa = @Raport and RAPRaportGrupaID = @PodgrupaID

if coalesce(@RaportID,0) = 0
begin
  insert into RAPRaport(RAPRaportGrupaID, Nazwa, Opis, Kwerenda, TimeOut, LiczbaBand, Dodal, DataDodania)
    values(@PodgrupaID, @Raport, @RaportOpis, @Kwerenda, @Timeout, @LiczbaBand, @UserID, GetDate())
  select @RaportID = scope_identity()
end
else if @KwerendaUpdate = 1
begin
  update RAPRaport set Kwerenda = @Kwerenda, TimeOut = @Timeout, LiczbaBand = @LiczbaBand, Zmodyfikowal = @UserID, DataModyfikacji = GetDate()
    where IDRAPRaport = @RaportID
end

if @ParametryUpdate = 1
begin
  delete from RAPRaportParametr where RAPRaportID = @RaportID
    insert into RAPRaportParametr(RAPRaportID, LP, Etykieta, Nazwa, Typ, CzyWymagany, CzyWielokrotny, WartoscDomyslna, WartoscMin, WartoscMax, DWOWystepowanieID, WyborKwerenda, SygnaturaIdentyfikator, SygnaturyOpis, Dodal, DataDodania)
    values(@RaportID, 1, 'GrupaUmow', ':GrupaUmow', 'INT', 1, 0, NULL, NULL, NULL, NULL, 'select IDSAK_SLOUmowaGrupa as ID, Nazwa as [GrupaUmow] from SAK_SLOUmowaGrupa', 'ID', 'GrupaUmow', @UserID, GetDate())
	insert into RAPRaportParametr(RAPRaportID, LP, Etykieta, Nazwa, Typ, CzyWymagany, CzyWielokrotny, WartoscDomyslna, WartoscMin, WartoscMax, DWOWystepowanieID, WyborKwerenda, SygnaturaIdentyfikator, SygnaturyOpis, Dodal, DataDodania)
    values(@RaportID, 2, 'Kontrahent3', ':Kontrahent3', 'INT', 1, 0, NULL, NULL, NULL, NULL, 'select distinct IDSAK_SLOKontrahent as ID, Nazwa as Kontrahent3 from SAK_SLOKontrahent where IDSAK_SLOKontrahent in (select SAK_SLOKontrahent3ID from SAK_SLOUmowa) order by Kontrahent3 asc', 'ID', 'Kontrahent3', @UserID, GetDate())
	insert into RAPRaportParametr(RAPRaportID, LP, Etykieta, Nazwa, Typ, CzyWymagany, CzyWielokrotny, WartoscDomyslna, WartoscMin, WartoscMax, DWOWystepowanieID, WyborKwerenda, SygnaturaIdentyfikator, SygnaturyOpis, Dodal, DataDodania)
    values(@RaportID, 3, 'Data od', ':DataOd', 'DATE', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @UserID, GetDate())
	insert into RAPRaportParametr(RAPRaportID, LP, Etykieta, Nazwa, Typ, CzyWymagany, CzyWielokrotny, WartoscDomyslna, WartoscMin, WartoscMax, DWOWystepowanieID, WyborKwerenda, SygnaturaIdentyfikator, SygnaturyOpis, Dodal, DataDodania)
    values(@RaportID, 4, 'Data do', ':DataDo', 'DATE', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @UserID, GetDate())
     
end
