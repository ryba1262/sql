---------------wdro1--------------2017_05_12_01_ESS_NumerKonta_drzewko_dane.sql
---------------wdro2---------------2017_05_22_01_SAK_POWKonfRekrutacja_dane.sql
---------------wdro3----------------2017_05_22_02_SAK_POWKonfEwidencja_dane.sql
---------------wdro4-----------------2017_05_23_01_ESS_DOS_Pensum_RD_Godziny_Z_Planu_Zajec.sql









----------------------------------------------wdro1----------------------------------------------------------


delete from Drzewko where IDDrzewko = 1703
INSERT INTO [Drzewko](IDDrzewko,Nazwa,CzyCzesne,CzyEwidencja,CzyRekrutacja,CzyRaporty,CzyDzialNauki,NazwaPola,NazwaPolaDB,NazwaPolaSzablon,Tabela,KluczObcy,Funkcja,WarunekWhere,Joiny,TabelaObcaID,Priorytet,OrderByField) values(1703,'Numer rachunku aktualny',0,1,1,1,0,'Numer rachunku aktualny','IBAN','SAKNumRach','SAK_RACHNumerRachunku',NULL,NULL,' and SAK_RACHNumerRachunku.CzyKontoAktywne=1','left join SAK_RACHNumerRachunku on SAK_RACHNumerRachunku.PodmiotID = IDIndeks and SAK_RACHNumerRachunku.PodmiotTyp=''Indeks'' ',NULL,NULL,NULL)



go

----------------------------------------------wdro2----------------------------------------------------------




Insert into DWOWariant
(IDDWOWariant, Nazwa,Opis,LP, DWOJoinID,Warunek, CzyPelnyWarunek, CzySprawdzacUprawnienia, Dodal, DataDodania)
values 
(40003, 'Kandydaci', 'Kandydaci', 3, 40001 , 'Indeks.StatusStudentaID = 11 ', 1,0,452,GETDATE())


update DWOPole
set Sygnatura =  'TELEFON_SMS'
where IDDWOPole = 40083



update DWOPole
set Sygnatura =  'EMAIL'
where IDDWOPole = 40084

declare @idGrupa int
select @idGrupa = IDPOWGrupaDocelowa from POWGrupaDocelowa where Nazwa = 'Rekrutacja'
if(@idGrupa is not null)
begin
  delete from POWMapaPol where IDPOWMapaPol between 80 and 104
  delete from POWKanal where POWGrupaDocelowaID = @idGrupa
  delete from POWGrupaDocelowa where IDPOWGrupaDocelowa = @idGrupa
end

insert into POWGrupaDocelowa (IDPOWGrupaDocelowa,Nazwa,Sygnatura,Dodal,DataDodania)
values (5,'Rekrutacja', 'REKRUTACJA',452, getdate())

select @idGrupa = IDPOWGrupaDocelowa from POWGrupaDocelowa where Nazwa = 'Rekrutacja'

insert into POWKanal (IDPOWKanal,POWGrupaDocelowaID,DWOWystepowanieID,Nazwa,TypAdresu,MaxDlugoscTresci,CzyDozwolonePolskieZnaki,CzyWymaganyNadawca,CzyWymaganyTemat,Dodal,DataDodania)
values (15,@idGrupa,40000,'D�ugi SMS z polskimi znakami','SMS',603,1,0,0,452, getdate() )
insert into POWKanal (IDPOWKanal,POWGrupaDocelowaID,DWOWystepowanieID,Nazwa,TypAdresu,MaxDlugoscTresci,CzyDozwolonePolskieZnaki,CzyWymaganyNadawca,CzyWymaganyTemat,Dodal,DataDodania)
values (16,@idGrupa,40000,'D�ugi SMS bez polskich znak�w','SMS',1377,0,0,0,452, getdate() )
insert into POWKanal (IDPOWKanal,POWGrupaDocelowaID,DWOWystepowanieID,Nazwa,TypAdresu,MaxDlugoscTresci,CzyDozwolonePolskieZnaki,CzyWymaganyNadawca,CzyWymaganyTemat,Dodal,DataDodania)
values (17,@idGrupa,40000,'Kr�tki SMS z polskimi znakami','SMS',70,1,0,0,452, getdate() )
insert into POWKanal (IDPOWKanal,POWGrupaDocelowaID,DWOWystepowanieID,Nazwa,TypAdresu,MaxDlugoscTresci,CzyDozwolonePolskieZnaki,CzyWymaganyNadawca,CzyWymaganyTemat,Dodal,DataDodania)
values (18,@idGrupa,40000,'Kr�tki SMS bez polskich znak�w','SMS',160,0,0,0,452, getdate() )
insert into POWKanal (IDPOWKanal,POWGrupaDocelowaID,DWOWystepowanieID,Nazwa,TypAdresu,MaxDlugoscTresci,CzyDozwolonePolskieZnaki,CzyWymaganyNadawca,CzyWymaganyTemat,Dodal,DataDodania)
values (19,@idGrupa,40000,'Wiadomo�� e-mail','Email',8000,1,1,1,452, getdate() )


GO


delete from POWMapaPol where IDPOWMapaPol between 80 and 104

INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(80,15,'IDINDEKS','IdentyfikatorOdbiorcy',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(81,15,'TELEFON_SMS','AdresOdbiorcy',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(82,15,'IMIE','NazwaOdbiorcy1',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(83,15,'NAZWISKO','NazwaOdbiorcy2',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(84,15,'NRREKRUTACJI','Informacja1',0,0,452,getdate(),NULL,NULL)


INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(85,16,'IDINDEKS','IdentyfikatorOdbiorcy',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(86,16,'TELEFON_SMS','AdresOdbiorcy',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(87,16,'IMIE','NazwaOdbiorcy1',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(88,16,'NAZWISKO','NazwaOdbiorcy2',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(89,16,'NRREKRUTACJI','Informacja1',0,0,452,getdate(),NULL,NULL)

INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(90,17,'IDINDEKS','IdentyfikatorOdbiorcy',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(91,17,'TELEFON_SMS','AdresOdbiorcy',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(92,17,'IMIE','NazwaOdbiorcy1',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(93,17,'NAZWISKO','NazwaOdbiorcy2',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(94,17,'NRREKRUTACJI','Informacja1',0,0,452,getdate(),NULL,NULL)


INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(95,18,'IDINDEKS','IdentyfikatorOdbiorcy',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(96,18,'TELEFON_SMS','AdresOdbiorcy',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(97,18,'IMIE','NazwaOdbiorcy1',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(98,18,'NAZWISKO','NazwaOdbiorcy2',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(99,18,'NRREKRUTACJI','Informacja1',0,0,452,getdate(),NULL,NULL)


INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(100,19,'IDINDEKS','IdentyfikatorOdbiorcy',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(101,19,'EMAIL','AdresOdbiorcy',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(102,19,'IMIE','NazwaOdbiorcy1',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(103,19,'NAZWISKO','NazwaOdbiorcy2',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(104,19,'NRREKRUTACJI','Informacja1',0,0,452,getdate(),NULL,NULL)









GO


----------------------------------------------wdro3----------------------------------------------------------




declare @idGrupa int
select @idGrupa = IDPOWGrupaDocelowa from POWGrupaDocelowa where Nazwa = 'Ewidencja student�w'
if(@idGrupa is not null)
begin
  delete from POWMapaPol where IDPOWMapaPol between 105 and 129
  delete from POWKanal where POWGrupaDocelowaID = @idGrupa
  delete from POWGrupaDocelowa where IDPOWGrupaDocelowa = @idGrupa
end

insert into POWGrupaDocelowa (IDPOWGrupaDocelowa,Nazwa,Sygnatura,Dodal,DataDodania)
values (6,'Ewidencja student�w', 'EWIDENCJA_STUDENTOW',452, getdate())

select @idGrupa = IDPOWGrupaDocelowa from POWGrupaDocelowa where Nazwa = 'Ewidencja student�w'

insert into POWKanal (IDPOWKanal,POWGrupaDocelowaID,DWOWystepowanieID,Nazwa,TypAdresu,MaxDlugoscTresci,CzyDozwolonePolskieZnaki,CzyWymaganyNadawca,CzyWymaganyTemat,Dodal,DataDodania)
values (20,@idGrupa,40000,'D�ugi SMS z polskimi znakami','SMS',603,1,0,0,452, getdate() )
insert into POWKanal (IDPOWKanal,POWGrupaDocelowaID,DWOWystepowanieID,Nazwa,TypAdresu,MaxDlugoscTresci,CzyDozwolonePolskieZnaki,CzyWymaganyNadawca,CzyWymaganyTemat,Dodal,DataDodania)
values (21,@idGrupa,40000,'D�ugi SMS bez polskich znak�w','SMS',1377,0,0,0,452, getdate() )
insert into POWKanal (IDPOWKanal,POWGrupaDocelowaID,DWOWystepowanieID,Nazwa,TypAdresu,MaxDlugoscTresci,CzyDozwolonePolskieZnaki,CzyWymaganyNadawca,CzyWymaganyTemat,Dodal,DataDodania)
values (22,@idGrupa,40000,'Kr�tki SMS z polskimi znakami','SMS',70,1,0,0,452, getdate() )
insert into POWKanal (IDPOWKanal,POWGrupaDocelowaID,DWOWystepowanieID,Nazwa,TypAdresu,MaxDlugoscTresci,CzyDozwolonePolskieZnaki,CzyWymaganyNadawca,CzyWymaganyTemat,Dodal,DataDodania)
values (23,@idGrupa,40000,'Kr�tki SMS bez polskich znak�w','SMS',160,0,0,0,452, getdate() )
insert into POWKanal (IDPOWKanal,POWGrupaDocelowaID,DWOWystepowanieID,Nazwa,TypAdresu,MaxDlugoscTresci,CzyDozwolonePolskieZnaki,CzyWymaganyNadawca,CzyWymaganyTemat,Dodal,DataDodania)
values (24,@idGrupa,40000,'Wiadomo�� e-mail','Email',8000,1,1,1,452, getdate() )


GO


delete from POWMapaPol where IDPOWMapaPol between 105 and 129

INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(105,20,'IDINDEKS','IdentyfikatorOdbiorcy',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(106,20,'TELEFON_SMS','AdresOdbiorcy',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(107,20,'IMIE','NazwaOdbiorcy1',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(108,20,'NAZWISKO','NazwaOdbiorcy2',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(109,20,'NRINDEKSU','Informacja1',1,0,452,getdate(),NULL,NULL)


INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(110,21,'IDINDEKS','IdentyfikatorOdbiorcy',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(111,21,'TELEFON_SMS','AdresOdbiorcy',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(112,21,'IMIE','NazwaOdbiorcy1',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(113,21,'NAZWISKO','NazwaOdbiorcy2',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(114,21,'NRINDEKSU','Informacja1',1,0,452,getdate(),NULL,NULL)

INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(115,22,'IDINDEKS','IdentyfikatorOdbiorcy',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(116,22,'TELEFON_SMS','AdresOdbiorcy',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(117,22,'IMIE','NazwaOdbiorcy1',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(118,22,'NAZWISKO','NazwaOdbiorcy2',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(119,22,'NRINDEKSU','Informacja1',1,0,452,getdate(),NULL,NULL)


INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(120,23,'IDINDEKS','IdentyfikatorOdbiorcy',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(121,23,'TELEFON_SMS','AdresOdbiorcy',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(122,23,'IMIE','NazwaOdbiorcy1',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(123,23,'NAZWISKO','NazwaOdbiorcy2',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(124,23,'NRINDEKSU','Informacja1',1,0,452,getdate(),NULL,NULL)


INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(125,24,'IDINDEKS','IdentyfikatorOdbiorcy',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(126,24,'EMAIL','AdresOdbiorcy',1,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(127,24,'IMIE','NazwaOdbiorcy1',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(128,24,'NAZWISKO','NazwaOdbiorcy2',0,0,452,getdate(),NULL,NULL)
INSERT INTO [POWMapaPol](IDPOWMapaPol,POWKanalID,SygnaturaPola,NazwaPolaPowiadomienia,CzyWymagane,CzyInformacjaDodatkowa,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(129,24,'NRINDEKSU','Informacja1',1,0,452,getdate(),NULL,NULL)



GO




----------------------------------------------wdro4----------------------------------------------------------




declare @UserID int, @GrupaID int, @PodgrupaID int, @RaportID int
declare @Grupa varchar(50), @GrupaOpis varchar(200)
declare @Podgrupa varchar(50), @PodgrupaOpis varchar(200)
declare @Raport varchar(50), @RaportOpis varchar(200)
declare @Kwerenda varchar(MAX), @KwerendaUpdate int, @ParametryUpdate int
declare @LiczbaBand int, @Timeout int

set @Grupa = 'DOS'
set @GrupaOpis = 'Raporty dotycz�ce DOS.'
set @Podgrupa = 'Pensum'
set @PodgrupaOpis = 'Pensum'
set @Raport = 'Pensum - Raport RD Godziny z planu zaj��'
set @RaportOpis = 'Pensum - Raport RD Godziny z planu zaj��.'
set @LiczbaBand = 5
set @Timeout = 720 * 10 

set @ParametryUpdate = 1 
set @KwerendaUpdate = 1
set @Kwerenda = '
declare @RokAkad int, @SemestrID int
set @RokAkad = :Rok
set @SemestrID = :Semestr
declare @WynikiRaportu table (Wykadowca varchar(max), PracownikID int, NumerPracownika int, RD varchar(10), Forma varchar(max),Rodzaj varchar(max),Stawka float, LGodzWPensum float,
                              LGodzDoZaplaty float, KwotaDoZaplaty float, Semestr varchar(20), IDRPWniosek int, IDFormaZajec int, IDRPRodzajZajec int)
                              
insert into @WynikiRaportu                              
select 
      (select Nazwisko + '' '' + coalesce(Imie,'''') + coalesce (('' '' + Imie2),'''') +  coalesce('', '' + Tytul.Skrot, '''')  from Pracownik left join Tytul on TytulID=IDTytul 
            where IDPracownik=PracownikID) Wykladowca,   PracownikID,       
      (select PRC_NUMER from PracownikSO  join Pracownik on Pracownik.SOID=PracownikSO.PRC_ID         
            where IDPracownik=PracownikID) NumerPracownika,         
   Sekcja.KodFirma + ''10'' + Kierunek.KodEgeria + TrybStudiow.KodEgeria + SystemStudiow.KodEgeria + Sekcja.KodEgeria RD,         
   FormaZajec.Nazwa Forma, RPRodzajZajec.Nazwa Rodzaj, Stawka,          
   Round((sum(Waga*LGodzWPensum)), 2) LGodzWPensum,         
   Round((sum(Waga*LGodzDoZaplaty)), 2) LGodzDoZaplaty,          
   sum(Waga*LGodzDoZaplaty*Stawka) KwotaDoZaplaty,       
   OkresRozliczeniowyPoz.Nazwa Semestr, IDRPWniosek, IDFormaZajec, IDRPRodzajZajec 
   from RPKODPOZ   
   join RPWniosek on RPWniosekID=IDRPWniosek  
   join OkresRozliczeniowyPoz on SemestrID=IDOkresRozliczeniowyPoz  
   join Sekcja on SekcjaID=IDSekcja  
   join Kierunek on KierunekID=IDKierunek  
   join TrybStudiow on TrybStudiowID=IDTrybStudiow  
   join SystemStudiow on SystemStudiowID=IDSystemStudiow  
   join FormaWymiar on FormaWymiarID=IDFormaWymiar  
   join FormaZajec on FormaZajecID=IDFormaZajec  
   join RPRodzajZajec on RPRodzajZajecID=IDRPRodzajZajec  
   where RokAkad= @RokAkad and  (SemestrID=@SemestrID or @SemestrID=0 ) and
    (ZatwierdzoneRozliczenie=1 or RozliczonyWKolejnymSemestrze=1) 
   group by PracownikID, IDRPWniosek,           
   Sekcja.KodFirma,Kierunek.KodEgeria, TrybStudiow.KodEgeria,            
   SystemStudiow.KodEgeria, Sekcja.KodEgeria,           
   OkresRozliczeniowyPoz.Nazwa,     FormaZajec.Nazwa, RPRodzajZajec.Nazwa, Stawka, IDRPWniosek, IDFormaZajec , IDRPRodzajZajec   
   having sum(LGodz)<>0  order by Wykladowca, Semestr desc
   
   
   
   select Wykadowca, PracownikID, NumerPracownika, RD , Forma,Rodzaj,Stawka, LGodzWPensum,
                              LGodzDoZaplaty, KwotaDoZaplaty, Semestr,
    dbo.PobierzGodzinyWMiesiacu(10,IDRPWniosek,IDFormaZajec,RD, IDRPRodzajZajec) as [pa�dziernik]
   , dbo.PobierzGodzinyWMiesiacu(11,IDRPWniosek,IDFormaZajec,RD, IDRPRodzajZajec) as [listopad]
   , dbo.PobierzGodzinyWMiesiacu(12,IDRPWniosek,IDFormaZajec,RD, IDRPRodzajZajec) as [grudzie�]
   , dbo.PobierzGodzinyWMiesiacu(1,IDRPWniosek,IDFormaZajec,RD, IDRPRodzajZajec) as [stycze�]
   , dbo.PobierzGodzinyWMiesiacu(2,IDRPWniosek,IDFormaZajec,RD, IDRPRodzajZajec) as [luty]
   , dbo.PobierzGodzinyWMiesiacu(3,IDRPWniosek,IDFormaZajec,RD, IDRPRodzajZajec) as [marzec]
   , dbo.PobierzGodzinyWMiesiacu(4,IDRPWniosek,IDFormaZajec,RD, IDRPRodzajZajec) as [kwiecie�]
   , dbo.PobierzGodzinyWMiesiacu(5,IDRPWniosek,IDFormaZajec,RD, IDRPRodzajZajec) as [maj]
   , dbo.PobierzGodzinyWMiesiacu(6,IDRPWniosek,IDFormaZajec,RD, IDRPRodzajZajec) as [czerwiec]
   , dbo.PobierzGodzinyWMiesiacu(7,IDRPWniosek,IDFormaZajec,RD, IDRPRodzajZajec) as [lipiec]
   , dbo.PobierzGodzinyWMiesiacu(8,IDRPWniosek,IDFormaZajec,RD, IDRPRodzajZajec) as [sierpie�]
   , dbo.PobierzGodzinyWMiesiacu(9,IDRPWniosek,IDFormaZajec,RD, IDRPRodzajZajec) as [wrzesie�]
    from @WynikiRaportu
    
    '

set @UserID = coalesce((select IDUzytkownik from Uzytkownik where Login = 'ktybinkowska'),0)
select @GrupaID = IDRAPRaportGrupa from RAPRaportGrupa where Nazwa = @Grupa and RAPRaportGrupaID is NULL

if coalesce(@GrupaID,0) = 0
begin
  insert into RAPRaportGrupa(RAPRaportGrupaID, Nazwa, Opis, Dodal, DataDodania)
    values(NULL, @Grupa, @GrupaOpis, @UserID, GetDate())
  select @GrupaID = scope_identity()
end

select @PodgrupaID = IDRAPRaportGrupa from RAPRaportGrupa where Nazwa = @Podgrupa and RAPRaportGrupaID = @GrupaID

if coalesce(@PodgrupaID,0) = 0
begin
  insert into RAPRaportGrupa(RAPRaportGrupaID, Nazwa, Opis, Dodal, DataDodania)
    values(@GrupaID, @Podgrupa, @PodgrupaOpis, @UserID, GetDate())
  select @PodgrupaID = scope_identity()
end

select @RaportID = IDRAPRaport from RAPRaport where Nazwa = @Raport and RAPRaportGrupaID = @PodgrupaID

if coalesce(@RaportID,0) = 0
begin
  insert into RAPRaport(RAPRaportGrupaID, Nazwa, Opis, Kwerenda, TimeOut, LiczbaBand, Dodal, DataDodania)
    values(@PodgrupaID, @Raport, @RaportOpis, @Kwerenda, @Timeout, @LiczbaBand, @UserID, GetDate())
  select @RaportID = scope_identity()
end
else if @KwerendaUpdate = 1
begin
  update RAPRaport set Kwerenda = @Kwerenda, TimeOut = @Timeout, LiczbaBand = @LiczbaBand, Zmodyfikowal = @UserID, DataModyfikacji = GetDate()
    where IDRAPRaport = @RaportID
end

if @ParametryUpdate = 1
begin
  delete from RAPRaportParametr where RAPRaportID = @RaportID
insert into RAPRaportParametr(RAPRaportID, LP, Etykieta, Nazwa, Typ, CzyWymagany, CzyWielokrotny, WartoscDomyslna, WartoscMin, WartoscMax, DWOWystepowanieID, WyborKwerenda, SygnaturaIdentyfikator, SygnaturyOpis, Dodal, DataDodania)
    values(@RaportID, 1, 'Rok akademicki', ':Rok', 'INT', 1, 0, NULL, NULL, NULL, NULL, 'select Distinct rokAkad as klucz, convert (varchar,rokakad )+ ''/''+convert (varchar,rokakad+1 ) as wartosc from semestrinfo order by rokakad desc','klucz', 'wartosc', @UserID, GetDate())
insert into RAPRaportParametr(RAPRaportID, LP, Etykieta, Nazwa, Typ, CzyWymagany, CzyWielokrotny, WartoscDomyslna, WartoscMin, WartoscMax, DWOWystepowanieID, WyborKwerenda, SygnaturaIdentyfikator, SygnaturyOpis, Dodal, DataDodania)
    values(@RaportID, 1, 'Semestr akademicki', ':Semestr', 'INT', 1, 0, NULL, NULL, NULL, NULL, 'select 0 as klucz, ''zimowy i letni'' as wartosc  union select IDOkresRozliczeniowyPoz as klucz, nazwa as wartosc from okresRozliczeniowyPoz where UczelniaID = 1 ','klucz', 'wartosc', @UserID, GetDate())

end
GO
