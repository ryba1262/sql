----------------------wdro1----------------------2018_03_26_04_ESS_SemestrInfo_Trigger.sql
----------------------wdro2----------------------2018_04_26_03_ESS_ROHistoriaStatusow_trg.sql








-------------------------------------------------wdro1--------------------------------------------------

DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''SemestrInfo_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER SemestrInfo_INSERT
')

EXEC('
CREATE TRIGGER SemestrInfo_INSERT ON ['+@BazaDanych+'].[dbo].[SemestrInfo] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''SemestrInfo'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_SemestrInfo]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDSemestrInfo]
  ,[Org_RokAkad]
  ,[Org_SemestrID]
  ,[Org_DataInauguracji]
  ,[Org_DataRozpoczecia]
  ,[Org_DataKoncowa]
  ,[Org_DataUdostepnieniaProtokolu]
  ,[Org_DataZablokowaniaProtokolu]
  ,[Org_DataSesjiOd]
  ,[Org_DataSesjiDo]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
  ,@ID
 ,ins.IDSemestrInfo
 ,ins.RokAkad
 ,ins.SemestrID
 ,ins.DataInauguracji
 ,ins.DataRozpoczecia
 ,ins.DataKoncowa
 ,ins.DataUdostepnieniaProtokolu
 ,ins.DataZablokowaniaProtokolu
 ,ins.DataSesjiOd
 ,ins.DataSesjiDo
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''SemestrInfo_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER SemestrInfo_UPDATE
')

EXEC('
CREATE TRIGGER SemestrInfo_UPDATE ON ['+@BazaDanych+'].[dbo].[SemestrInfo] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''SemestrInfo'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_SemestrInfo]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDSemestrInfo]
  ,[Org_RokAkad]
  ,[Org_SemestrID]
  ,[Org_DataInauguracji]
  ,[Org_DataRozpoczecia]
  ,[Org_DataKoncowa]
  ,[Org_DataUdostepnieniaProtokolu]
  ,[Org_DataZablokowaniaProtokolu]
  ,[Org_DataSesjiOd]
  ,[Org_DataSesjiDo]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
  ,@ID
 ,ins.IDSemestrInfo
 ,ins.RokAkad
 ,ins.SemestrID
 ,ins.DataInauguracji
 ,ins.DataRozpoczecia
 ,ins.DataKoncowa
 ,ins.DataUdostepnieniaProtokolu
 ,ins.DataZablokowaniaProtokolu
 ,ins.DataSesjiOd
 ,ins.DataSesjiDo
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''SemestrInfo_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER SemestrInfo_DELETE
')

EXEC('
CREATE TRIGGER SemestrInfo_DELETE ON ['+@BazaDanych+'].[dbo].[SemestrInfo] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''SemestrInfo'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_SemestrInfo]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDSemestrInfo]
  ,[Org_RokAkad]
  ,[Org_SemestrID]
  ,[Org_DataInauguracji]
  ,[Org_DataRozpoczecia]
  ,[Org_DataKoncowa]
  ,[Org_DataUdostepnieniaProtokolu]
  ,[Org_DataZablokowaniaProtokolu]
  ,[Org_DataSesjiOd]
  ,[Org_DataSesjiDo]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
  ,@ID
 ,del.IDSemestrInfo
 ,del.RokAkad
 ,del.SemestrID
 ,del.DataInauguracji
 ,del.DataRozpoczecia
 ,del.DataKoncowa
 ,del.DataUdostepnieniaProtokolu
 ,del.DataZablokowaniaProtokolu
 ,del.DataSesjiOd
 ,del.DataSesjiDo
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')

if not exists (select * from LOG_LogowanaTabela where Tabela = 'SemestrInfo')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('SemestrInfo', 1, 0,GetDate())
GO

-----------------------------------------wdro2-----------------------------------------------

DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------ROHistoriaStatusow-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ROHistoriaStatusow_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER ROHistoriaStatusow_INSERT
')

EXEC('
CREATE TRIGGER ROHistoriaStatusow_INSERT ON ['+@BazaDanych+'].[dbo].[ROHistoriaStatusow] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ROHistoriaStatusow'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ROHistoriaStatusow]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDROHistoriaStatusow]
  ,[Org_ROKandydatID]
  ,[Org_ROStatusKandydataID]
  ,[Org_Komentarz]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDROHistoriaStatusow
 ,ins.ROKandydatID
 ,ins.ROStatusKandydataID
 ,ins.Komentarz
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ROHistoriaStatusow_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER ROHistoriaStatusow_UPDATE
')

EXEC('
CREATE TRIGGER ROHistoriaStatusow_UPDATE ON ['+@BazaDanych+'].[dbo].[ROHistoriaStatusow] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ROHistoriaStatusow'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ROHistoriaStatusow]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDROHistoriaStatusow]
  ,[Org_ROKandydatID]
  ,[Org_ROStatusKandydataID]
  ,[Org_Komentarz]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDROHistoriaStatusow
 ,ins.ROKandydatID
 ,ins.ROStatusKandydataID
 ,ins.Komentarz
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ROHistoriaStatusow_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER ROHistoriaStatusow_DELETE
')

EXEC('
CREATE TRIGGER ROHistoriaStatusow_DELETE ON ['+@BazaDanych+'].[dbo].[ROHistoriaStatusow] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ROHistoriaStatusow'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ROHistoriaStatusow]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDROHistoriaStatusow]
  ,[Org_ROKandydatID]
  ,[Org_ROStatusKandydataID]
  ,[Org_Komentarz]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDROHistoriaStatusow
 ,del.ROKandydatID
 ,del.ROStatusKandydataID
 ,del.Komentarz
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'ROHistoriaStatusow')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('ROHistoriaStatusow', 1, 0,GetDate())

go