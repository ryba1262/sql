-------------------wdro1---------------------2018_03_26_03_ESS_SemestrInfo_Log.sql
-------------------wdro2---------------------2018_04_26_02_ESS_ROHistoriaStatusow_log.sql











--------------------------------------------------wdro1--------------------------------------------------

--------------------------------------
----------------SemestrInfo---------------
--------------------------------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_SemestrInfo]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_SemestrInfo] (
   [Log_SemestrInfoID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDSemestrInfo] [int] 
  ,[Org_RokAkad] [smallint] 
  ,[Org_SemestrID] [int] 
  ,[Org_DataInauguracji] [datetime] 
  ,[Org_DataRozpoczecia] [datetime] 
  ,[Org_DataKoncowa] [datetime] 
  ,[Org_DataUdostepnieniaProtokolu] [datetime] 
  ,[Org_DataZablokowaniaProtokolu] [datetime] 
  ,[Org_DataSesjiOd] [datetime] 
  ,[Org_DataSesjiDo] [datetime] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]
ALTER TABLE [dbo].[LOG_SemestrInfo] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_SemestrInfo] PRIMARY KEY  CLUSTERED
(
   [Log_SemestrInfoID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_SemestrInfo] (
   [Log_SemestrInfoID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDSemestrInfo] [int] 
  ,[Org_RokAkad] [smallint] 
  ,[Org_SemestrID] [int] 
  ,[Org_DataInauguracji] [datetime] 
  ,[Org_DataRozpoczecia] [datetime] 
  ,[Org_DataKoncowa] [datetime] 
  ,[Org_DataUdostepnieniaProtokolu] [datetime] 
  ,[Org_DataZablokowaniaProtokolu] [datetime] 
  ,[Org_DataSesjiOd] [datetime] 
  ,[Org_DataSesjiDo] [datetime] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]
SET IDENTITY_INSERT dbo.Tmp_LOG_SemestrInfo ON

IF EXISTS (SELECT * FROM dbo.LOG_SemestrInfo)
EXEC('INSERT INTO dbo.Tmp_LOG_SemestrInfo (Log_SemestrInfoID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDSemestrInfo, Org_RokAkad, Org_SemestrID, Org_DataInauguracji, Org_DataRozpoczecia, Org_DataKoncowa, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_SemestrInfoID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDSemestrInfo, Org_RokAkad, Org_SemestrID, Org_DataInauguracji, Org_DataRozpoczecia, Org_DataKoncowa, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_SemestrInfo WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_SemestrInfo OFF

DROP TABLE dbo.LOG_SemestrInfo

EXECUTE sp_rename N'dbo.Tmp_LOG_SemestrInfo', N'LOG_SemestrInfo', 'OBJECT'

ALTER TABLE [dbo].[LOG_SemestrInfo] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_SemestrInfo] PRIMARY KEY  CLUSTERED
(
   [Log_SemestrInfoID]
) ON [PRIMARY]

END
GO


--------------------------------------------------wdro2--------------------------------------------------



-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------ROHistoriaStatusow---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_ROHistoriaStatusow]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_ROHistoriaStatusow] (
   [Log_ROHistoriaStatusowID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDROHistoriaStatusow] [int] 
  ,[Org_ROKandydatID] [int] 
  ,[Org_ROStatusKandydataID] [int] 
  ,[Org_Komentarz] [varchar] (250)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_ROHistoriaStatusow] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_ROHistoriaStatusow] PRIMARY KEY  CLUSTERED
(
   [Log_ROHistoriaStatusowID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_ROHistoriaStatusow] (
   [Log_ROHistoriaStatusowID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDROHistoriaStatusow] [int] 
  ,[Org_ROKandydatID] [int] 
  ,[Org_ROStatusKandydataID] [int] 
  ,[Org_Komentarz] [varchar] (250)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_ROHistoriaStatusow ON

IF EXISTS (SELECT * FROM dbo.LOG_ROHistoriaStatusow)
EXEC('INSERT INTO dbo.Tmp_LOG_ROHistoriaStatusow (Log_ROHistoriaStatusowID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDROHistoriaStatusow, Org_ROKandydatID, Org_ROStatusKandydataID, Org_Komentarz, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_ROHistoriaStatusowID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDROHistoriaStatusow, Org_ROKandydatID, Org_ROStatusKandydataID, Org_Komentarz, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_ROHistoriaStatusow WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_ROHistoriaStatusow OFF

DROP TABLE dbo.LOG_ROHistoriaStatusow

EXECUTE sp_rename N'dbo.Tmp_LOG_ROHistoriaStatusow', N'LOG_ROHistoriaStatusow', 'OBJECT'

ALTER TABLE [dbo].[LOG_ROHistoriaStatusow] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_ROHistoriaStatusow] PRIMARY KEY  CLUSTERED
(
   [Log_ROHistoriaStatusowID]
) ON [PRIMARY]

END
GO





