-----------------wdro1----------------2018_03_20_01_ESS_WPD_PrzypominanieHasla_dane.sql
-----------------wdro2----------------2018_03_22_01_ESS_SLOTlumaczenieSzablon_dane.sql
-----------------wdro3----------------2018_03_28_01_DWODrzewko_PlanyZajec2_dane.sql
-----------------wdro4----------------2018_04_04_01_DWODrzewko_Protokoly_dane.sql
-----------------wdro5----------------2018_04_09_01_Protokoly_DWOUprawnienia_dane.sql
-----------------wdro6----------------2018_04_11_01_PlanZajec_DWOWariant_dane.sql
-----------------wdro7----------------2018_04_18_01_ESS_Zatrudnienie_TrescMaili_dane.sql
-----------------wdro8----------------2018_04_26_04_Protokoly_Uprawnienia_dane.sql
-----------------wdro9----------------2018_04_26_05_ZmianyStatusow_dane.sql
-----------------wdro10---------------2018_04_26_07_ESS_ROHistoriaStatusow_Status_dla_skopiowanego_z_poprzedniej_edycji_dane.sql
-----------------wdro11---------------2018_04_26_08_ESS_UstawieniaProgramu_ROStatusPoSkopiowaniuZPoprzedniejEdycji_dane.sql







----------------------------------------------------wdro1---------------------------------------------------------------

INSERT INTO [SLOTlumaczenieSzablon](Nazwa,JezykID,Temat,Szablon,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('WPWPrzypomnienieHasla',1,'Przywrócenie hasła','<html><head><meta charset="utf-8"></head>
    <body>
        
		<p>Otrzymaliśmy żądanie resetu hasła do twojego profilu w Wirtualnym Pokoju Dydaktyka AHE.<br />
Aby ustawić nowe hasło prosimy o kliknięcie w link <a href="https://wpd.ahe.lodz.pl/#!/aktywacja_konta/@Model.Token" target="_BLANK">https://wpd.ahe.lodz.pl/#!/aktywacja_konta/@Model.Token</a><br />
Jeśli nie korzystałeś/aś z opcji "Nie pamiętam hasła" w Wirtualnym Pokoju Dydaktyka i nie prosiłeś/aś o to obsługi serwisu zignoruj tę wiadomość.</p>

    </body>
</html>',1044,'2018-03-19',NULL,NULL)
GO





----------------------------------------------------wdro2---------------------------------------------------------------


delete from SLOTlumaczenieSzablon where Nazwa like 'WPDPrzypomnienieHaslaESS' or Nazwa like 'WPDDodanieKonta'

INSERT INTO [SLOTlumaczenieSzablon](Nazwa,JezykID,Temat,Szablon,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('WPDPrzypomnienieHaslaESS',1,'Przypomnienie hasła','Otrzymaliśmy żądanie resetu hasła do twojego profilu w Wirtualnym Pokoju Dydaktyka AHE.\nAby ustawić nowe hasło prosimy o kliknięcie w link  \n@Link \n',452,getdate(),NULL,NULL)
INSERT INTO [SLOTlumaczenieSzablon](Nazwa,JezykID,Temat,Szablon,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('WPDDodanieKonta',1,'Dodanie konta do Wirtualnego Pokoju Dydaktyka','Zostalo dodane konto do Wirtualnego Pokoju Dydaktyka. \nProszę o potwierdzenie adresu e-mail, klikając na link @Link',452,getdate(),NULL,NULL)

go




----------------------------------------------------wdro3---------------------------------------------------------------


delete from DWOPole where IDDWOPole = 80010

delete from DWOJoin where IDDWOJoin = 80011

 
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80011,80000,'Specjalnosc','left join Specjalnosc on Program.SpecjalnoscID = IDSpecjalnosc',80005,3,452,getdate(),NULL,NULL)  
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80010,80011,'Specjalność','SPECJALNOSC','Specjalnosc.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)        
 
 go
 
 
 
 ----------------------------------------------------wdro4---------------------------------------------------------------
 
 
delete from DWOPole where IDDWOPole between 88001 and 88025
delete from DWOWariant where IDDWOWariant = 88001
delete from DWOJoin where IDDWOJoin between 88001 and 88019
delete from DWOWystepowanie where IDDWOWystepowanie = 88000

insert into DWOWystepowanie (IDDWOWystepowanie,MiejsceWystepowania,RegKeyName,MaxLiczbaKolumn,LP,CzySprawdzacUprawnienia,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88000,'Protokoly','Protokoly',30,1,0,452,getdate(),NULL,NULL)
 
 
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88001,88000,'Protokol','from Protokol',NULL,0,452,getdate(),NULL,NULL) 

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88002,88000,'FormaWymiar','left join FormaWymiar on FormaWymiar.IDFormaWymiar = Protokol.FormaWymiarID',88001,1,452,getdate(),NULL,NULL) 

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88003,88000,'SposobRozliczenia','left join SposobRozliczenia on SposobRozliczenia.IDSposobRozliczenia = FormaWymiar.SposobRozliczeniaID',88002,2,452,getdate(),NULL,NULL) 

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88004,88000,'SemestrRealizacji','left join SemestrRealizacji on SemestrRealizacji.IDSemestrRealizacji = FormaWymiar.SemestrRealizacjiID',88002,2,452,getdate(),NULL,NULL) 

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88005,88000,'KartaUzup','left join KartaUzup on KartaUzup.IDKartaUzup = SemestrRealizacji.KartaUzupID',88004,3,452,getdate(),NULL,NULL) 
  
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88006,88000,'Przedmiot','left join Przedmiot on Przedmiot.IDPrzedmiot = KartaUzup.PrzedmiotID',88005,4,452,getdate(),NULL,NULL) 
 
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88007,88000,'FormaZajec','left join FormaZajec on FormaZajec.IDFormaZajec = FormaWymiar.FormaZajecID',88002,2,452,getdate(),NULL,NULL) 
 
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88009,88000,'OkresRozliczeniowyPoz','left join OkresRozliczeniowyPoz on OkresRozliczeniowyPoz.IDOkresRozliczeniowyPoz = Protokol.SemestrID',88001,1,452,getdate(),NULL,NULL)  
 
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88010,88000,'ProtokolPracownik','join ProtokolPracownik on ProtokolPracownik.ProtokolID = Protokol.IDProtokol',88001,1,452,getdate(),NULL,NULL)  
 
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88011,88000,'GrupaZajeciowa','left join GrupaZajeciowa on Protokol.GrupaZajeciowaID = GrupaZajeciowa.IDGrupaZajeciowa',88001,2,452,getdate(),NULL,NULL)  
  
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88012,88000,'ProtokolPoz','join ProtokolPoz  on Protokol.IDProtokol = ProtokolPoz.ProtokolID',88001,1,452,getdate(),NULL,NULL)     
 
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88013,88000,'Pracownik','left join Pracownik on Pracownik.IDPracownik = ProtokolPracownik.PracownikID',88010,2,452,getdate(),NULL,NULL)  
  
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88014,88000,'Tytul','left join Tytul on Pracownik.TytulID = Tytul.IDTytul',88013,3,452,getdate(),NULL,NULL) 
 
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88015,88000,'Kierunek','left join Kierunek on Protokol.KierunekID = Kierunek.IDKierunek',88001,2,452,getdate(),NULL,NULL) 
 
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88016,88000,'TrybStudiow','left join TrybStudiow on Protokol.TrybStudiowID = TrybStudiow.IDTrybStudiow',88001,2,452,getdate(),NULL,NULL) 
 
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88017,88000,'Sekcja','left join Sekcja on Protokol.SekcjaID = Sekcja.IDSekcja',88001,2,452,getdate(),NULL,NULL) 
 
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88018,88000,'SyStemStudiow','left join SystemStudiow on Protokol.SystemStudiowID = SystemStudiow.IDSystemStudiow',88001,2,452,getdate(),NULL,NULL) 
 
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88019,88000,'Wydzial','left join Wydzial on Kierunek.WydzialID =Wydzial.IDWydzial',88015,3,452,getdate(),NULL,NULL) 
 
  
  
insert into DWOWariant (IDDWOWariant,DWOJoinID,Nazwa,Opis,Warunek,CzyPelnyWarunek,CzySprawdzacUprawnienia,LP,Wymagany,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88001,88001,'Wszystkie protokoły','Wszystkie protokoły','1 = 1',1,0,1,0,452,getdate(),NULL,NULL) 


insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88001,88001,'IDProtokol','IDProtokol','Protokol.IDProtokol',NULL,'Protokol.IDProtokol',NULL,1,0,1,1,1,NULL,NULL,452,getdate(),NULL,NULL) 

insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88002,88001,'Rok akad',NULL,'cast(Protokol.RokAkad as varchar(4)) + ''/'' + cast((Protokol.RokAkad+1) as varchar(4))',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL) 
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88003,88009,'Semestr','SEMESTRID','OkresRozliczeniowyPoz.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)  

insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88004,88001,'Protokół - nazwa','PROTOKOLNAZWA','Protokol.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL) 

insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88005,88001,'DataZablokowania','DATAZABLOKOWANIA','Protokol.DataZablokowania',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88006,88001,'CzyZablokowany','CZYZABLOKOWANY','case when Protokol.Zablokowany = 1 then ''TAK'' else ''NIE'' end ',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88007,88002,'Nazwa formy','NAZWAFORMY','FormaWymiar.NazwaFormy',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)     
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88008,88006,'Kod przedmiotu','KODPRZEDMIOTU','Przedmiot.Kod + '' - '' + KartaUzup.KodUzupelniajacy',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)     
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88009,88006,'Nazwa przedmiotu','PRZEDMIOT','Przedmiot.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)     
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88010,88003,'Sposób rozliczenia - nazwa','SPNAZWA','SposobRozliczenia.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)  

insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88011,88003,'Sposób rozliczenia - symbol','SPSYMBOL','SposobRozliczenia.Symbol',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)    

insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88012,88007,'Forma zajęć - nazwa','FZNAZWA','FormaZajec.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   

insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88013,88007,'Forma zajęć - symbol','FZSYMBOL','FormaZajec.Symbol',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   

insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88014,88014,'Pracownik','PRACOWNIK','coalesce(Tytul.SkrotDoPlanu, '''') + '' '' + Pracownik.Nazwisko + '' '' + Pracownik.Imie',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88015,88018,'System studiów - nazwa','SYSTSTUDIOW','SystemStudiow.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88016,88018,'System studiów - symbol','SYSTSTUDIOWSYM','SystemStudiow.Symbol',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   
  
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88017,88016,'Tryb studiów - nazwa','TRYBSTUDIOW','TrybStudiow.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88018,88016,'Tryb studiów - symbol','TRYBSTUDIOWSYM','TrybStudiow.Symbol',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88019,88017,'Sekcja - nazwa','SEKCJA','Sekcja.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88020,88017,'Sekcja - symbol','SEKCJASYM','Sekcja.Symbol',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   
  
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88021,88019,'Kierunek - nazwa','KIERUNEK','''<'' + Wydzial.Symbol + ''>'' + Kierunek.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88022,88019,'Kierunek - symbol','KIERUNEKSYM','''<'' + Wydzial.Symbol + ''>'' + Kierunek.Symbol',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88023,88001,'Czy zajęcia wspólne','CZYZAJECIAWSPOLNE','case when Protokol.CzyZajeciaWspolne = 1 then ''TAK'' else ''NIE'' end',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88024,88001,'Czy dodany automatycznie','CZYDODAUTO','case when Protokol.CzyDodanyAutomatycznie = 1 then ''TAK'' else ''NIE'' end',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (88025,88001,'Czy zablokowany automatycznie','CZYBLOKAUTO','case when Protokol.CzyZablokowanyAutomatycznie = 1 then ''TAK'' else ''NIE'' end',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)
go


----------------------------------------------------wdro5---------------------------------------------------------------

 
 declare @IDDWOObiekt int

select @IDDWOObiekt = IDDWOObiekt from DWOObiekt where Sygnatura = 'SAK_Protokoly'

exec [dbo].[DWOUsunUprawnieniaDoObiektu] @IDDWOObiekt

delete from DWOObiekt where Sygnatura = 'SAK_Protokoly'

declare @IDDWOObiektNadrzedny int
select @IDDWOObiektNadrzedny = IDDWOObiekt from DWOObiekt where Sygnatura = 'IntegracjaZEss'

declare @UserID int
select @UserID = IDUzytkownik from Uzytkownik where Login = 'ktybinkowska'

INSERT INTO [DWOObiekt](ObiektNadrzednyID,DWOZestawUprawnienID,Sygnatura,Nazwa,Opis,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values(@IDDWOObiektNadrzedny,3,'SAK_Protokoly','Protokoły',NULL,coalesce(@UserID,0),getdate(),NULL,NULL)

select @IDDWOObiekt = scope_identity() 

exec [dbo].[DWODodajUprawnieniaDoObiektu] @IDDWOObiekt, 1
go


----------------------------------------------------wdro6---------------------------------------------------------------

delete from DWOWariant where IDDWOWariant = 80004
INSERT INTO [DWOWariant](IDDWOWariant,DWOJoinID,Nazwa,Opis,Warunek,CzyPelnyWarunek,CzySprawdzacUprawnienia,LP,Wymagany,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(80004,80001,'Plany dla roku i semestru','Plany dla roku i semestru','PlanZajec.RokAkad*10+PlanZajec.SemestrID = ',0,0,4,0,452,getdate(),NULL,NULL)
go


----------------------------------------------------wdro7---------------------------------------------------------------


update SLOTlumaczenieSzablon
set
Szablon = '<html><head><meta charset="utf-8"></head><body><p>Otrzymaliśmy żądanie resetu hasła do twojego profilu w Wirtualnym Pokoju Dydaktyka AHE.<BR/>Aby ustawić nowe hasło prosimy o kliknięcie w link  <br/><a href="@Link">@Link</a> </p></body><html>'
where Nazwa = 'WPDPrzypomnienieHaslaESS'


update SLOTlumaczenieSzablon
set
Szablon = '<html><head><meta charset="utf-8"></head><body><p>Zostalo dodane konto do Wirtualnego Pokoju Dydaktyka. <BR/>Proszę o potwierdzenie adresu e-mail, klikając na link <a href = "@Link">@Link</a></p></body></html>'
where Nazwa = 'WPDDodanieKonta'
go



----------------------------------------------------wdro8---------------------------------------------------------------

declare @IDDWOObiekt int, @DWOObiektNadrzednyID int
select @IDDWOObiekt = IDDWOObiekt from DWOObiekt where Sygnatura = 'SAK_Protokoly'

select @IDDWOObiekt = IDDWOObiekt from DWOObiekt where Sygnatura = 'SAK_IntegracjaZEss'

exec [dbo].[DWOUsunUprawnieniaDoObiektu] @IDDWOObiekt

if(@IDDWOObiekt is null)
begin
  INSERT INTO [DWOObiekt](ObiektNadrzednyID,DWOZestawUprawnienID,Sygnatura,Nazwa,Opis,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
  values(@IDDWOObiekt,3,'SAK_Protokoly','Protokoly',NULL,452,GETDATE(),NULL,NULL)

  select @IDDWOObiekt = scope_identity() 
end
exec [dbo].[DWODodajUprawnieniaDoObiektu] @IDDWOObiekt, 1
go



----------------------------------------------------wdro9---------------------------------------------------------------




insert into ROHistoriaStatusow (ROKandydatID, ROStatusKandydataID, Komentarz, Dodal, DataDodania)
select IDROkandydat, ROStatusKandydataID,'stan początkowy',0,coalesce(ROKandydat.DataModyfikacji, getdate())
from ROKandydat
where ROStatusKandydataID is not null and not exists (select IDROHistoriaStatusow from  ROHistoriaStatusow hs where hs.ROKandydatID = ROKandydat.IDROKandydat)
go


----------------------------------------------------wdro10---------------------------------------------------------------
INSERT INTO [ROStatusKandydata](LP,Nazwa,CzyAktywny,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(22,'skopiowany z poprzedniej edycji',1,1613,'2018-04-26',NULL,NULL)
GO



----------------------------------------------------wdro11---------------------------------------------------------------
INSERT INTO [UstawieniaProgramu](IDUstawieniaProgramu,Nazwa,Wartosc,WartoscFloat,WartoscText,CzyBlokowac,TypPola,Opis,IDRodzica) values(1036,'ROStatusPoSkopiowaniuZPoprzedniejEdycji',22,NULL,NULL,0,'EditInt','Identyfikator statusu po skopiowaniu kandydata z poprzedniej edycji.',84)


