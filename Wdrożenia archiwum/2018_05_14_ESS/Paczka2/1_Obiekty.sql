-----------------wdro1----------------2018_03_21_01_ESS_WPD_WPWPlanySzczegolowe_prc.sql
-----------------wdro2----------------2018_03_26_01_ESS_PobierzGodzinyDlaFormy_Fun.sql
-----------------wdro3----------------2018_03_26_02_ESS_SemestrInfo.sql
-----------------wdro4----------------2018_04_11_02_ESS_AktualizujProtokoly_proc.sql
-----------------wdro5----------------2018_04_26_01_ESS_ROHistoriaStatusow.sql
-----------------wdro6----------------2018_04_26_09_ESS_ROKandydat_KopiujDoAktualnejRekrutacji_prc.sql








--------------------------------------------------wdro1----------------------------------------------------

ALTER PROCEDURE [dbo].[WPWPlanySzczegolowe] @pracownikID INT, @DataOD DATE, @DataDO DATE, @SprawdzAktywnosc BIT
AS
select distinct
  [gz].[IDGrupaZajeciowa]
, DATEADD(minute, [ppd].[OdGodz], [ppd].[Data]) AS [DataOD]
, DATEADD(minute, [ppd].[DoGodz], [ppd].[Data]) AS [DataDO]
, [b].[Symbol] + [s].[Numer] AS [Sala]
, [b].[Adres] AS [Adres]
, CASE
	WHEN ([fw].[NazwaFormy] IS NOT NULL AND NOT [fw].[NazwaFormy] = '') THEN [p].[Nazwa] + ': ' + [fw].[NazwaFormy]
	ELSE [p].[Nazwa]
end as [PNazwa]
, [fz].[Symbol6Znakow] as [TypZajecSkrot]
, [fz].[Nazwa] as [TypZajec]
, [fz].[Kolor] as [FormaKolor]
from
					[PlanPozDef] [ppd]
JOIN				[PlanZajecPoz] [pzp]		on [ppd].[PlanZajecPozIDNr] = [pzp].[IDPlanZajecPoz]
LEFT JOIN			[GrupaZajeciowa] [gz]		on [pzp].[GrupaZajeciowaID] = [gz].[IDGrupaZajeciowa]
JOIN				[FormaWymiar] [fw]			on [gz].[FormaWymiarID] = [fw].[IDFormaWymiar]
JOIN				[SemestrRealizacji] [sr]	on [sr].[IDSemestrRealizacji] = [FW].[SemestrRealizacjiID]
JOIN				[KartaUzup] [ku]			on [ku].[IDKartaUzup] = [sr].[KartaUzupID]
JOIN				[Przedmiot] [p]				on [p].[IDPrzedmiot] = [ku].[PrzedmiotID]
LEFT OUTER JOIN		[PlanZajec] [pz]			on [gz].[PlanZajecID] = [pz].[IDPlanZajec]
LEFT JOIN			[Sala] [s]					on [s].[IDSala] = [ppd].[SalaIDNr]
LEFT JOIN			[Budynek] [b]				on [b].[IDBudynek] = [s].[BudynekID]
LEFT JOIN			[PakietZaj] [pakiet]		on [ppd].[PakietIDNr] = [pakiet].[IDPakietZaj]
LEFT JOIN			[FormaZajec] [fz]			on [fz].[IDFormaZajec] = [fw].[FormaZajecID]
WHERE
[ppd].[PracownikIDNr] = @pracownikID
and pz.IDPlanZajec is not null
and  (@SprawdzAktywnosc = 0 OR ([pz].[StatusAktywnyDydaktyk] = 1 OR [pz].[StatusAktywnyDydaktyk] is null))
and [ppd].Data >= @DataOD
and ppd.Data <= @DataDO 
group by [gz].[IDGrupaZajeciowa], [P].[Nazwa], [ppd].[Data], [ppd].[OdGodz], [ppd].[DoGodz], [FW].[NazwaFormy], [B].[Symbol], [S].[Numer], [B].[Adres], [fz].[Symbol6Znakow], [fz].[Nazwa], [fz].[Kolor]

UNION

select distinct
  [gz].[IDGrupaZajeciowa]
, DATEADD(minute, [ppd].[OdGodz], [ppd].[Data]) AS [DataOD]
, DATEADD(minute, [ppd].[DoGodz], [ppd].[Data]) AS [DataDO]
, [b].[Symbol] + [s].[Numer] AS [Sala]
, [b].[Adres] AS [Adres]
, CASE
	WHEN ([fw].[NazwaFormy] IS NOT NULL AND NOT [fw].[NazwaFormy] = '') THEN [p].[Nazwa] + ': ' + [fw].[NazwaFormy]
	ELSE [p].[Nazwa]
end as [PNazwa]
, [fz].[Symbol6Znakow] as [TypZajecSkrot]
, [fz].[Nazwa] as [TypZajec]
, [fz].[Kolor] as [FormaKolor]
from
				[PlanPozDef] [ppd]
JOIN			[PlanZajecPoz] [pzp]			on [ppd].[PlanZajecPozIDNr] = [pzp].[IDPlanZajecPoz]
LEFT JOIN		[GrupaZajeciowa] [gz]			on [pzp].[GrupaZajeciowaID] = [gz].[IDGrupaZajeciowa]
JOIN			[FormaWymiar] [fw]				on [gz].[FormaWymiarID] = [fw].[IDFormaWymiar]
JOIN			[SemestrRealizacji] [sr]		on [sr].[IDSemestrRealizacji] = [FW].[SemestrRealizacjiID]
JOIN			[KartaUzup] [ku]				on [ku].[IDKartaUzup] = [sr].[KartaUzupID]
JOIN			[Przedmiot] [p]					on [p].[IDPrzedmiot] = [ku].[PrzedmiotID]
FULL OUTER JOIN	[ZajeciaWspolne] [zw]			on [gz].IDGrupaZajeciowa = [zw].[IDZajeciaWspolne]
FULL OUTER JOIN	[ZajeciaWspolnePoz] [zwp]		on [zw].[IDZajeciaWspolne] = [zwp].[ZajeciaWspolneID]
FULL OUTER JOIN	[PlanZajec] [pz]				on [zwp].[PlanZajecID] = [pz].[IDPlanZajec]
LEFT JOIN		[Sala] [s]						on [s].[IDSala] = [ppd].[SalaIDNr]
LEFT JOIN		[Budynek] [b]					on [b].[IDBudynek] = [s].[BudynekID]
LEFT JOIN		[PakietZaj] [pakiet]			on [ppd].[PakietIDNr] = [pakiet].[IDPakietZaj]
LEFT JOIN		[FormaZajec] [fz]				on [fz].[IDFormaZajec] = [fw].[FormaZajecID]
LEFT JOIN		[PlanPozPracownik] [ppp]		on [pzp].[IDplanZajecPoz] = [ppp].[planZajecpozid]
where
[ppd].[PracownikIDNr] = @pracownikID
and [pz].[IDPlanZajec] is not null
and  (@SprawdzAktywnosc = 0 OR ([pz].[StatusAktywnyDydaktyk] = 1 OR [pz].[StatusAktywnyDydaktyk] is null))
and [ppd].Data >= @DataOD
and ppd.Data <= @DataDO 
group by [gz].[IDGrupaZajeciowa], [P].[Nazwa], [ppd].[Data], [ppd].[OdGodz], [ppd].[DoGodz], [FW].[NazwaFormy], [B].[Symbol], [S].[Numer], [B].[Adres], [fz].[Symbol6Znakow], [fz].[Nazwa], [fz].[Kolor]

order by [DataOd] desc


GO



--------------------------------------------------wdro2----------------------------------------------------


ALTER function [dbo].[PobierzGodzinyWMiesiacuDlaFormy] (@miesiac int, @idWniosek int, @idFormaWymiar int, @RDRodzajZajecID int, @idFormaZajec int, @idKierunek int)
returns int
as
begin

declare @liczbaGodzin int
set @liczbaGodzin = 0

declare @liczbaGodzin2 int
set @liczbaGodzin2 = 0

declare @liczbaGodzin3 int
set @liczbaGodzin3 = 0

declare @liczbaGodzin4 int
set @liczbaGodzin4 = 0

declare @liczbaGodzin5 int
set @liczbaGodzin5 = 0

select @liczbaGodzin = sum(coalesce(LGodzReal, p.LGodz))
from RPKODPoz
join RPWniosek w on RPWniosekID=IDRPWniosek
left join PlanZajecPoz p on PlanZajecPozIDNR = IDPlanZajecPoz
left join ZajeciaStatus on p.ZajeciaStatusID = IDZajeciaStatus
join Kalendarz k on k.ZajeciaStatusID = IDZajeciaStatus
   join FormaWymiar on FormaWymiarID=IDFormaWymiar  
left join PlanZmianaGodzin pzg on  pzg.PlanZajecPozID = IDPlanZajecPoz and pzg.PracownikID = w.PracownikID and pzg.PlanTypKorektyID = 1 and pzg.Data = k.Data
   where (ZatwierdzoneRozliczenie=1 or RozliczonyWKolejnymSemestrze=1)  
and IDRPWniosek = @idWniosek and CzyKalendarz = 1 and RPKODPoz.FormaWymiarID = @IDFormaWymiar
and MONTH(k.Data) = @miesiac  and RPRodzajZajecID = @RDRodzajZajecID and FormaZajecID = @idFormaZajec
and (PakietZajIDNR is null or (PakietZajIDNR is not null and PakietTopPoz = 1))
and KierunekID = @idKierunek --and RPKODPoz.ZajeciaWspolneIDNR is null

/*

select @liczbaGodzin5 = sum((coalesce(LGodzReal, p.LGodz)))
from RPKODPoz
join RPWniosek w on RPWniosekID=IDRPWniosek
left join GrupaZajeciowa gz on gz.ZajeciaWspolneID = RPKODPoz.ZajeciaWspolneIDNR
join PlanZajecPoz p on GrupaZajeciowaID = IDGrupaZajeciowa
left join ZajeciaStatus on p.ZajeciaStatusID = IDZajeciaStatus
join Kalendarz k on k.ZajeciaStatusID = IDZajeciaStatus
   join FormaWymiar on RPKodPoz.FormaWymiarID=IDFormaWymiar 
left join PlanZmianaGodzin pzg on  pzg.PlanZajecPozID = IDPlanZajecPoz and pzg.PracownikID = w.PracownikID and pzg.PlanTypKorektyID = 1 and pzg.Data = k.Data 

   where (ZatwierdzoneRozliczenie=1 or RozliczonyWKolejnymSemestrze=1)  
and IDRPWniosek = @idWniosek and CzyKalendarz = 1 and RPKODPoz.FormaWymiarID = @IDFormaWymiar
and MONTH(k.Data) = @miesiac  and RPRodzajZajecID = @RDRodzajZajecID and FormaZajecID = @idFormaZajec
and KierunekID = @idKierunek and RPKODPoz.ZajeciaWspolneIDNR is not null and RPKODPoz.ZWTrybTopPoz = 1
*/

select  @liczbaGodzin2 = sum((coalesce(LGodzReal, pz.LGodz))) from RPKODPoz
join RPWniosek w on RPWniosekID=IDRPWniosek
left join PlanZajecPoz pz on PlanZajecPozIDNR = IDPlanZajecPoz
left join ZajeciaStatus on pz.ZajeciaStatusID = IDZajeciaStatus
left join Kalendarz k on k.ZajeciaStatusID = IDZajeciaStatus
left join GrupaZajeciowa on GrupaZajeciowaID = IDGrupaZajeciowa
left join PlanZajec p on PlanZajecID = IDPlanZajec
left join Zjazd on p.ZjazdID = IDZjazd
left join DzienTygodnia on DzienTygodniaID = IDDzienTygodnia
join ZjazdTydzien zt on zt.ZjazdID = IDZjazd
left join ZjazdZmianyPlanu zzp on zzp.ZjazdID = IDZjazd and zzp.PlanZTygNr = NrTygodnia and zzp.PlanZDnia = IDDzienTygodnia and zzp.DzienWolny = 0
left join ZjazdZmianyPlanu zzpw on zzpw.ZjazdID = IDZjazd and zzpw.Data = dbo.ZnajdzDate(DataOd,IDDzienTygodnia) 
   left join PlanZmianaGodzin pzg on pzg.Data = coalesce(zzp.Data,dbo.ZnajdzDate(DataOd,IDDzienTygodnia)) and pzg.PlanZajecPozID = IDPlanZajecPoz 
                                     and pzg.PracownikID = w.PracownikID
  join FormaWymiar on RPKODPoz.FormaWymiarID=IDFormaWymiar  

where  (ZatwierdzoneRozliczenie=1 or RozliczonyWKolejnymSemestrze=1)  
and IDRPWniosek = @idWniosek  and CzyKalendarz = 0 and RPKodPoz.FormaWymiarID = @idFormaWymiar
and  MONTH(coalesce(zzp.Data,dbo.ZnajdzDate(DataOd,IDDzienTygodnia))) = @miesiac and ((cast(zt.NrTygodnia as int)- cast(NzX as int))%cast(CoXTyg as int)  ) = 0
 and RPRodzajZajecID = @RDRodzajZajecID and FormaZajecID = @idFormaZajec
and coalesce(zzp.Data,dbo.ZnajdzDate(DataOd,IDDzienTygodnia))<= OkresDo  and coalesce(zzp.Data,dbo.ZnajdzDate(DataOd,IDDzienTygodnia))>= OkresOd
and (NOT(zzpw.IDZjazdZmianyPlanu is not null and zzp.IDZjazdZmianyPlanu is NULL) )
and KierunekID = @idKierunek
and (PakietZajIDNR is null or (PakietZajIDNR is not null and PakietTopPoz = 1))



select  @liczbaGodzin4 = sum((coalesce(LGodzReal, pz.LGodz))) from RPKODPoz
join RPWniosek w on RPWniosekID=IDRPWniosek
left join PlanZajecPoz pz on PlanZajecPozIDNR = IDPlanZajecPoz
left join ZajeciaStatus on pz.ZajeciaStatusID = IDZajeciaStatus
left join Kalendarz k on k.ZajeciaStatusID = IDZajeciaStatus
left join GrupaZajeciowa on GrupaZajeciowaID = IDGrupaZajeciowa
left join ZajeciaWspolne p on ZajeciaWspolneID = IDZajeciaWspolne
left join Zjazd on p.ZjazdID = IDZjazd
left join DzienTygodnia on DzienTygodniaID = IDDzienTygodnia
join ZjazdTydzien zt on zt.ZjazdID = IDZjazd
left join ZjazdZmianyPlanu zzp on zzp.ZjazdID = IDZjazd and zzp.PlanZTygNr = NrTygodnia and zzp.PlanZDnia = IDDzienTygodnia and zzp.DzienWolny = 0
left join ZjazdZmianyPlanu zzpw on zzpw.ZjazdID = IDZjazd and zzpw.Data = dbo.ZnajdzDate(DataOd,IDDzienTygodnia) 
   left join PlanZmianaGodzin pzg on pzg.Data = coalesce(zzp.Data,dbo.ZnajdzDate(DataOd,IDDzienTygodnia)) and pzg.PlanZajecPozID = IDPlanZajecPoz 
                                     and pzg.PracownikID = w.PracownikID
  join FormaWymiar on RPKODPoz.FormaWymiarID=IDFormaWymiar 

where  (ZatwierdzoneRozliczenie=1 or RozliczonyWKolejnymSemestrze=1)  
and IDRPWniosek = @idWniosek  and CzyKalendarz = 0 and RPKodPoz.FormaWymiarID = @idFormaWymiar
and  MONTH(coalesce(zzp.Data,dbo.ZnajdzDate(DataOd,IDDzienTygodnia))) = @miesiac and ((cast(zt.NrTygodnia as int)- cast(NzX as int))%cast(CoXTyg as int)  ) = 0
 and RPRodzajZajecID = @RDRodzajZajecID and FormaZajecID = @idFormaZajec
and coalesce(zzp.Data,dbo.ZnajdzDate(DataOd,IDDzienTygodnia))<= OkresDo  and coalesce(zzp.Data,dbo.ZnajdzDate(DataOd,IDDzienTygodnia))>= OkresOd
and (NOT(zzpw.IDZjazdZmianyPlanu is not null and zzp.IDZjazdZmianyPlanu is NULL) )
and KierunekID = @idKierunek
and (PakietZajIDNR is null or (PakietZajIDNR is not null and PakietTopPoz = 1))



select @liczbaGodzin3 = sum(pzg.LGodzReal)
from RPKODPoz
join RPWniosek w on RPWniosekID=IDRPWniosek
left join PlanZajecPoz p on PlanZajecPozIDNR = IDPlanZajecPoz
left join ZajeciaStatus on p.ZajeciaStatusID = IDZajeciaStatus
left join PlanZmianaGodzin pzg on  pzg.PlanZajecPozID = IDPlanZajecPoz and pzg.PracownikID = w.PracownikID and pzg.PlanTypKorektyID = 3
   join FormaWymiar on FormaWymiarID=IDFormaWymiar  
    join Sekcja on SekcjaID=IDSekcja  
   join Kierunek on KierunekID=IDKierunek  
   join TrybStudiow on TrybStudiowID=IDTrybStudiow  
   join SystemStudiow on SystemStudiowID=IDSystemStudiow  
where (ZatwierdzoneRozliczenie=1 or RozliczonyWKolejnymSemestrze=1)  
and IDRPWniosek = @idWniosek and CzyKalendarz = 0 and RPKODPoz.FormaWymiarID = @idFormaWymiar and FormaZajecID = @idFormaZajec
and MONTH(pzg.Data) = @miesiac and KierunekID = @idKierunek


RETURN coalesce(@liczbaGodzin,0) + coalesce(@liczbaGodzin2,0) + coalesce(@liczbaGodzin3,0) + coalesce(@liczbaGodzin4,0) + coalesce(@liczbaGodzin5,0)
end

go

-------------------------------------------------wdro3-------------------------------------------------------------

--------------------------------------
----------------SemestrInfo---------------
-- Należy zweryfikować constrainty--
--------------------------------------
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[SemestrInfo]') AND name = N'PK_SemestrInfo')
ALTER TABLE [dbo].[SemestrInfo] DROP CONSTRAINT [PK_SemestrInfo]


if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[SemestrInfo]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[SemestrInfo] (
   [IDSemestrInfo] [int]  IDENTITY(1,1) NOT NULL
  ,[RokAkad] [smallint]  NOT NULL
  ,[SemestrID] [int]  NOT NULL
  ,[DataInauguracji] [datetime]  NOT NULL
  ,[DataRozpoczecia] [datetime]  NULL
  ,[DataKoncowa] [datetime]  NULL
  ,[DataUdostepnieniaProtokolu] [datetime]  NULL
  ,[DataZablokowaniaProtokolu] [datetime]  NULL
  ,[DataSesjiOd] [datetime]  NULL
  ,[DataSesjiDo] [datetime]  NULL
  ,[Dodal] [int]  NULL
  ,[DataDodania] [datetime]  NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[SemestrInfo] WITH NOCHECK ADD 
CONSTRAINT [PK_SemestrInfo] PRIMARY KEY  CLUSTERED
(
  IDSemestrInfo
) ON [PRIMARY]


End
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_SemestrInfo] (
   [IDSemestrInfo] [int]  IDENTITY(1,1) NOT NULL
  ,[RokAkad] [smallint]  NOT NULL
  ,[SemestrID] [int]  NOT NULL
  ,[DataInauguracji] [datetime]  NOT NULL
  ,[DataRozpoczecia] [datetime]  NULL
  ,[DataKoncowa] [datetime]  NULL
  ,[DataUdostepnieniaProtokolu] [datetime]  NULL
  ,[DataZablokowaniaProtokolu] [datetime]  NULL
  ,[DataSesjiOd] [datetime]  NULL
  ,[DataSesjiDo] [datetime]  NULL
  ,[Dodal] [int]  NULL
  ,[DataDodania] [datetime]  NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[Tmp_SemestrInfo] WITH NOCHECK ADD 
CONSTRAINT [PK_SemestrInfo] PRIMARY KEY  CLUSTERED
(
  IDSemestrInfo
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_SemestrInfo ON

IF EXISTS (SELECT * FROM dbo.SemestrInfo)
EXEC('INSERT INTO dbo.Tmp_SemestrInfo (IDSemestrInfo, RokAkad, SemestrID, DataInauguracji, DataRozpoczecia, DataKoncowa, Dodal, DataDodania, Zmodyfikowal, DataModyfikacji)
  SELECT IDSemestrInfo, RokAkad, SemestrID, DataInauguracji, DataRozpoczecia, DataKoncowa, Dodal, DataDodania, Zmodyfikowal, DataModyfikacji FROM dbo.SemestrInfo WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_SemestrInfo OFF

DROP TABLE dbo.SemestrInfo

EXECUTE sp_rename N'dbo.Tmp_SemestrInfo', N'SemestrInfo', 'OBJECT'


END
ALTER TABLE [dbo].[SemestrInfo] WITH NOCHECK ADD CONSTRAINT [FK_SemestrInfo_OkresRozliczeniowyPoz] FOREIGN KEY([SemestrID]) REFERENCES [dbo].[OkresRozliczeniowyPoz] ([IDOkresRozliczeniowyPoz])

go


------------------------------------------------------wdro4------------------------------------------------------



/****** Object:  StoredProcedure [dbo].[AktualizujProtokoly]    Script Date: 03/16/2018 15:04:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AktualizujProtokoly]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AktualizujProtokoly]
GO


/****** Object:  StoredProcedure [dbo].[AktualizujProtokoly]    Script Date: 03/16/2018 15:04:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Katarzyna Gocławska
-- Create date: 2018-03-16
-- Description:	Aktualizacja protokołów
-- =============================================
CREATE PROCEDURE [dbo].[AktualizujProtokoly]
AS
BEGIN
	
	declare @RokAkad int, @SemestrID int, @DataUdostepnienia datetime


select @RokAkad = Wartosc from UstawieniaProgramu where Nazwa = 'PRokAkadOd'
select @SemestrID = Wartosc from UstawieniaProgramu where Nazwa = 'PSemestrOdID'
select @DataUdostepnienia = DataUdostepnieniaProtokolu from SemestrInfo where RokAkad = @RokAkad and SemestrID = @SemestrID


insert into Protokol (ProtokolTypID,GrupaZajeciowaID,Nazwa,RokAkad,SemestrID,FormaWymiarID, SkalaOcenID,Opis,Dodal,DataDodania,DataUdostepnienia,
TrybStudiowID,SystemStudiowID,SekcjaID, KierunekID,CzyZajeciaWspolne,CzyDodanyAutomatycznie)
select IDProtokolTyp,IDGrupaZajeciowa,'Protokol_'+ ProtokolTyp.Nazwa + '_ZajeciaWspolneID_' + CAST(IDZajeciaWspolne as varchar) + '_GZ_' + CAST(IDGrupaZajeciowa as varchar), 
RokAkad, SemestrID, FormaWymiarID, 1,null,0, GETDATE(), @DataUdostepnienia,null, null, null, null,1,1 
from GrupaZajeciowa
join FormaWymiar on GrupaZajeciowa.FormaWymiarID = IDFormaWymiar
left join ZajeciaWspolne on GrupaZajeciowa.ZajeciaWspolneID = IDZajeciaWspolne
join ProtokolTyp on FormaWymiar.SposobRozliczeniaID = ProtokolTyp.SposobRozliczeniaID and (RokAkadOd * 10 + SemestrIDOd) <= (RokAkad*10 + SemestrID)
where ZajeciaWspolneID is not null and RokAkad*10+SemestrID >= @RokAkad*10+@SemestrID and 
not exists (select IDProtokol from Protokol where Protokol.FormaWymiarID = GrupaZajeciowa.FormaWymiarID and Protokol.GrupaZajeciowaID = GrupaZajeciowa.IDGrupaZajeciowa)

UNION
select IDProtokolTyp,IDGrupaZajeciowa,'Protokol_'+ ProtokolTyp.Nazwa + '_PlanZajecID_' + CAST(IDPlanZajec as varchar)+ '_GZ_' + CAST(IDGrupaZajeciowa as varchar),
PlanZajec.RokAkad, SemestrID, FormaWymiarID,1,null,0, GETDATE(), @DataUdostepnienia,  TrybStudiowID, SystemStudiowID,SekcjaID, KierunekID, 0,1
from GrupaZajeciowa
join FormaWymiar on FormaWymiarID = IDFormaWymiar
left join PlanZajec on PlanZajecID = IDPlanZajec
left join Siatka on SiatkaID = IDSiatka
left join Program on ProgramID = IDProgram
join ProtokolTyp on FormaWymiar.SposobRozliczeniaID = ProtokolTyp.SposobRozliczeniaID and (ProtokolTyp.RokAkadOd * 10 + SemestrIDOd) <= (PlanZajec.RokAkad*10 + SemestrID)
where PlanZajecID is not null and PlanZajec.RokAkad*10+SemestrID >= @RokAkad*10+@SemestrID and 
not exists (select IDProtokol from Protokol where Protokol.FormaWymiarID = GrupaZajeciowa.FormaWymiarID and Protokol.GrupaZajeciowaID = GrupaZajeciowa.IDGrupaZajeciowa)


insert into ProtokolPracownik (PracownikID, ProtokolID,CzyDodanyAutomatycznie,CzyBlokowac, Dodal, DataDodania)
select distinct PracownikID,IDProtokol,1,0,0,GETDATE() from Protokol
join PlanZajecPoz on Protokol.GrupaZajeciowaID = PlanZajecPoz.GrupaZajeciowaID
join PlanPozPracownik on PlanZajecPozID = IDPlanZajecPoz
where not exists (select IDProtokolPracownik from ProtokolPracownik pp where pp.ProtokolID = IDProtokol and pp.PracownikID = PlanPozPracownik.PracownikID)


insert into ProtokolOcenaDef (ProtokolID, ProtokolOcenaTypID,RezerwacjaSalID, Termin,Opis,CzyOcenaKoncowa,Waga,DodalTyp,Dodal, DataDodania)
select IDProtokol, ptot.ProtokolOcenaTypID,null, null, null, pot.CzyOcenaKoncowa, LiczbaPunktow,'Uzytkownik',0, GETDATE() 
from Protokol p 
join ProtokolTypOcenaTyp ptot on ptot.ProtokolTypID = p.ProtokolTypID
left join ProtokolOcenaTyp pot on pot.IDProtokolOcenaTyp = ptot.ProtokolOcenaTypID
left join FormaWymiar on FormaWymiarID = IDFormaWymiar
left join SemestrRealizacji on SemestrRealizacjiID = IDSemestrRealizacji
where not exists (select IDProtokolOcenaDef from ProtokolOcenaDef pod where pod.ProtokolID = p.IDProtokol and pod.ProtokolOcenaTypID = ptot.ProtokolOcenaTypID )

insert into ProtokolPoz (ProtokolID, IndeksID,  Zatwierdzona, GrupaZajIndeksID, KartaEgzFormaID, DodalTyp, Dodal, DataDodania, CzyDodanyAutomatycznie,
 FormaWymiarID, TypUczestnictwaID, CzyZatwierdzonyAutomatycznie)
select IDProtokol, IndeksID, 0, IDGrupaZajIndeks, null,  'Uzytkownik',0,GETDATE(), 1,
case when ztu.FormaWymiarID is null then Protokol.FormaWymiarID else ztu.FormaWymiarID end,
 ztu.TypUczestnictwaID, 0 
from GrupaZajIndeks
join Protokol on Protokol.GrupaZajeciowaID = GrupaZajIndeks.GrupaZajeciowaID
left join ZajeciaTypUczestnictwa ztu on ztu.Podmiot = 'GrupaZajIndeks' and ztu.PodmiotID = IDGrupaZajIndeks 
where not exists (select IDProtokolPoz from ProtokolPoz po where po.ProtokolID = IDProtokol and po.IndeksID = GrupaZajIndeks.IndeksID )


insert into ProtokolOcena (ProtokolPozID, ProtokolOcenaDefID, OcenaID, Waga, DodalTyp, Dodal, DataDodania)
select IDProtokolPoz, IDProtokolOcenaDef, null, Waga, 'Uzytkownik',0,GETDATE() 
from GrupaZajIndeks
join ProtokolPoz on ProtokolPoz.GrupaZajIndeksID = GrupaZajIndeks.IDGrupaZajIndeks
join ProtokolOcenaDef pod on pod.ProtokolID = ProtokolPoz.ProtokolID
where not exists (select IDProtokolOcena from ProtokolOcena po where po.ProtokolPozID = IDProtokolPoz and po.ProtokolOcenaDefID = IDProtokolOcenaDef)
END

GO

------------------------------------------------------wdro5------------------------------------------------------



IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ROHistoriaStatusow_ROKandydat]') AND parent_object_id = OBJECT_ID(N'[dbo].[ROHistoriaStatusow]'))
ALTER TABLE [dbo].[ROHistoriaStatusow] DROP CONSTRAINT [FK_ROHistoriaStatusow_ROKandydat]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ROHistoriaStatusow_ROStatusKandydata]') AND parent_object_id = OBJECT_ID(N'[dbo].[ROHistoriaStatusow]'))
ALTER TABLE [dbo].[ROHistoriaStatusow] DROP CONSTRAINT [FK_ROHistoriaStatusow_ROStatusKandydata]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ROHistoriaStatusow]') AND name = N'PK_ROHistoriaStatusow')
ALTER TABLE [dbo].[ROHistoriaStatusow] DROP CONSTRAINT [PK_ROHistoriaStatusow]


--------------------------------------
----------------ROHistoriaStatusow---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[ROHistoriaStatusow]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[ROHistoriaStatusow]
GO

CREATE TABLE [dbo].[ROHistoriaStatusow] (
   [IDROHistoriaStatusow] [int]  IDENTITY(1,1) NOT NULL
  ,[ROKandydatID] [int]  NOT NULL
  ,[ROStatusKandydataID] [int]  NOT NULL
  ,[Komentarz] [varchar] (250) NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO



ALTER TABLE [dbo].[ROHistoriaStatusow] ADD  CONSTRAINT [PK_ROHistoriaStatusow] PRIMARY KEY CLUSTERED( IDROHistoriaStatusow ) ON [PRIMARY]

ALTER TABLE [dbo].[ROHistoriaStatusow] WITH NOCHECK ADD CONSTRAINT [FK_ROHistoriaStatusow_ROStatusKandydata] FOREIGN KEY([ROStatusKandydataID]) REFERENCES [dbo].[ROStatusKandydata] ([IDROStatusKandydata])

ALTER TABLE [dbo].[ROHistoriaStatusow] WITH NOCHECK ADD CONSTRAINT [FK_ROHistoriaStatusow_ROKandydat] FOREIGN KEY([ROKandydatID]) REFERENCES [dbo].[ROKandydat] ([IDROKandydat])
go


------------------------------------------------------wdro6------------------------------------------------------


-- ******************* -- ROKandydat_KopiujDoAktualnejRekrutacji -- ******************* --

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ROKandydat_KopiujDoAktualnejRekrutacji]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ROKandydat_KopiujDoAktualnejRekrutacji]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		Łukasz Wizner
-- Create date: 2014-05-12
-- Description:	KopiujeDaneKandydataDoAktualnejRekrutacji
-- =============================================
CREATE PROCEDURE [dbo].[ROKandydat_KopiujDoAktualnejRekrutacji]
	@kandydatID int, 
	@email varchar(50), 
	@statusPoczatkowyKandydata int,
	@preferowanySystemStudiow varchar(30)
AS
BEGIN
	DECLARE @id INT;

	INSERT INTO [dbo].[ROKandydat]
       SELECT [dbo].[ROOstatniaRekrutacjaID](getdate())
      ,null
      ,[Imie]
      ,[DrugieImie]
      ,[Nazwisko]
      ,[NazwiskoPanienskie]
      ,[DataUrodzenia]
      ,[MiejsceUrodzeniaPolska]
      ,[MiejsceUrodzeniaID]
      ,CASE WHEN [MiejsceUrodzeniaMiejscowosc] is null then (select Nazwa from SlownikMiejscowosc where IDSlownikMiejscowosc = [MiejsceUrodzeniaID]) ELSE [MiejsceUrodzeniaMiejscowosc] END
      ,[MiejsceUrodzeniaGminaID]
      ,[MiejsceUrodzeniaObcokrajowcy]
      ,[PanstwoUrodzeniaID]
      ,[Plec]
      ,[ImieOjca]
      ,[ImieMatki]
      ,[NazwiskoPanienskieMatki]
      ,[StanCywilnyID]
      ,[ObywatelstwoID]
      ,[NarodowoscID]
      ,[PESEL]
      ,[DTSeriaNumer]
      ,[DTWydanyPrzez]
      ,[DTDataWydania]
      ,[DTNazwa]
      ,[DTDataWaznosci]
      ,[DOSeriaNumer]
      ,[DOWydanyPrzez]
      ,[DODataWydania]
      ,[DODataWaznosci]
      ,[NIP]
      ,[Pracuje]
      ,[StatusNiepelnosprawnosci]
      ,[DataOrzeczeniaNiepelnosprawnosciDo]
      ,[HasloHash]
      ,@email
      ,[TelefonKom]
      ,[TelefonSMS]
      ,[TelefonSMSKodKraju]
      ,[AdresKorPolska]
      ,[AdresKorPanstwoID]
      ,[AdresKorMiejscowoscID]
      ,CASE WHEN [AdresKorMiejscowosc] is null then (select Nazwa from SlownikMiejscowosc where IDSlownikMiejscowosc = [AdresKorMiejscowoscID]) ELSE [AdresKorMiejscowosc] END
      ,[AdresKorGminaID]
      ,[AdresKorKod]
      ,[AdresKorUlica]
      ,[AdresKorNrDomu]
      ,[AdresKorNrMieszkania]
      ,[AdresKorTelefon]
      ,[AdresKorObcokrajowcy]
      ,[AdresSPPolska]
      ,[AdresSPPanstwoID]
      ,[AdresSPMiejscowoscID]
      ,CASE WHEN [AdresSPMiejscowosc] is null then (select Nazwa from SlownikMiejscowosc where IDSlownikMiejscowosc = [AdresSPMiejscowoscID]) ELSE [AdresSPMiejscowosc] END
      ,[AdresSPGminaID]
      ,[AdresSPKod]
      ,[AdresSPUlica]
      ,[AdresSPNrDomu]
      ,[AdresSPNRMieszkania]
      ,[AdresSPTelefon]
      ,[AdresSPObcokrajowcy]
      ,[TenSamAdresKorISP]
      ,[SzkolaSredniaID]
      ,[SzkolaSredniaNazwa]
      ,[SzkolaSredniaMiejscowoscID]
      ,CASE WHEN [SzkolaSredniaMiejscowosc] is null then (select Nazwa from SlownikMiejscowosc where IDSlownikMiejscowosc = [SzkolaSredniaMiejscowoscID]) ELSE [SzkolaSredniaMiejscowosc] END
      ,[SzkolaSredniaGminaID]
      ,[SzkolaSredniaPanstwoID]
      ,[SzkolaSredniaTypID]
      ,[SzkolaSredniaDataSwiadM]
      ,[SzkolaSredniaDataUkonczenia]
      ,[SzkolaSredniaNrSwiadectwaM]
      ,[SzkolaSredniaLKlas]
      ,[OkregowaKomisjaEgzaminacyjnaID]
      ,[UczelniaUkonID]
      ,[UczelniaUkonWydzialID]
      ,[UczelniaUkonKierunekID]
      ,[UczelniaUkonNazwa]
      ,[UczelniaUkonWydzial]
      ,[UczelniaUkonKierunek]
      ,[UczelniaUkonPanstwoID]
      ,[UczelniaUkonMiejscowoscID]
      ,CASE WHEN [UczelniaUkonMiejscowosc] is null then (select Nazwa from SlownikMiejscowosc where IDSlownikMiejscowosc = [UczelniaUkonMiejscowoscID]) ELSE [UczelniaUkonMiejscowosc] END
      ,[UczelniaUkonGminaID]
      ,[UczelniaUkonNrDyplomu]
      ,[UczelniaUkonDataUkon]
      ,[UczelniaUkonLSemestrow]
      ,[UczelniaUkonOkresStudiowOd]
      ,[UczelniaUkonOkresStudiowDo]
      ,[ROTytulZawodowyID]
      ,[UczelniaUkonTytulZawodowy]
      ,[WKUID]
      ,[WKUNazwa]
      ,[WKUMiejscowoscID]
      ,CASE WHEN [WKUMiejscowosc] is null then (select Nazwa from SlownikMiejscowosc where IDSlownikMiejscowosc = [WKUMiejscowoscID]) ELSE [WKUMiejscowosc] END
      ,[WKUGminaID]
      ,[KategoriaWojskowaID]
      ,[StopienWojskowyID]
      ,[ZwolnionyOdPOSW]
      ,[KWSeriaNumer]
      ,[PrzeniesionyDoRezerwyDnia]
      ,[NrSpecjalnosciWojskowej]
      ,[JezykPodstawowyID]
      ,[JezykDodatkowyID]
      ,[JezykPodstawowyPoziomID]
      ,[JezykDodatkowyPoziomID]
      ,[ZakladPracyNazwa]
      ,[ZakladPracyTelefon]
      ,[ZakladPracyUlica]
      ,[ZakladPracyPanstwoID]
      ,[ZakladPracyMiejscowoscID]
      ,[ZakladPracyGminaID]
      ,CASE WHEN [ZakladPracyMiejscowosc] is null then (select Nazwa from SlownikMiejscowosc where IDSlownikMiejscowosc = [ZakladPracyMiejscowoscID]) ELSE [ZakladPracyMiejscowosc] END
      ,[ZakladPracyStanowisko]
      ,[ZakladPracyZawod]
      ,[ZrodloDochoduID]
      ,null
      ,[ZgodaPrzetwDanychRekrutacjaIStudia]
      ,[ZgodaPrzetwDanychPocztaISMS]
      ,[ZgodaPrzetwDanychMarketingIPromocje]
      ,[ESNiepelnosprawnoscKMinisterialnaID]
      ,[ESNiepelnosprawnoscKGUSID]
      ,@preferowanySystemStudiow
      ,@statusPoczatkowyKandydata
      ,null
      ,null
      ,null
      ,[Dodal]
      ,getdate()
      ,null
      ,null
  FROM [dbo].[ROKandydat]
  WHERE IDROKandydat = @kandydatID
  
  SET @id = SCOPE_IDENTITY()

	INSERT INTO ROHistoriaStatusow(
		ROKandydatID, ROStatusKandydataID, Komentarz, Dodal,DataDodania
	)
	VALUES
	(
		@id, @statusPoczatkowyKandydata, 'przeniesienie konta z poprzedniej rekrutacji, poprzedni IDKandydat:' + CAST(@kandydatID AS VARCHAR(10)),  0, GETDATE()
	)

	SELECT @id
END





GO


