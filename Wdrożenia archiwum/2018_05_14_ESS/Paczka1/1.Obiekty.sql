
----------wdro1------2018_03_08_01_ESS_SredniaArytmetyczna_fun
----------wdro2------2018_03_09_01_ESS_KE_ListaFormPrzedmiotu_fun
----------wdro3------2018_03_16_01_ESS_ProtokolTyp
----------wdro4------2018_03_16_05_ESS_ProtokolOcenaTyp
----------wdro5------2018_03_16_09_ESS_ProtokolTypOcenaTyp
----------wdro6------2018_03_16_13_ESS_Protokol.sql
----------wdro7------2018_03_16_16_ESS_ProtokolOcenaDef.sql
----------wdro8------2018_03_16_19_01_ESS_ProtokolZdarzenie.sql
----------wdro9------2018_03_16_19_ESS_ProtokolPowod.sql
----------wdro10-----2018_03_16_22_ESS_ProtokolPoz.sql
----------wdro11-----2018_03_16_25_ESS_ProtokolOcena.sql
----------wdro12-----2018_03_16_28_01_ESS_ProtokolPracownik.sql
----------wdro13-----2018_03_16_29_ESS_ProtokolKorekta.sql
----------wdro14-----2018_03_16_32_ESS_ProtokolKolizjaTyp.sql
----------wdro15-----2018_03_16_35_ESS_ProtokolKolizja.sql





--------------------------------------------------------wdro1-----------------------------------------------------


-- ******************* -- 2018_03_08_01_ESS_SredniaArytmetyczna_fun -- ******************* --






SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

-- =============================================
-- Author:		Katarzyna Goc�awska
-- Create date: 2015-08-18
-- Description:	Funkcja do obliczania �redniej arytmetycznej semestralnej, rocznej i �redniej ze studi�w
-- =============================================

ALTER FUNCTION [dbo].[KartaEgzSredniaArytmetyczna] (@IndeksID int, @RokAkad int, @SemestrID int, @SemestrStudiow int, @CzyRoczna bit, @CzyPoczatkowa bit, @CzyDecyzja bit, @CzyKartaPoczatkowa bit)
RETURNS float
AS
BEGIN

  DECLARE @Srednia float, @KERokAkadOd int, @KESemestrIDOd int

  select @KERokAkadOd = Wartosc from UstawieniaProgramu where Nazwa = 'KERokAkadOd'
  select @KESemestrIDOd = Wartosc from UstawieniaProgramu where Nazwa = 'KESemestrOdID'
  
  IF(@CzyKartaPoczatkowa = 1)
  BEGIN
      SET @Srednia = NULL
      RETURN @Srednia
  END  

  DECLARE @CzyZatwierdzona bit
  DECLARE @RozliczonaWarunkowo int

  IF(@RokAkad is not NULL) -- �rednia sem lub roczna
   BEGIN
       SELECT @CzyZatwierdzona = Zatwierdzona, @RozliczonaWarunkowo = RozliczonaWarunkowo
       FROM KartaEgz WHERE RokAkad = @RokAkad AND SemestrID = @SemestrID AND SemestrStudiow = @SemestrStudiow AND IndeksID = @IndeksID
   END
  ELSE
   BEGIN  -- srednia ze studi�w
       SELECT @CzyZatwierdzona = Zatwierdzona, @RozliczonaWarunkowo = RozliczonaWarunkowo
       FROM KartaEgz 
       LEFT JOIN OkresRozliczeniowyPoz orp on SemestrID = IDOkresRozliczeniowyPoz
       WHERE IndeksID = @IndeksID and (RokAkad*10+orp.LP) = (SELECT Max(RokAkad*10+okp.LP) FROM KartaEgz LEFT JOIN OkresRozliczeniowyPoz okp on SemestrID = IDOkresRozliczeniowyPoz WHERE IndeksID = @IndeksID)
   END


  DECLARE @RokOd int , @RokDo int, @WhrRokAkad int, @WhrSemestrID int
  DECLARE @SemestrOd int, @SemestrDo int
  DECLARE @SemS int  -- semestr studiow w przypadku sredniej semestralnej ustawiony, w innych przypadkach NULL
  SET @SemS = NULL
  
  	-- ostatnie wyzerowanie historii rozlicze�
	select  top (1)
			@WhrRokAkad = RokAkademicki,
			@WhrSemestrID = SemestrAkadID
	from HistoriaStudenta
	where ZdarzenieID = 32
	and Anulowany = 0
	and IndeksID = @IndeksID
	and ((@RokAkad is null and @SemestrID is null) or (RokAkademicki * 100 + SemestrAkadID <= @RokAkad * 100 + @SemestrID))
	order by RokAkademicki * 100 + SemestrAkadID desc 

  IF(@RozliczonaWarunkowo = 0 AND @CzyZatwierdzona = 1)
    BEGIN  -- rejestracja pe�na - karta zatwierdzona
     
       -- Wyznaczenie zakresow pobierania przedmiotow
       IF(@CzyRoczna = 0 AND @RokAkad is not NULL) --srednia semestralna
         BEGIN
            SET @RokDo = @RokAkad
            SET @SemestrDo = @SemestrID
            SET @SemS = @SemestrStudiow
            
            SELECT TOP 1 @RokOd = RokAkad, @SemestrOd = SemestrID FROM KartaEgz
            LEFT JOIN OkresRozliczeniowyPoz orp on SemestrID = IDOkresRozliczeniowyPoz            
            WHERE SemestrStudiow = @SemestrStudiow AND IndeksID = @IndeksID
            and RokAkad * 100 + SemestrID >= coalesce(@WhrRokAkad, 0) * 100 + coalesce(@WhrSemestrID, 0)
            ORDER BY RokAkad*10+orp.LP ASC 
         END
       ELSE IF(@CzyRoczna = 1 AND @RokAkad is not NULL)  --srednia roczna
         BEGIN
            IF(@SemestrStudiow %2=0)   
              BEGIN
                  SET @RokDo = @RokAkad
                  SET @SemestrDo = @SemestrID
                  
                  SELECT TOP 1 @RokOd = RokAkad, @SemestrOd = SemestrID FROM KartaEgz 
                  LEFT JOIN OkresRozliczeniowyPoz orp on SemestrID = IDOkresRozliczeniowyPoz
                  WHERE SemestrStudiow = (@SemestrStudiow-1) AND IndeksID = @IndeksID
                  and RokAkad * 100 + SemestrID >= coalesce(@WhrRokAkad, 0) * 100 + coalesce(@WhrSemestrID, 0)
                  ORDER BY RokAkad*10+orp.LP ASC 
                  
                  IF(((@SemestrStudiow-2)/2)+((@SemestrStudiow-2)%2) = 0) 
                  BEGIN
                     SELECT TOP 1 @RokOd = RokAkad, @SemestrOd = SemestrID FROM KartaEgz 
                     LEFT JOIN OkresRozliczeniowyPoz orp on SemestrID = IDOkresRozliczeniowyPoz
                     WHERE SemestrStudiow = (@SemestrStudiow-2) AND IndeksID = @IndeksID
                     and RokAkad * 100 + SemestrID >= coalesce(@WhrRokAkad, 0) * 100 + coalesce(@WhrSemestrID, 0)
                     ORDER BY RokAkad*10+orp.LP ASC 
                  END 
              END
            ELSE  -- srednia roczna z semestru nieparzystego = NULL
              BEGIN
                 SET @Srednia = NULL              
                 RETURN @Srednia
              END
         END
       ELSE  --srednia ze studiow
         BEGIN
            SELECT TOP 1 @RokOd = RokAkad, @SemestrOd = SemestrID FROM KartaEgz
            LEFT JOIN OkresRozliczeniowyPoz orp on SemestrID = IDOkresRozliczeniowyPoz
            WHERE IndeksID = @IndeksID
            and RokAkad * 100 + SemestrID >= coalesce(@WhrRokAkad, 0) * 100 + coalesce(@WhrSemestrID, 0)
            ORDER BY RokAkad*10+orp.LP ASC 

            SELECT TOP 1 @RokDo = RokAkad, @SemestrDo = SemestrID FROM KartaEgz 
            LEFT JOIN OkresRozliczeniowyPoz orp on SemestrID = IDOkresRozliczeniowyPoz
            WHERE IndeksID = @IndeksID
            and RokAkad * 100 + SemestrID >= coalesce(@WhrRokAkad, 0) * 100 + coalesce(@WhrSemestrID, 0)
            ORDER BY RokAkad*10+orp.LP DESC
         END  
      
      -- pobiera przedmioty z kart w zawezonym okresie
      DECLARE @TabelaPrzedmiotow TABLE (IDSemReal int ) 
      INSERT @TabelaPrzedmiotow  
      SELECT IDSemReal FROM [dbo].[KartaEgzHistoriaRozliczen] (@IndeksID, @RokOd,@SemestrOd, @RokDo, @SemestrDo,@CzyPoczatkowa, @SemS)      

      DECLARE @TabelaObliczenia TABLE(IDSemReal int, Punkty int, Ocena float, CzyZalicza bit, CzyOcena bit, NumerWiersza int, RokAkad int, SemestrID int, FormaWymiarID int, 
      CzyZaliczaNowe bit, CzyNoweKarty bit, OcenaCz float, CzyOcenaCz bit) 
      
      IF(@CzyPoczatkowa = 1)
      BEGIN
        INSERT @TabelaObliczenia
         SELECT IDSemestrRealizacji, LiczbaPunktow, SkalaOcenPoz.Wartosc,SkalaOcenPoz.CzyZalicza, SkalaOcenPoz.CzyOcena, ROW_NUMBER() over (partition by IDSemestrRealizacji order by FormaWymiarID asc), RokAkad, SemestrID, FormaWymiarID,
        case when (select COUNT(*) from KartaEgzForma 
		           left join SkalaOcenPoz on OcenaID = IDSkalaOcenPoz
		           JOIN FormaWymiar ON KartaEgzForma.FormaWymiarID = FormaWymiar.IDFormaWymiar
                   LEFT JOIN FormaZajec ON FormaWymiar.FormaZajecID = FormaZajec.IDFormaZajec
		           where KartaEgzForma.IDKartaEgzForma = kaf.IDKartaEgzForma	and CzyDrukowacNaKarcieEgz = 1 and (CzyZalicza <> 0 or CzyZalicza  is not null) and CzyPoprawkowy = 0)>0 then 1 else 0 end CzyZaliczaNowe, 
		case when RokAkad*10+SemestrID >=@KERokAkadOd*10+@KESemestrIDOd then 1 else 0 end CzyNoweKarty, skocz.Wartosc, skocz.CzyOcena        
        FROM KartaEgzPoz
        join KartaEgzForma kaf on kaf.KartaEgzPozID = IDKartaEgzPoz
        LEFT JOIN KartaEgz ON KartaEgzID = IDKartaEgz
        LEFT JOIN SemestrRealizacji  ON SemestrRealizacjiID = IDSemestrRealizacji
        LEFT JOIN SkalaOcenPoz ON OcenaKoncowaID = IDSkalaOcenPoz
        left join SkalaOcenPoz skocz on kaf.OcenaID = skocz.IDSkalaOcenPoz
        WHERE IndeksID = @IndeksID AND Zatwierdzona = 1 /*AND CzyZalicza = 1 AND CzyOcena=1*/ AND SemestrRealizacjiID IN (SELECT IDSemReal FROM @TabelaPrzedmiotow) AND 
              ((RokAkad*10 + SemestrID) BETWEEN (@RokOd * 10 + @SemestrOd) AND (@RokDo * 10 + @SemestrDo))           
      END
      ELSE
      BEGIN
        INSERT @TabelaObliczenia
         SELECT IDSemestrRealizacji, LiczbaPunktow, SkalaOcenPoz.Wartosc,SkalaOcenPoz.CzyZalicza, SkalaOcenPoz.CzyOcena, ROW_NUMBER() over (partition by IDSemestrRealizacji order by FormaWymiarID asc), RokAkad, SemestrID, FormaWymiarID ,  
        case when (select COUNT(*) from KartaEgzForma 
		           left join SkalaOcenPoz on OcenaID = IDSkalaOcenPoz
		           JOIN FormaWymiar ON KartaEgzForma.FormaWymiarID = FormaWymiar.IDFormaWymiar
                   LEFT JOIN FormaZajec ON FormaWymiar.FormaZajecID = FormaZajec.IDFormaZajec
		           where KartaEgzForma.IDKartaEgzForma = kaf.IDKartaEgzForma and CzyDrukowacNaKarcieEgz = 1 and (CzyZalicza<> 0 or CzyZalicza  is not null) and CzyPoprawkowy = 0)>0 then 1 else 0 end CzyZaliczaNowe, 
		case when RokAkad*10+SemestrID >=@KERokAkadOd*10+@KESemestrIDOd then 1 else 0 end CzyNoweKarty, skocz.Wartosc, skocz.CzyOcena     
        FROM KartaEgzPoz
        join KartaEgzForma kaf on kaf.KartaEgzPozID = IDKartaEgzPoz
        LEFT JOIN KartaEgz ON KartaEgzID = IDKartaEgz
        LEFT JOIN SemestrRealizacji  ON SemestrRealizacjiID = IDSemestrRealizacji
        LEFT JOIN SkalaOcenPoz ON OcenaKoncowaID = IDSkalaOcenPoz
        left join SkalaOcenPoz skocz on kaf.OcenaID = skocz.IDSkalaOcenPoz
        WHERE IndeksID = @IndeksID AND Zatwierdzona = 1 /*AND CzyZalicza = 1 AND CzyOcena=1*/ AND SemestrRealizacjiID IN (SELECT IDSemReal FROM @TabelaPrzedmiotow) AND 
              ((RokAkad*10 + SemestrID) BETWEEN (@RokOd * 10 + @SemestrOd) AND (@RokDo * 10 + @SemestrDo)) AND              
              (KartaPoczatkowa is NULL or KartaPoczatkowa = 0)
      END 

      IF(@CzyDecyzja = 1)
      BEGIN
         DECLARE @Korekta TABLE(ObowiazujacaLPunktow int, SemestrRealizacjiID int, KRokAkad int, KSemestrID int, Data datetime)
         INSERT @Korekta
         SELECT ObowiazujacaLPunktow,SemestrRealizacjiID,RokAkad, SemestrID, Data FROM KorektaLiczbyPunktowPoz
         JOIN KorektaLiczbyPunktow ON (KorektaLiczbyPunktowID=IDKorektaLiczbyPunktow AND KorektaLiczbyPunktow.IndeksID = @IndeksID)
         JOIN HistoriaStudenta ON (TabelaID=IDKorektaLiczbyPunktow AND ZdarzenieID=33 AND HistoriaStudenta.IndeksID = @IndeksID)
         WHERE SemestrRealizacjiID IN (SELECT IDSemReal FROM @TabelaPrzedmiotow) AND HistoriaStudenta.Anulowany = 0 
		 AND ((RokAkad*10 + SemestrID) BETWEEN (@RokOd * 10 + @SemestrOd) AND (@RokDo * 10 + @SemestrDo))

         UPDATE @TabelaObliczenia 
		 SET Punkty =
		 (
			SELECT TOP 1 ObowiazujacaLPunktow 
			FROM @Korekta 
            WHERE SemestrRealizacjiID = IDSemReal AND KRokAkad = RokAkad AND KSemestrID = SemestrID 
            ORDER BY Data DESC
		 )
         WHERE IDSemReal in (select SemestrRealizacjiID from @Korekta) 
      END

	  declare @SumaOceny float, @SumaIlosc int
	  select @SumaOceny = SUM(Ocena) from @TabelaObliczenia where CzyZalicza = 1 and CzyOcena = 1 and CzyNoweKarty = 0 and NumerWiersza = 1
	  select @SumaIlosc = Count(distinct IDSemReal) from @TabelaObliczenia where CzyZalicza = 1 and CzyOcena = 1 and CzyNoweKarty = 0

      declare @SumaOcenyN float, @SumaIloscN int
	  select @SumaOcenyN = SUM(OcenaCz) from @TabelaObliczenia where CzyNoweKarty = 1 and CzyZaliczaNowe = 1 and CzyOcenaCz = 1
	  select @SumaIloscN = Count(distinct FormaWymiarID) from @TabelaObliczenia where CzyNoweKarty = 1 and CzyZaliczaNowe = 1 and CzyOcenaCz = 1

	  set @Srednia = case when (@SumaIlosc + @SumaIloscN) > 0 then (coalesce(@SumaOceny,0)+ coalesce(@SumaOcenyN,0)) / (@SumaIlosc+ @SumaIloscN) else 0 end                
   END
  ELSE
   BEGIN
      SET @Srednia = NULL
   END

	
  RETURN ROUND(COALESCE(@Srednia,0),3)

END

go


--------------------------------------------------------wdro2-----------------------------------------------------


-- ******************* -- 2018_03_09_01_ESS_KE_ListaFormPrzedmiotu_fun -- ******************* --




ALTER FUNCTION [dbo].[KE_ListaFormPrzedmiotu] (@IndeksID int, @RokAkad int, @SemestrID int, @CzyPoprawkowy bit)  
RETURNS @Output table (SemestrRealizacjiID int, IDFormaWymiar int, NazwaFormy varchar(500), Przedmiot varchar(500), FormaZajec varchar(10), Kod varchar(100))
BEGIN

	
 declare @WyzerowanieRokAkad int, @WyzerowanieSemAkad int
  select @WyzerowanieRokAkad=RokAkademicki,
         @WyzerowanieSemAkad=SemestrAkadID
  from HistoriaStudenta
  where IndeksID=@IndeksID and ZdarzenieID=32 and Anulowany=0
  order by RokAkademicki desc,  SemestrAkadID desc


  -- szukamy poprzedniej karty egz.
  declare @KartaEgzPoprzID int, @SemStudenta int
  select @SemStudenta=Semestr from Indeks where idindeks=@IndeksID


    declare @OldRok int, @oldSemLp int, @oldSem int
    select top 1 @KartaEgzPoprzID = IDKartaEgz, @OldRok=RokAkad, @OldSemLp=Lp, @OldSem=SemestrStudiow  from KartaEgz
    join OkresRozliczeniowyPoz on SemestrID=IDOkresRozliczeniowyPoz
    where IndeksID=@IndeksID and ((RokAkad*10+Lp < @RokAkad*10+@SemestrID) or
                                  (RokAkad*10+Lp <= @RokAkad*10+@SemestrID) and KartaPoczatkowa=1) 
          and RokAkad*10+Lp >= coalesce(@WyzerowanieRokAkad,0)*10+coalesce(@WyzerowanieSemAkad,0)
    order by RokAkad desc, Lp desc, KartaPoczatkowa asc

  
   insert into @Output
    select KartaEgzPoz.SemestrRealizacjiID, FormaWymiarID, NazwaFormy, Przedmiot.Nazwa as Przedmiot, FormaZajec.Symbol6Znakow,  przedmiot.Kod + ' - ' + KartaUzup.KodUzupelniajacy 
		from KartaEgzPoz
    join KartaEgzForma on KartaEgzPozID = IDKartaEgzPoz
    left join SkalaOcenPoz on OcenaID=IDSkalaOcenPoz
    left join FormaWymiar on FormaWymiarID = IDFormaWymiar
    left join SemestrRealizacji on KartaEgzPoz.SemestrRealizacjiID = IDSemestrRealizacji
    left join KartaUzup on KartaUzupID = IDKartaUzup
    left join Przedmiot on PrzedmiotID = IDPrzedmiot
    left join FormaZajec on FormaZajecID = IDFormaZajec
    where KartaEgzID=@KartaEgzPoprzID and coalesce(CzyZalicza,0) = 0 and @CzyPoprawkowy = 1 and CzyDrukowacNaKarcieEgz = 1
    UNION
    select krpp.SemestrRealizacjiID, IDFormaWymiar, NazwaFormy, Przedmiot.Nazwa as Przedmiot, FormaZajec.Symbol6Znakow,  przedmiot.Kod + ' - ' + KartaUzup.KodUzupelniajacy 
    from KartaRoznicProgramowych
    left join KartaRoznicProgramowychPoz krpp on KartaRoznicProgramowychID = IDKartaRoznicProgramowych
    left join SemestrRealizacji on SemestrRealizacjiID = IDSemestrRealizacji
    left join KartaUzup on KartaUzupID = IDKartaUzup
    left join Przedmiot on PrzedmiotID = IDPrzedmiot
    join FormaWymiar on FormaWymiar.SemestrRealizacjiID = IDSemestrRealizacji
    left join FormaZajec on FormaZajecID = IDFormaZajec
    where IndeksID = @IndeksID and @CzyPoprawkowy = 0 and CzyDrukowacNaKarcieEgz = 1

	return

END
go

-----------------------------------------------------wdro3-----------------------------------------------------


-- ******************* -- 2018_03_16_01_ESS_ProtokolTyp -- ******************* --


IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Protokol_ProtokolTyp]') AND parent_object_id = OBJECT_ID(N'[dbo].[Protokol]'))
ALTER TABLE [dbo].[Protokol] DROP CONSTRAINT [FK_Protokol_ProtokolTyp]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolTyp_SposobRozliczenia]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolTyp]'))
ALTER TABLE [dbo].[ProtokolTyp] DROP CONSTRAINT [FK_ProtokolTyp_SposobRozliczenia]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolTypOcenaTyp_ProtokolTyp]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolTypOcenaTyp]'))
ALTER TABLE [dbo].[ProtokolTypOcenaTyp] DROP CONSTRAINT [FK_ProtokolTypOcenaTyp_ProtokolTyp]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ProtokolTyp]') AND name = N'PK__Protokol__A16A3D3B6ABD69FD')
ALTER TABLE [dbo].[ProtokolTyp] DROP CONSTRAINT [PK__Protokol__A16A3D3B6ABD69FD]

--------------------------------------
----------------ProtokolTyp---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[ProtokolTyp]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[ProtokolTyp]
GO

CREATE TABLE [dbo].[ProtokolTyp] (
   [IDProtokolTyp] [int]  IDENTITY(1,1) NOT NULL
  ,[Nazwa] [varchar] (255) NOT NULL
  ,[RokAkadOd] [int]  NOT NULL
  ,[SemestrIDOd] [int]  NOT NULL
  ,[RokAkadDo] [int]  NULL
  ,[SemestrIDDo] [int]  NULL
  ,[SposobRozliczeniaID] [int]  NOT NULL
  ,[AlgorytmWyliczania] [varchar] (255) NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO





ALTER TABLE [dbo].[ProtokolTyp] ADD  CONSTRAINT [PK__Protokol__A16A3D3B6ABD69FD] PRIMARY KEY CLUSTERED( IDProtokolTyp ) ON [PRIMARY]

ALTER TABLE [dbo].[ProtokolTyp] WITH NOCHECK ADD CONSTRAINT [FK_ProtokolTyp_SposobRozliczenia] FOREIGN KEY([SposobRozliczeniaID]) REFERENCES [dbo].[SposobRozliczenia] ([IDSposobRozliczenia])


go

----------------------------------------wdro4----------------------------------------------

-- ******************* -- 2018_03_16_05_ESS_ProtokolOcenaTyp -- ******************* --



IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolOcenaDef_ProtokolOcenaTyp]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolOcenaDef]'))
ALTER TABLE [dbo].[ProtokolOcenaDef] DROP CONSTRAINT [FK_ProtokolOcenaDef_ProtokolOcenaTyp]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolTypOcenaTyp_ProtokolOcenaTyp]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolTypOcenaTyp]'))
ALTER TABLE [dbo].[ProtokolTypOcenaTyp] DROP CONSTRAINT [FK_ProtokolTypOcenaTyp_ProtokolOcenaTyp]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ProtokolOcenaTyp]') AND name = N'PK_ProtokolOcenaTyp')
ALTER TABLE [dbo].[ProtokolOcenaTyp] DROP CONSTRAINT [PK_ProtokolOcenaTyp]


--------------------------------------
----------------ProtokolOcenaTyp---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[ProtokolOcenaTyp]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[ProtokolOcenaTyp]
GO

CREATE TABLE [dbo].[ProtokolOcenaTyp] (
   [IDProtokolOcenaTyp] [int]  IDENTITY(1,1) NOT NULL
  ,[Nazwa] [varchar] (255) NOT NULL
  ,[CzyOcenaKoncowa] [bit]  NOT NULL
  ,[CzyPrzenoszonaNaKarteEgz] [bit]  NOT NULL
  ,[Waga] [smallint]  NOT NULL
  ,[Lp] [smallint]  NOT NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO




ALTER TABLE [dbo].[ProtokolOcenaTyp] ADD  CONSTRAINT [PK_ProtokolOcenaTyp] PRIMARY KEY CLUSTERED( IDProtokolOcenaTyp ) ON [PRIMARY]



go


----------------------------------------wdro5----------------------------------------------

-- ******************* -- 2018_03_16_09_ESS_ProtokolTypOcenaTyp -- ******************* --



IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolTypOcenaTyp_ProtokolTyp]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolTypOcenaTyp]'))
ALTER TABLE [dbo].[ProtokolTypOcenaTyp] DROP CONSTRAINT [FK_ProtokolTypOcenaTyp_ProtokolTyp]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolTypOcenaTyp_ProtokolOcenaTyp]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolTypOcenaTyp]'))
ALTER TABLE [dbo].[ProtokolTypOcenaTyp] DROP CONSTRAINT [FK_ProtokolTypOcenaTyp_ProtokolOcenaTyp]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ProtokolTypOcenaTyp]') AND name = N'PK_ProtokolTypOcenaTyp')
ALTER TABLE [dbo].[ProtokolTypOcenaTyp] DROP CONSTRAINT [PK_ProtokolTypOcenaTyp]



--------------------------------------
----------------ProtokolTypOcenaTyp---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[ProtokolTypOcenaTyp]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[ProtokolTypOcenaTyp]
GO

CREATE TABLE [dbo].[ProtokolTypOcenaTyp] (
   [IDProtokolTypOcenaTyp] [int]  IDENTITY(1,1) NOT NULL
  ,[ProtokolOcenaTypID] [int]  NOT NULL
  ,[ProtokolTypID] [int]  NOT NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO



ALTER TABLE [dbo].[ProtokolTypOcenaTyp] ADD  CONSTRAINT [PK_ProtokolTypOcenaTyp] PRIMARY KEY CLUSTERED( IDProtokolTypOcenaTyp ) ON [PRIMARY]

ALTER TABLE [dbo].[ProtokolTypOcenaTyp] WITH NOCHECK ADD CONSTRAINT [FK_ProtokolTypOcenaTyp_ProtokolTyp] FOREIGN KEY([ProtokolTypID]) REFERENCES [dbo].[ProtokolTyp] ([IDProtokolTyp])

ALTER TABLE [dbo].[ProtokolTypOcenaTyp] WITH NOCHECK ADD CONSTRAINT [FK_ProtokolTypOcenaTyp_ProtokolOcenaTyp] FOREIGN KEY([ProtokolOcenaTypID]) REFERENCES [dbo].[ProtokolOcenaTyp] ([IDProtokolOcenaTyp])


go

--------------------------------------------------wdro6------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Protokol_FormaWymiar]') AND parent_object_id = OBJECT_ID(N'[dbo].[Protokol]'))
ALTER TABLE [dbo].[Protokol] DROP CONSTRAINT [FK_Protokol_FormaWymiar]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Protokol_OkresRozliczeniowyPoz]') AND parent_object_id = OBJECT_ID(N'[dbo].[Protokol]'))
ALTER TABLE [dbo].[Protokol] DROP CONSTRAINT [FK_Protokol_OkresRozliczeniowyPoz]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Protokol_ProtokolTyp]') AND parent_object_id = OBJECT_ID(N'[dbo].[Protokol]'))
ALTER TABLE [dbo].[Protokol] DROP CONSTRAINT [FK_Protokol_ProtokolTyp]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Protokol_SkalaOcen]') AND parent_object_id = OBJECT_ID(N'[dbo].[Protokol]'))
ALTER TABLE [dbo].[Protokol] DROP CONSTRAINT [FK_Protokol_SkalaOcen]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolOcenaDef_Protokol]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolOcenaDef]'))
ALTER TABLE [dbo].[ProtokolOcenaDef] DROP CONSTRAINT [FK_ProtokolOcenaDef_Protokol]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolPoz_Protokol]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolPoz]'))
ALTER TABLE [dbo].[ProtokolPoz] DROP CONSTRAINT [FK_ProtokolPoz_Protokol]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolPracownik_Protokol]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolPracownik]'))
ALTER TABLE [dbo].[ProtokolPracownik] DROP CONSTRAINT [FK_ProtokolPracownik_Protokol]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Protokol]') AND name = N'PK_Protokol')
ALTER TABLE [dbo].[Protokol] DROP CONSTRAINT [PK_Protokol]


--------------------------------------
----------------Protokol---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[Protokol]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[Protokol]
GO

CREATE TABLE [dbo].[Protokol] (
   [IDProtokol] [int]  IDENTITY(1,1) NOT NULL
  ,[ProtokolTypID] [int]  NOT NULL
  ,[GrupaZajeciowaID] [int]  NULL
  ,[KursGrupaZajID] [int]  NULL
  ,[Nazwa] [varchar] (255) NOT NULL
  ,[RokAkad] [int]  NOT NULL
  ,[SemestrID] [int]  NOT NULL
  ,[FormaWymiarID] [int]  NOT NULL
  ,[SkalaOcenID] [int]  NOT NULL
  ,[Opis] [varchar] (500) NULL
  ,[DataZablokowania] [datetime]  NULL
  ,[Zablokowany] [bit]  NULL
  ,[CzyZablokowanyAutomatycznie] [bit]  NULL
  ,[DataUdostepnienia] [datetime]  NULL
  ,[TrybStudiowID] [int]  NULL
  ,[SystemStudiowID] [int]  NULL
  ,[SekcjaID] [int]  NULL
  ,[KierunekID] [int]  NULL
  ,[CzyZajeciaWspolne] [bit]  NULL
  ,[CzyDodanyAutomatycznie] [bit]  NOT NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[Protokol] ADD  CONSTRAINT [PK_Protokol] PRIMARY KEY CLUSTERED( IDProtokol ) ON [PRIMARY]

ALTER TABLE [dbo].[Protokol] WITH NOCHECK ADD CONSTRAINT [FK_Protokol_SkalaOcen] FOREIGN KEY([SkalaOcenID]) REFERENCES [dbo].[SkalaOcen] ([IDSkalaOcen])

ALTER TABLE [dbo].[Protokol] WITH NOCHECK ADD CONSTRAINT [FK_Protokol_ProtokolTyp] FOREIGN KEY([ProtokolTypID]) REFERENCES [dbo].[ProtokolTyp] ([IDProtokolTyp])

ALTER TABLE [dbo].[Protokol] WITH NOCHECK ADD CONSTRAINT [FK_Protokol_OkresRozliczeniowyPoz] FOREIGN KEY([SemestrID]) REFERENCES [dbo].[OkresRozliczeniowyPoz] ([IDOkresRozliczeniowyPoz])

ALTER TABLE [dbo].[Protokol] WITH NOCHECK ADD CONSTRAINT [FK_Protokol_FormaWymiar] FOREIGN KEY([FormaWymiarID]) REFERENCES [dbo].[FormaWymiar] ([IDFormaWymiar])


go

--------------------------------------------------wdro7--------------------------------------------

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolOcena_ProtokolOcenaDef]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolOcena]'))
ALTER TABLE [dbo].[ProtokolOcena] DROP CONSTRAINT [FK_ProtokolOcena_ProtokolOcenaDef]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolOcenaDef_Protokol]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolOcenaDef]'))
ALTER TABLE [dbo].[ProtokolOcenaDef] DROP CONSTRAINT [FK_ProtokolOcenaDef_Protokol]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolOcenaDef_RezerwacjaSal]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolOcenaDef]'))
ALTER TABLE [dbo].[ProtokolOcenaDef] DROP CONSTRAINT [FK_ProtokolOcenaDef_RezerwacjaSal]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ProtokolOcenaDef]') AND name = N'PK_ProtokolOcenaDef')
ALTER TABLE [dbo].[ProtokolOcenaDef] DROP CONSTRAINT [PK_ProtokolOcenaDef]

--------------------------------------
----------------ProtokolOcenaDef---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[ProtokolOcenaDef]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[ProtokolOcenaDef]
GO

CREATE TABLE [dbo].[ProtokolOcenaDef] (
   [IDProtokolOcenaDef] [int]  IDENTITY(1,1) NOT NULL
  ,[ProtokolID] [int]  NOT NULL
  ,[ProtokolOcenaTypID] [int]  NOT NULL
  ,[RezerwacjaSalID] [int]  NULL
  ,[Termin] [datetime]  NULL
  ,[Opis] [varchar] (255) NULL
  ,[CzyOcenaKoncowa] [bit]  NOT NULL
  ,[Waga] [smallint]  NOT NULL
  ,[DodalTyp] [varchar] (255) NOT NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[ZmodyfikowalTyp] [varchar] (255) NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[ProtokolOcenaDef] ADD  CONSTRAINT [PK_ProtokolOcenaDef] PRIMARY KEY CLUSTERED( IDProtokolOcenaDef ) ON [PRIMARY]

ALTER TABLE [dbo].[ProtokolOcenaDef] WITH NOCHECK ADD CONSTRAINT [FK_ProtokolOcenaDef_RezerwacjaSal] FOREIGN KEY([RezerwacjaSalID]) REFERENCES [dbo].[RezerwacjaSal] ([IDRezerwacjaSal])

ALTER TABLE [dbo].[ProtokolOcenaDef] WITH NOCHECK ADD CONSTRAINT [FK_ProtokolOcenaDef_Protokol] FOREIGN KEY([ProtokolID]) REFERENCES [dbo].[Protokol] ([IDProtokol])

go





---------------------------------------------------------wdro8--------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ProtokolZdarzenie]') AND name = N'PK_ProtokolZdarzenie')
ALTER TABLE [dbo].[ProtokolZdarzenie] DROP CONSTRAINT [PK_ProtokolZdarzenie]

--------------------------------------
----------------ProtokolZdarzenie---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[ProtokolZdarzenie]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[ProtokolZdarzenie]
GO

CREATE TABLE [dbo].[ProtokolZdarzenie] (
   [IDProtokolZdarzenie] [int]  IDENTITY(1,1) NOT NULL
  ,[ZdarzenieID] [int]  NOT NULL
  ,[IndeksID] [int]  NOT NULL
  ,[HistoriaStudentaID] [int]  NOT NULL
  ,[DataOd] [datetime]  NULL
  ,[DataDo] [datetime]  NULL
  ,[CzyUsuniety] [int]  NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO



ALTER TABLE [dbo].[ProtokolZdarzenie] ADD  CONSTRAINT [PK_ProtokolZdarzenie] PRIMARY KEY CLUSTERED( IDProtokolZdarzenie ) ON [PRIMARY]


go



----------------------------------------------------------wdro9---------------------------------------------------


IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolKorekta_ProtokolPowod]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolKorekta]'))
ALTER TABLE [dbo].[ProtokolKorekta] DROP CONSTRAINT [FK_ProtokolKorekta_ProtokolPowod]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolPowod_ProtokolTyp]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolPowod]'))
ALTER TABLE [dbo].[ProtokolPowod] DROP CONSTRAINT [FK_ProtokolPowod_ProtokolTyp]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ProtokolPowod]') AND name = N'PK_ProtokolPowod')
ALTER TABLE [dbo].[ProtokolPowod] DROP CONSTRAINT [PK_ProtokolPowod]

--------------------------------------
----------------ProtokolPowod---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[ProtokolPowod]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[ProtokolPowod]
GO

CREATE TABLE [dbo].[ProtokolPowod] (
   [IDProtokolPowod] [int]  IDENTITY(1,1) NOT NULL
  ,[Nazwa] [varchar] (255) NOT NULL
  ,[ZdarzenieID] [int]  NULL
  ,[ProtokolTypID] [int]  NOT NULL
  ,[CzyWymaganyPowodText] [bit]  NOT NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[ProtokolPowod] ADD  CONSTRAINT [PK_ProtokolPowod] PRIMARY KEY CLUSTERED( IDProtokolPowod ) ON [PRIMARY]

ALTER TABLE [dbo].[ProtokolPowod] WITH NOCHECK ADD CONSTRAINT [FK_ProtokolPowod_ProtokolTyp] FOREIGN KEY([ProtokolTypID]) REFERENCES [dbo].[ProtokolTyp] ([IDProtokolTyp])


go
--------------------------------------------wdro10-----------------------------------------------

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolKorekta_ProtokolPoz]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolKorekta]'))
ALTER TABLE [dbo].[ProtokolKorekta] DROP CONSTRAINT [FK_ProtokolKorekta_ProtokolPoz]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolOcena_ProtokolPoz]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolOcena]'))
ALTER TABLE [dbo].[ProtokolOcena] DROP CONSTRAINT [FK_ProtokolOcena_ProtokolPoz]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolPoz_Indeks]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolPoz]'))
ALTER TABLE [dbo].[ProtokolPoz] DROP CONSTRAINT [FK_ProtokolPoz_Indeks]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolPoz_KartaEgzForma]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolPoz]'))
ALTER TABLE [dbo].[ProtokolPoz] DROP CONSTRAINT [FK_ProtokolPoz_KartaEgzForma]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolPoz_Protokol]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolPoz]'))
ALTER TABLE [dbo].[ProtokolPoz] DROP CONSTRAINT [FK_ProtokolPoz_Protokol]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolPoz_TypUczestnictwa]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolPoz]'))
ALTER TABLE [dbo].[ProtokolPoz] DROP CONSTRAINT [FK_ProtokolPoz_TypUczestnictwa]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ProtokolPoz]') AND name = N'PK_ProtokolPoz')
ALTER TABLE [dbo].[ProtokolPoz] DROP CONSTRAINT [PK_ProtokolPoz]


--------------------------------------
----------------ProtokolPoz---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[ProtokolPoz]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[ProtokolPoz]
GO

CREATE TABLE [dbo].[ProtokolPoz] (
   [IDProtokolPoz] [int]  IDENTITY(1,1) NOT NULL
  ,[ProtokolID] [int]  NOT NULL
  ,[IndeksID] [int]  NOT NULL
  ,[CzyPozycjaSkorygowana] [bit]  NULL
  ,[ZatwierdzilTyp] [varchar] (100) NULL
  ,[Zatwierdzil] [int]  NULL
  ,[Zatwierdzona] [bit]  NOT NULL
  ,[DataZatwierdzenia] [datetime]  NULL
  ,[GrupaZajIndeksID] [int]  NULL
  ,[KursPozIndeksID] [int]  NULL
  ,[KartaEgzFormaID] [int]  NULL
  ,[Uwagi] [varchar] (255) NULL
  ,[CzyUsuniety] [bit]  NULL
  ,[DataUsuniecia] [datetime]  NULL
  ,[FormaWymiarID] [int]  NOT NULL
  ,[TypUczestnictwaID] [int]  NULL
  ,[CzyZatwierdzonyAutomatycznie] [bit]  NULL
  ,[CzyDodanyAutomatycznie] [bit]  NOT NULL
  ,[CzyUsunietyAutomatycznie] [bit]  NULL
  ,[CzyKEFormaAktualna] [bit]  NULL
  ,[DodalTyp] [varchar] (255) NOT NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[ZmodyfikowalTyp] [varchar] (255) NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO




ALTER TABLE [dbo].[ProtokolPoz] ADD  CONSTRAINT [PK_ProtokolPoz] PRIMARY KEY CLUSTERED( IDProtokolPoz ) ON [PRIMARY]

ALTER TABLE [dbo].[ProtokolPoz] WITH NOCHECK ADD CONSTRAINT [FK_ProtokolPoz_TypUczestnictwa] FOREIGN KEY([TypUczestnictwaID]) REFERENCES [dbo].[TypUczestnictwa] ([IDTypUczestnictwa])

ALTER TABLE [dbo].[ProtokolPoz] WITH NOCHECK ADD CONSTRAINT [FK_ProtokolPoz_Protokol] FOREIGN KEY([ProtokolID]) REFERENCES [dbo].[Protokol] ([IDProtokol])

ALTER TABLE [dbo].[ProtokolPoz] WITH NOCHECK ADD CONSTRAINT [FK_ProtokolPoz_KartaEgzForma] FOREIGN KEY([KartaEgzFormaID]) REFERENCES [dbo].[KartaEgzForma] ([IDKartaEgzForma])

ALTER TABLE [dbo].[ProtokolPoz] WITH NOCHECK ADD CONSTRAINT [FK_ProtokolPoz_Indeks] FOREIGN KEY([IndeksID]) REFERENCES [dbo].[Indeks] ([IDIndeks])
go

--------------------------------------------------wdro11--------------------------------------------------

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolKorekta_ProtokolOcena]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolKorekta]'))
ALTER TABLE [dbo].[ProtokolKorekta] DROP CONSTRAINT [FK_ProtokolKorekta_ProtokolOcena]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolOcena_ProtokolOcenaDef]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolOcena]'))
ALTER TABLE [dbo].[ProtokolOcena] DROP CONSTRAINT [FK_ProtokolOcena_ProtokolOcenaDef]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolOcena_ProtokolPoz]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolOcena]'))
ALTER TABLE [dbo].[ProtokolOcena] DROP CONSTRAINT [FK_ProtokolOcena_ProtokolPoz]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolOcena_SkalaOcenPoz]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolOcena]'))
ALTER TABLE [dbo].[ProtokolOcena] DROP CONSTRAINT [FK_ProtokolOcena_SkalaOcenPoz]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ProtokolOcena]') AND name = N'PK_ProtokolOcena')
ALTER TABLE [dbo].[ProtokolOcena] DROP CONSTRAINT [PK_ProtokolOcena]

--------------------------------------
----------------ProtokolOcena---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[ProtokolOcena]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[ProtokolOcena]
GO

CREATE TABLE [dbo].[ProtokolOcena] (
   [IDProtokolOcena] [int]  IDENTITY(1,1) NOT NULL
  ,[ProtokolPozID] [int]  NOT NULL
  ,[ProtokolOcenaDefID] [int]  NOT NULL
  ,[OcenaID] [int]  NULL
  ,[Waga] [smallint]  NOT NULL
  ,[DataZaliczenia] [datetime]  NULL
  ,[DodalTyp] [varchar] (255) NOT NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[ZmodyfikowalTyp] [varchar] (255) NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO





ALTER TABLE [dbo].[ProtokolOcena] ADD  CONSTRAINT [PK_ProtokolOcena] PRIMARY KEY CLUSTERED( IDProtokolOcena ) ON [PRIMARY]

ALTER TABLE [dbo].[ProtokolOcena] WITH NOCHECK ADD CONSTRAINT [FK_ProtokolOcena_SkalaOcenPoz] FOREIGN KEY([OcenaID]) REFERENCES [dbo].[SkalaOcenPoz] ([IDSkalaOcenPoz])

ALTER TABLE [dbo].[ProtokolOcena] WITH NOCHECK ADD CONSTRAINT [FK_ProtokolOcena_ProtokolPoz] FOREIGN KEY([ProtokolPozID]) REFERENCES [dbo].[ProtokolPoz] ([IDProtokolPoz])

ALTER TABLE [dbo].[ProtokolOcena] WITH NOCHECK ADD CONSTRAINT [FK_ProtokolOcena_ProtokolOcenaDef] FOREIGN KEY([ProtokolOcenaDefID]) REFERENCES [dbo].[ProtokolOcenaDef] ([IDProtokolOcenaDef])

go



-----------------------------------------------------------wdro12-------------------------------------------------------

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolPracownik_Pracownik]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolPracownik]'))
ALTER TABLE [dbo].[ProtokolPracownik] DROP CONSTRAINT [FK_ProtokolPracownik_Pracownik]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolPracownik_Protokol]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolPracownik]'))
ALTER TABLE [dbo].[ProtokolPracownik] DROP CONSTRAINT [FK_ProtokolPracownik_Protokol]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ProtokolPracownik]') AND name = N'PK_ProtokolPracownik')
ALTER TABLE [dbo].[ProtokolPracownik] DROP CONSTRAINT [PK_ProtokolPracownik]

--------------------------------------
----------------ProtokolPracownik---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[ProtokolPracownik]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[ProtokolPracownik]
GO

CREATE TABLE [dbo].[ProtokolPracownik] (
   [IDProtokolPracownik] [int]  IDENTITY(1,1) NOT NULL
  ,[PracownikID] [int]  NOT NULL
  ,[ProtokolID] [int]  NOT NULL
  ,[CzyDodanyAutomatycznie] [bit]  NOT NULL
  ,[CzyUsuniety] [bit]  NULL
  ,[DataUsuniecia] [datetime]  NULL
  ,[CzyUsunietyAutomatycznie] [bit]  NULL
  ,[CzyBlokowac] [bit]  NOT NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO





ALTER TABLE [dbo].[ProtokolPracownik] ADD  CONSTRAINT [PK_ProtokolPracownik] PRIMARY KEY CLUSTERED( IDProtokolPracownik ) ON [PRIMARY]

ALTER TABLE [dbo].[ProtokolPracownik] WITH NOCHECK ADD CONSTRAINT [FK_ProtokolPracownik_Protokol] FOREIGN KEY([ProtokolID]) REFERENCES [dbo].[Protokol] ([IDProtokol])

ALTER TABLE [dbo].[ProtokolPracownik] WITH NOCHECK ADD CONSTRAINT [FK_ProtokolPracownik_Pracownik] FOREIGN KEY([PracownikID]) REFERENCES [dbo].[Pracownik] ([IDPracownik])
go



------------------------------------------------------------wdro13---------------------------------------------------------


IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolKorekta_ProtokolOcena]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolKorekta]'))
ALTER TABLE [dbo].[ProtokolKorekta] DROP CONSTRAINT [FK_ProtokolKorekta_ProtokolOcena]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolKorekta_ProtokolPowod]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolKorekta]'))
ALTER TABLE [dbo].[ProtokolKorekta] DROP CONSTRAINT [FK_ProtokolKorekta_ProtokolPowod]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolKorekta_ProtokolPoz]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolKorekta]'))
ALTER TABLE [dbo].[ProtokolKorekta] DROP CONSTRAINT [FK_ProtokolKorekta_ProtokolPoz]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolKorekta_SkalaOcenPoz]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolKorekta]'))
ALTER TABLE [dbo].[ProtokolKorekta] DROP CONSTRAINT [FK_ProtokolKorekta_SkalaOcenPoz]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ProtokolKorekta]') AND name = N'PK_ProtokolKorekta')
ALTER TABLE [dbo].[ProtokolKorekta] DROP CONSTRAINT [PK_ProtokolKorekta]

--------------------------------------
----------------ProtokolKorekta---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[ProtokolKorekta]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[ProtokolKorekta]
GO

CREATE TABLE [dbo].[ProtokolKorekta] (
   [IDProtokolKorekta] [int]  IDENTITY(1,1) NOT NULL
  ,[ProtokolPozID] [int]  NOT NULL
  ,[ProtokolOcenaID] [int]  NOT NULL
  ,[OcenaPrzedID] [int]  NULL
  ,[DataZaliczeniaPrzed] [datetime]  NULL
  ,[OcenaPoID] [int]  NULL
  ,[DataZaliczeniaPo] [datetime]  NULL
  ,[Waga] [int]  NULL
  ,[ProtokolPowodID] [int]  NULL
  ,[Powod] [varchar] (255) NULL
  ,[DodalTyp] [varchar] (100) NOT NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[ZmodyfikowalTyp] [varchar] (100) NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[ProtokolKorekta] ADD  CONSTRAINT [PK_ProtokolKorekta] PRIMARY KEY CLUSTERED( IDProtokolKorekta ) ON [PRIMARY]

ALTER TABLE [dbo].[ProtokolKorekta] WITH NOCHECK ADD CONSTRAINT [FK_ProtokolKorekta_SkalaOcenPoz] FOREIGN KEY([OcenaPrzedID]) REFERENCES [dbo].[SkalaOcenPoz] ([IDSkalaOcenPoz])

ALTER TABLE [dbo].[ProtokolKorekta] WITH NOCHECK ADD CONSTRAINT [FK_ProtokolKorekta_ProtokolPoz] FOREIGN KEY([ProtokolPozID]) REFERENCES [dbo].[ProtokolPoz] ([IDProtokolPoz])

ALTER TABLE [dbo].[ProtokolKorekta] WITH NOCHECK ADD CONSTRAINT [FK_ProtokolKorekta_ProtokolPowod] FOREIGN KEY([ProtokolPowodID]) REFERENCES [dbo].[ProtokolPowod] ([IDProtokolPowod])

ALTER TABLE [dbo].[ProtokolKorekta] WITH NOCHECK ADD CONSTRAINT [FK_ProtokolKorekta_ProtokolOcena] FOREIGN KEY([ProtokolOcenaID]) REFERENCES [dbo].[ProtokolOcena] ([IDProtokolOcena])

go


------------------------------------------------------------wdro14----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolKolizja_ProtokolKolizjaTyp]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolKolizja]'))
ALTER TABLE [dbo].[ProtokolKolizja] DROP CONSTRAINT [FK_ProtokolKolizja_ProtokolKolizjaTyp]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ProtokolKolizjaTyp]') AND name = N'PK_ProtokolKolizjaTyp')
ALTER TABLE [dbo].[ProtokolKolizjaTyp] DROP CONSTRAINT [PK_ProtokolKolizjaTyp]

--------------------------------------
----------------ProtokolKolizjaTyp---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[ProtokolKolizjaTyp]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[ProtokolKolizjaTyp]
GO

CREATE TABLE [dbo].[ProtokolKolizjaTyp] (
   [IDProtokolKolizjaTyp] [int]  IDENTITY(1,1) NOT NULL
  ,[Nazwa] [varchar] (255) NOT NULL
  ,[Podmiot] [varchar] (100) NOT NULL
  ,[PodmiotKolizja] [varchar] (100) NOT NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO




ALTER TABLE [dbo].[ProtokolKolizjaTyp] ADD  CONSTRAINT [PK_ProtokolKolizjaTyp] PRIMARY KEY CLUSTERED( IDProtokolKolizjaTyp ) ON [PRIMARY]
go



--------------------------------------------------------------wdro15----------------------------------------------------


IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolKolizja_ProtokolKolizjaTyp]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolKolizja]'))
ALTER TABLE [dbo].[ProtokolKolizja] DROP CONSTRAINT [FK_ProtokolKolizja_ProtokolKolizjaTyp]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ProtokolKolizja]') AND name = N'PK_ProtokolKolizja')
ALTER TABLE [dbo].[ProtokolKolizja] DROP CONSTRAINT [PK_ProtokolKolizja]

--------------------------------------
----------------ProtokolKolizja---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[ProtokolKolizja]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[ProtokolKolizja]
GO

CREATE TABLE [dbo].[ProtokolKolizja] (
   [IDProtokolKolizja] [int]  IDENTITY(1,1) NOT NULL
  ,[ProtokolKolizjaTypID] [int]  NOT NULL
  ,[Podmiot] [varchar] (100) NOT NULL
  ,[PodmiotID] [int]  NOT NULL
  ,[PodmiotKolizja] [varchar] (100) NULL
  ,[PodmiotKolizjaID] [int]  NULL
  ,[Opis] [text]  NULL
  ,[DataKolizji] [datetime]  NOT NULL
  ,[DataZakonczeniaKolizji] [datetime]  NULL
  ,[CzyAktywna] [bit]  NOT NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO




ALTER TABLE [dbo].[ProtokolKolizja] ADD  CONSTRAINT [PK_ProtokolKolizja] PRIMARY KEY CLUSTERED( IDProtokolKolizja ) ON [PRIMARY]

ALTER TABLE [dbo].[ProtokolKolizja] WITH NOCHECK ADD CONSTRAINT [FK_ProtokolKolizja_ProtokolKolizjaTyp] FOREIGN KEY([ProtokolKolizjaTypID]) REFERENCES [dbo].[ProtokolKolizjaTyp] ([IDProtokolKolizjaTyp])



