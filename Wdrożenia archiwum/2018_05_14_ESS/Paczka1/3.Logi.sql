
----------wdro1------2018_03_16_02_ESS_ProtokolTyp_Log
----------wdro2------2018_03_16_06_ESS_ProtokolOcenaTyp_Log
----------wdro3------2018_03_16_10_ESS_ProtokolTypOcenaTyp_Log
----------wdro4------2018_03_16_14_ESS_Protokol_Log.sql
----------wdro5------2018_03_16_17_ESS_ProtokolOcenaDef_Log.sql
----------wdro6------2018_03_16_19_02_ESS_ProtokolZdarzenie_Log.sql
----------wdro7------2018_03_16_20_ESS_ProtokolPowod_Log.sql
----------wdro8------2018_03_16_23_ESS_ProtokolPoz_Log.sql
----------wdro9------2018_03_16_26_ESS_ProtokolOcena_Log.sql
----------wdro10-----2018_03_16_28_02_ESS_ProtokolPracownik_Log.sql
----------wdro11-----2018_03_16_30_ESS_ProtokolKorekta_Log.sql
----------wdro12-----2018_03_16_33_ESS_ProtokolKolizjaTyp_Log.sql
----------wdro13-----2018_03_16_36_ESS_ProtokolKolizja_Log.sql

--------------------------------------------------------wdro1-----------------------------------------------------


-- ******************* -- 2018_03_16_02_ESS_ProtokolTyp_Log -- ******************* --




----------------ProtokolTyp---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_ProtokolTyp]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_ProtokolTyp] (
   [Log_ProtokolTypID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDProtokolTyp] [int] 
  ,[Org_Nazwa] [varchar] (255)
  ,[Org_RokAkadOd] [int] 
  ,[Org_SemestrIDOd] [int] 
  ,[Org_RokAkadDo] [int] 
  ,[Org_SemestrIDDo] [int] 
  ,[Org_SposobRozliczeniaID] [int] 
  ,[Org_AlgorytmWyliczania] [varchar] (255)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_ProtokolTyp] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_ProtokolTyp] PRIMARY KEY  CLUSTERED
(
   [Log_ProtokolTypID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_ProtokolTyp] (
   [Log_ProtokolTypID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDProtokolTyp] [int] 
  ,[Org_Nazwa] [varchar] (255)
  ,[Org_RokAkadOd] [int] 
  ,[Org_SemestrIDOd] [int] 
  ,[Org_RokAkadDo] [int] 
  ,[Org_SemestrIDDo] [int] 
  ,[Org_SposobRozliczeniaID] [int] 
  ,[Org_AlgorytmWyliczania] [varchar] (255)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_ProtokolTyp ON

IF EXISTS (SELECT * FROM dbo.LOG_ProtokolTyp)
EXEC('INSERT INTO dbo.Tmp_LOG_ProtokolTyp (Log_ProtokolTypID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDProtokolTyp, Org_Nazwa, Org_RokAkadOd, Org_SemestrIDOd, Org_RokAkadDo, Org_SemestrIDDo, Org_SposobRozliczeniaID, Org_AlgorytmWyliczania, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_ProtokolTypID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDProtokolTyp, Org_Nazwa, Org_RokAkadOd, Org_SemestrIDOd, Org_RokAkadDo, Org_SemestrIDDo, Org_SposobRozliczeniaID, Org_AlgorytmWyliczania, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_ProtokolTyp WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_ProtokolTyp OFF

DROP TABLE dbo.LOG_ProtokolTyp

EXECUTE sp_rename N'dbo.Tmp_LOG_ProtokolTyp', N'LOG_ProtokolTyp', 'OBJECT'

ALTER TABLE [dbo].[LOG_ProtokolTyp] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_ProtokolTyp] PRIMARY KEY  CLUSTERED
(
   [Log_ProtokolTypID]
) ON [PRIMARY]

END
GO



--------------------------------------------------------wdro2-----------------------------------------------------


-- ******************* -- 2018_03_16_06_ESS_ProtokolOcenaTyp_Log -- ******************* --



----------------ProtokolOcenaTyp---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_ProtokolOcenaTyp]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_ProtokolOcenaTyp] (
   [Log_ProtokolOcenaTypID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDProtokolOcenaTyp] [int] 
  ,[Org_Nazwa] [varchar] (255)
  ,[Org_CzyOcenaKoncowa] [bit] 
  ,[Org_CzyPrzenoszonaNaKarteEgz] [bit] 
  ,[Org_Waga] [smallint] 
  ,[Org_Lp] [smallint]
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_ProtokolOcenaTyp] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_ProtokolOcenaTyp] PRIMARY KEY  CLUSTERED
(
   [Log_ProtokolOcenaTypID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_ProtokolOcenaTyp] (
   [Log_ProtokolOcenaTypID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDProtokolOcenaTyp] [int] 
  ,[Org_Nazwa] [varchar] (255)
  ,[Org_CzyOcenaKoncowa] [bit] 
  ,[Org_CzyPrzenoszonaNaKarteEgz] [bit] 
  ,[Org_Waga] [smallint] 
  ,[Org_Lp] [smallint]
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_ProtokolOcenaTyp ON

IF EXISTS (SELECT * FROM dbo.LOG_ProtokolOcenaTyp)
EXEC('INSERT INTO dbo.Tmp_LOG_ProtokolOcenaTyp (Log_ProtokolOcenaTypID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDProtokolOcenaTyp, Org_Nazwa, Org_CzyOcenaKoncowa, Org_CzyPrzenoszonaNaKarteEgz, Org_Waga, Org_Lp, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_ProtokolOcenaTypID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDProtokolOcenaTyp, Org_Nazwa, Org_CzyOcenaKoncowa, Org_CzyPrzenoszonaNaKarteEgz, Org_Waga, Org_Lp, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_ProtokolOcenaTyp WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_ProtokolOcenaTyp OFF

DROP TABLE dbo.LOG_ProtokolOcenaTyp

EXECUTE sp_rename N'dbo.Tmp_LOG_ProtokolOcenaTyp', N'LOG_ProtokolOcenaTyp', 'OBJECT'

ALTER TABLE [dbo].[LOG_ProtokolOcenaTyp] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_ProtokolOcenaTyp] PRIMARY KEY  CLUSTERED
(
   [Log_ProtokolOcenaTypID]
) ON [PRIMARY]

END
GO





--------------------------------------------------------wdro3-----------------------------------------------------


-- ******************* -- 2018_03_16_10_ESS_ProtokolTypOcenaTyp_Log -- ******************* --





----------------ProtokolTypOcenaTyp---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_ProtokolTypOcenaTyp]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_ProtokolTypOcenaTyp] (
   [Log_ProtokolTypOcenaTypID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDProtokolTypOcenaTyp] [int] 
  ,[Org_ProtokolOcenaTypID] [int] 
  ,[Org_ProtokolTypID] [int] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_ProtokolTypOcenaTyp] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_ProtokolTypOcenaTyp] PRIMARY KEY  CLUSTERED
(
   [Log_ProtokolTypOcenaTypID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_ProtokolTypOcenaTyp] (
   [Log_ProtokolTypOcenaTypID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDProtokolTypOcenaTyp] [int] 
  ,[Org_ProtokolOcenaTypID] [int] 
  ,[Org_ProtokolTypID] [int] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_ProtokolTypOcenaTyp ON

IF EXISTS (SELECT * FROM dbo.LOG_ProtokolTypOcenaTyp)
EXEC('INSERT INTO dbo.Tmp_LOG_ProtokolTypOcenaTyp (Log_ProtokolTypOcenaTypID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDProtokolTypOcenaTyp, Org_ProtokolOcenaTypID, Org_ProtokolTypID, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_ProtokolTypOcenaTypID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDProtokolTypOcenaTyp, Org_ProtokolOcenaTypID, Org_ProtokolTypID, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_ProtokolTypOcenaTyp WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_ProtokolTypOcenaTyp OFF

DROP TABLE dbo.LOG_ProtokolTypOcenaTyp

EXECUTE sp_rename N'dbo.Tmp_LOG_ProtokolTypOcenaTyp', N'LOG_ProtokolTypOcenaTyp', 'OBJECT'

ALTER TABLE [dbo].[LOG_ProtokolTypOcenaTyp] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_ProtokolTypOcenaTyp] PRIMARY KEY  CLUSTERED
(
   [Log_ProtokolTypOcenaTypID]
) ON [PRIMARY]

END
GO


-----------------------------------------------------wdro4---------------------------------------------------


-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------Protokol---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_Protokol]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_Protokol] (
   [Log_ProtokolID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDProtokol] [int] 
  ,[Org_ProtokolTypID] [int] 
  ,[Org_GrupaZajeciowaID] [int] 
  ,[Org_KursGrupaZajID] [int] 
  ,[Org_Nazwa] [varchar] (255)
  ,[Org_RokAkad] [int] 
  ,[Org_SemestrID] [int] 
  ,[Org_FormaWymiarID] [int] 
  ,[Org_SkalaOcenID] [int] 
  ,[Org_Opis] [varchar] (500)
  ,[Org_DataZablokowania] [datetime] 
  ,[Org_Zablokowany] [bit] 
  ,[Org_CzyZablokowanyAutomatycznie] [bit] 
  ,[Org_DataUdostepnienia] [datetime] 
  ,[Org_TrybStudiowID] [int] 
  ,[Org_SystemStudiowID] [int] 
  ,[Org_SekcjaID] [int] 
  ,[Org_KierunekID] [int] 
  ,[Org_CzyZajeciaWspolne] [bit] 
  ,[Org_CzyDodanyAutomatycznie] [bit] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_Protokol] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_Protokol] PRIMARY KEY  CLUSTERED
(
   [Log_ProtokolID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_Protokol] (
   [Log_ProtokolID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDProtokol] [int] 
  ,[Org_ProtokolTypID] [int] 
  ,[Org_GrupaZajeciowaID] [int] 
  ,[Org_KursGrupaZajID] [int] 
  ,[Org_Nazwa] [varchar] (255)
  ,[Org_RokAkad] [int] 
  ,[Org_SemestrID] [int] 
  ,[Org_FormaWymiarID] [int] 
  ,[Org_SkalaOcenID] [int] 
  ,[Org_Opis] [varchar] (500)
  ,[Org_DataZablokowania] [datetime] 
  ,[Org_Zablokowany] [bit] 
  ,[Org_CzyZablokowanyAutomatycznie] [bit] 
  ,[Org_DataUdostepnienia] [datetime] 
  ,[Org_TrybStudiowID] [int] 
  ,[Org_SystemStudiowID] [int] 
  ,[Org_SekcjaID] [int] 
  ,[Org_KierunekID] [int] 
  ,[Org_CzyZajeciaWspolne] [bit] 
  ,[Org_CzyDodanyAutomatycznie] [bit] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_Protokol ON

IF EXISTS (SELECT * FROM dbo.LOG_Protokol)
EXEC('INSERT INTO dbo.Tmp_LOG_Protokol (Log_ProtokolID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDProtokol, Org_ProtokolTypID, Org_GrupaZajeciowaID, Org_KursGrupaZajID, Org_Nazwa, Org_RokAkad, Org_SemestrID, Org_FormaWymiarID, Org_SkalaOcenID, Org_Opis, Org_DataZablokowania, Org_Zablokowany, Org_CzyZablokowanyAutomatycznie, Org_DataUdostepnienia, Org_TrybStudiowID, Org_SystemStudiowID, Org_SekcjaID, Org_KierunekID, Org_CzyZajeciaWspolne, Org_CzyDodanyAutomatycznie, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_ProtokolID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDProtokol, Org_ProtokolTypID, Org_GrupaZajeciowaID, Org_KursGrupaZajID, Org_Nazwa, Org_RokAkad, Org_SemestrID, Org_FormaWymiarID, Org_SkalaOcenID, Org_Opis, Org_DataZablokowania, Org_Zablokowany, Org_CzyZablokowanyAutomatycznie, Org_DataUdostepnienia, Org_TrybStudiowID, Org_SystemStudiowID, Org_SekcjaID, Org_KierunekID, Org_CzyZajeciaWspolne, Org_CzyDodanyAutomatycznie, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_Protokol WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_Protokol OFF

DROP TABLE dbo.LOG_Protokol

EXECUTE sp_rename N'dbo.Tmp_LOG_Protokol', N'LOG_Protokol', 'OBJECT'

ALTER TABLE [dbo].[LOG_Protokol] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_Protokol] PRIMARY KEY  CLUSTERED
(
   [Log_ProtokolID]
) ON [PRIMARY]

END
GO




------------------------------------------------------wdro5--------------------------------------------------------


-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------ProtokolOcenaDef---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_ProtokolOcenaDef]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_ProtokolOcenaDef] (
   [Log_ProtokolOcenaDefID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDProtokolOcenaDef] [int] 
  ,[Org_ProtokolID] [int] 
  ,[Org_ProtokolOcenaTypID] [int] 
  ,[Org_RezerwacjaSalID] [int] 
  ,[Org_Termin] [datetime] 
  ,[Org_Opis] [varchar] (255)
  ,[Org_CzyOcenaKoncowa] [bit] 
  ,[Org_Waga] [smallint] 
  ,[Org_DodalTyp] [varchar] (255)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_ZmodyfikowalTyp] [varchar] (255)
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_ProtokolOcenaDef] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_ProtokolOcenaDef] PRIMARY KEY  CLUSTERED
(
   [Log_ProtokolOcenaDefID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_ProtokolOcenaDef] (
   [Log_ProtokolOcenaDefID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDProtokolOcenaDef] [int] 
  ,[Org_ProtokolID] [int] 
  ,[Org_ProtokolOcenaTypID] [int] 
  ,[Org_RezerwacjaSalID] [int] 
  ,[Org_Termin] [datetime] 
  ,[Org_Opis] [varchar] (255)
  ,[Org_CzyOcenaKoncowa] [bit] 
  ,[Org_Waga] [smallint] 
  ,[Org_DodalTyp] [varchar] (255)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_ZmodyfikowalTyp] [varchar] (255)
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_ProtokolOcenaDef ON

IF EXISTS (SELECT * FROM dbo.LOG_ProtokolOcenaDef)
EXEC('INSERT INTO dbo.Tmp_LOG_ProtokolOcenaDef (Log_ProtokolOcenaDefID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDProtokolOcenaDef, Org_ProtokolID, Org_ProtokolOcenaTypID, Org_RezerwacjaSalID, Org_Termin, Org_Opis, Org_CzyOcenaKoncowa, Org_Waga, Org_DodalTyp, Org_Dodal, Org_DataDodania, Org_ZmodyfikowalTyp, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_ProtokolOcenaDefID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDProtokolOcenaDef, Org_ProtokolID, Org_ProtokolOcenaTypID, Org_RezerwacjaSalID, Org_Termin, Org_Opis, Org_CzyOcenaKoncowa, Org_Waga, Org_DodalTyp, Org_Dodal, Org_DataDodania, Org_ZmodyfikowalTyp, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_ProtokolOcenaDef WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_ProtokolOcenaDef OFF

DROP TABLE dbo.LOG_ProtokolOcenaDef

EXECUTE sp_rename N'dbo.Tmp_LOG_ProtokolOcenaDef', N'LOG_ProtokolOcenaDef', 'OBJECT'

ALTER TABLE [dbo].[LOG_ProtokolOcenaDef] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_ProtokolOcenaDef] PRIMARY KEY  CLUSTERED
(
   [Log_ProtokolOcenaDefID]
) ON [PRIMARY]

END
GO


--------------------------------------------------------wdro6--------------------------------------------------------



-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------ProtokolZdarzenie---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_ProtokolZdarzenie]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_ProtokolZdarzenie] (
   [Log_ProtokolZdarzenieID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDProtokolZdarzenie] [int] 
  ,[Org_ZdarzenieID] [int] 
  ,[Org_IndeksID] [int] 
  ,[Org_HistoriaStudentaID] [int] 
  ,[Org_DataOd] [datetime] 
  ,[Org_DataDo] [datetime] 
  ,[Org_CzyUsuniety] [int] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_ProtokolZdarzenie] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_ProtokolZdarzenie] PRIMARY KEY  CLUSTERED
(
   [Log_ProtokolZdarzenieID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_ProtokolZdarzenie] (
   [Log_ProtokolZdarzenieID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDProtokolZdarzenie] [int] 
  ,[Org_ZdarzenieID] [int] 
  ,[Org_IndeksID] [int] 
  ,[Org_HistoriaStudentaID] [int] 
  ,[Org_DataOd] [datetime] 
  ,[Org_DataDo] [datetime] 
  ,[Org_CzyUsuniety] [int] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_ProtokolZdarzenie ON

IF EXISTS (SELECT * FROM dbo.LOG_ProtokolZdarzenie)
EXEC('INSERT INTO dbo.Tmp_LOG_ProtokolZdarzenie (Log_ProtokolZdarzenieID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDProtokolZdarzenie, Org_ZdarzenieID, Org_IndeksID, Org_HistoriaStudentaID, Org_DataOd, Org_DataDo, Org_CzyUsuniety, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_ProtokolZdarzenieID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDProtokolZdarzenie, Org_ZdarzenieID, Org_IndeksID, Org_HistoriaStudentaID, Org_DataOd, Org_DataDo, Org_CzyUsuniety, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_ProtokolZdarzenie WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_ProtokolZdarzenie OFF

DROP TABLE dbo.LOG_ProtokolZdarzenie

EXECUTE sp_rename N'dbo.Tmp_LOG_ProtokolZdarzenie', N'LOG_ProtokolZdarzenie', 'OBJECT'

ALTER TABLE [dbo].[LOG_ProtokolZdarzenie] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_ProtokolZdarzenie] PRIMARY KEY  CLUSTERED
(
   [Log_ProtokolZdarzenieID]
) ON [PRIMARY]

END
GO

go


-----------------------------------------------------------wdro7----------------------------------------------------



-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------ProtokolPowod---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_ProtokolPowod]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_ProtokolPowod] (
   [Log_ProtokolPowodID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDProtokolPowod] [int] 
  ,[Org_Nazwa] [varchar] (255)
  ,[Org_ZdarzenieID] [int] 
  ,[Org_ProtokolTypID] [int] 
  ,[Org_CzyWymaganyPowodText] [bit] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_ProtokolPowod] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_ProtokolPowod] PRIMARY KEY  CLUSTERED
(
   [Log_ProtokolPowodID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_ProtokolPowod] (
   [Log_ProtokolPowodID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDProtokolPowod] [int] 
  ,[Org_Nazwa] [varchar] (255)
  ,[Org_ZdarzenieID] [int] 
  ,[Org_ProtokolTypID] [int] 
  ,[Org_CzyWymaganyPowodText] [bit] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_ProtokolPowod ON

IF EXISTS (SELECT * FROM dbo.LOG_ProtokolPowod)
EXEC('INSERT INTO dbo.Tmp_LOG_ProtokolPowod (Log_ProtokolPowodID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDProtokolPowod, Org_Nazwa, Org_ZdarzenieID, Org_ProtokolTypID, Org_CzyWymaganyPowodText, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_ProtokolPowodID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDProtokolPowod, Org_Nazwa, Org_ZdarzenieID, Org_ProtokolTypID, Org_CzyWymaganyPowodText, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_ProtokolPowod WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_ProtokolPowod OFF

DROP TABLE dbo.LOG_ProtokolPowod

EXECUTE sp_rename N'dbo.Tmp_LOG_ProtokolPowod', N'LOG_ProtokolPowod', 'OBJECT'

ALTER TABLE [dbo].[LOG_ProtokolPowod] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_ProtokolPowod] PRIMARY KEY  CLUSTERED
(
   [Log_ProtokolPowodID]
) ON [PRIMARY]

END
GO


--------------------------------------------------wdro8---------------------------------------------------


-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------ProtokolPoz---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_ProtokolPoz]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_ProtokolPoz] (
   [Log_ProtokolPozID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDProtokolPoz] [int] 
  ,[Org_ProtokolID] [int] 
  ,[Org_IndeksID] [int] 
  ,[Org_CzyPozycjaSkorygowana] [bit] 
  ,[Org_ZatwierdzilTyp] [varchar] (100)
  ,[Org_Zatwierdzil] [int] 
  ,[Org_Zatwierdzona] [bit] 
  ,[Org_DataZatwierdzenia] [datetime] 
  ,[Org_GrupaZajIndeksID] [int] 
  ,[Org_KursPozIndeksID] [int] 
  ,[Org_KartaEgzFormaID] [int] 
  ,[Org_Uwagi] [varchar] (255)
  ,[Org_CzyUsuniety] [bit] 
  ,[Org_DataUsuniecia] [datetime] 
  ,[Org_FormaWymiarID] [int] 
  ,[Org_TypUczestnictwaID] [int] 
  ,[Org_CzyZatwierdzonyAutomatycznie] [bit] 
  ,[Org_CzyDodanyAutomatycznie] [bit] 
  ,[Org_CzyUsunietyAutomatycznie] [bit] 
  ,[Org_CzyKEFormaAktualna] [bit] 
  ,[Org_DodalTyp] [varchar] (255)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_ZmodyfikowalTyp] [varchar] (255)
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_ProtokolPoz] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_ProtokolPoz] PRIMARY KEY  CLUSTERED
(
   [Log_ProtokolPozID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_ProtokolPoz] (
   [Log_ProtokolPozID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDProtokolPoz] [int] 
  ,[Org_ProtokolID] [int] 
  ,[Org_IndeksID] [int] 
  ,[Org_CzyPozycjaSkorygowana] [bit] 
  ,[Org_ZatwierdzilTyp] [varchar] (100)
  ,[Org_Zatwierdzil] [int] 
  ,[Org_Zatwierdzona] [bit] 
  ,[Org_DataZatwierdzenia] [datetime] 
  ,[Org_GrupaZajIndeksID] [int] 
  ,[Org_KursPozIndeksID] [int] 
  ,[Org_KartaEgzFormaID] [int] 
  ,[Org_Uwagi] [varchar] (255)
  ,[Org_CzyUsuniety] [bit] 
  ,[Org_DataUsuniecia] [datetime] 
  ,[Org_FormaWymiarID] [int] 
  ,[Org_TypUczestnictwaID] [int] 
  ,[Org_CzyZatwierdzonyAutomatycznie] [bit] 
  ,[Org_CzyDodanyAutomatycznie] [bit] 
  ,[Org_CzyUsunietyAutomatycznie] [bit] 
  ,[Org_CzyKEFormaAktualna] [bit] 
  ,[Org_DodalTyp] [varchar] (255)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_ZmodyfikowalTyp] [varchar] (255)
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_ProtokolPoz ON

IF EXISTS (SELECT * FROM dbo.LOG_ProtokolPoz)
EXEC('INSERT INTO dbo.Tmp_LOG_ProtokolPoz (Log_ProtokolPozID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDProtokolPoz, Org_ProtokolID, Org_IndeksID, Org_CzyPozycjaSkorygowana, Org_ZatwierdzilTyp, Org_Zatwierdzil, Org_Zatwierdzona, Org_DataZatwierdzenia, Org_GrupaZajIndeksID, Org_KursPozIndeksID, Org_KartaEgzFormaID, Org_Uwagi, Org_CzyUsuniety, Org_DataUsuniecia, Org_FormaWymiarID, Org_TypUczestnictwaID, Org_CzyZatwierdzonyAutomatycznie, Org_CzyDodanyAutomatycznie, Org_CzyUsunietyAutomatycznie, Org_CzyKEFormaAktualna, Org_DodalTyp, Org_Dodal, Org_DataDodania, Org_ZmodyfikowalTyp, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_ProtokolPozID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDProtokolPoz, Org_ProtokolID, Org_IndeksID, Org_CzyPozycjaSkorygowana, Org_ZatwierdzilTyp, Org_Zatwierdzil, Org_Zatwierdzona, Org_DataZatwierdzenia, Org_GrupaZajIndeksID, Org_KursPozIndeksID, Org_KartaEgzFormaID, Org_Uwagi, Org_CzyUsuniety, Org_DataUsuniecia, Org_FormaWymiarID, Org_TypUczestnictwaID, Org_CzyZatwierdzonyAutomatycznie, Org_CzyDodanyAutomatycznie, Org_CzyUsunietyAutomatycznie, Org_CzyKEFormaAktualna, Org_DodalTyp, Org_Dodal, Org_DataDodania, Org_ZmodyfikowalTyp, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_ProtokolPoz WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_ProtokolPoz OFF

DROP TABLE dbo.LOG_ProtokolPoz

EXECUTE sp_rename N'dbo.Tmp_LOG_ProtokolPoz', N'LOG_ProtokolPoz', 'OBJECT'

ALTER TABLE [dbo].[LOG_ProtokolPoz] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_ProtokolPoz] PRIMARY KEY  CLUSTERED
(
   [Log_ProtokolPozID]
) ON [PRIMARY]

END
GO


-----------------------------------------------------------wdro9---------------------------------------------------


-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------ProtokolOcena---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_ProtokolOcena]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_ProtokolOcena] (
   [Log_ProtokolOcenaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDProtokolOcena] [int] 
  ,[Org_ProtokolPozID] [int] 
  ,[Org_ProtokolOcenaDefID] [int] 
  ,[Org_OcenaID] [int] 
  ,[Org_Waga] [smallint] 
  ,[Org_DataZaliczenia] [datetime] 
  ,[Org_DodalTyp] [varchar] (255)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_ZmodyfikowalTyp] [varchar] (255)
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_ProtokolOcena] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_ProtokolOcena] PRIMARY KEY  CLUSTERED
(
   [Log_ProtokolOcenaID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_ProtokolOcena] (
   [Log_ProtokolOcenaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDProtokolOcena] [int] 
  ,[Org_ProtokolPozID] [int] 
  ,[Org_ProtokolOcenaDefID] [int] 
  ,[Org_OcenaID] [int] 
  ,[Org_Waga] [smallint] 
  ,[Org_DataZaliczenia] [datetime] 
  ,[Org_DodalTyp] [varchar] (255)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_ZmodyfikowalTyp] [varchar] (255)
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_ProtokolOcena ON

IF EXISTS (SELECT * FROM dbo.LOG_ProtokolOcena)
EXEC('INSERT INTO dbo.Tmp_LOG_ProtokolOcena (Log_ProtokolOcenaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDProtokolOcena, Org_ProtokolPozID, Org_ProtokolOcenaDefID, Org_OcenaID, Org_Waga, Org_DataZaliczenia, Org_DodalTyp, Org_Dodal, Org_DataDodania, Org_ZmodyfikowalTyp, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_ProtokolOcenaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDProtokolOcena, Org_ProtokolPozID, Org_ProtokolOcenaDefID, Org_OcenaID, Org_Waga, Org_DataZaliczenia, Org_DodalTyp, Org_Dodal, Org_DataDodania, Org_ZmodyfikowalTyp, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_ProtokolOcena WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_ProtokolOcena OFF

DROP TABLE dbo.LOG_ProtokolOcena

EXECUTE sp_rename N'dbo.Tmp_LOG_ProtokolOcena', N'LOG_ProtokolOcena', 'OBJECT'

ALTER TABLE [dbo].[LOG_ProtokolOcena] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_ProtokolOcena] PRIMARY KEY  CLUSTERED
(
   [Log_ProtokolOcenaID]
) ON [PRIMARY]

END
GO




----------------------------------------------wdro10-------------------------------------------------



-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------ProtokolPracownik---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_ProtokolPracownik]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_ProtokolPracownik] (
   [Log_ProtokolPracownikID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDProtokolPracownik] [int] 
  ,[Org_PracownikID] [int] 
  ,[Org_ProtokolID] [int] 
  ,[Org_CzyDodanyAutomatycznie] [bit] 
  ,[Org_CzyUsuniety] [bit] 
  ,[Org_DataUsuniecia] [datetime] 
  ,[Org_CzyUsunietyAutomatycznie] [bit] 
  ,[Org_CzyBlokowac] [bit] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_ProtokolPracownik] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_ProtokolPracownik] PRIMARY KEY  CLUSTERED
(
   [Log_ProtokolPracownikID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_ProtokolPracownik] (
   [Log_ProtokolPracownikID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDProtokolPracownik] [int] 
  ,[Org_PracownikID] [int] 
  ,[Org_ProtokolID] [int] 
  ,[Org_CzyDodanyAutomatycznie] [bit] 
  ,[Org_CzyUsuniety] [bit] 
  ,[Org_DataUsuniecia] [datetime] 
  ,[Org_CzyUsunietyAutomatycznie] [bit] 
  ,[Org_CzyBlokowac] [bit] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_ProtokolPracownik ON

IF EXISTS (SELECT * FROM dbo.LOG_ProtokolPracownik)
EXEC('INSERT INTO dbo.Tmp_LOG_ProtokolPracownik (Log_ProtokolPracownikID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDProtokolPracownik, Org_PracownikID, Org_ProtokolID, Org_CzyDodanyAutomatycznie, Org_CzyUsuniety, Org_DataUsuniecia, Org_CzyUsunietyAutomatycznie, Org_CzyBlokowac, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_ProtokolPracownikID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDProtokolPracownik, Org_PracownikID, Org_ProtokolID, Org_CzyDodanyAutomatycznie, Org_CzyUsuniety, Org_DataUsuniecia, Org_CzyUsunietyAutomatycznie, Org_CzyBlokowac, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_ProtokolPracownik WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_ProtokolPracownik OFF

DROP TABLE dbo.LOG_ProtokolPracownik

EXECUTE sp_rename N'dbo.Tmp_LOG_ProtokolPracownik', N'LOG_ProtokolPracownik', 'OBJECT'

ALTER TABLE [dbo].[LOG_ProtokolPracownik] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_ProtokolPracownik] PRIMARY KEY  CLUSTERED
(
   [Log_ProtokolPracownikID]
) ON [PRIMARY]

END
GO


------------------------------------------------wdro11-------------------------------------------------



-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------ProtokolKorekta---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_ProtokolKorekta]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_ProtokolKorekta] (
   [Log_ProtokolKorektaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDProtokolKorekta] [int] 
  ,[Org_ProtokolPozID] [int] 
  ,[Org_ProtokolOcenaID] [int] 
  ,[Org_OcenaPrzedID] [int] 
  ,[Org_DataZaliczeniaPrzed] [datetime] 
  ,[Org_OcenaPoID] [int] 
  ,[Org_DataZaliczeniaPo] [datetime] 
  ,[Org_Waga] [int] 
  ,[Org_ProtokolPowodID] [int] 
  ,[Org_Powod] [varchar] (255)
  ,[Org_DodalTyp] [varchar] (100)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_ZmodyfikowalTyp] [varchar] (100)
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_ProtokolKorekta] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_ProtokolKorekta] PRIMARY KEY  CLUSTERED
(
   [Log_ProtokolKorektaID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_ProtokolKorekta] (
   [Log_ProtokolKorektaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDProtokolKorekta] [int] 
  ,[Org_ProtokolPozID] [int] 
  ,[Org_ProtokolOcenaID] [int] 
  ,[Org_OcenaPrzedID] [int] 
  ,[Org_DataZaliczeniaPrzed] [datetime] 
  ,[Org_OcenaPoID] [int] 
  ,[Org_DataZaliczeniaPo] [datetime] 
  ,[Org_Waga] [int] 
  ,[Org_ProtokolPowodID] [int] 
  ,[Org_Powod] [varchar] (255)
  ,[Org_DodalTyp] [varchar] (100)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_ZmodyfikowalTyp] [varchar] (100)
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_ProtokolKorekta ON

IF EXISTS (SELECT * FROM dbo.LOG_ProtokolKorekta)
EXEC('INSERT INTO dbo.Tmp_LOG_ProtokolKorekta (Log_ProtokolKorektaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDProtokolKorekta, Org_ProtokolPozID, Org_ProtokolOcenaID, Org_OcenaPrzedID, Org_DataZaliczeniaPrzed, Org_OcenaPoID, Org_DataZaliczeniaPo, Org_Waga, Org_ProtokolPowodID, Org_Powod, Org_DodalTyp, Org_Dodal, Org_DataDodania, Org_ZmodyfikowalTyp, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_ProtokolKorektaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDProtokolKorekta, Org_ProtokolPozID, Org_ProtokolOcenaID, Org_OcenaPrzedID, Org_DataZaliczeniaPrzed, Org_OcenaPoID, Org_DataZaliczeniaPo, Org_Waga, Org_ProtokolPowodID, Org_Powod, Org_DodalTyp, Org_Dodal, Org_DataDodania, Org_ZmodyfikowalTyp, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_ProtokolKorekta WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_ProtokolKorekta OFF

DROP TABLE dbo.LOG_ProtokolKorekta

EXECUTE sp_rename N'dbo.Tmp_LOG_ProtokolKorekta', N'LOG_ProtokolKorekta', 'OBJECT'

ALTER TABLE [dbo].[LOG_ProtokolKorekta] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_ProtokolKorekta] PRIMARY KEY  CLUSTERED
(
   [Log_ProtokolKorektaID]
) ON [PRIMARY]

END
GO


------------------------------------------------------wdro12-------------------------------------------------------



-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------ProtokolKolizjaTyp---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_ProtokolKolizjaTyp]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_ProtokolKolizjaTyp] (
   [Log_ProtokolKolizjaTypID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDProtokolKolizjaTyp] [int] 
  ,[Org_Nazwa] [varchar] (255)
  ,[Org_Podmiot] [varchar] (100)
  ,[Org_PodmiotKolizja] [varchar] (100)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_ProtokolKolizjaTyp] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_ProtokolKolizjaTyp] PRIMARY KEY  CLUSTERED
(
   [Log_ProtokolKolizjaTypID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_ProtokolKolizjaTyp] (
   [Log_ProtokolKolizjaTypID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDProtokolKolizjaTyp] [int] 
  ,[Org_Nazwa] [varchar] (255)
  ,[Org_Podmiot] [varchar] (100)
  ,[Org_PodmiotKolizja] [varchar] (100)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_ProtokolKolizjaTyp ON

IF EXISTS (SELECT * FROM dbo.LOG_ProtokolKolizjaTyp)
EXEC('INSERT INTO dbo.Tmp_LOG_ProtokolKolizjaTyp (Log_ProtokolKolizjaTypID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDProtokolKolizjaTyp, Org_Nazwa, Org_Podmiot, Org_PodmiotKolizja, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_ProtokolKolizjaTypID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDProtokolKolizjaTyp, Org_Nazwa, Org_Podmiot, Org_PodmiotKolizja, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_ProtokolKolizjaTyp WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_ProtokolKolizjaTyp OFF

DROP TABLE dbo.LOG_ProtokolKolizjaTyp

EXECUTE sp_rename N'dbo.Tmp_LOG_ProtokolKolizjaTyp', N'LOG_ProtokolKolizjaTyp', 'OBJECT'

ALTER TABLE [dbo].[LOG_ProtokolKolizjaTyp] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_ProtokolKolizjaTyp] PRIMARY KEY  CLUSTERED
(
   [Log_ProtokolKolizjaTypID]
) ON [PRIMARY]

END
GO


-----------------------------------------------------wdro13-------------------------------------------------


-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------ProtokolKolizja---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_ProtokolKolizja]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_ProtokolKolizja] (
   [Log_ProtokolKolizjaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDProtokolKolizja] [int] 
  ,[Org_ProtokolKolizjaTypID] [int] 
  ,[Org_Podmiot] [varchar] (100)
  ,[Org_PodmiotID] [int] 
  ,[Org_PodmiotKolizja] [varchar] (100)
  ,[Org_PodmiotKolizjaID] [int] 
  ,[Org_DataKolizji] [datetime] 
  ,[Org_DataZakonczeniaKolizji] [datetime] 
  ,[Org_CzyAktywna] [bit] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_ProtokolKolizja] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_ProtokolKolizja] PRIMARY KEY  CLUSTERED
(
   [Log_ProtokolKolizjaID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_ProtokolKolizja] (
   [Log_ProtokolKolizjaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDProtokolKolizja] [int] 
  ,[Org_ProtokolKolizjaTypID] [int] 
  ,[Org_Podmiot] [varchar] (100)
  ,[Org_PodmiotID] [int] 
  ,[Org_PodmiotKolizja] [varchar] (100)
  ,[Org_PodmiotKolizjaID] [int] 
  ,[Org_DataKolizji] [datetime] 
  ,[Org_DataZakonczeniaKolizji] [datetime] 
  ,[Org_CzyAktywna] [bit] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_ProtokolKolizja ON

IF EXISTS (SELECT * FROM dbo.LOG_ProtokolKolizja)
EXEC('INSERT INTO dbo.Tmp_LOG_ProtokolKolizja (Log_ProtokolKolizjaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDProtokolKolizja, Org_ProtokolKolizjaTypID, Org_Podmiot, Org_PodmiotID, Org_PodmiotKolizja, Org_PodmiotKolizjaID, Org_DataKolizji, Org_DataZakonczeniaKolizji, Org_CzyAktywna, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_ProtokolKolizjaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDProtokolKolizja, Org_ProtokolKolizjaTypID, Org_Podmiot, Org_PodmiotID, Org_PodmiotKolizja, Org_PodmiotKolizjaID, Org_DataKolizji, Org_DataZakonczeniaKolizji, Org_CzyAktywna, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_ProtokolKolizja WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_ProtokolKolizja OFF

DROP TABLE dbo.LOG_ProtokolKolizja

EXECUTE sp_rename N'dbo.Tmp_LOG_ProtokolKolizja', N'LOG_ProtokolKolizja', 'OBJECT'

ALTER TABLE [dbo].[LOG_ProtokolKolizja] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_ProtokolKolizja] PRIMARY KEY  CLUSTERED
(
   [Log_ProtokolKolizjaID]
) ON [PRIMARY]

END
GO

