----------wdro1------2018_03_16_03_ESS_ProtokolTyp_Trigger
----------wdro2------2018_03_16_07_ESS_ProtokolOcenaTyp_Trigger
----------wdro3------2018_03_16_11_ESS_ProtokolTypOcenaTyp_Trigger
----------wdro4------2018_03_16_15_ESS_Protokol_Trigger.sql
----------wdro5------2018_03_16_18_ESS_ProtokolOcenaDef_Trigger.sql
----------wdro6------2018_03_16_19_03_ESS_ProtokolZdarzenie_Trigger.sql
----------wdro7------2018_03_16_21_ESS_ProtokolPowod_Trigger.sql
----------wdro8------2018_03_16_24_ESS_ProtokolPoz_Trigger.sql
----------wdro9------2018_03_16_27_ESS_ProtokolOcena_Trigger.sql
----------wdro10-----2018_03_16_28_03_ESS_ProtokolPracownik_Trigger.sql
----------wdro11-----2018_03_16_31_ESS_ProtokolKorekta_Trigger.sql
----------wdro12-----2018_03_16_34_ESS_ProtokolKolizjaTyp_Trigger.sql
----------wdro13-----2018_03_16_37_ESS_ProtokolKolizja_Trigger.sql








--------------------------------------------------------wdro1-----------------------------------------------------


-- ******************* -- 2018_03_16_03_ESS_ProtokolTyp_Trigger -- ******************* --


DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------ProtokolTyp-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolTyp_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolTyp_INSERT
')

EXEC('
CREATE TRIGGER ProtokolTyp_INSERT ON ['+@BazaDanych+'].[dbo].[ProtokolTyp] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolTyp'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolTyp]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolTyp]
  ,[Org_Nazwa]
  ,[Org_RokAkadOd]
  ,[Org_SemestrIDOd]
  ,[Org_RokAkadDo]
  ,[Org_SemestrIDDo]
  ,[Org_SposobRozliczeniaID]
  ,[Org_AlgorytmWyliczania]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDProtokolTyp
 ,ins.Nazwa
 ,ins.RokAkadOd
 ,ins.SemestrIDOd
 ,ins.RokAkadDo
 ,ins.SemestrIDDo
 ,ins.SposobRozliczeniaID
 ,ins.AlgorytmWyliczania
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolTyp_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolTyp_UPDATE
')

EXEC('
CREATE TRIGGER ProtokolTyp_UPDATE ON ['+@BazaDanych+'].[dbo].[ProtokolTyp] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolTyp'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolTyp]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolTyp]
  ,[Org_Nazwa]
  ,[Org_RokAkadOd]
  ,[Org_SemestrIDOd]
  ,[Org_RokAkadDo]
  ,[Org_SemestrIDDo]
  ,[Org_SposobRozliczeniaID]
  ,[Org_AlgorytmWyliczania]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDProtokolTyp
 ,ins.Nazwa
 ,ins.RokAkadOd
 ,ins.SemestrIDOd
 ,ins.RokAkadDo
 ,ins.SemestrIDDo
 ,ins.SposobRozliczeniaID
 ,ins.AlgorytmWyliczania
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolTyp_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolTyp_DELETE
')

EXEC('
CREATE TRIGGER ProtokolTyp_DELETE ON ['+@BazaDanych+'].[dbo].[ProtokolTyp] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolTyp'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolTyp]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolTyp]
  ,[Org_Nazwa]
  ,[Org_RokAkadOd]
  ,[Org_SemestrIDOd]
  ,[Org_RokAkadDo]
  ,[Org_SemestrIDDo]
  ,[Org_SposobRozliczeniaID]
  ,[Org_AlgorytmWyliczania]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDProtokolTyp
 ,del.Nazwa
 ,del.RokAkadOd
 ,del.SemestrIDOd
 ,del.RokAkadDo
 ,del.SemestrIDDo
 ,del.SposobRozliczeniaID
 ,del.AlgorytmWyliczania
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'ProtokolTyp')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('ProtokolTyp', 1, 0,GetDate())


GO


--------------------------------------------------------wdro2-----------------------------------------------------


-- ******************* -- 2018_03_16_07_ESS_ProtokolOcenaTyp_Trigger -- ******************* --



DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------ProtokolOcenaTyp-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolOcenaTyp_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolOcenaTyp_INSERT
')

EXEC('
CREATE TRIGGER ProtokolOcenaTyp_INSERT ON ['+@BazaDanych+'].[dbo].[ProtokolOcenaTyp] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolOcenaTyp'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolOcenaTyp]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolOcenaTyp]
  ,[Org_Nazwa]
  ,[Org_CzyOcenaKoncowa]
  ,[Org_CzyPrzenoszonaNaKarteEgz]
  ,[Org_Waga]
  ,[Org_Lp]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDProtokolOcenaTyp
 ,ins.Nazwa
 ,ins.CzyOcenaKoncowa
 ,ins.CzyPrzenoszonaNaKarteEgz
 ,ins.Waga
 ,ins.Lp
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolOcenaTyp_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolOcenaTyp_UPDATE
')

EXEC('
CREATE TRIGGER ProtokolOcenaTyp_UPDATE ON ['+@BazaDanych+'].[dbo].[ProtokolOcenaTyp] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolOcenaTyp'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolOcenaTyp]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolOcenaTyp]
  ,[Org_Nazwa]
  ,[Org_CzyOcenaKoncowa]
  ,[Org_CzyPrzenoszonaNaKarteEgz]
  ,[Org_Waga]
  ,[Org_Lp]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDProtokolOcenaTyp
 ,ins.Nazwa
 ,ins.CzyOcenaKoncowa
 ,ins.CzyPrzenoszonaNaKarteEgz
 ,ins.Waga
 ,ins.Lp
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolOcenaTyp_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolOcenaTyp_DELETE
')

EXEC('
CREATE TRIGGER ProtokolOcenaTyp_DELETE ON ['+@BazaDanych+'].[dbo].[ProtokolOcenaTyp] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolOcenaTyp'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolOcenaTyp]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolOcenaTyp]
  ,[Org_Nazwa]
  ,[Org_CzyOcenaKoncowa]
  ,[Org_CzyPrzenoszonaNaKarteEgz]
  ,[Org_Waga]
  ,[Org_Lp]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDProtokolOcenaTyp
 ,del.Nazwa
 ,del.CzyOcenaKoncowa
 ,del.CzyPrzenoszonaNaKarteEgz
 ,del.Waga
 ,del.Lp
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'ProtokolOcenaTyp')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('ProtokolOcenaTyp', 1, 0,GetDate())

GO






--------------------------------------------------------wdro3-----------------------------------------------------


-- ******************* -- 2018_03_16_11_ESS_ProtokolTypOcenaTyp_Trigger -- ******************* --


DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------ProtokolTypOcenaTyp-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolTypOcenaTyp_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolTypOcenaTyp_INSERT
')

EXEC('
CREATE TRIGGER ProtokolTypOcenaTyp_INSERT ON ['+@BazaDanych+'].[dbo].[ProtokolTypOcenaTyp] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolTypOcenaTyp'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolTypOcenaTyp]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolTypOcenaTyp]
  ,[Org_ProtokolOcenaTypID]
  ,[Org_ProtokolTypID]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDProtokolTypOcenaTyp
 ,ins.ProtokolOcenaTypID
 ,ins.ProtokolTypID
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolTypOcenaTyp_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolTypOcenaTyp_UPDATE
')

EXEC('
CREATE TRIGGER ProtokolTypOcenaTyp_UPDATE ON ['+@BazaDanych+'].[dbo].[ProtokolTypOcenaTyp] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolTypOcenaTyp'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolTypOcenaTyp]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolTypOcenaTyp]
  ,[Org_ProtokolOcenaTypID]
  ,[Org_ProtokolTypID]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDProtokolTypOcenaTyp
 ,ins.ProtokolOcenaTypID
 ,ins.ProtokolTypID
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolTypOcenaTyp_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolTypOcenaTyp_DELETE
')

EXEC('
CREATE TRIGGER ProtokolTypOcenaTyp_DELETE ON ['+@BazaDanych+'].[dbo].[ProtokolTypOcenaTyp] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolTypOcenaTyp'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolTypOcenaTyp]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolTypOcenaTyp]
  ,[Org_ProtokolOcenaTypID]
  ,[Org_ProtokolTypID]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDProtokolTypOcenaTyp
 ,del.ProtokolOcenaTypID
 ,del.ProtokolTypID
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'ProtokolTypOcenaTyp')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('ProtokolTypOcenaTyp', 1, 0,GetDate())

GO


--------------------------------------------------wdro4------------------------------------------------------

DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------Protokol-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''Protokol_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER Protokol_INSERT
')

EXEC('
CREATE TRIGGER Protokol_INSERT ON ['+@BazaDanych+'].[dbo].[Protokol] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''Protokol'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_Protokol]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokol]
  ,[Org_ProtokolTypID]
  ,[Org_GrupaZajeciowaID]
  ,[Org_KursGrupaZajID]
  ,[Org_Nazwa]
  ,[Org_RokAkad]
  ,[Org_SemestrID]
  ,[Org_FormaWymiarID]
  ,[Org_SkalaOcenID]
  ,[Org_Opis]
  ,[Org_DataZablokowania]
  ,[Org_Zablokowany]
  ,[Org_CzyZablokowanyAutomatycznie]
  ,[Org_DataUdostepnienia]
  ,[Org_TrybStudiowID]
  ,[Org_SystemStudiowID]
  ,[Org_SekcjaID]
  ,[Org_KierunekID]
  ,[Org_CzyZajeciaWspolne]
  ,[Org_CzyDodanyAutomatycznie]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDProtokol
 ,ins.ProtokolTypID
 ,ins.GrupaZajeciowaID
 ,ins.KursGrupaZajID
 ,ins.Nazwa
 ,ins.RokAkad
 ,ins.SemestrID
 ,ins.FormaWymiarID
 ,ins.SkalaOcenID
 ,ins.Opis
 ,ins.DataZablokowania
 ,ins.Zablokowany
 ,ins.CzyZablokowanyAutomatycznie
 ,ins.DataUdostepnienia
 ,ins.TrybStudiowID
 ,ins.SystemStudiowID
 ,ins.SekcjaID
 ,ins.KierunekID
 ,ins.CzyZajeciaWspolne
 ,ins.CzyDodanyAutomatycznie
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''Protokol_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER Protokol_UPDATE
')

EXEC('
CREATE TRIGGER Protokol_UPDATE ON ['+@BazaDanych+'].[dbo].[Protokol] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''Protokol'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_Protokol]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokol]
  ,[Org_ProtokolTypID]
  ,[Org_GrupaZajeciowaID]
  ,[Org_KursGrupaZajID]
  ,[Org_Nazwa]
  ,[Org_RokAkad]
  ,[Org_SemestrID]
  ,[Org_FormaWymiarID]
  ,[Org_SkalaOcenID]
  ,[Org_Opis]
  ,[Org_DataZablokowania]
  ,[Org_Zablokowany]
  ,[Org_CzyZablokowanyAutomatycznie]
  ,[Org_DataUdostepnienia]
  ,[Org_TrybStudiowID]
  ,[Org_SystemStudiowID]
  ,[Org_SekcjaID]
  ,[Org_KierunekID]
  ,[Org_CzyZajeciaWspolne]
  ,[Org_CzyDodanyAutomatycznie]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDProtokol
 ,ins.ProtokolTypID
 ,ins.GrupaZajeciowaID
 ,ins.KursGrupaZajID
 ,ins.Nazwa
 ,ins.RokAkad
 ,ins.SemestrID
 ,ins.FormaWymiarID
 ,ins.SkalaOcenID
 ,ins.Opis
 ,ins.DataZablokowania
 ,ins.Zablokowany
 ,ins.CzyZablokowanyAutomatycznie
 ,ins.DataUdostepnienia
 ,ins.TrybStudiowID
 ,ins.SystemStudiowID
 ,ins.SekcjaID
 ,ins.KierunekID
 ,ins.CzyZajeciaWspolne
 ,ins.CzyDodanyAutomatycznie
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''Protokol_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER Protokol_DELETE
')

EXEC('
CREATE TRIGGER Protokol_DELETE ON ['+@BazaDanych+'].[dbo].[Protokol] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''Protokol'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_Protokol]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokol]
  ,[Org_ProtokolTypID]
  ,[Org_GrupaZajeciowaID]
  ,[Org_KursGrupaZajID]
  ,[Org_Nazwa]
  ,[Org_RokAkad]
  ,[Org_SemestrID]
  ,[Org_FormaWymiarID]
  ,[Org_SkalaOcenID]
  ,[Org_Opis]
  ,[Org_DataZablokowania]
  ,[Org_Zablokowany]
  ,[Org_CzyZablokowanyAutomatycznie]
  ,[Org_DataUdostepnienia]
  ,[Org_TrybStudiowID]
  ,[Org_SystemStudiowID]
  ,[Org_SekcjaID]
  ,[Org_KierunekID]
  ,[Org_CzyZajeciaWspolne]
  ,[Org_CzyDodanyAutomatycznie]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDProtokol
 ,del.ProtokolTypID
 ,del.GrupaZajeciowaID
 ,del.KursGrupaZajID
 ,del.Nazwa
 ,del.RokAkad
 ,del.SemestrID
 ,del.FormaWymiarID
 ,del.SkalaOcenID
 ,del.Opis
 ,del.DataZablokowania
 ,del.Zablokowany
 ,del.CzyZablokowanyAutomatycznie
 ,del.DataUdostepnienia
 ,del.TrybStudiowID
 ,del.SystemStudiowID
 ,del.SekcjaID
 ,del.KierunekID
 ,del.CzyZajeciaWspolne
 ,del.CzyDodanyAutomatycznie
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'Protokol')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('Protokol', 1, 0,GetDate())

go



---------------------------------------------------------wdro5---------------------------------------------


DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------ProtokolOcenaDef-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolOcenaDef_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolOcenaDef_INSERT
')

EXEC('
CREATE TRIGGER ProtokolOcenaDef_INSERT ON ['+@BazaDanych+'].[dbo].[ProtokolOcenaDef] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolOcenaDef'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolOcenaDef]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolOcenaDef]
  ,[Org_ProtokolID]
  ,[Org_ProtokolOcenaTypID]
  ,[Org_RezerwacjaSalID]
  ,[Org_Termin]
  ,[Org_Opis]
  ,[Org_CzyOcenaKoncowa]
  ,[Org_Waga]
  ,[Org_DodalTyp]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_ZmodyfikowalTyp]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDProtokolOcenaDef
 ,ins.ProtokolID
 ,ins.ProtokolOcenaTypID
 ,ins.RezerwacjaSalID
 ,ins.Termin
 ,ins.Opis
 ,ins.CzyOcenaKoncowa
 ,ins.Waga
 ,ins.DodalTyp
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.ZmodyfikowalTyp
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolOcenaDef_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolOcenaDef_UPDATE
')

EXEC('
CREATE TRIGGER ProtokolOcenaDef_UPDATE ON ['+@BazaDanych+'].[dbo].[ProtokolOcenaDef] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolOcenaDef'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolOcenaDef]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolOcenaDef]
  ,[Org_ProtokolID]
  ,[Org_ProtokolOcenaTypID]
  ,[Org_RezerwacjaSalID]
  ,[Org_Termin]
  ,[Org_Opis]
  ,[Org_CzyOcenaKoncowa]
  ,[Org_Waga]
  ,[Org_DodalTyp]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_ZmodyfikowalTyp]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDProtokolOcenaDef
 ,ins.ProtokolID
 ,ins.ProtokolOcenaTypID
 ,ins.RezerwacjaSalID
 ,ins.Termin
 ,ins.Opis
 ,ins.CzyOcenaKoncowa
 ,ins.Waga
 ,ins.DodalTyp
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.ZmodyfikowalTyp
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolOcenaDef_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolOcenaDef_DELETE
')

EXEC('
CREATE TRIGGER ProtokolOcenaDef_DELETE ON ['+@BazaDanych+'].[dbo].[ProtokolOcenaDef] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolOcenaDef'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolOcenaDef]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolOcenaDef]
  ,[Org_ProtokolID]
  ,[Org_ProtokolOcenaTypID]
  ,[Org_RezerwacjaSalID]
  ,[Org_Termin]
  ,[Org_Opis]
  ,[Org_CzyOcenaKoncowa]
  ,[Org_Waga]
  ,[Org_DodalTyp]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_ZmodyfikowalTyp]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDProtokolOcenaDef
 ,del.ProtokolID
 ,del.ProtokolOcenaTypID
 ,del.RezerwacjaSalID
 ,del.Termin
 ,del.Opis
 ,del.CzyOcenaKoncowa
 ,del.Waga
 ,del.DodalTyp
 ,del.Dodal
 ,del.DataDodania
 ,del.ZmodyfikowalTyp
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'ProtokolOcenaDef')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('ProtokolOcenaDef', 1, 0,GetDate())
go



----------------------------------------------------wdro6-------------------------------------------------------

DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------ProtokolZdarzenie-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolZdarzenie_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolZdarzenie_INSERT
')

EXEC('
CREATE TRIGGER ProtokolZdarzenie_INSERT ON ['+@BazaDanych+'].[dbo].[ProtokolZdarzenie] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolZdarzenie'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolZdarzenie]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolZdarzenie]
  ,[Org_ZdarzenieID]
  ,[Org_IndeksID]
  ,[Org_HistoriaStudentaID]
  ,[Org_DataOd]
  ,[Org_DataDo]
  ,[Org_CzyUsuniety]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDProtokolZdarzenie
 ,ins.ZdarzenieID
 ,ins.IndeksID
 ,ins.HistoriaStudentaID
 ,ins.DataOd
 ,ins.DataDo
 ,ins.CzyUsuniety
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolZdarzenie_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolZdarzenie_UPDATE
')

EXEC('
CREATE TRIGGER ProtokolZdarzenie_UPDATE ON ['+@BazaDanych+'].[dbo].[ProtokolZdarzenie] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolZdarzenie'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolZdarzenie]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolZdarzenie]
  ,[Org_ZdarzenieID]
  ,[Org_IndeksID]
  ,[Org_HistoriaStudentaID]
  ,[Org_DataOd]
  ,[Org_DataDo]
  ,[Org_CzyUsuniety]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDProtokolZdarzenie
 ,ins.ZdarzenieID
 ,ins.IndeksID
 ,ins.HistoriaStudentaID
 ,ins.DataOd
 ,ins.DataDo
 ,ins.CzyUsuniety
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolZdarzenie_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolZdarzenie_DELETE
')

EXEC('
CREATE TRIGGER ProtokolZdarzenie_DELETE ON ['+@BazaDanych+'].[dbo].[ProtokolZdarzenie] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolZdarzenie'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolZdarzenie]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolZdarzenie]
  ,[Org_ZdarzenieID]
  ,[Org_IndeksID]
  ,[Org_HistoriaStudentaID]
  ,[Org_DataOd]
  ,[Org_DataDo]
  ,[Org_CzyUsuniety]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDProtokolZdarzenie
 ,del.ZdarzenieID
 ,del.IndeksID
 ,del.HistoriaStudentaID
 ,del.DataOd
 ,del.DataDo
 ,del.CzyUsuniety
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'ProtokolZdarzenie')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('ProtokolZdarzenie', 1, 0,GetDate())

go



--------------------------------------------------------wdro7------------------------------------------------

DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------ProtokolPowod-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolPowod_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolPowod_INSERT
')

EXEC('
CREATE TRIGGER ProtokolPowod_INSERT ON ['+@BazaDanych+'].[dbo].[ProtokolPowod] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolPowod'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolPowod]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolPowod]
  ,[Org_Nazwa]
  ,[Org_ZdarzenieID]
  ,[Org_ProtokolTypID]
  ,[Org_CzyWymaganyPowodText]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDProtokolPowod
 ,ins.Nazwa
 ,ins.ZdarzenieID
 ,ins.ProtokolTypID
 ,ins.CzyWymaganyPowodText
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolPowod_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolPowod_UPDATE
')

EXEC('
CREATE TRIGGER ProtokolPowod_UPDATE ON ['+@BazaDanych+'].[dbo].[ProtokolPowod] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolPowod'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolPowod]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolPowod]
  ,[Org_Nazwa]
  ,[Org_ZdarzenieID]
  ,[Org_ProtokolTypID]
  ,[Org_CzyWymaganyPowodText]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDProtokolPowod
 ,ins.Nazwa
 ,ins.ZdarzenieID
 ,ins.ProtokolTypID
 ,ins.CzyWymaganyPowodText
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolPowod_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolPowod_DELETE
')

EXEC('
CREATE TRIGGER ProtokolPowod_DELETE ON ['+@BazaDanych+'].[dbo].[ProtokolPowod] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolPowod'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolPowod]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolPowod]
  ,[Org_Nazwa]
  ,[Org_ZdarzenieID]
  ,[Org_ProtokolTypID]
  ,[Org_CzyWymaganyPowodText]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDProtokolPowod
 ,del.Nazwa
 ,del.ZdarzenieID
 ,del.ProtokolTypID
 ,del.CzyWymaganyPowodText
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'ProtokolPowod')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('ProtokolPowod', 1, 0,GetDate())
go



-----------------------------------------------------wdro8------------------------------------------------------------

DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------ProtokolPoz-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolPoz_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolPoz_INSERT
')

EXEC('
CREATE TRIGGER ProtokolPoz_INSERT ON ['+@BazaDanych+'].[dbo].[ProtokolPoz] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolPoz'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolPoz]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolPoz]
  ,[Org_ProtokolID]
  ,[Org_IndeksID]
  ,[Org_CzyPozycjaSkorygowana]
  ,[Org_ZatwierdzilTyp]
  ,[Org_Zatwierdzil]
  ,[Org_Zatwierdzona]
  ,[Org_DataZatwierdzenia]
  ,[Org_GrupaZajIndeksID]
  ,[Org_KursPozIndeksID]
  ,[Org_KartaEgzFormaID]
  ,[Org_Uwagi]
  ,[Org_CzyUsuniety]
  ,[Org_DataUsuniecia]
  ,[Org_FormaWymiarID]
  ,[Org_TypUczestnictwaID]
  ,[Org_CzyZatwierdzonyAutomatycznie]
  ,[Org_CzyDodanyAutomatycznie]
  ,[Org_CzyUsunietyAutomatycznie]
  ,[Org_CzyKEFormaAktualna]
  ,[Org_DodalTyp]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_ZmodyfikowalTyp]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDProtokolPoz
 ,ins.ProtokolID
 ,ins.IndeksID
 ,ins.CzyPozycjaSkorygowana
 ,ins.ZatwierdzilTyp
 ,ins.Zatwierdzil
 ,ins.Zatwierdzona
 ,ins.DataZatwierdzenia
 ,ins.GrupaZajIndeksID
 ,ins.KursPozIndeksID
 ,ins.KartaEgzFormaID
 ,ins.Uwagi
 ,ins.CzyUsuniety
 ,ins.DataUsuniecia
 ,ins.FormaWymiarID
 ,ins.TypUczestnictwaID
 ,ins.CzyZatwierdzonyAutomatycznie
 ,ins.CzyDodanyAutomatycznie
 ,ins.CzyUsunietyAutomatycznie
 ,ins.CzyKEFormaAktualna
 ,ins.DodalTyp
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.ZmodyfikowalTyp
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolPoz_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolPoz_UPDATE
')

EXEC('
CREATE TRIGGER ProtokolPoz_UPDATE ON ['+@BazaDanych+'].[dbo].[ProtokolPoz] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolPoz'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolPoz]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolPoz]
  ,[Org_ProtokolID]
  ,[Org_IndeksID]
  ,[Org_CzyPozycjaSkorygowana]
  ,[Org_ZatwierdzilTyp]
  ,[Org_Zatwierdzil]
  ,[Org_Zatwierdzona]
  ,[Org_DataZatwierdzenia]
  ,[Org_GrupaZajIndeksID]
  ,[Org_KursPozIndeksID]
  ,[Org_KartaEgzFormaID]
  ,[Org_Uwagi]
  ,[Org_CzyUsuniety]
  ,[Org_DataUsuniecia]
  ,[Org_FormaWymiarID]
  ,[Org_TypUczestnictwaID]
  ,[Org_CzyZatwierdzonyAutomatycznie]
  ,[Org_CzyDodanyAutomatycznie]
  ,[Org_CzyUsunietyAutomatycznie]
  ,[Org_CzyKEFormaAktualna]
  ,[Org_DodalTyp]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_ZmodyfikowalTyp]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDProtokolPoz
 ,ins.ProtokolID
 ,ins.IndeksID
 ,ins.CzyPozycjaSkorygowana
 ,ins.ZatwierdzilTyp
 ,ins.Zatwierdzil
 ,ins.Zatwierdzona
 ,ins.DataZatwierdzenia
 ,ins.GrupaZajIndeksID
 ,ins.KursPozIndeksID
 ,ins.KartaEgzFormaID
 ,ins.Uwagi
 ,ins.CzyUsuniety
 ,ins.DataUsuniecia
 ,ins.FormaWymiarID
 ,ins.TypUczestnictwaID
 ,ins.CzyZatwierdzonyAutomatycznie
 ,ins.CzyDodanyAutomatycznie
 ,ins.CzyUsunietyAutomatycznie
 ,ins.CzyKEFormaAktualna
 ,ins.DodalTyp
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.ZmodyfikowalTyp
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolPoz_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolPoz_DELETE
')

EXEC('
CREATE TRIGGER ProtokolPoz_DELETE ON ['+@BazaDanych+'].[dbo].[ProtokolPoz] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolPoz'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolPoz]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolPoz]
  ,[Org_ProtokolID]
  ,[Org_IndeksID]
  ,[Org_CzyPozycjaSkorygowana]
  ,[Org_ZatwierdzilTyp]
  ,[Org_Zatwierdzil]
  ,[Org_Zatwierdzona]
  ,[Org_DataZatwierdzenia]
  ,[Org_GrupaZajIndeksID]
  ,[Org_KursPozIndeksID]
  ,[Org_KartaEgzFormaID]
  ,[Org_Uwagi]
  ,[Org_CzyUsuniety]
  ,[Org_DataUsuniecia]
  ,[Org_FormaWymiarID]
  ,[Org_TypUczestnictwaID]
  ,[Org_CzyZatwierdzonyAutomatycznie]
  ,[Org_CzyDodanyAutomatycznie]
  ,[Org_CzyUsunietyAutomatycznie]
  ,[Org_CzyKEFormaAktualna]
  ,[Org_DodalTyp]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_ZmodyfikowalTyp]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDProtokolPoz
 ,del.ProtokolID
 ,del.IndeksID
 ,del.CzyPozycjaSkorygowana
 ,del.ZatwierdzilTyp
 ,del.Zatwierdzil
 ,del.Zatwierdzona
 ,del.DataZatwierdzenia
 ,del.GrupaZajIndeksID
 ,del.KursPozIndeksID
 ,del.KartaEgzFormaID
 ,del.Uwagi
 ,del.CzyUsuniety
 ,del.DataUsuniecia
 ,del.FormaWymiarID
 ,del.TypUczestnictwaID
 ,del.CzyZatwierdzonyAutomatycznie
 ,del.CzyDodanyAutomatycznie
 ,del.CzyUsunietyAutomatycznie
 ,del.CzyKEFormaAktualna
 ,del.DodalTyp
 ,del.Dodal
 ,del.DataDodania
 ,del.ZmodyfikowalTyp
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'ProtokolPoz')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('ProtokolPoz', 1, 0,GetDate())
go




-----------------------------------------------------------------wdro9----------------------------------------------------


DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------ProtokolOcena-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolOcena_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolOcena_INSERT
')

EXEC('
CREATE TRIGGER ProtokolOcena_INSERT ON ['+@BazaDanych+'].[dbo].[ProtokolOcena] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolOcena'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolOcena]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolOcena]
  ,[Org_ProtokolPozID]
  ,[Org_ProtokolOcenaDefID]
  ,[Org_OcenaID]
  ,[Org_Waga]
  ,[Org_DataZaliczenia]
  ,[Org_DodalTyp]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_ZmodyfikowalTyp]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDProtokolOcena
 ,ins.ProtokolPozID
 ,ins.ProtokolOcenaDefID
 ,ins.OcenaID
 ,ins.Waga
 ,ins.DataZaliczenia
 ,ins.DodalTyp
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.ZmodyfikowalTyp
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolOcena_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolOcena_UPDATE
')

EXEC('
CREATE TRIGGER ProtokolOcena_UPDATE ON ['+@BazaDanych+'].[dbo].[ProtokolOcena] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolOcena'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolOcena]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolOcena]
  ,[Org_ProtokolPozID]
  ,[Org_ProtokolOcenaDefID]
  ,[Org_OcenaID]
  ,[Org_Waga]
  ,[Org_DataZaliczenia]
  ,[Org_DodalTyp]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_ZmodyfikowalTyp]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDProtokolOcena
 ,ins.ProtokolPozID
 ,ins.ProtokolOcenaDefID
 ,ins.OcenaID
 ,ins.Waga
 ,ins.DataZaliczenia
 ,ins.DodalTyp
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.ZmodyfikowalTyp
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolOcena_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolOcena_DELETE
')

EXEC('
CREATE TRIGGER ProtokolOcena_DELETE ON ['+@BazaDanych+'].[dbo].[ProtokolOcena] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolOcena'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolOcena]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolOcena]
  ,[Org_ProtokolPozID]
  ,[Org_ProtokolOcenaDefID]
  ,[Org_OcenaID]
  ,[Org_Waga]
  ,[Org_DataZaliczenia]
  ,[Org_DodalTyp]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_ZmodyfikowalTyp]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDProtokolOcena
 ,del.ProtokolPozID
 ,del.ProtokolOcenaDefID
 ,del.OcenaID
 ,del.Waga
 ,del.DataZaliczenia
 ,del.DodalTyp
 ,del.Dodal
 ,del.DataDodania
 ,del.ZmodyfikowalTyp
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'ProtokolOcena')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('ProtokolOcena', 1, 0,GetDate())

go



-------------------------------------------------------------------wdro10----------------------------------------------


DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------ProtokolPracownik-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolPracownik_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolPracownik_INSERT
')

EXEC('
CREATE TRIGGER ProtokolPracownik_INSERT ON ['+@BazaDanych+'].[dbo].[ProtokolPracownik] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolPracownik'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolPracownik]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolPracownik]
  ,[Org_PracownikID]
  ,[Org_ProtokolID]
  ,[Org_CzyDodanyAutomatycznie]
  ,[Org_CzyUsuniety]
  ,[Org_DataUsuniecia]
  ,[Org_CzyUsunietyAutomatycznie]
  ,[Org_CzyBlokowac]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDProtokolPracownik
 ,ins.PracownikID
 ,ins.ProtokolID
 ,ins.CzyDodanyAutomatycznie
 ,ins.CzyUsuniety
 ,ins.DataUsuniecia
 ,ins.CzyUsunietyAutomatycznie
 ,ins.CzyBlokowac
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolPracownik_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolPracownik_UPDATE
')

EXEC('
CREATE TRIGGER ProtokolPracownik_UPDATE ON ['+@BazaDanych+'].[dbo].[ProtokolPracownik] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolPracownik'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolPracownik]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolPracownik]
  ,[Org_PracownikID]
  ,[Org_ProtokolID]
  ,[Org_CzyDodanyAutomatycznie]
  ,[Org_CzyUsuniety]
  ,[Org_DataUsuniecia]
  ,[Org_CzyUsunietyAutomatycznie]
  ,[Org_CzyBlokowac]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDProtokolPracownik
 ,ins.PracownikID
 ,ins.ProtokolID
 ,ins.CzyDodanyAutomatycznie
 ,ins.CzyUsuniety
 ,ins.DataUsuniecia
 ,ins.CzyUsunietyAutomatycznie
 ,ins.CzyBlokowac
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolPracownik_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolPracownik_DELETE
')

EXEC('
CREATE TRIGGER ProtokolPracownik_DELETE ON ['+@BazaDanych+'].[dbo].[ProtokolPracownik] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolPracownik'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolPracownik]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolPracownik]
  ,[Org_PracownikID]
  ,[Org_ProtokolID]
  ,[Org_CzyDodanyAutomatycznie]
  ,[Org_CzyUsuniety]
  ,[Org_DataUsuniecia]
  ,[Org_CzyUsunietyAutomatycznie]
  ,[Org_CzyBlokowac]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDProtokolPracownik
 ,del.PracownikID
 ,del.ProtokolID
 ,del.CzyDodanyAutomatycznie
 ,del.CzyUsuniety
 ,del.DataUsuniecia
 ,del.CzyUsunietyAutomatycznie
 ,del.CzyBlokowac
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'ProtokolPracownik')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('ProtokolPracownik', 1, 0,GetDate())

go



----------------------------------------------------wdro11------------------------------------------------------

DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------ProtokolKorekta-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolKorekta_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolKorekta_INSERT
')

EXEC('
CREATE TRIGGER ProtokolKorekta_INSERT ON ['+@BazaDanych+'].[dbo].[ProtokolKorekta] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolKorekta'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolKorekta]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolKorekta]
  ,[Org_ProtokolPozID]
  ,[Org_ProtokolOcenaID]
  ,[Org_OcenaPrzedID]
  ,[Org_DataZaliczeniaPrzed]
  ,[Org_OcenaPoID]
  ,[Org_DataZaliczeniaPo]
  ,[Org_Waga]
  ,[Org_ProtokolPowodID]
  ,[Org_Powod]
  ,[Org_DodalTyp]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_ZmodyfikowalTyp]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDProtokolKorekta
 ,ins.ProtokolPozID
 ,ins.ProtokolOcenaID
 ,ins.OcenaPrzedID
 ,ins.DataZaliczeniaPrzed
 ,ins.OcenaPoID
 ,ins.DataZaliczeniaPo
 ,ins.Waga
 ,ins.ProtokolPowodID
 ,ins.Powod
 ,ins.DodalTyp
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.ZmodyfikowalTyp
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolKorekta_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolKorekta_UPDATE
')

EXEC('
CREATE TRIGGER ProtokolKorekta_UPDATE ON ['+@BazaDanych+'].[dbo].[ProtokolKorekta] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolKorekta'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolKorekta]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolKorekta]
  ,[Org_ProtokolPozID]
  ,[Org_ProtokolOcenaID]
  ,[Org_OcenaPrzedID]
  ,[Org_DataZaliczeniaPrzed]
  ,[Org_OcenaPoID]
  ,[Org_DataZaliczeniaPo]
  ,[Org_Waga]
  ,[Org_ProtokolPowodID]
  ,[Org_Powod]
  ,[Org_DodalTyp]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_ZmodyfikowalTyp]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDProtokolKorekta
 ,ins.ProtokolPozID
 ,ins.ProtokolOcenaID
 ,ins.OcenaPrzedID
 ,ins.DataZaliczeniaPrzed
 ,ins.OcenaPoID
 ,ins.DataZaliczeniaPo
 ,ins.Waga
 ,ins.ProtokolPowodID
 ,ins.Powod
 ,ins.DodalTyp
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.ZmodyfikowalTyp
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolKorekta_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolKorekta_DELETE
')

EXEC('
CREATE TRIGGER ProtokolKorekta_DELETE ON ['+@BazaDanych+'].[dbo].[ProtokolKorekta] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolKorekta'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolKorekta]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolKorekta]
  ,[Org_ProtokolPozID]
  ,[Org_ProtokolOcenaID]
  ,[Org_OcenaPrzedID]
  ,[Org_DataZaliczeniaPrzed]
  ,[Org_OcenaPoID]
  ,[Org_DataZaliczeniaPo]
  ,[Org_Waga]
  ,[Org_ProtokolPowodID]
  ,[Org_Powod]
  ,[Org_DodalTyp]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_ZmodyfikowalTyp]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDProtokolKorekta
 ,del.ProtokolPozID
 ,del.ProtokolOcenaID
 ,del.OcenaPrzedID
 ,del.DataZaliczeniaPrzed
 ,del.OcenaPoID
 ,del.DataZaliczeniaPo
 ,del.Waga
 ,del.ProtokolPowodID
 ,del.Powod
 ,del.DodalTyp
 ,del.Dodal
 ,del.DataDodania
 ,del.ZmodyfikowalTyp
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'ProtokolKorekta')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('ProtokolKorekta', 1, 0,GetDate())

go


--------------------------------------------------wdro12-------------------------------------------------------

DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------ProtokolKolizjaTyp-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolKolizjaTyp_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolKolizjaTyp_INSERT
')

EXEC('
CREATE TRIGGER ProtokolKolizjaTyp_INSERT ON ['+@BazaDanych+'].[dbo].[ProtokolKolizjaTyp] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolKolizjaTyp'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolKolizjaTyp]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolKolizjaTyp]
  ,[Org_Nazwa]
  ,[Org_Podmiot]
  ,[Org_PodmiotKolizja]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDProtokolKolizjaTyp
 ,ins.Nazwa
 ,ins.Podmiot
 ,ins.PodmiotKolizja
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolKolizjaTyp_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolKolizjaTyp_UPDATE
')

EXEC('
CREATE TRIGGER ProtokolKolizjaTyp_UPDATE ON ['+@BazaDanych+'].[dbo].[ProtokolKolizjaTyp] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolKolizjaTyp'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolKolizjaTyp]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolKolizjaTyp]
  ,[Org_Nazwa]
  ,[Org_Podmiot]
  ,[Org_PodmiotKolizja]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDProtokolKolizjaTyp
 ,ins.Nazwa
 ,ins.Podmiot
 ,ins.PodmiotKolizja
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolKolizjaTyp_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolKolizjaTyp_DELETE
')

EXEC('
CREATE TRIGGER ProtokolKolizjaTyp_DELETE ON ['+@BazaDanych+'].[dbo].[ProtokolKolizjaTyp] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolKolizjaTyp'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolKolizjaTyp]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolKolizjaTyp]
  ,[Org_Nazwa]
  ,[Org_Podmiot]
  ,[Org_PodmiotKolizja]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDProtokolKolizjaTyp
 ,del.Nazwa
 ,del.Podmiot
 ,del.PodmiotKolizja
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'ProtokolKolizjaTyp')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('ProtokolKolizjaTyp', 1, 0,GetDate())

go



----------------------------------------------------------------wdro13------------------------------------------------

DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------ProtokolKolizja-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolKolizja_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolKolizja_INSERT
')

EXEC('
CREATE TRIGGER ProtokolKolizja_INSERT ON ['+@BazaDanych+'].[dbo].[ProtokolKolizja] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolKolizja'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolKolizja]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolKolizja]
  ,[Org_ProtokolKolizjaTypID]
  ,[Org_Podmiot]
  ,[Org_PodmiotID]
  ,[Org_PodmiotKolizja]
  ,[Org_PodmiotKolizjaID]
  ,[Org_DataKolizji]
  ,[Org_DataZakonczeniaKolizji]
  ,[Org_CzyAktywna]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDProtokolKolizja
 ,ins.ProtokolKolizjaTypID
 ,ins.Podmiot
 ,ins.PodmiotID
 ,ins.PodmiotKolizja
 ,ins.PodmiotKolizjaID
 ,ins.DataKolizji
 ,ins.DataZakonczeniaKolizji
 ,ins.CzyAktywna
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolKolizja_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolKolizja_UPDATE
')

EXEC('
CREATE TRIGGER ProtokolKolizja_UPDATE ON ['+@BazaDanych+'].[dbo].[ProtokolKolizja] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolKolizja'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolKolizja]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolKolizja]
  ,[Org_ProtokolKolizjaTypID]
  ,[Org_Podmiot]
  ,[Org_PodmiotID]
  ,[Org_PodmiotKolizja]
  ,[Org_PodmiotKolizjaID]
  ,[Org_DataKolizji]
  ,[Org_DataZakonczeniaKolizji]
  ,[Org_CzyAktywna]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDProtokolKolizja
 ,ins.ProtokolKolizjaTypID
 ,ins.Podmiot
 ,ins.PodmiotID
 ,ins.PodmiotKolizja
 ,ins.PodmiotKolizjaID
 ,ins.DataKolizji
 ,ins.DataZakonczeniaKolizji
 ,ins.CzyAktywna
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ProtokolKolizja_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER ProtokolKolizja_DELETE
')

EXEC('
CREATE TRIGGER ProtokolKolizja_DELETE ON ['+@BazaDanych+'].[dbo].[ProtokolKolizja] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ProtokolKolizja'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ProtokolKolizja]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDProtokolKolizja]
  ,[Org_ProtokolKolizjaTypID]
  ,[Org_Podmiot]
  ,[Org_PodmiotID]
  ,[Org_PodmiotKolizja]
  ,[Org_PodmiotKolizjaID]
  ,[Org_DataKolizji]
  ,[Org_DataZakonczeniaKolizji]
  ,[Org_CzyAktywna]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDProtokolKolizja
 ,del.ProtokolKolizjaTypID
 ,del.Podmiot
 ,del.PodmiotID
 ,del.PodmiotKolizja
 ,del.PodmiotKolizjaID
 ,del.DataKolizji
 ,del.DataZakonczeniaKolizji
 ,del.CzyAktywna
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'ProtokolKolizja')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('ProtokolKolizja', 1, 0,GetDate())

