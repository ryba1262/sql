----------wdro1------2018_03_06_01_ESS_UstawieniaProgramu_Harmonogram_dane
----------wdro2------2018_03_07_01_ESS_DWODrzewko_Kursy_dane
----------wdro3------2018_03_16_04_ESS_ProtokolTyp_Dane
----------wdro4------2018_03_16_08_ESS_ProtokolOcenaTyp_Dane
----------wdro5------2018_03_16_12_ESS_ProtokolTypOcenaTyp_Dane
----------wdro6------2018_03_16_22_01_ESS_ProtokolPowod_Dane.sql
----------wdro7------2018_03_16_28_ESS_UstawieniaProgramu_Protokoly_dane.sql





--------------------------------------------------------wdro1-----------------------------------------------------


-- ******************* -- 2018_03_06_01_ESS_UstawieniaProgramu_Harmonogram_dane -- ******************* --


delete from UstawieniaProgramu where IDUstawieniaProgramu in (1030,1031,1032)

insert into UstawieniaProgramu (IDUstawieniaProgramu,Nazwa,Wartosc, CzyBlokowac, TypPola) 
                               values (1030,'Harmonogramy', NULL, 0, 'Rodzic')
                               
                               
                            
insert into UstawieniaProgramu (IDUstawieniaProgramu,Nazwa,Wartosc, CzyBlokowac, TypPola, IDRodzica, Opis) 
                               values (1031,'HarmRokAkadOd', 2018, 0, 'EditInt',1030,'Rok akademicki od blokowania dodawania grup dziekańskich w harmonogramach')
                               
                               
insert into UstawieniaProgramu (IDUstawieniaProgramu,Nazwa,Wartosc, CzyBlokowac, TypPola,IDRodzica,Opis) 
                               values (1032,'HarmSemestrOdID', 1, 0, 'EditInt',1030,'Semestr od blokowania dodawania grup dziekańskich w harmonogramach')  

GO

--------------------------------------------------------wdro2------------------------------------------------------

-- ******************* -- 2018_03_07_01_ESS_DWODrzewko_Kursy_dane -- ******************* --

 delete from DWOPole where IDDWOPole in (84009,84010, 84011, 84012)
delete from DWOJoin where IDDWOJoin in (84012, 84013)

 
 insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84012,84000,'SemestrAkadPoz','left join OkresRozliczeniowyPoz SemestrAkadPoz on (2-((Kurs.SemestrID-(KursPoz.Semestr-1)%2)%2)) = SemestrAkadPoz.IDOkresRozliczeniowyPoz',84011,2,452,getdate(),NULL,NULL)  
 
  insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84013,84000,'Specjalnosc','left join Specjalnosc on IDSpecjalnosc = Program.SpecjalnoscID',84005,3,452,getdate(),NULL,NULL)  
 
 
 update DWOPole
 set NazwaPola = 'Rok akad siatki'
 where IDDWOPole = 84002
 
 update DWOPole
 set NazwaPola = 'Semestr siatki'
 where IDDWOPole = 84003
 
 

 insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84009,84011,'Rok akad','ROKAKADPOZ','cast((Kurs.RokAkad + Round((KursPoz.Semestr - (Kurs.SemestrID%2))/2,0,1)) as varchar(4)) + ''/'' + cast(((Kurs.RokAkad + Round((KursPoz.Semestr - (Kurs.SemestrID%2))/2,0,1))+1) as varchar(4))',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL) 
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84010,84012,'Semestr','SEMESTRPOZID','SemestrAkadPoz.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)  

insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84011,84013,'Specjalność','SPECJALNOSC','Specjalnosc.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)  

 GO

  --------------------------------------------------------wdro3------------------------------------------------------

-- ******************* -- 2018_03_16_04_ESS_ProtokolTyp_Dane -- ******************* --


 -- -- ProtokolTyp -- -- 
DELETE FROM ProtokolTyp
INSERT INTO [ProtokolTyp](Nazwa,RokAkadOd,SemestrIDOd,RokAkadDo,SemestrIDDo,SposobRozliczeniaID,AlgorytmWyliczania,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('Zaliczeniowy',2018,1,NULL,NULL,5,NULL,452,GETDATE(),NULL,NULL)
INSERT INTO [ProtokolTyp](Nazwa,RokAkadOd,SemestrIDOd,RokAkadDo,SemestrIDDo,SposobRozliczeniaID,AlgorytmWyliczania,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('Egzaminacyjny',2018,1,NULL,NULL,6,NULL,452,GETDATE(),NULL,NULL)
INSERT INTO [ProtokolTyp](Nazwa,RokAkadOd,SemestrIDOd,RokAkadDo,SemestrIDDo,SposobRozliczeniaID,AlgorytmWyliczania,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('Zaliczeniowy bez oceny',2018,1,NULL,NULL,8,NULL,452,GETDATE(),NULL,NULL)
GO


  --------------------------------------------------------wdro4------------------------------------------------------

-- ******************* -- 2018_03_16_08_ESS_ProtokolOcenaTyp_Dane -- ******************* --


 -- -- ProtokolOcenaTyp -- -- 
DELETE FROM ProtokolOcenaTyp
INSERT INTO [ProtokolOcenaTyp](Nazwa,CzyOcenaKoncowa,CzyPrzenoszonaNaKarteEgz,Waga,Lp,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('Ocena - Termin I',0,0,'0',1,452,GETDATE(),NULL,NULL)
INSERT INTO [ProtokolOcenaTyp](Nazwa,CzyOcenaKoncowa,CzyPrzenoszonaNaKarteEgz,Waga,Lp,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('Ocena - Termin II',0,0,'0',2,452,GETDATE(),NULL,NULL)
INSERT INTO [ProtokolOcenaTyp](Nazwa,CzyOcenaKoncowa,CzyPrzenoszonaNaKarteEgz,Waga,Lp,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('Ocena - Termin III',0,0,'0',3,452,GETDATE(),NULL,NULL)
INSERT INTO [ProtokolOcenaTyp](Nazwa,CzyOcenaKoncowa,CzyPrzenoszonaNaKarteEgz,Waga,Lp,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('Ocena',0,1,'0',4,452,GETDATE(),NULL,NULL)
INSERT INTO [ProtokolOcenaTyp](Nazwa,CzyOcenaKoncowa,CzyPrzenoszonaNaKarteEgz,Waga,Lp,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('Ocena końcowa',1,1,'0',5,452,GETDATE(),NULL,NULL)
GO






  --------------------------------------------------------wdro5------------------------------------------------------

-- ******************* -- 2018_03_16_12_ESS_ProtokolTypOcenaTyp_Dane -- ******************* --



delete from ProtokolTypOcenaTyp
insert into ProtokolTypOcenaTyp (ProtokolTypID,ProtokolOcenaTypID,Dodal,DataDodania)
select IDProtokolTyp,IDProtokolOcenaTyp, 452, GETDATE() from ProtokolTyp,ProtokolOcenaTyp
GO



-----------------------------------------------wdro6-------------------------------------------------


 -- -- ProtokolPowod -- -- 
DELETE FROM ProtokolPowod
INSERT INTO [ProtokolPowod](Nazwa,ZdarzenieID,ProtokolTypID,CzyWymaganyPowodText,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
select 'IOS',48,IDProtokolTyp,0,452,GETDATE(),NULL,NULL from ProtokolTyp
INSERT INTO [ProtokolPowod](Nazwa,ZdarzenieID,ProtokolTypID,CzyWymaganyPowodText,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
select 'Przedłużenie sesji egzaminacyjnej',45,IDProtokolTyp,0,452,GETDATE(),NULL,NULL from ProtokolTyp
INSERT INTO [ProtokolPowod](Nazwa,ZdarzenieID,ProtokolTypID,CzyWymaganyPowodText,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
select 'Przedłużenie pracy dyplomowej',38,IDProtokolTyp,0,452,GETDATE(),NULL,NULL from ProtokolTyp
INSERT INTO [ProtokolPowod](Nazwa,ZdarzenieID,ProtokolTypID,CzyWymaganyPowodText,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
select 'Inny',NULL,IDProtokolTyp,1,452,GETDATE(),NULL,NULL from ProtokolTyp
GO


-------------------------------------------------------wdro7------------------------------------------------------



delete from UstawieniaProgramu where IDUstawieniaProgramu in (1033,1034,1035)

insert into UstawieniaProgramu (IDUstawieniaProgramu,Nazwa,Wartosc, CzyBlokowac, TypPola) 
                               values (1033,'Protokoły', NULL, 0, 'Rodzic')
                               
                               
                            
insert into UstawieniaProgramu (IDUstawieniaProgramu,Nazwa,Wartosc, CzyBlokowac, TypPola, IDRodzica, Opis) 
                               values (1034,'PRokAkadOd', 2018, 0, 'EditInt',1033,'Rok akademicki od obowiązywania nowych protokołów')
                               
                               
insert into UstawieniaProgramu (IDUstawieniaProgramu,Nazwa,Wartosc, CzyBlokowac, TypPola,IDRodzica,Opis) 
                               values (1035,'PSemestrOdID', 1, 0, 'EditInt',1033,'Semestr od obowiązywania nowych protokołów') 



