--------------------wdro1--------------------2017_08_29_03_ESS_RekrutacjaPromocja_Trigger
--------------------wdro2--------------------2017_09_04_03_ESS_OplataDodatkowa_Trigger
--------------------wdro3--------------------2017_09_04_06_ESS_HistoriaOplat_Trigger


-------------------------------------------------------wdro1--------------------------------------------------------


DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''RekrutacjaPromocja_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER RekrutacjaPromocja_INSERT
')

EXEC('
CREATE TRIGGER RekrutacjaPromocja_INSERT ON ['+@BazaDanych+'].[dbo].[RekrutacjaPromocja] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''RekrutacjaPromocja'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_RekrutacjaPromocja]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDRekrutacjaPromocja]
  ,[Org_Symbol]
  ,[Org_Nazwa]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
  ,@ID
 ,ins.IDRekrutacjaPromocja
 ,ins.Symbol
 ,ins.Nazwa
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''RekrutacjaPromocja_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER RekrutacjaPromocja_UPDATE
')

EXEC('
CREATE TRIGGER RekrutacjaPromocja_UPDATE ON ['+@BazaDanych+'].[dbo].[RekrutacjaPromocja] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''RekrutacjaPromocja'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_RekrutacjaPromocja]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDRekrutacjaPromocja]
  ,[Org_Symbol]
  ,[Org_Nazwa]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
  ,@ID
 ,ins.IDRekrutacjaPromocja
 ,ins.Symbol
 ,ins.Nazwa
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''RekrutacjaPromocja_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER RekrutacjaPromocja_DELETE
')

EXEC('
CREATE TRIGGER RekrutacjaPromocja_DELETE ON ['+@BazaDanych+'].[dbo].[RekrutacjaPromocja] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''RekrutacjaPromocja'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_RekrutacjaPromocja]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDRekrutacjaPromocja]
  ,[Org_Symbol]
  ,[Org_Nazwa]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
  ,@ID
 ,del.IDRekrutacjaPromocja
 ,del.Symbol
 ,del.Nazwa
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')

if not exists (select * from LOG_LogowanaTabela where Tabela = 'RekrutacjaPromocja')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('RekrutacjaPromocja', 1, 0,GetDate())
GO




------------------------------------------------------wdro2---------------------------------------------------------

DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------OplataDodatkowa-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''OplataDodatkowa_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER OplataDodatkowa_INSERT
')

EXEC('
CREATE TRIGGER OplataDodatkowa_INSERT ON ['+@BazaDanych+'].[dbo].[OplataDodatkowa] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''OplataDodatkowa'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_OplataDodatkowa]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDOplataDodatkowa]
  ,[Org_Nazwa]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDOplataDodatkowa
 ,ins.Nazwa
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''OplataDodatkowa_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER OplataDodatkowa_UPDATE
')

EXEC('
CREATE TRIGGER OplataDodatkowa_UPDATE ON ['+@BazaDanych+'].[dbo].[OplataDodatkowa] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''OplataDodatkowa'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_OplataDodatkowa]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDOplataDodatkowa]
  ,[Org_Nazwa]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDOplataDodatkowa
 ,ins.Nazwa
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''OplataDodatkowa_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER OplataDodatkowa_DELETE
')

EXEC('
CREATE TRIGGER OplataDodatkowa_DELETE ON ['+@BazaDanych+'].[dbo].[OplataDodatkowa] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''OplataDodatkowa'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_OplataDodatkowa]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDOplataDodatkowa]
  ,[Org_Nazwa]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDOplataDodatkowa
 ,del.Nazwa
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'OplataDodatkowa')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('OplataDodatkowa', 1, 0,GetDate())

go

-----------------------------------------------------wdro3------------------------------------------------


DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------HistoriaOplat-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''HistoriaOplat_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER HistoriaOplat_INSERT
')

EXEC('
CREATE TRIGGER HistoriaOplat_INSERT ON ['+@BazaDanych+'].[dbo].[HistoriaOplat] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''HistoriaOplat'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_HistoriaOplat]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDHistoriaOplat]
  ,[Org_IndeksID]
  ,[Org_OplataDodatkowaID]
  ,[Org_Lp]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDHistoriaOplat
 ,ins.IndeksID
 ,ins.OplataDodatkowaID
 ,ins.Lp
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''HistoriaOplat_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER HistoriaOplat_UPDATE
')

EXEC('
CREATE TRIGGER HistoriaOplat_UPDATE ON ['+@BazaDanych+'].[dbo].[HistoriaOplat] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''HistoriaOplat'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_HistoriaOplat]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDHistoriaOplat]
  ,[Org_IndeksID]
  ,[Org_OplataDodatkowaID]
  ,[Org_Lp]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDHistoriaOplat
 ,ins.IndeksID
 ,ins.OplataDodatkowaID
 ,ins.Lp
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''HistoriaOplat_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER HistoriaOplat_DELETE
')

EXEC('
CREATE TRIGGER HistoriaOplat_DELETE ON ['+@BazaDanych+'].[dbo].[HistoriaOplat] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''HistoriaOplat'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_HistoriaOplat]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDHistoriaOplat]
  ,[Org_IndeksID]
  ,[Org_OplataDodatkowaID]
  ,[Org_Lp]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDHistoriaOplat
 ,del.IndeksID
 ,del.OplataDodatkowaID
 ,del.Lp
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'HistoriaOplat')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('HistoriaOplat', 1, 0,GetDate())



