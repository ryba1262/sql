--------------------wdro1----------------2017_09_05_01_ESS_OplatyDodatkowe_Slownik_dane
--------------------wdro2----------------2017_09_07_01_ESS_HistoriaOplat_Uprawnienia_dane


---------------------------------------------wdro1----------------------------------------------

declare @IDDWOObiekt int

select @IDDWOObiekt = IDDWOObiekt from DWOObiekt where Sygnatura = 'dxBarButtonOplatyDodatkowe'

exec [dbo].[DWOUsunUprawnieniaDoObiektu] @IDDWOObiekt

delete from DWOObiekt where Sygnatura = 'dxBarButtonOplatyDodatkowe'

declare @IDDWOObiektNadrzedny int
select @IDDWOObiektNadrzedny = IDDWOObiekt from DWOObiekt where Sygnatura = 'dxBarSubItemSlowniki'

declare @UserID int
select @UserID = IDUzytkownik from Uzytkownik where Login = 'ktybinkowska'

INSERT INTO [DWOObiekt](ObiektNadrzednyID,DWOZestawUprawnienID,Sygnatura,Nazwa,Opis,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values(@IDDWOObiektNadrzedny,4,'dxBarButtonOplatyDodatkowe','Opłaty dodatkowe',NULL,coalesce(@UserID,0),getdate(),NULL,NULL)

select @IDDWOObiekt = scope_identity() 

exec [dbo].[DWODodajUprawnieniaDoObiektu] @IDDWOObiekt, 1

go


--------------------------------------------------wdro2-----------------------------------------------




declare @IDDWOObiekt int

select @IDDWOObiekt = IDDWOObiekt from DWOObiekt where Sygnatura = 'dxBarButtonHistoriaOplat'

exec [dbo].[DWOUsunUprawnieniaDoObiektu] @IDDWOObiekt

delete from DWOObiekt where Sygnatura = 'dxBarButtonHistoriaOplat'

declare @IDDWOObiektNadrzedny int
select @IDDWOObiektNadrzedny = IDDWOObiekt from DWOObiekt where Sygnatura = 'dxBarButtonEwidencjaStudentow'

declare @UserID int
select @UserID = IDUzytkownik from Uzytkownik where Login = 'ktybinkowska'

INSERT INTO [DWOObiekt](ObiektNadrzednyID,DWOZestawUprawnienID,Sygnatura,Nazwa,Opis,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values(@IDDWOObiektNadrzedny,3,'dxBarButtonHistoriaOplat','Historia opłat',NULL,coalesce(@UserID,0),getdate(),NULL,NULL)

select @IDDWOObiekt = scope_identity() 

exec [dbo].[DWODodajUprawnieniaDoObiektu] @IDDWOObiekt, 1

