--------------wdro1---------------2017_08_29_02_ESS_RekrutacjaPromocja_Log
--------------wdro2---------------2017_09_04_02_ESS_OplataDodatkowa_Log
--------------wdro3---------------2017_09_04_05_ESS_HistoriaOplat_Log







--------------------------------------
----------------RekrutacjaPromocja---------------
--------------------------------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_RekrutacjaPromocja]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_RekrutacjaPromocja] (
   [Log_RekrutacjaPromocjaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDRekrutacjaPromocja] [int] 
  ,[Org_Symbol] [varchar] (10)
  ,[Org_Nazwa] [varchar] (200)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]
ALTER TABLE [dbo].[LOG_RekrutacjaPromocja] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_RekrutacjaPromocja] PRIMARY KEY  CLUSTERED
(
   [Log_RekrutacjaPromocjaID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_RekrutacjaPromocja] (
   [Log_RekrutacjaPromocjaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDRekrutacjaPromocja] [int] 
  ,[Org_Symbol] [varchar] (10)
  ,[Org_Nazwa] [varchar] (200)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]
SET IDENTITY_INSERT dbo.Tmp_LOG_RekrutacjaPromocja ON

IF EXISTS (SELECT * FROM dbo.LOG_RekrutacjaPromocja)
EXEC('INSERT INTO dbo.Tmp_LOG_RekrutacjaPromocja (Log_RekrutacjaPromocjaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDRekrutacjaPromocja, Org_Symbol, Org_Nazwa, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_RekrutacjaPromocjaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDRekrutacjaPromocja, Org_Symbol, Org_Nazwa, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_RekrutacjaPromocja WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_RekrutacjaPromocja OFF

DROP TABLE dbo.LOG_RekrutacjaPromocja

EXECUTE sp_rename N'dbo.Tmp_LOG_RekrutacjaPromocja', N'LOG_RekrutacjaPromocja', 'OBJECT'

ALTER TABLE [dbo].[LOG_RekrutacjaPromocja] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_RekrutacjaPromocja] PRIMARY KEY  CLUSTERED
(
   [Log_RekrutacjaPromocjaID]
) ON [PRIMARY]

END
GO


----------------------------------------------wdro2----------------------------------------------


-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------OplataDodatkowa---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_OplataDodatkowa]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_OplataDodatkowa] (
   [Log_OplataDodatkowaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDOplataDodatkowa] [int] 
  ,[Org_Nazwa] [varchar] (1000)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_OplataDodatkowa] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_OplataDodatkowa] PRIMARY KEY  CLUSTERED
(
   [Log_OplataDodatkowaID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_OplataDodatkowa] (
   [Log_OplataDodatkowaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDOplataDodatkowa] [int] 
  ,[Org_Nazwa] [varchar] (1000)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_OplataDodatkowa ON

IF EXISTS (SELECT * FROM dbo.LOG_OplataDodatkowa)
EXEC('INSERT INTO dbo.Tmp_LOG_OplataDodatkowa (Log_OplataDodatkowaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDOplataDodatkowa, Org_Nazwa, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_OplataDodatkowaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDOplataDodatkowa, Org_Nazwa, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_OplataDodatkowa WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_OplataDodatkowa OFF

DROP TABLE dbo.LOG_OplataDodatkowa

EXECUTE sp_rename N'dbo.Tmp_LOG_OplataDodatkowa', N'LOG_OplataDodatkowa', 'OBJECT'

ALTER TABLE [dbo].[LOG_OplataDodatkowa] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_OplataDodatkowa] PRIMARY KEY  CLUSTERED
(
   [Log_OplataDodatkowaID]
) ON [PRIMARY]

END
GO

-----------------------------------------------------wdro3-----------------------------------------------------


-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------HistoriaOplat---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_HistoriaOplat]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_HistoriaOplat] (
   [Log_HistoriaOplatID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDHistoriaOplat] [int] 
  ,[Org_IndeksID] [int] 
  ,[Org_OplataDodatkowaID] [int] 
  ,[Org_Lp] [int] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_HistoriaOplat] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_HistoriaOplat] PRIMARY KEY  CLUSTERED
(
   [Log_HistoriaOplatID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_HistoriaOplat] (
   [Log_HistoriaOplatID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDHistoriaOplat] [int] 
  ,[Org_IndeksID] [int] 
  ,[Org_OplataDodatkowaID] [int] 
  ,[Org_Lp] [int] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_HistoriaOplat ON

IF EXISTS (SELECT * FROM dbo.LOG_HistoriaOplat)
EXEC('INSERT INTO dbo.Tmp_LOG_HistoriaOplat (Log_HistoriaOplatID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDHistoriaOplat, Org_IndeksID, Org_OplataDodatkowaID, Org_Lp, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_HistoriaOplatID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDHistoriaOplat, Org_IndeksID, Org_OplataDodatkowaID, Org_Lp, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_HistoriaOplat WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_HistoriaOplat OFF

DROP TABLE dbo.LOG_HistoriaOplat

EXECUTE sp_rename N'dbo.Tmp_LOG_HistoriaOplat', N'LOG_HistoriaOplat', 'OBJECT'

ALTER TABLE [dbo].[LOG_HistoriaOplat] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_HistoriaOplat] PRIMARY KEY  CLUSTERED
(
   [Log_HistoriaOplatID]
) ON [PRIMARY]

END
GO



