---------wdro1----------2017_08_29_01_ESS_RekrutacjaPromocja
---------wdro2----------2017_09_04_01_ESS_OplataDodatkowa
---------wdro3----------2017_09_04_04_ESS_HistoriaOplat




---------------------------------------------------wdro1--------------------------------------------------------------

----------------RekrutacjaPromocja---------------
-- Należy zweryfikować constrainty--
--------------------------------------
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Indeks_RekrutacjaPromocja]') AND parent_object_id = OBJECT_ID(N'[dbo].[Indeks]'))
ALTER TABLE [dbo].[Indeks] DROP CONSTRAINT [FK_Indeks_RekrutacjaPromocja]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[RekrutacjaPromocja]') AND name = N'PK_RekrutacjaPromocja')
ALTER TABLE [dbo].[RekrutacjaPromocja] DROP CONSTRAINT [PK_RekrutacjaPromocja]


if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[RekrutacjaPromocja]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[RekrutacjaPromocja] (
   [IDRekrutacjaPromocja] [int]  IDENTITY(1,1) NOT NULL
  ,[Symbol] [varchar] (10) NOT NULL
  ,[Nazwa] [varchar] (200) NOT NULL
  ,[Dodal] [int]  NOT NULL DEFAULT (0)
  ,[DataDodania] [datetime]  NOT NULL DEFAULT (getdate())
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[RekrutacjaPromocja] WITH NOCHECK ADD 
CONSTRAINT [PK_RekrutacjaPromocja] PRIMARY KEY  CLUSTERED
(
  IDRekrutacjaPromocja
) ON [PRIMARY]


End
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_RekrutacjaPromocja] (
   [IDRekrutacjaPromocja] [int]  IDENTITY(1,1) NOT NULL
  ,[Symbol] [varchar] (10) NOT NULL
  ,[Nazwa] [varchar] (200) NOT NULL
  ,[Dodal] [int]  NOT NULL DEFAULT (0)
  ,[DataDodania] [datetime]  NOT NULL DEFAULT (getdate())
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[Tmp_RekrutacjaPromocja] WITH NOCHECK ADD 
CONSTRAINT [PK_RekrutacjaPromocja] PRIMARY KEY  CLUSTERED
(
  IDRekrutacjaPromocja
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_RekrutacjaPromocja ON

IF EXISTS (SELECT * FROM dbo.RekrutacjaPromocja)
EXEC('INSERT INTO dbo.Tmp_RekrutacjaPromocja (IDRekrutacjaPromocja, Symbol, Nazwa, Dodal, DataDodania, Zmodyfikowal, DataModyfikacji)
  SELECT IDRekrutacjaPromocja, Symbol, Nazwa, Dodal, DataDodania, Zmodyfikowal, DataModyfikacji FROM dbo.RekrutacjaPromocja WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_RekrutacjaPromocja OFF

DROP TABLE dbo.RekrutacjaPromocja

EXECUTE sp_rename N'dbo.Tmp_RekrutacjaPromocja', N'RekrutacjaPromocja', 'OBJECT'


END
ALTER TABLE [dbo].[Indeks] WITH NOCHECK ADD CONSTRAINT [FK_Indeks_RekrutacjaPromocja] FOREIGN KEY([RekrutacjaPromocjaID]) REFERENCES [dbo].[RekrutacjaPromocja] ([IDRekrutacjaPromocja])


IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OplataDodatkowa]') AND name = N'PK_OplataDodatkowa')
ALTER TABLE [dbo].[OplataDodatkowa] DROP CONSTRAINT [PK_OplataDodatkowa]

go



------------------------------------------------------------wdro2-----------------------------------------------

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OplataDodatkowa]') AND name = N'PK_OplataDodatkowa')
ALTER TABLE [dbo].[OplataDodatkowa] DROP CONSTRAINT [PK_OplataDodatkowa]

--------------------------------------
----------------OplataDodatkowa---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[OplataDodatkowa]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[OplataDodatkowa]
GO

CREATE TABLE [dbo].[OplataDodatkowa] (
   [IDOplataDodatkowa] [int]  IDENTITY(1,1) NOT NULL
  ,[Nazwa] [varchar] (1000) NOT NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[OplataDodatkowa] ADD  CONSTRAINT [PK_OplataDodatkowa] PRIMARY KEY CLUSTERED( IDOplataDodatkowa ) ON [PRIMARY]


go

--------------------------------------------------------------wdro3-----------------------------------------------------

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_HistoriaOplat_OplataDodatkowa]') AND parent_object_id = OBJECT_ID(N'[dbo].[HistoriaOplat]'))
ALTER TABLE [dbo].[HistoriaOplat] DROP CONSTRAINT [FK_HistoriaOplat_OplataDodatkowa]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[HistoriaOplat]') AND name = N'PK_HistoriaOplat')
ALTER TABLE [dbo].[HistoriaOplat] DROP CONSTRAINT [PK_HistoriaOplat]
--------------------------------------
----------------HistoriaOplat---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[HistoriaOplat]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[HistoriaOplat]
GO

CREATE TABLE [dbo].[HistoriaOplat] (
   [IDHistoriaOplat] [int]  IDENTITY(1,1) NOT NULL
  ,[IndeksID] [int]  NOT NULL
  ,[OplataDodatkowaID] [int]  NOT NULL
  ,[Lp] [int]  NOT NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO




ALTER TABLE [dbo].[HistoriaOplat] ADD  CONSTRAINT [PK_HistoriaOplat] PRIMARY KEY CLUSTERED( IDHistoriaOplat ) ON [PRIMARY]

ALTER TABLE [dbo].[HistoriaOplat] WITH NOCHECK ADD CONSTRAINT [FK_HistoriaOplat_OplataDodatkowa] FOREIGN KEY([OplataDodatkowaID]) REFERENCES [dbo].[OplataDodatkowa] ([IDOplataDodatkowa])


