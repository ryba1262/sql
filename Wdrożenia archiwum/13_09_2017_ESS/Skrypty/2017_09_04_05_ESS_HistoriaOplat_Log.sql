
-------- pami�taj coby ten skrypt odpala� na bazie log�w  --------


----------------HistoriaOplat---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_HistoriaOplat]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_HistoriaOplat] (
   [Log_HistoriaOplatID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDHistoriaOplat] [int] 
  ,[Org_IndeksID] [int] 
  ,[Org_OplataDodatkowaID] [int] 
  ,[Org_Lp] [int] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_HistoriaOplat] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_HistoriaOplat] PRIMARY KEY  CLUSTERED
(
   [Log_HistoriaOplatID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_HistoriaOplat] (
   [Log_HistoriaOplatID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDHistoriaOplat] [int] 
  ,[Org_IndeksID] [int] 
  ,[Org_OplataDodatkowaID] [int] 
  ,[Org_Lp] [int] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_HistoriaOplat ON

IF EXISTS (SELECT * FROM dbo.LOG_HistoriaOplat)
EXEC('INSERT INTO dbo.Tmp_LOG_HistoriaOplat (Log_HistoriaOplatID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDHistoriaOplat, Org_IndeksID, Org_OplataDodatkowaID, Org_Lp, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_HistoriaOplatID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDHistoriaOplat, Org_IndeksID, Org_OplataDodatkowaID, Org_Lp, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_HistoriaOplat WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_HistoriaOplat OFF

DROP TABLE dbo.LOG_HistoriaOplat

EXECUTE sp_rename N'dbo.Tmp_LOG_HistoriaOplat', N'LOG_HistoriaOplat', 'OBJECT'

ALTER TABLE [dbo].[LOG_HistoriaOplat] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_HistoriaOplat] PRIMARY KEY  CLUSTERED
(
   [Log_HistoriaOplatID]
) ON [PRIMARY]

END
GO

