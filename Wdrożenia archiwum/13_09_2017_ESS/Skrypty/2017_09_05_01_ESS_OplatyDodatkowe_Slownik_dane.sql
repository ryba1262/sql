


declare @IDDWOObiekt int

select @IDDWOObiekt = IDDWOObiekt from DWOObiekt where Sygnatura = 'dxBarButtonOplatyDodatkowe'

exec [dbo].[DWOUsunUprawnieniaDoObiektu] @IDDWOObiekt

delete from DWOObiekt where Sygnatura = 'dxBarButtonOplatyDodatkowe'

declare @IDDWOObiektNadrzedny int
select @IDDWOObiektNadrzedny = IDDWOObiekt from DWOObiekt where Sygnatura = 'dxBarSubItemSlowniki'

declare @UserID int
select @UserID = IDUzytkownik from Uzytkownik where Login = 'ktybinkowska'

INSERT INTO [DWOObiekt](ObiektNadrzednyID,DWOZestawUprawnienID,Sygnatura,Nazwa,Opis,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values(@IDDWOObiektNadrzedny,4,'dxBarButtonOplatyDodatkowe','Op�aty dodatkowe',NULL,coalesce(@UserID,0),getdate(),NULL,NULL)

select @IDDWOObiekt = scope_identity() 

exec [dbo].[DWODodajUprawnieniaDoObiektu] @IDDWOObiekt, 1

