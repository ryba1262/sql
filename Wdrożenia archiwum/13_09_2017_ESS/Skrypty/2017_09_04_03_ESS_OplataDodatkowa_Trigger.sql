DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------OplataDodatkowa-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''OplataDodatkowa_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER OplataDodatkowa_INSERT
')

EXEC('
CREATE TRIGGER OplataDodatkowa_INSERT ON ['+@BazaDanych+'].[dbo].[OplataDodatkowa] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''OplataDodatkowa'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_OplataDodatkowa]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDOplataDodatkowa]
  ,[Org_Nazwa]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDOplataDodatkowa
 ,ins.Nazwa
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''OplataDodatkowa_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER OplataDodatkowa_UPDATE
')

EXEC('
CREATE TRIGGER OplataDodatkowa_UPDATE ON ['+@BazaDanych+'].[dbo].[OplataDodatkowa] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''OplataDodatkowa'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_OplataDodatkowa]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDOplataDodatkowa]
  ,[Org_Nazwa]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDOplataDodatkowa
 ,ins.Nazwa
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''OplataDodatkowa_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER OplataDodatkowa_DELETE
')

EXEC('
CREATE TRIGGER OplataDodatkowa_DELETE ON ['+@BazaDanych+'].[dbo].[OplataDodatkowa] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''OplataDodatkowa'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_OplataDodatkowa]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDOplataDodatkowa]
  ,[Org_Nazwa]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDOplataDodatkowa
 ,del.Nazwa
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'OplataDodatkowa')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('OplataDodatkowa', 1, 0,GetDate())

