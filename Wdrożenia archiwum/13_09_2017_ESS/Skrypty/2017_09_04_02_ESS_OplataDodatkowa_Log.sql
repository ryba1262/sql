
-------- pami�taj coby ten skrypt odpala� na bazie log�w  --------


----------------OplataDodatkowa---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_OplataDodatkowa]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_OplataDodatkowa] (
   [Log_OplataDodatkowaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDOplataDodatkowa] [int] 
  ,[Org_Nazwa] [varchar] (1000)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_OplataDodatkowa] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_OplataDodatkowa] PRIMARY KEY  CLUSTERED
(
   [Log_OplataDodatkowaID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_OplataDodatkowa] (
   [Log_OplataDodatkowaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDOplataDodatkowa] [int] 
  ,[Org_Nazwa] [varchar] (1000)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_OplataDodatkowa ON

IF EXISTS (SELECT * FROM dbo.LOG_OplataDodatkowa)
EXEC('INSERT INTO dbo.Tmp_LOG_OplataDodatkowa (Log_OplataDodatkowaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDOplataDodatkowa, Org_Nazwa, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_OplataDodatkowaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDOplataDodatkowa, Org_Nazwa, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_OplataDodatkowa WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_OplataDodatkowa OFF

DROP TABLE dbo.LOG_OplataDodatkowa

EXECUTE sp_rename N'dbo.Tmp_LOG_OplataDodatkowa', N'LOG_OplataDodatkowa', 'OBJECT'

ALTER TABLE [dbo].[LOG_OplataDodatkowa] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_OplataDodatkowa] PRIMARY KEY  CLUSTERED
(
   [Log_OplataDodatkowaID]
) ON [PRIMARY]

END
GO

