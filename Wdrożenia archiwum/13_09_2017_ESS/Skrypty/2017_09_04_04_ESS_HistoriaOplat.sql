IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_HistoriaOplat_OplataDodatkowa]') AND parent_object_id = OBJECT_ID(N'[dbo].[HistoriaOplat]'))
ALTER TABLE [dbo].[HistoriaOplat] DROP CONSTRAINT [FK_HistoriaOplat_OplataDodatkowa]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[HistoriaOplat]') AND name = N'PK_HistoriaOplat')
ALTER TABLE [dbo].[HistoriaOplat] DROP CONSTRAINT [PK_HistoriaOplat]
--------------------------------------
----------------HistoriaOplat---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[HistoriaOplat]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[HistoriaOplat]
GO

CREATE TABLE [dbo].[HistoriaOplat] (
   [IDHistoriaOplat] [int]  IDENTITY(1,1) NOT NULL
  ,[IndeksID] [int]  NOT NULL
  ,[OplataDodatkowaID] [int]  NOT NULL
  ,[Lp] [int]  NOT NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO




ALTER TABLE [dbo].[HistoriaOplat] ADD  CONSTRAINT [PK_HistoriaOplat] PRIMARY KEY CLUSTERED( IDHistoriaOplat ) ON [PRIMARY]

ALTER TABLE [dbo].[HistoriaOplat] WITH NOCHECK ADD CONSTRAINT [FK_HistoriaOplat_OplataDodatkowa] FOREIGN KEY([OplataDodatkowaID]) REFERENCES [dbo].[OplataDodatkowa] ([IDOplataDodatkowa])


