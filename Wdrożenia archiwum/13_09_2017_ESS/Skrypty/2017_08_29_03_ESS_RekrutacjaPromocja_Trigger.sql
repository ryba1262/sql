DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''RekrutacjaPromocja_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER RekrutacjaPromocja_INSERT
')

EXEC('
CREATE TRIGGER RekrutacjaPromocja_INSERT ON ['+@BazaDanych+'].[dbo].[RekrutacjaPromocja] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''RekrutacjaPromocja'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_RekrutacjaPromocja]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDRekrutacjaPromocja]
  ,[Org_Symbol]
  ,[Org_Nazwa]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
  ,@ID
 ,ins.IDRekrutacjaPromocja
 ,ins.Symbol
 ,ins.Nazwa
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''RekrutacjaPromocja_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER RekrutacjaPromocja_UPDATE
')

EXEC('
CREATE TRIGGER RekrutacjaPromocja_UPDATE ON ['+@BazaDanych+'].[dbo].[RekrutacjaPromocja] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''RekrutacjaPromocja'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_RekrutacjaPromocja]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDRekrutacjaPromocja]
  ,[Org_Symbol]
  ,[Org_Nazwa]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
  ,@ID
 ,ins.IDRekrutacjaPromocja
 ,ins.Symbol
 ,ins.Nazwa
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''RekrutacjaPromocja_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER RekrutacjaPromocja_DELETE
')

EXEC('
CREATE TRIGGER RekrutacjaPromocja_DELETE ON ['+@BazaDanych+'].[dbo].[RekrutacjaPromocja] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''RekrutacjaPromocja'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_RekrutacjaPromocja]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDRekrutacjaPromocja]
  ,[Org_Symbol]
  ,[Org_Nazwa]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
  ,@ID
 ,del.IDRekrutacjaPromocja
 ,del.Symbol
 ,del.Nazwa
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')

if not exists (select * from LOG_LogowanaTabela where Tabela = 'RekrutacjaPromocja')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('RekrutacjaPromocja', 1, 0,GetDate())
GO

