--------------------------------------
----------------RekrutacjaPromocja---------------
--------------------------------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_RekrutacjaPromocja]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_RekrutacjaPromocja] (
   [Log_RekrutacjaPromocjaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDRekrutacjaPromocja] [int] 
  ,[Org_Symbol] [varchar] (10)
  ,[Org_Nazwa] [varchar] (200)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]
ALTER TABLE [dbo].[LOG_RekrutacjaPromocja] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_RekrutacjaPromocja] PRIMARY KEY  CLUSTERED
(
   [Log_RekrutacjaPromocjaID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_RekrutacjaPromocja] (
   [Log_RekrutacjaPromocjaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDRekrutacjaPromocja] [int] 
  ,[Org_Symbol] [varchar] (10)
  ,[Org_Nazwa] [varchar] (200)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]
SET IDENTITY_INSERT dbo.Tmp_LOG_RekrutacjaPromocja ON

IF EXISTS (SELECT * FROM dbo.LOG_RekrutacjaPromocja)
EXEC('INSERT INTO dbo.Tmp_LOG_RekrutacjaPromocja (Log_RekrutacjaPromocjaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDRekrutacjaPromocja, Org_Symbol, Org_Nazwa, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_RekrutacjaPromocjaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDRekrutacjaPromocja, Org_Symbol, Org_Nazwa, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_RekrutacjaPromocja WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_RekrutacjaPromocja OFF

DROP TABLE dbo.LOG_RekrutacjaPromocja

EXECUTE sp_rename N'dbo.Tmp_LOG_RekrutacjaPromocja', N'LOG_RekrutacjaPromocja', 'OBJECT'

ALTER TABLE [dbo].[LOG_RekrutacjaPromocja] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_RekrutacjaPromocja] PRIMARY KEY  CLUSTERED
(
   [Log_RekrutacjaPromocjaID]
) ON [PRIMARY]

END
GO


