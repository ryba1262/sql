--------------------------------------
----------------RekrutacjaPromocja---------------
-- Nale�y zweryfikowa� constrainty--
--------------------------------------
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Indeks_RekrutacjaPromocja]') AND parent_object_id = OBJECT_ID(N'[dbo].[Indeks]'))
ALTER TABLE [dbo].[Indeks] DROP CONSTRAINT [FK_Indeks_RekrutacjaPromocja]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[RekrutacjaPromocja]') AND name = N'PK_RekrutacjaPromocja')
ALTER TABLE [dbo].[RekrutacjaPromocja] DROP CONSTRAINT [PK_RekrutacjaPromocja]


if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[RekrutacjaPromocja]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[RekrutacjaPromocja] (
   [IDRekrutacjaPromocja] [int]  IDENTITY(1,1) NOT NULL
  ,[Symbol] [varchar] (10) NOT NULL
  ,[Nazwa] [varchar] (200) NOT NULL
  ,[Dodal] [int]  NOT NULL DEFAULT (0)
  ,[DataDodania] [datetime]  NOT NULL DEFAULT (getdate())
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[RekrutacjaPromocja] WITH NOCHECK ADD 
CONSTRAINT [PK_RekrutacjaPromocja] PRIMARY KEY  CLUSTERED
(
  IDRekrutacjaPromocja
) ON [PRIMARY]


End
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_RekrutacjaPromocja] (
   [IDRekrutacjaPromocja] [int]  IDENTITY(1,1) NOT NULL
  ,[Symbol] [varchar] (10) NOT NULL
  ,[Nazwa] [varchar] (200) NOT NULL
  ,[Dodal] [int]  NOT NULL DEFAULT (0)
  ,[DataDodania] [datetime]  NOT NULL DEFAULT (getdate())
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[Tmp_RekrutacjaPromocja] WITH NOCHECK ADD 
CONSTRAINT [PK_RekrutacjaPromocja] PRIMARY KEY  CLUSTERED
(
  IDRekrutacjaPromocja
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_RekrutacjaPromocja ON

IF EXISTS (SELECT * FROM dbo.RekrutacjaPromocja)
EXEC('INSERT INTO dbo.Tmp_RekrutacjaPromocja (IDRekrutacjaPromocja, Symbol, Nazwa, Dodal, DataDodania, Zmodyfikowal, DataModyfikacji)
  SELECT IDRekrutacjaPromocja, Symbol, Nazwa, Dodal, DataDodania, Zmodyfikowal, DataModyfikacji FROM dbo.RekrutacjaPromocja WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_RekrutacjaPromocja OFF

DROP TABLE dbo.RekrutacjaPromocja

EXECUTE sp_rename N'dbo.Tmp_RekrutacjaPromocja', N'RekrutacjaPromocja', 'OBJECT'


END
ALTER TABLE [dbo].[Indeks] WITH NOCHECK ADD CONSTRAINT [FK_Indeks_RekrutacjaPromocja] FOREIGN KEY([RekrutacjaPromocjaID]) REFERENCES [dbo].[RekrutacjaPromocja] ([IDRekrutacjaPromocja])


