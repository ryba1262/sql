IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OplataDodatkowa]') AND name = N'PK_OplataDodatkowa')
ALTER TABLE [dbo].[OplataDodatkowa] DROP CONSTRAINT [PK_OplataDodatkowa]

--------------------------------------
----------------OplataDodatkowa---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[OplataDodatkowa]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[OplataDodatkowa]
GO

CREATE TABLE [dbo].[OplataDodatkowa] (
   [IDOplataDodatkowa] [int]  IDENTITY(1,1) NOT NULL
  ,[Nazwa] [varchar] (1000) NOT NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[OplataDodatkowa] ADD  CONSTRAINT [PK_OplataDodatkowa] PRIMARY KEY CLUSTERED( IDOplataDodatkowa ) ON [PRIMARY]


