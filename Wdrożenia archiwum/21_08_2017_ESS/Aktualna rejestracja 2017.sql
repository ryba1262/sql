------------wdro1----------AktualnaRejestracja_2017/2018_zimowy------------
------------wdro2----------AktualnaRejestracja_2017/2018_letni------------




----------------------------------------------wdro1-----------------------------------------


INSERT INTO [dbo].[Drzewko]
          
           (IDDrzewko
		   ,[Nazwa]
           ,[CzyCzesne]
           ,[CzyEwidencja]
           ,[CzyRekrutacja]
           ,[CzyRaporty]
           ,[CzyDzialNauki]
           ,[NazwaPola]
           ,[NazwaPolaDB]
           ,[NazwaPolaSzablon]
           ,[Tabela]
           ,[KluczObcy]
           ,[Funkcja]
           ,[WarunekWhere]
           ,[Joiny]
           ,[TabelaObcaID]
           ,[Priorytet]
           ,[OrderByField])
     VALUES
           (
		   1704,
		   'Aktualna rejestracja 2017/2018 zimowy',
		   0,
		   1,
		   1,
		   1,
		   0,
		   'Aktualna rejestracja 2017/2018 zimowy',
		   NULL,
		   NULL,
		   'AktualnaRej20171',
		   NULL,
		   'case when AktualnaRej20171.IndeksID is null then ''Nie'' else ''Tak'' end',
		   NULL,
		   'left join AktRejNaSem20171 AktualnaRej20171 on AktualnaRej20171.IndeksID = IDIndeks',
		   NULL,
		   1,
		   NULL

		   )
GO


--------------------------------------------wdro2------------------------------------------


INSERT INTO [dbo].[Drzewko]
          
           (IDDrzewko
		   ,[Nazwa]
           ,[CzyCzesne]
           ,[CzyEwidencja]
           ,[CzyRekrutacja]
           ,[CzyRaporty]
           ,[CzyDzialNauki]
           ,[NazwaPola]
           ,[NazwaPolaDB]
           ,[NazwaPolaSzablon]
           ,[Tabela]
           ,[KluczObcy]
           ,[Funkcja]
           ,[WarunekWhere]
           ,[Joiny]
           ,[TabelaObcaID]
           ,[Priorytet]
           ,[OrderByField])
     
	 VALUES
           (
		   1705,
		   'Aktualna rejestracja 2017/2018 letni',
		   0,
		   1,
		   1,
		   1,
		   0,
		   'Aktualna rejestracja 2017/2018 letni',
		   NULL,
		   NULL,
		   'AktualnaRej20172',
		   NULL,
		   'case when AktualnaRej20172.IndeksID is null then ''Nie'' else ''Tak'' end',
		   NULL,
		   'left join AktRejNaSem20172 AktualnaRej20172 on AktualnaRej20172.IndeksID = IDIndeks',
		   NULL,
		   1,
		   NULL

		   )
GO










