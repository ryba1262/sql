/****** Object:  View [dbo].[AktRejNaSem20172]    Script Date: 2017-08-21 12:33:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[AktRejNaSem20171]
AS
SELECT     dbo.HistoriaStudenta.IndeksID
FROM         dbo.HistoriaStudenta INNER JOIN
                      dbo.Indeks ON dbo.HistoriaStudenta.IndeksID = dbo.Indeks.IDIndeks
WHERE     (dbo.HistoriaStudenta.ZdarzenieID = 1) AND (dbo.HistoriaStudenta.Anulowany = 0) AND 
                      (dbo.HistoriaStudenta.RokAkademicki * 10 + dbo.HistoriaStudenta.SemestrAkadID = 20171)
UNION
SELECT     IDIndeks
FROM         dbo.Indeks AS Indeks_1
WHERE     (RozpRokAkad * 10 + RozpSemestrID = 20171) AND (RozpOdSem > 0)


GO


/****** Object:  View [dbo].[AktRejNaSem20172]    Script Date: 2017-08-21 12:33:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[AktRejNaSem20172]
AS
SELECT     dbo.HistoriaStudenta.IndeksID
FROM         dbo.HistoriaStudenta INNER JOIN
                      dbo.Indeks ON dbo.HistoriaStudenta.IndeksID = dbo.Indeks.IDIndeks
WHERE     (dbo.HistoriaStudenta.ZdarzenieID = 1) AND (dbo.HistoriaStudenta.Anulowany = 0) AND 
                      (dbo.HistoriaStudenta.RokAkademicki * 10 + dbo.HistoriaStudenta.SemestrAkadID = 20172)
UNION
SELECT     IDIndeks
FROM         dbo.Indeks AS Indeks_1
WHERE     (RozpRokAkad * 10 + RozpSemestrID = 20172) AND (RozpOdSem > 0)


GO


