declare @UserID int, @GrupaID int, @PodgrupaID int, @RaportID int
declare @Grupa varchar(50), @GrupaOpis varchar(200)
declare @Podgrupa varchar(50), @PodgrupaOpis varchar(200)
declare @Raport varchar(50), @RaportOpis varchar(200)
declare @Kwerenda varchar(MAX), @KwerendaUpdate int, @ParametryUpdate int
declare @LiczbaBand int, @Timeout int

set @Grupa = 'DOS'
set @GrupaOpis = 'Raporty dotycz�ce DOS.'
set @Podgrupa = 'Pensum'
set @PodgrupaOpis = 'Pensum'
set @Raport = 'Dane osobowe'
set @RaportOpis = 'Dane osobowe.'
set @LiczbaBand = 5
set @Timeout = 60 * 10 

set @ParametryUpdate = 1 
set @KwerendaUpdate = 1
set @Kwerenda = 'set dateformat ymd

declare @RokAkademicki INT
set @RokAkademicki = :RokAkademicki 

SELECT distinct IDRPWniosek, Nazwisko + '' '' + coalesce(Imie,'''') + '' '' + coalesce(Imie2,'''') as [Wykladowca], 
       Tytul.Skrot as [Tytulnaukowy],  
       case when SemestrID=1 then ''Z'' else ''L'' end as SemestrTyp, 
Zatrudnienie, Pensum, 
        (select PRC_NUMER from PracownikSO 
        join Pracownik on Pracownik.SOID=PracownikSO.PRC_ID
        where IDPracownik=PracownikID) NumerPracownika,
AdresUlica, AdresNrDomu, AdresNrMieszkania, AdresMiasto, AdresKod, 
AdresKorUlica, AdresKorNrDomu, AdresKorNrMieszkania, AdresKorMiasto, AdresKorKod, 
TelefonDomowy, TelefonKom, TelefonPraca, Email, DataUrodzenia, MiejsceUrodzenia, PESEL, NIP,
Kierunek.Symbol PracKierunek, Sekcja.Symbol PracSekcja,
usnazwa as [Urzad Skarbowy - Nazwa], 
UsAdres as  [Urzad Skarbowy - Adres],
imieOjca as [Imie ojca], 
ImieMatki as [Imie matki]
FROM RPWniosek
INNER JOIN Pracownik ON PracownikID=IDPracownik
LEFT JOIN Tytul ON Pracownik.TytulID=Tytul.IDTytul
LEFT JOIN Kierunek ON Pracownik.KierunekID=IDKierunek
LEFT JOIN Sekcja ON Pracownik.SekcjaID=Sekcja.IDSekcja
WHERE RokAkad=:RokAkademicki
ORDER BY [Wykladowca]
'

set @UserID = coalesce((select IDUzytkownik from Uzytkownik where Login = 'ptlustochowicz'),0)
select @GrupaID = IDRAPRaportGrupa from RAPRaportGrupa where Nazwa = @Grupa and RAPRaportGrupaID is NULL

if coalesce(@GrupaID,0) = 0
begin
  insert into RAPRaportGrupa(RAPRaportGrupaID, Nazwa, Opis, Dodal, DataDodania)
    values(NULL, @Grupa, @GrupaOpis, @UserID, GetDate())
  select @GrupaID = scope_identity()
end

select @PodgrupaID = IDRAPRaportGrupa from RAPRaportGrupa where Nazwa = @Podgrupa and RAPRaportGrupaID = @GrupaID

if coalesce(@PodgrupaID,0) = 0
begin
  insert into RAPRaportGrupa(RAPRaportGrupaID, Nazwa, Opis, Dodal, DataDodania)
    values(@GrupaID, @Podgrupa, @PodgrupaOpis, @UserID, GetDate())
  select @PodgrupaID = scope_identity()
end

select @RaportID = IDRAPRaport from RAPRaport where Nazwa = @Raport and RAPRaportGrupaID = @PodgrupaID

if coalesce(@RaportID,0) = 0
begin
  insert into RAPRaport(RAPRaportGrupaID, Nazwa, Opis, Kwerenda, TimeOut, LiczbaBand, Dodal, DataDodania)
    values(@PodgrupaID, @Raport, @RaportOpis, @Kwerenda, @Timeout, @LiczbaBand, @UserID, GetDate())
  select @RaportID = scope_identity()
end
else if @KwerendaUpdate = 1
begin
  update RAPRaport set Kwerenda = @Kwerenda, TimeOut = @Timeout, LiczbaBand = @LiczbaBand, Zmodyfikowal = @UserID, DataModyfikacji = GetDate()
    where IDRAPRaport = @RaportID
end

if @ParametryUpdate = 1
begin
  delete from RAPRaportParametr where RAPRaportID = @RaportID
insert into RAPRaportParametr(RAPRaportID, LP, Etykieta, Nazwa, Typ, CzyWymagany, CzyWielokrotny, WartoscDomyslna, WartoscMin, WartoscMax, DWOWystepowanieID, WyborKwerenda, SygnaturaIdentyfikator, SygnaturyOpis, Dodal, DataDodania)
    values(@RaportID, 2, 'Rok akademicki', ':RokAkademicki', 'INT', 1, 0, NULL, NULL, NULL, NULL, 'select Distinct rokAkad as klucz, convert (varchar,rokakad )+ ''/''+convert (varchar,rokakad+1 ) as wartosc from semestrinfo order by rokakad desc','klucz', 'wartosc', @UserID, GetDate())

end
GO
