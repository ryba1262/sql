-----------wdro1-----------18_01_2018_WUParametry_kwestionariusz_magisterskie
-----------wdro2-----------18_01_2018_Raporty_Dyplom odpis - Olivetti grudzien 2017 Angielski



---------------------------------------------wdro1-----------------------------------------

SET IDENTITY_INSERT WUParametry ON

INSERT INTO dbo.WUParametry
			(IDWUParametry
		   ,[WUWydrukWersjaID]
           ,[Parametr1]
           ,[Parametr2]
           ,[Parametr3]
           ,[Parametr4]
           ,[Parametr5]
           ,[Parametr6]
           ,[Parametr7]
           ,[Parametr8]
           ,[Parametr9]
           ,[Parametr10])
     VALUES
           (
		   80
		   ,5
		   ,3
		   ,'K'
		   ,NULL
		   ,NULL
		   ,NULL
		   ,NULL
		   ,NULL
		   ,NULL
		   ,NULL
		   ,NULL
		   )


SET IDENTITY_INSERT WUParametry OFF

go


-----------------------------------------wdro2-------------------------------------------------


INSERT INTO [dbo].[Raporty]
           ([IDRaporty]
           ,[TypRaportu]
           ,[Nazwa]
           ,[NazwaSzablonu]
           ,[Modul]
           ,[EdytujCelWydania]
           ,[OpisCelWydania]
           ,[EdytujDataWaznosci]
           ,[OpisDataWaznosci]
           ,[CzyEwidencjonowac]
           ,[RaportDef]
           ,[WarunekParametr]
           ,[CzyWybor])
     VALUES
           (
		   10156
		   ,1
		   ,'Dyplom odpis - Olivetti grudzien 2017 Angielski'
		   ,'WSHE_ESDyplomOdpisOlivettiLipiec2013Ang'
		   ,';EgzaminDyp;'
		   ,0
		   ,'Za�wiadczenie wydaje si� w celu:'
		   ,0
		   ,'Data wa�no�ci'
		   ,1
		   ,'1027,1137,1230,1336,1347,1348,1401,1408,1409,1410,1448,1620,1621,1622,1623,1624'
		   ,'IDEgzaminDyplomowy = %s'
		   ,NULL

		   )
GO