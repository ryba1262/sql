-------wdro1---2018_11_08_01_ESS_WSPA_ELGZlecenie.sql
-------wdro2---2018_11_08_04_ESS_WSPA_ELGZleceniePoz.sql
-------wdro3---2018_11_08_07_ESS_WSPA_ELGZlecenieLog.sql
-------wdro4---2018_11_08_10_ESS_WSPA_ELGZlecenieRejestr.sql
-------wdro5---2018_11_08_13_ESS_WSPA_ELGKodBledu.sql
-------wdro6---2018_11_08_17_ESS_WSPA_ELGStatusZlecenia.sql
-------wdro7---2018_11_08_21_ESS_WSPA_ELGZlecenia_view.sql
-------wdro8---2018_11_08_22_ESS_WSPA_REJRejestrIndeksowILegitymacji_tab.sql
-------wdro9---2018_11_09_03_ESS_WSPA_SAK_SD_SetStatusZlecenia_proc.sql
-------wdro10---2018_11_09_04_ESS_WSPA_SAK_SD_SetDaneOsobowe_proc.sql




----------------------------------wdro1----------------------------------------------

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Tmp_ELGZl__DataZ__17DAF48E]') AND parent_object_id = OBJECT_ID(N'[dbo].[ELGZlecenie]'))
ALTER TABLE [dbo].[ELGZlecenie] DROP CONSTRAINT [DF__Tmp_ELGZl__DataZ__17DAF48E]

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Tmp_ELGZl__Druka__1AB76139]') AND parent_object_id = OBJECT_ID(N'[dbo].[ELGZlecenie]'))
ALTER TABLE [dbo].[ELGZlecenie] DROP CONSTRAINT [DF__Tmp_ELGZl__Druka__1AB76139]

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Tmp_ELGZl__Grupy__1C9FA9AB]') AND parent_object_id = OBJECT_ID(N'[dbo].[ELGZlecenie]'))
ALTER TABLE [dbo].[ELGZlecenie] DROP CONSTRAINT [DF__Tmp_ELGZl__Grupy__1C9FA9AB]

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Tmp_ELGZl__Liczb__1BAB8572]') AND parent_object_id = OBJECT_ID(N'[dbo].[ELGZlecenie]'))
ALTER TABLE [dbo].[ELGZlecenie] DROP CONSTRAINT [DF__Tmp_ELGZl__Liczb__1BAB8572]

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Tmp_ELGZl__Statu__18CF18C7]') AND parent_object_id = OBJECT_ID(N'[dbo].[ELGZlecenie]'))
ALTER TABLE [dbo].[ELGZlecenie] DROP CONSTRAINT [DF__Tmp_ELGZl__Statu__18CF18C7]

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Tmp_ELGZl__Statu__19C33D00]') AND parent_object_id = OBJECT_ID(N'[dbo].[ELGZlecenie]'))
ALTER TABLE [dbo].[ELGZlecenie] DROP CONSTRAINT [DF__Tmp_ELGZl__Statu__19C33D00]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ELGZlecenieLog_ELGZlecenie]') AND parent_object_id = OBJECT_ID(N'[dbo].[ELGZlecenieLog]'))
ALTER TABLE [dbo].[ELGZlecenieLog] DROP CONSTRAINT [FK_ELGZlecenieLog_ELGZlecenie]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ELGZleceniePoz_ELGZlecenie]') AND parent_object_id = OBJECT_ID(N'[dbo].[ELGZleceniePoz]'))
ALTER TABLE [dbo].[ELGZleceniePoz] DROP CONSTRAINT [FK_ELGZleceniePoz_ELGZlecenie]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ELGZlecenieRejestr_ELGZlecenie]') AND parent_object_id = OBJECT_ID(N'[dbo].[ELGZlecenieRejestr]'))
ALTER TABLE [dbo].[ELGZlecenieRejestr] DROP CONSTRAINT [FK_ELGZlecenieRejestr_ELGZlecenie]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ELGZlecenie]') AND name = N'PK_ELGZlecenie')
ALTER TABLE [dbo].[ELGZlecenie] DROP CONSTRAINT [PK_ELGZlecenie]


--------------------------------------
----------------ELGZlecenie---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[ELGZlecenie]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[ELGZlecenie]
GO

CREATE TABLE [dbo].[ELGZlecenie] (
   [IDELGZlecenie] [int]  IDENTITY(1,1) NOT NULL
  ,[Nazwa] [varchar] (255) NOT NULL
  ,[Zlecenie] [xml]  NOT NULL
  ,[DataZlecenia] [datetime]  NOT NULL DEFAULT (getdate())
  ,[Status] [int]  NULL DEFAULT ((0))
  ,[DataRozpoczecia] [datetime]  NULL
  ,[DataZakonczenia] [datetime]  NULL
  ,[StatusCKM] [varchar] (6) NOT NULL DEFAULT ('')
  ,[Drukarka] [int]  NOT NULL DEFAULT ((0))
  ,[LiczbaRekordow] [int]  NULL DEFAULT ((0))
  ,[Grupy] [varchar] (50) NULL DEFAULT ('')
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO






ALTER TABLE [dbo].[ELGZlecenie] ADD  CONSTRAINT [PK_ELGZlecenie] PRIMARY KEY CLUSTERED( IDELGZlecenie ) ON [PRIMARY]

ALTER TABLE [dbo].[ELGZlecenie] ADD  CONSTRAINT [DF__Tmp_ELGZl__Statu__19C33D00]  DEFAULT ('') FOR [StatusCKM]

ALTER TABLE [dbo].[ELGZlecenie] ADD  CONSTRAINT [DF__Tmp_ELGZl__Statu__18CF18C7]  DEFAULT ((0)) FOR [Status]

ALTER TABLE [dbo].[ELGZlecenie] ADD  CONSTRAINT [DF__Tmp_ELGZl__Liczb__1BAB8572]  DEFAULT ((0)) FOR [LiczbaRekordow]

ALTER TABLE [dbo].[ELGZlecenie] ADD  CONSTRAINT [DF__Tmp_ELGZl__Grupy__1C9FA9AB]  DEFAULT ('') FOR [Grupy]

ALTER TABLE [dbo].[ELGZlecenie] ADD  CONSTRAINT [DF__Tmp_ELGZl__Druka__1AB76139]  DEFAULT ((0)) FOR [Drukarka]

ALTER TABLE [dbo].[ELGZlecenie] ADD  CONSTRAINT [DF__Tmp_ELGZl__DataZ__17DAF48E]  DEFAULT (getdate()) FOR [DataZlecenia]

go


-------------------------------------------------------wdro2---------------------------------


IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ELGZleceniePoz_ELGKodBledu]') AND parent_object_id = OBJECT_ID(N'[dbo].[ELGZleceniePoz]'))
ALTER TABLE [dbo].[ELGZleceniePoz] DROP CONSTRAINT [FK_ELGZleceniePoz_ELGKodBledu]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ELGZleceniePoz_ELGZlecenie]') AND parent_object_id = OBJECT_ID(N'[dbo].[ELGZleceniePoz]'))
ALTER TABLE [dbo].[ELGZleceniePoz] DROP CONSTRAINT [FK_ELGZleceniePoz_ELGZlecenie]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ELGZleceniePoz_Indeks]') AND parent_object_id = OBJECT_ID(N'[dbo].[ELGZleceniePoz]'))
ALTER TABLE [dbo].[ELGZleceniePoz] DROP CONSTRAINT [FK_ELGZleceniePoz_Indeks]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ELGZleceniePoz_Student]') AND parent_object_id = OBJECT_ID(N'[dbo].[ELGZleceniePoz]'))
ALTER TABLE [dbo].[ELGZleceniePoz] DROP CONSTRAINT [FK_ELGZleceniePoz_Student]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ELGZleceniePoz]') AND name = N'PK_ELGZleceniePoz')
ALTER TABLE [dbo].[ELGZleceniePoz] DROP CONSTRAINT [PK_ELGZleceniePoz]

--------------------------------------
----------------ELGZleceniePoz---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[ELGZleceniePoz]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[ELGZleceniePoz]
GO

CREATE TABLE [dbo].[ELGZleceniePoz] (
   [IDELGZleceniePoz] [int]  IDENTITY(1,1) NOT NULL
  ,[ELGZlecenieID] [int]  NOT NULL
  ,[StudentID] [int]  NOT NULL
  ,[IndeksID] [int]  NOT NULL
  ,[Rekord] [xml]  NOT NULL
  ,[TrescBledu] [varchar] (max) NULL
  ,[ELGKodBleduID] [int]  NULL
  ,[Edycja] [char] (1) NOT NULL
  ,[DataWaznosci] [datetime]  NOT NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO



ALTER TABLE [dbo].[ELGZleceniePoz] ADD  CONSTRAINT [PK_ELGZleceniePoz] PRIMARY KEY CLUSTERED( IDELGZleceniePoz ) ON [PRIMARY]

ALTER TABLE [dbo].[ELGZleceniePoz] WITH NOCHECK ADD CONSTRAINT [FK_ELGZleceniePoz_Student] FOREIGN KEY([StudentID]) REFERENCES [dbo].[Student] ([IDStudent])

ALTER TABLE [dbo].[ELGZleceniePoz] WITH NOCHECK ADD CONSTRAINT [FK_ELGZleceniePoz_Indeks] FOREIGN KEY([IndeksID]) REFERENCES [dbo].[Indeks] ([IDIndeks])

ALTER TABLE [dbo].[ELGZleceniePoz] WITH NOCHECK ADD CONSTRAINT [FK_ELGZleceniePoz_ELGZlecenie] FOREIGN KEY([ELGZlecenieID]) REFERENCES [dbo].[ELGZlecenie] ([IDELGZlecenie])
go

-------------------------------------------------wdro3------------------------------------------

IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__ELGZleceni__Data__2DFF3FD7]') AND parent_object_id = OBJECT_ID(N'[dbo].[ELGZlecenieLog]'))
ALTER TABLE [dbo].[ELGZlecenieLog] DROP CONSTRAINT [DF__ELGZleceni__Data__2DFF3FD7]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ELGZlecenieLog_ELGZlecenie]') AND parent_object_id = OBJECT_ID(N'[dbo].[ELGZlecenieLog]'))
ALTER TABLE [dbo].[ELGZlecenieLog] DROP CONSTRAINT [FK_ELGZlecenieLog_ELGZlecenie]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ELGZlecenieLog]') AND name = N'PK_ELGZlecenieLog')
ALTER TABLE [dbo].[ELGZlecenieLog] DROP CONSTRAINT [PK_ELGZlecenieLog]

--------------------------------------
----------------ELGZlecenieLog---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[ELGZlecenieLog]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[ELGZlecenieLog]
GO

CREATE TABLE [dbo].[ELGZlecenieLog] (
   [IDELGZlecenieLog] [int]  IDENTITY(1,1) NOT NULL
  ,[ELGZlecenieID] [int]  NOT NULL
  ,[StatusCKM] [varchar] (6) NOT NULL
  ,[Status] [int]  NOT NULL
  ,[Data] [datetime]  NOT NULL DEFAULT (getdate())
  ,[KtoZmienia] [varchar] (255) NOT NULL
  ,[InfoBledy] [varchar] (max) NULL
) ON [PRIMARY]

GO




ALTER TABLE [dbo].[ELGZlecenieLog] ADD  CONSTRAINT [PK_ELGZlecenieLog] PRIMARY KEY CLUSTERED( IDELGZlecenieLog ) ON [PRIMARY]

ALTER TABLE [dbo].[ELGZlecenieLog] WITH NOCHECK ADD CONSTRAINT [FK_ELGZlecenieLog_ELGZlecenie] FOREIGN KEY([ELGZlecenieID]) REFERENCES [dbo].[ELGZlecenie] ([IDELGZlecenie])

ALTER TABLE [dbo].[ELGZlecenieLog] ADD  CONSTRAINT [DF__ELGZleceni__Data__2DFF3FD7]  DEFAULT (getdate()) FOR [Data]

go
-----------------------------------------wdro4------------------------------------------

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ELGZlecenieRejestr_ELGZlecenie]') AND parent_object_id = OBJECT_ID(N'[dbo].[ELGZlecenieRejestr]'))
ALTER TABLE [dbo].[ELGZlecenieRejestr] DROP CONSTRAINT [FK_ELGZlecenieRejestr_ELGZlecenie]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ELGZlecenieRejestr_REJRejestrIndeksowILegitymacji]') AND parent_object_id = OBJECT_ID(N'[dbo].[ELGZlecenieRejestr]'))
ALTER TABLE [dbo].[ELGZlecenieRejestr] DROP CONSTRAINT [FK_ELGZlecenieRejestr_REJRejestrIndeksowILegitymacji]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ELGZlecenieRejestr_Student]') AND parent_object_id = OBJECT_ID(N'[dbo].[ELGZlecenieRejestr]'))
ALTER TABLE [dbo].[ELGZlecenieRejestr] DROP CONSTRAINT [FK_ELGZlecenieRejestr_Student]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ELGZlecenieRejestr]') AND name = N'PK_ELGZlecenieRejestr')
ALTER TABLE [dbo].[ELGZlecenieRejestr] DROP CONSTRAINT [PK_ELGZlecenieRejestr]

--------------------------------------
----------------ELGZlecenieRejestr---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[ELGZlecenieRejestr]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[ELGZlecenieRejestr]
GO

CREATE TABLE [dbo].[ELGZlecenieRejestr] (
   [IDELGZlecenieRejestr] [int]  IDENTITY(1,1) NOT NULL
  ,[ELGZlecenieID] [int]  NOT NULL
  ,[StudentID] [int]  NOT NULL
  ,[REJRejestrIndeksowILegitymacjiID] [int]  NULL
  ,[DataPersonalizacji] [datetime]  NOT NULL
  ,[Edycja] [char] (1) NOT NULL
  ,[NrSeryjny] [varchar] (16) NOT NULL
  ,[NrSeryjnyMifare] [varchar] (16) NOT NULL
  ,[DataWaznosci] [datetime]  NOT NULL
) ON [PRIMARY]

GO




ALTER TABLE [dbo].[ELGZlecenieRejestr] ADD  CONSTRAINT [PK_ELGZlecenieRejestr] PRIMARY KEY CLUSTERED( IDELGZlecenieRejestr ) ON [PRIMARY]

ALTER TABLE [dbo].[ELGZlecenieRejestr] WITH NOCHECK ADD CONSTRAINT [FK_ELGZlecenieRejestr_Student] FOREIGN KEY([StudentID]) REFERENCES [dbo].[Student] ([IDStudent])

ALTER TABLE [dbo].[ELGZlecenieRejestr] WITH NOCHECK ADD CONSTRAINT [FK_ELGZlecenieRejestr_REJRejestrIndeksowILegitymacji] FOREIGN KEY([REJRejestrIndeksowILegitymacjiID]) REFERENCES [dbo].[REJRejestrIndeksowILegitymacji] ([IDREJRejestrIndeksowILegitymacji])

ALTER TABLE [dbo].[ELGZlecenieRejestr] WITH NOCHECK ADD CONSTRAINT [FK_ELGZlecenieRejestr_ELGZlecenie] FOREIGN KEY([ELGZlecenieID]) REFERENCES [dbo].[ELGZlecenie] ([IDELGZlecenie])

go
-----------------------------------wdro5--------------------------------------

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ELGZleceniePoz_ELGKodBledu]') AND parent_object_id = OBJECT_ID(N'[dbo].[ELGZleceniePoz]'))
ALTER TABLE [dbo].[ELGZleceniePoz] DROP CONSTRAINT [FK_ELGZleceniePoz_ELGKodBledu]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ELGKodBledu]') AND name = N'PK_ELGKodBledu')
ALTER TABLE [dbo].[ELGKodBledu] DROP CONSTRAINT [PK_ELGKodBledu]

--------------------------------------
----------------ELGKodBledu---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[ELGKodBledu]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[ELGKodBledu]
GO

CREATE TABLE [dbo].[ELGKodBledu] (
   [IDELGKodBledu] [int]  IDENTITY(1,1) NOT NULL
  ,[Kod] [int]  NOT NULL
  ,[Opis] [varchar] (max) NOT NULL
) ON [PRIMARY]

GO



ALTER TABLE [dbo].[ELGKodBledu] ADD  CONSTRAINT [PK_ELGKodBledu] PRIMARY KEY CLUSTERED( IDELGKodBledu ) ON [PRIMARY]

ALTER TABLE [dbo].[ELGZleceniePoz] WITH NOCHECK ADD CONSTRAINT [FK_ELGZleceniePoz_ELGKodBledu] FOREIGN KEY([ELGKodBleduID]) REFERENCES [dbo].[ELGKodBledu] ([IDELGKodBledu])

go
-------------------------------------wdro6----------------------------------

--------------------------------------
----------------ELGStatusZlecenia---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[ELGStatusZlecenia]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[ELGStatusZlecenia]
GO

CREATE TABLE [dbo].[ELGStatusZlecenia] (
   [IDELGStatusZlecenia] [int]  IDENTITY(1,1) NOT NULL
  ,[StatusCKM] [varchar] (6) NOT NULL
  ,[Opis] [varchar] (255) NULL
) ON [PRIMARY]

GO


----------------------------------------------wdro7-----------------------------------------

-- ******************* -- SD_ListaWydzialow -- ******************* --

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[SD_ListaWydzialow]'))
DROP VIEW [dbo].[SD_ListaWydzialow]
GO





CREATE VIEW [dbo].[SD_ListaWydzialow]
AS
 select CAST(IDWydzial as VARCHAR(7)) as wydzial, left(Nazwa, 100) as opisWydzialu from Wydzial



GO


-- ******************* -- SD_ListaProgramow -- ******************* --

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[SD_ListaProgramow]'))
DROP VIEW [dbo].[SD_ListaProgramow]
GO


CREATE VIEW SD_ListaProgramow
AS
  select null as wydzial, null as kierunek, null as program, null as opisProgramu


GO


-- ******************* -- SD_ListaKierunkow -- ******************* --

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[SD_ListaKierunkow]'))
DROP VIEW [dbo].[SD_ListaKierunkow]
GO



CREATE VIEW [dbo].[SD_ListaKierunkow]
AS
	SELECT WydzialID AS wydzial, IDKierunek AS kierunek, LEFT(Nazwa, 100) AS opisKierunku FROM Kierunek




GO


-- ******************* -- ELGKodBledu -- ******************* --

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[SD_KodyBledow]'))
DROP VIEW [dbo].[SD_KodyBledow]
GO


CREATE VIEW SD_KodyBledow
AS
	SELECT kod, opis FROM ELGKodBledu


GO


-- ******************* -- SD_DaneOsobowe -- ******************* --

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[SD_DaneOsobowe]'))
DROP VIEW [dbo].[SD_DaneOsobowe]
GO






-- =============================================
-- Author:		Mateusz Kordowiak
-- Create date: 2018-10-19
-- Description:	Widok danych osobowych studenta dla systemu OPTICAMP.
-- UWAGA, zdjęcie jest wymagane.
-- =============================================
CREATE VIEW [dbo].[SD_DaneOsobowe]
AS
	select 
		IDStudent as idOsoby,
		's' as grupaOsobowa,
		i.NrIndeksu as album,
		k.WydzialID as wydzial,
		k.IDKierunek as kierunek,
		null as program,
		1 as semestr,
		Zdjecie as foto,
		s.Imie as pierwszeImie,
		s.DrugieImie as drugieImie,
		s.Nazwisko as nazwisko,
		mur.Nazwa as urodzeniaMiejscowosc,
		s.DataUrodzenia as urodzeniaData,
		s.ImieOjca as imieOjca,
		s.ImieMatki as imieMatki,
		case
			when s.AdresSPPolska = 0 then
				s.AdresSPObcokrajowcy
			else
				s.AdresSPUlica
		end as adresUlica,
		s.AdresSPNrDomu as adresNrDomu,
		s.AdresSPNrMieszkania as adresNrLokalu,
		s.AdresSPKod as adresKodPocztowy,
		null as adresPoczta,
		case
			when s.AdresSPPolska = 0 then
				panstwo.Nazwa
			else
				msp.Nazwa
		end as adresMiejscowosc,
		s.TelefonKom as nrTelefonu,
		s.Email1 as eMail,
		null as tytul,
		null as stanowisko,
		case
			when ((s.PESEL = '' or s.PESEL IS null) and s.ObywatelstwoID <> 1 )
				then
					RIGHT(LEFT(CONVERT(VARCHAR, s.DataUrodzenia, 120), 4), 2) +
					RIGHT(LEFT(CONVERT(VARCHAR, s.DataUrodzenia, 120), 7), 2) +
					RIGHT(LEFT(CONVERT(VARCHAR, s.DataUrodzenia, 120), 10), 2)+
					'00000'
			else s.PESEL
		end as PESEL,
		case
			when s.ObywatelstwoID = 1 THEN 0
			else 1
			END as obcokrajowiec,
		0 as biletMZK,
		null as kodGrupyDanych,
		i.DataRozpoczeciaStudiow as dataRozpoczecia,
		null as login
	from Student s
	join Indeks i on s.IDStudent = i.StudentID
	join StatusStudenta ss on i.StatusStudentaID = ss.IDStatusStudenta
	join Studia st on i.StudiaID = st.IDStudia
	join Kierunek k on st.Kierunek1ID = k.IDKierunek
	left join Miejscowosc as mur on s.MiejsceUrodzeniaID = mur.IDMiejscowosc
	left join Miejscowosc as msp on s.AdresSPMiejscowoscID = msp.IDMiejscowosc
	left join Panstwo as panstwo on s.AdresSPPanstwoID = panstwo.IDPanstwo
	where ss.Symbol = 's'
	and i.NrIndeksu > 1
	and s.Zdjecie is not null




GO


------------------------------------------wdro8-----------------------------------------------

--------------------------------------
----------------REJRejestrIndeksowILegitymacji---------------
-- Należy zweryfikować constrainty--
--------------------------------------
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ELGZlecenieRejestr_REJRejestrIndeksowILegitymacji]') AND parent_object_id = OBJECT_ID(N'[dbo].[ELGZlecenieRejestr]'))
ALTER TABLE [dbo].[ELGZlecenieRejestr] DROP CONSTRAINT [FK_ELGZlecenieRejestr_REJRejestrIndeksowILegitymacji]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[REJRejestrIndeksowILegitymacji]') AND name = N'PK_REJRejestrIndeksowILegitymacji')
ALTER TABLE [dbo].[REJRejestrIndeksowILegitymacji] DROP CONSTRAINT [PK_REJRejestrIndeksowILegitymacji]


if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[REJRejestrIndeksowILegitymacji]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[REJRejestrIndeksowILegitymacji] (
   [IDREJRejestrIndeksowILegitymacji] [int]  IDENTITY(1,1) NOT NULL
  ,[IndeksID] [int]  NOT NULL
  ,[StudiaID] [int]  NOT NULL
  ,[ELSKluczTransportowyID] [int]  NULL
  ,[DataWydaniaDokumentu] [datetime]  NULL
  ,[DataWaznosciDokumentu] [datetime]  NULL
  ,[OryginalDuplikat] [varchar] (1) NULL
  ,[Wersja] [varchar] (1) NULL
  ,[Edycja] [varchar] (1) NULL
  ,[CzyLegitymacja] [bit]  NOT NULL
  ,[CzyAnulowany] [bit]  NOT NULL DEFAULT ((0))
  ,[ImieStudenta] [varchar] (50) NULL
  ,[DrugieImieStudenta] [varchar] (50) NULL
  ,[NazwiskoStudenta] [varchar] (50) NULL
  ,[Imie1Karta] [varchar] (24) NULL
  ,[Imie2Karta] [varchar] (24) NULL
  ,[Nazwisko1Karta] [varchar] (28) NULL
  ,[Nazwisko2Karta] [varchar] (28) NULL
  ,[AdresUlica] [varchar] (50) NULL
  ,[AdresKodMiejscowosc] [varchar] (50) NULL
  ,[NazwaUczelni] [varchar] (100) NULL
  ,[WydanePrzez] [varchar] (100) NULL DEFAULT 'ESS'
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[REJRejestrIndeksowILegitymacji] WITH NOCHECK ADD 
CONSTRAINT [PK_REJRejestrIndeksowILegitymacji] PRIMARY KEY  CLUSTERED
(
  IDREJRejestrIndeksowILegitymacji
) ON [PRIMARY]


End
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_REJRejestrIndeksowILegitymacji] (
   [IDREJRejestrIndeksowILegitymacji] [int]  IDENTITY(1,1) NOT NULL
  ,[IndeksID] [int]  NOT NULL
  ,[StudiaID] [int]  NOT NULL
  ,[ELSKluczTransportowyID] [int]  NULL
  ,[DataWydaniaDokumentu] [datetime]  NULL
  ,[DataWaznosciDokumentu] [datetime]  NULL
  ,[OryginalDuplikat] [varchar] (1) NULL
  ,[Wersja] [varchar] (1) NULL
  ,[Edycja] [varchar] (1) NULL
  ,[CzyLegitymacja] [bit]  NOT NULL
  ,[CzyAnulowany] [bit]  NOT NULL DEFAULT ((0))
  ,[ImieStudenta] [varchar] (50) NULL
  ,[DrugieImieStudenta] [varchar] (50) NULL
  ,[NazwiskoStudenta] [varchar] (50) NULL
  ,[Imie1Karta] [varchar] (24) NULL
  ,[Imie2Karta] [varchar] (24) NULL
  ,[Nazwisko1Karta] [varchar] (28) NULL
  ,[Nazwisko2Karta] [varchar] (28) NULL
  ,[AdresUlica] [varchar] (50) NULL
  ,[AdresKodMiejscowosc] [varchar] (50) NULL
  ,[NazwaUczelni] [varchar] (100) NULL
  ,[WydanePrzez] [varchar] (100) NULL DEFAULT 'ESS'
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[Tmp_REJRejestrIndeksowILegitymacji] WITH NOCHECK ADD 
CONSTRAINT [PK_REJRejestrIndeksowILegitymacji] PRIMARY KEY  CLUSTERED
(
  IDREJRejestrIndeksowILegitymacji
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_REJRejestrIndeksowILegitymacji ON

IF EXISTS (SELECT * FROM dbo.REJRejestrIndeksowILegitymacji)
EXEC('INSERT INTO dbo.Tmp_REJRejestrIndeksowILegitymacji (IDREJRejestrIndeksowILegitymacji, IndeksID, StudiaID, ELSKluczTransportowyID, DataWydaniaDokumentu, DataWaznosciDokumentu, OryginalDuplikat, Wersja, Edycja, CzyLegitymacja, CzyAnulowany, ImieStudenta, DrugieImieStudenta, NazwiskoStudenta, Imie1Karta, Imie2Karta, Nazwisko1Karta, Nazwisko2Karta, AdresUlica, AdresKodMiejscowosc, NazwaUczelni, Dodal, DataDodania, Zmodyfikowal, DataModyfikacji)
  SELECT IDREJRejestrIndeksowILegitymacji, IndeksID, StudiaID, ELSKluczTransportowyID, DataWydaniaDokumentu, DataWaznosciDokumentu, OryginalDuplikat, Wersja, Edycja, CzyLegitymacja, CzyAnulowany, ImieStudenta, DrugieImieStudenta, NazwiskoStudenta, Imie1Karta, Imie2Karta, Nazwisko1Karta, Nazwisko2Karta, AdresUlica, AdresKodMiejscowosc, NazwaUczelni, Dodal, DataDodania, Zmodyfikowal, DataModyfikacji FROM dbo.REJRejestrIndeksowILegitymacji WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_REJRejestrIndeksowILegitymacji OFF

DROP TABLE dbo.REJRejestrIndeksowILegitymacji

EXECUTE sp_rename N'dbo.Tmp_REJRejestrIndeksowILegitymacji', N'REJRejestrIndeksowILegitymacji', 'OBJECT'


END
ALTER TABLE [dbo].[REJRejestrIndeksowILegitymacji] WITH NOCHECK ADD CONSTRAINT [FK_REJRejestrIndeksowILegitymacji_Studia] FOREIGN KEY([StudiaID]) REFERENCES [dbo].[Studia] ([IDStudia])

ALTER TABLE [dbo].[REJRejestrIndeksowILegitymacji] WITH NOCHECK ADD CONSTRAINT [FK_REJRejestrIndeksowILegitymacji_Indeks] FOREIGN KEY([IndeksID]) REFERENCES [dbo].[Indeks] ([IDIndeks])

ALTER TABLE [dbo].[REJRejestrIndeksowILegitymacji] WITH NOCHECK ADD CONSTRAINT [FK_REJRejestrIndeksowILegitymacji_ELSKluczTransportowy] FOREIGN KEY([ELSKluczTransportowyID]) REFERENCES [dbo].[ELSKluczTransportowy] ([IDELSKluczTransportowy])

ALTER TABLE [dbo].[ELGZlecenieRejestr] WITH NOCHECK ADD CONSTRAINT [FK_ELGZlecenieRejestr_REJRejestrIndeksowILegitymacji] FOREIGN KEY([REJRejestrIndeksowILegitymacjiID]) REFERENCES [dbo].[REJRejestrIndeksowILegitymacji] ([IDREJRejestrIndeksowILegitymacji])

go
----------------------------------------wdro9------------------------------------------


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SD_SetStatusZlecenia]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SD_SetStatusZlecenia]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[SD_SetStatusZlecenia]
	@idZlecenia int,
	@statusZlec varchar(6),
	@ktoZmienia varchar(255),
	@data datetime,
	@log varchar(max),
	@status int OUTPUT
as
begin
	INSERT INTO ELGZlecenieLog(ELGZlecenieID, StatusCKM, Status, Data, KtoZmienia,InfoBledy)
	VALUES(@idZlecenia, @statusZlec, -1, @data, @ktoZmienia,@log);

	UPDATE ELGZlecenie SET Status=@status, statusCKM=@statusZlec
	where IDELGZlecenie = @idZlecenia

    if(@log <>'')
    begin
		declare @xml_string xml = cast(@log as xml)

		declare @xml xml, @synchronizacja_xml xml, @TrescBledu varchar(max)
		
		declare @Table table(Rekord xml)
		insert into @Table
		SELECT T.c.query('.')  
		FROM   @xml_string.nodes('/zlecenie/rekord') T(c)  


		declare Kursor cursor for
		select Rekord from @Table

		open Kursor
		fetch next from Kursor into @xml

	    while ( @@fetch_status = 0 )
		begin

			SELECT @synchronizacja_xml = T.c.query('.') 
			FROM   @xml.nodes('/rekord/synchronizacja') T(c)  
     
			SELECT @TrescBledu = T.C.value('(synchronizacja)[1]', 'varchar(max)')
			FROM @xml.nodes('rekord') T(C)
     
			SET @xml.modify('delete /rekord/synchronizacja')
     
			update ELGZleceniePoz
			set TrescBledu = @TrescBledu
			where cast(Rekord as varchar(max)) like cast(@xml AS varchar(max))

			fetch next from Kursor into @xml
		end
		close Kursor
		deallocate Kursor
    end
    

	SET @status = -1;
end;



GO


----------------------------------------------wdro10------------------------------------------


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SD_SetDaneOsobowe]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SD_SetDaneOsobowe]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SD_SetDaneOsobowe]
	@idOsoby int,
	@idZlecenia varchar(6),
	@dataPersonalizacji datetime,
	@edycja char(1),
	@nrSeryjny varchar(16),
	@nrSeryjnyMifare varchar(16),
	@dataWaznosci datetime,
	@status int OUTPUT
as
begin
	SET @status = -1;
	insert into ELGZlecenieRejestr( StudentID,
		ELGZlecenieID,
		DataPersonalizacji,
		Edycja,
		NrSeryjny,
		nrSeryjnyMifare,
		DataWaznosci)
	VALUES (@idOsoby,@idZlecenia,@dataPersonalizacji,@edycja,@nrSeryjny, @nrSeryjnyMifare,@dataWaznosci);
	
end;



GO


