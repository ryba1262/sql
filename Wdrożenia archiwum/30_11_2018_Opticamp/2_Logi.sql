-------wdro1---2018_11_08_02_ESS_WSPA_ELGZlecenie_Log.sql
-------wdro2---2018_11_08_05_ESS_WSPA_ELGZleceniePoz_Log.sql
-------wdro3---2018_11_08_08_ESS_WSPA_ELGZlecenieLog_Log.sql
-------wdro4---2018_11_08_11_ESS_WSPA_ELGZlecenieRejestr_Log.sql
-------wdro5---2018_11_08_14_ESS_WSPA_ELGKodBledu_Log.sql
-------wdro6---2018_11_08_18_ESS_WSPA_ELGStatusZlecenia_Log.sql
-------wdro7---2018_11_08_23_ESS_WSPA_REJRejestrIndeksowILegitymacji_Log.sql


------------------------------------wdro1--------------------------------------


-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------ELGZlecenie---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_ELGZlecenie]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_ELGZlecenie] (
   [Log_ELGZlecenieID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDELGZlecenie] [int] 
  ,[Org_Nazwa] [varchar] (255)
  ,[Org_Zlecenie] [xml] 
  ,[Org_DataZlecenia] [datetime] 
  ,[Org_Status] [int] 
  ,[Org_DataRozpoczecia] [datetime] 
  ,[Org_DataZakonczenia] [datetime] 
  ,[Org_StatusCKM] [varchar] (6)
  ,[Org_Drukarka] [int] 
  ,[Org_LiczbaRekordow] [int] 
  ,[Org_Grupy] [varchar] (50)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_ELGZlecenie] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_ELGZlecenie] PRIMARY KEY  CLUSTERED
(
   [Log_ELGZlecenieID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_ELGZlecenie] (
   [Log_ELGZlecenieID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDELGZlecenie] [int] 
  ,[Org_Nazwa] [varchar] (255)
  ,[Org_Zlecenie] [xml] 
  ,[Org_DataZlecenia] [datetime] 
  ,[Org_Status] [int] 
  ,[Org_DataRozpoczecia] [datetime] 
  ,[Org_DataZakonczenia] [datetime] 
  ,[Org_StatusCKM] [varchar] (6)
  ,[Org_Drukarka] [int] 
  ,[Org_LiczbaRekordow] [int] 
  ,[Org_Grupy] [varchar] (50)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_ELGZlecenie ON

IF EXISTS (SELECT * FROM dbo.LOG_ELGZlecenie)
EXEC('INSERT INTO dbo.Tmp_LOG_ELGZlecenie (Log_ELGZlecenieID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDELGZlecenie, Org_Nazwa, Org_Zlecenie, Org_DataZlecenia, Org_Status, Org_DataRozpoczecia, Org_DataZakonczenia, Org_StatusCKM, Org_Drukarka, Org_LiczbaRekordow, Org_Grupy, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_ELGZlecenieID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDELGZlecenie, Org_Nazwa, Org_Zlecenie, Org_DataZlecenia, Org_Status, Org_DataRozpoczecia, Org_DataZakonczenia, Org_StatusCKM, Org_Drukarka, Org_LiczbaRekordow, Org_Grupy, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_ELGZlecenie WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_ELGZlecenie OFF

DROP TABLE dbo.LOG_ELGZlecenie

EXECUTE sp_rename N'dbo.Tmp_LOG_ELGZlecenie', N'LOG_ELGZlecenie', 'OBJECT'

ALTER TABLE [dbo].[LOG_ELGZlecenie] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_ELGZlecenie] PRIMARY KEY  CLUSTERED
(
   [Log_ELGZlecenieID]
) ON [PRIMARY]

END
GO

------------------------------------wdro2--------------------------------------


-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------ELGZleceniePoz---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_ELGZleceniePoz]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_ELGZleceniePoz] (
   [Log_ELGZleceniePozID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDELGZleceniePoz] [int] 
  ,[Org_ELGZlecenieID] [int] 
  ,[Org_StudentID] [int] 
  ,[Org_IndeksID] [int] 
  ,[Org_Rekord] [xml] 
  ,[Org_TrescBledu] [varchar] (max)
  ,[Org_ELGKodBleduID] [int] 
  ,[Org_Edycja] [char] (1)
  ,[Org_DataWaznosci] [datetime] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_ELGZleceniePoz] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_ELGZleceniePoz] PRIMARY KEY  CLUSTERED
(
   [Log_ELGZleceniePozID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_ELGZleceniePoz] (
   [Log_ELGZleceniePozID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDELGZleceniePoz] [int] 
  ,[Org_ELGZlecenieID] [int] 
  ,[Org_StudentID] [int] 
  ,[Org_IndeksID] [int] 
  ,[Org_Rekord] [xml] 
  ,[Org_TrescBledu] [varchar] (max)
  ,[Org_ELGKodBleduID] [int] 
  ,[Org_Edycja] [char] (1)
  ,[Org_DataWaznosci] [datetime] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_ELGZleceniePoz ON

IF EXISTS (SELECT * FROM dbo.LOG_ELGZleceniePoz)
EXEC('INSERT INTO dbo.Tmp_LOG_ELGZleceniePoz (Log_ELGZleceniePozID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDELGZleceniePoz, Org_ELGZlecenieID, Org_StudentID, Org_IndeksID, Org_Rekord, Org_TrescBledu, Org_ELGKodBleduID, Org_Edycja, Org_DataWaznosci, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_ELGZleceniePozID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDELGZleceniePoz, Org_ELGZlecenieID, Org_StudentID, Org_IndeksID, Org_Rekord, Org_TrescBledu, Org_ELGKodBleduID, Org_Edycja, Org_DataWaznosci, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_ELGZleceniePoz WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_ELGZleceniePoz OFF

DROP TABLE dbo.LOG_ELGZleceniePoz

EXECUTE sp_rename N'dbo.Tmp_LOG_ELGZleceniePoz', N'LOG_ELGZleceniePoz', 'OBJECT'

ALTER TABLE [dbo].[LOG_ELGZleceniePoz] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_ELGZleceniePoz] PRIMARY KEY  CLUSTERED
(
   [Log_ELGZleceniePozID]
) ON [PRIMARY]

END
GO

----------------------------------------------------wdro3----------------------------------------------


-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------ELGZlecenieLog---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_ELGZlecenieLog]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_ELGZlecenieLog] (
   [Log_ELGZlecenieLogID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDELGZlecenieLog] [int] 
  ,[Org_ELGZlecenieID] [int] 
  ,[Org_StatusCKM] [varchar] (6)
  ,[Org_Status] [int] 
  ,[Org_Data] [datetime] 
  ,[Org_KtoZmienia] [varchar] (255)
  ,[Org_InfoBledy] [varchar] (max)
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_ELGZlecenieLog] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_ELGZlecenieLog] PRIMARY KEY  CLUSTERED
(
   [Log_ELGZlecenieLogID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_ELGZlecenieLog] (
   [Log_ELGZlecenieLogID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDELGZlecenieLog] [int] 
  ,[Org_ELGZlecenieID] [int] 
  ,[Org_StatusCKM] [varchar] (6)
  ,[Org_Status] [int] 
  ,[Org_Data] [datetime] 
  ,[Org_KtoZmienia] [varchar] (255)
  ,[Org_InfoBledy] [varchar] (max)
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_ELGZlecenieLog ON

IF EXISTS (SELECT * FROM dbo.LOG_ELGZlecenieLog)
EXEC('INSERT INTO dbo.Tmp_LOG_ELGZlecenieLog (Log_ELGZlecenieLogID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDELGZlecenieLog, Org_ELGZlecenieID, Org_StatusCKM, Org_Status, Org_Data, Org_KtoZmienia, Org_InfoBledy)
  SELECT Log_ELGZlecenieLogID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDELGZlecenieLog, Org_ELGZlecenieID, Org_StatusCKM, Org_Status, Org_Data, Org_KtoZmienia, Org_InfoBledy FROM dbo.Log_ELGZlecenieLog WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_ELGZlecenieLog OFF

DROP TABLE dbo.LOG_ELGZlecenieLog

EXECUTE sp_rename N'dbo.Tmp_LOG_ELGZlecenieLog', N'LOG_ELGZlecenieLog', 'OBJECT'

ALTER TABLE [dbo].[LOG_ELGZlecenieLog] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_ELGZlecenieLog] PRIMARY KEY  CLUSTERED
(
   [Log_ELGZlecenieLogID]
) ON [PRIMARY]

END
GO

-------------------------------------------------wdro4---------------------------------------


-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------ELGZlecenieRejestr---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_ELGZlecenieRejestr]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_ELGZlecenieRejestr] (
   [Log_ELGZlecenieRejestrID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDELGZlecenieRejestr] [int] 
  ,[Org_ELGZlecenieID] [int] 
  ,[Org_StudentID] [int] 
  ,[Org_REJRejestrIndeksowILegitymacjiID] [int] 
  ,[Org_DataPersonalizacji] [datetime] 
  ,[Org_Edycja] [char] (1)
  ,[Org_NrSeryjny] [varchar] (16)
  ,[Org_NrSeryjnyMifare] [varchar] (16)
  ,[Org_DataWaznosci] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_ELGZlecenieRejestr] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_ELGZlecenieRejestr] PRIMARY KEY  CLUSTERED
(
   [Log_ELGZlecenieRejestrID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_ELGZlecenieRejestr] (
   [Log_ELGZlecenieRejestrID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDELGZlecenieRejestr] [int] 
  ,[Org_ELGZlecenieID] [int] 
  ,[Org_StudentID] [int] 
  ,[Org_REJRejestrIndeksowILegitymacjiID] [int] 
  ,[Org_DataPersonalizacji] [datetime] 
  ,[Org_Edycja] [char] (1)
  ,[Org_NrSeryjny] [varchar] (16)
  ,[Org_NrSeryjnyMifare] [varchar] (16)
  ,[Org_DataWaznosci] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_ELGZlecenieRejestr ON

IF EXISTS (SELECT * FROM dbo.LOG_ELGZlecenieRejestr)
EXEC('INSERT INTO dbo.Tmp_LOG_ELGZlecenieRejestr (Log_ELGZlecenieRejestrID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDELGZlecenieRejestr, Org_ELGZlecenieID, Org_StudentID, Org_REJRejestrIndeksowILegitymacjiID, Org_DataPersonalizacji, Org_Edycja, Org_NrSeryjny, Org_NrSeryjnyMifare, Org_DataWaznosci)
  SELECT Log_ELGZlecenieRejestrID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDELGZlecenieRejestr, Org_ELGZlecenieID, Org_StudentID, Org_REJRejestrIndeksowILegitymacjiID, Org_DataPersonalizacji, Org_Edycja, Org_NrSeryjny, Org_NrSeryjnyMifare, Org_DataWaznosci FROM dbo.Log_ELGZlecenieRejestr WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_ELGZlecenieRejestr OFF

DROP TABLE dbo.LOG_ELGZlecenieRejestr

EXECUTE sp_rename N'dbo.Tmp_LOG_ELGZlecenieRejestr', N'LOG_ELGZlecenieRejestr', 'OBJECT'

ALTER TABLE [dbo].[LOG_ELGZlecenieRejestr] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_ELGZlecenieRejestr] PRIMARY KEY  CLUSTERED
(
   [Log_ELGZlecenieRejestrID]
) ON [PRIMARY]

END
GO

---------------------------------------------wdro5---------------------------------------------


-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------ELGKodBledu---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_ELGKodBledu]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_ELGKodBledu] (
   [Log_ELGKodBleduID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDELGKodBledu] [int] 
  ,[Org_Kod] [int] 
  ,[Org_Opis] [varchar] (max)
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_ELGKodBledu] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_ELGKodBledu] PRIMARY KEY  CLUSTERED
(
   [Log_ELGKodBleduID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_ELGKodBledu] (
   [Log_ELGKodBleduID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDELGKodBledu] [int] 
  ,[Org_Kod] [int] 
  ,[Org_Opis] [varchar] (max)
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_ELGKodBledu ON

IF EXISTS (SELECT * FROM dbo.LOG_ELGKodBledu)
EXEC('INSERT INTO dbo.Tmp_LOG_ELGKodBledu (Log_ELGKodBleduID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDELGKodBledu, Org_Kod, Org_Opis)
  SELECT Log_ELGKodBleduID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDELGKodBledu, Org_Kod, Org_Opis FROM dbo.Log_ELGKodBledu WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_ELGKodBledu OFF

DROP TABLE dbo.LOG_ELGKodBledu

EXECUTE sp_rename N'dbo.Tmp_LOG_ELGKodBledu', N'LOG_ELGKodBledu', 'OBJECT'

ALTER TABLE [dbo].[LOG_ELGKodBledu] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_ELGKodBledu] PRIMARY KEY  CLUSTERED
(
   [Log_ELGKodBleduID]
) ON [PRIMARY]

END
GO

---------------------------------------------wdro6------------------------------------------


-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------ELGStatusZlecenia---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_ELGStatusZlecenia]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_ELGStatusZlecenia] (
   [Log_ELGStatusZleceniaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDELGStatusZlecenia] [int] 
  ,[Org_StatusCKM] [varchar] (6)
  ,[Org_Opis] [varchar] (255)
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_ELGStatusZlecenia] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_ELGStatusZlecenia] PRIMARY KEY  CLUSTERED
(
   [Log_ELGStatusZleceniaID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_ELGStatusZlecenia] (
   [Log_ELGStatusZleceniaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDELGStatusZlecenia] [int] 
  ,[Org_StatusCKM] [varchar] (6)
  ,[Org_Opis] [varchar] (255)
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_ELGStatusZlecenia ON

IF EXISTS (SELECT * FROM dbo.LOG_ELGStatusZlecenia)
EXEC('INSERT INTO dbo.Tmp_LOG_ELGStatusZlecenia (Log_ELGStatusZleceniaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDELGStatusZlecenia, Org_StatusCKM, Org_Opis)
  SELECT Log_ELGStatusZleceniaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDELGStatusZlecenia, Org_StatusCKM, Org_Opis FROM dbo.Log_ELGStatusZlecenia WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_ELGStatusZlecenia OFF

DROP TABLE dbo.LOG_ELGStatusZlecenia

EXECUTE sp_rename N'dbo.Tmp_LOG_ELGStatusZlecenia', N'LOG_ELGStatusZlecenia', 'OBJECT'

ALTER TABLE [dbo].[LOG_ELGStatusZlecenia] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_ELGStatusZlecenia] PRIMARY KEY  CLUSTERED
(
   [Log_ELGStatusZleceniaID]
) ON [PRIMARY]

END
GO

----------------------------------------wdro7-------------------------------------

--------------------------------------
----------------REJRejestrIndeksowILegitymacji---------------
--------------------------------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_REJRejestrIndeksowILegitymacji]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_REJRejestrIndeksowILegitymacji] (
   [Log_REJRejestrIndeksowILegitymacjiID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDREJRejestrIndeksowILegitymacji] [int] 
  ,[Org_IndeksID] [int] 
  ,[Org_StudiaID] [int] 
  ,[Org_ELSKluczTransportowyID] [int] 
  ,[Org_DataWydaniaDokumentu] [datetime] 
  ,[Org_DataWaznosciDokumentu] [datetime] 
  ,[Org_OryginalDuplikat] [varchar] (1)
  ,[Org_Wersja] [varchar] (1)
  ,[Org_Edycja] [varchar] (1)
  ,[Org_CzyLegitymacja] [bit] 
  ,[Org_CzyAnulowany] [bit] 
  ,[Org_ImieStudenta] [varchar] (50)
  ,[Org_DrugieImieStudenta] [varchar] (50)
  ,[Org_NazwiskoStudenta] [varchar] (50)
  ,[Org_Imie1Karta] [varchar] (24)
  ,[Org_Imie2Karta] [varchar] (24)
  ,[Org_Nazwisko1Karta] [varchar] (28)
  ,[Org_Nazwisko2Karta] [varchar] (28)
  ,[Org_AdresUlica] [varchar] (50)
  ,[Org_AdresKodMiejscowosc] [varchar] (50)
  ,[Org_NazwaUczelni] [varchar] (100)
  ,[Org_WydanePrzez] [varchar] (100)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]
ALTER TABLE [dbo].[LOG_REJRejestrIndeksowILegitymacji] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_REJRejestrIndeksowILegitymacji] PRIMARY KEY  CLUSTERED
(
   [Log_REJRejestrIndeksowILegitymacjiID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_REJRejestrIndeksowILegitymacji] (
   [Log_REJRejestrIndeksowILegitymacjiID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDREJRejestrIndeksowILegitymacji] [int] 
  ,[Org_IndeksID] [int] 
  ,[Org_StudiaID] [int] 
  ,[Org_ELSKluczTransportowyID] [int] 
  ,[Org_DataWydaniaDokumentu] [datetime] 
  ,[Org_DataWaznosciDokumentu] [datetime] 
  ,[Org_OryginalDuplikat] [varchar] (1)
  ,[Org_Wersja] [varchar] (1)
  ,[Org_Edycja] [varchar] (1)
  ,[Org_CzyLegitymacja] [bit] 
  ,[Org_CzyAnulowany] [bit] 
  ,[Org_ImieStudenta] [varchar] (50)
  ,[Org_DrugieImieStudenta] [varchar] (50)
  ,[Org_NazwiskoStudenta] [varchar] (50)
  ,[Org_Imie1Karta] [varchar] (24)
  ,[Org_Imie2Karta] [varchar] (24)
  ,[Org_Nazwisko1Karta] [varchar] (28)
  ,[Org_Nazwisko2Karta] [varchar] (28)
  ,[Org_AdresUlica] [varchar] (50)
  ,[Org_AdresKodMiejscowosc] [varchar] (50)
  ,[Org_NazwaUczelni] [varchar] (100)
  ,[Org_WydanePrzez] [varchar] (100)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]
SET IDENTITY_INSERT dbo.Tmp_LOG_REJRejestrIndeksowILegitymacji ON

IF EXISTS (SELECT * FROM dbo.LOG_REJRejestrIndeksowILegitymacji)
EXEC('INSERT INTO dbo.Tmp_LOG_REJRejestrIndeksowILegitymacji (Log_REJRejestrIndeksowILegitymacjiID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDREJRejestrIndeksowILegitymacji, Org_IndeksID, Org_StudiaID, Org_ELSKluczTransportowyID, Org_DataWydaniaDokumentu, Org_DataWaznosciDokumentu, Org_OryginalDuplikat, Org_Wersja, Org_Edycja, Org_CzyLegitymacja, Org_CzyAnulowany, Org_ImieStudenta, Org_DrugieImieStudenta, Org_NazwiskoStudenta, Org_Imie1Karta, Org_Imie2Karta, Org_Nazwisko1Karta, Org_Nazwisko2Karta, Org_AdresUlica, Org_AdresKodMiejscowosc, Org_NazwaUczelni, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_REJRejestrIndeksowILegitymacjiID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDREJRejestrIndeksowILegitymacji, Org_IndeksID, Org_StudiaID, Org_ELSKluczTransportowyID, Org_DataWydaniaDokumentu, Org_DataWaznosciDokumentu, Org_OryginalDuplikat, Org_Wersja, Org_Edycja, Org_CzyLegitymacja, Org_CzyAnulowany, Org_ImieStudenta, Org_DrugieImieStudenta, Org_NazwiskoStudenta, Org_Imie1Karta, Org_Imie2Karta, Org_Nazwisko1Karta, Org_Nazwisko2Karta, Org_AdresUlica, Org_AdresKodMiejscowosc, Org_NazwaUczelni, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_REJRejestrIndeksowILegitymacji WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_REJRejestrIndeksowILegitymacji OFF

DROP TABLE dbo.LOG_REJRejestrIndeksowILegitymacji

EXECUTE sp_rename N'dbo.Tmp_LOG_REJRejestrIndeksowILegitymacji', N'LOG_REJRejestrIndeksowILegitymacji', 'OBJECT'

ALTER TABLE [dbo].[LOG_REJRejestrIndeksowILegitymacji] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_REJRejestrIndeksowILegitymacji] PRIMARY KEY  CLUSTERED
(
   [Log_REJRejestrIndeksowILegitymacjiID]
) ON [PRIMARY]

END
GO


