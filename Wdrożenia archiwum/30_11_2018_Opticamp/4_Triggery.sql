----------------wdro1-------------2018_11_08_03_ESS_WSPA_ELGZlecenie_Trigger
----------------wdro2-------------2018_11_08_06_ESS_WSPA_ELGZleceniePoz_Trigger
----------------wdro3-------------2018_11_08_09_ESS_WSPA_ELGZlecenieLog_Trigger.sql
----------------wdro4-------------2018_11_08_12_ESS_WSPA_ELGZlecenieRejestr_Trigger.sql
----------------wdro5-------------2018_11_08_15_ESS_WSPA_ELGKodBledu_Trigger.sql
----------------wdro6-------------2018_11_08_19_ESS_WSPA_ELGStatusZlecenia_Trigger.sql
----------------wdro7-------------2018_11_08_24_ESS_WSPA_REJRejestrIndeksowILegitymacji_Trigger.sql


-------------------------------------------------------wdro1---------------------------------------------------






DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------ELGZlecenie-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ELGZlecenie_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER ELGZlecenie_INSERT
')

EXEC('
CREATE TRIGGER ELGZlecenie_INSERT ON ['+@BazaDanych+'].[dbo].[ELGZlecenie] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ELGZlecenie'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ELGZlecenie]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDELGZlecenie]
  ,[Org_Nazwa]
  ,[Org_Zlecenie]
  ,[Org_DataZlecenia]
  ,[Org_Status]
  ,[Org_DataRozpoczecia]
  ,[Org_DataZakonczenia]
  ,[Org_StatusCKM]
  ,[Org_Drukarka]
  ,[Org_LiczbaRekordow]
  ,[Org_Grupy]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDELGZlecenie
 ,ins.Nazwa
 ,ins.Zlecenie
 ,ins.DataZlecenia
 ,ins.Status
 ,ins.DataRozpoczecia
 ,ins.DataZakonczenia
 ,ins.StatusCKM
 ,ins.Drukarka
 ,ins.LiczbaRekordow
 ,ins.Grupy
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ELGZlecenie_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER ELGZlecenie_UPDATE
')

EXEC('
CREATE TRIGGER ELGZlecenie_UPDATE ON ['+@BazaDanych+'].[dbo].[ELGZlecenie] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ELGZlecenie'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ELGZlecenie]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDELGZlecenie]
  ,[Org_Nazwa]
  ,[Org_Zlecenie]
  ,[Org_DataZlecenia]
  ,[Org_Status]
  ,[Org_DataRozpoczecia]
  ,[Org_DataZakonczenia]
  ,[Org_StatusCKM]
  ,[Org_Drukarka]
  ,[Org_LiczbaRekordow]
  ,[Org_Grupy]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDELGZlecenie
 ,ins.Nazwa
 ,ins.Zlecenie
 ,ins.DataZlecenia
 ,ins.Status
 ,ins.DataRozpoczecia
 ,ins.DataZakonczenia
 ,ins.StatusCKM
 ,ins.Drukarka
 ,ins.LiczbaRekordow
 ,ins.Grupy
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ELGZlecenie_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER ELGZlecenie_DELETE
')

EXEC('
CREATE TRIGGER ELGZlecenie_DELETE ON ['+@BazaDanych+'].[dbo].[ELGZlecenie] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ELGZlecenie'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ELGZlecenie]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDELGZlecenie]
  ,[Org_Nazwa]
  ,[Org_Zlecenie]
  ,[Org_DataZlecenia]
  ,[Org_Status]
  ,[Org_DataRozpoczecia]
  ,[Org_DataZakonczenia]
  ,[Org_StatusCKM]
  ,[Org_Drukarka]
  ,[Org_LiczbaRekordow]
  ,[Org_Grupy]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDELGZlecenie
 ,del.Nazwa
 ,del.Zlecenie
 ,del.DataZlecenia
 ,del.Status
 ,del.DataRozpoczecia
 ,del.DataZakonczenia
 ,del.StatusCKM
 ,del.Drukarka
 ,del.LiczbaRekordow
 ,del.Grupy
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'ELGZlecenie')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('ELGZlecenie', 1, 0,GetDate())

go

-------------------------------------------------------wdro2---------------------------------------------------




DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------ELGZleceniePoz-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ELGZleceniePoz_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER ELGZleceniePoz_INSERT
')

EXEC('
CREATE TRIGGER ELGZleceniePoz_INSERT ON ['+@BazaDanych+'].[dbo].[ELGZleceniePoz] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ELGZleceniePoz'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ELGZleceniePoz]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDELGZleceniePoz]
  ,[Org_ELGZlecenieID]
  ,[Org_StudentID]
  ,[Org_IndeksID]
  ,[Org_Rekord]
  ,[Org_TrescBledu]
  ,[Org_ELGKodBleduID]
  ,[Org_Edycja]
  ,[Org_DataWaznosci]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDELGZleceniePoz
 ,ins.ELGZlecenieID
 ,ins.StudentID
 ,ins.IndeksID
 ,ins.Rekord
 ,ins.TrescBledu
 ,ins.ELGKodBleduID
 ,ins.Edycja
 ,ins.DataWaznosci
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ELGZleceniePoz_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER ELGZleceniePoz_UPDATE
')

EXEC('
CREATE TRIGGER ELGZleceniePoz_UPDATE ON ['+@BazaDanych+'].[dbo].[ELGZleceniePoz] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ELGZleceniePoz'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ELGZleceniePoz]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDELGZleceniePoz]
  ,[Org_ELGZlecenieID]
  ,[Org_StudentID]
  ,[Org_IndeksID]
  ,[Org_Rekord]
  ,[Org_TrescBledu]
  ,[Org_ELGKodBleduID]
  ,[Org_Edycja]
  ,[Org_DataWaznosci]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDELGZleceniePoz
 ,ins.ELGZlecenieID
 ,ins.StudentID
 ,ins.IndeksID
 ,ins.Rekord
 ,ins.TrescBledu
 ,ins.ELGKodBleduID
 ,ins.Edycja
 ,ins.DataWaznosci
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ELGZleceniePoz_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER ELGZleceniePoz_DELETE
')

EXEC('
CREATE TRIGGER ELGZleceniePoz_DELETE ON ['+@BazaDanych+'].[dbo].[ELGZleceniePoz] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ELGZleceniePoz'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ELGZleceniePoz]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDELGZleceniePoz]
  ,[Org_ELGZlecenieID]
  ,[Org_StudentID]
  ,[Org_IndeksID]
  ,[Org_Rekord]
  ,[Org_TrescBledu]
  ,[Org_ELGKodBleduID]
  ,[Org_Edycja]
  ,[Org_DataWaznosci]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDELGZleceniePoz
 ,del.ELGZlecenieID
 ,del.StudentID
 ,del.IndeksID
 ,del.Rekord
 ,del.TrescBledu
 ,del.ELGKodBleduID
 ,del.Edycja
 ,del.DataWaznosci
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'ELGZleceniePoz')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('ELGZleceniePoz', 1, 0,GetDate())


go
-------------------------------------------------------wdro3---------------------------------------------------


DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------ELGZlecenieLog-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ELGZlecenieLog_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER ELGZlecenieLog_INSERT
')

EXEC('
CREATE TRIGGER ELGZlecenieLog_INSERT ON ['+@BazaDanych+'].[dbo].[ELGZlecenieLog] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ELGZlecenieLog'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ELGZlecenieLog]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDELGZlecenieLog]
  ,[Org_ELGZlecenieID]
  ,[Org_StatusCKM]
  ,[Org_Status]
  ,[Org_Data]
  ,[Org_KtoZmienia]
  ,[Org_InfoBledy]
)
SELECT
1
	,@ID
 ,ins.IDELGZlecenieLog
 ,ins.ELGZlecenieID
 ,ins.StatusCKM
 ,ins.Status
 ,ins.Data
 ,ins.KtoZmienia
 ,ins.InfoBledy
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ELGZlecenieLog_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER ELGZlecenieLog_UPDATE
')

EXEC('
CREATE TRIGGER ELGZlecenieLog_UPDATE ON ['+@BazaDanych+'].[dbo].[ELGZlecenieLog] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ELGZlecenieLog'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ELGZlecenieLog]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDELGZlecenieLog]
  ,[Org_ELGZlecenieID]
  ,[Org_StatusCKM]
  ,[Org_Status]
  ,[Org_Data]
  ,[Org_KtoZmienia]
  ,[Org_InfoBledy]
)
SELECT
2
	,@ID
 ,ins.IDELGZlecenieLog
 ,ins.ELGZlecenieID
 ,ins.StatusCKM
 ,ins.Status
 ,ins.Data
 ,ins.KtoZmienia
 ,ins.InfoBledy
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ELGZlecenieLog_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER ELGZlecenieLog_DELETE
')

EXEC('
CREATE TRIGGER ELGZlecenieLog_DELETE ON ['+@BazaDanych+'].[dbo].[ELGZlecenieLog] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ELGZlecenieLog'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ELGZlecenieLog]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDELGZlecenieLog]
  ,[Org_ELGZlecenieID]
  ,[Org_StatusCKM]
  ,[Org_Status]
  ,[Org_Data]
  ,[Org_KtoZmienia]
  ,[Org_InfoBledy]
)
SELECT
3
	,@ID
 ,del.IDELGZlecenieLog
 ,del.ELGZlecenieID
 ,del.StatusCKM
 ,del.Status
 ,del.Data
 ,del.KtoZmienia
 ,del.InfoBledy
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'ELGZlecenieLog')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('ELGZlecenieLog', 1, 0,GetDate())


go
---------------------------------------------------wdro4-----------------------------------------------------------------

DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------ELGZlecenieRejestr-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ELGZlecenieRejestr_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER ELGZlecenieRejestr_INSERT
')

EXEC('
CREATE TRIGGER ELGZlecenieRejestr_INSERT ON ['+@BazaDanych+'].[dbo].[ELGZlecenieRejestr] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ELGZlecenieRejestr'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ELGZlecenieRejestr]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDELGZlecenieRejestr]
  ,[Org_ELGZlecenieID]
  ,[Org_StudentID]
  ,[Org_REJRejestrIndeksowILegitymacjiID]
  ,[Org_DataPersonalizacji]
  ,[Org_Edycja]
  ,[Org_NrSeryjny]
  ,[Org_NrSeryjnyMifare]
  ,[Org_DataWaznosci]
)
SELECT
1
	,@ID
 ,ins.IDELGZlecenieRejestr
 ,ins.ELGZlecenieID
 ,ins.StudentID
 ,ins.REJRejestrIndeksowILegitymacjiID
 ,ins.DataPersonalizacji
 ,ins.Edycja
 ,ins.NrSeryjny
 ,ins.NrSeryjnyMifare
 ,ins.DataWaznosci
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ELGZlecenieRejestr_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER ELGZlecenieRejestr_UPDATE
')

EXEC('
CREATE TRIGGER ELGZlecenieRejestr_UPDATE ON ['+@BazaDanych+'].[dbo].[ELGZlecenieRejestr] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ELGZlecenieRejestr'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ELGZlecenieRejestr]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDELGZlecenieRejestr]
  ,[Org_ELGZlecenieID]
  ,[Org_StudentID]
  ,[Org_REJRejestrIndeksowILegitymacjiID]
  ,[Org_DataPersonalizacji]
  ,[Org_Edycja]
  ,[Org_NrSeryjny]
  ,[Org_NrSeryjnyMifare]
  ,[Org_DataWaznosci]
)
SELECT
2
	,@ID
 ,ins.IDELGZlecenieRejestr
 ,ins.ELGZlecenieID
 ,ins.StudentID
 ,ins.REJRejestrIndeksowILegitymacjiID
 ,ins.DataPersonalizacji
 ,ins.Edycja
 ,ins.NrSeryjny
 ,ins.NrSeryjnyMifare
 ,ins.DataWaznosci
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ELGZlecenieRejestr_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER ELGZlecenieRejestr_DELETE
')

EXEC('
CREATE TRIGGER ELGZlecenieRejestr_DELETE ON ['+@BazaDanych+'].[dbo].[ELGZlecenieRejestr] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ELGZlecenieRejestr'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ELGZlecenieRejestr]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDELGZlecenieRejestr]
  ,[Org_ELGZlecenieID]
  ,[Org_StudentID]
  ,[Org_REJRejestrIndeksowILegitymacjiID]
  ,[Org_DataPersonalizacji]
  ,[Org_Edycja]
  ,[Org_NrSeryjny]
  ,[Org_NrSeryjnyMifare]
  ,[Org_DataWaznosci]
)
SELECT
3
	,@ID
 ,del.IDELGZlecenieRejestr
 ,del.ELGZlecenieID
 ,del.StudentID
 ,del.REJRejestrIndeksowILegitymacjiID
 ,del.DataPersonalizacji
 ,del.Edycja
 ,del.NrSeryjny
 ,del.NrSeryjnyMifare
 ,del.DataWaznosci
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'ELGZlecenieRejestr')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('ELGZlecenieRejestr', 1, 0,GetDate())


go
--------------------------------------------------wdro5---------------------------------------------------------

DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------ELGKodBledu-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ELGKodBledu_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER ELGKodBledu_INSERT
')

EXEC('
CREATE TRIGGER ELGKodBledu_INSERT ON ['+@BazaDanych+'].[dbo].[ELGKodBledu] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ELGKodBledu'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ELGKodBledu]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDELGKodBledu]
  ,[Org_Kod]
  ,[Org_Opis]
)
SELECT
1
	,@ID
 ,ins.IDELGKodBledu
 ,ins.Kod
 ,ins.Opis
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ELGKodBledu_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER ELGKodBledu_UPDATE
')

EXEC('
CREATE TRIGGER ELGKodBledu_UPDATE ON ['+@BazaDanych+'].[dbo].[ELGKodBledu] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ELGKodBledu'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ELGKodBledu]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDELGKodBledu]
  ,[Org_Kod]
  ,[Org_Opis]
)
SELECT
2
	,@ID
 ,ins.IDELGKodBledu
 ,ins.Kod
 ,ins.Opis
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ELGKodBledu_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER ELGKodBledu_DELETE
')

EXEC('
CREATE TRIGGER ELGKodBledu_DELETE ON ['+@BazaDanych+'].[dbo].[ELGKodBledu] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ELGKodBledu'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ELGKodBledu]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDELGKodBledu]
  ,[Org_Kod]
  ,[Org_Opis]
)
SELECT
3
	,@ID
 ,del.IDELGKodBledu
 ,del.Kod
 ,del.Opis
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'ELGKodBledu')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('ELGKodBledu', 1, 0,GetDate())


go

------------------------------------------------wdro6-----------------------------------------------------------

DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------ELGStatusZlecenia-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ELGStatusZlecenia_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER ELGStatusZlecenia_INSERT
')

EXEC('
CREATE TRIGGER ELGStatusZlecenia_INSERT ON ['+@BazaDanych+'].[dbo].[ELGStatusZlecenia] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ELGStatusZlecenia'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ELGStatusZlecenia]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDELGStatusZlecenia]
  ,[Org_StatusCKM]
  ,[Org_Opis]
)
SELECT
1
	,@ID
 ,ins.IDELGStatusZlecenia
 ,ins.StatusCKM
 ,ins.Opis
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ELGStatusZlecenia_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER ELGStatusZlecenia_UPDATE
')

EXEC('
CREATE TRIGGER ELGStatusZlecenia_UPDATE ON ['+@BazaDanych+'].[dbo].[ELGStatusZlecenia] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ELGStatusZlecenia'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ELGStatusZlecenia]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDELGStatusZlecenia]
  ,[Org_StatusCKM]
  ,[Org_Opis]
)
SELECT
2
	,@ID
 ,ins.IDELGStatusZlecenia
 ,ins.StatusCKM
 ,ins.Opis
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ELGStatusZlecenia_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER ELGStatusZlecenia_DELETE
')

EXEC('
CREATE TRIGGER ELGStatusZlecenia_DELETE ON ['+@BazaDanych+'].[dbo].[ELGStatusZlecenia] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ELGStatusZlecenia'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ELGStatusZlecenia]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDELGStatusZlecenia]
  ,[Org_StatusCKM]
  ,[Org_Opis]
)
SELECT
3
	,@ID
 ,del.IDELGStatusZlecenia
 ,del.StatusCKM
 ,del.Opis
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'ELGStatusZlecenia')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('ELGStatusZlecenia', 1, 0,GetDate())



go
-------------------------------------------wdro7--------------------------------------------

DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''REJRejestrIndeksowILegitymacji_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER REJRejestrIndeksowILegitymacji_INSERT
')

EXEC('
CREATE TRIGGER REJRejestrIndeksowILegitymacji_INSERT ON ['+@BazaDanych+'].[dbo].[REJRejestrIndeksowILegitymacji] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''REJRejestrIndeksowILegitymacji'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_REJRejestrIndeksowILegitymacji]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDREJRejestrIndeksowILegitymacji]
  ,[Org_IndeksID]
  ,[Org_StudiaID]
  ,[Org_ELSKluczTransportowyID]
  ,[Org_DataWydaniaDokumentu]
  ,[Org_DataWaznosciDokumentu]
  ,[Org_OryginalDuplikat]
  ,[Org_Wersja]
  ,[Org_Edycja]
  ,[Org_CzyLegitymacja]
  ,[Org_CzyAnulowany]
  ,[Org_ImieStudenta]
  ,[Org_DrugieImieStudenta]
  ,[Org_NazwiskoStudenta]
  ,[Org_Imie1Karta]
  ,[Org_Imie2Karta]
  ,[Org_Nazwisko1Karta]
  ,[Org_Nazwisko2Karta]
  ,[Org_AdresUlica]
  ,[Org_AdresKodMiejscowosc]
  ,[Org_NazwaUczelni]
  ,[Org_WydanePrzez]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
  ,@ID
 ,ins.IDREJRejestrIndeksowILegitymacji
 ,ins.IndeksID
 ,ins.StudiaID
 ,ins.ELSKluczTransportowyID
 ,ins.DataWydaniaDokumentu
 ,ins.DataWaznosciDokumentu
 ,ins.OryginalDuplikat
 ,ins.Wersja
 ,ins.Edycja
 ,ins.CzyLegitymacja
 ,ins.CzyAnulowany
 ,ins.ImieStudenta
 ,ins.DrugieImieStudenta
 ,ins.NazwiskoStudenta
 ,ins.Imie1Karta
 ,ins.Imie2Karta
 ,ins.Nazwisko1Karta
 ,ins.Nazwisko2Karta
 ,ins.AdresUlica
 ,ins.AdresKodMiejscowosc
 ,ins.NazwaUczelni
 ,ins.WydanePrzez
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''REJRejestrIndeksowILegitymacji_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER REJRejestrIndeksowILegitymacji_UPDATE
')

EXEC('
CREATE TRIGGER REJRejestrIndeksowILegitymacji_UPDATE ON ['+@BazaDanych+'].[dbo].[REJRejestrIndeksowILegitymacji] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''REJRejestrIndeksowILegitymacji'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_REJRejestrIndeksowILegitymacji]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDREJRejestrIndeksowILegitymacji]
  ,[Org_IndeksID]
  ,[Org_StudiaID]
  ,[Org_ELSKluczTransportowyID]
  ,[Org_DataWydaniaDokumentu]
  ,[Org_DataWaznosciDokumentu]
  ,[Org_OryginalDuplikat]
  ,[Org_Wersja]
  ,[Org_Edycja]
  ,[Org_CzyLegitymacja]
  ,[Org_CzyAnulowany]
  ,[Org_ImieStudenta]
  ,[Org_DrugieImieStudenta]
  ,[Org_NazwiskoStudenta]
  ,[Org_Imie1Karta]
  ,[Org_Imie2Karta]
  ,[Org_Nazwisko1Karta]
  ,[Org_Nazwisko2Karta]
  ,[Org_AdresUlica]
  ,[Org_AdresKodMiejscowosc]
  ,[Org_NazwaUczelni]
  ,[Org_WydanePrzez]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
  ,@ID
 ,ins.IDREJRejestrIndeksowILegitymacji
 ,ins.IndeksID
 ,ins.StudiaID
 ,ins.ELSKluczTransportowyID
 ,ins.DataWydaniaDokumentu
 ,ins.DataWaznosciDokumentu
 ,ins.OryginalDuplikat
 ,ins.Wersja
 ,ins.Edycja
 ,ins.CzyLegitymacja
 ,ins.CzyAnulowany
 ,ins.ImieStudenta
 ,ins.DrugieImieStudenta
 ,ins.NazwiskoStudenta
 ,ins.Imie1Karta
 ,ins.Imie2Karta
 ,ins.Nazwisko1Karta
 ,ins.Nazwisko2Karta
 ,ins.AdresUlica
 ,ins.AdresKodMiejscowosc
 ,ins.NazwaUczelni
 ,ins.WydanePrzez
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''REJRejestrIndeksowILegitymacji_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER REJRejestrIndeksowILegitymacji_DELETE
')

EXEC('
CREATE TRIGGER REJRejestrIndeksowILegitymacji_DELETE ON ['+@BazaDanych+'].[dbo].[REJRejestrIndeksowILegitymacji] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''REJRejestrIndeksowILegitymacji'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_REJRejestrIndeksowILegitymacji]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDREJRejestrIndeksowILegitymacji]
  ,[Org_IndeksID]
  ,[Org_StudiaID]
  ,[Org_ELSKluczTransportowyID]
  ,[Org_DataWydaniaDokumentu]
  ,[Org_DataWaznosciDokumentu]
  ,[Org_OryginalDuplikat]
  ,[Org_Wersja]
  ,[Org_Edycja]
  ,[Org_CzyLegitymacja]
  ,[Org_CzyAnulowany]
  ,[Org_ImieStudenta]
  ,[Org_DrugieImieStudenta]
  ,[Org_NazwiskoStudenta]
  ,[Org_Imie1Karta]
  ,[Org_Imie2Karta]
  ,[Org_Nazwisko1Karta]
  ,[Org_Nazwisko2Karta]
  ,[Org_AdresUlica]
  ,[Org_AdresKodMiejscowosc]
  ,[Org_NazwaUczelni]
  ,[Org_WydanePrzez]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
  ,@ID
 ,del.IDREJRejestrIndeksowILegitymacji
 ,del.IndeksID
 ,del.StudiaID
 ,del.ELSKluczTransportowyID
 ,del.DataWydaniaDokumentu
 ,del.DataWaznosciDokumentu
 ,del.OryginalDuplikat
 ,del.Wersja
 ,del.Edycja
 ,del.CzyLegitymacja
 ,del.CzyAnulowany
 ,del.ImieStudenta
 ,del.DrugieImieStudenta
 ,del.NazwiskoStudenta
 ,del.Imie1Karta
 ,del.Imie2Karta
 ,del.Nazwisko1Karta
 ,del.Nazwisko2Karta
 ,del.AdresUlica
 ,del.AdresKodMiejscowosc
 ,del.NazwaUczelni
 ,del.WydanePrzez
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')

if not exists (select * from LOG_LogowanaTabela where Tabela = 'REJRejestrIndeksowILegitymacji')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('REJRejestrIndeksowILegitymacji', 1, 0,GetDate())
GO

