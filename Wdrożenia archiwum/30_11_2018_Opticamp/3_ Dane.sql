----------------wdro1-------------2018_10_16_01_ESS_WSPA_Raport_Protokol_Kolizje_Z_Karta_dane
----------------wdro2-------------2018_10_24_01_ESS_WSPA_SAK_EwidencjaStudentow_Uprawnienia_dane
----------------wdro3-------------2018_10_24_02_ESS_WSPA_DWOWariant_dane
----------------wdro4-------------2018_10_30_01_SAK_Czesne_UmowyBezDecyzji_dane
----------------wdro5-------------2018_11_08_16_ESS_WSPA_ELGKodBledu_Dane.sql
----------------wdro6-------------2018_11_08_20_ESS_WSPA_ELGStatusZlecenia_Dane.sql
----------------wdro7-------------2018_11_09_01_ESS_WSPA_SAK_DWODrzewko_LegitymacjaElektroniczna_dane.sql
----------------wdro8-------------2018_11_09_02_ESS_WSPA_SAK_LegitymacjeElektroniczne_Uprawnienia_dane.sql
----------------wdro9-------------2018_11_23_01_ESS_WSPA_SAK_DWODrzewko_EwidencjaStudentow_dane.sql
----------------wdro10------------2018_11_30_01_ESS_WSPA_SAK_UstawieniaProgramu_OptiCamp_dane.sql
----------------wdro11------------2018_11_30_02_ESS_SAK_Protokoly_DWODrzewka_dane.sql






-------------------------------------------------------wdro1---------------------------------------------------


declare @UserID int, @GrupaID int, @PodgrupaID int, @RaportID int
declare @Grupa varchar(50), @GrupaOpis varchar(200)
declare @Podgrupa varchar(50), @PodgrupaOpis varchar(200)
declare @Raport varchar(50), @RaportOpis varchar(200)
declare @Kwerenda varchar(MAX), @KwerendaUpdate int, @ParametryUpdate int
declare @LiczbaBand int, @Timeout int

set @Grupa = 'SAK'
set @GrupaOpis = 'Raporty dotycz�ce SAKa.'
set @Podgrupa = 'Dziekanat'
set @PodgrupaOpis = 'Dziakanat.'
set @Raport = 'Protoko�y - kolizje z kart� egzaminacyjn�'
set @RaportOpis = 'Protoko�y - kolizje z zatwierdzon� kart� egzaminacyjn�.'
set @LiczbaBand = 5
set @Timeout = 60 * 10 

set @ParametryUpdate = 1
set @KwerendaUpdate = 1
set @Kwerenda = 
'
select ProtokolKolizja.Opis, cast(DataKolizji as Date) as DataKolizji, cast(DataZakonczeniaKolizji as date) as DataZakonczeniaKolizji, case when CzyAktywna = 1 then ''Tak'' else ''Nie'' end [Czy aktywna kolizja],
 NrIndeksu, Student.Nazwisko + '' '' + Student.Imie as [Student], ''<'' + Wydzial.Symbol + ''> '' + Kierunek.Nazwa as [Kierunek],
Pracownik.Nazwisko + '' '' + Pracownik.Imie as [Pracownik],
Przedmiot.Nazwa as Przedmiot, FormaWymiar.NazwaFormy as Forma, FormaZajec.Symbol as [Forma zaj��],
case when ProtokolPoz.CzyPozycjaSkorygowana = 0 then cast(ProtOcena.DataZaliczenia as date) else cast(ProtKorekta.DataZaliczeniaPo as date) end DataZaliczenia,
case when ProtokolPoz.CzyPozycjaSkorygowana = 0 then ProtOcena.Powod else ProtKorekta.Powod end Powod,
case when ProtokolPoz.CzyPozycjaSkorygowana = 0 then ''Nie'' else ''Tak'' end [Czy korekta]
from ProtokolKolizja
join ProtokolPoz on Podmiot = ''ProtokolPoz'' and PodmiotID = IDProtokolPoz
join ProtokolPracownik on ProtokolPracownik.ProtokolID = ProtokolPoz.ProtokolID and ProtokolPracownik.CzyBlokowac = 0 and ProtokolPracownik.CzyUsuniety = 0
left join Protokol on ProtokolPoz.ProtokolID = IDProtokol
left join Pracownik on PracownikID = IDPracownik
left join Indeks on IndeksID = IDIndeks
left join Student on StudentID = IDStudent
left join Studia on StudiaID = IDStudia
left join Kierunek on Kierunek1ID = IDKierunek
left join SystemStudiow on Studia.SystemStudiowID = IDSystemStudiow
left join Wydzial on WydzialID = IDWydzial
left join FormaWymiar on ProtokolPoz.FormaWymiarID = IDFormaWymiar
left join SemestrRealizacji on SemestrRealizacjiID = IDSemestrRealizacji
left join KartaUzup on KartaUzupID = IDKartaUzup
left join Przedmiot on PrzedmiotID = IDPrzedmiot
left join FormaZajec on FormaZajecID = IDFormaZajec
 LEFT join (select OcenaID as Ocena, '''' as Powod, DataZaliczenia, ProtokolPozID from ProtokolOcena join ProtokolOcenaDef on ProtokolOcenaDef.IDProtokolOcenaDef = ProtokolOcena.ProtokolOcenaDefID and CzyOcenaKoncowa = 1) ProtOcena on ProtOcena.ProtokolPozID = IDProtokolPoz and ProtokolPoz.CzyPozycjaSkorygowana = 0
 LEFT join (select OcenaPoID as Ocena, Powod, DataZaliczeniaPo,  ProtokolKorekta.ProtokolPozID 
                      from ProtokolKorekta 
                      join ProtokolOcena on ProtokolOcena.IDProtokolOcena = ProtokolKorekta.ProtokolOcenaID 
                      join ProtokolOcenaDef on ProtokolOcenaDef.IDProtokolOcenaDef = ProtokolOcena.ProtokolOcenaDefID and CzyOcenaKoncowa = 1) ProtKorekta 
                           on ProtKorekta.ProtokolPozID = IDProtokolPoz and ProtokolPoz.CzyPozycjaSkorygowana = 1
where ProtokolKolizjaTypID = 1   and RokAkad = :RokAkad and SemestrID = :Semestr and SystemStudiow.CzyPodyplomowe = :CzyPodyplomowe


'

set @UserID = coalesce((select IDUzytkownik from Uzytkownik where Login = 'ktybinkowska'),0)
select @GrupaID = IDRAPRaportGrupa from RAPRaportGrupa where Nazwa = @Grupa and RAPRaportGrupaID is NULL

if coalesce(@GrupaID,0) = 0
begin
  insert into RAPRaportGrupa(RAPRaportGrupaID, Nazwa, Opis, Dodal, DataDodania)
    values(NULL, @Grupa, @GrupaOpis, @UserID, GetDate())
  select @GrupaID = scope_identity()
end

select @PodgrupaID = IDRAPRaportGrupa from RAPRaportGrupa where Nazwa = @Podgrupa and RAPRaportGrupaID = @GrupaID

if coalesce(@PodgrupaID,0) = 0
begin
  insert into RAPRaportGrupa(RAPRaportGrupaID, Nazwa, Opis, Dodal, DataDodania)
    values(@GrupaID, @Podgrupa, @PodgrupaOpis, @UserID, GetDate())
  select @PodgrupaID = scope_identity()
end

select @RaportID = IDRAPRaport from RAPRaport where Nazwa = @Raport and RAPRaportGrupaID = @PodgrupaID

if coalesce(@RaportID,0) = 0
begin
  insert into RAPRaport(RAPRaportGrupaID, Nazwa, Opis, Kwerenda, TimeOut, LiczbaBand, Dodal, DataDodania)
    values(@PodgrupaID, @Raport, @RaportOpis, @Kwerenda, @Timeout, @LiczbaBand, @UserID, GetDate())
  select @RaportID = scope_identity()
end
else if @KwerendaUpdate = 1
begin
  update RAPRaport set Kwerenda = @Kwerenda, TimeOut = @Timeout, LiczbaBand = @LiczbaBand, Zmodyfikowal = @UserID, DataModyfikacji = GetDate()
    where IDRAPRaport = @RaportID
end

if @ParametryUpdate = 1
begin
  delete from RAPRaportParametr where RAPRaportID = @RaportID
   insert into RAPRaportParametr(RAPRaportID, LP, Etykieta, Nazwa, Typ, CzyWymagany, CzyWielokrotny, WartoscDomyslna, WartoscMin, WartoscMax, DWOWystepowanieID, WyborKwerenda, SygnaturaIdentyfikator, SygnaturyOpis, Dodal, DataDodania)
    values(@RaportID, 1, 'Rok akademicki', ':RokAkad', 'INT', 1, 0, NULL, NULL, NULL, NULL, 'select distinct RokAkad, cast(RokAkad as varchar)+''/''+cast(RokAkad+1 as varchar) RokAkadOpis from semestrInfo', 'RokAkad', 'RokAkadOpis', @UserID, GetDate())
  insert into RAPRaportParametr(RAPRaportID, LP, Etykieta, Nazwa, Typ, CzyWymagany, CzyWielokrotny, WartoscDomyslna, WartoscMin, WartoscMax, DWOWystepowanieID, WyborKwerenda, SygnaturaIdentyfikator, SygnaturyOpis, Dodal, DataDodania)
    values(@RaportID, 2, 'Semestr (letni/zimowy)', ':Semestr', 'INT', 1, 0, NULL, NULL, NULL, NULL, 'select IDOkresRozliczeniowyPoz, Nazwa NazwaSemestru from OkresRozliczeniowyPoz where UczelniaID=1', 'IDOkresRozliczeniowyPoz', 'NazwaSemestru', @UserID, GetDate())
  insert into RAPRaportParametr(RAPRaportID, LP, Etykieta, Nazwa, Typ, CzyWymagany, CzyWielokrotny, WartoscDomyslna, WartoscMin, WartoscMax, DWOWystepowanieID, WyborKwerenda, SygnaturaIdentyfikator, SygnaturyOpis, Dodal, DataDodania)
    values(@RaportID, 3, 'Czy podyplomowe)', ':CzyPodyplomowe', 'INT', 1, 0, NULL, NULL, NULL, NULL, 'select 0 CzyPodyplomowe, ''Nie'' Nazwa union select 1 CzyPodyplomowe, ''Tak'' Nazwa', 'CzyPodyplomowe', 'Nazwa', @UserID, GetDate())
  
 end

GO




-------------------------------------------------------wdro2---------------------------------------------------


 declare @IDDWOObiekt int

select @IDDWOObiekt = IDDWOObiekt from DWOObiekt where Sygnatura = 'SAK_EwidencjaStudentow'

exec [dbo].[DWOUsunUprawnieniaDoObiektu] @IDDWOObiekt

delete from DWOObiekt where Sygnatura = 'SAK_EwidencjaStudentow'

declare @IDDWOObiektNadrzedny int
select @IDDWOObiektNadrzedny = IDDWOObiekt from DWOObiekt where Sygnatura = 'IntegracjaZEss'

declare @UserID int
select @UserID = IDUzytkownik from Uzytkownik where Login = 'ktybinkowska'

INSERT INTO [DWOObiekt](ObiektNadrzednyID,DWOZestawUprawnienID,Sygnatura,Nazwa,Opis,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values(@IDDWOObiektNadrzedny,3,'SAK_EwidencjaStudentow','EwidencjaStudentow',NULL,coalesce(@UserID,0),getdate(),NULL,NULL)

select @IDDWOObiekt = scope_identity() 

exec [dbo].[DWODodajUprawnieniaDoObiektu] @IDDWOObiekt, 1
go




-------------------------------------------------------wdro3---------------------------------------------------

delete from DWOWariant where IDDWOWariant between 40004 and 40006
insert into DWOWariant (IDDWOWariant, DWOJoinID, Nazwa, Opis, Warunek, CzyPelnyWarunek, CzySprawdzacUprawnienia, LP, Wymagany, Dodal , DataDodania)
values (40004,40006, 'Studenci L/I/M', 'Studenci L/I/M', 'SystemStudiow.CzyPodyplomowe = 0 and SystemStudiow.Symbol<>''DR'' and Indeks.StatusStudentaID<>11 ',1,1,1,0,452,GETDATE())

insert into DWOWariant (IDDWOWariant, DWOJoinID, Nazwa, Opis, Warunek, CzyPelnyWarunek, CzySprawdzacUprawnienia, LP, Wymagany, Dodal , DataDodania)
values (40005,40006, 'Studenci Dok', 'Studenci Dok', 'SystemStudiow.CzyPodyplomowe = 0 and SystemStudiow.Symbol=''DR'' and Indeks.StatusStudentaID<>11  ',1,1,2,0,452,GETDATE())

insert into DWOWariant (IDDWOWariant, DWOJoinID, Nazwa, Opis, Warunek, CzyPelnyWarunek, CzySprawdzacUprawnienia, LP, Wymagany, Dodal , DataDodania)
values (40006,40006, 'Studenci Podyp', 'Studenci Podyp', 'SystemStudiow.CzyPodyplomowe = 1 and Indeks.StatusStudentaID<>11  ',1,1,3,0,452,GETDATE())

update DWOWariant set LP = 4 where IDDWOWariant = 40003
update DWOWariant set LP = 5, Nazwa = 'Wszyscy', Opis = 'Wszyscy' where IDDWOWariant = 40001
update DWOWariant set LP = 6 where IDDWOWariant = 40002

go

-------------------------------------------------------wdro4---------------------------------------------------


declare @UserID int, @GrupaID int, @PodgrupaID int, @RaportID int
declare @Grupa varchar(50), @GrupaOpis varchar(200)
declare @Podgrupa varchar(50), @PodgrupaOpis varchar(200)
declare @Raport varchar(50), @RaportOpis varchar(200)
declare @Kwerenda varchar(MAX), @KwerendaUpdate int, @ParametryUpdate int
declare @LiczbaBand int, @Timeout int

set @Grupa = 'SAK'
set @GrupaOpis = 'Raporty dotycz�ce SAK.'
set @Podgrupa = 'Czesne'
set @PodgrupaOpis = 'Raporty dotycz�ce modu��w czesnego'
set @Raport = 'Umowy bez decyzji'
set @RaportOpis = 'Raport prezentuj�cy umowy bez dodanych decyzji dla towaru ''czesne''. Decyzje s� sprawdzane dla podanego przedzia�u czasowego.'
set @LiczbaBand = 5
set @Timeout = 60 * 10 --sekundy

set @ParametryUpdate = 1
set @KwerendaUpdate = 1
set @Kwerenda = '
DECLARE @DataOd datetime, @DataDo datetime, @DataDoPlusJeden datetime

SET @DataOd = :DataOd
SET @DataDo = :DataDo
SET @DataDoPlusJeden = dateadd(day,1,@DataDo)


;with DecyzjeZCzesne (IDSAK_Decyzja,SAK_SLOUmowaID, DataOd, DataDo, Towar)as
(
select D.IDSAK_Decyzja,D.SAK_SLOUmowaID,D.DataOd,D.DataDo,T.Nazwa
FROM dbo.SAK_Decyzja D 
LEFT JOIN SAK_DecyzjaPoz DP on D.IDSAK_Decyzja = DP.SAK_DecyzjaID
LEFT JOIN SAK_SLOTowar T on T.IDSAK_SLOTowar = DP.SAK_SLOTowarID 
WHERE T.Nazwa like ''%Czesne%''
and ((DataOd>=@DataOd) and (@DataDoPlusJeden is NULL or (@DataDoPlusJeden is not NULL and DataDo<@DataDoPlusJeden)))
or  ((DataOd>=@DataOd)and (@DataDoPlusJeden is NULL or (@DataDoPlusJeden is not NULL and DataDo>=@DataDoPlusJeden and DataOd<@DataDoPlusJeden)))
or  ((DataOd<@DataOd)and (DataDo>@DataOd) and (@DataDoPlusJeden is NULL or (@DataDoPlusJeden is not NULL and DataDo<@DataDoPlusJeden)))
or  ((DataOd<@DataOd)and (DataDo>@DataOd) and (@DataDoPlusJeden is NULL or (@DataDoPlusJeden is not NULL and DataDo>@DataDoPlusJeden and DataDo>@DataOd)))
),
RaportUmowy(GrupaUmow, Identyfikator2,Imie,Nazwisko,Kierunek, SystemStudiow,StatusStudenta,Sekcja,Rok, Semestr, RokAkadROzpoczecia,Identyfikator1)
as
(
SELECT UG.Nazwa AS [Umowa grupa],U.Identyfikator2,S.Imie,S.Nazwisko,KK.Nazwa,SST.Symbol,SS.Symbol,
SE.Nazwa,I.Rok,I.Semestr,I.RozpRokAkad, U.Identyfikator
FROM SAK_SLOUmowa U
LEFT JOIN  DecyzjeZCzesne DzC on U.IDSAK_SLOUmowa = DzC.SAK_SLOUmowaID 
JOIN SAK_SLOUmowaGrupa UG on UG.IDSAK_SLOUmowaGrupa = U.SAK_SLOUmowaGrupaID
LEFT JOIN Indeks I on U.Identyfikator = I.IDIndeks
LEFT JOIN Student S on S.IDStudent = I.StudentID
LEFT JOIN Studia ST on I.StudiaID = ST.IDStudia
LEFT JOIN SystemStudiow SST on ST.SystemStudiowID = SST.IDSystemStudiow
LEFT JOIN Sekcja SE on ST.SekcjaID = SE.IDSekcja
LEFT JOIN StatusStudenta SS on I.StatusStudentaID = SS.IDStatusStudenta
LEFT JOIN Kierunek KK on  ST.Kierunek1ID = KK.IDKierunek
WHERE  UG.Identyfikator1Text = ''IndeksID'' and DzC.IDSAK_Decyzja is NULL

),
HistoriaZdarzen as
(
select  IndeksID,
			ZdarzenieID,
			Data,
			DataDodania as DataWprowadzeniaZdarzenia,
			row_number() over (partition by IndeksID order by Data desc,IDHistoriaStudenta desc) as Priorytet
	from HistoriaStudenta
	where Anulowany = 0   and IndeksID in (select Identyfikator1 from RaportUmowy)
)
,
HistoriaPriorytet as 
(
select IndeksID,
			ZdarzenieID,
			Data,
			DataWprowadzeniaZdarzenia,Zdarzenie.Nazwa
from HistoriaZdarzen
left join Zdarzenie on ZdarzenieID = IDZdarzenie
where Priorytet = 1			
)

select GrupaUmow, Identyfikator2,Imie,Nazwisko,Kierunek, SystemStudiow,StatusStudenta,Sekcja,Rok, Semestr, RokAkadRozpoczecia, Nazwa as Zdarzenie
from RaportUmowy
left join HistoriaPriorytet on IndeksID =Identyfikator1

'

set @UserID = coalesce((select IDUzytkownik from Uzytkownik where Login = 'ktybinkowska'),0)
select @GrupaID = IDRAPRaportGrupa from RAPRaportGrupa where Nazwa = @Grupa and RAPRaportGrupaID is NULL

if coalesce(@GrupaID,0) = 0
begin
  insert into RAPRaportGrupa(RAPRaportGrupaID, Nazwa, Opis, Dodal, DataDodania)
    values(NULL, @Grupa, @GrupaOpis, @UserID, GetDate())
  select @GrupaID = scope_identity()
end

select @PodgrupaID = IDRAPRaportGrupa from RAPRaportGrupa where Nazwa = @Podgrupa and RAPRaportGrupaID = @GrupaID

if coalesce(@PodgrupaID,0) = 0
begin
  insert into RAPRaportGrupa(RAPRaportGrupaID, Nazwa, Opis, Dodal, DataDodania)
    values(@GrupaID, @Podgrupa, @PodgrupaOpis, @UserID, GetDate())
  select @PodgrupaID = scope_identity()
end

select @RaportID = IDRAPRaport from RAPRaport where Nazwa = @Raport and RAPRaportGrupaID = @PodgrupaID

if coalesce(@RaportID,0) = 0
begin
  insert into RAPRaport(RAPRaportGrupaID, Nazwa, Opis, Kwerenda, TimeOut, LiczbaBand, Dodal, DataDodania)
    values(@PodgrupaID, @Raport, @RaportOpis, @Kwerenda, @Timeout, @LiczbaBand, @UserID, GetDate())
  select @RaportID = scope_identity()
end
else if @KwerendaUpdate = 1
begin
  update RAPRaport set Kwerenda = @Kwerenda, TimeOut = @Timeout, LiczbaBand = @LiczbaBand, Zmodyfikowal = @UserID, DataModyfikacji = GetDate()
    where IDRAPRaport = @RaportID
end

if @ParametryUpdate = 1
begin
  delete from RAPRaportParametr where RAPRaportID = @RaportID
insert into RAPRaportParametr(RAPRaportID, LP, Etykieta, Nazwa, Typ, CzyWymagany, CzyWielokrotny, WartoscDomyslna, WartoscMin, WartoscMax, DWOWystepowanieID, WyborKwerenda, SygnaturaIdentyfikator, SygnaturyOpis, Dodal, DataDodania)
    values(@RaportID, 1, 'Data obow. od', ':DataOd', 'DATE', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @UserID, GetDate())
insert into RAPRaportParametr(RAPRaportID, LP, Etykieta, Nazwa, Typ, CzyWymagany, CzyWielokrotny, WartoscDomyslna, WartoscMin, WartoscMax, DWOWystepowanieID, WyborKwerenda, SygnaturaIdentyfikator, SygnaturyOpis, Dodal, DataDodania)
    values(@RaportID, 1, 'Data obow. do', ':DataDo', 'DATE', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, @UserID, GetDate())

end

GO



-------------------------------------------------------wdro5---------------------------------------------------



 -- -- ELGKodBledu -- -- 
DELETE FROM ELGKodBledu
INSERT INTO [ELGKodBledu](Kod,Opis) values(-1,'OK')
INSERT INTO [ELGKodBledu](Kod,Opis) values(0,'Podczas przetwarzania zlecenia jeden z element�w zlecenia posiada� b��dny rekord')
INSERT INTO [ELGKodBledu](Kod,Opis) values(1,'Nie uda�o si� odczyta� identyfikatora zlecenia z pliku XML przes�anego przez system dziekanatowy')
INSERT INTO [ELGKodBledu](Kod,Opis) values(2,'Ustawiono wznowienie dla zlecenia, kt�re nie istnieje w systemie OPTICamp. Zlecenie zosta�o odrzucone')
INSERT INTO [ELGKodBledu](Kod,Opis) values(3,'Nie ustawiono wznowienia dla zlecenia, kt�re ju� istnieje w systemie OPTICamp. Zlecenie zosta�o odrzucone')
INSERT INTO [ELGKodBledu](Kod,Opis) values(4,'Pusty parametr wej�ciowy')
INSERT INTO [ELGKodBledu](Kod,Opis) values(5,'B��d podczas zapisu do bazy danych w metodzie zapisz zlecenie')
INSERT INTO [ELGKodBledu](Kod,Opis) values(6,'Anulowanie zlecenia by�o niemo�liwe, poniewa� rozpocz�to personalizacj� tego zlecenia lub ju� spersonalizowano to zlecenie')
INSERT INTO [ELGKodBledu](Kod,Opis) values(7,'Status zlecenia nie pozwala na modyfikacj�')
INSERT INTO [ELGKodBledu](Kod,Opis) values(8,'Wyst�pi� b��d w metodzie SetZlecenia. Szczeg�y zosta�y zapisane do dziennika b��d�w OPTICamp zlecenia')
INSERT INTO [ELGKodBledu](Kod,Opis) values(9,'Wyst�pi� b��d w metodzie SetZlecenia. Nie uda�o si� zapisa� szczeg��w b��du do dziennika b��d�w OPTICamp zlecenia. Sprawd� �a�cuch danych do bazy OPTICamp i prawa dost�pu do tej bazy dla serwisu zlece�')
INSERT INTO [ELGKodBledu](Kod,Opis) values(10,'Wyst�pi� b��d w metodzie AnulujZlecenie. Szczeg�y zosta�y zapisane do dziennika b��d�w OPTICamp zlecenia')
INSERT INTO [ELGKodBledu](Kod,Opis) values(11,'Wyst�pi� b��d podczas przetwarzania metody')
INSERT INTO [ELGKodBledu](Kod,Opis) values(12,'Wykryto poziom kompatybilno�ci bazy OPTICamp danych ni�szy ni� SQL Server 2005')
INSERT INTO [ELGKodBledu](Kod,Opis) values(13,'Wyst�pi� b��d w metodzie AnulujZlecenie. Nie uda�o si� zapisa� szczeg��w b��du do dziennika b��d�w OPTICamp zlecenia. Sprawd� �a�cuch po��czenia i prawa dost�pu do bazy OPTICamp dla serwisu zlece�')
INSERT INTO [ELGKodBledu](Kod,Opis) values(14,'B��d podczas zapisu do bazy danych w metodzie modyfikuj zlecenie')
INSERT INTO [ELGKodBledu](Kod,Opis) values(15,'B��d podczas zapisu do bazy danych w metodzie wyczy�� zlecenie z rejestru kart')
INSERT INTO [ELGKodBledu](Kod,Opis) values(16,'Nie uda�o si� odnale�� zlecenia o danym identyfikatorze w bazie OPTICamp')
INSERT INTO [ELGKodBledu](Kod,Opis) values(17,'Zlecenie zosta�o wcze�niej anulowane')
INSERT INTO [ELGKodBledu](Kod,Opis) values(18,'Wyst�pi� b��d w metodzie SetModyfikacjaDanych. Nie uda�o si� zapisa� szczeg��w b��du do dziennika b��d�w OPTICamp zlecenia. Sprawd� �a�cuch danych do bazy OPTICamp i prawa dost�pu do tej bazy dla serwisu zlece�')
INSERT INTO [ELGKodBledu](Kod,Opis) values(19,'Wyst�pi� b��d w metodzie SetModyfikacjaDanych. Szczeg�y zosta�y zapisane do dziennika b��d�w OPTICamp zlecenia')
INSERT INTO [ELGKodBledu](Kod,Opis) values(20,'Wyst�pi� b��d w metodzie ZapiszAktualizacjeDanych. Szczeg�y zosta�y zapisane do dziennika b��d�w OPTICamp zlecenia')
INSERT INTO [ELGKodBledu](Kod,Opis) values(21,'Wyst�pi� b��d w metodzie ZapiszAktualizacjeDanych. Nie uda�o si� zapisa� szczeg��w b��du do dziennika b��d�w OPTICamp zlecenia. Sprawd� �a�cuch danych do bazy OPTICamp i prawa dost�pu do tej bazy dla serwisu zlece�')
GO




----------------------------------------------------wdro6-----------------------------------------------


 -- -- ELGStatusZlecenia -- -- 
DELETE FROM ELGStatusZlecenia
INSERT INTO [ELGStatusZlecenia](StatusCKM,Opis) values('OT','Otwarte, mo�na dopisywa� osoby')
INSERT INTO [ELGStatusZlecenia](StatusCKM,Opis) values('RO','Rozpocz�to pobieranie do personalizacji')
INSERT INTO [ELGStatusZlecenia](StatusCKM,Opis) values('AD','Aktualnie drukowane')
INSERT INTO [ELGStatusZlecenia](StatusCKM,Opis) values('AN','Anulowane')
INSERT INTO [ELGStatusZlecenia](StatusCKM,Opis) values('DR','Wydrukowane - fizycznie wydrukowane')
INSERT INTO [ELGStatusZlecenia](StatusCKM,Opis) values('KO','Koniec (ustawia operator Systemu dziekanatowego) ')
INSERT INTO [ELGStatusZlecenia](StatusCKM,Opis) values('OP','Oczekuj�ce na przetworzenie')
INSERT INTO [ELGStatusZlecenia](StatusCKM,Opis) values('PA','Pro�ba o anulowanie')
INSERT INTO [ELGStatusZlecenia](StatusCKM,Opis) values('PO','Pobrane przez personalizacj�')
INSERT INTO [ELGStatusZlecenia](StatusCKM,Opis) values('PW','Wys�ane ponownie - poprawiono dane po ZW')
INSERT INTO [ELGStatusZlecenia](StatusCKM,Opis) values('WY','Zlecenie wydruku wys�ane')
GO


----------------------------------------------wdro7--------------------------------------------

delete from DWOPole where IDDWOPole between 92001 and 92009
delete from DWOWariant where IDDWOWariant = 92001
delete from DWOJoin where IDDWOJoin between 92001 and 92009
delete from DWOWystepowanie where IDDWOWystepowanie = 92000

insert into DWOWystepowanie (IDDWOWystepowanie,MiejsceWystepowania,RegKeyName,MaxLiczbaKolumn,LP,CzySprawdzacUprawnienia,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (92000,'LegitymacjeElektroniczne','LegitymacjeElektroniczne',30,1,0,452,getdate(),NULL,NULL)
 
 
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (92001,92000,'ELGZlecenie',' from ELGZlecenie',NULL,0,452,getdate(),NULL,NULL)

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (92002,92000,'ELGZleceniePoz','join ELGZleceniePoz  on ELGZleceniePoz.ELGZlecenieID = ELGZlecenie.IDELGZlecenie ',92001,1,452,getdate(),NULL,NULL) 

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (92003,92000,'ELGZlecenieLog','join ELGZlecenieLog on ELGZlecenieLog.ELGZlecenieID = ELGZlecenie.IDELGZlecenie ',92001,1,452,getdate(),NULL,NULL)  
 
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (92004,92000,'Student','left join Student on Student.IDStudent = ELGZleceniePoz.StudentID',92002,1,452,getdate(),NULL,NULL) 

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (92005,92000,'ELGZlecenieRejestr','join ELGZlecenieRejestr on ELGZlecenieRejestr.ELGZlecenieID = ELGZlecenie.IDELGZlecenie ',92001,1,452,getdate(),NULL,NULL) 

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (92006,92000,'ELGStatusZlecenia','join ELGStatusZlecenia on ELGStatusZlecenia.StatusCKM = ELGZlecenie.StatusCKM ',92001,1,452,getdate(),NULL,NULL) 
  
 
insert into DWOWariant (IDDWOWariant,DWOJoinID,Nazwa,Opis,Warunek,CzyPelnyWarunek,CzySprawdzacUprawnienia,LP,Wymagany,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (92001,92001,'Wszystkie zlecenia','Wszystkie zlecenia','1 = 1',1,0,1,0,452,getdate(),NULL,NULL) 


insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (92001,92001,'IDELGZlecenie','IDELGZLECENIE','ELGZlecenie.IDELGZlecenie',NULL,'ELGZlecenie.IDELGZlecenie',NULL,1,0,1,1,1,NULL,NULL,452,getdate(),NULL,NULL) 

insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (92002,92001,'DataZlecenia','DATAZLECENIA','ELGZlecenie.DataZlecenia',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL) 
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (92003,92001,'Status','STATUS','ELGZlecenie.Status',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)  

insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (92004,92001,'StatusCKM','STATUSCKM','ELGZlecenie.StatusCKM',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL) 

insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (92005,92001,'DataRozpoczecia','DATAROZPOCZECIA','ELGZlecenie.DataRozpoczecia',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (92006,92001,'DataZakonczenia','DATAZAKONCZENIA','ELGZlecenie.DataZakonczenia',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (92007,92001,'LiczbaRekordow','LICZBAREKORDOW','ELGZlecenie.LiczbaRekordow',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   
  
 insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (92008,92001,'Nazwa','NAZWA','ELGZlecenie.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   
 
  insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (92009,92006,'Opis statusu CKM','OPISCKM','ELGStatusZlecenia.Opis',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   
 
 go
 ----------------------------------------------wdro8----------------------------------------------------------

 
 declare @IDDWOObiekt int

select @IDDWOObiekt = IDDWOObiekt from DWOObiekt where Sygnatura = 'SAK_LegitymacjeElektroniczne'

exec [dbo].[DWOUsunUprawnieniaDoObiektu] @IDDWOObiekt

delete from DWOObiekt where Sygnatura = 'SAK_LegitymacjeElektroniczne'

declare @IDDWOObiektNadrzedny int
select @IDDWOObiektNadrzedny = IDDWOObiekt from DWOObiekt where Sygnatura = 'IntegracjaZEss'

declare @UserID int
select @UserID = IDUzytkownik from Uzytkownik where Login = 'ktybinkowska'

INSERT INTO [DWOObiekt](ObiektNadrzednyID,DWOZestawUprawnienID,Sygnatura,Nazwa,Opis,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values(@IDDWOObiektNadrzedny,3,'SAK_LegitymacjeElektroniczne','LegitymacjeElektroniczne',NULL,coalesce(@UserID,0),getdate(),NULL,NULL)

select @IDDWOObiekt = scope_identity() 

exec [dbo].[DWODodajUprawnieniaDoObiektu] @IDDWOObiekt, 1
go


----------------------------------------------wdro9------------------------------------------------

delete from DWOPole where IDDWOPole between 40161 and 40163
delete from DWOJoin where IDDWOJoin = 40054

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
  values (40054,40000,'RejestrLegitymacji','left join ( select IndeksID, max(IDREJRejestrIndeksowILegitymacji) as IDRejestr from REJRejestrIndeksowILegitymacji  where CzyLegitymacja = 1 and CzyAnulowany = 0 group by IndeksID ) RejestrLegitymacji on RejestrLegitymacji.IndeksID = IDIndeks',40001,1,452,getdate(),NULL,NULL) 
  
  

insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
  values (40161,40054,'Czy wydano legitymacje',NULL,'case when RejestrLegitymacji.IDRejestr is null then  ''Nie'' else ''Tak'' end ','CZYWYDANOLEG',NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)  
  
 insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
  values (40162,40002,'Czy jest zdj�cie',NULL,'case when Zdjecie IS null then ''Nie'' else ''Tak'' end','CZYZDJECIE',NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)  
  

delete from DWOWyszukiwanie where IDDWOWyszukiwanie in (92001,92002)
INSERT INTO [DWOWyszukiwanie](IDDWOWyszukiwanie,DWOPoleID,Nazwa,TypZmiennej,RodzajWarunku,Opis,LP,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(92001,92001,'IDZlecenie','int','between','Podaj ID Zlecenia (zakres od 0 do 999999)',1,452,GETDATE(),NULL,NULL)
INSERT INTO [DWOWyszukiwanie](IDDWOWyszukiwanie,DWOPoleID,Nazwa,TypZmiennej,RodzajWarunku,Opis,LP,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
values(92002,92008,'Nazwa','text','like','Znak % oznacza we wzorcu dowolny ci�g znak�w.',2,452,GETDATE(),NULL,NULL)

go
--------------------------------------------------------wdro10--------------------------------------------------------

declare @IDRodzic int
select @IDRodzic= IDUstawieniaProgramu from UstawieniaProgramu where Nazwa = 'Inne'

insert into UstawieniaProgramu (IDUstawieniaProgramu, Nazwa, WartoscText, CzyBlokowac, TypPola, Opis, IDRodzica)
values ( 1044,'SerwerOptiCamp', '',0,'EditText','Serwer na kt�rym znajduje si� baza Opticamp', @IDRodzic)

go
----------------------------------------------------------wdro11------------------------------------------------


delete from DWOPole where IDDWOPole in (40163,40164)
delete from DWOJoin where IDDWOJoin in (40055,40056)

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
  values (40055,40000,'AktualnaRej20181','left join AktRejNaSem20181 AktualnaRej20181 on AktualnaRej20181.IndeksID = IDIndeks',40001,1,452,getdate(),NULL,NULL) 
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
  values (40056,40000,'AktualnaRej20182','left join AktRejNaSem20182 AktualnaRej20182 on AktualnaRej20182.IndeksID = IDIndeks',40001,1,452,getdate(),NULL,NULL) 
  
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
  values (40163,40055,'Aktualna rej 2018/2019 zimowy',NULL,'case when AktualnaRej20181.IndeksID is null then ''Nie'' else ''Tak'' end',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) 
  values (40164,40056,'Aktualna rej 2018/2019 letni',NULL,'case when AktualnaRej20182.IndeksID is null then ''Nie'' else ''Tak'' end',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)
  
  