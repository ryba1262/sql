
-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------USR_HistoriaHasla---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_USR_HistoriaHasla]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_USR_HistoriaHasla] (
   [Log_USR_HistoriaHaslaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_HistoriaHasla] [int] 
  ,[Org_USR_ProfilID] [int] 
  ,[Org_HasloPlain] [varchar] (128)
  ,[Org_HasloHash] [varchar] (160)
  ,[Org_ZiarnoHasla] [varchar] (32)
  ,[Org_IP] [varchar] (39)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_USR_HistoriaHasla] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_USR_HistoriaHasla] PRIMARY KEY  CLUSTERED
(
   [Log_USR_HistoriaHaslaID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_USR_HistoriaHasla] (
   [Log_USR_HistoriaHaslaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_HistoriaHasla] [int] 
  ,[Org_USR_ProfilID] [int] 
  ,[Org_HasloPlain] [varchar] (128)
  ,[Org_HasloHash] [varchar] (160)
  ,[Org_ZiarnoHasla] [varchar] (32)
  ,[Org_IP] [varchar] (39)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_HistoriaHasla ON

IF EXISTS (SELECT * FROM dbo.LOG_USR_HistoriaHasla)
EXEC('INSERT INTO dbo.Tmp_LOG_USR_HistoriaHasla (Log_USR_HistoriaHaslaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_HistoriaHasla, Org_USR_ProfilID, Org_HasloPlain, Org_HasloHash, Org_ZiarnoHasla, Org_IP, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_USR_HistoriaHaslaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_HistoriaHasla, Org_USR_ProfilID, Org_HasloPlain, Org_HasloHash, Org_ZiarnoHasla, Org_IP, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_USR_HistoriaHasla WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_HistoriaHasla OFF

DROP TABLE dbo.LOG_USR_HistoriaHasla

EXECUTE sp_rename N'dbo.Tmp_LOG_USR_HistoriaHasla', N'LOG_USR_HistoriaHasla', 'OBJECT'

ALTER TABLE [dbo].[LOG_USR_HistoriaHasla] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_USR_HistoriaHasla] PRIMARY KEY  CLUSTERED
(
   [Log_USR_HistoriaHaslaID]
) ON [PRIMARY]

END
GO


-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------USR_HistoriaAkcji---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_USR_HistoriaAkcji]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_USR_HistoriaAkcji] (
   [Log_USR_HistoriaAkcjiID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_HistoriaAkcji] [int] 
  ,[Org_Uri] [varchar] (256)
  ,[Org_USR_ProfilID] [int] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_USR_HistoriaAkcji] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_USR_HistoriaAkcji] PRIMARY KEY  CLUSTERED
(
   [Log_USR_HistoriaAkcjiID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_USR_HistoriaAkcji] (
   [Log_USR_HistoriaAkcjiID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_HistoriaAkcji] [int] 
  ,[Org_Uri] [varchar] (256)
  ,[Org_USR_ProfilID] [int] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_HistoriaAkcji ON

IF EXISTS (SELECT * FROM dbo.LOG_USR_HistoriaAkcji)
EXEC('INSERT INTO dbo.Tmp_LOG_USR_HistoriaAkcji (Log_USR_HistoriaAkcjiID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_HistoriaAkcji, Org_Uri, Org_USR_ProfilID, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_USR_HistoriaAkcjiID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_HistoriaAkcji, Org_Uri, Org_USR_ProfilID, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_USR_HistoriaAkcji WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_HistoriaAkcji OFF

DROP TABLE dbo.LOG_USR_HistoriaAkcji

EXECUTE sp_rename N'dbo.Tmp_LOG_USR_HistoriaAkcji', N'LOG_USR_HistoriaAkcji', 'OBJECT'

ALTER TABLE [dbo].[LOG_USR_HistoriaAkcji] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_USR_HistoriaAkcji] PRIMARY KEY  CLUSTERED
(
   [Log_USR_HistoriaAkcjiID]
) ON [PRIMARY]

END
GO

