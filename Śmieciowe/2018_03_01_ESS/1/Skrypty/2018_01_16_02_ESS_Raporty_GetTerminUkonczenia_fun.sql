﻿
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

ALTER function Raporty_GetTerminUkonczenia(@IDIndeks int, @IDUczelnia int)
returns varchar(50)
as 
begin

    declare @TerminUkonczenia varchar(50)
  declare @OkresRozliczeniowyID int, @RokAkad int
  declare @Aktual int
  declare @DataAktualna datetime
  select @DataAktualna = AktualnaData from dbo.AktualnaData
  select  @Aktual = dbo.DajAktualnySemestr(@DataAktualna)
  set @OkresRozliczeniowyID = @Aktual - (@Aktual/10)*10
  set @RokAkad = @Aktual/10

    declare @CzasTrwania int, @Semestr int
    declare @RokWyrownawczy int

    SELECT
    @CzasTrwania = LiczbaSemestrow, @RokWyrownawczy = RokWyrownawczy, @Semestr=Semestr
    FROM Student join Indeks on StudentID=IDStudent
    left join Studia on StudiaID = IDStudia
    left join SystemStudiow on Studia.SystemStudiowID = IDSystemStudiow
    WHERE IDIndeks=  @IDIndeks;


    declare @OkresRozlLiczba int
    SELECT @OkresRozlLiczba = OkresRozlLiczba FROM WSHE WHERE IDWSHE = @IDUczelnia

    declare @IleZostalo int

    if(@RokWyrownawczy <> 0 and @RokWyrownawczy is not null) 
      set @IleZostalo = @CzasTrwania +@OkresRozlLiczba - @Semestr
    else
      set @IleZostalo = @CzasTrwania - @Semestr;

    declare @deltaOkresRozl int, @deltaRok int
    set @deltaOkresRozl  = @IleZostalo % @OkresRozlLiczba;
    set @deltaRok        = @IleZostalo / @OkresRozlLiczba;

    declare @Lp int
    SELECT @Lp = Lp FROM OkresRozliczeniowyPoz WHERE IDOkresRozliczeniowyPoz = @OkresRozliczeniowyID

    if (@Lp +  @deltaOkresRozl >@OkresRozlLiczba )
       set @deltaRok = @deltaRok+1

    declare @LpUkonczenia int
    set @LpUkonczenia  = (@Lp + @deltaOkresRozl) %@OkresRozlLiczba
    if(@LpUkonczenia = 0)
      set @LpUkonczenia = @OkresRozlLiczba

   declare @DeltaRokDo int, @UkStudiowDzien int,@UkStudiowMiesiac int
   SELECT @UkStudiowDzien=UkStudiowDzien,@UkStudiowMiesiac=UkStudiowMiesiac,@DeltaRokDo=DeltaRokDo FROM OkresRozliczeniowyPoz
   WHERE UczelniaID = @IDUczelnia  AND  Lp = @LpUkonczenia

    declare @RokUkonczenia int
    set @RokUkonczenia =@RokAkad + @deltaRok +@DeltaRokDo

    set @TerminUkonczenia = cast(@RokUkonczenia as varchar) +'-'

    if(@UkStudiowMiesiac < 10)
      set @TerminUkonczenia = @TerminUkonczenia +'0'

    set  @TerminUkonczenia =@TerminUkonczenia + cast(@UkStudiowMiesiac as varchar) + '-'

    if (@UkStudiowMiesiac = 2) and (@UkStudiowDzien > 28) and ((@RokUkonczenia%4)<>0) -- korekta lat nieprzestępnych
      set @UkStudiowDzien = 28

    if(@UkStudiowDzien < 10)
      set @TerminUkonczenia = @TerminUkonczenia +'0'
    set @TerminUkonczenia = @TerminUkonczenia + cast(@UkStudiowDzien as varchar)

   return @TerminUkonczenia

end









