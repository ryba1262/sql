----------wdro1------2017_12_08_01_ESS_Raporty_KartaEgzElektroniczna_dane
----------wdro2------2018_01_04_01_ESS_KartaEgz_UstawieniaProgramu_dane
----------wdro3------2018_01_11_01_ESS_Raporty_KartaEgzBezOcen_dane
----------wdro4------2018_01_16_01_ESS_FlagaKartaPolaka_dane
----------wdro5------2018_01_24_01_ESS_WPW_Ustawienia_dane

----------wdro6------2018_01_24_06_ESS_WPD_USR_Rola_dane




--------------------------------------------------------wdro1-----------------------------------------------------


-- ******************* -- Raporty_KartaEgzElektroniczna -- ******************* --

delete from Raporty where IDRaporty = 10156
insert into Raporty (IDRaporty,TypRaportu,Nazwa,NazwaSzablonu,Modul,EdytujCelWydania,OpisCelWydania,EdytujDataWaznosci,OpisDataWaznosci,CzyEwidencjonowac,RaportDef,WarunekParametr,CzyWybor)
values (10156,0,'Karta egzaminacyjna elektroniczna', 'KartaEgzaminacyjnaElektroniczna', NULL, 0,'Za�wiadczenie wydaje si� w celu:',0,'Data wa�no�ci',1,NULL,NULL,NULL)



--------------------------------------------------------wdro2-----------------------------------------------------


-- ******************* -- KartaEgz_UstawieniaProgramu -- ******************* --


delete from UstawieniaProgramu where IDUstawieniaProgramu in (1019,1020,1021)

insert into UstawieniaProgramu (IDUstawieniaProgramu,Nazwa,Wartosc, CzyBlokowac, TypPola) 
                               values (1019,'Karta egzaminacyjna', NULL, 0, 'Rodzic')
                               
                               
                            
insert into UstawieniaProgramu (IDUstawieniaProgramu,Nazwa,Wartosc, CzyBlokowac, TypPola, IDRodzica, Opis) 
                               values (1020,'KERokAkadOd', 2017, 0, 'EditInt',1019,'Rok akademicki od obowi�zywania nowych kart egzaminacyjnych')
                               
                               
insert into UstawieniaProgramu (IDUstawieniaProgramu,Nazwa,Wartosc, CzyBlokowac, TypPola,IDRodzica,Opis) 
                               values (1021,'KESemestrOdID', 1, 0, 'EditInt',1019,'Semestr od obowi�zywania nowych kart egzaminacyjnych')  


--------------------------------------------------------wdro3-----------------------------------------------------


-- ******************* -- Raporty_KartaEgzBezOcen -- ******************* --


delete from Raporty where IDRaporty = 10157
insert into Raporty (IDRaporty,TypRaportu,Nazwa,NazwaSzablonu,Modul,EdytujCelWydania,OpisCelWydania,EdytujDataWaznosci,OpisDataWaznosci,CzyEwidencjonowac,RaportDef,WarunekParametr,CzyWybor)
values (10157,0,'Karta egzaminacyjna bez ocen', 'KartaEgzaminacyjnaBezOcen', NULL, 0,'Za�wiadczenie wydaje si� w celu:',0,'Data wa�no�ci',1,NULL,NULL,NULL)



--------------------------------------------------------wdro4-----------------------------------------------------


-- ******************* -- FlagaKartaPolaka -- ******************* --



delete from Drzewko where IDDrzewko = 1707
delete from ESFlaga where IDESFlaga = 53
insert into ESFlaga (IDESFlaga,Nazwa,Symbol,Lp,Dodal,DataDodania)
values (53,'Karta Polaka','', 53,452,GETDATE())

INSERT INTO [Drzewko](IDDrzewko,Nazwa,CzyCzesne,CzyEwidencja,CzyRekrutacja,CzyRaporty,CzyDzialNauki,NazwaPola,NazwaPolaDB,NazwaPolaSzablon,Tabela,KluczObcy,Funkcja,WarunekWhere,Joiny,TabelaObcaID,Priorytet,OrderByField) 
values(1707,'Flaga - Karta Polaka',0,1,1,1,0,'Flaga - Karta Polaka',NULL,NULL,'ESFlagaKartaPol',NULL,'case when ESFlagaKartaPol.ESFlagaID = 53 then ''TAK'' else ''NIE'' end',NULL,'left join ESFlagaIndeks ESFlagaKartaPol on ESFlagaKartaPol.IndeksID = IDIndeks and ESFlagaKartaPol.ESFlagaID = 53',NULL,NULL,NULL)


--------------------------------------------------------wdro5-----------------------------------------------------


-- ******************* -- WPW_Ustawienia -- ******************* --


delete from UstawieniaProgramu where IDUstawieniaProgramu >=1022 and IDUstawieniaProgramu<=1027

INSERT INTO [dbo].[UstawieniaProgramu] ([IDUstawieniaProgramu],[Nazwa],[Wartosc],[WartoscFloat],[WartoscText],[CzyBlokowac],[TypPola],[Opis],[IDRodzica])
 VALUES (1022,'Wirtualny Pok�j Dydaktyka',null ,null,null,0,'Rodzic',null,null)

INSERT INTO [dbo].[UstawieniaProgramu] ([IDUstawieniaProgramu],[Nazwa],[Wartosc],[WartoscFloat],[WartoscText],[CzyBlokowac],[TypPola],[Opis],[IDRodzica])
 VALUES (1023,'WPWAdresPocztyWychodzacej',null ,null,'wpd@ahe.lodz.pl',0,'EditText',null,1022) 
INSERT INTO [dbo].[UstawieniaProgramu] ([IDUstawieniaProgramu],[Nazwa],[Wartosc],[WartoscFloat],[WartoscText],[CzyBlokowac],[TypPola],[Opis],[IDRodzica]) 
VALUES (1024,'WPWSerwerPocztyWychodzacej',null ,null,'smtp3.ahe.lodz.pl',0,'EditText',null,1022) 
INSERT INTO [dbo].[UstawieniaProgramu] ([IDUstawieniaProgramu],[Nazwa],[Wartosc],[WartoscFloat],[WartoscText],[CzyBlokowac],[TypPola],[Opis],[IDRodzica]) 
VALUES (1025,'WPWUzytkownikPocztyWychodzacej',null ,null,'wpd@ahe.lodz.pl',0,'EditText',null,1022) 
INSERT INTO [dbo].[UstawieniaProgramu] ([IDUstawieniaProgramu],[Nazwa],[Wartosc],[WartoscFloat],[WartoscText],[CzyBlokowac],[TypPola],[Opis],[IDRodzica]) 
VALUES (1026,'WPWHasloPocztyWychodzacej',null ,null,'CeD7UxU@45',0,'EditText',null,1022)
INSERT INTO [dbo].[UstawieniaProgramu] ([IDUstawieniaProgramu],[Nazwa],[Wartosc],[WartoscFloat],[WartoscText],[CzyBlokowac],[TypPola],[Opis],[IDRodzica]) 
VALUES (1027,'WPWLink',null ,null,'http://wpd.t.ahe.lodz.pl/#!/aktywacja_konta?token=',0,'EditText',null,1022)



--------------------------------------------------------wdro6-----------------------------------------------------


-- ******************* -- 2018_01_24_06_ESS_WPD_USR_Rola_dane -- ******************* --




 -- -- USR_Rola -- -- 
DELETE FROM USR_Rola
INSERT INTO [USR_Rola](Nazwa,Podmiot,Klucz,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('dydaktyk','Pracownik','IDPracownik',1,'2018-01-16',NULL,NULL)
GO


