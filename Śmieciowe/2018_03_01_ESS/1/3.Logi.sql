----------wdro1------2018_01_24_03_ESS_WPD_USR_Profil_log
----------wdro2------2018_01_24_07_ESS_WPD_USR_Rola_log
----------wdro3------2018_01_24_10_ESS_WPD_USR_ProfilToRola_log
----------wdro4------2018_01_24_13_ESS_WPD_USR_HistoriaLogowan_log
----------wdro5------2018_01_24_16_ESS_WPD_USR_HistoriaHasla_log
----------wdro6------2018_01_24_19_ESS_WPD_USR_HistoriaAkcji_log



--------------------------------------------------------wdro1-----------------------------------------------------


-- ******************* -- 2018_01_24_03_ESS_WPD_USR_Profil_log -- ******************* --



-------- pami�taj coby ten skrypt odpala� na bazie log�w  --------


----------------USR_Profil---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_USR_Profil]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_USR_Profil] (
   [Log_USR_ProfilID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_Profil] [int] 
  ,[Org_Email] [varchar] (254)
  ,[Org_Login] [varchar] (50)
  ,[Org_HasloPlain] [varchar] (128)
  ,[Org_HasloHash] [varchar] (160)
  ,[Org_ZiarnoHasla] [varchar] (32)
  ,[Org_Imie] [varchar] (50)
  ,[Org_DrugieImie] [varchar] (50)
  ,[Org_NickName] [varchar] (50)
  ,[Org_Nazwisko] [varchar] (50)
  ,[Org_PrefiksTelefonu] [varchar] (5)
  ,[Org_Telefon] [varchar] (20)
  ,[Org_CzyHasloJednorazowe] [bit] 
  ,[Org_CzyKontoAktywne] [bit] 
  ,[Org_DataOstatniejZmianyHasla] [datetime] 
  ,[Org_Token] [uniqueidentifier] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_USR_Profil] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_USR_Profil] PRIMARY KEY  CLUSTERED
(
   [Log_USR_ProfilID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_USR_Profil] (
   [Log_USR_ProfilID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_Profil] [int] 
  ,[Org_Email] [varchar] (254)
  ,[Org_Login] [varchar] (50)
  ,[Org_HasloPlain] [varchar] (128)
  ,[Org_HasloHash] [varchar] (160)
  ,[Org_ZiarnoHasla] [varchar] (32)
  ,[Org_Imie] [varchar] (50)
  ,[Org_DrugieImie] [varchar] (50)
  ,[Org_NickName] [varchar] (50)
  ,[Org_Nazwisko] [varchar] (50)
  ,[Org_PrefiksTelefonu] [varchar] (5)
  ,[Org_Telefon] [varchar] (20)
  ,[Org_CzyHasloJednorazowe] [bit] 
  ,[Org_CzyKontoAktywne] [bit] 
  ,[Org_DataOstatniejZmianyHasla] [datetime] 
  ,[Org_Token] [uniqueidentifier] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_Profil ON

IF EXISTS (SELECT * FROM dbo.LOG_USR_Profil)
EXEC('INSERT INTO dbo.Tmp_LOG_USR_Profil (Log_USR_ProfilID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_Profil, Org_Email, Org_Login, Org_HasloPlain, Org_HasloHash, Org_ZiarnoHasla, Org_Imie, Org_DrugieImie, Org_NickName, Org_Nazwisko, Org_PrefiksTelefonu, Org_Telefon, Org_CzyHasloJednorazowe, Org_CzyKontoAktywne, Org_DataOstatniejZmianyHasla, Org_Token, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_USR_ProfilID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_Profil, Org_Email, Org_Login, Org_HasloPlain, Org_HasloHash, Org_ZiarnoHasla, Org_Imie, Org_DrugieImie, Org_NickName, Org_Nazwisko, Org_PrefiksTelefonu, Org_Telefon, Org_CzyHasloJednorazowe, Org_CzyKontoAktywne, Org_DataOstatniejZmianyHasla, Org_Token, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_USR_Profil WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_Profil OFF

DROP TABLE dbo.LOG_USR_Profil

EXECUTE sp_rename N'dbo.Tmp_LOG_USR_Profil', N'LOG_USR_Profil', 'OBJECT'

ALTER TABLE [dbo].[LOG_USR_Profil] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_USR_Profil] PRIMARY KEY  CLUSTERED
(
   [Log_USR_ProfilID]
) ON [PRIMARY]

END
GO



--------------------------------------------------------wdro2-----------------------------------------------------


-- ******************* -- 2018_01_24_07_ESS_WPD_USR_Rola_log -- ******************* --



-------- pami�taj coby ten skrypt odpala� na bazie log�w  --------


----------------USR_Profil---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_USR_Profil]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_USR_Profil] (
   [Log_USR_ProfilID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_Profil] [int] 
  ,[Org_Email] [varchar] (254)
  ,[Org_Login] [varchar] (50)
  ,[Org_HasloPlain] [varchar] (128)
  ,[Org_HasloHash] [varchar] (160)
  ,[Org_ZiarnoHasla] [varchar] (32)
  ,[Org_Imie] [varchar] (50)
  ,[Org_DrugieImie] [varchar] (50)
  ,[Org_NickName] [varchar] (50)
  ,[Org_Nazwisko] [varchar] (50)
  ,[Org_PrefiksTelefonu] [varchar] (5)
  ,[Org_Telefon] [varchar] (20)
  ,[Org_CzyHasloJednorazowe] [bit] 
  ,[Org_CzyKontoAktywne] [bit] 
  ,[Org_DataOstatniejZmianyHasla] [datetime] 
  ,[Org_Token] [uniqueidentifier] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_USR_Profil] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_USR_Profil] PRIMARY KEY  CLUSTERED
(
   [Log_USR_ProfilID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_USR_Profil] (
   [Log_USR_ProfilID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_Profil] [int] 
  ,[Org_Email] [varchar] (254)
  ,[Org_Login] [varchar] (50)
  ,[Org_HasloPlain] [varchar] (128)
  ,[Org_HasloHash] [varchar] (160)
  ,[Org_ZiarnoHasla] [varchar] (32)
  ,[Org_Imie] [varchar] (50)
  ,[Org_DrugieImie] [varchar] (50)
  ,[Org_NickName] [varchar] (50)
  ,[Org_Nazwisko] [varchar] (50)
  ,[Org_PrefiksTelefonu] [varchar] (5)
  ,[Org_Telefon] [varchar] (20)
  ,[Org_CzyHasloJednorazowe] [bit] 
  ,[Org_CzyKontoAktywne] [bit] 
  ,[Org_DataOstatniejZmianyHasla] [datetime] 
  ,[Org_Token] [uniqueidentifier] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_Profil ON

IF EXISTS (SELECT * FROM dbo.LOG_USR_Profil)
EXEC('INSERT INTO dbo.Tmp_LOG_USR_Profil (Log_USR_ProfilID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_Profil, Org_Email, Org_Login, Org_HasloPlain, Org_HasloHash, Org_ZiarnoHasla, Org_Imie, Org_DrugieImie, Org_NickName, Org_Nazwisko, Org_PrefiksTelefonu, Org_Telefon, Org_CzyHasloJednorazowe, Org_CzyKontoAktywne, Org_DataOstatniejZmianyHasla, Org_Token, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_USR_ProfilID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_Profil, Org_Email, Org_Login, Org_HasloPlain, Org_HasloHash, Org_ZiarnoHasla, Org_Imie, Org_DrugieImie, Org_NickName, Org_Nazwisko, Org_PrefiksTelefonu, Org_Telefon, Org_CzyHasloJednorazowe, Org_CzyKontoAktywne, Org_DataOstatniejZmianyHasla, Org_Token, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_USR_Profil WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_Profil OFF

DROP TABLE dbo.LOG_USR_Profil

EXECUTE sp_rename N'dbo.Tmp_LOG_USR_Profil', N'LOG_USR_Profil', 'OBJECT'

ALTER TABLE [dbo].[LOG_USR_Profil] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_USR_Profil] PRIMARY KEY  CLUSTERED
(
   [Log_USR_ProfilID]
) ON [PRIMARY]

END
GO


-------- pami�taj coby ten skrypt odpala� na bazie log�w  --------


----------------USR_Rola---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_USR_Rola]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_USR_Rola] (
   [Log_USR_RolaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_Rola] [int] 
  ,[Org_Nazwa] [varchar] (128)
  ,[Org_Podmiot] [varchar] (128)
  ,[Org_Klucz] [varchar] (128)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_USR_Rola] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_USR_Rola] PRIMARY KEY  CLUSTERED
(
   [Log_USR_RolaID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_USR_Rola] (
   [Log_USR_RolaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_Rola] [int] 
  ,[Org_Nazwa] [varchar] (128)
  ,[Org_Podmiot] [varchar] (128)
  ,[Org_Klucz] [varchar] (128)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_Rola ON

IF EXISTS (SELECT * FROM dbo.LOG_USR_Rola)
EXEC('INSERT INTO dbo.Tmp_LOG_USR_Rola (Log_USR_RolaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_Rola, Org_Nazwa, Org_Podmiot, Org_Klucz, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_USR_RolaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_Rola, Org_Nazwa, Org_Podmiot, Org_Klucz, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_USR_Rola WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_Rola OFF

DROP TABLE dbo.LOG_USR_Rola

EXECUTE sp_rename N'dbo.Tmp_LOG_USR_Rola', N'LOG_USR_Rola', 'OBJECT'

ALTER TABLE [dbo].[LOG_USR_Rola] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_USR_Rola] PRIMARY KEY  CLUSTERED
(
   [Log_USR_RolaID]
) ON [PRIMARY]

END
GO



--------------------------------------------------------wdro3-----------------------------------------------------


-- ******************* -- 2018_01_24_10_ESS_WPD_USR_ProfilToRola_log -- ******************* --



-------- pami�taj coby ten skrypt odpala� na bazie log�w  --------


----------------USR_ProfilToRola---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_USR_ProfilToRola]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_USR_ProfilToRola] (
   [Log_USR_ProfilToRolaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_ProfilToRola] [int] 
  ,[Org_USR_ProfilID] [int] 
  ,[Org_USR_RolaID] [int] 
  ,[Org_KluczZdalny] [int] 
  ,[Org_Aktywny] [bit] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_USR_ProfilToRola] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_USR_ProfilToRola] PRIMARY KEY  CLUSTERED
(
   [Log_USR_ProfilToRolaID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_USR_ProfilToRola] (
   [Log_USR_ProfilToRolaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_ProfilToRola] [int] 
  ,[Org_USR_ProfilID] [int] 
  ,[Org_USR_RolaID] [int] 
  ,[Org_KluczZdalny] [int] 
  ,[Org_Aktywny] [bit] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_ProfilToRola ON

IF EXISTS (SELECT * FROM dbo.LOG_USR_ProfilToRola)
EXEC('INSERT INTO dbo.Tmp_LOG_USR_ProfilToRola (Log_USR_ProfilToRolaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_ProfilToRola, Org_USR_ProfilID, Org_USR_RolaID, Org_KluczZdalny, Org_Aktywny, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_USR_ProfilToRolaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_ProfilToRola, Org_USR_ProfilID, Org_USR_RolaID, Org_KluczZdalny, Org_Aktywny, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_USR_ProfilToRola WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_ProfilToRola OFF

DROP TABLE dbo.LOG_USR_ProfilToRola

EXECUTE sp_rename N'dbo.Tmp_LOG_USR_ProfilToRola', N'LOG_USR_ProfilToRola', 'OBJECT'

ALTER TABLE [dbo].[LOG_USR_ProfilToRola] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_USR_ProfilToRola] PRIMARY KEY  CLUSTERED
(
   [Log_USR_ProfilToRolaID]
) ON [PRIMARY]

END
GO



--------------------------------------------------------wdro4-----------------------------------------------------


-- ******************* -- 2018_01_24_13_ESS_WPD_USR_HistoriaLogowan_log -- ******************* --



-------- pami�taj coby ten skrypt odpala� na bazie log�w  --------


----------------USR_HistoriaLogowan---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_USR_HistoriaLogowan]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_USR_HistoriaLogowan] (
   [Log_USR_HistoriaLogowanID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_HistoriaLogowan] [int] 
  ,[Org_Login] [varchar] (50)
  ,[Org_AdresZdalny] [varchar] (50)
  ,[Org_Powodzenie] [bit] 
  ,[Org_DataAkcji] [datetime] 
  ,[Org_UserAgent] [varchar] (256)
  ,[Org_SessionToken] [varchar] (32)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DadaModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_USR_HistoriaLogowan] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_USR_HistoriaLogowan] PRIMARY KEY  CLUSTERED
(
   [Log_USR_HistoriaLogowanID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_USR_HistoriaLogowan] (
   [Log_USR_HistoriaLogowanID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_HistoriaLogowan] [int] 
  ,[Org_Login] [varchar] (50)
  ,[Org_AdresZdalny] [varchar] (50)
  ,[Org_Powodzenie] [bit] 
  ,[Org_DataAkcji] [datetime] 
  ,[Org_UserAgent] [varchar] (256)
  ,[Org_SessionToken] [varchar] (32)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DadaModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_HistoriaLogowan ON

IF EXISTS (SELECT * FROM dbo.LOG_USR_HistoriaLogowan)
EXEC('INSERT INTO dbo.Tmp_LOG_USR_HistoriaLogowan (Log_USR_HistoriaLogowanID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_HistoriaLogowan, Org_Login, Org_AdresZdalny, Org_Powodzenie, Org_DataAkcji, Org_UserAgent, Org_SessionToken, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DadaModyfikacji)
  SELECT Log_USR_HistoriaLogowanID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_HistoriaLogowan, Org_Login, Org_AdresZdalny, Org_Powodzenie, Org_DataAkcji, Org_UserAgent, Org_SessionToken, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DadaModyfikacji FROM dbo.Log_USR_HistoriaLogowan WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_HistoriaLogowan OFF

DROP TABLE dbo.LOG_USR_HistoriaLogowan

EXECUTE sp_rename N'dbo.Tmp_LOG_USR_HistoriaLogowan', N'LOG_USR_HistoriaLogowan', 'OBJECT'

ALTER TABLE [dbo].[LOG_USR_HistoriaLogowan] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_USR_HistoriaLogowan] PRIMARY KEY  CLUSTERED
(
   [Log_USR_HistoriaLogowanID]
) ON [PRIMARY]

END
GO




--------------------------------------------------------wdro5-----------------------------------------------------


-- ******************* -- 2018_01_24_16_ESS_WPD_USR_HistoriaHasla_log -- ******************* --



-------- pami�taj coby ten skrypt odpala� na bazie log�w  --------


----------------USR_HistoriaHasla---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_USR_HistoriaHasla]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_USR_HistoriaHasla] (
   [Log_USR_HistoriaHaslaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_HistoriaHasla] [int] 
  ,[Org_USR_ProfilID] [int] 
  ,[Org_HasloPlain] [varchar] (128)
  ,[Org_HasloHash] [varchar] (160)
  ,[Org_ZiarnoHasla] [varchar] (32)
  ,[Org_IP] [varchar] (39)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_USR_HistoriaHasla] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_USR_HistoriaHasla] PRIMARY KEY  CLUSTERED
(
   [Log_USR_HistoriaHaslaID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_USR_HistoriaHasla] (
   [Log_USR_HistoriaHaslaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_HistoriaHasla] [int] 
  ,[Org_USR_ProfilID] [int] 
  ,[Org_HasloPlain] [varchar] (128)
  ,[Org_HasloHash] [varchar] (160)
  ,[Org_ZiarnoHasla] [varchar] (32)
  ,[Org_IP] [varchar] (39)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_HistoriaHasla ON

IF EXISTS (SELECT * FROM dbo.LOG_USR_HistoriaHasla)
EXEC('INSERT INTO dbo.Tmp_LOG_USR_HistoriaHasla (Log_USR_HistoriaHaslaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_HistoriaHasla, Org_USR_ProfilID, Org_HasloPlain, Org_HasloHash, Org_ZiarnoHasla, Org_IP, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_USR_HistoriaHaslaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_HistoriaHasla, Org_USR_ProfilID, Org_HasloPlain, Org_HasloHash, Org_ZiarnoHasla, Org_IP, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_USR_HistoriaHasla WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_HistoriaHasla OFF

DROP TABLE dbo.LOG_USR_HistoriaHasla

EXECUTE sp_rename N'dbo.Tmp_LOG_USR_HistoriaHasla', N'LOG_USR_HistoriaHasla', 'OBJECT'

ALTER TABLE [dbo].[LOG_USR_HistoriaHasla] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_USR_HistoriaHasla] PRIMARY KEY  CLUSTERED
(
   [Log_USR_HistoriaHaslaID]
) ON [PRIMARY]

END
GO





--------------------------------------------------------wdro6-----------------------------------------------------


-- ******************* -- 2018_01_24_19_ESS_WPD_USR_HistoriaAkcji_log -- ******************* --




-------- pami�taj coby ten skrypt odpala� na bazie log�w  --------


----------------USR_HistoriaHasla---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_USR_HistoriaHasla]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_USR_HistoriaHasla] (
   [Log_USR_HistoriaHaslaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_HistoriaHasla] [int] 
  ,[Org_USR_ProfilID] [int] 
  ,[Org_HasloPlain] [varchar] (128)
  ,[Org_HasloHash] [varchar] (160)
  ,[Org_ZiarnoHasla] [varchar] (32)
  ,[Org_IP] [varchar] (39)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_USR_HistoriaHasla] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_USR_HistoriaHasla] PRIMARY KEY  CLUSTERED
(
   [Log_USR_HistoriaHaslaID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_USR_HistoriaHasla] (
   [Log_USR_HistoriaHaslaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_HistoriaHasla] [int] 
  ,[Org_USR_ProfilID] [int] 
  ,[Org_HasloPlain] [varchar] (128)
  ,[Org_HasloHash] [varchar] (160)
  ,[Org_ZiarnoHasla] [varchar] (32)
  ,[Org_IP] [varchar] (39)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_HistoriaHasla ON

IF EXISTS (SELECT * FROM dbo.LOG_USR_HistoriaHasla)
EXEC('INSERT INTO dbo.Tmp_LOG_USR_HistoriaHasla (Log_USR_HistoriaHaslaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_HistoriaHasla, Org_USR_ProfilID, Org_HasloPlain, Org_HasloHash, Org_ZiarnoHasla, Org_IP, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_USR_HistoriaHaslaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_HistoriaHasla, Org_USR_ProfilID, Org_HasloPlain, Org_HasloHash, Org_ZiarnoHasla, Org_IP, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_USR_HistoriaHasla WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_HistoriaHasla OFF

DROP TABLE dbo.LOG_USR_HistoriaHasla

EXECUTE sp_rename N'dbo.Tmp_LOG_USR_HistoriaHasla', N'LOG_USR_HistoriaHasla', 'OBJECT'

ALTER TABLE [dbo].[LOG_USR_HistoriaHasla] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_USR_HistoriaHasla] PRIMARY KEY  CLUSTERED
(
   [Log_USR_HistoriaHaslaID]
) ON [PRIMARY]

END
GO


-------- pami�taj coby ten skrypt odpala� na bazie log�w  --------


----------------USR_HistoriaAkcji---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_USR_HistoriaAkcji]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_USR_HistoriaAkcji] (
   [Log_USR_HistoriaAkcjiID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_HistoriaAkcji] [int] 
  ,[Org_Uri] [varchar] (256)
  ,[Org_USR_ProfilID] [int] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_USR_HistoriaAkcji] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_USR_HistoriaAkcji] PRIMARY KEY  CLUSTERED
(
   [Log_USR_HistoriaAkcjiID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_USR_HistoriaAkcji] (
   [Log_USR_HistoriaAkcjiID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_HistoriaAkcji] [int] 
  ,[Org_Uri] [varchar] (256)
  ,[Org_USR_ProfilID] [int] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_HistoriaAkcji ON

IF EXISTS (SELECT * FROM dbo.LOG_USR_HistoriaAkcji)
EXEC('INSERT INTO dbo.Tmp_LOG_USR_HistoriaAkcji (Log_USR_HistoriaAkcjiID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_HistoriaAkcji, Org_Uri, Org_USR_ProfilID, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_USR_HistoriaAkcjiID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_HistoriaAkcji, Org_Uri, Org_USR_ProfilID, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_USR_HistoriaAkcji WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_HistoriaAkcji OFF

DROP TABLE dbo.LOG_USR_HistoriaAkcji

EXECUTE sp_rename N'dbo.Tmp_LOG_USR_HistoriaAkcji', N'LOG_USR_HistoriaAkcji', 'OBJECT'

ALTER TABLE [dbo].[LOG_USR_HistoriaAkcji] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_USR_HistoriaAkcji] PRIMARY KEY  CLUSTERED
(
   [Log_USR_HistoriaAkcjiID]
) ON [PRIMARY]

END
GO

