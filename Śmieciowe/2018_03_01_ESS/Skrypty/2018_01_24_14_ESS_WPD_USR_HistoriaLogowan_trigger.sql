DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------USR_HistoriaLogowan-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_HistoriaLogowan_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER USR_HistoriaLogowan_INSERT
')

EXEC('
CREATE TRIGGER USR_HistoriaLogowan_INSERT ON ['+@BazaDanych+'].[dbo].[USR_HistoriaLogowan] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_HistoriaLogowan'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_HistoriaLogowan]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_HistoriaLogowan]
  ,[Org_Login]
  ,[Org_AdresZdalny]
  ,[Org_Powodzenie]
  ,[Org_DataAkcji]
  ,[Org_UserAgent]
  ,[Org_SessionToken]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DadaModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDUSR_HistoriaLogowan
 ,ins.Login
 ,ins.AdresZdalny
 ,ins.Powodzenie
 ,ins.DataAkcji
 ,ins.UserAgent
 ,ins.SessionToken
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DadaModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_HistoriaLogowan_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER USR_HistoriaLogowan_UPDATE
')

EXEC('
CREATE TRIGGER USR_HistoriaLogowan_UPDATE ON ['+@BazaDanych+'].[dbo].[USR_HistoriaLogowan] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_HistoriaLogowan'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_HistoriaLogowan]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_HistoriaLogowan]
  ,[Org_Login]
  ,[Org_AdresZdalny]
  ,[Org_Powodzenie]
  ,[Org_DataAkcji]
  ,[Org_UserAgent]
  ,[Org_SessionToken]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DadaModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDUSR_HistoriaLogowan
 ,ins.Login
 ,ins.AdresZdalny
 ,ins.Powodzenie
 ,ins.DataAkcji
 ,ins.UserAgent
 ,ins.SessionToken
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DadaModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_HistoriaLogowan_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER USR_HistoriaLogowan_DELETE
')

EXEC('
CREATE TRIGGER USR_HistoriaLogowan_DELETE ON ['+@BazaDanych+'].[dbo].[USR_HistoriaLogowan] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_HistoriaLogowan'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_HistoriaLogowan]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_HistoriaLogowan]
  ,[Org_Login]
  ,[Org_AdresZdalny]
  ,[Org_Powodzenie]
  ,[Org_DataAkcji]
  ,[Org_UserAgent]
  ,[Org_SessionToken]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DadaModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDUSR_HistoriaLogowan
 ,del.Login
 ,del.AdresZdalny
 ,del.Powodzenie
 ,del.DataAkcji
 ,del.UserAgent
 ,del.SessionToken
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DadaModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'USR_HistoriaLogowan')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('USR_HistoriaLogowan', 1, 0,GetDate())

GO