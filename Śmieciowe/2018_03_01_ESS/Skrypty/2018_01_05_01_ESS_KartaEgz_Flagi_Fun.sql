
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KartaEgzFlagi]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[KartaEgzFlagi]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[KartaEgzFlagi] (@IDKartaEgz int)  
RETURNS @Output table (Flaga int, BrakujacePunkty int, Czesne int, BrakPkt int, BrakCzesne int)
BEGIN

	declare @Flaga int, @BrakujacePunkty int, @ZdobytePunkty int, @BrakujacePunktyCzesne int, @DodatkowePunkty int,
			@RokAkad int, @SemestrID int, @IndeksID int, @BrakujacePrzedmiotyZaZero int,
			@Zatwierdzona int, @DecyzjaDziekanaPunkty int, @NastepnaRejestracjaID int, @BrakujacePunktyHistoria int,
			@BrakujacePunktyCzesneHistoria int, @NominalnaLiczbaPunktow int, @KERokAkadOd int, @KESemestrIDOd int
	
	set @Flaga = 0	 
	set @BrakujacePunkty = 0 
	set @ZdobytePunkty = 0 
	set @BrakujacePunktyCzesne = 0 
	set @DodatkowePunkty = 0
	set @BrakujacePrzedmiotyZaZero = 0
	set @NominalnaLiczbaPunktow = 0

	select @Zatwierdzona = Zatwierdzona, @RokAkad = RokAkad, @SemestrID = SemestrID, @IndeksID = IndeksID
	from KartaEgz where IDKartaEgz = @IDKartaEgz

	declare @ZRokAkad int, @ZSemestrID int
	set @ZRokAkad = 0
	set @ZSemestrID = 0

	select top 1 @ZRokAkad = RokAkademicki, @ZSemestrID = SemestrAkadID
	from HistoriaStudenta where ZdarzenieID = 32 and IndeksID = @IndeksID and (@RokAkad * 100 + @SemestrID) >= (RokAkademicki * 100 + SemestrAkadID) and Anulowany = 0
	order by RokAkademicki desc, SemestrAkadID desc

	if (@Zatwierdzona = 1)
	begin

		select  @NastepnaRejestracjaID = nast_rej.IDHistoriaStudenta,
				@BrakujacePunktyHistoria = cast(substring(nast_rej.OldParams, 10, 2) as int),
				@BrakujacePunktyCzesneHistoria = cast(substring(nast_rej.OldParams, 12, 2) as int)
		from KartaEgz ke
		cross apply
		(
			select top 1 IDHistoriaStudenta, OldParams
			from HistoriaStudenta
			where IndeksID = ke.IndeksID
			and ZdarzenieID = 1
			and Anulowany = 0
			and (RokAkademicki * 100 + SemestrAkadID) > (ke.RokAkad * 100 + ke.SemestrID)
			and cast(substring(OldParams, 1, 1) as int) = ke.SemestrStudiow		
			order by DataDodania	
		) nast_rej
		where ke.IDKartaEgz = @IDKartaEgz

		if (@NastepnaRejestracjaID is null)
		  begin
			set @BrakujacePunktyHistoria = -1
			set @BrakujacePunktyCzesneHistoria = -1
		  end

		
		select @KERokAkadOd = Wartosc from UstawieniaProgramu where Nazwa = 'KERokAkadOd'
	    select @KESemestrIDOd = Wartosc from UstawieniaProgramu where Nazwa = 'KESemestrOdID'
		set @NominalnaLiczbaPunktow = dbo.KartaEgzNominalnaLiczbaPkt(@IDKartaEgz)		

		;with ListaZaliczonychPrzedmiotow
		as
		(
			select  ke.IndeksID,
					ke.IDKartaEgz,
					kep.IDKartaEgzPoz,
					hr.IDSemReal,
					sr.LiczbaPunktow,
					sop.CzyZalicza,
					sop.Wartosc,
					case when (select COUNT(*) from KartaEgzForma 
		                       left join SkalaOcenPoz on OcenaID = IDSkalaOcenPoz
		                       JOIN FormaWymiar ON KartaEgzForma.FormaWymiarID = FormaWymiar.IDFormaWymiar
                                LEFT JOIN FormaZajec ON FormaWymiar.FormaZajecID = FormaZajec.IDFormaZajec
		                        where KartaEgzPozID =kep.IDKartaEgzPoz	and CzyDrukowacNaKarcieEgz = 1 and (CzyZalicza = 0 or CzyZalicza  is null) and CzyPoprawkowy = 0)>0 then 0 else 1 end CzyZaliczaNowe, 
		            case when RokAkad*10+SemestrID >=@KERokAkadOd*10+@KESemestrIDOd then 1 else 0 end CzyNoweKarty,
					row_number() over (partition by IDSemReal order by kep.IDKartaEgzPoz desc) as NrWiersza
			from dbo.KartaEgzHistoriaRozliczen(@IndeksID, coalesce(@ZRokAkad, 0), coalesce(@ZSemestrID, 0), @RokAkad, @SemestrID, 1, null) hr
			join SemestrRealizacji sr on hr.IDSemReal = sr.IDSemestrRealizacji
			join KartaEgzPoz kep on hr.IDSemReal = kep.SemestrRealizacjiID
			join KartaEgz ke on kep.KartaEgzID = ke.IDKartaEgz
							 and ke.IndeksID = @IndeksID							  
							 and (ke.RokAkad * 100 + ke.SemestrID) <= (@RokAkad * 100 + @SemestrID)  
							 and (ke.RokAkad * 100 + ke.SemestrID) >= (coalesce(@ZRokAkad, 0) * 100 + coalesce(@ZSemestrID, 0))
			left join SkalaOcenPoz sop on kep.OcenaKoncowaID = sop.IDSkalaOcenPoz
								 
		)

		select @ZdobytePunkty = sum(LiczbaPunktow)
		from ListaZaliczonychPrzedmiotow
		where NrWiersza = 1 and ((CzyZalicza = 1 and CzyNoweKarty = 0) or (CzyZaliczaNowe = 1 and CzyNoweKarty = 1))

		set @BrakujacePunkty = coalesce(@NominalnaLiczbaPunktow, 0) - coalesce(@ZdobytePunkty, 0)



		;with KorektyLiczbyPunktow as
		( 
			select  klpp.ObowiazujacaLPunktow,
					sr.LiczbaPunktow,
					row_number() over (partition by hr.IDSemReal order by hs.RokAkademicki desc, hs.SemestrAkadID desc) as NrWiersza
			from dbo.KartaEgzHistoriaRozliczen(@IndeksID, coalesce(@ZRokAkad, 0), coalesce(@ZSemestrID, 0), @RokAkad, @SemestrID, 1, null) hr
			join SemestrRealizacji sr on hr.IDSemReal = sr.IDSemestrRealizacji
			join KorektaLiczbyPunktowPoz as klpp on hr.IDSemReal = klpp.SemestrRealizacjiID
			join KorektaLiczbyPunktow klp on klpp.KorektaLiczbyPunktowID = klp.IDKorektaLiczbyPunktow 
											 and klp.IndeksID = @IndeksID 
											 and (klp.RokAkad * 100 + klp.SemestrID) <= (@RokAkad * 100 + @SemestrID)
											 and (klp.RokAkad * 100 + klp.SemestrID) >= (coalesce(@ZRokAkad, 0) * 100 + coalesce(@ZSemestrID, 0))
			join HistoriaStudenta hs on hs.TabelaID = klp.IDKorektaLiczbyPunktow 
									 and hs.ZdarzenieID = 33 
									 and hs.IndeksID = @IndeksID
									 and hs.Anulowany = 0
		)

		select @DecyzjaDziekanaPunkty = sum(ObowiazujacaLPunktow - LiczbaPunktow)
		from KorektyLiczbyPunktow
		where NrWiersza = 1

		set @BrakujacePunkty = coalesce(@BrakujacePunkty, 0) - coalesce(@DecyzjaDziekanaPunkty, 0)



		select top 1 @DodatkowePunkty = PoprzSemPkt
		from KartaEgz ke	
		where ke.IndeksID = @IndeksID				 
		and (ke.RokAkad * 100 + ke.SemestrID) <= (@RokAkad * 100 + @SemestrID)
		and (ke.RokAkad * 100 + ke.SemestrID) >= (coalesce(@ZRokAkad, 0) * 100 + coalesce(@ZSemestrID, 0))
		order by ke.RokAkad, ke.SemestrID, ke.KartaPoczatkowa desc

		set @BrakujacePunkty = coalesce(@BrakujacePunkty, 0) - coalesce(@DodatkowePunkty, 0)



		select @BrakujacePrzedmiotyZaZero = dbo.KartaEgzBrakujacePrzedmiotyZaZero(@IDKartaEgz)
		
		if(@BrakujacePunkty < 0)
			set @BrakujacePunkty = 0
		set @BrakujacePunktyCzesne = @BrakujacePunkty + (coalesce(@BrakujacePrzedmiotyZaZero, 0) * 3)

		if(@BrakujacePunktyCzesne >= 3)
			set @BrakujacePunktyCzesne = @BrakujacePunktyCzesne - 3
		else 
			set @BrakujacePunktyCzesne = 0



		if (@BrakujacePunktyHistoria = -1 and @BrakujacePunktyCzesneHistoria = -1) 
		  begin	
			set @Flaga = 0
			set @BrakujacePunktyHistoria = @BrakujacePunkty
			set @BrakujacePunktyCzesneHistoria = @BrakujacePunktyCzesne
		  end
		else
		  begin
			if ((@BrakujacePunkty - @BrakujacePunktyHistoria) = 0 and (@BrakujacePunktyCzesne - @BrakujacePunktyCzesneHistoria) = 0)
				set @Flaga = 0 
			else 
			  begin
				if ((@BrakujacePunkty - @BrakujacePunktyHistoria) <> 0 and (@BrakujacePunktyCzesne - @BrakujacePunktyCzesneHistoria) <> 0) 
					set @Flaga = 3
				else if ((@BrakujacePunkty - @BrakujacePunktyHistoria) <> 0)
					set @Flaga = 1
				else if ((@BrakujacePunktyCzesne - @BrakujacePunktyCzesneHistoria) <> 0)
					set @Flaga = 2			
			  end
		  end
	end

	insert into @Output (Flaga, BrakujacePunkty, Czesne, BrakPkt, BrakCzesne)  
	select @Flaga, @BrakujacePunkty, @BrakujacePunkty, @BrakujacePunktyHistoria, @BrakujacePunktyCzesneHistoria

	return

END

GO


