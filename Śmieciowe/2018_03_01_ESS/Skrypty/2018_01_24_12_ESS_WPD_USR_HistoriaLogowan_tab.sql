IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[USR_HistoriaLogowan]') AND name = N'PK_USR_HistoriaLogowan')
ALTER TABLE [dbo].[USR_HistoriaLogowan] DROP CONSTRAINT [PK_USR_HistoriaLogowan]

--------------------------------------
----------------USR_HistoriaLogowan---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[USR_HistoriaLogowan]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[USR_HistoriaLogowan]
GO

CREATE TABLE [dbo].[USR_HistoriaLogowan] (
   [IDUSR_HistoriaLogowan] [int]  IDENTITY(1,1) NOT NULL
  ,[Login] [varchar] (50) NOT NULL
  ,[AdresZdalny] [varchar] (50) NULL
  ,[Powodzenie] [bit]  NOT NULL
  ,[DataAkcji] [datetime]  NOT NULL
  ,[UserAgent] [varchar] (256) NULL
  ,[SessionToken] [varchar] (32) NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DadaModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO


ALTER TABLE [dbo].[USR_HistoriaLogowan] ADD  CONSTRAINT [PK_USR_HistoriaLogowan] PRIMARY KEY CLUSTERED( IDUSR_HistoriaLogowan ) ON [PRIMARY]
GO