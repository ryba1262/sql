DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------USR_ProfilToRola-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_ProfilToRola_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER USR_ProfilToRola_INSERT
')

EXEC('
CREATE TRIGGER USR_ProfilToRola_INSERT ON ['+@BazaDanych+'].[dbo].[USR_ProfilToRola] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_ProfilToRola'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_ProfilToRola]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_ProfilToRola]
  ,[Org_USR_ProfilID]
  ,[Org_USR_RolaID]
  ,[Org_KluczZdalny]
  ,[Org_Aktywny]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDUSR_ProfilToRola
 ,ins.USR_ProfilID
 ,ins.USR_RolaID
 ,ins.KluczZdalny
 ,ins.Aktywny
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_ProfilToRola_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER USR_ProfilToRola_UPDATE
')

EXEC('
CREATE TRIGGER USR_ProfilToRola_UPDATE ON ['+@BazaDanych+'].[dbo].[USR_ProfilToRola] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_ProfilToRola'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_ProfilToRola]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_ProfilToRola]
  ,[Org_USR_ProfilID]
  ,[Org_USR_RolaID]
  ,[Org_KluczZdalny]
  ,[Org_Aktywny]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDUSR_ProfilToRola
 ,ins.USR_ProfilID
 ,ins.USR_RolaID
 ,ins.KluczZdalny
 ,ins.Aktywny
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_ProfilToRola_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER USR_ProfilToRola_DELETE
')

EXEC('
CREATE TRIGGER USR_ProfilToRola_DELETE ON ['+@BazaDanych+'].[dbo].[USR_ProfilToRola] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_ProfilToRola'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_ProfilToRola]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_ProfilToRola]
  ,[Org_USR_ProfilID]
  ,[Org_USR_RolaID]
  ,[Org_KluczZdalny]
  ,[Org_Aktywny]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDUSR_ProfilToRola
 ,del.USR_ProfilID
 ,del.USR_RolaID
 ,del.KluczZdalny
 ,del.Aktywny
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'USR_ProfilToRola')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('USR_ProfilToRola', 1, 0,GetDate())

GO