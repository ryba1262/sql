-- =============================================
-- Author:		Agata Stelcer-Matuszczak
-- Create date: 2018-02-05
-- Description:	Dodanie kolumny "Kolor" do tabeli FormaZajec
-- =============================================



--------------------------------------
----------------FormaZajec---------------
-- Należy zweryfikować constrainty--
--------------------------------------
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RPRozliczenieKolejnosc_FormaZajec]') AND parent_object_id = OBJECT_ID(N'[dbo].[RPRozliczenieKolejnosc]'))
ALTER TABLE [dbo].[RPRozliczenieKolejnosc] DROP CONSTRAINT [FK_RPRozliczenieKolejnosc_FormaZajec]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RPWniosekPoz_FormaZajec]') AND parent_object_id = OBJECT_ID(N'[dbo].[RPWniosekPoz]'))
ALTER TABLE [dbo].[RPWniosekPoz] DROP CONSTRAINT [FK_RPWniosekPoz_FormaZajec]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[FormaZajec]') AND name = N'PK_FormaZajec')
ALTER TABLE [dbo].[FormaZajec] DROP CONSTRAINT [PK_FormaZajec]


if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[FormaZajec]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[FormaZajec] (
   [IDFormaZajec] [int]  IDENTITY(1,1) NOT NULL
  ,[Symbol] [varchar] (2) NULL
  ,[Nazwa] [varchar] (50) NOT NULL
  ,[Opis] [varchar] (200) NULL
  ,[Symbol6Znakow] [varchar] (6) NULL
  ,[CzyDrukowacNaKarcieEgz] [bit]  NOT NULL DEFAULT ((1))
  ,[Kolor] [varchar] (10) NULL
  ,[DataModyfikacji] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Dodal] [int]  NOT NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[FormaZajec] WITH NOCHECK ADD 
CONSTRAINT [PK_FormaZajec] PRIMARY KEY  CLUSTERED
(
  IDFormaZajec
) ON [PRIMARY]


End
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_FormaZajec] (
   [IDFormaZajec] [int]  IDENTITY(1,1) NOT NULL
  ,[Symbol] [varchar] (2) NULL
  ,[Nazwa] [varchar] (50) NOT NULL
  ,[Opis] [varchar] (200) NULL
  ,[Symbol6Znakow] [varchar] (6) NULL
  ,[CzyDrukowacNaKarcieEgz] [bit]  NOT NULL DEFAULT ((1))
  ,[Kolor] [varchar] (10) NULL
  ,[DataModyfikacji] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Dodal] [int]  NOT NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[Tmp_FormaZajec] WITH NOCHECK ADD 
CONSTRAINT [PK_FormaZajec] PRIMARY KEY  CLUSTERED
(
  IDFormaZajec
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_FormaZajec ON

IF EXISTS (SELECT * FROM dbo.FormaZajec)
EXEC('INSERT INTO dbo.Tmp_FormaZajec (IDFormaZajec, Symbol, Nazwa, Opis, Symbol6Znakow, CzyDrukowacNaKarcieEgz, DataModyfikacji, Zmodyfikowal, DataDodania, Dodal)
  SELECT IDFormaZajec, Symbol, Nazwa, Opis, Symbol6Znakow, CzyDrukowacNaKarcieEgz, DataModyfikacji, Zmodyfikowal, DataDodania, Dodal FROM dbo.FormaZajec WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_FormaZajec OFF

DROP TABLE dbo.FormaZajec

EXECUTE sp_rename N'dbo.Tmp_FormaZajec', N'FormaZajec', 'OBJECT'


END
ALTER TABLE [dbo].[RPWniosekPoz] WITH NOCHECK ADD CONSTRAINT [FK_RPWniosekPoz_FormaZajec] FOREIGN KEY([FormaZajecID]) REFERENCES [dbo].[FormaZajec] ([IDFormaZajec])

ALTER TABLE [dbo].[RPRozliczenieKolejnosc] WITH NOCHECK ADD CONSTRAINT [FK_RPRozliczenieKolejnosc_FormaZajec] FOREIGN KEY([FormaZajecID]) REFERENCES [dbo].[FormaZajec] ([IDFormaZajec])


