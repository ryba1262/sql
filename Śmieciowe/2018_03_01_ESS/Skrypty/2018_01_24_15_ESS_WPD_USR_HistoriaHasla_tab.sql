IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_USR_HistoriaHasla_USR_Profil]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_HistoriaHasla]'))
ALTER TABLE [dbo].[USR_HistoriaHasla] DROP CONSTRAINT [FK_USR_HistoriaHasla_USR_Profil]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[USR_HistoriaHasla]') AND name = N'PK_USR_HistoriaHasla')
ALTER TABLE [dbo].[USR_HistoriaHasla] DROP CONSTRAINT [PK_USR_HistoriaHasla]

--------------------------------------
----------------USR_HistoriaHasla---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[USR_HistoriaHasla]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[USR_HistoriaHasla]
GO

CREATE TABLE [dbo].[USR_HistoriaHasla] (
   [IDUSR_HistoriaHasla] [int]  IDENTITY(1,1) NOT NULL
  ,[USR_ProfilID] [int]  NULL
  ,[HasloPlain] [varchar] (128) NOT NULL
  ,[HasloHash] [varchar] (160) NOT NULL
  ,[ZiarnoHasla] [varchar] (32) NOT NULL
  ,[IP] [varchar] (39) NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO



ALTER TABLE [dbo].[USR_HistoriaHasla] ADD  CONSTRAINT [PK_USR_HistoriaHasla] PRIMARY KEY CLUSTERED( IDUSR_HistoriaHasla ) ON [PRIMARY]

ALTER TABLE [dbo].[USR_HistoriaHasla] WITH NOCHECK ADD CONSTRAINT [FK_USR_HistoriaHasla_USR_Profil] FOREIGN KEY([USR_ProfilID]) REFERENCES [dbo].[USR_Profil] ([IDUSR_Profil])
GO