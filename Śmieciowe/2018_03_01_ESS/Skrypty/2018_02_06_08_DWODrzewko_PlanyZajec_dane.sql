delete from DWOPole where IDDWOPole between 80000 and 80010
delete from DWOWariant where IDDWOWariant between 80000 and 80010
delete from DWOJoin where IDDWOJoin between 80000 and 80011
delete from DWOWystepowanie where IDDWOWystepowanie = 80000




insert into DWOWystepowanie (IDDWOWystepowanie,MiejsceWystepowania,RegKeyName,MaxLiczbaKolumn,LP,CzySprawdzacUprawnienia,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80000,'PlanyZajec','PlanyZajec',30,1,0,452,getdate(),NULL,NULL)
 
 insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80001,80000,'PlanZajec','from PlanZajec',NULL,0,452,getdate(),NULL,NULL) 

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80002,80000,'Sekcja','left join Sekcja on PlanZajec.SekcjaID=IDSekcja',80001,1,452,getdate(),NULL,NULL) 

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80003,80000,'Siatka','left join Siatka on PlanZajec.SiatkaID = IDSiatka',80001,1,452,getdate(),NULL,NULL) 

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80004,80000,'TrybStudiow','left join TrybStudiow on Siatka.TrybStudiowID = IDTrybStudiow',80003,2,452,getdate(),NULL,NULL) 

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80005,80000,'Program','left join Program on Siatka.ProgramID = IDProgram',80003,2,452,getdate(),NULL,NULL) 
  
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80006,80000,'Kierunek','left join Kierunek on Program.KierunekID = IDKierunek',80005,3,452,getdate(),NULL,NULL) 
 
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80007,80000,'SystemStudiow','left join SystemStudiow on Program.SystemStudiowID = IDSystemStudiow',80005,3,452,getdate(),NULL,NULL) 
 
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80009,80000,'Wydzial','left join Wydzial on Kierunek.WydzialID = IDWydzial',80006,4,452,getdate(),NULL,NULL)  
 
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80010,80000,'SemestrAkad','left join OkresRozliczeniowyPoz SemestrAkad on PlanZajec.SemestrID = SemestrAkad.IDOkresRozliczeniowyPoz',80001,1,452,getdate(),NULL,NULL)  
 
  
 
INSERT INTO DWOWariant(IDDWOWariant,DWOJoinID,Nazwa,Opis,Warunek,CzyPelnyWarunek,CzySprawdzacUprawnienia,LP,Wymagany,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
  values(80001,80007,'Bez podyplomowych','Bez podyplomowych','SystemStudiow.CzyPodyplomowe = 0 ',1,0,1,0,452,GetDate(),NULL,NULL)

INSERT INTO DWOWariant(IDDWOWariant,DWOJoinID,Nazwa,Opis,Warunek,CzyPelnyWarunek,CzySprawdzacUprawnienia,LP,Wymagany,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
  values(80002,80007,'Podyplomowe','Podyplomowe','SystemStudiow.CzyPodyplomowe = 1 ',1,0,2,0,452,GetDate(),NULL,NULL)

insert into DWOWariant (IDDWOWariant,DWOJoinID,Nazwa,Opis,Warunek,CzyPelnyWarunek,CzySprawdzacUprawnienia,LP,Wymagany,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80003,80001,'Wszystkie plany','Wszystkie plany zajęć','1 = 1',1,0,3,0,452,getdate(),NULL,NULL) 


insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80001,80001,'IDPlanZajec','IDPLANZAJEC','PlanZajec.IDPlanZajec',NULL,'PlanZajec.IDPlanZajec',NULL,1,0,1,1,1,NULL,NULL,452,getdate(),NULL,NULL) 

insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80002,80001,'Rok akad',NULL,'cast(PlanZajec.RokAkad as varchar(4)) + ''/'' + cast((PlanZajec.RokAkad+1) as varchar(4))',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL) 
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80003,80010,'Semestr','SEMESTRID','SemestrAkad.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)  

insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80004,80002,'Sekcja','SEKCJA','Sekcja.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL) 

insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80005,80004,'Tryb studiów','TRYBSTUDIOW','TrybStudiow.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80006,80007,'System studiów','SYSTEMSTUDIOW','SystemStudiow.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80007,80009,'Kierunek','KIERUNEK','''<'' + Wydzial.Symbol  + ''> '' + Kierunek.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)     
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80008,80009,'Semestr studiów','SEMESTRSTUDIOW','PlanZajec.SemestrStudiow',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)        