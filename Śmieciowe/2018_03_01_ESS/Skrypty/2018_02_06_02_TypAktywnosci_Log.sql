
-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------TypUczestnictwa---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_TypUczestnictwa]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_TypUczestnictwa] (
   [Log_TypUczestnictwaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDTypUczestnictwa] [int] 
  ,[Org_NazwaTypu] [varchar] (256)
  ,[Org_Opis] [varchar] (500)
  ,[Org_CzyFormaWymiar] [bit] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_TypUczestnictwa] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_TypUczestnictwa] PRIMARY KEY  CLUSTERED
(
   [Log_TypUczestnictwaID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_TypUczestnictwa] (
   [Log_TypUczestnictwaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDTypUczestnictwa] [int] 
  ,[Org_NazwaTypu] [varchar] (256)
  ,[Org_Opis] [varchar] (500)
  ,[Org_CzyFormaWymiar] [bit] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_TypUczestnictwa ON

IF EXISTS (SELECT * FROM dbo.LOG_TypUczestnictwa)
EXEC('INSERT INTO dbo.Tmp_LOG_TypUczestnictwa (Log_TypUczestnictwaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDTypUczestnictwa, Org_NazwaTypu, Org_Opis, Org_CzyFormaWymiar, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_TypUczestnictwaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDTypUczestnictwa, Org_NazwaTypu, Org_Opis, Org_CzyFormaWymiar, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_TypUczestnictwa WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_TypUczestnictwa OFF

DROP TABLE dbo.LOG_TypUczestnictwa

EXECUTE sp_rename N'dbo.Tmp_LOG_TypUczestnictwa', N'LOG_TypUczestnictwa', 'OBJECT'

ALTER TABLE [dbo].[LOG_TypUczestnictwa] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_TypUczestnictwa] PRIMARY KEY  CLUSTERED
(
   [Log_TypUczestnictwaID]
) ON [PRIMARY]

END
GO

