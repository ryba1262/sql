DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''PlanZajec_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER PlanZajec_INSERT
')

EXEC('
CREATE TRIGGER PlanZajec_INSERT ON ['+@BazaDanych+'].[dbo].[PlanZajec] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''PlanZajec'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_PlanZajec]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDPlanZajec]
  ,[Org_SiatkaID]
  ,[Org_SekcjaID]
  ,[Org_Opis]
  ,[Org_RokAkad]
  ,[Org_SemestrStudiow]
  ,[Org_ZjazdID]
  ,[Org_SemestrID]
  ,[Org_PrzerwaPoXGodz]
  ,[Org_DataDodania]
  ,[Org_Dodal]
  ,[Org_DataModyfikacji]
  ,[Org_Zmodyfikowal]
  ,[Org_Dokladnosc]
  ,[Org_GodzOd]
  ,[Org_GodzDo]
  ,[Org_CzasPrzerwy]
  ,[Org_CzasZajec]
  ,[Org_StatusAktywny]
  ,[Org_StatusAktywnyDydaktyk]
  ,[Org_EdycjaSesjaID]
)
SELECT
1
  ,@ID
 ,ins.IDPlanZajec
 ,ins.SiatkaID
 ,ins.SekcjaID
 ,ins.Opis
 ,ins.RokAkad
 ,ins.SemestrStudiow
 ,ins.ZjazdID
 ,ins.SemestrID
 ,ins.PrzerwaPoXGodz
 ,ins.DataDodania
 ,ins.Dodal
 ,ins.DataModyfikacji
 ,ins.Zmodyfikowal
 ,ins.Dokladnosc
 ,ins.GodzOd
 ,ins.GodzDo
 ,ins.CzasPrzerwy
 ,ins.CzasZajec
 ,ins.StatusAktywny
 ,ins.StatusAktywnyDydaktyk
 ,ins.EdycjaSesjaID
FROM  inserted ins 
END
END
')

EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''PlanZajec_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER PlanZajec_UPDATE
')

EXEC('
CREATE TRIGGER PlanZajec_UPDATE ON ['+@BazaDanych+'].[dbo].[PlanZajec] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''PlanZajec'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_PlanZajec]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDPlanZajec]
  ,[Org_SiatkaID]
  ,[Org_SekcjaID]
  ,[Org_Opis]
  ,[Org_RokAkad]
  ,[Org_SemestrStudiow]
  ,[Org_ZjazdID]
  ,[Org_SemestrID]
  ,[Org_PrzerwaPoXGodz]
  ,[Org_DataDodania]
  ,[Org_Dodal]
  ,[Org_DataModyfikacji]
  ,[Org_Zmodyfikowal]
  ,[Org_Dokladnosc]
  ,[Org_GodzOd]
  ,[Org_GodzDo]
  ,[Org_CzasPrzerwy]
  ,[Org_CzasZajec]
  ,[Org_StatusAktywny]
  ,[Org_StatusAktywnyDydaktyk]
  ,[Org_EdycjaSesjaID]
)
SELECT
2
  ,@ID
 ,ins.IDPlanZajec
 ,ins.SiatkaID
 ,ins.SekcjaID
 ,ins.Opis
 ,ins.RokAkad
 ,ins.SemestrStudiow
 ,ins.ZjazdID
 ,ins.SemestrID
 ,ins.PrzerwaPoXGodz
 ,ins.DataDodania
 ,ins.Dodal
 ,ins.DataModyfikacji
 ,ins.Zmodyfikowal
 ,ins.Dokladnosc
 ,ins.GodzOd
 ,ins.GodzDo
 ,ins.CzasPrzerwy
 ,ins.CzasZajec
 ,ins.StatusAktywny
 ,ins.StatusAktywnyDydaktyk
 ,ins.EdycjaSesjaID
FROM  inserted ins 
END
END
')

EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''PlanZajec_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER PlanZajec_DELETE
')

EXEC('
CREATE TRIGGER PlanZajec_DELETE ON ['+@BazaDanych+'].[dbo].[PlanZajec] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''PlanZajec'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_PlanZajec]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDPlanZajec]
  ,[Org_SiatkaID]
  ,[Org_SekcjaID]
  ,[Org_Opis]
  ,[Org_RokAkad]
  ,[Org_SemestrStudiow]
  ,[Org_ZjazdID]
  ,[Org_SemestrID]
  ,[Org_PrzerwaPoXGodz]
  ,[Org_DataDodania]
  ,[Org_Dodal]
  ,[Org_DataModyfikacji]
  ,[Org_Zmodyfikowal]
  ,[Org_Dokladnosc]
  ,[Org_GodzOd]
  ,[Org_GodzDo]
  ,[Org_CzasPrzerwy]
  ,[Org_CzasZajec]
  ,[Org_StatusAktywny]
  ,[Org_StatusAktywnyDydaktyk]
  ,[Org_EdycjaSesjaID]
)
SELECT
3
  ,@ID
 ,del.IDPlanZajec
 ,del.SiatkaID
 ,del.SekcjaID
 ,del.Opis
 ,del.RokAkad
 ,del.SemestrStudiow
 ,del.ZjazdID
 ,del.SemestrID
 ,del.PrzerwaPoXGodz
 ,del.DataDodania
 ,del.Dodal
 ,del.DataModyfikacji
 ,del.Zmodyfikowal
 ,del.Dokladnosc
 ,del.GodzOd
 ,del.GodzDo
 ,del.CzasPrzerwy
 ,del.CzasZajec
 ,del.StatusAktywny
 ,del.StatusAktywnyDydaktyk
 ,del.EdycjaSesjaID
FROM  deleted del 
END
END
')

if not exists (select * from LOG_LogowanaTabela where Tabela = 'PlanZajec')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('PlanZajec', 1, 0,GetDate())
GO

