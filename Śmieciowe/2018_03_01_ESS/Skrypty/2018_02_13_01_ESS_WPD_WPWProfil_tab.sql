--------------------------------------
----------------USR_Profil---------------
-- Należy zweryfikować constrainty--
--------------------------------------
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__USR_Histo__USR_P__6FE211CD]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_HistoriaAkcji]'))
ALTER TABLE [dbo].[USR_HistoriaAkcji] DROP CONSTRAINT [FK__USR_Histo__USR_P__6FE211CD]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__USR_Profi__USR_P__6B1D5CB0]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_ProfilToRola]'))
ALTER TABLE [dbo].[USR_ProfilToRola] DROP CONSTRAINT [FK__USR_Profi__USR_P__6B1D5CB0]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_USR_HistoriaHasla_USR_Profil]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_HistoriaHasla]'))
ALTER TABLE [dbo].[USR_HistoriaHasla] DROP CONSTRAINT [FK_USR_HistoriaHasla_USR_Profil]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[USR_Profil]') AND name = N'PK_USR_Profil')
ALTER TABLE [dbo].[USR_Profil] DROP CONSTRAINT [PK_USR_Profil]


if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[USR_Profil]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[USR_Profil] (
   [IDUSR_Profil] [int]  IDENTITY(1000000,1) NOT NULL
  ,[Email] [varchar] (254) NULL
  ,[Login] [varchar] (50) NULL
  ,[HasloPlain] [varchar] (128) NULL
  ,[HasloHash] [varchar] (160) NULL
  ,[ZiarnoHasla] [varchar] (32) NULL
  ,[Imie] [varchar] (50) NOT NULL
  ,[DrugieImie] [varchar] (50) NULL
  ,[NickName] [varchar] (50) NULL
  ,[Nazwisko] [varchar] (50) NOT NULL
  ,[PrefiksTelefonu] [varchar] (5) NULL
  ,[Telefon] [varchar] (20) NULL
  ,[CzyHasloJednorazowe] [bit]  NULL
  ,[CzyKontoAktywne] [bit]  NOT NULL DEFAULT ((0))
  ,[DataOstatniejZmianyHasla] [datetime]  NULL
  ,[Token] [uniqueidentifier]  NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[USR_Profil] WITH NOCHECK ADD 
CONSTRAINT [PK_USR_Profil] PRIMARY KEY  CLUSTERED
(
  IDUSR_Profil
) ON [PRIMARY]


End
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_USR_Profil] (
   [IDUSR_Profil] [int]  IDENTITY(1000000,1) NOT NULL
  ,[Email] [varchar] (254) NULL
  ,[Login] [varchar] (50) NULL
  ,[HasloPlain] [varchar] (128) NULL
  ,[HasloHash] [varchar] (160) NULL
  ,[ZiarnoHasla] [varchar] (32) NULL
  ,[Imie] [varchar] (50) NOT NULL
  ,[DrugieImie] [varchar] (50) NULL
  ,[NickName] [varchar] (50) NULL
  ,[Nazwisko] [varchar] (50) NOT NULL
  ,[PrefiksTelefonu] [varchar] (5) NULL
  ,[Telefon] [varchar] (20) NULL
  ,[CzyHasloJednorazowe] [bit]  NULL
  ,[CzyKontoAktywne] [bit]  NOT NULL DEFAULT ((0))
  ,[DataOstatniejZmianyHasla] [datetime]  NULL
  ,[Token] [uniqueidentifier]  NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[Tmp_USR_Profil] WITH NOCHECK ADD 
CONSTRAINT [PK_USR_Profil] PRIMARY KEY  CLUSTERED
(
  IDUSR_Profil
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_USR_Profil ON

IF EXISTS (SELECT * FROM dbo.USR_Profil)
EXEC('INSERT INTO dbo.Tmp_USR_Profil (IDUSR_Profil, Email, Login, HasloPlain, HasloHash, ZiarnoHasla, Imie, DrugieImie, NickName, Nazwisko, PrefiksTelefonu, Telefon, CzyHasloJednorazowe, CzyKontoAktywne, DataOstatniejZmianyHasla, Token, Dodal, DataDodania, Zmodyfikowal, DataModyfikacji)
  SELECT IDUSR_Profil, Email, Login, HasloPlain, HasloHash, ZiarnoHasla, Imie, DrugieImie, NickName, Nazwisko, PrefiksTelefonu, Telefon, CzyHasloJednorazowe, CzyKontoAktywne, DataOstatniejZmianyHasla, Token, Dodal, DataDodania, Zmodyfikowal, DataModyfikacji FROM dbo.USR_Profil WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_USR_Profil OFF

DROP TABLE dbo.USR_Profil

EXECUTE sp_rename N'dbo.Tmp_USR_Profil', N'USR_Profil', 'OBJECT'


END
ALTER TABLE [dbo].[USR_HistoriaHasla] WITH NOCHECK ADD CONSTRAINT [FK_USR_HistoriaHasla_USR_Profil] FOREIGN KEY([USR_ProfilID]) REFERENCES [dbo].[USR_Profil] ([IDUSR_Profil])

ALTER TABLE [dbo].[USR_ProfilToRola] WITH NOCHECK ADD CONSTRAINT [FK__USR_Profi__USR_P__6B1D5CB0] FOREIGN KEY([USR_ProfilID]) REFERENCES [dbo].[USR_Profil] ([IDUSR_Profil])

ALTER TABLE [dbo].[USR_HistoriaAkcji] WITH NOCHECK ADD CONSTRAINT [FK__USR_Histo__USR_P__6FE211CD] FOREIGN KEY([USR_ProfilID]) REFERENCES [dbo].[USR_Profil] ([IDUSR_Profil])


