
-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------USR_UprawnianiaDefinicje---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_USR_UprawnianiaDefinicje]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_USR_UprawnianiaDefinicje] (
   [Log_USR_UprawnianiaDefinicjeID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_UprawnianiaDefinicje] [int] 
  ,[Org_Adres] [varchar] (250)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_USR_UprawnianiaDefinicje] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_USR_UprawnianiaDefinicje] PRIMARY KEY  CLUSTERED
(
   [Log_USR_UprawnianiaDefinicjeID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_USR_UprawnianiaDefinicje] (
   [Log_USR_UprawnianiaDefinicjeID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_UprawnianiaDefinicje] [int] 
  ,[Org_Adres] [varchar] (250)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_UprawnianiaDefinicje ON

IF EXISTS (SELECT * FROM dbo.LOG_USR_UprawnianiaDefinicje)
EXEC('INSERT INTO dbo.Tmp_LOG_USR_UprawnianiaDefinicje (Log_USR_UprawnianiaDefinicjeID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_UprawnianiaDefinicje, Org_Adres, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_USR_UprawnianiaDefinicjeID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_UprawnianiaDefinicje, Org_Adres, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_USR_UprawnianiaDefinicje WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_UprawnianiaDefinicje OFF

DROP TABLE dbo.LOG_USR_UprawnianiaDefinicje

EXECUTE sp_rename N'dbo.Tmp_LOG_USR_UprawnianiaDefinicje', N'LOG_USR_UprawnianiaDefinicje', 'OBJECT'

ALTER TABLE [dbo].[LOG_USR_UprawnianiaDefinicje] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_USR_UprawnianiaDefinicje] PRIMARY KEY  CLUSTERED
(
   [Log_USR_UprawnianiaDefinicjeID]
) ON [PRIMARY]

END
GO

