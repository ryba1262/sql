-------------------wdro1----------------------2018_02_05_04_FormaZajecNowaKolumna_DANE
-------------------wdro2----------------------2018_02_05_08_ESS_PlanZajec_Dane
-------------------wdro3----------------------2018_02_05_09_ESS_WPD_UstawieniaProgramu_dane
-------------------wdro4----------------------2018_02_06_04_TypAktywnosci_Dane
-------------------wdro5----------------------2018_02_06_08_DWODrzewko_PlanyZajec_dane
-------------------wdro6----------------------2018_02_19_01_DWODrzewko_Kursy_dane
-------------------wdro7----------------------2018_02_21_07_ESS_WPD_USR_UprawnieniaDefinicje_dane
-------------------wdro8----------------------2018_02_21_08_ESS_WPD_USR_UprawnieniaToRola_dane
-------------------wdro9----------------------2018_02_23_01_UstawieniaProgramu_GrupyZaj_dane
-------------------wdro10---------------------2018_03_03_01_ESS_WPD_USR_Uprawnienia_dane.sql

----------------------------------------------------wdro1-------------------------------------------------------------


-- =============================================
-- Author:		Agata Stelcer-Matuszczak
-- Create date: 2018-02-05
-- Description:	Wypelnienie kolumny Kolor danymi
-- =============================================

  UPDATE FormaZajec  SET Kolor='#ADEAEA'  where IDFormaZajec=1
  UPDATE FormaZajec  SET Kolor='#D8BFD8'  where IDFormaZajec=2
  UPDATE FormaZajec  SET Kolor='#DB9370'  where IDFormaZajec=3
  UPDATE FormaZajec  SET Kolor='#D9D9F3'  where IDFormaZajec=4
  UPDATE FormaZajec  SET Kolor='#EAEAAE'  where IDFormaZajec=5
  UPDATE FormaZajec  SET Kolor='#cfcfcf'  where IDFormaZajec=6
  UPDATE FormaZajec  SET Kolor='#ffcc99'  where IDFormaZajec=7
  UPDATE FormaZajec  SET Kolor='#ff6666'  where IDFormaZajec=8
  UPDATE FormaZajec  SET Kolor='#cc9999'  where IDFormaZajec=9
  UPDATE FormaZajec  SET Kolor='#ff9966'  where IDFormaZajec=10
  UPDATE FormaZajec  SET Kolor='#ff9933'  where IDFormaZajec=11
  UPDATE FormaZajec  SET Kolor='#cccc99'  where IDFormaZajec=12
  UPDATE FormaZajec  SET Kolor='#cccc66'  where IDFormaZajec=14
  UPDATE FormaZajec  SET Kolor='#cccc33'  where IDFormaZajec=15
  UPDATE FormaZajec  SET Kolor='#ccff99'  where IDFormaZajec=16
  UPDATE FormaZajec  SET Kolor='#99ffcc'  where IDFormaZajec=17
  UPDATE FormaZajec  SET Kolor='#66ffcc'  where IDFormaZajec=18
  UPDATE FormaZajec  SET Kolor='#99cccc'  where IDFormaZajec=19
  UPDATE FormaZajec  SET Kolor='#00ccff'  where IDFormaZajec=20
  UPDATE FormaZajec  SET Kolor='#66ccff'  where IDFormaZajec=22
  UPDATE FormaZajec  SET Kolor='#6699cc'  where IDFormaZajec=23
  UPDATE FormaZajec  SET Kolor='#9999cc'  where IDFormaZajec=26
  UPDATE FormaZajec  SET Kolor='#9999ff'  where IDFormaZajec=27
  UPDATE FormaZajec  SET Kolor='#ccccff'  where IDFormaZajec=32
  UPDATE FormaZajec  SET Kolor='#cc66ff'  where IDFormaZajec=33
  UPDATE FormaZajec  SET Kolor='#cc33ff'  where IDFormaZajec=34
  UPDATE FormaZajec  SET Kolor='#ff9999'  where IDFormaZajec=36
  UPDATE FormaZajec  SET Kolor='#ff99ff'  where IDFormaZajec=37
  UPDATE FormaZajec  SET Kolor='#ffccff'  where IDFormaZajec=38
  UPDATE FormaZajec  SET Kolor='#ffffcc'  where IDFormaZajec=39
  UPDATE FormaZajec  SET Kolor='#999966'  where IDFormaZajec=40
  UPDATE FormaZajec  SET Kolor='#F0CFCC'  where IDFormaZajec=41
  UPDATE FormaZajec  SET Kolor='#E38981'  where IDFormaZajec=42
  UPDATE FormaZajec  SET Kolor='#E3C481'  where IDFormaZajec=43
  UPDATE FormaZajec  SET Kolor='#81E3C6'  where IDFormaZajec=44
  UPDATE FormaZajec  SET Kolor='#9CA8FF'  where IDFormaZajec=45
  UPDATE FormaZajec  SET Kolor='#8795FF'  where IDFormaZajec=46
  UPDATE FormaZajec  SET Kolor='#87F7FF'  where IDFormaZajec=47
  UPDATE FormaZajec  SET Kolor='#FFC7D5'  where IDFormaZajec=48
  UPDATE FormaZajec  SET Kolor='#FDC7FF'  where IDFormaZajec=49
  UPDATE FormaZajec  SET Kolor='#C7FFDF'  where IDFormaZajec=50
  UPDATE FormaZajec  SET Kolor='#FFF9C7'  where IDFormaZajec=51


  go

  ----------------------------------------------------------------wdro2-----------------------------------------------------------


  update PlanZajec
set StatusAktywnyDydaktyk = StatusAktywny

go


---------------------------------------------------------------wdro3--------------------------------------------------------------------



UPDATE [UstawieniaProgramu] SET [WartoscText] = 'http://wpd.t.ahe.lodz.pl/#!/aktywacja_konta/' WHERE Nazwa = 'WPWLink';
GO



-------------------------------------------------------------wdro4----------------------------------------------------------------------


 -- -- TypUczestnictwa -- -- 
DELETE FROM TypUczestnictwa
INSERT INTO [TypUczestnictwa](NazwaTypu,Opis,CzyFormaWymiar,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('standardowy','standardowy',0,452,'2018-02-06',NULL,NULL)
INSERT INTO [TypUczestnictwa](NazwaTypu,Opis,CzyFormaWymiar,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('hybrydowy','hybrydowy',0,452,'2018-02-06',NULL,NULL)
INSERT INTO [TypUczestnictwa](NazwaTypu,Opis,CzyFormaWymiar,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('poprawkowy',NULL,1,452,'2018-02-06',NULL,NULL)
INSERT INTO [TypUczestnictwa](NazwaTypu,Opis,CzyFormaWymiar,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('r�nice programowe',NULL,1,452,'2018-02-06',NULL,NULL)
GO


--------------------------------------------------------wdro5------------------------------------------------------------------


delete from DWOPole where IDDWOPole between 80000 and 80010
delete from DWOWariant where IDDWOWariant between 80000 and 80010
delete from DWOJoin where IDDWOJoin between 80000 and 80011
delete from DWOWystepowanie where IDDWOWystepowanie = 80000




insert into DWOWystepowanie (IDDWOWystepowanie,MiejsceWystepowania,RegKeyName,MaxLiczbaKolumn,LP,CzySprawdzacUprawnienia,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80000,'PlanyZajec','PlanyZajec',30,1,0,452,getdate(),NULL,NULL)
 
 insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80001,80000,'PlanZajec','from PlanZajec',NULL,0,452,getdate(),NULL,NULL) 

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80002,80000,'Sekcja','left join Sekcja on PlanZajec.SekcjaID=IDSekcja',80001,1,452,getdate(),NULL,NULL) 

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80003,80000,'Siatka','left join Siatka on PlanZajec.SiatkaID = IDSiatka',80001,1,452,getdate(),NULL,NULL) 

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80004,80000,'TrybStudiow','left join TrybStudiow on Siatka.TrybStudiowID = IDTrybStudiow',80003,2,452,getdate(),NULL,NULL) 

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80005,80000,'Program','left join Program on Siatka.ProgramID = IDProgram',80003,2,452,getdate(),NULL,NULL) 
  
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80006,80000,'Kierunek','left join Kierunek on Program.KierunekID = IDKierunek',80005,3,452,getdate(),NULL,NULL) 
 
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80007,80000,'SystemStudiow','left join SystemStudiow on Program.SystemStudiowID = IDSystemStudiow',80005,3,452,getdate(),NULL,NULL) 
 
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80009,80000,'Wydzial','left join Wydzial on Kierunek.WydzialID = IDWydzial',80006,4,452,getdate(),NULL,NULL)  
 
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80010,80000,'SemestrAkad','left join OkresRozliczeniowyPoz SemestrAkad on PlanZajec.SemestrID = SemestrAkad.IDOkresRozliczeniowyPoz',80001,1,452,getdate(),NULL,NULL)  
 
  
 
INSERT INTO DWOWariant(IDDWOWariant,DWOJoinID,Nazwa,Opis,Warunek,CzyPelnyWarunek,CzySprawdzacUprawnienia,LP,Wymagany,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
  values(80001,80007,'Bez podyplomowych','Bez podyplomowych','SystemStudiow.CzyPodyplomowe = 0 ',1,0,1,0,452,GetDate(),NULL,NULL)

INSERT INTO DWOWariant(IDDWOWariant,DWOJoinID,Nazwa,Opis,Warunek,CzyPelnyWarunek,CzySprawdzacUprawnienia,LP,Wymagany,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
  values(80002,80007,'Podyplomowe','Podyplomowe','SystemStudiow.CzyPodyplomowe = 1 ',1,0,2,0,452,GetDate(),NULL,NULL)

insert into DWOWariant (IDDWOWariant,DWOJoinID,Nazwa,Opis,Warunek,CzyPelnyWarunek,CzySprawdzacUprawnienia,LP,Wymagany,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80003,80001,'Wszystkie plany','Wszystkie plany zaj��','1 = 1',1,0,3,0,452,getdate(),NULL,NULL) 


insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80001,80001,'IDPlanZajec','IDPLANZAJEC','PlanZajec.IDPlanZajec',NULL,'PlanZajec.IDPlanZajec',NULL,1,0,1,1,1,NULL,NULL,452,getdate(),NULL,NULL) 

insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80002,80001,'Rok akad',NULL,'cast(PlanZajec.RokAkad as varchar(4)) + ''/'' + cast((PlanZajec.RokAkad+1) as varchar(4))',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL) 
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80003,80010,'Semestr','SEMESTRID','SemestrAkad.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)  

insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80004,80002,'Sekcja','SEKCJA','Sekcja.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL) 

insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80005,80004,'Tryb studi�w','TRYBSTUDIOW','TrybStudiow.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80006,80007,'System studi�w','SYSTEMSTUDIOW','SystemStudiow.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80007,80009,'Kierunek','KIERUNEK','''<'' + Wydzial.Symbol  + ''> '' + Kierunek.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)     
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (80008,80009,'Semestr studi�w','SEMESTRSTUDIOW','PlanZajec.SemestrStudiow',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)        

 go

 ----------------------------------------------------------------------wdro6--------------------------------------------------------------------------


 delete from DWOPole where IDDWOPole between 84000 and 84010
delete from DWOWariant where IDDWOWariant between 84000 and 84010
delete from DWOJoin where IDDWOJoin between 84000 and 84012
delete from DWOWystepowanie where IDDWOWystepowanie = 84000


insert into DWOWystepowanie (IDDWOWystepowanie,MiejsceWystepowania,RegKeyName,MaxLiczbaKolumn,LP,CzySprawdzacUprawnienia,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84000,'Kursy','Kursy',30,1,0,452,getdate(),NULL,NULL)


insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84001,84000,'Kurs','from Kurs',NULL,0,452,getdate(),NULL,NULL) 

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84002,84000,'Sekcja','left join Sekcja on Kurs.SekcjaID=IDSekcja',84001,1,452,getdate(),NULL,NULL) 

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84003,84000,'Siatka','left join Siatka on Kurs.SiatkaID = IDSiatka',84001,1,452,getdate(),NULL,NULL) 

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84004,84000,'TrybStudiow','left join TrybStudiow on Siatka.TrybStudiowID = IDTrybStudiow',84003,2,452,getdate(),NULL,NULL) 

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84005,84000,'Program','left join Program on Siatka.ProgramID = IDProgram',84003,2,452,getdate(),NULL,NULL) 
  
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84006,84000,'Kierunek','left join Kierunek on Program.KierunekID = IDKierunek',84005,3,452,getdate(),NULL,NULL) 
 
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84007,84000,'SystemStudiow','left join SystemStudiow on Program.SystemStudiowID = IDSystemStudiow',84005,3,452,getdate(),NULL,NULL) 
 
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84009,84000,'Wydzial','left join Wydzial on Kierunek.WydzialID = IDWydzial',84006,4,452,getdate(),NULL,NULL)  
 
insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84010,84000,'SemestrAkad','left join OkresRozliczeniowyPoz SemestrAkad on Kurs.SemestrID = SemestrAkad.IDOkresRozliczeniowyPoz',84001,1,452,getdate(),NULL,NULL)  

insert into DWOJoin (IDDWOJoin,DWOWystepowanieID,Alias,DefinicjaJoina,RodzicID,Priorytet,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84011,84000,'KursPoz','join KursPoz on KursPoz.KursID = Kurs.IDKurs',84001,1,452,getdate(),NULL,NULL)  
 

INSERT INTO DWOWariant(IDDWOWariant,DWOJoinID,Nazwa,Opis,Warunek,CzyPelnyWarunek,CzySprawdzacUprawnienia,LP,Wymagany,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
  values(84001,84007,'Bez podyplomowych','Bez podyplomowych','SystemStudiow.CzyPodyplomowe = 0 ',1,0,1,0,452,GetDate(),NULL,NULL)

insert into DWOWariant (IDDWOWariant,DWOJoinID,Nazwa,Opis,Warunek,CzyPelnyWarunek,CzySprawdzacUprawnienia,LP,Wymagany,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84002,84001,'Wszystkie kursy','Wszystkie kursy','1 = 1',1,0,2,0,452,getdate(),NULL,NULL) 
 
INSERT INTO DWOWariant(IDDWOWariant,DWOJoinID,Nazwa,Opis,Warunek,CzyPelnyWarunek,CzySprawdzacUprawnienia,LP,Wymagany,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
  values(84003,84007,'Podyplomowe','Podyplomowe','SystemStudiow.CzyPodyplomowe = 1 ',1,0,3,0,452,GetDate(),NULL,NULL)


insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84001,84001,'IDKurs','IDKURS','Kurs.IDKurs',NULL,'Kurs.IDKurs',NULL,1,0,1,1,1,NULL,NULL,452,getdate(),NULL,NULL) 

insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84002,84001,'Rok akad',NULL,'cast(Kurs.RokAkad as varchar(4)) + ''/'' + cast((Kurs.RokAkad+1) as varchar(4))',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL) 
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84003,84010,'Semestr','SEMESTRID','SemestrAkad.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)  

insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84004,84002,'Sekcja','SEKCJA','Sekcja.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL) 

insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84005,84004,'Tryb studi�w','TRYBSTUDIOW','TrybStudiow.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84006,84007,'System studi�w','SYSTEMSTUDIOW','SystemStudiow.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)   
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84007,84009,'Kierunek','KIERUNEK','''<'' + Wydzial.Symbol  + ''> '' + Kierunek.Nazwa',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)     
 
insert into DWOPole (IDDWOPole,DWOJoinID,NazwaPola,Sygnatura,DefinicjaPola,NazwaPolaSzablon,Klucz,OrderByField,Drzewko,WymaganeDrzewko,Wyswietlane,WymaganeWyswietlane,CzyUprawnienia,TypPola,TypKlucza,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji)
 values (84008,84011,'Semestr studi�w','SEMESTRSTUDIOW','KursPoz.Semestr',NULL,NULL,NULL,1,0,1,0,0,NULL,NULL,452,getdate(),NULL,NULL)       
 
  
  go

  -----------------------------------------------------------------------------wdro7-----------------------------------------------------------------


  
 -- -- USR_UprawnianiaDefinicje -- -- 
DELETE FROM USR_UprawnianiaDefinicje
INSERT INTO [USR_UprawnianiaDefinicje](Adres,Opis,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('/api/ProfilPracownika/GetProfilPracownika',NULL,0,'2018-02-19',NULL,NULL)
INSERT INTO [USR_UprawnianiaDefinicje](Adres,Opis,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('/api/ProfilPracownika/POSTZapiszZdjecie',NULL,0,'2018-02-20',NULL,NULL)
INSERT INTO [USR_UprawnianiaDefinicje](Adres,Opis,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('/api/Semestr/GetAktualnySemestr',NULL,0,'2018-02-21',NULL,NULL)
INSERT INTO [USR_UprawnianiaDefinicje](Adres,Opis,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('/api/Semestr/GetListaSemestrow',NULL,0,'2018-02-21',NULL,NULL)
INSERT INTO [USR_UprawnianiaDefinicje](Adres,Opis,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('/api/Student/GetStudent',NULL,0,'2018-02-21',NULL,NULL)
INSERT INTO [USR_UprawnianiaDefinicje](Adres,Opis,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('/api/Student/GetIndeks',NULL,0,'2018-02-21',NULL,NULL)
INSERT INTO [USR_UprawnianiaDefinicje](Adres,Opis,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('/api/ListaGrupy/GetListaGrupy',NULL,0,'2018-02-21',NULL,NULL)
INSERT INTO [USR_UprawnianiaDefinicje](Adres,Opis,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('/api/PlanWykladowcy/GetPlanRoczny',NULL,0,'2018-02-21',NULL,NULL)
INSERT INTO [USR_UprawnianiaDefinicje](Adres,Opis,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('/api/PlanWykladowcy/GetPlanSzczegolowy',NULL,0,'2018-02-21',NULL,NULL)
INSERT INTO [USR_UprawnianiaDefinicje](Adres,Opis,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('/api/PlanWykladowcy/GetGrupaZajeciowa',NULL,0,'2018-02-21',NULL,NULL)
INSERT INTO [USR_UprawnianiaDefinicje](Adres,Opis,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('/api/PlanWykladowcy/GetHarmonogram',NULL,0,'2018-02-21',NULL,NULL)
INSERT INTO [USR_UprawnianiaDefinicje](Adres,Opis,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('/api/PlanWykladowcy/GetListyJednolite',NULL,0,'2018-02-21',NULL,NULL)
INSERT INTO [USR_UprawnianiaDefinicje](Adres,Opis,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('/api/PlanWykladowcy/GetNazwaFormyPrzedmiotu',NULL,0,'2018-02-21',NULL,NULL)
INSERT INTO [USR_UprawnianiaDefinicje](Adres,Opis,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('/api/PlanWykladowcy/GetNazwaPrzedmiotu',NULL,0,'2018-02-21',NULL,NULL)
INSERT INTO [USR_UprawnianiaDefinicje](Adres,Opis,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values('/api/Uprawnienia/GetUprawnienia',NULL,0,'2018-02-21',NULL,NULL)
GO




-----------------------------------------------------------------------wdro8----------------------------------------------------------------------------



 -- -- USR_UprawnianiaToRola -- -- 
DELETE FROM USR_UprawnianiaToRola
INSERT INTO [USR_UprawnianiaToRola](USR_RolaID,USR_UprawnieniaDefinicjeID,CzyMozeWykonac,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(1,1,1,0,'2018-02-21',NULL,NULL)
INSERT INTO [USR_UprawnianiaToRola](USR_RolaID,USR_UprawnieniaDefinicjeID,CzyMozeWykonac,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(1,2,1,0,'2018-02-21',NULL,NULL)
INSERT INTO [USR_UprawnianiaToRola](USR_RolaID,USR_UprawnieniaDefinicjeID,CzyMozeWykonac,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(1,3,1,0,'2018-02-21',NULL,NULL)
INSERT INTO [USR_UprawnianiaToRola](USR_RolaID,USR_UprawnieniaDefinicjeID,CzyMozeWykonac,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(1,4,1,0,'2018-02-21',NULL,NULL)
INSERT INTO [USR_UprawnianiaToRola](USR_RolaID,USR_UprawnieniaDefinicjeID,CzyMozeWykonac,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(1,5,1,0,'2018-02-21',NULL,NULL)
INSERT INTO [USR_UprawnianiaToRola](USR_RolaID,USR_UprawnieniaDefinicjeID,CzyMozeWykonac,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(1,6,1,0,'2018-02-21',NULL,NULL)
INSERT INTO [USR_UprawnianiaToRola](USR_RolaID,USR_UprawnieniaDefinicjeID,CzyMozeWykonac,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(1,7,1,0,'2018-02-21',NULL,NULL)
INSERT INTO [USR_UprawnianiaToRola](USR_RolaID,USR_UprawnieniaDefinicjeID,CzyMozeWykonac,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(1,8,1,0,'2018-02-21',NULL,NULL)
INSERT INTO [USR_UprawnianiaToRola](USR_RolaID,USR_UprawnieniaDefinicjeID,CzyMozeWykonac,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(1,9,1,0,'2018-02-21',NULL,NULL)
INSERT INTO [USR_UprawnianiaToRola](USR_RolaID,USR_UprawnieniaDefinicjeID,CzyMozeWykonac,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(1,10,1,0,'2018-02-21',NULL,NULL)
INSERT INTO [USR_UprawnianiaToRola](USR_RolaID,USR_UprawnieniaDefinicjeID,CzyMozeWykonac,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(1,11,1,0,'2018-02-21',NULL,NULL)
INSERT INTO [USR_UprawnianiaToRola](USR_RolaID,USR_UprawnieniaDefinicjeID,CzyMozeWykonac,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(1,12,1,0,'2018-02-21',NULL,NULL)
INSERT INTO [USR_UprawnianiaToRola](USR_RolaID,USR_UprawnieniaDefinicjeID,CzyMozeWykonac,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(1,13,1,0,'2018-02-21',NULL,NULL)
INSERT INTO [USR_UprawnianiaToRola](USR_RolaID,USR_UprawnieniaDefinicjeID,CzyMozeWykonac,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(1,14,1,0,'2018-02-21',NULL,NULL)
INSERT INTO [USR_UprawnianiaToRola](USR_RolaID,USR_UprawnieniaDefinicjeID,CzyMozeWykonac,Dodal,DataDodania,Zmodyfikowal,DataModyfikacji) values(1,15,1,0,'2018-02-21',NULL,NULL)
GO


-----------------------------------------------------------------wdro9------------------------------------------------------------------------


delete from UstawieniaProgramu where IDUstawieniaProgramu >=1028 and IDUstawieniaProgramu<=1029

INSERT INTO [dbo].[UstawieniaProgramu] ([IDUstawieniaProgramu],[Nazwa],[Wartosc],[WartoscFloat],[WartoscText],[CzyBlokowac],[TypPola],[Opis],[IDRodzica])
 VALUES (1028,'GrupyZajeciowe',null ,null,null,0,'Rodzic',null,null)

INSERT INTO [dbo].[UstawieniaProgramu] ([IDUstawieniaProgramu],[Nazwa],[Wartosc],[WartoscFloat],[WartoscText],[CzyBlokowac],[TypPola],[Opis],[IDRodzica])
 VALUES (1029,'TypUczestnictwa',null ,null,'standardowy',0,'EditText',null,1028) 





 --------------------------------------------------------wdro10-----------------------------------------------

 UPDATE [USR_UprawnianiaDefinicje] SET [Adres] = '/api/Profil/GetSzczegoloweProfilPracownika'
WHERE [Adres] = '/api/ProfilPracownika/GetProfilPracownika'

UPDATE [USR_UprawnianiaDefinicje] SET [Adres] = '/api/Profil/POSTZapiszZdjeciePracownika'
WHERE [Adres] = '/api/ProfilPracownika/POSTZapiszZdjecie'
GO