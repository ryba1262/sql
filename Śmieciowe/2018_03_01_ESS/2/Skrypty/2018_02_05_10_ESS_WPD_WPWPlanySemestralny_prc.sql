-- ******************* -- WPWPlanySemestralny -- ******************* --

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WPWPlanySemestralny]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[WPWPlanySemestralny]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[WPWPlanySemestralny] @pracownikID INT, @Sem INT, @Rok INT
AS


declare @table table(id int, liczbaKier int, liczbaSpec int, kier int, spec int)
  declare @TabDaty table (id int identity(8,1), Daty varchar(1000))

	insert into @table
		select distinct idplanzajecpoz,count( distinct spec.kierunekid), count(distinct SpecjalnoscID), max(spec.kierunekID), max(specjalnoscID)
		from PlanZajecPoz pzp
 	  join GrupaZajeciowa gz on pzp.GrupaZajeciowaID = IDgrupaZajeciowa
		left join PlanPozPracownik ppp on pzp.IDplanZajecPoz = ppp.planZajecpozid
	  join PlanZajec pz on pz.IDplanZajec = gz.PlanZajecID or
		pz.IDplanZajec in (select PlanZajecID from ZajeciaWspolnePoz where ZajeciaWspolneID = gz.ZajeciaWspolneID)
		join siatka on siatkaid = idsiatka
		join program on programid = idprogram
		join Specjalnosc spec on specjalnoscid = idspecjalnosc

		where ppp.pracownikid = @PracownikID
		and pz.RokAkad = @Rok
		and pz.SemestrID = @Sem
		group by idplanzajecpoz

   insert into @TabDaty
    select dbo.DatyZKalendarza(idplanzajecpoz)
    from PlanZajecPoz
    join @table tab on idplanzajecpoz = tab.id
    join zajeciaStatus on ZajeciaStatusID = IDZajeciaStatus 
    where CzyKalendarz = 1

		select
			distinct
			IDPlanZajecPoz,
			gz.IDGrupaZajeciowa,
			gz.FormaWymiarID,
      pz.SemestrStudiow Sem,
			prz.Nazwa + coalesce(' - ' + nullif(fw.NazwaFormy,''),'') PrzedmiotNazwa,
			fz.Symbol6Znakow FormaZajec,
			sr.Numer as SRNumer,
			coalesce(bud.Symbol + ' ' + cast(sala.Numer as varchar(6)),'<bez sali>') Gdzie,
			dbo.FormatCzas(OdGodz) OdGodziny,
			dbo.FormatCzas(OdGodz + LGodz*pz.CzasZajec + CzasPrzerw) +
			case when zs.CoXtyg = 2 and CzyKalendarz = 0 then case when zs.NzX = 1 then +' (*)' else +' (**)' end else '' end as DoGodziny,
      odGodz as OdMinuty,
			case when zs.CzyKalendarz = 0 then dt.Nazwa else dbo.DatyZKalendarza(pzp.IDPlanZajecPoz) end Kiedy,
			case when zs.CzyKalendarz = 0 then 1+(6+(pzp.dzienTygodniaID-1)%7)%7 else
        ( select top 1 ID from @tabDaty where Daty = dbo.DatyZKalendarza(pzp.IDPlanZajecPoz) ) end DzienID,

			prz.Kod + ' - ' +ku.koduzupelniajacy Kod,

			case when ( select liczbaKier from @table where id = pzp.idplanzajecpoz ) = 1 then
			(
				select '<'+rtrim(w.Symbol)+'> '+k.Symbol from Kierunek   k
        join Wydzial w on WydzialID = IDWydzial
				where IDkierunek = (select kier from @table where id = pzp.idplanzajecpoz )

			)
			else
			 '' end KierSymbol,

			case when ( select LiczbaSpec from @table where id = pzp.idplanzajecpoz	) = 1 then
			(
				select '<' +ss.Symbol + '> ' +spec.Nazwa from Specjalnosc spec

				join SystemStudiow SS on SystemStudiowID = IDSystemStudiow
				where IDSpecjalnosc = (select spec from @table where id = pzp.idplanzajecpoz )
			)
			else '' end SpecNazwa,

			tryb.symbol as TrybSymbol,
	    dbo.GrupyStudenckieStr(pzp.GrupaZajeciowaID) Grupy,
      Zjazd.Nazwa as ZjazdNazwa,
      Sekcja.Nazwa as SekcjaNazwa,
      Sekcja.Symbol as SekcjaSymbol,
	  fz.Kolor as FormaKolor
      
		from PlanZajecPoz pzp
 	  join GrupaZajeciowa gz on pzp.GrupaZajeciowaID = IDgrupaZajeciowa
	  join ZajeciaStatus zs on ZajeciaStatusid = idZajeciaStatus

		-- do grupy
		left join PlanPozGrupa ppg on ppg.GrupaZajeciowaId = gz.IdGrupazajeciowa

		-- rodzaj studiów
	  join PlanZajec pz on pz.IDplanZajec = gz.PlanZajecID or
		pz.IDplanZajec in (select PlanZajecID from ZajeciaWspolnePoz where ZajeciaWspolneID = gz.ZajeciaWspolneID)
		join siatka on siatkaID = IDSiatka
		join program pr on siatka.programid = idprogram
 	  join TrybStudiow tryb on siatka.TrybstudiowID = IDtrybStudiow
    join Zjazd on pz.ZjazdID = IDZjazd
    join Sekcja on SekcjaID = IDSekcja

		-- do przedmiotu
		left join FormaWymiar fw on FormaWymiarID = IDformaWymiar
		left join SemestrRealizacji sr on IDSemestrRealizacji = fw.SemestrRealizacjiID
		left join KartaUzup ku on sr.KartaUzupID = IDkartaUzup
		left join Przedmiot prz on ku.PrzedmiotID = IDprzedmiot
		left join FormaZajec fz on fw.FormaZajecID = IDFormaZajec

		-- do sali
		left join Sala on pzp.SalaID = IDsala
		left join Budynek bud on budynekid = idbudynek

		-- dzienTygodnia
		left join DzienTygodnia dt on pzp.DzienTygodniaID = IDDzienTygodnia

		where pzp.idPlanZajecPoz in (select ID from @Table)

    order by DzienID, odGodz
	



GO


