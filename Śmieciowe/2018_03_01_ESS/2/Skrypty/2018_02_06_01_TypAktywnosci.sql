IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__Tmp_TypUc__CzyFo__5F4BB751]') AND parent_object_id = OBJECT_ID(N'[dbo].[TypUczestnictwa]'))
ALTER TABLE [dbo].[TypUczestnictwa] DROP CONSTRAINT [DF__Tmp_TypUc__CzyFo__5F4BB751]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ZajeciaTypUczestnictwa_TypUczestnictwa]') AND parent_object_id = OBJECT_ID(N'[dbo].[ZajeciaTypUczestnictwa]'))
ALTER TABLE [dbo].[ZajeciaTypUczestnictwa] DROP CONSTRAINT [FK_ZajeciaTypUczestnictwa_TypUczestnictwa]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TypUczestnictwa]') AND name = N'PK_TypUczestnictwa')
ALTER TABLE [dbo].[TypUczestnictwa] DROP CONSTRAINT [PK_TypUczestnictwa]
--------------------------------------
----------------TypUczestnictwa---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[TypUczestnictwa]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[TypUczestnictwa]
GO

CREATE TABLE [dbo].[TypUczestnictwa] (
   [IDTypUczestnictwa] [int]  IDENTITY(1,1) NOT NULL
  ,[NazwaTypu] [varchar] (256) NOT NULL
  ,[Opis] [varchar] (500) NULL
  ,[CzyFormaWymiar] [bit]  NOT NULL DEFAULT ((0))
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO




ALTER TABLE [dbo].[TypUczestnictwa] ADD  CONSTRAINT [PK_TypUczestnictwa] PRIMARY KEY CLUSTERED( IDTypUczestnictwa ) ON [PRIMARY]



