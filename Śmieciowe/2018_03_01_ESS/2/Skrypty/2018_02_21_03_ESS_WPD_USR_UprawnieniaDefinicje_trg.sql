DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------USR_UprawnianiaDefinicje-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_UprawnianiaDefinicje_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER USR_UprawnianiaDefinicje_INSERT
')

EXEC('
CREATE TRIGGER USR_UprawnianiaDefinicje_INSERT ON ['+@BazaDanych+'].[dbo].[USR_UprawnianiaDefinicje] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_UprawnianiaDefinicje'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_UprawnianiaDefinicje]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_UprawnianiaDefinicje]
  ,[Org_Adres]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDUSR_UprawnianiaDefinicje
 ,ins.Adres
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_UprawnianiaDefinicje_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER USR_UprawnianiaDefinicje_UPDATE
')

EXEC('
CREATE TRIGGER USR_UprawnianiaDefinicje_UPDATE ON ['+@BazaDanych+'].[dbo].[USR_UprawnianiaDefinicje] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_UprawnianiaDefinicje'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_UprawnianiaDefinicje]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_UprawnianiaDefinicje]
  ,[Org_Adres]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDUSR_UprawnianiaDefinicje
 ,ins.Adres
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_UprawnianiaDefinicje_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER USR_UprawnianiaDefinicje_DELETE
')

EXEC('
CREATE TRIGGER USR_UprawnianiaDefinicje_DELETE ON ['+@BazaDanych+'].[dbo].[USR_UprawnianiaDefinicje] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_UprawnianiaDefinicje'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_UprawnianiaDefinicje]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_UprawnianiaDefinicje]
  ,[Org_Adres]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDUSR_UprawnianiaDefinicje
 ,del.Adres
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'USR_UprawnianiaDefinicje')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('USR_UprawnianiaDefinicje', 1, 0,GetDate())

