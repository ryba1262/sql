
-------- pamiętaj coby ten skrypt odpalać na bazie logów  --------


----------------USR_UprawnianiaToRola---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_USR_UprawnianiaToRola]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_USR_UprawnianiaToRola] (
   [Log_USR_UprawnianiaToRolaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_UprawnianiaToRola] [int] 
  ,[Org_USR_RolaID] [int] 
  ,[Org_USR_UprawnieniaDefinicjeID] [int] 
  ,[Org_CzyMozeWykonac] [bit] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_USR_UprawnianiaToRola] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_USR_UprawnianiaToRola] PRIMARY KEY  CLUSTERED
(
   [Log_USR_UprawnianiaToRolaID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_USR_UprawnianiaToRola] (
   [Log_USR_UprawnianiaToRolaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_UprawnianiaToRola] [int] 
  ,[Org_USR_RolaID] [int] 
  ,[Org_USR_UprawnieniaDefinicjeID] [int] 
  ,[Org_CzyMozeWykonac] [bit] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_UprawnianiaToRola ON

IF EXISTS (SELECT * FROM dbo.LOG_USR_UprawnianiaToRola)
EXEC('INSERT INTO dbo.Tmp_LOG_USR_UprawnianiaToRola (Log_USR_UprawnianiaToRolaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_UprawnianiaToRola, Org_USR_RolaID, Org_USR_UprawnieniaDefinicjeID, Org_CzyMozeWykonac, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_USR_UprawnianiaToRolaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_UprawnianiaToRola, Org_USR_RolaID, Org_USR_UprawnieniaDefinicjeID, Org_CzyMozeWykonac, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_USR_UprawnianiaToRola WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_UprawnianiaToRola OFF

DROP TABLE dbo.LOG_USR_UprawnianiaToRola

EXECUTE sp_rename N'dbo.Tmp_LOG_USR_UprawnianiaToRola', N'LOG_USR_UprawnianiaToRola', 'OBJECT'

ALTER TABLE [dbo].[LOG_USR_UprawnianiaToRola] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_USR_UprawnianiaToRola] PRIMARY KEY  CLUSTERED
(
   [Log_USR_UprawnianiaToRolaID]
) ON [PRIMARY]

END
GO

