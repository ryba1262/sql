---------------------wdro1------------------2018_02_05_03_FormaZajecNowaKolumna_Trigger
---------------------wdro2------------------2018_02_05_07_ESS_PlanZajec_Trigger
---------------------wdro3------------------2018_02_06_03_TypAktywnosci_Trigger
---------------------wdro4------------------2018_02_06_07_ZajeciaTypAktywnosci_Trigger
---------------------wdro5------------------2018_02_21_03_ESS_WPD_USR_UprawnieniaDefinicje_trg
---------------------wdro6------------------2018_02_21_06_ESS_WPD_USR_UprawnieniaToRola_trg
---------------------wdro7------------------2018_02_26_03_ESS_WPD_USR_Rola_CzyPolitykaHasel_trg.sql





----------------------------------------------------------------wdro1-------------------------------------------------------

-- =============================================
-- Author:		Agata Stelcer-Matuszczak
-- Create date: 2018-02-05
-- Description:	Dodanie nowej kolumny "Kolor" - TRIGGERY
-- =============================================

DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''FormaZajec_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER FormaZajec_INSERT
')

EXEC('
CREATE TRIGGER FormaZajec_INSERT ON ['+@BazaDanych+'].[dbo].[FormaZajec] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''FormaZajec'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_FormaZajec]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDFormaZajec]
  ,[Org_Symbol]
  ,[Org_Nazwa]
  ,[Org_Opis]
  ,[Org_Symbol6Znakow]
  ,[Org_CzyDrukowacNaKarcieEgz]
  ,[Org_Kolor]
  ,[Org_DataModyfikacji]
  ,[Org_Zmodyfikowal]
  ,[Org_DataDodania]
  ,[Org_Dodal]
)
SELECT
1
  ,@ID
 ,ins.IDFormaZajec
 ,ins.Symbol
 ,ins.Nazwa
 ,ins.Opis
 ,ins.Symbol6Znakow
 ,ins.CzyDrukowacNaKarcieEgz
 ,ins.Kolor
 ,ins.DataModyfikacji
 ,ins.Zmodyfikowal
 ,ins.DataDodania
 ,ins.Dodal
FROM  inserted ins 
END
END
')

EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''FormaZajec_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER FormaZajec_UPDATE
')

EXEC('
CREATE TRIGGER FormaZajec_UPDATE ON ['+@BazaDanych+'].[dbo].[FormaZajec] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''FormaZajec'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_FormaZajec]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDFormaZajec]
  ,[Org_Symbol]
  ,[Org_Nazwa]
  ,[Org_Opis]
  ,[Org_Symbol6Znakow]
  ,[Org_CzyDrukowacNaKarcieEgz]
  ,[Org_Kolor]
  ,[Org_DataModyfikacji]
  ,[Org_Zmodyfikowal]
  ,[Org_DataDodania]
  ,[Org_Dodal]
)
SELECT
2
  ,@ID
 ,ins.IDFormaZajec
 ,ins.Symbol
 ,ins.Nazwa
 ,ins.Opis
 ,ins.Symbol6Znakow
 ,ins.CzyDrukowacNaKarcieEgz
 ,ins.Kolor
 ,ins.DataModyfikacji
 ,ins.Zmodyfikowal
 ,ins.DataDodania
 ,ins.Dodal
FROM  inserted ins 
END
END
')

EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''FormaZajec_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER FormaZajec_DELETE
')

EXEC('
CREATE TRIGGER FormaZajec_DELETE ON ['+@BazaDanych+'].[dbo].[FormaZajec] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''FormaZajec'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_FormaZajec]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDFormaZajec]
  ,[Org_Symbol]
  ,[Org_Nazwa]
  ,[Org_Opis]
  ,[Org_Symbol6Znakow]
  ,[Org_CzyDrukowacNaKarcieEgz]
  ,[Org_Kolor]
  ,[Org_DataModyfikacji]
  ,[Org_Zmodyfikowal]
  ,[Org_DataDodania]
  ,[Org_Dodal]
)
SELECT
3
  ,@ID
 ,del.IDFormaZajec
 ,del.Symbol
 ,del.Nazwa
 ,del.Opis
 ,del.Symbol6Znakow
 ,del.CzyDrukowacNaKarcieEgz
 ,del.Kolor
 ,del.DataModyfikacji
 ,del.Zmodyfikowal
 ,del.DataDodania
 ,del.Dodal
FROM  deleted del 
END
END
')

if not exists (select * from LOG_LogowanaTabela where Tabela = 'FormaZajec')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('FormaZajec', 1, 0,GetDate())
GO





----------------------------------------------------------------wdro2-------------------------------------------------------


DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''PlanZajec_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER PlanZajec_INSERT
')

EXEC('
CREATE TRIGGER PlanZajec_INSERT ON ['+@BazaDanych+'].[dbo].[PlanZajec] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''PlanZajec'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_PlanZajec]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDPlanZajec]
  ,[Org_SiatkaID]
  ,[Org_SekcjaID]
  ,[Org_Opis]
  ,[Org_RokAkad]
  ,[Org_SemestrStudiow]
  ,[Org_ZjazdID]
  ,[Org_SemestrID]
  ,[Org_PrzerwaPoXGodz]
  ,[Org_DataDodania]
  ,[Org_Dodal]
  ,[Org_DataModyfikacji]
  ,[Org_Zmodyfikowal]
  ,[Org_Dokladnosc]
  ,[Org_GodzOd]
  ,[Org_GodzDo]
  ,[Org_CzasPrzerwy]
  ,[Org_CzasZajec]
  ,[Org_StatusAktywny]
  ,[Org_StatusAktywnyDydaktyk]
  ,[Org_EdycjaSesjaID]
)
SELECT
1
  ,@ID
 ,ins.IDPlanZajec
 ,ins.SiatkaID
 ,ins.SekcjaID
 ,ins.Opis
 ,ins.RokAkad
 ,ins.SemestrStudiow
 ,ins.ZjazdID
 ,ins.SemestrID
 ,ins.PrzerwaPoXGodz
 ,ins.DataDodania
 ,ins.Dodal
 ,ins.DataModyfikacji
 ,ins.Zmodyfikowal
 ,ins.Dokladnosc
 ,ins.GodzOd
 ,ins.GodzDo
 ,ins.CzasPrzerwy
 ,ins.CzasZajec
 ,ins.StatusAktywny
 ,ins.StatusAktywnyDydaktyk
 ,ins.EdycjaSesjaID
FROM  inserted ins 
END
END
')

EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''PlanZajec_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER PlanZajec_UPDATE
')

EXEC('
CREATE TRIGGER PlanZajec_UPDATE ON ['+@BazaDanych+'].[dbo].[PlanZajec] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''PlanZajec'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_PlanZajec]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDPlanZajec]
  ,[Org_SiatkaID]
  ,[Org_SekcjaID]
  ,[Org_Opis]
  ,[Org_RokAkad]
  ,[Org_SemestrStudiow]
  ,[Org_ZjazdID]
  ,[Org_SemestrID]
  ,[Org_PrzerwaPoXGodz]
  ,[Org_DataDodania]
  ,[Org_Dodal]
  ,[Org_DataModyfikacji]
  ,[Org_Zmodyfikowal]
  ,[Org_Dokladnosc]
  ,[Org_GodzOd]
  ,[Org_GodzDo]
  ,[Org_CzasPrzerwy]
  ,[Org_CzasZajec]
  ,[Org_StatusAktywny]
  ,[Org_StatusAktywnyDydaktyk]
  ,[Org_EdycjaSesjaID]
)
SELECT
2
  ,@ID
 ,ins.IDPlanZajec
 ,ins.SiatkaID
 ,ins.SekcjaID
 ,ins.Opis
 ,ins.RokAkad
 ,ins.SemestrStudiow
 ,ins.ZjazdID
 ,ins.SemestrID
 ,ins.PrzerwaPoXGodz
 ,ins.DataDodania
 ,ins.Dodal
 ,ins.DataModyfikacji
 ,ins.Zmodyfikowal
 ,ins.Dokladnosc
 ,ins.GodzOd
 ,ins.GodzDo
 ,ins.CzasPrzerwy
 ,ins.CzasZajec
 ,ins.StatusAktywny
 ,ins.StatusAktywnyDydaktyk
 ,ins.EdycjaSesjaID
FROM  inserted ins 
END
END
')

EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''PlanZajec_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER PlanZajec_DELETE
')

EXEC('
CREATE TRIGGER PlanZajec_DELETE ON ['+@BazaDanych+'].[dbo].[PlanZajec] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''PlanZajec'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_PlanZajec]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDPlanZajec]
  ,[Org_SiatkaID]
  ,[Org_SekcjaID]
  ,[Org_Opis]
  ,[Org_RokAkad]
  ,[Org_SemestrStudiow]
  ,[Org_ZjazdID]
  ,[Org_SemestrID]
  ,[Org_PrzerwaPoXGodz]
  ,[Org_DataDodania]
  ,[Org_Dodal]
  ,[Org_DataModyfikacji]
  ,[Org_Zmodyfikowal]
  ,[Org_Dokladnosc]
  ,[Org_GodzOd]
  ,[Org_GodzDo]
  ,[Org_CzasPrzerwy]
  ,[Org_CzasZajec]
  ,[Org_StatusAktywny]
  ,[Org_StatusAktywnyDydaktyk]
  ,[Org_EdycjaSesjaID]
)
SELECT
3
  ,@ID
 ,del.IDPlanZajec
 ,del.SiatkaID
 ,del.SekcjaID
 ,del.Opis
 ,del.RokAkad
 ,del.SemestrStudiow
 ,del.ZjazdID
 ,del.SemestrID
 ,del.PrzerwaPoXGodz
 ,del.DataDodania
 ,del.Dodal
 ,del.DataModyfikacji
 ,del.Zmodyfikowal
 ,del.Dokladnosc
 ,del.GodzOd
 ,del.GodzDo
 ,del.CzasPrzerwy
 ,del.CzasZajec
 ,del.StatusAktywny
 ,del.StatusAktywnyDydaktyk
 ,del.EdycjaSesjaID
FROM  deleted del 
END
END
')

if not exists (select * from LOG_LogowanaTabela where Tabela = 'PlanZajec')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('PlanZajec', 1, 0,GetDate())
GO




---------------------------------------------------------------wdro3------------------------------------------------------------------


DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------TypUczestnictwa-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''TypUczestnictwa_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER TypUczestnictwa_INSERT
')

EXEC('
CREATE TRIGGER TypUczestnictwa_INSERT ON ['+@BazaDanych+'].[dbo].[TypUczestnictwa] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''TypUczestnictwa'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_TypUczestnictwa]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDTypUczestnictwa]
  ,[Org_NazwaTypu]
  ,[Org_Opis]
  ,[Org_CzyFormaWymiar]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDTypUczestnictwa
 ,ins.NazwaTypu
 ,ins.Opis
 ,ins.CzyFormaWymiar
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''TypUczestnictwa_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER TypUczestnictwa_UPDATE
')

EXEC('
CREATE TRIGGER TypUczestnictwa_UPDATE ON ['+@BazaDanych+'].[dbo].[TypUczestnictwa] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''TypUczestnictwa'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_TypUczestnictwa]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDTypUczestnictwa]
  ,[Org_NazwaTypu]
  ,[Org_Opis]
  ,[Org_CzyFormaWymiar]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDTypUczestnictwa
 ,ins.NazwaTypu
 ,ins.Opis
 ,ins.CzyFormaWymiar
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''TypUczestnictwa_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER TypUczestnictwa_DELETE
')

EXEC('
CREATE TRIGGER TypUczestnictwa_DELETE ON ['+@BazaDanych+'].[dbo].[TypUczestnictwa] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''TypUczestnictwa'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_TypUczestnictwa]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDTypUczestnictwa]
  ,[Org_NazwaTypu]
  ,[Org_Opis]
  ,[Org_CzyFormaWymiar]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDTypUczestnictwa
 ,del.NazwaTypu
 ,del.Opis
 ,del.CzyFormaWymiar
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'TypUczestnictwa')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('TypUczestnictwa', 1, 0,GetDate())


go

------------------------------------------------------------wdro4------------------------------------------------------------------


DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------ZajeciaTypUczestnictwa-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ZajeciaTypUczestnictwa_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER ZajeciaTypUczestnictwa_INSERT
')

EXEC('
CREATE TRIGGER ZajeciaTypUczestnictwa_INSERT ON ['+@BazaDanych+'].[dbo].[ZajeciaTypUczestnictwa] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ZajeciaTypUczestnictwa'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ZajeciaTypUczestnictwa]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDZajeciaTypUczestnictwa]
  ,[Org_TypUczestnictwaID]
  ,[Org_Podmiot]
  ,[Org_PodmiotID]
  ,[Org_FormaWymiarID]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDZajeciaTypUczestnictwa
 ,ins.TypUczestnictwaID
 ,ins.Podmiot
 ,ins.PodmiotID
 ,ins.FormaWymiarID
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ZajeciaTypUczestnictwa_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER ZajeciaTypUczestnictwa_UPDATE
')

EXEC('
CREATE TRIGGER ZajeciaTypUczestnictwa_UPDATE ON ['+@BazaDanych+'].[dbo].[ZajeciaTypUczestnictwa] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ZajeciaTypUczestnictwa'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ZajeciaTypUczestnictwa]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDZajeciaTypUczestnictwa]
  ,[Org_TypUczestnictwaID]
  ,[Org_Podmiot]
  ,[Org_PodmiotID]
  ,[Org_FormaWymiarID]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDZajeciaTypUczestnictwa
 ,ins.TypUczestnictwaID
 ,ins.Podmiot
 ,ins.PodmiotID
 ,ins.FormaWymiarID
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ZajeciaTypUczestnictwa_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER ZajeciaTypUczestnictwa_DELETE
')

EXEC('
CREATE TRIGGER ZajeciaTypUczestnictwa_DELETE ON ['+@BazaDanych+'].[dbo].[ZajeciaTypUczestnictwa] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ZajeciaTypUczestnictwa'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ZajeciaTypUczestnictwa]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDZajeciaTypUczestnictwa]
  ,[Org_TypUczestnictwaID]
  ,[Org_Podmiot]
  ,[Org_PodmiotID]
  ,[Org_FormaWymiarID]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDZajeciaTypUczestnictwa
 ,del.TypUczestnictwaID
 ,del.Podmiot
 ,del.PodmiotID
 ,del.FormaWymiarID
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'ZajeciaTypUczestnictwa')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('ZajeciaTypUczestnictwa', 1, 0,GetDate())


go
----------------------------------------------------------------wdro5-------------------------------------------------------------


DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------USR_UprawnianiaDefinicje-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_UprawnianiaDefinicje_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER USR_UprawnianiaDefinicje_INSERT
')

EXEC('
CREATE TRIGGER USR_UprawnianiaDefinicje_INSERT ON ['+@BazaDanych+'].[dbo].[USR_UprawnianiaDefinicje] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_UprawnianiaDefinicje'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_UprawnianiaDefinicje]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_UprawnianiaDefinicje]
  ,[Org_Adres]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDUSR_UprawnianiaDefinicje
 ,ins.Adres
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_UprawnianiaDefinicje_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER USR_UprawnianiaDefinicje_UPDATE
')

EXEC('
CREATE TRIGGER USR_UprawnianiaDefinicje_UPDATE ON ['+@BazaDanych+'].[dbo].[USR_UprawnianiaDefinicje] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_UprawnianiaDefinicje'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_UprawnianiaDefinicje]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_UprawnianiaDefinicje]
  ,[Org_Adres]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDUSR_UprawnianiaDefinicje
 ,ins.Adres
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_UprawnianiaDefinicje_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER USR_UprawnianiaDefinicje_DELETE
')

EXEC('
CREATE TRIGGER USR_UprawnianiaDefinicje_DELETE ON ['+@BazaDanych+'].[dbo].[USR_UprawnianiaDefinicje] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_UprawnianiaDefinicje'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_UprawnianiaDefinicje]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_UprawnianiaDefinicje]
  ,[Org_Adres]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDUSR_UprawnianiaDefinicje
 ,del.Adres
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'USR_UprawnianiaDefinicje')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('USR_UprawnianiaDefinicje', 1, 0,GetDate())
go


----------------------------------------------------------------wdro6--------------------------------------------------------------------------


DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------USR_UprawnianiaToRola-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_UprawnianiaToRola_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER USR_UprawnianiaToRola_INSERT
')

EXEC('
CREATE TRIGGER USR_UprawnianiaToRola_INSERT ON ['+@BazaDanych+'].[dbo].[USR_UprawnianiaToRola] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_UprawnianiaToRola'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_UprawnianiaToRola]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_UprawnianiaToRola]
  ,[Org_USR_RolaID]
  ,[Org_USR_UprawnieniaDefinicjeID]
  ,[Org_CzyMozeWykonac]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDUSR_UprawnianiaToRola
 ,ins.USR_RolaID
 ,ins.USR_UprawnieniaDefinicjeID
 ,ins.CzyMozeWykonac
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_UprawnianiaToRola_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER USR_UprawnianiaToRola_UPDATE
')

EXEC('
CREATE TRIGGER USR_UprawnianiaToRola_UPDATE ON ['+@BazaDanych+'].[dbo].[USR_UprawnianiaToRola] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_UprawnianiaToRola'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_UprawnianiaToRola]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_UprawnianiaToRola]
  ,[Org_USR_RolaID]
  ,[Org_USR_UprawnieniaDefinicjeID]
  ,[Org_CzyMozeWykonac]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDUSR_UprawnianiaToRola
 ,ins.USR_RolaID
 ,ins.USR_UprawnieniaDefinicjeID
 ,ins.CzyMozeWykonac
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_UprawnianiaToRola_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER USR_UprawnianiaToRola_DELETE
')

EXEC('
CREATE TRIGGER USR_UprawnianiaToRola_DELETE ON ['+@BazaDanych+'].[dbo].[USR_UprawnianiaToRola] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_UprawnianiaToRola'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_UprawnianiaToRola]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_UprawnianiaToRola]
  ,[Org_USR_RolaID]
  ,[Org_USR_UprawnieniaDefinicjeID]
  ,[Org_CzyMozeWykonac]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDUSR_UprawnianiaToRola
 ,del.USR_RolaID
 ,del.USR_UprawnieniaDefinicjeID
 ,del.CzyMozeWykonac
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'USR_UprawnianiaToRola')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('USR_UprawnianiaToRola', 1, 0,GetDate())

GO


---------------------------------------------------wdro7--------------------------------------------------------


DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------USR_UprawnianiaToRola-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_UprawnianiaToRola_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER USR_UprawnianiaToRola_INSERT
')

EXEC('
CREATE TRIGGER USR_UprawnianiaToRola_INSERT ON ['+@BazaDanych+'].[dbo].[USR_UprawnianiaToRola] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_UprawnianiaToRola'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_UprawnianiaToRola]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_UprawnianiaToRola]
  ,[Org_USR_RolaID]
  ,[Org_USR_UprawnieniaDefinicjeID]
  ,[Org_CzyMozeWykonac]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDUSR_UprawnianiaToRola
 ,ins.USR_RolaID
 ,ins.USR_UprawnieniaDefinicjeID
 ,ins.CzyMozeWykonac
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_UprawnianiaToRola_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER USR_UprawnianiaToRola_UPDATE
')

EXEC('
CREATE TRIGGER USR_UprawnianiaToRola_UPDATE ON ['+@BazaDanych+'].[dbo].[USR_UprawnianiaToRola] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_UprawnianiaToRola'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_UprawnianiaToRola]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_UprawnianiaToRola]
  ,[Org_USR_RolaID]
  ,[Org_USR_UprawnieniaDefinicjeID]
  ,[Org_CzyMozeWykonac]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDUSR_UprawnianiaToRola
 ,ins.USR_RolaID
 ,ins.USR_UprawnieniaDefinicjeID
 ,ins.CzyMozeWykonac
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_UprawnianiaToRola_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER USR_UprawnianiaToRola_DELETE
')

EXEC('
CREATE TRIGGER USR_UprawnianiaToRola_DELETE ON ['+@BazaDanych+'].[dbo].[USR_UprawnianiaToRola] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_UprawnianiaToRola'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_UprawnianiaToRola]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_UprawnianiaToRola]
  ,[Org_USR_RolaID]
  ,[Org_USR_UprawnieniaDefinicjeID]
  ,[Org_CzyMozeWykonac]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDUSR_UprawnianiaToRola
 ,del.USR_RolaID
 ,del.USR_UprawnieniaDefinicjeID
 ,del.CzyMozeWykonac
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'USR_UprawnianiaToRola')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('USR_UprawnianiaToRola', 1, 0,GetDate())

