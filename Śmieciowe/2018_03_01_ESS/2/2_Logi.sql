-------------------wdro1------------------2018_02_05_02_FormaZajecNowaKolumna_LOG
-------------------wdro2------------------2018_02_05_06_ESS_PlanZajec_Log
-------------------wdro3------------------2018_02_06_02_TypAktywnosci_Log
-------------------wdro4------------------2018_02_06_06_ZajeciaTypAktywnosci_Log
-------------------wdro5------------------2018_02_21_02_ESS_WPD_USR_UprawnieniaDefinicje_log
-------------------wdro6------------------2018_02_21_05_ESS_WPD_USR_UprawnieniaToRola_log
-------------------wdro7------------------2018_02_26_02_ESS_WPD_USR_Rola_CzyPolitykaHasel_log.sql












------------------------------------------------------------wdro1-----------------------------------------------------------------------------

-- =============================================
-- Author:		Agata Stelcer-Matuszczak
-- Create date: 2018-02-05
-- Description:	Dodanie nowej kolumny "Kolor" do Logow
-- =============================================

--------------------------------------
----------------FormaZajec---------------
--------------------------------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_FormaZajec]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_FormaZajec] (
   [Log_FormaZajecID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDFormaZajec] [int] 
  ,[Org_Symbol] [varchar] (2)
  ,[Org_Nazwa] [varchar] (50)
  ,[Org_Opis] [varchar] (200)
  ,[Org_Symbol6Znakow] [varchar] (6)
  ,[Org_CzyDrukowacNaKarcieEgz] [bit] 
  ,[Org_Kolor] [varchar] (10)
  ,[Org_DataModyfikacji] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Dodal] [int] 
) ON [PRIMARY]
ALTER TABLE [dbo].[LOG_FormaZajec] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_FormaZajec] PRIMARY KEY  CLUSTERED
(
   [Log_FormaZajecID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_FormaZajec] (
   [Log_FormaZajecID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDFormaZajec] [int] 
  ,[Org_Symbol] [varchar] (2)
  ,[Org_Nazwa] [varchar] (50)
  ,[Org_Opis] [varchar] (200)
  ,[Org_Symbol6Znakow] [varchar] (6)
  ,[Org_CzyDrukowacNaKarcieEgz] [bit] 
  ,[Org_Kolor] [varchar] (10)
  ,[Org_DataModyfikacji] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Dodal] [int] 
) ON [PRIMARY]
SET IDENTITY_INSERT dbo.Tmp_LOG_FormaZajec ON

IF EXISTS (SELECT * FROM dbo.LOG_FormaZajec)
EXEC('INSERT INTO dbo.Tmp_LOG_FormaZajec (Log_FormaZajecID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDFormaZajec, Org_Symbol, Org_Nazwa, Org_Opis, Org_Symbol6Znakow, Org_CzyDrukowacNaKarcieEgz, Org_DataModyfikacji, Org_Zmodyfikowal, Org_DataDodania, Org_Dodal)
  SELECT Log_FormaZajecID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDFormaZajec, Org_Symbol, Org_Nazwa, Org_Opis, Org_Symbol6Znakow, Org_CzyDrukowacNaKarcieEgz, Org_DataModyfikacji, Org_Zmodyfikowal, Org_DataDodania, Org_Dodal FROM dbo.Log_FormaZajec WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_FormaZajec OFF

DROP TABLE dbo.LOG_FormaZajec

EXECUTE sp_rename N'dbo.Tmp_LOG_FormaZajec', N'LOG_FormaZajec', 'OBJECT'

ALTER TABLE [dbo].[LOG_FormaZajec] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_FormaZajec] PRIMARY KEY  CLUSTERED
(
   [Log_FormaZajecID]
) ON [PRIMARY]

END
GO





----------------------------------------------------------wdro2-----------------------------------------------------------------------


--------------------------------------
----------------PlanZajec---------------
--------------------------------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_PlanZajec]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_PlanZajec] (
   [Log_PlanZajecID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDPlanZajec] [int] 
  ,[Org_SiatkaID] [int] 
  ,[Org_SekcjaID] [int] 
  ,[Org_Opis] [varchar] (100)
  ,[Org_RokAkad] [smallint] 
  ,[Org_SemestrStudiow] [tinyint] 
  ,[Org_ZjazdID] [int] 
  ,[Org_SemestrID] [int] 
  ,[Org_PrzerwaPoXGodz] [tinyint] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Dodal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_Dokladnosc] [int] 
  ,[Org_GodzOd] [int] 
  ,[Org_GodzDo] [int] 
  ,[Org_CzasPrzerwy] [int] 
  ,[Org_CzasZajec] [int] 
  ,[Org_StatusAktywny] [bit] 
  ,[Org_StatusAktywnyDydaktyk] [bit] 
  ,[Org_EdycjaSesjaID] [int] 
) ON [PRIMARY]
ALTER TABLE [dbo].[LOG_PlanZajec] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_PlanZajec] PRIMARY KEY  CLUSTERED
(
   [Log_PlanZajecID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_PlanZajec] (
   [Log_PlanZajecID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDPlanZajec] [int] 
  ,[Org_SiatkaID] [int] 
  ,[Org_SekcjaID] [int] 
  ,[Org_Opis] [varchar] (100)
  ,[Org_RokAkad] [smallint] 
  ,[Org_SemestrStudiow] [tinyint] 
  ,[Org_ZjazdID] [int] 
  ,[Org_SemestrID] [int] 
  ,[Org_PrzerwaPoXGodz] [tinyint] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Dodal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_Dokladnosc] [int] 
  ,[Org_GodzOd] [int] 
  ,[Org_GodzDo] [int] 
  ,[Org_CzasPrzerwy] [int] 
  ,[Org_CzasZajec] [int] 
  ,[Org_StatusAktywny] [bit] 
  ,[Org_StatusAktywnyDydaktyk] [bit] 
  ,[Org_EdycjaSesjaID] [int] 
) ON [PRIMARY]
SET IDENTITY_INSERT dbo.Tmp_LOG_PlanZajec ON

IF EXISTS (SELECT * FROM dbo.LOG_PlanZajec)
EXEC('INSERT INTO dbo.Tmp_LOG_PlanZajec (Log_PlanZajecID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDPlanZajec, Org_SiatkaID, Org_SekcjaID, Org_Opis, Org_RokAkad, Org_SemestrStudiow, Org_ZjazdID, Org_SemestrID, Org_PrzerwaPoXGodz, Org_DataDodania, Org_Dodal, Org_DataModyfikacji, Org_Zmodyfikowal, Org_Dokladnosc, Org_GodzOd, Org_GodzDo, Org_CzasPrzerwy, Org_CzasZajec, Org_StatusAktywny, Org_EdycjaSesjaID)
  SELECT Log_PlanZajecID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDPlanZajec, Org_SiatkaID, Org_SekcjaID, Org_Opis, Org_RokAkad, Org_SemestrStudiow, Org_ZjazdID, Org_SemestrID, Org_PrzerwaPoXGodz, Org_DataDodania, Org_Dodal, Org_DataModyfikacji, Org_Zmodyfikowal, Org_Dokladnosc, Org_GodzOd, Org_GodzDo, Org_CzasPrzerwy, Org_CzasZajec, Org_StatusAktywny, Org_EdycjaSesjaID FROM dbo.Log_PlanZajec WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_PlanZajec OFF

DROP TABLE dbo.LOG_PlanZajec

EXECUTE sp_rename N'dbo.Tmp_LOG_PlanZajec', N'LOG_PlanZajec', 'OBJECT'

ALTER TABLE [dbo].[LOG_PlanZajec] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_PlanZajec] PRIMARY KEY  CLUSTERED
(
   [Log_PlanZajecID]
) ON [PRIMARY]

END
GO


----------------------------------------------------------wdro3-----------------------------------------------------------------


-------- pami�taj coby ten skrypt odpala� na bazie log�w  --------


----------------TypUczestnictwa---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_TypUczestnictwa]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_TypUczestnictwa] (
   [Log_TypUczestnictwaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDTypUczestnictwa] [int] 
  ,[Org_NazwaTypu] [varchar] (256)
  ,[Org_Opis] [varchar] (500)
  ,[Org_CzyFormaWymiar] [bit] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_TypUczestnictwa] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_TypUczestnictwa] PRIMARY KEY  CLUSTERED
(
   [Log_TypUczestnictwaID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_TypUczestnictwa] (
   [Log_TypUczestnictwaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDTypUczestnictwa] [int] 
  ,[Org_NazwaTypu] [varchar] (256)
  ,[Org_Opis] [varchar] (500)
  ,[Org_CzyFormaWymiar] [bit] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_TypUczestnictwa ON

IF EXISTS (SELECT * FROM dbo.LOG_TypUczestnictwa)
EXEC('INSERT INTO dbo.Tmp_LOG_TypUczestnictwa (Log_TypUczestnictwaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDTypUczestnictwa, Org_NazwaTypu, Org_Opis, Org_CzyFormaWymiar, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_TypUczestnictwaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDTypUczestnictwa, Org_NazwaTypu, Org_Opis, Org_CzyFormaWymiar, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_TypUczestnictwa WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_TypUczestnictwa OFF

DROP TABLE dbo.LOG_TypUczestnictwa

EXECUTE sp_rename N'dbo.Tmp_LOG_TypUczestnictwa', N'LOG_TypUczestnictwa', 'OBJECT'

ALTER TABLE [dbo].[LOG_TypUczestnictwa] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_TypUczestnictwa] PRIMARY KEY  CLUSTERED
(
   [Log_TypUczestnictwaID]
) ON [PRIMARY]

END
GO



-----------------------------------------------wdro4-------------------------------------------------------



-------- pami�taj coby ten skrypt odpala� na bazie log�w  --------


----------------ZajeciaTypUczestnictwa---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_ZajeciaTypUczestnictwa]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_ZajeciaTypUczestnictwa] (
   [Log_ZajeciaTypUczestnictwaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDZajeciaTypUczestnictwa] [int] 
  ,[Org_TypUczestnictwaID] [int] 
  ,[Org_Podmiot] [varchar] (100)
  ,[Org_PodmiotID] [int] 
  ,[Org_FormaWymiarID] [int] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_ZajeciaTypUczestnictwa] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_ZajeciaTypUczestnictwa] PRIMARY KEY  CLUSTERED
(
   [Log_ZajeciaTypUczestnictwaID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_ZajeciaTypUczestnictwa] (
   [Log_ZajeciaTypUczestnictwaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDZajeciaTypUczestnictwa] [int] 
  ,[Org_TypUczestnictwaID] [int] 
  ,[Org_Podmiot] [varchar] (100)
  ,[Org_PodmiotID] [int] 
  ,[Org_FormaWymiarID] [int] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_ZajeciaTypUczestnictwa ON

IF EXISTS (SELECT * FROM dbo.LOG_ZajeciaTypUczestnictwa)
EXEC('INSERT INTO dbo.Tmp_LOG_ZajeciaTypUczestnictwa (Log_ZajeciaTypUczestnictwaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDZajeciaTypUczestnictwa, Org_TypUczestnictwaID, Org_Podmiot, Org_PodmiotID, Org_FormaWymiarID, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_ZajeciaTypUczestnictwaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDZajeciaTypUczestnictwa, Org_TypUczestnictwaID, Org_Podmiot, Org_PodmiotID, Org_FormaWymiarID, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_ZajeciaTypUczestnictwa WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_ZajeciaTypUczestnictwa OFF

DROP TABLE dbo.LOG_ZajeciaTypUczestnictwa

EXECUTE sp_rename N'dbo.Tmp_LOG_ZajeciaTypUczestnictwa', N'LOG_ZajeciaTypUczestnictwa', 'OBJECT'

ALTER TABLE [dbo].[LOG_ZajeciaTypUczestnictwa] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_ZajeciaTypUczestnictwa] PRIMARY KEY  CLUSTERED
(
   [Log_ZajeciaTypUczestnictwaID]
) ON [PRIMARY]

END
GO




---------------------------------------------------------------wdro5---------------------------------------------------------


-------- pami�taj coby ten skrypt odpala� na bazie log�w  --------


----------------USR_UprawnianiaDefinicje---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_USR_UprawnianiaDefinicje]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_USR_UprawnianiaDefinicje] (
   [Log_USR_UprawnianiaDefinicjeID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_UprawnianiaDefinicje] [int] 
  ,[Org_Adres] [varchar] (250)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_USR_UprawnianiaDefinicje] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_USR_UprawnianiaDefinicje] PRIMARY KEY  CLUSTERED
(
   [Log_USR_UprawnianiaDefinicjeID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_USR_UprawnianiaDefinicje] (
   [Log_USR_UprawnianiaDefinicjeID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_UprawnianiaDefinicje] [int] 
  ,[Org_Adres] [varchar] (250)
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_UprawnianiaDefinicje ON

IF EXISTS (SELECT * FROM dbo.LOG_USR_UprawnianiaDefinicje)
EXEC('INSERT INTO dbo.Tmp_LOG_USR_UprawnianiaDefinicje (Log_USR_UprawnianiaDefinicjeID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_UprawnianiaDefinicje, Org_Adres, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_USR_UprawnianiaDefinicjeID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_UprawnianiaDefinicje, Org_Adres, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_USR_UprawnianiaDefinicje WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_UprawnianiaDefinicje OFF

DROP TABLE dbo.LOG_USR_UprawnianiaDefinicje

EXECUTE sp_rename N'dbo.Tmp_LOG_USR_UprawnianiaDefinicje', N'LOG_USR_UprawnianiaDefinicje', 'OBJECT'

ALTER TABLE [dbo].[LOG_USR_UprawnianiaDefinicje] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_USR_UprawnianiaDefinicje] PRIMARY KEY  CLUSTERED
(
   [Log_USR_UprawnianiaDefinicjeID]
) ON [PRIMARY]

END
GO




-------------------------------------------------------------wdro6-----------------------------------------------------------------



-------- pami�taj coby ten skrypt odpala� na bazie log�w  --------


----------------USR_UprawnianiaToRola---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_USR_UprawnianiaToRola]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_USR_UprawnianiaToRola] (
   [Log_USR_UprawnianiaToRolaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_UprawnianiaToRola] [int] 
  ,[Org_USR_RolaID] [int] 
  ,[Org_USR_UprawnieniaDefinicjeID] [int] 
  ,[Org_CzyMozeWykonac] [bit] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_USR_UprawnianiaToRola] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_USR_UprawnianiaToRola] PRIMARY KEY  CLUSTERED
(
   [Log_USR_UprawnianiaToRolaID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_USR_UprawnianiaToRola] (
   [Log_USR_UprawnianiaToRolaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_UprawnianiaToRola] [int] 
  ,[Org_USR_RolaID] [int] 
  ,[Org_USR_UprawnieniaDefinicjeID] [int] 
  ,[Org_CzyMozeWykonac] [bit] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_UprawnianiaToRola ON

IF EXISTS (SELECT * FROM dbo.LOG_USR_UprawnianiaToRola)
EXEC('INSERT INTO dbo.Tmp_LOG_USR_UprawnianiaToRola (Log_USR_UprawnianiaToRolaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_UprawnianiaToRola, Org_USR_RolaID, Org_USR_UprawnieniaDefinicjeID, Org_CzyMozeWykonac, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_USR_UprawnianiaToRolaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_UprawnianiaToRola, Org_USR_RolaID, Org_USR_UprawnieniaDefinicjeID, Org_CzyMozeWykonac, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_USR_UprawnianiaToRola WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_UprawnianiaToRola OFF

DROP TABLE dbo.LOG_USR_UprawnianiaToRola

EXECUTE sp_rename N'dbo.Tmp_LOG_USR_UprawnianiaToRola', N'LOG_USR_UprawnianiaToRola', 'OBJECT'

ALTER TABLE [dbo].[LOG_USR_UprawnianiaToRola] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_USR_UprawnianiaToRola] PRIMARY KEY  CLUSTERED
(
   [Log_USR_UprawnianiaToRolaID]
) ON [PRIMARY]

END
GO




-------------------------------------------------------------wdro7-------------------------------------------------------------------


-------- pami�taj coby ten skrypt odpala� na bazie log�w  --------


----------------USR_UprawnianiaToRola---------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_USR_UprawnianiaToRola]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_USR_UprawnianiaToRola] (
   [Log_USR_UprawnianiaToRolaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_UprawnianiaToRola] [int] 
  ,[Org_USR_RolaID] [int] 
  ,[Org_USR_UprawnieniaDefinicjeID] [int] 
  ,[Org_CzyMozeWykonac] [bit] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

ALTER TABLE [dbo].[LOG_USR_UprawnianiaToRola] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_USR_UprawnianiaToRola] PRIMARY KEY  CLUSTERED
(
   [Log_USR_UprawnianiaToRolaID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_USR_UprawnianiaToRola] (
   [Log_USR_UprawnianiaToRolaID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDUSR_UprawnianiaToRola] [int] 
  ,[Org_USR_RolaID] [int] 
  ,[Org_USR_UprawnieniaDefinicjeID] [int] 
  ,[Org_CzyMozeWykonac] [bit] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_UprawnianiaToRola ON

IF EXISTS (SELECT * FROM dbo.LOG_USR_UprawnianiaToRola)
EXEC('INSERT INTO dbo.Tmp_LOG_USR_UprawnianiaToRola (Log_USR_UprawnianiaToRolaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_UprawnianiaToRola, Org_USR_RolaID, Org_USR_UprawnieniaDefinicjeID, Org_CzyMozeWykonac, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji)
  SELECT Log_USR_UprawnianiaToRolaID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDUSR_UprawnianiaToRola, Org_USR_RolaID, Org_USR_UprawnieniaDefinicjeID, Org_CzyMozeWykonac, Org_Dodal, Org_DataDodania, Org_Zmodyfikowal, Org_DataModyfikacji FROM dbo.Log_USR_UprawnianiaToRola WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_USR_UprawnianiaToRola OFF

DROP TABLE dbo.LOG_USR_UprawnianiaToRola

EXECUTE sp_rename N'dbo.Tmp_LOG_USR_UprawnianiaToRola', N'LOG_USR_UprawnianiaToRola', 'OBJECT'

ALTER TABLE [dbo].[LOG_USR_UprawnianiaToRola] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_USR_UprawnianiaToRola] PRIMARY KEY  CLUSTERED
(
   [Log_USR_UprawnianiaToRolaID]
) ON [PRIMARY]

END
GO

