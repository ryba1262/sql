UPDATE dbo.Student
   SET NrAlbumu = NULL
      where IDStudent in (
  select Student.IDStudent
  from Student
   join Indeks   on StudentID = IDStudent
   join Studia   on StudiaID = IDStudia
   join Kierunek   on Kierunek1ID = IDKierunek
   join SystemStudiow  on SystemStudiowID = IDSystemStudiow
   join TrybStudiow  on TrybStudiowID = IDTrybStudiow
  where Student.NrAlbumu is not null
  GROUP BY Student.IDStudent
  HAVING min(cast(SystemStudiow.CzyPodyplomowe as int)) > 0
   )