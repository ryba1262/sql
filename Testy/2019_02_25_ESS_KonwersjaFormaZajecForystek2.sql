UPDATE [dbo].[FormaWymiar]
   SET [FormaZajecID] = 16
     
 WHERE IDFormaWymiar in (
 select FormaWymiar.IDFormaWymiar
from FormaWymiar

join FormaZajec on FormaZajecID = IDFormaZajec
join SemestrRealizacji on SemestrRealizacjiID = IDSemestrRealizacji
join KartaUzup on KartaUzupID = IDKartaUzup
join Przedmiot on PrzedmiotID = IDPrzedmiot

where Przedmiot.Kod in ('jknb 001','jap2 001','jwkp 001','jkk 004')
and FormaWymiar.FormaZajecID = 15
 )
GO


