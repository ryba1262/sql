USE [ESS_wdro]
GO

UPDATE [dbo].[FormaWymiar]
   SET [FormaZajecID] = 14
     
 WHERE IDFormaWymiar in (
 select FormaWymiar.IDFormaWymiar
from FormaWymiar

join FormaZajec on FormaZajecID = IDFormaZajec
join SemestrRealizacji on SemestrRealizacjiID = IDSemestrRealizacji
join KartaUzup on KartaUzupID = IDKartaUzup
join Przedmiot on PrzedmiotID = IDPrzedmiot

where Przedmiot.Kod in ('jja 018','jti 018','jupk 018','jja 018','jkkwp','jtk 001','jaut1 001','jkk 006',
'jja 018','jti 018','jupk 018','jja 018','jkk 001','jtk 001','jap2 001','jwkp 001','jwkp 002','jkk 003','jkk 004')
and FormaZajec.Symbol6Znakow like 'SA'
 )
GO


