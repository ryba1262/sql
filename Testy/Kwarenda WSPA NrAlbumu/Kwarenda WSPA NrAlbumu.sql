GO

with gba as (
 select NrAlbumu, COUNT(*) Liczba_albumow from Student
 where NrAlbumu is not null 
 group by NrAlbumu having COUNT(*) > 1
)
select st.NrAlbumu, st.IDStudent, ix.NrIndeksu, st.Nazwisko, st.Imie, st.PESEL, k.Nazwa, ss.Nazwa, ts.Nazwa, ix.Rok, ix.Semestr, st.DataDodania 
from Student as st 
 join Indeks as ix on StudentID = IDStudent
 join Studia on StudiaID = IDStudia
 join Kierunek as k on Kierunek1ID = IDKierunek
 join SystemStudiow as ss on SystemStudiowID = IDSystemStudiow
 join TrybStudiow as ts on TrybStudiowID = IDTrybStudiow
where st.NrAlbumu in (select distinct gba.NrAlbumu from gba)
order by NrAlbumu, NrIndeksu; 