/****** Script for SelectTopNRows command from SSMS  ******/
SELECT Imie, Nazwisko, Email 
      ,[ProtokolID], pt.Nazwa
	   ,sys.Nazwa as SystemStudiów
	  ,kier.Nazwa as Kierunek
	  ,sek.Nazwa as Sekcja
      ,[LiczbaWszystkichPoz] as Wszystkie
      ,[LiczbaZatwierdzonychPoz] as Zatwierdzone 
      ,[LiczbaWystawionychOcen] as WystawioneOceny
      ,[DataAktualizacji]
	 
  FROM [ESS].[dbo].[ProtokolStatystyka] prstat

  join Pracownik pr on IDPracownik = prstat.PracownikID
  join Protokol pt on prstat.ProtokolID  = IDProtokol
  join SystemStudiow sys on sys.IDSystemStudiow = pt.SystemStudiowID
  join Kierunek kier on kier.IDKierunek = pt.KierunekID
  join Sekcja sek on sek.IDSekcja = pt.SekcjaID

  where prstat.RokAkad = 2017 and prstat.SemestrID = 2 and LiczbaWszystkichPoz <> LiczbaWystawionychOcen