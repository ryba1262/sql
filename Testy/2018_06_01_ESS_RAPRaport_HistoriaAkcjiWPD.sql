declare @UserID int, @GrupaID int, @PodgrupaID int, @RaportID int
declare @Grupa varchar(50), @GrupaOpis varchar(200)
declare @Podgrupa varchar(50), @PodgrupaOpis varchar(200)
declare @Raport varchar(50), @RaportOpis varchar(200)
declare @Kwerenda varchar(MAX), @KwerendaUpdate int, @ParametryUpdate int
declare @LiczbaBand int, @Timeout int

set @Grupa = 'WPD'
set @GrupaOpis = 'Raporty dotyczące WPD.'
set @Podgrupa = 'Raporty aktywności w WPD'
set @PodgrupaOpis = 'Raporty aktywności w WPD.'
set @Raport = 'Historia Akcji WPD'
set @RaportOpis = 'Historia aktywności dydaktyka w WPD'
set @LiczbaBand = 5
set @Timeout = 60 * 30 --sekundy

set @ParametryUpdate = 1
set @KwerendaUpdate = 1
set @Kwerenda = '

declare @IDUSR_Profil int
set @IDUSR_Profil = :IDUSR_Profil

select USR_HistoriaAkcji.USR_ProfilID, USR_Profil.Imie, USR_Profil.Nazwisko, USR_HistoriaAkcji.Uri, USR_HistoriaAkcji.DaneWejsciowe, USR_HistoriaAkcji.DaneWyjsciowe, USR_HistoriaAkcji.DataDodania
from USR_HistoriaAkcji
join USR_Profil on USR_ProfilID  = IDUSR_Profil
where USR_ProfilID = @IDUSR_Profil
Order by DataDodania desc

'

set @UserID = coalesce((select IDUzytkownik from Uzytkownik where Login = 'ktybinkowska'),0)
select @GrupaID = IDRAPRaportGrupa from RAPRaportGrupa where Nazwa = @Grupa and RAPRaportGrupaID is NULL

if coalesce(@GrupaID,0) = 0
begin
  insert into RAPRaportGrupa(RAPRaportGrupaID, Nazwa, Opis, Dodal, DataDodania)
    values(NULL, @Grupa, @GrupaOpis, @UserID, GetDate())
  select @GrupaID = scope_identity()
end

select @PodgrupaID = IDRAPRaportGrupa from RAPRaportGrupa where Nazwa = @Podgrupa and RAPRaportGrupaID = @GrupaID

if coalesce(@PodgrupaID,0) = 0
begin
  insert into RAPRaportGrupa(RAPRaportGrupaID, Nazwa, Opis, Dodal, DataDodania)
    values(@GrupaID, @Podgrupa, @PodgrupaOpis, @UserID, GetDate())
  select @PodgrupaID = scope_identity()
end

select @RaportID = IDRAPRaport from RAPRaport where Nazwa = @Raport and RAPRaportGrupaID = @PodgrupaID

if coalesce(@RaportID,0) = 0
begin
  insert into RAPRaport(RAPRaportGrupaID, Nazwa, Opis, Kwerenda, TimeOut, LiczbaBand, Dodal, DataDodania)
    values(@PodgrupaID, @Raport, @RaportOpis, @Kwerenda, @Timeout, @LiczbaBand, @UserID, GetDate())
  select @RaportID = scope_identity()
end
else if @KwerendaUpdate = 1
begin
  update RAPRaport set Kwerenda = @Kwerenda, TimeOut = @Timeout, LiczbaBand = @LiczbaBand, Zmodyfikowal = @UserID, DataModyfikacji = GetDate()
    where IDRAPRaport = @RaportID
end

if @ParametryUpdate = 1
begin
  delete from RAPRaportParametr where RAPRaportID = @RaportID
 
end

GO