select Student.NrAlbumu, Indeks.NrIndeksu, Student.Nazwisko, Student.Imie, Kierunek.Nazwa as kierunek
,SystemStudiow.Nazwa as System, TrybStudiow.Nazwa as tryb, Program.IDProgram, Przedmiot.Nazwa as Przedmiot, Siatka.RokAkad as RokAkadSiatki 
from Student 
 join Indeks  on StudentID = IDStudent
 join Studia on StudiaID = IDStudia
 join Kierunek on Kierunek1ID = IDKierunek
 join SystemStudiow  on SystemStudiowID = IDSystemStudiow
 join TrybStudiow  on TrybStudiowID = IDTrybStudiow
 join Program on Program.KierunekID = Kierunek.IDKierunek
 join Siatka on Siatka.ProgramID = Program.IDProgram
 join SiatkaPoz on SiatkaPoz.SiatkaID = Siatka.IDSiatka
 join KartaUzup on KartaUzupID = IDKartaUzup
 join Przedmiot on PrzedmiotID = IDPrzedmiot
 where NrIndeksu = 136982 

 order by Przedmiot