select Student.NrAlbumu, Indeks.NrIndeksu, Student.Nazwisko, Student.Imie, Kierunek.Nazwa as kierunek, 
SystemStudiow.Nazwa as System, TrybStudiow.Nazwa as tryb, Indeks.Rok, Indeks.Semestr, Student.DataDodania, 
Program.SystemStudiowID, Program.IDProgram
from Student 
 join Indeks  on StudentID = IDStudent
 join Studia on StudiaID = IDStudia
 join Kierunek on Kierunek1ID = IDKierunek
 join SystemStudiow  on SystemStudiowID = IDSystemStudiow
 join TrybStudiow  on TrybStudiowID = IDTrybStudiow
 join Program on Program.KierunekID = Kierunek.IDKierunek
 where NrIndeksu = 136982 
