UPDATE [dbo].[TrybStudiow]
   SET

      FormaJakiej = 'niestacjonarnej'
 WHERE Forma like 'niestacjonarna'
GO


UPDATE [dbo].[TrybStudiow]
   SET

      FormaJakiej = 'stacjonarnej'
 WHERE Forma like 'stacjonarna'
GO


INSERT INTO [dbo].[Drzewko]
           (
		    IDDrzewko
           ,[Nazwa]
           ,[CzyCzesne]
           ,[CzyEwidencja]
           ,[CzyRekrutacja]
           ,[CzyRaporty]
           ,[CzyDzialNauki]
           ,[NazwaPola]
           ,[NazwaPolaDB]
           ,[NazwaPolaSzablon]
           ,[Tabela]
           ,[KluczObcy]
           ,[Funkcja]
           ,[WarunekWhere]
           ,[Joiny]
           ,[TabelaObcaID]
           ,[Priorytet]
           ,[OrderByField])
     VALUES
           (
		    1710
           ,'Forma studi�w jakiej'
           ,0
           ,0
           ,0
           ,0
           ,0
           ,'Forma studi�w'
           ,'FormaJakiej'
           ,'FormaStudiowOdmJakiej'
           ,'FormaStudiow'
           ,'Studia.TrybStudiowID'
           ,NULL
           ,NULL
           ,'join TrybStudiow FormaStudiow on Studia.TrybStudiowID = FormaStudiow.IDTrybStudiow'
           ,1109
           ,NULL
           ,NULL)
GO


UPDATE [dbo].[Raporty]
   SET 
      [RaportDef] = '1014,1027,1029,1030,1034,1062,1292,1294,1295,1306,1336,1353,1710'
     
  where NazwaSzablonu like 'WSHE_Cudzoziemiec_kontynuuje_studia'
GO