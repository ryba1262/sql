DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''TrybStudiow_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER TrybStudiow_INSERT
')

EXEC('
CREATE TRIGGER TrybStudiow_INSERT ON ['+@BazaDanych+'].[dbo].[TrybStudiow] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''TrybStudiow'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_TrybStudiow]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDTrybStudiow]
  ,[Org_Nazwa]
  ,[Org_NazwaAng]
  ,[Org_Miejscownik]
  ,[Org_Symbol]
  ,[Org_KodEGERIA]
  ,[Org_SiatkaDzielnik]
  ,[Org_Jakie]
  ,[Org_Forma]
  ,[Org_RPStawkaKolejnosc]
  ,[Org_RPPrzelicznikStd]
  ,[Org_NewNazwa]
  ,[Org_NewNazwaAng]
  ,[Org_NewMiejscownik]
  ,[Org_NewJakie]
  ,[Org_RekrutacjaNazwa]
  ,[Org_FormaJakiej]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
  ,[Org_Dodal]
  ,[Org_DataDodania]
)
SELECT
1
  ,@ID
 ,ins.IDTrybStudiow
 ,ins.Nazwa
 ,ins.NazwaAng
 ,ins.Miejscownik
 ,ins.Symbol
 ,ins.KodEGERIA
 ,ins.SiatkaDzielnik
 ,ins.Jakie
 ,ins.Forma
 ,ins.RPStawkaKolejnosc
 ,ins.RPPrzelicznikStd
 ,ins.NewNazwa
 ,ins.NewNazwaAng
 ,ins.NewMiejscownik
 ,ins.NewJakie
 ,ins.RekrutacjaNazwa
 ,ins.FormaJakiej
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
 ,ins.Dodal
 ,ins.DataDodania
FROM  inserted ins 
END
END
')

EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''TrybStudiow_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER TrybStudiow_UPDATE
')

EXEC('
CREATE TRIGGER TrybStudiow_UPDATE ON ['+@BazaDanych+'].[dbo].[TrybStudiow] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''TrybStudiow'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_TrybStudiow]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDTrybStudiow]
  ,[Org_Nazwa]
  ,[Org_NazwaAng]
  ,[Org_Miejscownik]
  ,[Org_Symbol]
  ,[Org_KodEGERIA]
  ,[Org_SiatkaDzielnik]
  ,[Org_Jakie]
  ,[Org_Forma]
  ,[Org_RPStawkaKolejnosc]
  ,[Org_RPPrzelicznikStd]
  ,[Org_NewNazwa]
  ,[Org_NewNazwaAng]
  ,[Org_NewMiejscownik]
  ,[Org_NewJakie]
  ,[Org_RekrutacjaNazwa]
  ,[Org_FormaJakiej]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
  ,[Org_Dodal]
  ,[Org_DataDodania]
)
SELECT
2
  ,@ID
 ,ins.IDTrybStudiow
 ,ins.Nazwa
 ,ins.NazwaAng
 ,ins.Miejscownik
 ,ins.Symbol
 ,ins.KodEGERIA
 ,ins.SiatkaDzielnik
 ,ins.Jakie
 ,ins.Forma
 ,ins.RPStawkaKolejnosc
 ,ins.RPPrzelicznikStd
 ,ins.NewNazwa
 ,ins.NewNazwaAng
 ,ins.NewMiejscownik
 ,ins.NewJakie
 ,ins.RekrutacjaNazwa
 ,ins.FormaJakiej
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
 ,ins.Dodal
 ,ins.DataDodania
FROM  inserted ins 
END
END
')

EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''TrybStudiow_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER TrybStudiow_DELETE
')

EXEC('
CREATE TRIGGER TrybStudiow_DELETE ON ['+@BazaDanych+'].[dbo].[TrybStudiow] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''TrybStudiow'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_TrybStudiow]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDTrybStudiow]
  ,[Org_Nazwa]
  ,[Org_NazwaAng]
  ,[Org_Miejscownik]
  ,[Org_Symbol]
  ,[Org_KodEGERIA]
  ,[Org_SiatkaDzielnik]
  ,[Org_Jakie]
  ,[Org_Forma]
  ,[Org_RPStawkaKolejnosc]
  ,[Org_RPPrzelicznikStd]
  ,[Org_NewNazwa]
  ,[Org_NewNazwaAng]
  ,[Org_NewMiejscownik]
  ,[Org_NewJakie]
  ,[Org_RekrutacjaNazwa]
  ,[Org_FormaJakiej]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
  ,[Org_Dodal]
  ,[Org_DataDodania]
)
SELECT
3
  ,@ID
 ,del.IDTrybStudiow
 ,del.Nazwa
 ,del.NazwaAng
 ,del.Miejscownik
 ,del.Symbol
 ,del.KodEGERIA
 ,del.SiatkaDzielnik
 ,del.Jakie
 ,del.Forma
 ,del.RPStawkaKolejnosc
 ,del.RPPrzelicznikStd
 ,del.NewNazwa
 ,del.NewNazwaAng
 ,del.NewMiejscownik
 ,del.NewJakie
 ,del.RekrutacjaNazwa
 ,del.FormaJakiej
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
 ,del.Dodal
 ,del.DataDodania
FROM  deleted del 
END
END
')

if not exists (select * from LOG_LogowanaTabela where Tabela = 'TrybStudiow')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('TrybStudiow', 1, 0,GetDate())
GO

