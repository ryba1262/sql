-----------------------------2018_09_06_ESS_TrybStudiow_Obiekty-----------------------------------
----------------TrybStudiow---------------
-- Nale�y zweryfikowa� constrainty--
--------------------------------------
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GrupaDziekanska_TrybStudiow]') AND parent_object_id = OBJECT_ID(N'[dbo].[GrupaDziekanska]'))
ALTER TABLE [dbo].[GrupaDziekanska] DROP CONSTRAINT [FK_GrupaDziekanska_TrybStudiow]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ProtokolEgz_TrybStudiow]') AND parent_object_id = OBJECT_ID(N'[dbo].[ProtokolEgz]'))
ALTER TABLE [dbo].[ProtokolEgz] DROP CONSTRAINT [FK_ProtokolEgz_TrybStudiow]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PublikowanaDanaOdbiorca_TrybStudiow]') AND parent_object_id = OBJECT_ID(N'[dbo].[PublikowanaDanaOdbiorca]'))
ALTER TABLE [dbo].[PublikowanaDanaOdbiorca] DROP CONSTRAINT [FK_PublikowanaDanaOdbiorca_TrybStudiow]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RODostepneStudia_TrybStudiow]') AND parent_object_id = OBJECT_ID(N'[dbo].[RODostepneStudia]'))
ALTER TABLE [dbo].[RODostepneStudia] DROP CONSTRAINT [FK_RODostepneStudia_TrybStudiow]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RPKODPoz_TrybStudiow]') AND parent_object_id = OBJECT_ID(N'[dbo].[RPKODPoz]'))
ALTER TABLE [dbo].[RPKODPoz] DROP CONSTRAINT [FK_RPKODPoz_TrybStudiow]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RPPrzelicznik_TrybStudiow]') AND parent_object_id = OBJECT_ID(N'[dbo].[RPPrzelicznik]'))
ALTER TABLE [dbo].[RPPrzelicznik] DROP CONSTRAINT [FK_RPPrzelicznik_TrybStudiow]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RPRozliczenieKolejnosc_TrybStudiow]') AND parent_object_id = OBJECT_ID(N'[dbo].[RPRozliczenieKolejnosc]'))
ALTER TABLE [dbo].[RPRozliczenieKolejnosc] DROP CONSTRAINT [FK_RPRozliczenieKolejnosc_TrybStudiow]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RPWniosekPoz_TrybStudiow]') AND parent_object_id = OBJECT_ID(N'[dbo].[RPWniosekPoz]'))
ALTER TABLE [dbo].[RPWniosekPoz] DROP CONSTRAINT [FK_RPWniosekPoz_TrybStudiow]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RSCennikDefStawki_TrybStudiow]') AND parent_object_id = OBJECT_ID(N'[dbo].[RSCennikDefStawki]'))
ALTER TABLE [dbo].[RSCennikDefStawki] DROP CONSTRAINT [FK_RSCennikDefStawki_TrybStudiow]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Siatka_TrybStudiow]') AND parent_object_id = OBJECT_ID(N'[dbo].[Siatka]'))
ALTER TABLE [dbo].[Siatka] DROP CONSTRAINT [FK_Siatka_TrybStudiow]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_SPSSzablon_TrybStudiow]') AND parent_object_id = OBJECT_ID(N'[dbo].[SPSSzablon]'))
ALTER TABLE [dbo].[SPSSzablon] DROP CONSTRAINT [FK_SPSSzablon_TrybStudiow]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Studia_TrybStudiow]') AND parent_object_id = OBJECT_ID(N'[dbo].[Studia]'))
ALTER TABLE [dbo].[Studia] DROP CONSTRAINT [FK_Studia_TrybStudiow]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Suplement_TrybStudiow]') AND parent_object_id = OBJECT_ID(N'[dbo].[Suplement]'))
ALTER TABLE [dbo].[Suplement] DROP CONSTRAINT [FK_Suplement_TrybStudiow]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_WPSOperacja_TrybStudiow]') AND parent_object_id = OBJECT_ID(N'[dbo].[WPSOperacja]'))
ALTER TABLE [dbo].[WPSOperacja] DROP CONSTRAINT [FK_WPSOperacja_TrybStudiow]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[TrybStudiow]') AND name = N'PK_TrybStudiow')
ALTER TABLE [dbo].[TrybStudiow] DROP CONSTRAINT [PK_TrybStudiow]


if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[TrybStudiow]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[TrybStudiow] (
   [IDTrybStudiow] [int]  IDENTITY(1,1) NOT NULL
  ,[Nazwa] [varchar] (30) NOT NULL
  ,[NazwaAng] [varchar] (30) NULL
  ,[Miejscownik] [varchar] (30) NOT NULL
  ,[Symbol] [char] (1) NOT NULL
  ,[KodEGERIA] [char] (1) NULL
  ,[SiatkaDzielnik] [int]  NULL
  ,[Jakie] [varchar] (30) NULL
  ,[Forma] [varchar] (50) NULL
  ,[RPStawkaKolejnosc] [int]  NULL
  ,[RPPrzelicznikStd] [float]  NULL
  ,[NewNazwa] [varchar] (30) NULL
  ,[NewNazwaAng] [varchar] (30) NULL
  ,[NewMiejscownik] [varchar] (30) NULL
  ,[NewJakie] [varchar] (30) NULL
  ,[RekrutacjaNazwa] [varchar] (50) NULL
  ,[FormaJakiej] [varchar] (20) NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[TrybStudiow] WITH NOCHECK ADD 
CONSTRAINT [PK_TrybStudiow] PRIMARY KEY  CLUSTERED
(
  IDTrybStudiow
) ON [PRIMARY]


End
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_TrybStudiow] (
   [IDTrybStudiow] [int]  IDENTITY(1,1) NOT NULL
  ,[Nazwa] [varchar] (30) NOT NULL
  ,[NazwaAng] [varchar] (30) NULL
  ,[Miejscownik] [varchar] (30) NOT NULL
  ,[Symbol] [char] (1) NOT NULL
  ,[KodEGERIA] [char] (1) NULL
  ,[SiatkaDzielnik] [int]  NULL
  ,[Jakie] [varchar] (30) NULL
  ,[Forma] [varchar] (50) NULL
  ,[RPStawkaKolejnosc] [int]  NULL
  ,[RPPrzelicznikStd] [float]  NULL
  ,[NewNazwa] [varchar] (30) NULL
  ,[NewNazwaAng] [varchar] (30) NULL
  ,[NewMiejscownik] [varchar] (30) NULL
  ,[NewJakie] [varchar] (30) NULL
  ,[RekrutacjaNazwa] [varchar] (50) NULL
  ,[FormaJakiej] [varchar] (20) NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
) ON [PRIMARY]

ALTER TABLE [dbo].[Tmp_TrybStudiow] WITH NOCHECK ADD 
CONSTRAINT [PK_TrybStudiow] PRIMARY KEY  CLUSTERED
(
  IDTrybStudiow
) ON [PRIMARY]

SET IDENTITY_INSERT dbo.Tmp_TrybStudiow ON

IF EXISTS (SELECT * FROM dbo.TrybStudiow)
EXEC('INSERT INTO dbo.Tmp_TrybStudiow (IDTrybStudiow, Nazwa, NazwaAng, Miejscownik, Symbol, KodEGERIA, SiatkaDzielnik, Jakie, Forma, RPStawkaKolejnosc, RPPrzelicznikStd, NewNazwa, NewNazwaAng, NewMiejscownik, NewJakie, RekrutacjaNazwa, Zmodyfikowal, DataModyfikacji, Dodal, DataDodania)
  SELECT IDTrybStudiow, Nazwa, NazwaAng, Miejscownik, Symbol, KodEGERIA, SiatkaDzielnik, Jakie, Forma, RPStawkaKolejnosc, RPPrzelicznikStd, NewNazwa, NewNazwaAng, NewMiejscownik, NewJakie, RekrutacjaNazwa, Zmodyfikowal, DataModyfikacji, Dodal, DataDodania FROM dbo.TrybStudiow WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_TrybStudiow OFF

DROP TABLE dbo.TrybStudiow

EXECUTE sp_rename N'dbo.Tmp_TrybStudiow', N'TrybStudiow', 'OBJECT'


END
ALTER TABLE [dbo].[WPSOperacja] WITH NOCHECK ADD CONSTRAINT [FK_WPSOperacja_TrybStudiow] FOREIGN KEY([TrybStudiowID]) REFERENCES [dbo].[TrybStudiow] ([IDTrybStudiow])

ALTER TABLE [dbo].[Suplement] WITH NOCHECK ADD CONSTRAINT [FK_Suplement_TrybStudiow] FOREIGN KEY([TrybStudiowID]) REFERENCES [dbo].[TrybStudiow] ([IDTrybStudiow])

ALTER TABLE [dbo].[Studia] WITH NOCHECK ADD CONSTRAINT [FK_Studia_TrybStudiow] FOREIGN KEY([TrybStudiowID]) REFERENCES [dbo].[TrybStudiow] ([IDTrybStudiow])

ALTER TABLE [dbo].[SPSSzablon] WITH NOCHECK ADD CONSTRAINT [FK_SPSSzablon_TrybStudiow] FOREIGN KEY([TrybStudiowID]) REFERENCES [dbo].[TrybStudiow] ([IDTrybStudiow])

ALTER TABLE [dbo].[Siatka] WITH NOCHECK ADD CONSTRAINT [FK_Siatka_TrybStudiow] FOREIGN KEY([TrybStudiowID]) REFERENCES [dbo].[TrybStudiow] ([IDTrybStudiow])

ALTER TABLE [dbo].[RSCennikDefStawki] WITH NOCHECK ADD CONSTRAINT [FK_RSCennikDefStawki_TrybStudiow] FOREIGN KEY([TrybStudiowID]) REFERENCES [dbo].[TrybStudiow] ([IDTrybStudiow])

ALTER TABLE [dbo].[RPWniosekPoz] WITH NOCHECK ADD CONSTRAINT [FK_RPWniosekPoz_TrybStudiow] FOREIGN KEY([TrybStudiowID]) REFERENCES [dbo].[TrybStudiow] ([IDTrybStudiow])

ALTER TABLE [dbo].[RPRozliczenieKolejnosc] WITH NOCHECK ADD CONSTRAINT [FK_RPRozliczenieKolejnosc_TrybStudiow] FOREIGN KEY([TrybStudiowID]) REFERENCES [dbo].[TrybStudiow] ([IDTrybStudiow])

ALTER TABLE [dbo].[RPPrzelicznik] WITH NOCHECK ADD CONSTRAINT [FK_RPPrzelicznik_TrybStudiow] FOREIGN KEY([TrybStudiowID]) REFERENCES [dbo].[TrybStudiow] ([IDTrybStudiow])

ALTER TABLE [dbo].[RPKODPoz] WITH NOCHECK ADD CONSTRAINT [FK_RPKODPoz_TrybStudiow] FOREIGN KEY([TrybStudiowID]) REFERENCES [dbo].[TrybStudiow] ([IDTrybStudiow])

ALTER TABLE [dbo].[RODostepneStudia] WITH NOCHECK ADD CONSTRAINT [FK_RODostepneStudia_TrybStudiow] FOREIGN KEY([TrybStudiowID]) REFERENCES [dbo].[TrybStudiow] ([IDTrybStudiow])

ALTER TABLE [dbo].[PublikowanaDanaOdbiorca] WITH NOCHECK ADD CONSTRAINT [FK_PublikowanaDanaOdbiorca_TrybStudiow] FOREIGN KEY([TrybStudiowID]) REFERENCES [dbo].[TrybStudiow] ([IDTrybStudiow])

ALTER TABLE [dbo].[ProtokolEgz] WITH NOCHECK ADD CONSTRAINT [FK_ProtokolEgz_TrybStudiow] FOREIGN KEY([TrybStudiowID]) REFERENCES [dbo].[TrybStudiow] ([IDTrybStudiow])

ALTER TABLE [dbo].[GrupaDziekanska] WITH NOCHECK ADD CONSTRAINT [FK_GrupaDziekanska_TrybStudiow] FOREIGN KEY([TrybStudiowID]) REFERENCES [dbo].[TrybStudiow] ([IDTrybStudiow])


