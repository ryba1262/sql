--------------------------------------
----------------TrybStudiow---------------
--------------------------------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_TrybStudiow]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_TrybStudiow] (
   [Log_TrybStudiowID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDTrybStudiow] [int] 
  ,[Org_Nazwa] [varchar] (30)
  ,[Org_NazwaAng] [varchar] (30)
  ,[Org_Miejscownik] [varchar] (30)
  ,[Org_Symbol] [char] (1)
  ,[Org_KodEGERIA] [char] (1)
  ,[Org_SiatkaDzielnik] [int] 
  ,[Org_Jakie] [varchar] (30)
  ,[Org_Forma] [varchar] (50)
  ,[Org_RPStawkaKolejnosc] [int] 
  ,[Org_RPPrzelicznikStd] [float] 
  ,[Org_NewNazwa] [varchar] (30)
  ,[Org_NewNazwaAng] [varchar] (30)
  ,[Org_NewMiejscownik] [varchar] (30)
  ,[Org_NewJakie] [varchar] (30)
  ,[Org_RekrutacjaNazwa] [varchar] (50)
  ,[Org_FormaJakiej] [varchar] (20)
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
) ON [PRIMARY]
ALTER TABLE [dbo].[LOG_TrybStudiow] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_TrybStudiow] PRIMARY KEY  CLUSTERED
(
   [Log_TrybStudiowID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_TrybStudiow] (
   [Log_TrybStudiowID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDTrybStudiow] [int] 
  ,[Org_Nazwa] [varchar] (30)
  ,[Org_NazwaAng] [varchar] (30)
  ,[Org_Miejscownik] [varchar] (30)
  ,[Org_Symbol] [char] (1)
  ,[Org_KodEGERIA] [char] (1)
  ,[Org_SiatkaDzielnik] [int] 
  ,[Org_Jakie] [varchar] (30)
  ,[Org_Forma] [varchar] (50)
  ,[Org_RPStawkaKolejnosc] [int] 
  ,[Org_RPPrzelicznikStd] [float] 
  ,[Org_NewNazwa] [varchar] (30)
  ,[Org_NewNazwaAng] [varchar] (30)
  ,[Org_NewMiejscownik] [varchar] (30)
  ,[Org_NewJakie] [varchar] (30)
  ,[Org_RekrutacjaNazwa] [varchar] (50)
  ,[Org_FormaJakiej] [varchar] (20)
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataModyfikacji] [datetime] 
  ,[Org_Dodal] [int] 
  ,[Org_DataDodania] [datetime] 
) ON [PRIMARY]
SET IDENTITY_INSERT dbo.Tmp_LOG_TrybStudiow ON

IF EXISTS (SELECT * FROM dbo.LOG_TrybStudiow)
EXEC('INSERT INTO dbo.Tmp_LOG_TrybStudiow (Log_TrybStudiowID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDTrybStudiow, Org_Nazwa, Org_NazwaAng, Org_Miejscownik, Org_Symbol, Org_KodEGERIA, Org_SiatkaDzielnik, Org_Jakie, Org_Forma, Org_RPStawkaKolejnosc, Org_RPPrzelicznikStd, Org_NewNazwa, Org_NewNazwaAng, Org_NewMiejscownik, Org_NewJakie, Org_RekrutacjaNazwa, Org_Zmodyfikowal, Org_DataModyfikacji, Org_Dodal, Org_DataDodania)
  SELECT Log_TrybStudiowID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDTrybStudiow, Org_Nazwa, Org_NazwaAng, Org_Miejscownik, Org_Symbol, Org_KodEGERIA, Org_SiatkaDzielnik, Org_Jakie, Org_Forma, Org_RPStawkaKolejnosc, Org_RPPrzelicznikStd, Org_NewNazwa, Org_NewNazwaAng, Org_NewMiejscownik, Org_NewJakie, Org_RekrutacjaNazwa, Org_Zmodyfikowal, Org_DataModyfikacji, Org_Dodal, Org_DataDodania FROM dbo.Log_TrybStudiow WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_TrybStudiow OFF

DROP TABLE dbo.LOG_TrybStudiow

EXECUTE sp_rename N'dbo.Tmp_LOG_TrybStudiow', N'LOG_TrybStudiow', 'OBJECT'

ALTER TABLE [dbo].[LOG_TrybStudiow] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_TrybStudiow] PRIMARY KEY  CLUSTERED
(
   [Log_TrybStudiowID]
) ON [PRIMARY]

END
GO


