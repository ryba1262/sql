/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [IDPlanPozPracownik]
      ,[PlanZajecPozID]
      ,[PracownikID]
	  ,prz.Kod as PRzedmiotKod
	  ,prz.Nazwa as PrzedmiotNazwa
	  ,pr.Nazwa
	  ,pr.IDProgram
      ,[LGodzSem]
      ,[KartaEgz]
      ,ppz.Dodal
      ,ppz.DataDodania
      ,ppz.Zmodyfikowal
      ,ppz.DataModyfikacji
  FROM [ESS].[dbo].[PlanPozPracownik] ppz

  join PlanZajecPoz on PlanZajecPozID = IDPlanZajecPoz
  join GrupaZajeciowa grz on GrupaZajeciowaID = IDGrupaZajeciowa
  join PlanZajec on PlanZajecID = IDPlanZajec
  join Siatka on SiatkaID = IDSiatka
  join Program pr on ProgramID = IDProgram
  join ProgramPoz pp on ParentID = IDProgram
  join Przedmiot prz on PrzedmiotID = IDPrzedmiot
  where PracownikID = 158945 and prz.Kod like 'oPOWS 002'