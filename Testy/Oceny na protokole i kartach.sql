/****** Script for SelectTopNRows command from SSMS  ******/
SELECT  [IDProtokolOcena]
      ,[ProtokolPozID]
	  ,kpoz.IDKartaEgzPoz
      ,[ProtokolOcenaDefID]
	  ,SkalaOcenPoz.Nazwa as ocenaNaProtokole
	  ,proce.DataDodania as DataDodaniaOcenyNaProtokole
      ,proce.Zmodyfikowal as ZmodyfikowalNaProtokole
      ,proce.DataModyfikacji as DataModyfikacjiNaProtokole
	  ,kpoz.OcenaKoncowaID as ocenaNaKarcie
	  ,kpoz.DataDodania as DataDodaniaOcenyNaKarcie
	  ,kpoz.DataModyfikacji as DataModyfikacjiOcenyNaKarcie
	  ,kpoz.Zmodyfikowal as ZmodyfikowałNaKarcie
      ,[Waga]
      ,proce.DataZaliczenia
      ,Pr.Dodal
      
	  
  FROM [ESS].[dbo].[ProtokolOcena] proce

  join ProtokolPoz on IDProtokolPoz = ProtokolPozID
  join Protokol pr on IDProtokol = ProtokolID
  join Indeks ind on IDIndeks = IndeksID
  join KartaEgzForma kform on IDKartaEgzForma = KartaEgzFormaID
  join KartaEgzPoz kpoz on IDKartaEgzPoz = KartaEgzPozID
  join SkalaOcenPoz on IDSkalaOcenPoz = proce.OcenaID
 
  where IDProtokol = 5519 and NrIndeksu = 40566