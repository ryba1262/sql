
UPDATE [dbo].[WUWydrukWersja]
   SET 
      [Szablon] = '<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:decimal-format name="pln"
decimal-separator="," grouping-separator="." NaN="" />
  <xsl:param name="dataWygenerowania"/>
  <xsl:template match="/Dane">
    <fo:root font-family="Times" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml">
      <fo:layout-master-set xmlns:rx="http://www.renderx.com/XSL/Extensions" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882">
        <fo:simple-page-master master-name="section1-first-page" page-width="8.268055555555555in" page-height="11.693055555555556in" margin-top="35.4pt" margin-bottom="14.05pt" margin-right="70.85pt" margin-left="70.85pt">
          <fo:region-body margin-top="55pt" margin-bottom="55pt"></fo:region-body>
          <fo:region-before region-name="first-page-header" extent="11in"></fo:region-before>
          <fo:region-after region-name="first-page-footer" extent="11in" display-align="after"></fo:region-after>
        </fo:simple-page-master>
        <fo:simple-page-master master-name="section1-odd-page" page-width="8.268055555555555in" page-height="11.693055555555556in" margin-top="35.4pt" margin-bottom="14.05pt" margin-right="70.85pt" margin-left="70.85pt">
          <fo:region-body margin-top="55pt" margin-bottom="55pt"></fo:region-body>
          <fo:region-before region-name="odd-page-header" extent="11in"></fo:region-before>
          <fo:region-after region-name="odd-page-footer" extent="11in" display-align="after"></fo:region-after>
        </fo:simple-page-master>
        <fo:simple-page-master master-name="section1-even-page" page-width="8.268055555555555in" page-height="11.693055555555556in" margin-top="35.4pt" margin-bottom="14.05pt" margin-right="70.85pt" margin-left="70.85pt">
          <fo:region-body margin-top="55pt" margin-bottom="55pt"></fo:region-body>
          <fo:region-before region-name="even-page-header" extent="11in"></fo:region-before>
          <fo:region-after region-name="even-page-footer" extent="11in" display-align="after"></fo:region-after>
        </fo:simple-page-master>
        <fo:page-sequence-master master-name="section1-page-sequence-master">
          <fo:repeatable-page-master-alternatives>
            <fo:conditional-page-master-reference odd-or-even="odd" master-reference="section1-odd-page" />
            <fo:conditional-page-master-reference odd-or-even="even" master-reference="section1-even-page" />
          </fo:repeatable-page-master-alternatives>
        </fo:page-sequence-master>
      </fo:layout-master-set>
      <fo:page-sequence master-reference="section1-page-sequence-master" format="1" xmlns:rx="http://www.renderx.com/XSL/Extensions" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882">
        <fo:static-content flow-name="first-page-header">
          <fo:retrieve-marker retrieve-class-name="first-page-header" retrieve-position="first-including-carryover" retrieve-boundary="page" />
        </fo:static-content>
        <fo:static-content flow-name="first-page-footer">
          <fo:retrieve-marker retrieve-class-name="first-page-footer" retrieve-position="first-including-carryover" retrieve-boundary="page" />
        </fo:static-content>
        <fo:static-content flow-name="odd-page-header">
          <fo:block font-family="Times" font-size="18pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
            <fo:inline>
              <fo:external-graphic content-width="186.1pt" content-height="51.45pt" src="https://www.ahe.lodz.pl/sites/default/files/logo_ckp.jpg" />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:external-graphic content-width="116.85pt" content-height="44.15pt" src="url(&quot;data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB&#xA;AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEB&#xA;AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wgARCAA7AJwDASIA&#xA;AhEBAxEB/8QAHgAAAQUBAQEBAQAAAAAAAAAAAAUGBwgJBAMKAQL/xAAaAQEAAwEBAQAAAAAAAAAA&#xA;AAAABQYHCAQD/9oADAMBAAIQAxAAAAHdiIK4xNR7roAZrL/N2p6FFeagw3ltFHlcqx9rw/07/sWt&#xA;714zOZW9QLAFXpTJOAAAACFsPforodnWqVQVK8z9z/v0eO+CXnZvtFeuUBX7m8zSVCtNJuhOc9V1&#xA;jJoNaXViW/TZ4x49jWNe+euRza9eyL10ObGnXDKHNNTspAK73cTbNDH9e3Fr83aS4tXLR1XGe7ha&#xA;DI7vxGUGikOEevLECaS9H3QonJK0M20Gg7wCEptKpJV5RbQFIncI7p6El/8AbXmWnYV2BANFgQAA&#xA;A8PcEBfAAD//xAAlEAACAgICAQQCAwAAAAAAAAAFBgMEAgcAARYIECA1EhQmMED/2gAIAQEAAQUC&#xA;JGRAaPrZK5NY80rc80rc80rc80rcat/iBOCj6ju7xr+zfeKwNr5JmvzUUi84r3AewKV6zzZTRPBy&#xA;gujUkQvAbbs9kjZCsYhcZL2FtjtXe6rrLcIDm3usUXredqz8n5JoP65aFuOsSYXaoyzGwdJDhWFv&#xA;lpfHKAOEXTNEybwf0+gjFYaUAXCLJ4gMwr2A6yJ5Mp17U8ikMlhHiq42T2vlBgrHkZQZKQ9ncoOE&#xA;TVVkk8EItXq9GuBCUGBr2zcJ9EFFcGAhqj9SbebILeiq7bCchdFyZi/VXcLBGPPbkfYAEzS9zB2+&#xA;xvBXwC7YtSWHE4dclBl2K++frNGxW9SnJpYq8UMnW02XDCKvEUK4sEOpIesoNvQR90A/5dCFH6k0&#xA;BVT+VcKoD8KdFNyqlAOtbzHXXEmoIqryEN7KjktjLuCGLbg6RrJOTlS8lJpOKZfA2L/GAV0dBLYh&#xA;rQ4Jghhgya60dRP1NPF+m7fyJu66666UfqWGsQiOC6JXOYetz2Mp1+MawZDy8dBiFnL05MQWn6tr&#xA;8sd4Ld7sH/gQVJ7l3wuxwjr6YiPFj3NNY0bVJalh4XY4GHZi6fzlijmjqChlDL/P/8QAKxEAAgIB&#xA;AgUDBAIDAAAAAAAAAQIDBBEFEgYHEyEiABRBICMxMkJRFTOx/9oACAEDAQE/AeAdJ0fXNUv1dbFj&#xA;28FESxNVyzpM08SK0q9avmPaz9lkLZ29vUvLXh7Y0tJPfwqNz9KzcSeMf3LWkcSAfnyj6qfO/wBU&#xA;+Xug3Z0gjqSZPdmNuzhEH7OfufHx+MnA+fXMDg7h/Q+GDqmi1bAnTVadFbktuWSOz1FnM6xQMxQo&#xA;pVF62B38ULYZvr5S2Xq69qHTrLbNrTPayQEbusjW67BNuGDHco/Kt/wi7BpVCeMo9mnfG0tXpzJZ&#xA;WJzjwYMrYb+LRGcjvjYFPqaGJrK0INtaS6os6g6Da/SRVUwRqC6xmVupI6BiBktl49oPNfVks8Ox&#xA;6fUiWKjW1Cts8fN2jjmVcbvJEXLEDs8jMZJfIhU+rlRLYi1fVmrPFFP/AIcrHJJJFE67rlYSdF5S&#xA;qpL0t43BlYIX2kN+aiRRWqyCVJ7U88MfUXLRVTLIil+oQOtON3iyZijP3Fd3C7J0MXFdhm3pHUzL&#xA;KyjOyGKmgLDv+pyAO4/f49cwm36IzYxu1CFsf1kTHH16Zq97R5ZJqEqxSSx9Jy0ccmU3K+MSKwHk&#xA;o7jv6TjriVGV1uxhkYOp9pV7MpBB/wBXwQPU3MvjCf3Jl1CAtb2idl06gjOqsXCbkrghdx8lUgNh&#xA;QRhQBqPEur6rX9rdsJJD1Fk2rBDGd6AhTujRW/ke2cfX/8QAKBEAAQQBAwMEAwEBAAAAAAAAAwEC&#xA;BAUGERITAAcUFSAhIwgiMVOC/9oACAECAQE/Ae7t2PC8OoL5tvfUy21yWtdYwMeqruGF448gmw4J&#xA;80C8SqNq7xPHIe9hGDG5jHKsvPe7hoUi2xPLqXMqqKzmlrVUsEFzXB/0ssflwh2IBouusiM2bERq&#xA;K5ZO1FXrDe6XeHNb+HQ1t3CG8+4sqWWlq1BAghRHSpp9IqfoJqojW7m8pniCjmuIi9do7iszAt1B&#xA;M+RcUdNDlMfmchkaGlraRk3zHVIIIY4nwK34Z5G1I5yO4mLM4ySV93dPObHGqGjp24/Hy6tvshbX&#xA;nx2QLkdKceEdUbE+mQ1SvcNn1mjnE7TcjRv+zrIqrtThd5CsKy8ybE8xGoykoMWPGyFtVKLt3QTq&#xA;ZnA57nO4TwH2ZBka7j8bx3N3W1DVzcmj4Hjbh43Z51FFkWfTBsHGmsqYwWItHWRgmlBhltjNl2Uy&#xA;EKSYQ0KrnmlwdBLgPc8FxnIcFxGH6RhdFQ2MYY1Yo5VmSIaINpTI77Axmu3vaF6+RJK90ueqme0I&#xA;Pd3gkGhYGeXSzqyFlbrSPDqj2FjX1R4kGREl+rSamXZnjRm2L4qLG3sMOSCISS+O5HuTrH4tTVXt&#xA;NHdYx7nJbO3rofkxCKepx7zpoQmm+d/La5Y0jljFiK+uryKkwUybMaJIp0kR/wAk7e0kPJHg46CR&#xA;dWMhE0bFooOIiZu0+GoEjCgjNRNE3HRP7r1+PZ/K7oS5O3Z5NRdH2a67OaVEJt/53ae/LMLx7Noc&#xA;aBkUQkuNEk+WBg5UmKrT8Tw7lfGIJzk4yPTa5Vb866aonUfsR21iyASo9PLGeMYcgBEuLXUZgvQg&#xA;3prLVNWvajk1RU+Or/DMNvx2aEx9IZ70EONdy4tnaNk2EeCRTBiIRZW6LC5lR5IoFRpUGAJHPAAQ&#xA;24v2qwrDrP1egrTxZ/jFicpLCdJbwGVjiN45ByD1VRs/bbuTT4X5X3//xABCEAACAgECBQEEBAkK&#xA;BwAAAAACAwEEBRESAAYTFCEiFSMxMgc0QVEIECRCYWJxk9EWICUwM0NSc4SxQERTcoGho//aAAgB&#xA;AQAGPwIHZfKY7FqZMitmRu1qQMKI1kQKyxcGUR5mB1mI4ZXoXaOR2FtE6WTpPhn6wQthzMTx9Sf+&#xA;8D+HH1J/7wP4cfUn/vA/hx9Sf+8D+HB1sHQjLZOD2F1HyGPr7SiD6jlrmbBfGBCtMhr87h02lSw3&#xA;NOGq0hyFhNZOTxj2dCsyywVJ7urbky6G4tXWQtaqHz25xrMf1mD5gzeOdlLxv9hUFsY/2dSr2WDc&#xA;yGROul9XuL6qqGLopdY7VrWiVmu9aPRFvABPYsge2uUrTlt02Dr1Ulqhb9fLVFVDYesdMR0Hjrcu&#xA;ZmctVDz7Kymkltj8xREUKn9PSOjM/Zu+32Zma54PLCXSJNrUUMb/AIBYyAJJl+at8RE6iINaUx+I&#xA;OWsWZ93bEe+JGsthTvCaS9nq6tnXcyI9XRkAjWHzxPMHMak3cs0duOxbIE1JeY7hExnUWOD5rDdJ&#xA;CuMTCt7ZAixOJAfOTvg68xahBaKYGdrIOgRjpgK6wNlYaCEnsVHzRwnDY3GIutZjSyG513s4gAeS&#xA;JWP5M6JnwMxrIx5+zTjGJx2M6mUyB3QOpYtQlFKcfOlmW2RUzqRrp0umrU4+bZPpnCCIuxtpHOFf&#xA;D5WquxvA9onJr6oQEPrNjSfUMeYmJidIKZpxVxlXTIFR6d7Mdvf0B3Tk4qTT0kyjytPW9R+7g9fP&#xA;GTxLnRZu2OasjXr99blFeljgJa1xDTFn95uXVqKHUz3eVxpJcxCR2T7XOWa49xY64gIgudtcekvo&#xA;I8+lOrdvn3k6/wA+xgL7SqyTVWqd5awa2ldRr03iB6QQyBsQ4INZMQ1oCxclBw6sRtXFZsom6gSd&#xA;ir4hPokt0dP1j5hbhCwrWdu2fVwIZpJUH+IJ1eCsVS+8unGtlX/ZA2P8z7OPOaxlbIAGlS8T113B&#xA;9sLcFiUk5Ez8VF6h1mVSBTOuTw+XILmRxa4DFWAbD1W4KQBYk8dYatQMGytmu5lYSUW1gRq/nXmd&#xA;mt6wB3RZZ9U1UN9UO0n/AJy3ujpiMbwAwSuINhjxA1UtZvLoY6kPnoIifmPzsEi06tp0ztH7S6aw&#xA;0dk5GLOeu61rd6fIqQGw+0pjP9mjqepp6dSyYgTNBBSl17wWrdCmvDHUO1j7CU2uvNomQr3i3T0i&#xA;At0lAfMI6FGnGPVUbeouxpPKtfqvGLu619Z6zGLat3Xn54NWkfKGwdR4wta9lhp2X8wrvUDyGSqJ&#xA;uZzOyJTCI7mBm69oaz2tMBbMDqER6plbbmVzNxa7S7cVX2a3bdRR71jMLprb0wnxEQ2J2+N3F1JM&#xA;taXszOcYyCR1QtSYH00lNedtfUPkmCZpJe988ZFqDcRZO8y+/qkEwLmwMEKtqw0X6fEHJl+vP465&#xA;5TI0MaFy2jH1Dv269MbV+1uitRrlYYuHW7EiXQrL3ObtLpgWk/isYlWRoMytRCrNrGLt1zyFas/w&#xA;ixYpCybKUOnwprFiDPzCn8eWt5Mh7brGHRkRYVoyj011KL+1Nn+H5YjUzkQEiht+tQpYHFbpGDTW&#xA;hSBGCn0rWuAm5Z/6px01QUSMknwvg3X33rEJWTbDm2BrpEFjJGe1KxIAgYmfLTmP8XC6dOp2+JqM&#xA;ZesKOSYztQJcLrOYySYROmEC4CL3THWoX6Q4p48zheK7YbFdKinRrYIgYyxG0Y3rn0KDUxBfrjaT&#xA;TjhB0h6ti5XS595gxDnQwBZAx8ekmNY2pGZj7Tkz9XH+pd/sHGP5fyvMFbE8pW/ous5QK2RfRpUX&#xA;8xL5lKvDQt2oWZWxxwzHbjY29KJZ0dfVx+Drjmc85jE2ufp+lv8AlHmMdVws3rf8l7L3Yj0W8ZYq&#xA;B0F1u293XDcphyW5m04+javnr9bN3cN+EVl+Spy17D4crlzHYlRdtZmOy6VDIbXbJuYwKlnYA+9k&#xA;pYZ8qqv85105SfwiXcscwIsNxKLaeSlW7Syq365qA6mOAQWJXiBRjp5ta68czPxOcJ9a1+ELnuSq&#xA;fMFf+T+mK5STXqWcfRxt/KCvAI7sTPtcvlmGhSuoxljyLl/R9jUc48y421zR9Ks8uXss/JfR7zFk&#xA;xxTcHVdCa9nletdwSlLsSTlosr9oC0mG78lbV45Sx2X54pXFLDP+17fLmZ5C5d5hyId7/QNp8c8V&#xA;sZRMFU5ivZDGV9IeLZcx59J5fQRcHmvMhy1n8v8ASRjObb/Nscv1WXLWJqR7Mr3r+NQrE9GtfKV4&#xA;2xj2pG2uAh0m7qBFXO5TnS6GPwn4TNjA07SIw8Y+lhQfXLEZfuipkv8AoFXWOhYa0qjAvNK9FqOh&#xA;K8zylT5qzXsTlrB8r+yb+JzP0YYO3zMOQxKLVnmW3kOcIRVycXnHO0eX19jW0iDXXIo6/Ms3udsj&#xA;Xda5V5fydehbZgQnPIsdbTBDtpjNqnitd6TxpRbKVbnWXL3fia9xipKVm1rTnaC1LGTYZlPiBAYk&#xA;imfhEcZTmO11Q5ap3WJxFPUgmzGuu9nwlZNVCXWtPee+XWFm1czIgsVoQkNoiMCtSlhHiIiNBABG&#xA;P0REcZHtNxcu4dTrWRux/Y5d1JZWIxlYvz6kGAzccOsNGOiBQB7mZu+c73vsoURz8dBFri8/rk7U&#xA;vv2j93GHs+Osu49A/wCW5O8//EEhf7Nf08YqD8H7Oo7o+4u2Vr/74/1Lv9g4UjmPDcv5o6i2WEKz&#xA;WOx2SKsopgWvUu8l0pWUgMMYECMyMQU+OMUePwvLqJwicjY5eCjj8Wk8cu8JFkywnSUHZDkNS74q&#xA;XSGzunuJPWeKFw8Hy7jSZkG8w1VWKOIS5OadOx+YXIhpGVbp73IpLuj8Qbp4irluT+UMjlcgibrs&#xA;lfwWBtNaySEQCxYs1zsMsughJe6SkwkZiZ1jiziamC5Vq4G9Z/K8bXxmJTiLlyekv8pprQNOxZno&#xA;JD3iyb7lQ/3YxFalSwfKFCaOS9p06lXGYarNPMSoEe0KyFJDt8lKVqT3ahG10gBe/aIxxbqcx8s4&#xA;DLWMJUrl32exOKvglF2d8KQ68lxpDf5MfQEnOvmeMbhsNn8Zy3gsLNhrcZj+XOTc3iD37CQZ0c5i&#xA;8jSodnttEs6qq+7u3y2S0HQ+TRirzDj8vft5y6vN18TYVlblyUsa8MWmqrGhUUKK0Vq9WpCK61L2&#xA;+fXNKvkuUuWcgjGVl0scm9gcVbVj6ao2qqUlvqmFWssYgVoRAKCI0EYjijlbGExD8njA6WNyLsbT&#xA;bfx6tCjp0bZpmxUDQzjYhix0MvHqn8WawhOKvGXxWQxkvHySe+qtrdWI1jd0+pu26xu02z8eH8uW&#xA;sQnKxXtOJdmhcBa5OYCPmeAz0z2RMdVaWBrOsFGmkRn7I08X8ZwmNYetj9W/eiRJoxPxBO1ReJ2g&#xA;cQXGWrUlhWSnHkC1JGBAEwQ9QYGPGkr37vtnWZnzxlakeHLdXeY/eLRYEF/84if2x9/GA5YT6grF&#xA;1r8j+YL9jnRP3EujX3j+mwI/HiIiNIiNIiPhER9nH+pd/sHB5GvjreQr2eWbeG/I+kbE2m2DaBMB&#xA;jVT0tCj1Dr9viZjScKDMRkKsYzl3JY9zLC1CtlhyS6cJkHHJQU+mN4hOv2ccrV8njSKrXwWSq3Yc&#xA;sCiu9zC6UTu3bHedyjiNQLQomJ4wVivhzyGORj/Z5sFVZ7k2FsXFS1Z60rIu3VAQL41NS16L+URk&#xA;cJOGyBMXzTGS71YoOiVTu+pvhkP6sFsnXbKfEfNMFqMZoSw57mWmdgdHCYhoPrCQyhzsqbRyK3mM&#xA;at2CUxPo+EyA80TFOww72DwSETprL7COl3AD58muYLfrxc7GiqlVtcjW6LWLBVWrOSYZaDYkdgdX&#xA;bpJNP4BGslpHHKNQqr67sVgrVN/UKuYMJdNaZYkq73aqkl+JZC58xpE/zbFobagh7JOBlZzMa/Zr&#xA;E8fXU/uz/jxeoHfSI3KlirJdI529dRL3/H4ju3R+zizjj5SyFrJeyioLimB2KmRPu91LIw9cbSri&#xA;oOh7v3mxexgpd1NljPZ+yiOYsxJNsrKOrNFRlv7feHo6h+mXQvVa4BaF+FzJ/XU/uz/jx2xsFs9U&#xA;2bhiRj1bfGk/s/qGJcsGqaBLapgwa2LONpgYFqJCQzMEMxpMeJ4I6OOo0zONpHVqoQZD8dpEsBmR&#xA;1iJ0mdNf+I//xAAhEAEBAQEBAAICAgMAAAAAAAABESEAMRBBIFEwYUBxof/aAAgBAQABPyECb1CS&#xA;tyEkBSPUoaEPJ4IRLv41q1as5p1nbjVM4KtPYJxzYXOjJZnpPdPP5FGxlcY7xWyW1J7GM6OG2hgx&#xA;F/7PI/cYZV5oJnJC+Q8LUr2LJk9u03yPRRiDHCaCxOzaRsuzjWu7ueqKW4JYKJ856isdQVqupGLV&#xA;KVvsR15kUUCdoybFNE24gaIu0j48BBPamFUmIZxEihmCUMUZ4gx8LMiDTvSirwH5NdlTrd+qXTVF&#xA;pRUpoS5emchiITqLM/a98CCrc5KLBF2rRHwqtHr+Gkp36IoSLvS8zn+IFWBwFeRUCsRUUx0S8h6V&#xA;YTYIzJPIwdK5za0xVXMJSUNCvI7laMMbMARVMAlAWjN1F/qiJtPV+hBRUUve3KRTchUwxS1QbBHJ&#xA;ylpgmbb8j/yplBU9peCJcvV3JqACFehEnzI1fqouqJB4epVS+y1GKsW90Y6HO/OAzgqFB10ELMSj&#xA;kMLn4fKwgo4w0CSQUTP6RJp93wkQtnzvsf8A9JKzj/iD0p4YPycX3VivVu8lcTYSlHrLQW3yLQsX&#xA;gFsksBgmPyCHMzt3ov7tUMSGGJcvXrAWfh3JPCXkJUXok/Vi63aguCoSZxXfgfHTd6McvLFyjGVh&#xA;bcrHLwZXCXs3GhC2IFLh33TTQlUmRGQV3g6jjw4bMgA30HDzBei4NEC0YB9ucDyNsf2PPFhjm5DS&#xA;ypb/AGxnonJ6IBAmxAi/6o+GW04OjVQBUspABdb1VCQSlzzc+QOdnBGRgaLODTOnAO9uTAcNjCn5&#xA;IjKevHFd46DYs+iTY+T7A35PuMhlBa8PJ3UEiVbpCPVEwfXO33Z/ovA2aGkwfUDkEDgwHGSGVkYQ&#xA;HFeHgdaF8dX09oAuXnwUW2wk/F/e2RYMoaTymOkAIXzourARF9nKVemfYYbX7lH/AIjt1VGsD/b2&#xA;agkNcCYYCAQAYAYH0fDONU/IK+MqzPPSE+n80SDZKeZ3mpsK8YAEuUhHFkEs8r8C3Zu4sDdA0bQV&#xA;5KcQRfO/peM78Lnci/TQe3NuGGjrxXGNulMIlOidPyLdoeGpT1J+AKK7DGoFz0+P9oEh2THtQH7H&#xA;CbRBjimov2NOV5oEzcfcOVEfj3/6jzuCClaff+BODgIIYnElII9v0LkB6PwSAy/5H//aAAwDAQAC&#xA;AAMAAAAQfAFVkIAAAAAE2gUkwEEg/XDrAUAMwgDzzgAAAAAA/8QAGhEBAQADAQEAAAAAAAAAAAAA&#xA;AREAITEgYf/aAAgBAwEBPxAbnseEhUjQEJdJThpZ5QodKIOgUPurpnUs6DBa7iHDwSC6TlX0NzR6&#xA;ZDInFFFJA+aJgh2NJyip1qwUgI0DAooYHpTyswbZadVUdlAVb2pn8rwf68IhAwMAVqK8XFHVRV/i&#xA;Tx0wPydq8NOqH1E9XrGuWWFg/fcqiCuiGCmBAlimC2ZNCHrKgUlMR2qCN6n1A7bKFzMfLhBAHoux&#xA;h7//xAAbEQEBAAMBAQEAAAAAAAAAAAABEQAgITFhgf/aAAgBAgEBPxAMnqIeyyHIewmJrHwgmxpo&#xA;OGO99xPLpeNRuc+EptwhuuTQncxDPmPoIz1iNIQ5T4VTkxKXd3lkD1MMYXPlrkZjcdDZbBrdpRBk&#xA;dkERCI1I9hbH4PHJM72jkL7mgKFFRsFSSWCGSKIcFeXhQFD8Nvs27Z6mikHQD4ORiNEBIggRKY9S&#xA;vyalTijOwkzEr05z5x6IApt//8QAIRABAAMAAgEEAwAAAAAAAAAAAQARIRAxMCBBUYFAYfD/2gAI&#xA;AQEAAT8QLTmtc8E78uCo1QK3oVCqLBBsQ5RIkSIqAarSHyw6d8Qsa7EkhtdvnCeVnOTCEYRAIKBE&#xA;RESxExE0TE8js7KkNdMjsrcBiqXQbA8KlUk2/wBfb2OTLdyz6fuTJxCkMD54AiI9JpkqCsOq2P4q&#xA;PnPTFEp7gOhiWPpVBYzg2ZHI+jAIgDoNGbgxzrypZsHgbnn9Luj+kdNo7xHm+M5RFA2lBqCtV+6J&#xA;UTH/AJp84vayK+BHsU+p/FgVng4LbU6tMxGcChb2K/GEdslglAroJa81oIIgeHnAun2majAlzDFR&#xA;DEThkQBkx++tpT9hk8ErykLD2cs0T5MRRLsZCNaqx+WKK13JgtOzIQWxsX7dg+Lke9BG0OJ2dWlC&#xA;ms5aDNGCppiH2+GEEhZU2KTehCIgcC1TbxBbD5xDgq5Dq1aiweQBw/QMKxLlX4x56vTZw0xNkgNF&#xA;X1zCCyT40pWBko7RGXqXVuXFKQk0hujO8k4hDfOR0kcOsXRAsn8y/d5oyNC7N8JoRPl1P2V78RKU&#xA;iZWAnRMMnxknmRyYoD32PzRYfutKKjXD/muPAkmOqeFoWquRB2xLvULDiyn1tLzMXWpT9IN/LmY2&#xA;hA243Sh5RlaZrpLyS8TKg99YZCagZxGLCm1QCuSfI01HEhIgCQw6IeIIFmZrb3d2dOPgtAEQPhwT&#xA;OhIyoAHcu0NjQxw9MNiCmdEWHRKdnR9P8/N8RhM1go9sZc0SO4d12RaWMJ/7zMOWw9YAq2tRKeuR&#xA;s11kbIF/K5Skvikz3FhaAP8AMDSzNYov3y6qECpEW7bgOylbpTjxkKs2lTqXX8x5ZMK4PsSWEMT9&#xA;cUXfkwIBxoLeraqtbg2lJYO58Dah3FAuKgW8LJLTAp3YaUPg/Pw41kID4anpaAzVQbwgoVZeiSZF&#xA;4NCDmpWmwx4QGwGAAAAAToi7tTnSzhdGxSTwyvgB7dCg9loZBFKUqRpAWgOU7P1GEXXoy+/Ysv6R&#xA;gwYMgYg2rBYG1tC0/wC3UAW4mDLVE70Z+LrAOO0eOEWwG8a824WR9ICexZwgk2kA3rh8RX1ATFna&#xA;oXuI+EdfBw3lvRQTTdSCjl+N3m9p9+9Tl3gHV2pEox8CJq5qsEQluUP0a8t3pIEVh/I//9k=&#xA;&quot;)" />
            </fo:inline>
          </fo:block>
        </fo:static-content>
        <fo:static-content flow-name="odd-page-footer">
          <fo:retrieve-marker retrieve-class-name="odd-page-footer" retrieve-position="first-including-carryover" retrieve-boundary="page" />
        </fo:static-content>
        <fo:static-content flow-name="even-page-header">
          <fo:block font-family="Times" font-size="18pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
            <fo:inline>
              <fo:external-graphic content-width="186.1pt" content-height="51.45pt" src="https://www.ahe.lodz.pl/sites/default/files/logo_ckp.jpg" />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:external-graphic content-width="116.85pt" content-height="44.15pt" src="url(&quot;data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB&#xA;AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEB&#xA;AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wgARCAA7AJwDASIA&#xA;AhEBAxEB/8QAHgAAAQUBAQEBAQAAAAAAAAAAAAUGBwgJBAMKAQL/xAAaAQEAAwEBAQAAAAAAAAAA&#xA;AAAABQYHCAQD/9oADAMBAAIQAxAAAAHdiIK4xNR7roAZrL/N2p6FFeagw3ltFHlcqx9rw/07/sWt&#xA;714zOZW9QLAFXpTJOAAAACFsPforodnWqVQVK8z9z/v0eO+CXnZvtFeuUBX7m8zSVCtNJuhOc9V1&#xA;jJoNaXViW/TZ4x49jWNe+euRza9eyL10ObGnXDKHNNTspAK73cTbNDH9e3Fr83aS4tXLR1XGe7ha&#xA;DI7vxGUGikOEevLECaS9H3QonJK0M20Gg7wCEptKpJV5RbQFIncI7p6El/8AbXmWnYV2BANFgQAA&#xA;A8PcEBfAAD//xAAlEAACAgICAQQCAwAAAAAAAAAFBgMEAgcAARYIECA1EhQmMED/2gAIAQEAAQUC&#xA;JGRAaPrZK5NY80rc80rc80rc80rcat/iBOCj6ju7xr+zfeKwNr5JmvzUUi84r3AewKV6zzZTRPBy&#xA;gujUkQvAbbs9kjZCsYhcZL2FtjtXe6rrLcIDm3usUXredqz8n5JoP65aFuOsSYXaoyzGwdJDhWFv&#xA;lpfHKAOEXTNEybwf0+gjFYaUAXCLJ4gMwr2A6yJ5Mp17U8ikMlhHiq42T2vlBgrHkZQZKQ9ncoOE&#xA;TVVkk8EItXq9GuBCUGBr2zcJ9EFFcGAhqj9SbebILeiq7bCchdFyZi/VXcLBGPPbkfYAEzS9zB2+&#xA;xvBXwC7YtSWHE4dclBl2K++frNGxW9SnJpYq8UMnW02XDCKvEUK4sEOpIesoNvQR90A/5dCFH6k0&#xA;BVT+VcKoD8KdFNyqlAOtbzHXXEmoIqryEN7KjktjLuCGLbg6RrJOTlS8lJpOKZfA2L/GAV0dBLYh&#xA;rQ4Jghhgya60dRP1NPF+m7fyJu66666UfqWGsQiOC6JXOYetz2Mp1+MawZDy8dBiFnL05MQWn6tr&#xA;8sd4Ld7sH/gQVJ7l3wuxwjr6YiPFj3NNY0bVJalh4XY4GHZi6fzlijmjqChlDL/P/8QAKxEAAgIB&#xA;AgUDBAIDAAAAAAAAAQIDBBEFEgYHEyEiABRBICMxMkJRFTOx/9oACAEDAQE/AeAdJ0fXNUv1dbFj&#xA;28FESxNVyzpM08SK0q9avmPaz9lkLZ29vUvLXh7Y0tJPfwqNz9KzcSeMf3LWkcSAfnyj6qfO/wBU&#xA;+Xug3Z0gjqSZPdmNuzhEH7OfufHx+MnA+fXMDg7h/Q+GDqmi1bAnTVadFbktuWSOz1FnM6xQMxQo&#xA;pVF62B38ULYZvr5S2Xq69qHTrLbNrTPayQEbusjW67BNuGDHco/Kt/wi7BpVCeMo9mnfG0tXpzJZ&#xA;WJzjwYMrYb+LRGcjvjYFPqaGJrK0INtaS6os6g6Da/SRVUwRqC6xmVupI6BiBktl49oPNfVks8Ox&#xA;6fUiWKjW1Cts8fN2jjmVcbvJEXLEDs8jMZJfIhU+rlRLYi1fVmrPFFP/AIcrHJJJFE67rlYSdF5S&#xA;qpL0t43BlYIX2kN+aiRRWqyCVJ7U88MfUXLRVTLIil+oQOtON3iyZijP3Fd3C7J0MXFdhm3pHUzL&#xA;KyjOyGKmgLDv+pyAO4/f49cwm36IzYxu1CFsf1kTHH16Zq97R5ZJqEqxSSx9Jy0ccmU3K+MSKwHk&#xA;o7jv6TjriVGV1uxhkYOp9pV7MpBB/wBXwQPU3MvjCf3Jl1CAtb2idl06gjOqsXCbkrghdx8lUgNh&#xA;QRhQBqPEur6rX9rdsJJD1Fk2rBDGd6AhTujRW/ke2cfX/8QAKBEAAQQBAwMEAwEBAAAAAAAAAwEC&#xA;BAUGERITAAcUFSAhIwgiMVOC/9oACAECAQE/Ae7t2PC8OoL5tvfUy21yWtdYwMeqruGF448gmw4J&#xA;80C8SqNq7xPHIe9hGDG5jHKsvPe7hoUi2xPLqXMqqKzmlrVUsEFzXB/0ssflwh2IBouusiM2bERq&#xA;K5ZO1FXrDe6XeHNb+HQ1t3CG8+4sqWWlq1BAghRHSpp9IqfoJqojW7m8pniCjmuIi9do7iszAt1B&#xA;M+RcUdNDlMfmchkaGlraRk3zHVIIIY4nwK34Z5G1I5yO4mLM4ySV93dPObHGqGjp24/Hy6tvshbX&#xA;nx2QLkdKceEdUbE+mQ1SvcNn1mjnE7TcjRv+zrIqrtThd5CsKy8ybE8xGoykoMWPGyFtVKLt3QTq&#xA;ZnA57nO4TwH2ZBka7j8bx3N3W1DVzcmj4Hjbh43Z51FFkWfTBsHGmsqYwWItHWRgmlBhltjNl2Uy&#xA;EKSYQ0KrnmlwdBLgPc8FxnIcFxGH6RhdFQ2MYY1Yo5VmSIaINpTI77Axmu3vaF6+RJK90ueqme0I&#xA;Pd3gkGhYGeXSzqyFlbrSPDqj2FjX1R4kGREl+rSamXZnjRm2L4qLG3sMOSCISS+O5HuTrH4tTVXt&#xA;NHdYx7nJbO3rofkxCKepx7zpoQmm+d/La5Y0jljFiK+uryKkwUybMaJIp0kR/wAk7e0kPJHg46CR&#xA;dWMhE0bFooOIiZu0+GoEjCgjNRNE3HRP7r1+PZ/K7oS5O3Z5NRdH2a67OaVEJt/53ae/LMLx7Noc&#xA;aBkUQkuNEk+WBg5UmKrT8Tw7lfGIJzk4yPTa5Vb866aonUfsR21iyASo9PLGeMYcgBEuLXUZgvQg&#xA;3prLVNWvajk1RU+Or/DMNvx2aEx9IZ70EONdy4tnaNk2EeCRTBiIRZW6LC5lR5IoFRpUGAJHPAAQ&#xA;24v2qwrDrP1egrTxZ/jFicpLCdJbwGVjiN45ByD1VRs/bbuTT4X5X3//xABCEAACAgECBQEEBAkK&#xA;BwAAAAACAwEEBRESAAYTFCEiFSMxMgc0QVEIECRCYWJxk9EWICUwM0NSc4SxQERTcoGho//aAAgB&#xA;AQAGPwIHZfKY7FqZMitmRu1qQMKI1kQKyxcGUR5mB1mI4ZXoXaOR2FtE6WTpPhn6wQthzMTx9Sf+&#xA;8D+HH1J/7wP4cfUn/vA/hx9Sf+8D+HB1sHQjLZOD2F1HyGPr7SiD6jlrmbBfGBCtMhr87h02lSw3&#xA;NOGq0hyFhNZOTxj2dCsyywVJ7urbky6G4tXWQtaqHz25xrMf1mD5gzeOdlLxv9hUFsY/2dSr2WDc&#xA;yGROul9XuL6qqGLopdY7VrWiVmu9aPRFvABPYsge2uUrTlt02Dr1Ulqhb9fLVFVDYesdMR0Hjrcu&#xA;ZmctVDz7Kymkltj8xREUKn9PSOjM/Zu+32Zma54PLCXSJNrUUMb/AIBYyAJJl+at8RE6iINaUx+I&#xA;OWsWZ93bEe+JGsthTvCaS9nq6tnXcyI9XRkAjWHzxPMHMak3cs0duOxbIE1JeY7hExnUWOD5rDdJ&#xA;CuMTCt7ZAixOJAfOTvg68xahBaKYGdrIOgRjpgK6wNlYaCEnsVHzRwnDY3GIutZjSyG513s4gAeS&#xA;JWP5M6JnwMxrIx5+zTjGJx2M6mUyB3QOpYtQlFKcfOlmW2RUzqRrp0umrU4+bZPpnCCIuxtpHOFf&#xA;D5WquxvA9onJr6oQEPrNjSfUMeYmJidIKZpxVxlXTIFR6d7Mdvf0B3Tk4qTT0kyjytPW9R+7g9fP&#xA;GTxLnRZu2OasjXr99blFeljgJa1xDTFn95uXVqKHUz3eVxpJcxCR2T7XOWa49xY64gIgudtcekvo&#xA;I8+lOrdvn3k6/wA+xgL7SqyTVWqd5awa2ldRr03iB6QQyBsQ4INZMQ1oCxclBw6sRtXFZsom6gSd&#xA;ir4hPokt0dP1j5hbhCwrWdu2fVwIZpJUH+IJ1eCsVS+8unGtlX/ZA2P8z7OPOaxlbIAGlS8T113B&#xA;9sLcFiUk5Ez8VF6h1mVSBTOuTw+XILmRxa4DFWAbD1W4KQBYk8dYatQMGytmu5lYSUW1gRq/nXmd&#xA;mt6wB3RZZ9U1UN9UO0n/AJy3ujpiMbwAwSuINhjxA1UtZvLoY6kPnoIifmPzsEi06tp0ztH7S6aw&#xA;0dk5GLOeu61rd6fIqQGw+0pjP9mjqepp6dSyYgTNBBSl17wWrdCmvDHUO1j7CU2uvNomQr3i3T0i&#xA;At0lAfMI6FGnGPVUbeouxpPKtfqvGLu619Z6zGLat3Xn54NWkfKGwdR4wta9lhp2X8wrvUDyGSqJ&#xA;uZzOyJTCI7mBm69oaz2tMBbMDqER6plbbmVzNxa7S7cVX2a3bdRR71jMLprb0wnxEQ2J2+N3F1JM&#xA;taXszOcYyCR1QtSYH00lNedtfUPkmCZpJe988ZFqDcRZO8y+/qkEwLmwMEKtqw0X6fEHJl+vP465&#xA;5TI0MaFy2jH1Dv269MbV+1uitRrlYYuHW7EiXQrL3ObtLpgWk/isYlWRoMytRCrNrGLt1zyFas/w&#xA;ixYpCybKUOnwprFiDPzCn8eWt5Mh7brGHRkRYVoyj011KL+1Nn+H5YjUzkQEiht+tQpYHFbpGDTW&#xA;hSBGCn0rWuAm5Z/6px01QUSMknwvg3X33rEJWTbDm2BrpEFjJGe1KxIAgYmfLTmP8XC6dOp2+JqM&#xA;ZesKOSYztQJcLrOYySYROmEC4CL3THWoX6Q4p48zheK7YbFdKinRrYIgYyxG0Y3rn0KDUxBfrjaT&#xA;TjhB0h6ti5XS595gxDnQwBZAx8ekmNY2pGZj7Tkz9XH+pd/sHGP5fyvMFbE8pW/ous5QK2RfRpUX&#xA;8xL5lKvDQt2oWZWxxwzHbjY29KJZ0dfVx+Drjmc85jE2ufp+lv8AlHmMdVws3rf8l7L3Yj0W8ZYq&#xA;B0F1u293XDcphyW5m04+javnr9bN3cN+EVl+Spy17D4crlzHYlRdtZmOy6VDIbXbJuYwKlnYA+9k&#xA;pYZ8qqv85105SfwiXcscwIsNxKLaeSlW7Syq365qA6mOAQWJXiBRjp5ta68czPxOcJ9a1+ELnuSq&#xA;fMFf+T+mK5STXqWcfRxt/KCvAI7sTPtcvlmGhSuoxljyLl/R9jUc48y421zR9Ks8uXss/JfR7zFk&#xA;xxTcHVdCa9nletdwSlLsSTlosr9oC0mG78lbV45Sx2X54pXFLDP+17fLmZ5C5d5hyId7/QNp8c8V&#xA;sZRMFU5ivZDGV9IeLZcx59J5fQRcHmvMhy1n8v8ASRjObb/Nscv1WXLWJqR7Mr3r+NQrE9GtfKV4&#xA;2xj2pG2uAh0m7qBFXO5TnS6GPwn4TNjA07SIw8Y+lhQfXLEZfuipkv8AoFXWOhYa0qjAvNK9FqOh&#xA;K8zylT5qzXsTlrB8r+yb+JzP0YYO3zMOQxKLVnmW3kOcIRVycXnHO0eX19jW0iDXXIo6/Ms3udsj&#xA;Xda5V5fydehbZgQnPIsdbTBDtpjNqnitd6TxpRbKVbnWXL3fia9xipKVm1rTnaC1LGTYZlPiBAYk&#xA;imfhEcZTmO11Q5ap3WJxFPUgmzGuu9nwlZNVCXWtPee+XWFm1czIgsVoQkNoiMCtSlhHiIiNBABG&#xA;P0REcZHtNxcu4dTrWRux/Y5d1JZWIxlYvz6kGAzccOsNGOiBQB7mZu+c73vsoURz8dBFri8/rk7U&#xA;vv2j93GHs+Osu49A/wCW5O8//EEhf7Nf08YqD8H7Oo7o+4u2Vr/74/1Lv9g4UjmPDcv5o6i2WEKz&#xA;WOx2SKsopgWvUu8l0pWUgMMYECMyMQU+OMUePwvLqJwicjY5eCjj8Wk8cu8JFkywnSUHZDkNS74q&#xA;XSGzunuJPWeKFw8Hy7jSZkG8w1VWKOIS5OadOx+YXIhpGVbp73IpLuj8Qbp4irluT+UMjlcgibrs&#xA;lfwWBtNaySEQCxYs1zsMsughJe6SkwkZiZ1jiziamC5Vq4G9Z/K8bXxmJTiLlyekv8pprQNOxZno&#xA;JD3iyb7lQ/3YxFalSwfKFCaOS9p06lXGYarNPMSoEe0KyFJDt8lKVqT3ahG10gBe/aIxxbqcx8s4&#xA;DLWMJUrl32exOKvglF2d8KQ68lxpDf5MfQEnOvmeMbhsNn8Zy3gsLNhrcZj+XOTc3iD37CQZ0c5i&#xA;8jSodnttEs6qq+7u3y2S0HQ+TRirzDj8vft5y6vN18TYVlblyUsa8MWmqrGhUUKK0Vq9WpCK61L2&#xA;+fXNKvkuUuWcgjGVl0scm9gcVbVj6ao2qqUlvqmFWssYgVoRAKCI0EYjijlbGExD8njA6WNyLsbT&#xA;bfx6tCjp0bZpmxUDQzjYhix0MvHqn8WawhOKvGXxWQxkvHySe+qtrdWI1jd0+pu26xu02z8eH8uW&#xA;sQnKxXtOJdmhcBa5OYCPmeAz0z2RMdVaWBrOsFGmkRn7I08X8ZwmNYetj9W/eiRJoxPxBO1ReJ2g&#xA;cQXGWrUlhWSnHkC1JGBAEwQ9QYGPGkr37vtnWZnzxlakeHLdXeY/eLRYEF/84if2x9/GA5YT6grF&#xA;1r8j+YL9jnRP3EujX3j+mwI/HiIiNIiNIiPhER9nH+pd/sHB5GvjreQr2eWbeG/I+kbE2m2DaBMB&#xA;jVT0tCj1Dr9viZjScKDMRkKsYzl3JY9zLC1CtlhyS6cJkHHJQU+mN4hOv2ccrV8njSKrXwWSq3Yc&#xA;sCiu9zC6UTu3bHedyjiNQLQomJ4wVivhzyGORj/Z5sFVZ7k2FsXFS1Z60rIu3VAQL41NS16L+URk&#xA;cJOGyBMXzTGS71YoOiVTu+pvhkP6sFsnXbKfEfNMFqMZoSw57mWmdgdHCYhoPrCQyhzsqbRyK3mM&#xA;at2CUxPo+EyA80TFOww72DwSETprL7COl3AD58muYLfrxc7GiqlVtcjW6LWLBVWrOSYZaDYkdgdX&#xA;bpJNP4BGslpHHKNQqr67sVgrVN/UKuYMJdNaZYkq73aqkl+JZC58xpE/zbFobagh7JOBlZzMa/Zr&#xA;E8fXU/uz/jxeoHfSI3KlirJdI529dRL3/H4ju3R+zizjj5SyFrJeyioLimB2KmRPu91LIw9cbSri&#xA;oOh7v3mxexgpd1NljPZ+yiOYsxJNsrKOrNFRlv7feHo6h+mXQvVa4BaF+FzJ/XU/uz/jx2xsFs9U&#xA;2bhiRj1bfGk/s/qGJcsGqaBLapgwa2LONpgYFqJCQzMEMxpMeJ4I6OOo0zONpHVqoQZD8dpEsBmR&#xA;1iJ0mdNf+I//xAAhEAEBAQEBAAICAgMAAAAAAAABESEAMRBBIFEwYUBxof/aAAgBAQABPyECb1CS&#xA;tyEkBSPUoaEPJ4IRLv41q1as5p1nbjVM4KtPYJxzYXOjJZnpPdPP5FGxlcY7xWyW1J7GM6OG2hgx&#xA;F/7PI/cYZV5oJnJC+Q8LUr2LJk9u03yPRRiDHCaCxOzaRsuzjWu7ueqKW4JYKJ856isdQVqupGLV&#xA;KVvsR15kUUCdoybFNE24gaIu0j48BBPamFUmIZxEihmCUMUZ4gx8LMiDTvSirwH5NdlTrd+qXTVF&#xA;pRUpoS5emchiITqLM/a98CCrc5KLBF2rRHwqtHr+Gkp36IoSLvS8zn+IFWBwFeRUCsRUUx0S8h6V&#xA;YTYIzJPIwdK5za0xVXMJSUNCvI7laMMbMARVMAlAWjN1F/qiJtPV+hBRUUve3KRTchUwxS1QbBHJ&#xA;ylpgmbb8j/yplBU9peCJcvV3JqACFehEnzI1fqouqJB4epVS+y1GKsW90Y6HO/OAzgqFB10ELMSj&#xA;kMLn4fKwgo4w0CSQUTP6RJp93wkQtnzvsf8A9JKzj/iD0p4YPycX3VivVu8lcTYSlHrLQW3yLQsX&#xA;gFsksBgmPyCHMzt3ov7tUMSGGJcvXrAWfh3JPCXkJUXok/Vi63aguCoSZxXfgfHTd6McvLFyjGVh&#xA;bcrHLwZXCXs3GhC2IFLh33TTQlUmRGQV3g6jjw4bMgA30HDzBei4NEC0YB9ucDyNsf2PPFhjm5DS&#xA;ypb/AGxnonJ6IBAmxAi/6o+GW04OjVQBUspABdb1VCQSlzzc+QOdnBGRgaLODTOnAO9uTAcNjCn5&#xA;IjKevHFd46DYs+iTY+T7A35PuMhlBa8PJ3UEiVbpCPVEwfXO33Z/ovA2aGkwfUDkEDgwHGSGVkYQ&#xA;HFeHgdaF8dX09oAuXnwUW2wk/F/e2RYMoaTymOkAIXzourARF9nKVemfYYbX7lH/AIjt1VGsD/b2&#xA;agkNcCYYCAQAYAYH0fDONU/IK+MqzPPSE+n80SDZKeZ3mpsK8YAEuUhHFkEs8r8C3Zu4sDdA0bQV&#xA;5KcQRfO/peM78Lnci/TQe3NuGGjrxXGNulMIlOidPyLdoeGpT1J+AKK7DGoFz0+P9oEh2THtQH7H&#xA;CbRBjimov2NOV5oEzcfcOVEfj3/6jzuCClaff+BODgIIYnElII9v0LkB6PwSAy/5H//aAAwDAQAC&#xA;AAMAAAAQfAFVkIAAAAAE2gUkwEEg/XDrAUAMwgDzzgAAAAAA/8QAGhEBAQADAQEAAAAAAAAAAAAA&#xA;AREAITEgYf/aAAgBAwEBPxAbnseEhUjQEJdJThpZ5QodKIOgUPurpnUs6DBa7iHDwSC6TlX0NzR6&#xA;ZDInFFFJA+aJgh2NJyip1qwUgI0DAooYHpTyswbZadVUdlAVb2pn8rwf68IhAwMAVqK8XFHVRV/i&#xA;Tx0wPydq8NOqH1E9XrGuWWFg/fcqiCuiGCmBAlimC2ZNCHrKgUlMR2qCN6n1A7bKFzMfLhBAHoux&#xA;h7//xAAbEQEBAAMBAQEAAAAAAAAAAAABEQAgITFhgf/aAAgBAgEBPxAMnqIeyyHIewmJrHwgmxpo&#xA;OGO99xPLpeNRuc+EptwhuuTQncxDPmPoIz1iNIQ5T4VTkxKXd3lkD1MMYXPlrkZjcdDZbBrdpRBk&#xA;dkERCI1I9hbH4PHJM72jkL7mgKFFRsFSSWCGSKIcFeXhQFD8Nvs27Z6mikHQD4ORiNEBIggRKY9S&#xA;vyalTijOwkzEr05z5x6IApt//8QAIRABAAMAAgEEAwAAAAAAAAAAAQARIRAxMCBBUYFAYfD/2gAI&#xA;AQEAAT8QLTmtc8E78uCo1QK3oVCqLBBsQ5RIkSIqAarSHyw6d8Qsa7EkhtdvnCeVnOTCEYRAIKBE&#xA;RESxExE0TE8js7KkNdMjsrcBiqXQbA8KlUk2/wBfb2OTLdyz6fuTJxCkMD54AiI9JpkqCsOq2P4q&#xA;PnPTFEp7gOhiWPpVBYzg2ZHI+jAIgDoNGbgxzrypZsHgbnn9Luj+kdNo7xHm+M5RFA2lBqCtV+6J&#xA;UTH/AJp84vayK+BHsU+p/FgVng4LbU6tMxGcChb2K/GEdslglAroJa81oIIgeHnAun2majAlzDFR&#xA;DEThkQBkx++tpT9hk8ErykLD2cs0T5MRRLsZCNaqx+WKK13JgtOzIQWxsX7dg+Lke9BG0OJ2dWlC&#xA;ms5aDNGCppiH2+GEEhZU2KTehCIgcC1TbxBbD5xDgq5Dq1aiweQBw/QMKxLlX4x56vTZw0xNkgNF&#xA;X1zCCyT40pWBko7RGXqXVuXFKQk0hujO8k4hDfOR0kcOsXRAsn8y/d5oyNC7N8JoRPl1P2V78RKU&#xA;iZWAnRMMnxknmRyYoD32PzRYfutKKjXD/muPAkmOqeFoWquRB2xLvULDiyn1tLzMXWpT9IN/LmY2&#xA;hA243Sh5RlaZrpLyS8TKg99YZCagZxGLCm1QCuSfI01HEhIgCQw6IeIIFmZrb3d2dOPgtAEQPhwT&#xA;OhIyoAHcu0NjQxw9MNiCmdEWHRKdnR9P8/N8RhM1go9sZc0SO4d12RaWMJ/7zMOWw9YAq2tRKeuR&#xA;s11kbIF/K5Skvikz3FhaAP8AMDSzNYov3y6qECpEW7bgOylbpTjxkKs2lTqXX8x5ZMK4PsSWEMT9&#xA;cUXfkwIBxoLeraqtbg2lJYO58Dah3FAuKgW8LJLTAp3YaUPg/Pw41kID4anpaAzVQbwgoVZeiSZF&#xA;4NCDmpWmwx4QGwGAAAAAToi7tTnSzhdGxSTwyvgB7dCg9loZBFKUqRpAWgOU7P1GEXXoy+/Ysv6R&#xA;gwYMgYg2rBYG1tC0/wC3UAW4mDLVE70Z+LrAOO0eOEWwG8a824WR9ICexZwgk2kA3rh8RX1ATFna&#xA;oXuI+EdfBw3lvRQTTdSCjl+N3m9p9+9Tl3gHV2pEox8CJq5qsEQluUP0a8t3pIEVh/I//9k=&#xA;&quot;)" />
            </fo:inline>
          </fo:block>
        </fo:static-content>
        <fo:static-content flow-name="even-page-footer">
          <fo:retrieve-marker retrieve-class-name="odd-page-footer" retrieve-position="first-including-carryover" retrieve-boundary="page" />
        </fo:static-content>
        <fo:static-content flow-name="xsl-footnote-separator">
          <fo:block>
            <fo:leader leader-pattern="rule" leader-length="144pt" rule-thickness="0.5pt" rule-style="solid" color="gray" />
          </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">
          <fo:block widows="2" orphans="2" font-size="10pt" line-height="1.147" white-space-collapse="false">
            <fo:marker marker-class-name="first-page-header" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:marker marker-class-name="first-page-footer" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:marker marker-class-name="odd-page-header" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:block font-family="Arial" font-size="12pt" language="PL">
                <fo:inline>
                  <fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline>
                  <fo:block />
                </fo:inline>
                <fo:inline>
                  <fo:leader leader-pattern="space"  />
                </fo:inline>
              </fo:block>
            </fo:marker>
            <fo:marker marker-class-name="odd-page-footer" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:block font-family="Arial" font-size="7pt" language="PL" line-height="1.147" text-align="center">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="PL" line-height="1.147" text-align="center">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="PL" line-height="1.147" text-align="center" color="#999999">
                <fo:inline color="#999999" font-size="7pt">
                  <fo:leader leader-length="0pt" />Centrum Kształcenia Podyplomowego Akademii Humanistyczno-Ekonomicznej w Łodzi
                </fo:inline>
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="DE" line-height="1.147" text-align="center" color="#999999">
                <fo:inline color="#999999" font-size="7pt" language="DE">
                  <fo:leader leader-length="0pt" />ul. Sterlinga 26, 90-212 Łódź; Tel: (+48 42) 63 15 039, (+48 42) 29 95 637.
                </fo:inline>
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="EN-US" line-height="1.147" text-align="center" color="#999999">
                <fo:inline color="#999999" font-size="7pt" language="EN-US">
                  <fo:leader leader-length="0pt" />mail: ckp@ahe.lodz.pl,
                </fo:inline>
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="EN-US" line-height="1.147" text-align="center" color="#999999">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="EN-US" line-height="1.147" text-align="center">
                <fo:inline color="#999999" font-size="7pt" language="EN-US">
                  <fo:leader leader-length="0pt" />www.ckp-lodz.pl
                </fo:inline>
              </fo:block>
            </fo:marker>
            <fo:marker marker-class-name="even-page-header" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:marker marker-class-name="even-page-footer" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:block font-family="Times" font-size="18pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" space-before="5pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />UMOWA<fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />dotycząca studiów podyplomowych<fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" /><xsl:value-of select="Kierunek"/><fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />zawarta w dniu <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="BiezacaData" />
              <fo:inline font-family="Arial" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="$dataWygenerowania"/>
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" /> pomiędzy
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />Akademią Humanistyczno - Ekonomiczną w Łodzi <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" /> wpisaną pod numerem 30 do Rejestru Uczelni Niepublicznych prowadzonego przez ministra właściwego do spraw szkolnictwa wyższego - zwaną dalej <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />AHE<fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />  lub<fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" /> Uczelnią<fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />,  reprezentowaną na podstawie stosownego pełnomocnictwa, przez: <fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Verdana" font-size="10pt" font-weight="bold">
                <fo:leader leader-length="0pt" />Dyrektora Centrum Kształcenia Podyplomowego Paulinę Gocała<fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />a
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />Panem/ią <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="Student" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="Student"/>
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" start-indent="72pt" text-indent="-72pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />zwanym/ą dalej Słuchaczem,
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" start-indent="72pt" text-indent="-72pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />zameldowanym/ą <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="AdresSP" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="AdresSP"/>
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block id="DOSeriaNumer" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:block id="DOTSNazwa" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="DOTSNazwa"/>
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" /> nr <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="DOTSSeriaNumer" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="DOTSSeriaNumer"/>,
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="-10pt" />PESEL<fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:inline color="white">-</fo:inline>
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="PESEL"/>.
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 1.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />AHE zobowiązuje się do zorganizowania kształcenia zgodnie z wymaganiami studiów<fo:inline color="white">--------------</fo:inline> podyplomowych: <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline id="Kierunek_1" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />
                      <xsl:value-of select="Kierunek"/>
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Kształcenie, o którym mowa w niniejszej umowie, prowadzone jest:
                    </fo:inline>
                  </fo:block>
                  <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
                    <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" start-indent="45pt">
                      <fo:list-item-label font-size="10pt" start-indent="38pt" text-indent="0pt" font-family="Tahoma">
                        <fo:block>a.</fo:block>
                      </fo:list-item-label>
                      <fo:list-item-body end-indent="inherit" start-indent="50pt" text-indent="0pt">
                        <fo:block>
                          <fo:inline font-family="Tahoma" font-size="10pt">
                            <fo:leader leader-length="0pt" />w formie z wykorzystaniem metod i technik nauczania na odległość,<fo:leader leader-length="0pt" />
                          </fo:inline>
                        </fo:block>
                      </fo:list-item-body>
                    </fo:list-item>
                  </fo:list-block>
                  <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
                    <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" start-indent="45pt">
                      <fo:list-item-label font-size="10pt" start-indent="38pt" text-indent="0pt" font-family="Tahoma">
                        <fo:block>b.</fo:block>
                      </fo:list-item-label>
                      <fo:list-item-body end-indent="inherit" start-indent="50pt" text-indent="0pt">
                        <fo:block>
                          <fo:inline font-family="Tahoma" font-size="10pt">
                            <fo:leader leader-length="0pt" />w trakcie zjazdów.
                          </fo:inline>
                        </fo:block>
                      </fo:list-item-body>
                    </fo:list-item>
                  </fo:list-block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>3.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Słuchacz wyraża zgodę na świadczenie przez Uczelnię usług określonych w ust. 1 drogą elektroniczną oraz na otrzymywanie tą drogą wszelkich informacji i materiałów związanych z realizacją studiów podyplomowych.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>4.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Słuchacz oświadcza, iż przed podpisaniem niniejszej umowy zapoznał się z Regulaminem Studiów Podyplomowych wraz z załącznikami (które dostępne są w Centrum Kształcenia Podyplomowego i na stronie internetowej www.ckp-lodz.pl) oraz Regulaminem Rekrutacji Online, jeśli uczestniczył w rekrutacji online (który dostępny jest na stronie internetowej rekrutacji online www.rekrutacja.ahe.lodz.pl) oraz zobowiązuje się przestrzegać ich postanowień. Słuchacz oświadcza, iż zapoznał się z prawem odstąpienia od umowy zawartej na odległość lub poza lokalem Uczelni.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>5.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Słuchacz zobowiązany jest na mocy umowy wnieść określone opłaty.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>6.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Opłatami, o których mowa w ust. 5 są:
                    </fo:inline>
                  </fo:block>
                  <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
                    <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" start-indent="45pt">
                      <fo:list-item-label font-size="10pt" start-indent="38pt" text-indent="0pt" font-family="Tahoma">
                        <fo:block>a.</fo:block>
                      </fo:list-item-label>
                      <fo:list-item-body end-indent="inherit" start-indent="50pt" text-indent="0pt">
                        <fo:block>
                          <fo:inline font-family="Tahoma" font-size="10pt">
                            <fo:leader leader-length="0pt" />czesne<fo:leader leader-length="0pt" />
                          </fo:inline>
                        </fo:block>
                      </fo:list-item-body>
                    </fo:list-item>
                  </fo:list-block>
                  <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
                    <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" start-indent="45pt">
                      <fo:list-item-label font-size="10pt" start-indent="38pt" text-indent="0pt" font-family="Tahoma">
                        <fo:block>b.</fo:block>
                      </fo:list-item-label>
                      <fo:list-item-body end-indent="inherit" start-indent="50pt" text-indent="0pt">
                        <fo:block>
                          <fo:inline font-family="Tahoma" font-size="10pt">
                            <fo:leader leader-length="0pt" />inne opłaty określone w cenniku opłat za usługi nieobjęte czesnym stanowiącym załącznik do Umowy.
						  </fo:inline>
                        </fo:block>
                      </fo:list-item-body>
                    </fo:list-item>
                  </fo:list-block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
			  <fo:block font-family="Tahoma" font-size="12pt" text-align="justify" language="PL" start-indent="18pt" text-indent="-18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-family="Tahoma" font-size="10pt">
					  <fo:leader leader-length="0pt" />7.  Opłaty, o których mowa w ust. 6b płatne są w przypadkach wskazanych w Cenniku i w terminach w nim podanych.<fo:leader leader-length="0pt" />
				  </fo:inline>
			  </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 2.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Studia podyplomowe będące przedmiotem niniejszej umowy trwają <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline id="UmowaLiczbaSem" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" /><xsl:value-of select="UmowaLiczbaSem"/>
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> semestry akademickie. Czas trwania studiów podyplomowych może ulec zmianie w przypadku zmiany przepisów regulujących zasady organizacji i przeprowadzania studiów podyplomowych.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" start-indent="17.85pt" end-indent="-5.4pt" text-indent="-17.85pt" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Semestr zimowy rozpoczyna się w miesiącu październiku, natomiast semestr letni w miesiącu marcu.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" start-indent="17.85pt" end-indent="-5.4pt" text-indent="-17.85pt" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>3.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Zajęcia w semestrze zimowym rozpoczynają się najpóźniej w miesiącu listopadzie, natomiast                         w semestrze letnim najpóźniej w miesiącu kwietniu.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" start-indent="17.85pt" end-indent="-5.4pt" text-indent="-17.85pt" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>4.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Nieuczestniczenie w zajęciach dydaktycznych nie zwalnia Słuchacza z obowiązku uiszczania opłat za studia podyplomowe.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>5.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Słuchacz zobowiązany jest do<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> zapłaty czesnego<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> w wysokości <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline id="UmowaCzesne10Miesiec" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" /> <xsl:value-of select="format-number(UmowaCzesne10Miesiec, &quot;##0,00&quot;, &quot;pln&quot;)"/> zł<fo:leader leader-length="0pt" />,<fo:leader leader-length="0pt" />
                  </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> za cały cykl studiów podyplomowych.<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" color="#3366FF" font-size="10pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> Płatność dokonywana jest w <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />10<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> równych miesięcznych ratach, po <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline id="UmowaCzesne" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" /><xsl:value-of select="format-number(UmowaCzesne, &quot;##0,00&quot;, &quot;pln&quot;)"/> zł<fo:leader leader-length="0pt" />
                  </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> każda.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>6.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Zapłaty czesnego należy dokonywać do<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" /> 5-go<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> dnia każdego miesiąca, począwszy od drugiego miesiąca każdego semestru, w którym rozpoczynają się studia tj. dla studiów rozpoczynających się w semestrze zimowym płatność następuje od listopada, a dla studiów rozpoczynających się w semestrze letnim od miesiąca kwietnia. W przypadku niedokonania wpłaty czesnego, oraz innych opłat, o których mowa w § 1 ust. 6b w terminie, wskazanym w ust. 6, Słuchacz zobowiązany jest do zapłaty zaległej kwoty powiększonej o odsetki umowne za opóźnienie. Odsetki umowne za opóźnienie wynoszą w stosunku rocznym dwukrotność odsetek ustawowych za opóźnienie od każdej zaległej kwoty, nie więcej jednak niż czterokrotność wysokości stopy kredytu lombardowego Narodowego Banku Polskiego.<fo:leader leader-length="0pt" />
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>7.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku przyjęcia na studia podyplomowe po upływie terminu płatności za studia w danym miesiącu, Słuchacz zobowiązuje się do wpłaty zaległych rat czesnego w ciągu siedmiu dni licząc od daty podpisania niniejszej umowy.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>8.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Wpłaty  należy  dokonywać  przekazem pocztowym lub przelewem na indywidualny numer konta
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />PKO BP nr rachunku <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline id="NrRORPM3" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" /><xsl:value-of select="NrRORPM3PKO"/>
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" start-indent="18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />Blankiety wpłaty czesnego powinny być wypełnione czytelnie z podaniem nazwiska i imienia, przeznaczenia (opłata za studia podyplomowe, odsetki) wpłaconej kwoty oraz z dopiskiem <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />studia podyplomowe: <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="Kierunek_2" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" /><xsl:value-of select="Kierunek"/><fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>9.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Za datę dokonania wpłaty za studia podyplomowe uznaje się datę wpływu wpłaty na rachunek bankowy Uczelni.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Times" font-size="12pt" language="PL" text-align="left" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 3.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Nie później niż na pięć dni przed dniem, w którym zgodnie z ofertą <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />AHE miały rozpocząć się <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />zajęcia w ramach studiów objętych niniejszą umową <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-size="9pt">
                      <fo:leader leader-length="0pt" />, <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />AHE ma prawo do odstąpienia od umowy w przypadku, gdy organizacja studiów będzie nieopłacalna i zostanie podjęta decyzja <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />o nie prowadzeniu w danym roku akademickim zajęć w ramach studiów podyplomowych objętych niniejszą umową. <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-size="9.5pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Decyzję <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />o nie prowadzeniu w danym roku akademickim zajęć w ramach studiów podyplomowych objętych niniejszą umową <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-size="9pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />oraz o odstąpieniu od umowy podejmuje <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Kierownik <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Centrum Kształcenia Podyplomowego, działając w ramach umocowania udzielonego przez właściwy organ AHE<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Oświadczenie woli o odstąpieniu od umowy przez Uczelnię może być złożone Słuchaczowi na piśmie lub za pośrednictwem poczty elektronicznej na indywidualny adres e-mail Słuchacza podany w procesie rekrutacji.
                    </fo:inline>
                    <fo:inline font-size="9pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 4.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="21.3pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Słuchacz ma prawo do rozwiązania niniejszej umowy, przy czym oświadczenie o rozwiązaniu umowy należy złożyć pod rygorem nieważności na piśmie w Centrum Kształcenia Podyplomowego.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="21.3pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku rozwiązania niniejszej umowy w trybie określonym w ust. 1 AHE stosuje przepisy Kodeksu Cywilnego, w szczególności art. 746 Kodeksu Cywilnego, dotyczące zwrotu wydatków poniesionych przez przyjmującego zlecenie na wykonanie umowy zlecenia i ewentualnych roszczeń odszkodowawczych.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>3.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="21.3pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Niezachowanie formy czynności, o której mowa w ust. 1, będzie skutkowało dalszym naliczaniem opłaty za studia podyplomowe w następnych miesiącach.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>4.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="21.3pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Postanowień ust. 3, odnośnie formy oświadczenia o rozwiązaniu umowy, nie stosuje się, gdy rozwiązanie umowy przez Słuchacza nastąpiło po uiszczeniu całej kwoty czesnego za studia wskazanej w § 2 ust. 5.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>5.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="21.3pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku dyscyplinarnego skreślenia z listy Słuchaczy o zaprzestaniu naliczania opłat decyduje data decyzji dotyczącej skreślenia.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>6.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="21.3pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Uczelnia ma prawo rozwiązać niniejszą umowę w każdym czasie w przypadku naruszenia przez Słuchacza postanowień niniejszej umowy.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 5.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Słuchacz zobowiązany jest do przestrzegania zasad korzystania z zajęć świadczonych drogą elektroniczną z wykorzystaniem Internetu, w tym w szczególności do:
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-indent="-18pt" text-align="justify" start-indent="36pt">
                <fo:list-item-label font-size="10pt" start-indent="38pt" text-indent="0pt" font-family="Tahoma" font-weight="normal" font-style="normal">
                  <fo:block>a.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="50pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />dbania o ochronę prywatności innych osób (słuchaczy, prowadzących zajęcia, administracji), w tym danych przekazywanych bądź składowanych w systemie informatycznym,
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-indent="35.4pt" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="38pt" text-indent="0pt" font-family="Tahoma" font-weight="normal" font-style="normal">
                  <fo:block>b.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="50pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />respektowania praw autorskich — zarówno materiałów edukacyjnych, jak i udostępnionego oprogramowania.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-indent="-18pt" text-align="justify" start-indent="18pt">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18.75pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Za nieprzestrzeganie zasad wymienionych w ust. 1 Słuchacz może zostać skreślony z listy słuchaczy studiów podyplomowych.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-indent="0pt" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
              <fo:block page-break-before="always">
                <fo:leader />
              </fo:block>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 6.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify" start-indent="18pt" text-indent="-18pt">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18.75pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Wszelkie zmiany dokonane przez Słuchacza w odniesieniu do postanowień umowy nie są wiążące dla Uczelni i pociągają za sobą skutek w postaci niedojścia do skutku umowy w wersji zmodyfikowanej przez Słuchacza.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify" start-indent="18pt" text-indent="-18pt">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18.75pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Wszelkie próby dokonywania przez Słuchacza zmian w treści umowy, bez złożenia odrębnego wyraźnego oświadczenia woli o fakcie dokonania modyfikacji niniejszej umowy, będą uznane za podstęp w rozumieniu przepisów Kodeksu Cywilnego.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify" start-indent="18pt" text-indent="-18pt">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>3.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18.75pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku samowolnego dokonania przez Słuchacza modyfikacji treści umowy, bez złożenia odrębnego wyraźnego oświadczenia woli w tym zakresie i przesłania do siedziby Uczelni podpisanej zmodyfikowanej wersji umowy, przyjmuje się, iż Słuchacz wyraził zgodę na zawarcie niniejszej umowy w treści udostępnionej za pomocą Internetu.
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-pattern="space"  />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-pattern="space"  />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-pattern="space"  />
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 7.
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />W sprawach nieuregulowanych w niniejszej umowie stosuje się przepisy Regulaminu Studiów Podyplomowych, Regulaminu Rekrutacji Online i przepisy Kodeksu Cywilnego.
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>


	    <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 8.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Słuchacz wyraża zgodę na wystawianie dokumentów finansowych drogą elektroniczną, bez jego podpisu oraz zamieszczenie ich do jego wglądu i pobrania w Wirtualnym Pokoju Studenta.
					</fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Zamieszczenie dokumentów, o których mowa w ust. 1 w Wirtualnym Pokoju Studenta jest równoznaczne z możliwością zapoznania się przez Słuchacza z ich treścią.
					</fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>


            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 9.
              </fo:inline>
            </fo:block>
			  <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
					  <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
						  <fo:block>1.</fo:block>
					  </fo:list-item-label>
					  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
						  <fo:block>
							  <fo:inline font-family="Tahoma" font-size="10pt">
								  <fo:leader leader-length="0pt" />Umowa została sporządzona w dwóch jednobrzmiących egzemplarzach, po jednym dla każdej ze stron.
							  </fo:inline>
						  </fo:block>
					  </fo:list-item-body>
				  </fo:list-item>
			  </fo:list-block>
			  <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
					  <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
						  <fo:block>2.</fo:block>
					  </fo:list-item-label>
					  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
						  <fo:block>
							  <fo:inline font-family="Tahoma" font-size="10pt">
								  <fo:leader leader-length="0pt" />Załącznik, o którym mowa w § 1 ust. 6b do umowy stanowi jej nierozerwalną całość.
							  </fo:inline>
						  </fo:block>
					  </fo:list-item-body>
				  </fo:list-item>
			  </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-pattern="space" leader-length="14.25pt" />
                <fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Times" font-size="12pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-pattern="space" leader-length="21pt" />
              </fo:inline>
              <fo:inline>
                <fo:leader leader-length="0pt" />_____________________
              </fo:inline>
              <fo:inline>
                <fo:leader leader-pattern="space" leader-length="15pt" />
              </fo:inline>
              <fo:inline>
                <fo:leader leader-pattern="space" leader-length="35.25pt" /><fo:leader leader-length="0pt" />______________________
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="9pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="9pt">
                <fo:leader leader-length="0pt" />                        AHE                                                             Słuchacz (czytelny podpis)
              </fo:inline>
            </fo:block>

			  <fo:block page-break-before="always"/>

			  <fo:block font-family="Arial" font-size="12pt" language="PL" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:leader />
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />ZAŁĄCZNIK 1 DO UMOWY O WARUNKACH ODPŁATNOŚCI ZA STUDIA Z DNIA
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline color="black" font-size="10pt" />
				  <fo:inline color="black" font-size="10pt">
					  <fo:leader leader-length="0pt" />
					  <xsl:value-of select="$dataWygenerowania"/>
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="12pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />zawartej pomiędzy Akademią Humanistyczno-Ekonomiczną w Łodzi a Słuchaczem
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="12pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-family="Tahoma" font-weight="bold" color="black" font-size="10pt" />
				  <fo:inline font-family="Tahoma" font-weight="bold" color="black" font-size="10pt">
					  <fo:leader leader-length="0pt" />
					  <xsl:value-of select="Student"/>
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" text-align="left" end-indent="-43.85pt" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:leader />
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />CENNIK OPŁAT ZA USŁUGI DYDAKTYCZNE NIEOBJĘTE CZESNYM
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />obowiązujący na studiach podyplomowych od 21 marca 2013 r.
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:leader />
			  </fo:block>
			  <fo:table font-family="Times" start-indent="-28.6pt" border-top-style="solid" border-top-color="black"
						border-top-width="0.5pt" border-left-style="solid" border-left-color="black" border-left-width="0.5pt"
						border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.5pt" border-right-style="solid"
						border-right-color="black" border-right-width="0.5pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
						table-layout="fixed" width="100.0%">
				  <fo:table-column column-number="1" column-width="27.5pt" />
				  <fo:table-column column-number="2" column-width="251.95pt" />
				  <fo:table-column column-number="3" column-width="80.55pt" />
				  <fo:table-column column-number="4" column-width="153pt" />
				  <fo:table-body start-indent="0pt" end-indent="0pt">
					  <fo:table-row>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  L.p.
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  WYSZCZEGÓLNIENIE
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  OPŁATY
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  UWAGI
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>1.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Reaktywacja – wznowienie studiów, wpisanie na semestr/uzupełnianie przedmiotów
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  100 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />1a
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Uzupełnianie przedmiotu w ramach reaktywacji
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  300 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Opłata jednorazowa za przedmiot
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" number-rows-spanned="3" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />1b
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Powtarzanie / wznowienie nauki na semestrze w ramach reaktywacji
								  </fo:inline>
							  </fo:block>
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="18pt" text-indent="0pt" font-family="Symbol">
										  <fo:block font-size="12pt" font-family="Arial">•</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="36pt" text-indent="0pt">
										  <fo:block>
											  <fo:inline font-size="10pt">
												  <fo:leader leader-length="0pt" />Dla studiów trwających 2 semestry opłata za semestr
											  </fo:inline>
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="5pt" padding-bottom="0pt" padding-right="5pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  1 opłaty za cały tok studiów *
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />opłata liczona od ceny studiów dla edycji do której dołącza słuchacz.
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="18pt" text-indent="0pt" font-family="Symbol">
										  <fo:block font-size="12pt" font-family="Arial">•</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="36pt" text-indent="0pt">
										  <fo:block>
											  <fo:inline font-size="10pt">
												  <fo:leader leader-length="0pt" />Dla studiów trwających 3 semestry opłata za każdy semestr pozostały do ukończenia studiów
											  </fo:inline>
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  1/3 opłaty za cały tok studiów *
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />opłata liczona od ceny studiów dla edycji do której dołącza słuchacz.
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="18pt" text-indent="0pt" font-family="Symbol">
										  <fo:block font-size="12pt" font-family="Arial">•</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="36pt" text-indent="0pt">
										  <fo:block>
											  <fo:inline font-size="10pt">
												  <fo:leader leader-length="0pt" />Dla studiów trwających 4 semestry opłata za każdy semestr pozostały do ukończenia studiów
											  </fo:inline>
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="5pt" padding-bottom="0pt" padding-right="5pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  1 opłaty za cały tok studiów *
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />opłata liczona od ceny studiów dla edycji do której dołącza słuchacz.
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />1c
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Reaktywacja – powołanie składu Komisji Egzaminacyjnej na wniosek słuchacza
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  200 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>2.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Zaświadczenie
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  10 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Obowiązuje osoby ze statusem X, R, A, N do 90 dnia po zmianie ze statusu słuchacza -S
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>3.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Zaświadczenie
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  35 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Obowiązuje osoby ze statusem  X, R, A, N od 91 dnia po zmianie ze statusu słuchacza -S
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>4.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Opłata za monit
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  20 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>5.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Opłata za dodatkowy przedmiot
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  300 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>6.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Przedłużanie okresu dyplomowania
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  50 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Opłata za każdy miesiąc przedłużenia
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>7.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Duplikat świadectwa ukończenia studiów podyplomowych
								  </fo:inline>
							  </fo:block>
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  <fo:leader />
							  </fo:block>
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  60 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
				  </fo:table-body>
			  </fo:table>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />* opłata liczona od ceny studiów dla edycji, do której dołącza słuchacz.
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" text-align="justify" end-indent="-43.85pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:leader />
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" text-align="justify" end-indent="-43.85pt" text-indent="35.45pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />Uczelnia
				  </fo:inline>
				  <fo:inline font-size="10pt">
					  <fo:leader leader-pattern="space" leader-length="33pt" />
					  <fo:leader leader-length="0pt" />
				  </fo:inline>
				  <fo:inline font-size="10pt">
					  <fo:leader leader-pattern="space" leader-length="34.5pt" />
				  </fo:inline>
				  <fo:inline font-size="10pt">
					  <fo:leader leader-pattern="space" leader-length="35.25pt" />
					  <fo:leader leader-length="0pt" />
				  </fo:inline>
				  <fo:inline font-size="10pt">
					  <fo:leader leader-pattern="space" leader-length="29.25pt" />
				  </fo:inline>
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="133pt" />Słuchacz
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="12pt" language="PL" line-height="1.7205" text-align="justify" start-indent="-18pt" end-indent="-43.85pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline>
					  <fo:leader leader-length="0pt" />
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Tahoma" font-size="12pt" language="" line-height="1.7205" text-align="justify" start-indent="-18pt" end-indent="-43.85pt" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline>
					  <fo:leader leader-length="0pt" />______________________
					  <fo:leader leader-length="133pt" />_________________________
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="12pt" language="PL" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:leader />
			  </fo:block>
			  
          </fo:block>
          <fo:block id="IDACIYWC" />
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>
</xsl:stylesheet>'
     
 WHERE Opis like 'Umowa podyplomowe PD3 wirtualni'
GO

UPDATE [dbo].[WUWydrukWersja]
   SET 
      [Szablon] = '<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:decimal-format name="pln"
decimal-separator="," grouping-separator="." NaN="" />
  <xsl:param name="dataWygenerowania"/>
  <xsl:template match="/Dane">
    <fo:root font-family="Tahoma" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml">
      <fo:layout-master-set xmlns:rx="http://www.renderx.com/XSL/Extensions" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882">
        <fo:simple-page-master master-name="section1-first-page" page-width="8.268055555555555in" page-height="11.693055555555556in" margin-top="33.7pt" margin-bottom="14.05pt" margin-right="70.85pt" margin-left="70.85pt">
          <fo:region-body margin-top="45pt" margin-bottom="55pt"></fo:region-body>
          <fo:region-before region-name="first-page-header" extent="11in"></fo:region-before>
          <fo:region-after region-name="first-page-footer" extent="11in" display-align="after"></fo:region-after>
        </fo:simple-page-master>
        <fo:simple-page-master master-name="section1-odd-page" page-width="8.268055555555555in" page-height="11.693055555555556in" margin-top="33.7pt" margin-bottom="14.05pt" margin-right="70.85pt" margin-left="70.85pt">
          <fo:region-body margin-top="45pt" margin-bottom="55pt"></fo:region-body>
          <fo:region-before region-name="odd-page-header" extent="11in"></fo:region-before>
          <fo:region-after region-name="odd-page-footer" extent="11in" display-align="after"></fo:region-after>
        </fo:simple-page-master>
        <fo:simple-page-master master-name="section1-even-page" page-width="8.268055555555555in" page-height="11.693055555555556in" margin-top="33.7pt" margin-bottom="14.05pt" margin-right="70.85pt" margin-left="70.85pt">
          <fo:region-body margin-top="45pt" margin-bottom="55pt"></fo:region-body>
          <fo:region-before region-name="even-page-header" extent="11in"></fo:region-before>
          <fo:region-after region-name="even-page-footer" extent="11in" display-align="after"></fo:region-after>
        </fo:simple-page-master>
        <fo:page-sequence-master master-name="section1-page-sequence-master">
          <fo:repeatable-page-master-alternatives>
            <fo:conditional-page-master-reference odd-or-even="odd" master-reference="section1-odd-page" />
            <fo:conditional-page-master-reference odd-or-even="even" master-reference="section1-even-page" />
          </fo:repeatable-page-master-alternatives>
        </fo:page-sequence-master>
      </fo:layout-master-set>
      <fo:page-sequence master-reference="section1-page-sequence-master" format="1" xmlns:rx="http://www.renderx.com/XSL/Extensions" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882">
        <fo:static-content flow-name="first-page-header">
          <fo:retrieve-marker retrieve-class-name="first-page-header" retrieve-position="first-including-carryover" retrieve-boundary="page" />
        </fo:static-content>
        <fo:static-content flow-name="first-page-footer">
          <fo:retrieve-marker retrieve-class-name="first-page-footer" retrieve-position="first-including-carryover" retrieve-boundary="page" />
        </fo:static-content>
        <fo:static-content flow-name="odd-page-header">
          <fo:block font-family="Tahoma" font-size="18pt" language="PL" text-align="left" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
            <fo:inline>
              <fo:external-graphic content-width="186.1pt" content-height="51.45pt" src="https://www.ahe.lodz.pl/sites/default/files/logo_ckp.jpg" />
            </fo:inline>
          </fo:block>
        </fo:static-content>
        <fo:static-content flow-name="odd-page-footer">
          <fo:retrieve-marker retrieve-class-name="odd-page-footer" retrieve-position="first-including-carryover" retrieve-boundary="page" />
        </fo:static-content>
        <fo:static-content flow-name="even-page-header">
          <fo:block font-family="Tahoma" font-size="18pt" language="PL" text-align="left" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
            <fo:inline>
              <fo:external-graphic content-width="186.1pt" content-height="51.45pt" src="https://www.ahe.lodz.pl/sites/default/files/logo_ckp.jpg" />
            </fo:inline>
          </fo:block>
        </fo:static-content>
        <fo:static-content flow-name="even-page-footer">
          <fo:retrieve-marker retrieve-class-name="odd-page-footer" retrieve-position="first-including-carryover" retrieve-boundary="page" />
        </fo:static-content>
        <fo:static-content flow-name="xsl-footnote-separator">
          <fo:block>
            <fo:leader leader-pattern="rule" leader-length="144pt" rule-thickness="0.5pt" rule-style="solid" color="gray" />
          </fo:block>
                </fo:static-content>
        <fo:flow flow-name="xsl-region-body">
          <fo:block widows="2" orphans="2" font-size="10pt" line-height="1.147" white-space-collapse="false">
            <fo:marker marker-class-name="first-page-header" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:marker marker-class-name="first-page-footer" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:marker marker-class-name="odd-page-header" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:block font-family="Arial" font-size="12pt" language="PL">
                <fo:inline>
                  <fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline>
                  <fo:block />
                </fo:inline>
                <fo:inline>
                  <fo:leader leader-pattern="space"  />
                </fo:inline>
              </fo:block>
            </fo:marker>
            <fo:marker marker-class-name="odd-page-footer" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:block font-family="Arial" font-size="7pt" language="PL" line-height="1.147" text-align="center">
                <fo:leader />
              </fo:block>>
              <fo:block font-family="Arial" font-size="7pt" language="PL" line-height="1.147" text-align="center">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="PL" line-height="1.147" text-align="center" color="#999999">
                <fo:inline color="#999999" font-size="7pt">
                  <fo:leader leader-length="0pt" />Centrum Kształcenia Podyplomowego Akademii Humanistyczno-Ekonomicznej w Łodzi
                </fo:inline>
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="DE" line-height="1.147" text-align="center" color="#999999">
                <fo:inline color="#999999" font-size="7pt" language="DE">
                  <fo:leader leader-length="0pt" />ul.Sterlinga 26, 90-212 Łódź; Tel: (+48 42) 63 15 039, (+48 42) 29 95 637. 
                </fo:inline>
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="EN-US" line-height="1.147" text-align="center" color="#999999">
                <fo:inline color="#999999" font-size="7pt" language="EN-US">
                  <fo:leader leader-length="0pt" />mail: ckp@ahe.lodz.pl,
                </fo:inline>
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="EN-US" line-height="1.147" text-align="center" color="#999999">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="EN-US" line-height="1.147" text-align="center">
                <fo:inline color="#999999" font-size="7pt" language="EN-US">
                  <fo:leader leader-length="0pt" />www.ckp-lodz.pl
                </fo:inline>
              </fo:block>
            </fo:marker>
            <fo:marker marker-class-name="even-page-header" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:marker marker-class-name="even-page-footer" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />UMOWA<fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />dotycząca studiów podyplomowych<fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" /><xsl:value-of select="Kierunek"/><fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />zawarta w dniu <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="BiezacaData" />
              <fo:inline font-family="Arial" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="$dataWygenerowania"/>
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" /> pomiędzy <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />Akademią Humanistyczno - Ekonomiczną w Łodzi <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" /> wpisaną pod numerem 30 do Rejestru Uczelni Niepublicznych prowadzonego przez ministra właściwego do spraw szkolnictwa wyższego - zwaną dalej <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />AHE<fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" /> lub<fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" /> Uczelnią<fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />, reprezentowaną na podstawie stosownego pełnomocnictwa, przez: <fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Verdana" font-size="10pt" font-weight="bold">
                <fo:leader leader-length="0pt" />Dyrektora Centrum Kształcenia Podyplomowego Paulinę Gocała<fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />a
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />Panem/ią <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="Student" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="Student"/>
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" start-indent="72pt" text-indent="-72pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />zwanym/ą dalej Słuchaczem,
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" start-indent="72pt" text-indent="-72pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />zameldowanym/ą <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="AdresSP" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="AdresSP"/>
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" start-indent="72pt" text-indent="-72pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" start-indent="48pt"  xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />powiat <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="AdresSPPowiat"/>
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />                     województwo <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="AdresSPWojewodztwo"/>
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block id="DOSeriaNumer" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:block id="DOTSNazwa" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />legitymującym/ą się dowodem osobistym<fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:inline color="white">-</fo:inline>
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="DOTSSeriaNumer"/>,
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="-10pt" />PESEL<fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:inline color="white">-</fo:inline>
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="PESEL"/>.
              </fo:inline> 
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 1.
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="12pt" text-align="justify" language="PL" start-indent="18pt" text-indent="-18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />1.  AHE zobowiązuje się do prowadzenia zajęć dydaktycznych i organizowania zaliczeń i egzaminów zgodnie z wymaganiami studiów podyplomowych: <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="Kierunek_1" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="Kierunek"/>
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />.
              </fo:inline>
            </fo:block>
            





            <fo:block font-family="Tahoma" font-size="12pt" text-align="justify" language="PL" start-indent="18pt" text-indent="-18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />2.  Słuchacz oświadcza, iż przed podpisaniem niniejszej umowy zapoznał się z Regulaminem Studiów Podyplomowych wraz z załącznikami (które dostępne są w Centrum Kształcenia Podyplomowego i na stronie internetowej www.ckp-lodz.pl) oraz Regulaminem Rekrutacji Online, jeśli uczestniczył w rekrutacji online (który dostępny jest na stronie internetowej rekrutacji online www.rekrutacja.ahe.lodz.pl) oraz zobowiązuje się przestrzegać ich postanowień. Słuchacz oświadcza, iż zapoznał się z prawem odstąpienia od umowy zawartej na odległość lub poza lokalem Uczelni.<fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="12pt" text-align="justify" language="PL" start-indent="18pt" text-indent="-18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />3.  Słuchacz zobowiązany jest na mocy umowy wnieść określone opłaty.<fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="12pt" text-align="justify" language="PL" start-indent="18pt" text-indent="-18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />4.  Opłatami, o których mowa w ust. 3 są:<fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:list-block>
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" start-indent="38pt" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="38pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>a.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="50pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-size="10pt">
                      <fo:leader leader-length="0pt" />czesne<fo:leader leader-length="0pt" />
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block>
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" start-indent="38pt" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="38pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>b.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="50pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-size="10pt">
                      <fo:leader leader-length="0pt" />inne opłaty określone w cenniku opłat za usługi nieobjęte czesnym stanowiącym załącznik do Umowy.<fo:leader leader-length="0pt" />
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
			  <fo:block font-family="Tahoma" font-size="12pt" text-align="justify" language="PL" start-indent="18pt" text-indent="-18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-family="Tahoma" font-size="10pt">
					  <fo:leader leader-length="0pt" />5.  Opłaty, o których mowa w ust. 4b płatne są w przypadkach wskazanych w Cenniku i w terminach w nim podanych.<fo:leader leader-length="0pt" />
				  </fo:inline>
			  </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 2.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Studia podyplomowe będące przedmiotem niniejszej umowy trwają <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline id="UmowaLiczbaSem" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />2<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> semestry akademickie. Czas trwania studiów podyplomowych może ulec zmianie w przypadku zmiany przepisów regulujących zasady organizacji i przeprowadzania studiów podyplomowych.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" start-indent="17.85pt" end-indent="-3.7pt" text-indent="-17.85pt" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Nieuczestniczenie w zajęciach dydaktycznych nie zwalnia Słuchacza z obowiązku uiszczania opłat za studia podyplomowe.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>3.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Słuchacz zobowiązany jest do<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> zapłaty czesnego<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> w wysokości <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline id="UmowaCzesne10Miesiec" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" /> <xsl:value-of select="format-number(UmowaCzesne8Miesiec, &quot;##0,00&quot;, &quot;pln&quot;)"/> zł<fo:leader leader-length="0pt" />,<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> za cały cykl studiów podyplomowych.<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" color="#3366FF" font-size="10pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> Czesne płatne jest w <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt" font-weight="bold">
                      <fo:leader leader-length="0pt" />8<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> równych miesięcznych ratach, po <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline id="UmowaCzesne" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" /><xsl:value-of select="format-number(UmowaCzesne, &quot;##0,00&quot;, &quot;pln&quot;)"/> zł<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> każda.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>4.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Zapłaty czesnego należy dokonywać do<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" /> 5-go<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> dnia każdego miesiąca, począwszy od miesiąca <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt" font-weight="bold">
                      <fo:leader leader-length="0pt" /> listopada<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> dla studiów rozpoczynających się w semestrze zimowym, a dla studiów rozpoczynających się w semestrze letnim od miesiąca <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt" font-weight="bold">
                      <fo:leader leader-length="0pt" />kwietnia.<fo:leader leader-length="0pt" />
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>5.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku niedokonania wpłaty czesnego, oraz innych opłat, o których mowa w § 1 ust. 4b w terminie, wskazanym w ust. 4, Słuchacz zobowiązany jest do zapłaty zaległej kwoty powiększonej o odsetki umowne za opóźnienie. Odsetki umowne za opóźnienie wynoszą w stosunku rocznym dwukrotność odsetek ustawowych za opóźnienie od każdej zaległej kwoty, nie więcej jednak niż czterokrotność wysokości stopy kredytu lombardowego Narodowego Banku Polskiego.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>6.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku przyjęcia na studia podyplomowe po upływie terminu płatności czesnego w danym miesiącu, Słuchacz zobowiązuje się do wpłaty zaległych rat czesnego w ciągu siedmiu dni licząc od daty podpisania niniejszej umowy.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>7.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Wpłaty  należy  dokonywać  przekazem pocztowym lub poprzez przelew na indywidualny numer konta:
                    </fo:inline>
                  </fo:block>
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />PKO BP nr rachunku <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline id="NrRORPM3" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />
                      <xsl:value-of select="NrRORPM3PKO"/>
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" start-indent="18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />Blankiety wpłaty czesnego powinny być wypełnione czytelnie z podaniem przeznaczenia (czesne, odsetki) wpłaconej kwoty oraz z dopiskiem <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />studia podyplomowe: <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="Kierunek_2" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="Kierunek"/>
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <!--<fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />.
              </fo:inline>-->
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>8.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Za datę dokonania wpłaty czesnego uznaje się datę wpływu środków pieniężnych na rachunek bankowy Uczelni.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 3.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Nie później niż na pięć dni przed dniem, w którym zgodnie z ofertą AHE miały rozpocząć się na pierwszym semestrze studia podyplomowe objęte niniejszą umową, AHE ma prawo do odstąpienia od umowy w przypadku, gdy organizacja studiów będzie nieopłacalna i zostanie podjęta decyzja o nie prowadzeniu w danym semestrze studiów podyplomowych objętych niniejszą umową. Decyzję o nie prowadzeniu w danym semestrze studiów podyplomowych objętych niniejszą umową oraz o odstąpieniu od umowy podejmuje Kierownik Centrum Kształcenia Podyplomowego działając w ramach umocowania udzielonego przez właściwy organ AHE.<fo:leader leader-length="0pt" />
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>

            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Oświadczenie woli o odstąpieniu od umowy przez Uczelnię może być złożone Słuchaczowi na piśmie lub za pośrednictwem poczty elektronicznej na indywidualny adres e-mail Słuchacza podany w procesie rekrutacji.
                    </fo:inline>
                    <fo:inline font-size="9pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 4.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Słuchacz ma prawo do rozwiązania niniejszej umowy, przy czym oświadczenie o rozwiązaniu umowy należy złożyć pod rygorem nieważności na piśmie pracownikowi Centrum Kształcenia Podyplomowego.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku rozwiązania niniejszej umowy w trybie określonym w ust. 1, AHE stosuje przepisy Kodeksu Cywilnego, w szczególności art. 746 Kodeksu Cywilnego, dotyczące zwrotu wydatków poniesionych przez przyjmującego zlecenie na wykonanie umowy zlecenia i ewentualnych roszczeń odszkodowawczych.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>3.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Niezachowanie przez Słuchacza formy czynności, o której mowa w ust. 1, będzie skutkowało nieważnością oświadczenia o rozwiązaniu umowy i dalszym naliczaniem opłaty za studia podyplomowe w następnych miesiącach.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>4.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Postanowień ust. 3, odnośnie formy oświadczenia o rozwiązaniu umowy nie stosuje się, gdy rozwiązanie umowy przez Słuchacza nastąpiło po uiszczeniu całej kwoty czesnego za studia wskazanej w § 2 ust. 3.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>5.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku dyscyplinarnego skreślenia z listy Słuchaczy o zaprzestaniu naliczania opłat decyduje data decyzji dotyczącej skreślenia.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>6.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Uczelnia ma prawo rozwiązać niniejszą umowę w każdym czasie w przypadku naruszenia przez Słuchacza postanowień niniejszej umowy.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 5.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Wszelkie zmiany dokonane przez Słuchacza w odniesieniu do postanowień umowy nie są wiążące dla Uczelni i pociągają za sobą skutek w postaci niedojścia do skutku umowy w wersji zmodyfikowanej przez Słuchacza.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Wszelkie próby dokonywania przez Słuchacza zmian w treści umowy, bez złożenia odrębnego wyraźnego oświadczenia woli o fakcie dokonania modyfikacji niniejszej umowy, będą uznane za podstęp w rozumieniu przepisów Kodeksu Cywilnego.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
                  <fo:block>3.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku samowolnego dokonania przez Słuchacza modyfikacji treści umowy, bez złożenia odrębnego wyraźnego oświadczenia woli w tym zakresie i przesłania do siedziby Uczelni podpisanej zmodyfikowanej wersji umowy, przyjmuje się, iż Słuchacz wyraził zgodę na zawarcie niniejszej umowy w treści udostępnionej za pomocą internetu.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 6.
              </fo:inline>
            </fo:block>
            <fo:block  text-align="justify" >
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />W sprawach nieuregulowanych w niniejszej umowie stosuje się przepisy Regulaminu Studiów Podyplomowych, Regulaminu Rekrutacji Online i przepisy Kodeksu Cywilnego.
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-indent="0pt" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-indent="0pt" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
	          <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
               <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt"><fo:leader leader-length="0pt" /></fo:inline><fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 7.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Słuchacz wyraża zgodę na wystawianie dokumentów finansowych drogą elektroniczną, bez jego podpisu oraz zamieszczenie ich do jego wglądu i pobrania w Wirtualnym Pokoju Studenta.
					</fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Zamieszczenie dokumentów, o których mowa w ust. 1 w Wirtualnym Pokoju Studenta jest równoznaczne z możliwością zapoznania się przez Słuchacza z ich treścią.
					</fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>


	    <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 8.
              </fo:inline>
            </fo:block>
			  <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
					  <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
						  <fo:block>1.</fo:block>
					  </fo:list-item-label>
					  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
						  <fo:block>
							  <fo:inline font-family="Tahoma" font-size="10pt">
								  <fo:leader leader-length="0pt" />Umowa została sporządzona w dwóch jednobrzmiących egzemplarzach, po jednym dla każdej ze stron.
							  </fo:inline>
						  </fo:block>
					  </fo:list-item-body>
				  </fo:list-item>
			  </fo:list-block>
			  <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
					  <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
						  <fo:block>2.</fo:block>
					  </fo:list-item-label>
					  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
						  <fo:block>
							  <fo:inline font-family="Tahoma" font-size="10pt">
								  <fo:leader leader-length="0pt" />Załącznik, o którym mowa w § 1 ust. 4b do umowy stanowi jej nierozerwalną całość.
							  </fo:inline>
						  </fo:block>
					  </fo:list-item-body>
				  </fo:list-item>
			  </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-pattern="space" leader-length="14.25pt" />
                <fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="12pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-pattern="space" leader-length="21pt" />
              </fo:inline>
              <fo:inline>
                <fo:leader leader-length="0pt" />_____________________
              </fo:inline>
              <fo:inline>
                <fo:leader leader-pattern="space" leader-length="15pt" />
              </fo:inline>
              <fo:inline>
                <fo:leader leader-pattern="space" leader-length="35.25pt" /><fo:leader leader-length="0pt" />______________________
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="9pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="9pt">
                <fo:leader leader-length="0pt" />                        AHE                                                                       Słuchacz (czytelny podpis)
              </fo:inline>
            </fo:block>

			<fo:block page-break-before="always"/>

			<fo:block font-family="Arial" font-size="12pt" language="PL" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				<fo:leader />
			</fo:block>
			<fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				<fo:inline font-size="10pt">
					<fo:leader leader-length="0pt" />ZAŁĄCZNIK 1 DO UMOWY O WARUNKACH ODPŁATNOŚCI ZA STUDIA Z DNIA
				</fo:inline>
			</fo:block>
			<fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				<fo:inline color="black" font-size="10pt" />
				<fo:inline color="black" font-size="10pt">
					<fo:leader leader-length="0pt" />
					<xsl:value-of select="$dataWygenerowania"/>
				</fo:inline>
			</fo:block>
			<fo:block font-family="Arial" font-size="12pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				<fo:inline font-size="10pt">
					<fo:leader leader-length="0pt" />zawartej pomiędzy Akademią Humanistyczno-Ekonomiczną w Łodzi a Słuchaczem
				</fo:inline>
			</fo:block>
			<fo:block font-family="Arial" font-size="12pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				<fo:inline font-family="Tahoma" font-weight="bold" color="black" font-size="10pt" />
				<fo:inline font-family="Tahoma" font-weight="bold" color="black" font-size="10pt">
					<fo:leader leader-length="0pt" />
					<xsl:value-of select="Student"/>
				</fo:inline>
			</fo:block>
			<fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" text-align="left" end-indent="-43.85pt" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				<fo:leader />
			</fo:block>
			<fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				<fo:inline font-size="10pt">
					<fo:leader leader-length="0pt" />CENNIK OPŁAT ZA USŁUGI DYDAKTYCZNE NIEOBJĘTE CZESNYM
				</fo:inline>
			</fo:block>
			<fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				<fo:inline font-size="10pt">
					<fo:leader leader-length="0pt" />obowiązujący na studiach podyplomowych od 21 marca 2013 r.
				</fo:inline>
			</fo:block>
			<fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				<fo:leader />
			</fo:block>
			<fo:table font-family="Times" start-indent="-28.6pt" border-top-style="solid" border-top-color="black" 
					  border-top-width="0.5pt" border-left-style="solid" border-left-color="black" border-left-width="0.5pt" 
					  border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.5pt" border-right-style="solid" 
					  border-right-color="black" border-right-width="0.5pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
					  table-layout="fixed" width="100.0%">
				<fo:table-column column-number="1" column-width="27.5pt" />
				<fo:table-column column-number="2" column-width="251.95pt" />
				<fo:table-column column-number="3" column-width="80.55pt" />
				<fo:table-column column-number="4" column-width="153pt" />
				<fo:table-body start-indent="0pt" end-indent="0pt">
					<fo:table-row>
						<fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								L.p.
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								WYSZCZEGÓLNIENIE
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								OPŁATY
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								UWAGI
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:list-block>
								<fo:list-item font-family="Arial" font-size="10pt" language="PL">
									<fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										<fo:block>1.</fo:block>
									</fo:list-item-label>
									<fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										<fo:block>
											<fo:leader />
										</fo:block>
									</fo:list-item-body>
								</fo:list-item>
							</fo:list-block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:inline font-size="10pt">
									<fo:leader leader-length="0pt" />Reaktywacja – wznowienie studiów, wpisanie na semestr/uzupełnianie przedmiotów
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								100 zł
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:leader />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:inline font-size="10pt">
									<fo:leader leader-length="0pt" />1a
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:inline font-size="10pt">
									<fo:leader leader-length="0pt" />Uzupełnianie przedmiotu w ramach reaktywacji
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								300 zł
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:inline font-size="10pt">
									<fo:leader leader-length="0pt" />Opłata jednorazowa za przedmiot
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" number-rows-spanned="3" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:inline font-size="10pt">
									<fo:leader leader-length="0pt" />1b
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:inline font-size="10pt">
									<fo:leader leader-length="0pt" />Powtarzanie / wznowienie nauki na semestrze w ramach reaktywacji
								</fo:inline>
							</fo:block>
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:leader />
							</fo:block>
							<fo:list-block>
								<fo:list-item font-family="Arial" font-size="10pt" language="PL">
									<fo:list-item-label font-size="12pt" start-indent="18pt" text-indent="0pt" font-family="Symbol">
										<fo:block font-size="12pt" font-family="Arial">•</fo:block>
									</fo:list-item-label>
									<fo:list-item-body end-indent="inherit" start-indent="36pt" text-indent="0pt">
										<fo:block>
											<fo:inline font-size="10pt">
												<fo:leader leader-length="0pt" />Dla studiów trwających 2 semestry opłata za semestr
											</fo:inline>
										</fo:block>
									</fo:list-item-body>
								</fo:list-item>
							</fo:list-block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="5pt" padding-bottom="0pt" padding-right="5pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								1 opłaty za cały tok studiów *
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:inline font-size="10pt">
									<fo:leader leader-length="0pt" />opłata liczona od ceny studiów dla edycji do której dołącza słuchacz.
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:list-block>
								<fo:list-item font-family="Arial" font-size="10pt" language="PL">
									<fo:list-item-label font-size="12pt" start-indent="18pt" text-indent="0pt" font-family="Symbol">
										<fo:block font-size="12pt" font-family="Arial">•</fo:block>
									</fo:list-item-label>
									<fo:list-item-body end-indent="inherit" start-indent="36pt" text-indent="0pt">
										<fo:block>
											<fo:inline font-size="10pt">
												<fo:leader leader-length="0pt" />Dla studiów trwających 3 semestry opłata za każdy semestr pozostały do ukończenia studiów
											</fo:inline>
										</fo:block>
									</fo:list-item-body>
								</fo:list-item>
							</fo:list-block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								1/3 opłaty za cały tok studiów *
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:inline font-size="10pt">
									<fo:leader leader-length="0pt" />opłata liczona od ceny studiów dla edycji do której dołącza słuchacz.
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:list-block>
								<fo:list-item font-family="Arial" font-size="10pt" language="PL">
									<fo:list-item-label font-size="12pt" start-indent="18pt" text-indent="0pt" font-family="Symbol">
										<fo:block font-size="12pt" font-family="Arial">•</fo:block>
									</fo:list-item-label>
									<fo:list-item-body end-indent="inherit" start-indent="36pt" text-indent="0pt">
										<fo:block>
											<fo:inline font-size="10pt">
												<fo:leader leader-length="0pt" />Dla studiów trwających 4 semestry opłata za każdy semestr pozostały do ukończenia studiów
											</fo:inline>
										</fo:block>
									</fo:list-item-body>
								</fo:list-item>
							</fo:list-block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="5pt" padding-bottom="0pt" padding-right="5pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								1 opłaty za cały tok studiów *
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:inline font-size="10pt">
									<fo:leader leader-length="0pt" />opłata liczona od ceny studiów dla edycji do której dołącza słuchacz.
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:inline font-size="10pt">
									<fo:leader leader-length="0pt" />1c
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:inline font-size="10pt">
									<fo:leader leader-length="0pt" />Reaktywacja – powołanie składu Komisji Egzaminacyjnej na wniosek słuchacza
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								200 zł
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:leader />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:list-block>
								<fo:list-item font-family="Arial" font-size="10pt" language="PL">
									<fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										<fo:block>2.</fo:block>
									</fo:list-item-label>
									<fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										<fo:block>
											<fo:leader />
										</fo:block>
									</fo:list-item-body>
								</fo:list-item>
							</fo:list-block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:inline font-size="10pt">
									<fo:leader leader-length="0pt" />Zaświadczenie
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								10 zł
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:inline font-size="10pt">
									<fo:leader leader-length="0pt" />Obowiązuje osoby ze statusem X, R, A, N do 90 dnia po zmianie ze statusu słuchacza -S
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:list-block>
								<fo:list-item font-family="Arial" font-size="10pt" language="PL">
									<fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										<fo:block>3.</fo:block>
									</fo:list-item-label>
									<fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										<fo:block>
											<fo:leader />
										</fo:block>
									</fo:list-item-body>
								</fo:list-item>
							</fo:list-block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:inline font-size="10pt">
									<fo:leader leader-length="0pt" />Zaświadczenie
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								35 zł
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:inline font-size="10pt">
									<fo:leader leader-length="0pt" />Obowiązuje osoby ze statusem  X, R, A, N od 91 dnia po zmianie ze statusu słuchacza -S
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:list-block>
								<fo:list-item font-family="Arial" font-size="10pt" language="PL">
									<fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										<fo:block>4.</fo:block>
									</fo:list-item-label>
									<fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										<fo:block>
											<fo:leader />
										</fo:block>
									</fo:list-item-body>
								</fo:list-item>
							</fo:list-block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:inline font-size="10pt">
									<fo:leader leader-length="0pt" />Opłata za monit
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								20 zł
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:leader />
							</fo:block>
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:leader />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:list-block>
								<fo:list-item font-family="Arial" font-size="10pt" language="PL">
									<fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										<fo:block>5.</fo:block>
									</fo:list-item-label>
									<fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										<fo:block>
											<fo:leader />
										</fo:block>
									</fo:list-item-body>
								</fo:list-item>
							</fo:list-block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:inline font-size="10pt">
									<fo:leader leader-length="0pt" />Opłata za dodatkowy przedmiot
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								300 zł
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:leader />
							</fo:block>
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:leader />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:list-block>
								<fo:list-item font-family="Arial" font-size="10pt" language="PL">
									<fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										<fo:block>6.</fo:block>
									</fo:list-item-label>
									<fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										<fo:block>
											<fo:leader />
										</fo:block>
									</fo:list-item-body>
								</fo:list-item>
							</fo:list-block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:inline font-size="10pt">
									<fo:leader leader-length="0pt" />Przedłużanie okresu dyplomowania
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								50 zł
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:inline font-size="10pt">
									<fo:leader leader-length="0pt" />Opłata za każdy miesiąc przedłużenia
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt">
							<fo:list-block>
								<fo:list-item font-family="Arial" font-size="10pt" language="PL">
									<fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										<fo:block>7.</fo:block>
									</fo:list-item-label>
									<fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										<fo:block>
											<fo:leader />
										</fo:block>
									</fo:list-item-body>
								</fo:list-item>
							</fo:list-block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt">
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:inline font-size="10pt">
									<fo:leader leader-length="0pt" />Duplikat świadectwa ukończenia studiów podyplomowych
								</fo:inline>
							</fo:block>
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:leader />
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt">
							<fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								<fo:leader />
							</fo:block>
							<fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								60 zł
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt">
							<fo:block font-family="Arial" font-size="10pt" language="PL">
								<fo:leader />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
			<fo:block font-family="Arial" font-size="10pt" language="PL" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				<fo:inline font-size="10pt">
					<fo:leader leader-length="0pt" />* opłata liczona od ceny studiów dla edycji, do której dołącza słuchacz.
				</fo:inline>
			</fo:block>
			<fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" text-align="justify" end-indent="-43.85pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				<fo:leader />
			</fo:block>
			<fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" text-align="justify" end-indent="-43.85pt" text-indent="35.45pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				<fo:inline font-size="10pt">
					<fo:leader leader-length="0pt" />Uczelnia
				</fo:inline>
				<fo:inline font-size="10pt">
					<fo:leader leader-pattern="space" leader-length="33pt" />
					<fo:leader leader-length="0pt" />
				</fo:inline>
				<fo:inline font-size="10pt">
					<fo:leader leader-pattern="space" leader-length="34.5pt" />
				</fo:inline>
				<fo:inline font-size="10pt">
					<fo:leader leader-pattern="space" leader-length="35.25pt" />
					<fo:leader leader-length="0pt" />
				</fo:inline>
				<fo:inline font-size="10pt">
					<fo:leader leader-pattern="space" leader-length="29.25pt" />
				</fo:inline>
				<fo:inline font-size="10pt">
					<fo:leader leader-length="133pt" />Słuchacz
				</fo:inline>
			</fo:block>
			<fo:block font-family="Arial" font-size="12pt" language="PL" line-height="1.7205" text-align="justify" start-indent="-18pt" end-indent="-43.85pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				<fo:inline>
					<fo:leader leader-length="0pt" />
				</fo:inline>
			</fo:block>
			<fo:block font-family="Tahoma" font-size="12pt" language="" line-height="1.7205" text-align="justify" start-indent="-18pt" end-indent="-43.85pt" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				<fo:inline>
					<fo:leader leader-length="0pt" />______________________
					<fo:leader leader-length="133pt" />_________________________
				</fo:inline>
			</fo:block>
			<fo:block font-family="Arial" font-size="12pt" language="PL" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				<fo:leader />
			</fo:block>

		  </fo:block>
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>
</xsl:stylesheet>'
     
 WHERE Opis like 'Umowa podyplomowe PD2'
GO
UPDATE [dbo].[WUWydrukWersja]
   SET 
      [Szablon] = '<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:decimal-format name="pln"
decimal-separator="," grouping-separator="." NaN="" />
  <xsl:param name="dataWygenerowania"/>
  <xsl:template match="/Dane">
    <fo:root font-family="Times" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml">
      <fo:layout-master-set xmlns:rx="http://www.renderx.com/XSL/Extensions" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882">
        <fo:simple-page-master master-name="section1-first-page" page-width="8.268055555555555in" page-height="11.693055555555556in" margin-top="35.4pt" margin-bottom="14.05pt" margin-right="70.85pt" margin-left="70.85pt">
          <fo:region-body margin-top="50pt" margin-bottom="45pt"></fo:region-body>
          <fo:region-before region-name="first-page-header" extent="11in"></fo:region-before>
          <fo:region-after region-name="first-page-footer" extent="11in" display-align="after"></fo:region-after>
        </fo:simple-page-master>
        <fo:simple-page-master master-name="section1-odd-page" page-width="8.268055555555555in" page-height="11.693055555555556in" margin-top="35.4pt" margin-bottom="14.05pt" margin-right="70.85pt" margin-left="70.85pt">
          <fo:region-body margin-top="50pt" margin-bottom="45pt"></fo:region-body>
          <fo:region-before region-name="odd-page-header" extent="11in"></fo:region-before>
          <fo:region-after region-name="odd-page-footer" extent="11in" display-align="after"></fo:region-after>
        </fo:simple-page-master>
        <fo:simple-page-master master-name="section1-even-page" page-width="8.268055555555555in" page-height="11.693055555555556in" margin-top="35.4pt" margin-bottom="14.05pt" margin-right="70.85pt" margin-left="70.85pt">
          <fo:region-body margin-top="50pt" margin-bottom="45pt"></fo:region-body>
          <fo:region-before region-name="even-page-header" extent="11in"></fo:region-before>
          <fo:region-after region-name="even-page-footer" extent="11in" display-align="after"></fo:region-after>
        </fo:simple-page-master>
        <fo:page-sequence-master master-name="section1-page-sequence-master">
          <fo:repeatable-page-master-alternatives>
            <fo:conditional-page-master-reference odd-or-even="odd" master-reference="section1-odd-page" />
            <fo:conditional-page-master-reference odd-or-even="even" master-reference="section1-even-page" />
          </fo:repeatable-page-master-alternatives>
        </fo:page-sequence-master>
      </fo:layout-master-set>
      <fo:page-sequence master-reference="section1-page-sequence-master" format="1" xmlns:rx="http://www.renderx.com/XSL/Extensions" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882">
        <fo:static-content flow-name="first-page-header">
          <fo:retrieve-marker retrieve-class-name="first-page-header" retrieve-position="first-including-carryover" retrieve-boundary="page" />
        </fo:static-content>
        <fo:static-content flow-name="first-page-footer">
          <fo:retrieve-marker retrieve-class-name="first-page-footer" retrieve-position="first-including-carryover" retrieve-boundary="page" />
        </fo:static-content>
        <fo:static-content flow-name="odd-page-header">
          <fo:block font-family="Times" font-size="18pt" language="PL" text-align="left" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
            <fo:inline>
              <fo:external-graphic content-width="186.1pt" content-height="51.45pt" src="https://www.ahe.lodz.pl/sites/default/files/logo_ckp.jpg" />
            </fo:inline>
          </fo:block>
        </fo:static-content>
        <fo:static-content flow-name="odd-page-footer">
          <fo:retrieve-marker retrieve-class-name="odd-page-footer" retrieve-position="first-including-carryover" retrieve-boundary="page" />
        </fo:static-content>
        <fo:static-content flow-name="even-page-header">
          <fo:block font-family="Times" font-size="18pt" language="PL" text-align="left" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
            <fo:inline>
              <fo:external-graphic content-width="186.1pt" content-height="51.45pt" src="https://www.ahe.lodz.pl/sites/default/files/logo_ckp.jpg" />
            </fo:inline>
          </fo:block>
        </fo:static-content>
        <fo:static-content flow-name="even-page-footer">
          <fo:retrieve-marker retrieve-class-name="odd-page-footer" retrieve-position="first-including-carryover" retrieve-boundary="page" />
        </fo:static-content>
        <fo:static-content flow-name="xsl-footnote-separator">
          <fo:block>
            <fo:leader leader-pattern="rule" leader-length="144pt" rule-thickness="0.5pt" rule-style="solid" color="gray" />
          </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">
          <fo:block widows="2" orphans="2" font-size="10pt" line-height="1.147" white-space-collapse="false">
            <fo:marker marker-class-name="first-page-header" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:marker marker-class-name="first-page-footer" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:marker marker-class-name="odd-page-header" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:block font-family="Arial" font-size="12pt" language="PL">
                <fo:inline>
                  <fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline>
                  <fo:block />
                </fo:inline>
                <fo:inline>
                  <fo:leader leader-pattern="space"  />
                </fo:inline>
              </fo:block>
            </fo:marker>
            <fo:marker marker-class-name="odd-page-footer" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:block font-family="Arial" font-size="7pt" language="PL" line-height="1.147" text-align="center">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="PL" line-height="1.147" text-align="center">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="PL" line-height="1.147" text-align="center" color="#999999">
                <fo:inline color="#999999" font-size="7pt">
                  <fo:leader leader-length="0pt" />Centrum Kształcenia Podyplomowego Akademii Humanistyczno-Ekonomicznej w Łodzi
                </fo:inline>
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="DE" line-height="1.147" text-align="center" color="#999999">
                <fo:inline color="#999999" font-size="7pt" language="DE">
                  <fo:leader leader-length="0pt" />ul. Sterlinga 26, 90-212 Łódź; Tel: (+48 42) 63 15 039, (+48 42) 29 95 637.
                </fo:inline>
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="EN-US" line-height="1.147" text-align="center" color="#999999">
                <fo:inline color="#999999" font-size="7pt" language="EN-US">
                  <fo:leader leader-length="0pt" />mail: ckp@ahe.lodz.pl,
                </fo:inline>
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="EN-US" line-height="1.147" text-align="center" color="#999999">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="EN-US" line-height="1.147" text-align="center">
                <fo:inline color="#999999" font-size="7pt" language="EN-US">
                  <fo:leader leader-length="0pt" />www.ckp-lodz.pl
                </fo:inline>
              </fo:block>
            </fo:marker>
            <fo:marker marker-class-name="even-page-header" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:marker marker-class-name="even-page-footer" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />UMOWA<fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />dotycząca studiów podyplomowych<fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" /><xsl:value-of select="Kierunek"/><fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />zawarta w dniu <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="BiezacaData" />
              <fo:inline font-family="Arial" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="$dataWygenerowania"/>
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" /> pomiędzy <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />Akademią Humanistyczno - Ekonomiczną w Łodzi <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" /> wpisaną pod numerem 30 do Rejestru Uczelni Niepublicznych prowadzonego przez ministra właściwego do spraw szkolnictwa wyższego - zwaną dalej <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />AHE<fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />  lub<fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" /> Uczelnią<fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />, reprezentowaną na podstawie stosownego pełnomocnictwa, przez: <fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Verdana" font-size="10pt" font-weight="bold">
                <fo:leader leader-length="0pt" />Dyrektora Centrum Kształcenia Podyplomowego Paulinę Gocała<fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />a
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />Panem/ią <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="Student" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="Student"/>
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" start-indent="72pt" text-indent="-72pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />zwanym/ą dalej Słuchaczem,
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" start-indent="72pt" text-indent="-72pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />zameldowanym/ą <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="AdresSP" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="AdresSP"/>
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" start-indent="72pt" text-indent="-72pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" start-indent="48pt"  xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />powiat <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="AdresSPPowiat"/>
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />                     województwo <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="AdresSPWojewodztwo"/>
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block id="DOSeriaNumer" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:block id="DOTSNazwa" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />legitymującym/ą się dowodem osobistym <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" /><xsl:value-of select="DOTSSeriaNumer"/>,
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="-10pt" />PESEL<fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:inline color="white">-</fo:inline>
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="PESEL"/>.
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 1.
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="12pt" text-align="justify" language="PL" start-indent="18pt" text-indent="-18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />1.  AHE zobowiązuje się do prowadzenia zajęć dydaktycznych i organizowania zaliczeń i egzaminów zgodnie z wymaganiami studiów podyplomowych: <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="Kierunek_1" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" /> <xsl:value-of select="Kierunek"/> <fo:leader leader-length="0pt" />
              </fo:inline>


              <fo:block font-family="Tahoma" font-size="12pt" text-align="justify" language="PL" start-indent="18pt" text-indent="-18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
                <fo:inline font-family="Tahoma" font-size="10pt">
                  <fo:leader leader-length="0pt" />2.  Słuchacz oświadcza, iż przed podpisaniem niniejszej umowy zapoznał się z Regulaminem Studiów Podyplomowych wraz z załącznikami (które dostępne są w Centrum Kształcenia Podyplomowego i na stronie internetowej www.ckp-lodz.pl) oraz Regulaminem Rekrutacji Online, jeśli uczestniczył w rekrutacji online (który dostępny jest na stronie internetowej rekrutacji online www.rekrutacja.ahe.lodz.pl) oraz zobowiązuje się przestrzegać ich postanowień. Słuchacz oświadcza, iż zapoznał się z prawem odstąpienia od umowy zawartej na odległość lub poza lokalem Uczelni. <fo:leader leader-length="0pt" />
                </fo:inline>
              </fo:block>
              <fo:block font-family="Tahoma" font-size="12pt" text-align="justify" language="PL" start-indent="18pt" text-indent="-18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
                <fo:inline font-family="Tahoma" font-size="10pt">
                  <fo:leader leader-length="0pt" />3.  Słuchacz zobowiązany jest na mocy umowy wnieść określone opłaty.<fo:leader leader-length="0pt" />
                </fo:inline>
              </fo:block>
              <fo:block font-family="Tahoma" font-size="12pt" text-align="justify" language="PL" start-indent="18pt" text-indent="-18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
                <fo:inline font-family="Tahoma" font-size="10pt">
                  <fo:leader leader-length="0pt" />4.  Opłatami, o których mowa w ust. 3 są:<fo:leader leader-length="0pt" />
                </fo:inline>
              </fo:block>
              <fo:list-block>
                <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" start-indent="38pt" text-align="justify">
                  <fo:list-item-label font-size="10pt" start-indent="38pt" text-indent="0pt" font-family="Tahoma">
                    <fo:block>a.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="50pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="10pt">
                        <fo:leader leader-length="0pt" />czesne<fo:leader leader-length="0pt" />
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:list-block>
                <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" start-indent="38pt" text-align="justify">
                  <fo:list-item-label font-size="10pt" start-indent="38pt" text-indent="0pt" font-family="Tahoma">
                    <fo:block>b.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="50pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="10pt">
                        <fo:leader leader-length="0pt" />inne opłaty określone w cenniku opłat za usługi nieobjęte czesnym stanowiącym załącznik do Umowy.<fo:leader leader-length="0pt" />
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
				<fo:block font-family="Tahoma" font-size="12pt" text-align="justify" language="PL" start-indent="18pt" text-indent="-18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
					<fo:inline font-family="Tahoma" font-size="10pt">
						<fo:leader leader-length="0pt" />5.  Opłaty, o których mowa w ust. 4b płatne są w przypadkach wskazanych w Cenniku i w terminach w nim podanych.<fo:leader leader-length="0pt" />
					</fo:inline>
				</fo:block>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 2.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Studia podyplomowe będące przedmiotem niniejszej umowy trwają <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline id="UmowaLiczbaSem" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />3<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> semestry akademickie. Czas trwania studiów podyplomowych może ulec zmianie w przypadku zmiany przepisów regulujących zasady organizacji i przeprowadzania studiów podyplomowych.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" start-indent="17.85pt" end-indent="-5.4pt" text-indent="-17.85pt" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Nieuczestniczenie w zajęciach dydaktycznych nie zwalnia Słuchacza z obowiązku uiszczania opłat za studia podyplomowe.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>3.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Słuchacz zobowiązany jest do<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> zapłaty czesnego<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> w wysokości <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline id="UmowaCzesne10Miesiec" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" /> <xsl:value-of select="format-number(UmowaCzesne10Miesiec, &quot;##0,00&quot;, &quot;pln&quot;)"/> zł<fo:leader leader-length="0pt" />,<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> za cały cykl studiów podyplomowych.<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" color="#3366FF" font-size="10pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> Czesne płatne jest w <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt" font-weight="bold">
                      <fo:leader leader-length="0pt" />10<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> równych miesięcznych ratach, po <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline id="UmowaCzesne" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" /><xsl:value-of select="format-number(UmowaCzesne, &quot;##0,00&quot;, &quot;pln&quot;)"/> zł<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> każda.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>4.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Zapłaty czesnego należy dokonywać do<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" /> 5-go<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> dnia każdego miesiąca, począwszy od miesiąca <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt" font-weight="bold">
                      <fo:leader leader-length="0pt" /> listopada<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> dla studiów rozpoczynających się w semestrze zimowym, a dla studiów rozpoczynających się w semestrze letnim od miesiąca <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt" font-weight="bold">
                      <fo:leader leader-length="0pt" />kwietnia.<fo:leader leader-length="0pt" />
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>5.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku niedokonania wpłaty czesnego, oraz innych opłat, o których mowa w § 1 ust. 4b w terminie, wskazanym w ust. 4, Słuchacz zobowiązany jest do zapłaty zaległej kwoty powiększonej o odsetki umowne za opóźnienie. Odsetki umowne za opóźnienie wynoszą w stosunku rocznym dwukrotność odsetek ustawowych za opóźnienie od każdej zaległej kwoty, nie więcej jednak niż czterokrotność wysokości stopy kredytu lombardowego Narodowego Banku Polskiego.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>6.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku przyjęcia na studia podyplomowe po upływie terminu płatności czesnego w danym miesiącu, Słuchacz zobowiązuje się do wpłaty zaległych rat czesnego w ciągu siedmiu dni licząc od daty podpisania niniejszej umowy.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>7.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Wpłaty  należy  dokonywać  przekazem pocztowym lub poprzez przelew na indywidualny numer konta:
                    </fo:inline>
                  </fo:block>
                    <fo:block>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />PKO BP nr rachunku <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline id="NrRORPM3" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />
                      <xsl:value-of select="NrRORPM3PKO"/>
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" start-indent="18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />Blankiety wpłaty czesnego powinny być wypełnione czytelnie z podaniem przeznaczenia (czesne, odsetki) wpłaconej kwoty oraz z dopiskiem <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />studia podyplomowe: <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="Kierunek_2" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" /><xsl:value-of select="Kierunek"/><fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>8.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Za datę dokonania wpłaty czesnego uznaje się datę wpływu środków pieniężnych na rachunek bankowy Uczelni.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 3.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Nie później niż na pięć dni przed dniem, w którym zgodnie z ofertą AHE miały rozpocząć się na pierwszym semestrze studia podyplomowe objęte niniejszą umową, AHE ma prawo do odstąpienia od umowy w przypadku, gdy organizacja studiów będzie nieopłacalna i zostanie podjęta decyzja o nie prowadzeniu w danym semestrze studiów podyplomowych objętych niniejszą umową. Decyzję o nie prowadzeniu w danym semestrze studiów podyplomowych objętych niniejszą umową oraz o odstąpieniu od umowy podejmuje Kierownik Centrum Kształcenia Podyplomowego działając w ramach umocowania udzielonego przez właściwy organ AHE.<fo:leader leader-length="0pt" />
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>

            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Oświadczenie woli o odstąpieniu od umowy przez Uczelnię może być złożone Słuchaczowi na piśmie lub za pośrednictwem poczty elektronicznej na indywidualny adres e-mail Słuchacza podany w procesie rekrutacji.
                    </fo:inline>
                    <fo:inline font-size="9pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 4.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Słuchacz ma prawo do rozwiązania niniejszej umowy, przy czym oświadczenie o rozwiązaniu umowy należy złożyć pod rygorem nieważności na piśmie pracownikowi Centrum Kształcenia Podyplomowego.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku rozwiązania niniejszej umowy w trybie określonym w ust. 1, AHE stosuje przepisy Kodeksu Cywilnego, w szczególności art. 746 Kodeksu Cywilnego, dotyczące zwrotu wydatków poniesionych przez przyjmującego zlecenie na wykonanie umowy zlecenia i ewentualnych roszczeń odszkodowawczych.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>3.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Niezachowanie przez Słuchacza formy czynności, o której mowa w ust. 1, będzie skutkowało nieważnością oświadczenia o rozwiązaniu umowy i dalszym naliczaniem opłaty za studia podyplomowe w następnych miesiącach.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>4.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Postanowień ust. 3, odnośnie formy oświadczenia o rozwiązaniu umowy nie stosuje się, gdy rozwiązanie umowy przez Słuchacza nastąpiło po uiszczeniu całej kwoty czesnego za studia wskazanej w § 2 ust. 3.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>5.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku dyscyplinarnego skreślenia z listy Słuchaczy o zaprzestaniu naliczania opłat decyduje data decyzji dotyczącej skreślenia.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>6.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Uczelnia ma prawo rozwiązać niniejszą umowę w każdym czasie w przypadku naruszenia przez Słuchacza postanowień niniejszej umowy.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 5.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Wszelkie zmiany dokonane przez Słuchacza w odniesieniu do postanowień umowy nie są wiążące dla Uczelni i pociągają za sobą skutek w postaci niedojścia do skutku umowy w wersji zmodyfikowanej przez Słuchacza.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Wszelkie próby dokonywania przez Słuchacza zmian w treści umowy, bez złożenia odrębnego wyraźnego oświadczenia woli o fakcie dokonania modyfikacji niniejszej umowy, będą uznane za podstęp w rozumieniu przepisów Kodeksu Cywilnego.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
                  <fo:block>3.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku samowolnego dokonania przez Słuchacza modyfikacji treści umowy, bez złożenia odrębnego wyraźnego oświadczenia woli w tym zakresie i przesłania do siedziby Uczelni podpisanej zmodyfikowanej wersji umowy, przyjmuje się, iż Słuchacz wyraził zgodę na zawarcie niniejszej umowy w treści udostępnionej za pomocą internetu.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block page-break-before="always">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 6.
              </fo:inline>
            </fo:block>
            <fo:block text-align="justify">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />W sprawach nieuregulowanych w niniejszej umowie stosuje się przepisy Regulaminu Studiów Podyplomowych, Regulaminu Rekrutacji Online i przepisy Kodeksu Cywilnego.
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-indent="0pt" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>

 	    <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 7.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Słuchacz wyraża zgodę na wystawianie dokumentów finansowych drogą elektroniczną, bez jego podpisu oraz zamieszczenie ich do jego wglądu i pobrania w Wirtualnym Pokoju Studenta.
					</fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Zamieszczenie dokumentów, o których mowa w ust. 1 w Wirtualnym Pokoju Studenta jest równoznaczne z możliwością zapoznania się przez Słuchacza z ich treścią.
					</fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 8.
              </fo:inline>
            </fo:block>
			  <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
					  <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
						  <fo:block>1.</fo:block>
					  </fo:list-item-label>
					  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
						  <fo:block>
							  <fo:inline font-family="Tahoma" font-size="10pt">
								  <fo:leader leader-length="0pt" />Umowa została sporządzona w dwóch jednobrzmiących egzemplarzach, po jednym dla każdej ze stron.
							  </fo:inline>
						  </fo:block>
					  </fo:list-item-body>
				  </fo:list-item>
			  </fo:list-block>
			  <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
					  <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
						  <fo:block>2.</fo:block>
					  </fo:list-item-label>
					  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
						  <fo:block>
							  <fo:inline font-family="Tahoma" font-size="10pt">
								  <fo:leader leader-length="0pt" />Załącznik, o którym mowa w § 1 ust. 4b do umowy stanowi jej nierozerwalną całość.
							  </fo:inline>
						  </fo:block>
					  </fo:list-item-body>
				  </fo:list-item>
			  </fo:list-block>

            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>

            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Times" font-size="12pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-pattern="space" leader-length="21pt" />
              </fo:inline>
              <fo:inline>
                <fo:leader leader-length="0pt" />_____________________
              </fo:inline>
              <fo:inline>
                <fo:leader leader-pattern="space" leader-length="15pt" />
              </fo:inline>
              <fo:inline>
                <fo:leader leader-pattern="space" leader-length="35.25pt" /><fo:leader leader-length="0pt" />______________________
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="9pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="9pt">
                <fo:leader leader-length="0pt" />                        AHE                                                             Słuchacz (czytelny podpis)
              </fo:inline>
            </fo:block>

			  <fo:block page-break-before="always"/>

			  <fo:block font-family="Arial" font-size="12pt" language="PL" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:leader />
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />ZAŁĄCZNIK 1 DO UMOWY O WARUNKACH ODPŁATNOŚCI ZA STUDIA Z DNIA
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline color="black" font-size="10pt" />
				  <fo:inline color="black" font-size="10pt">
					  <fo:leader leader-length="0pt" />
					  <xsl:value-of select="$dataWygenerowania"/>
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="12pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />zawartej pomiędzy Akademią Humanistyczno-Ekonomiczną w Łodzi a Słuchaczem
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="12pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-family="Tahoma" font-weight="bold" color="black" font-size="10pt" />
				  <fo:inline font-family="Tahoma" font-weight="bold" color="black" font-size="10pt">
					  <fo:leader leader-length="0pt" />
					  <xsl:value-of select="Student"/>
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" text-align="left" end-indent="-43.85pt" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:leader />
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />CENNIK OPŁAT ZA USŁUGI DYDAKTYCZNE NIEOBJĘTE CZESNYM
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />obowiązujący na studiach podyplomowych od 21 marca 2013 r.
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:leader />
			  </fo:block>
			  <fo:table font-family="Times" start-indent="-28.6pt" border-top-style="solid" border-top-color="black"
						border-top-width="0.5pt" border-left-style="solid" border-left-color="black" border-left-width="0.5pt"
						border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.5pt" border-right-style="solid"
						border-right-color="black" border-right-width="0.5pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
						table-layout="fixed" width="100.0%">
				  <fo:table-column column-number="1" column-width="27.5pt" />
				  <fo:table-column column-number="2" column-width="251.95pt" />
				  <fo:table-column column-number="3" column-width="80.55pt" />
				  <fo:table-column column-number="4" column-width="153pt" />
				  <fo:table-body start-indent="0pt" end-indent="0pt">
					  <fo:table-row>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  L.p.
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  WYSZCZEGÓLNIENIE
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  OPŁATY
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  UWAGI
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>1.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Reaktywacja – wznowienie studiów, wpisanie na semestr/uzupełnianie przedmiotów
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  100 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />1a
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Uzupełnianie przedmiotu w ramach reaktywacji
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  300 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Opłata jednorazowa za przedmiot
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" number-rows-spanned="3" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />1b
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Powtarzanie / wznowienie nauki na semestrze w ramach reaktywacji
								  </fo:inline>
							  </fo:block>
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="18pt" text-indent="0pt" font-family="Symbol">
										  <fo:block font-size="12pt" font-family="Arial">•</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="36pt" text-indent="0pt">
										  <fo:block>
											  <fo:inline font-size="10pt">
												  <fo:leader leader-length="0pt" />Dla studiów trwających 2 semestry opłata za semestr
											  </fo:inline>
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="5pt" padding-bottom="0pt" padding-right="5pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  1 opłaty za cały tok studiów *
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />opłata liczona od ceny studiów dla edycji do której dołącza słuchacz.
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="18pt" text-indent="0pt" font-family="Symbol">
										  <fo:block font-size="12pt" font-family="Arial">•</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="36pt" text-indent="0pt">
										  <fo:block>
											  <fo:inline font-size="10pt">
												  <fo:leader leader-length="0pt" />Dla studiów trwających 3 semestry opłata za każdy semestr pozostały do ukończenia studiów
											  </fo:inline>
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  1/3 opłaty za cały tok studiów *
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />opłata liczona od ceny studiów dla edycji do której dołącza słuchacz.
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="18pt" text-indent="0pt" font-family="Symbol">
										  <fo:block font-size="12pt" font-family="Arial">•</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="36pt" text-indent="0pt">
										  <fo:block>
											  <fo:inline font-size="10pt">
												  <fo:leader leader-length="0pt" />Dla studiów trwających 4 semestry opłata za każdy semestr pozostały do ukończenia studiów
											  </fo:inline>
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="5pt" padding-bottom="0pt" padding-right="5pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  1 opłaty za cały tok studiów *
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />opłata liczona od ceny studiów dla edycji do której dołącza słuchacz.
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />1c
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Reaktywacja – powołanie składu Komisji Egzaminacyjnej na wniosek słuchacza
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  200 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>2.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Zaświadczenie
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  10 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Obowiązuje osoby ze statusem X, R, A, N do 90 dnia po zmianie ze statusu słuchacza -S
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>3.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Zaświadczenie
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  35 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Obowiązuje osoby ze statusem  X, R, A, N od 91 dnia po zmianie ze statusu słuchacza -S
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>4.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Opłata za monit
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  20 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>5.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Opłata za dodatkowy przedmiot
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  300 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>6.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Przedłużanie okresu dyplomowania
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  50 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Opłata za każdy miesiąc przedłużenia
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>7.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Duplikat świadectwa ukończenia studiów podyplomowych
								  </fo:inline>
							  </fo:block>
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  <fo:leader />
							  </fo:block>
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  60 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
				  </fo:table-body>
			  </fo:table>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />* opłata liczona od ceny studiów dla edycji, do której dołącza słuchacz.
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" text-align="justify" end-indent="-43.85pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:leader />
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" text-align="justify" end-indent="-43.85pt" text-indent="35.45pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />Uczelnia
				  </fo:inline>
				  <fo:inline font-size="10pt">
					  <fo:leader leader-pattern="space" leader-length="33pt" />
					  <fo:leader leader-length="0pt" />
				  </fo:inline>
				  <fo:inline font-size="10pt">
					  <fo:leader leader-pattern="space" leader-length="34.5pt" />
				  </fo:inline>
				  <fo:inline font-size="10pt">
					  <fo:leader leader-pattern="space" leader-length="35.25pt" />
					  <fo:leader leader-length="0pt" />
				  </fo:inline>
				  <fo:inline font-size="10pt">
					  <fo:leader leader-pattern="space" leader-length="29.25pt" />
				  </fo:inline>
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="133pt" />Słuchacz
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="12pt" language="PL" line-height="1.7205" text-align="justify" start-indent="-18pt" end-indent="-43.85pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline>
					  <fo:leader leader-length="0pt" />
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Tahoma" font-size="12pt" language="" line-height="1.7205" text-align="justify" start-indent="-18pt" end-indent="-43.85pt" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline>
					  <fo:leader leader-length="0pt" />______________________
					  <fo:leader leader-length="133pt" />_________________________
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="12pt" language="PL" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:leader />
			  </fo:block>
			  
          </fo:block>
          <fo:block id="IDACIYWC" />
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>
</xsl:stylesheet>'
     
 WHERE Opis like 'Umowa podyplomowe PD3'
GO
UPDATE [dbo].[WUWydrukWersja]
   SET 
      [Szablon] = '<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:decimal-format name="pln"
decimal-separator="," grouping-separator="." NaN="" />
  <xsl:param name="dataWygenerowania"/>
  <xsl:template match="/Dane">
    <fo:root font-family="Times" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml">
      <fo:layout-master-set xmlns:rx="http://www.renderx.com/XSL/Extensions" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882">
        <fo:simple-page-master master-name="section1-first-page" page-width="8.268055555555555in" page-height="11.693055555555556in" margin-top="35.4pt" margin-bottom="14.05pt" margin-right="70.85pt" margin-left="70.85pt">
          <fo:region-body margin-top="50pt" margin-bottom="50pt"></fo:region-body>
          <fo:region-before region-name="first-page-header" extent="11in"></fo:region-before>
          <fo:region-after region-name="first-page-footer" extent="11in" display-align="after"></fo:region-after>
        </fo:simple-page-master>
        <fo:simple-page-master master-name="section1-odd-page" page-width="8.268055555555555in" page-height="11.693055555555556in" margin-top="35.4pt" margin-bottom="14.05pt" margin-right="70.85pt" margin-left="70.85pt">
          <fo:region-body margin-top="50pt" margin-bottom="50pt"></fo:region-body>
          <fo:region-before region-name="odd-page-header" extent="11in"></fo:region-before>
          <fo:region-after region-name="odd-page-footer" extent="11in" display-align="after"></fo:region-after>
        </fo:simple-page-master>
        <fo:simple-page-master master-name="section1-even-page" page-width="8.268055555555555in" page-height="11.693055555555556in" margin-top="35.4pt" margin-bottom="14.05pt" margin-right="70.85pt" margin-left="70.85pt">
          <fo:region-body margin-top="50pt" margin-bottom="50pt"></fo:region-body>
          <fo:region-before region-name="even-page-header" extent="11in"></fo:region-before>
          <fo:region-after region-name="even-page-footer" extent="11in" display-align="after"></fo:region-after>
        </fo:simple-page-master>
        <fo:page-sequence-master master-name="section1-page-sequence-master">
          <fo:repeatable-page-master-alternatives>
            <fo:conditional-page-master-reference odd-or-even="odd" master-reference="section1-odd-page" />
            <fo:conditional-page-master-reference odd-or-even="even" master-reference="section1-even-page" />
          </fo:repeatable-page-master-alternatives>
        </fo:page-sequence-master>
      </fo:layout-master-set>
      <fo:page-sequence master-reference="section1-page-sequence-master" format="1" xmlns:rx="http://www.renderx.com/XSL/Extensions" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882">
        <fo:static-content flow-name="first-page-header">
          <fo:retrieve-marker retrieve-class-name="first-page-header" retrieve-position="first-including-carryover" retrieve-boundary="page" />
        </fo:static-content>
        <fo:static-content flow-name="first-page-footer">
          <fo:retrieve-marker retrieve-class-name="first-page-footer" retrieve-position="first-including-carryover" retrieve-boundary="page" />
        </fo:static-content>
        <fo:static-content flow-name="odd-page-header">
          <fo:block font-family="Times" font-size="18pt" language="PL" text-align="left" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
            <fo:inline>
              <fo:external-graphic content-width="186.1pt" content-height="51.45pt" src="https://www.ahe.lodz.pl/sites/default/files/logo_ckp.jpg" />
            </fo:inline>
          </fo:block>
        </fo:static-content>
        <fo:static-content flow-name="odd-page-footer">
          <fo:retrieve-marker retrieve-class-name="odd-page-footer" retrieve-position="first-including-carryover" retrieve-boundary="page" />
        </fo:static-content>
        <fo:static-content flow-name="even-page-header">
          <fo:block font-family="Times" font-size="18pt" language="PL" text-align="left" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
            <fo:inline>
              <fo:external-graphic content-width="186.1pt" content-height="51.45pt" src="https://www.ahe.lodz.pl/sites/default/files/logo_ckp.jpg" />
            </fo:inline>
          </fo:block>
        </fo:static-content>
        <fo:static-content flow-name="even-page-footer">
          <fo:retrieve-marker retrieve-class-name="odd-page-footer" retrieve-position="first-including-carryover" retrieve-boundary="page" />
        </fo:static-content>
        <fo:static-content flow-name="xsl-footnote-separator">
          <fo:block>
            <fo:leader leader-pattern="rule" leader-length="144pt" rule-thickness="0.5pt" rule-style="solid" color="gray" />
          </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">
          <fo:block widows="2" orphans="2" font-size="10pt" line-height="1.147" white-space-collapse="false">
            <fo:marker marker-class-name="first-page-header" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:marker marker-class-name="first-page-footer" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:marker marker-class-name="odd-page-header" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:block font-family="Arial" font-size="12pt" language="PL">
                <fo:inline>
                  <fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline>
                  <fo:block />
                </fo:inline>
                <fo:inline>
                  <fo:leader leader-pattern="space"  />
                </fo:inline>
              </fo:block>
            </fo:marker>
            <fo:marker marker-class-name="odd-page-footer" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:block font-family="Arial" font-size="7pt" language="PL" line-height="1.147" text-align="center">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="PL" line-height="1.147" text-align="center">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="PL" line-height="1.147" text-align="center" color="#999999">
                <fo:inline color="#999999" font-size="7pt">
                  <fo:leader leader-length="0pt" />Centrum Kształcenia Podyplomowego Akademii Humanistyczno-Ekonomicznej w Łodzi
                </fo:inline>
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="DE" line-height="1.147" text-align="center" color="#999999">
                <fo:inline color="#999999" font-size="7pt" language="DE">
                  <fo:leader leader-length="0pt" />ul. Sterlinga 26, 90-212 Łódź; Tel: (+48 42) 63 15 039, (+48 42) 29 95 637.
                </fo:inline>
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="EN-US" line-height="1.147" text-align="center" color="#999999">
                <fo:inline color="#999999" font-size="7pt" language="EN-US">
                  <fo:leader leader-length="0pt" />mail: ckp@ahe.lodz.pl,
                </fo:inline>
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="EN-US" line-height="1.147" text-align="center" color="#999999">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="EN-US" line-height="1.147" text-align="center">
                <fo:inline color="#999999" font-size="7pt" language="EN-US">
                  <fo:leader leader-length="0pt" />www.ckp-lodz.pl
                </fo:inline>
              </fo:block>
            </fo:marker>
            <fo:marker marker-class-name="even-page-header" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:marker marker-class-name="even-page-footer" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />UMOWA<fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />dotycząca studiów podyplomowych<fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="Kierunek"/>
                <fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />zawarta w dniu <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="BiezacaData" />
              <fo:inline font-family="Arial" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="$dataWygenerowania"/>
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" /> pomiędzy <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />Akademią Humanistyczno - Ekonomiczną w Łodzi <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" /> wpisaną pod numerem 30 do Rejestru Uczelni Niepublicznych prowadzonego przez ministra właściwego do spraw szkolnictwa wyższego - zwaną dalej <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />AHE<fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />  lub<fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" /> Uczelnią<fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />, reprezentowaną na podstawie stosownego pełnomocnictwa, przez: <fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Verdana" font-size="10pt" font-weight="bold">
                <fo:leader leader-length="0pt" />Dyrektora Centrum Kształcenia Podyplomowego Paulinę Gocała<fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />a
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />Panem/ią <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="Student" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="Student"/>
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" start-indent="72pt" text-indent="-72pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />zwanym/ą dalej Słuchaczem,
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" start-indent="72pt" text-indent="-72pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />zameldowanym/ą <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="AdresSP" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="AdresSP"/>
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" start-indent="72pt" text-indent="-72pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" start-indent="48pt"  xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />powiat <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="AdresSPPowiat"/>
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />                     województwo <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="AdresSPWojewodztwo"/>
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block id="DOSeriaNumer" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:block id="DOTSNazwa" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />legitymującym/ą się dowodem osobistym <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="DOTSSeriaNumer"/>,
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="-10pt" />PESEL<fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:inline color="white">-</fo:inline>
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="PESEL"/>.
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 1.
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="12pt" text-align="justify" language="PL" start-indent="18pt" text-indent="-18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />1.  AHE zobowiązuje się do prowadzenia zajęć dydaktycznych i organizowania zaliczeń i egzaminów zgodnie z wymaganiami studiów podyplomowych: <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="Kierunek_1" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="Kierunek"/>
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />.
              </fo:inline>
            </fo:block>



            <fo:block font-family="Tahoma" font-size="12pt" text-align="justify" language="PL" start-indent="18pt" text-indent="-18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />2.  Słuchacz oświadcza, iż przed podpisaniem niniejszej umowy zapoznał się z Regulaminem Studiów Podyplomowych wraz z załącznikami (które dostępne są w Centrum Kształcenia Podyplomowego i na stronie internetowej www.ckp-lodz.pl) oraz Regulaminem Rekrutacji Online, jeśli uczestniczył w rekrutacji online (który dostępny jest na stronie internetowej rekrutacji online www.rekrutacja.ahe.lodz.pl) oraz zobowiązuje się przestrzegać ich postanowień. Słuchacz oświadcza, iż zapoznał się z prawem odstąpienia od umowy zawartej na odległość lub poza lokalem Uczelni. <fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="12pt" text-align="justify" language="PL" start-indent="18pt" text-indent="-18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />3.  Słuchacz zobowiązany jest na mocy umowy wnieść określone opłaty.<fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="12pt" text-align="justify" language="PL" start-indent="18pt" text-indent="-18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />4.  Opłatami, o których mowa w ust. 3 są:<fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:list-block>
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" start-indent="38pt" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="38pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>a.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="50pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-size="10pt">
                      <fo:leader leader-length="0pt" />czesne<fo:leader leader-length="0pt" />
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block>
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" start-indent="38pt" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="38pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>b.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="50pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-size="10pt">
                      <fo:leader leader-length="0pt" />inne opłaty określone w cenniku opłat za usługi nieobjęte czesnym stanowiącym załącznik do Umowy.<fo:leader leader-length="0pt" />
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>

			  <fo:block font-family="Tahoma" font-size="12pt" text-align="justify" language="PL" start-indent="18pt" text-indent="-18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-family="Tahoma" font-size="10pt">
					  <fo:leader leader-length="0pt" />5.  Opłaty, o których mowa w ust. 4b płatne są w przypadkach wskazanych w Cenniku i w terminach w nim podanych.<fo:leader leader-length="0pt" />
				  </fo:inline>
			  </fo:block>

            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 2.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Studia podyplomowe będące przedmiotem niniejszej umowy trwają <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline id="UmowaLiczbaSem" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />4<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> semestry akademickie. Czas trwania studiów podyplomowych może ulec zmianie w przypadku zmiany przepisów regulujących zasady organizacji i przeprowadzania studiów podyplomowych.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" start-indent="17.85pt" end-indent="-5.4pt" text-indent="-17.85pt" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Nieuczestniczenie w zajęciach dydaktycznych nie zwalnia Słuchacza z obowiązku uiszczania opłat za studia podyplomowe.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>3.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Słuchacz zobowiązany jest do<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> zapłaty czesnego<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> w wysokości <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline id="UmowaCzesne10Miesiec" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" /> <xsl:value-of select="format-number(UmowaCzesne10Miesiec, &quot;##0,00&quot;, &quot;pln&quot;)"/> zł<fo:leader leader-length="0pt" />,<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> za cały cykl studiów podyplomowych.<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" color="#3366FF" font-size="10pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> Czesne płatne jest w <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt" font-weight="bold">
                      <fo:leader leader-length="0pt" />10<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> równych miesięcznych ratach, po <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline id="UmowaCzesne" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" /><xsl:value-of select="format-number(UmowaCzesne, &quot;##0,00&quot;, &quot;pln&quot;)"/> zł<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> każda.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>4.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Zapłaty czesnego należy dokonywać do<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" /> 5-go<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> dnia każdego miesiąca, począwszy od miesiąca <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt" font-weight="bold">
                      <fo:leader leader-length="0pt" /> listopada<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> dla studiów rozpoczynających się w semestrze zimowym, a dla studiów rozpoczynających się w semestrze letnim od miesiąca <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt" font-weight="bold">
                      <fo:leader leader-length="0pt" />kwietnia.<fo:leader leader-length="0pt" />
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>5.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku niedokonania wpłaty czesnego, oraz innych opłat, o których mowa w § 1 ust. 4b w terminie, wskazanym w ust. 4, Słuchacz zobowiązany jest do zapłaty zaległej kwoty powiększonej o odsetki umowne za opóźnienie. Odsetki umowne za opóźnienie wynoszą w stosunku rocznym dwukrotność odsetek ustawowych za opóźnienie od każdej zaległej kwoty, nie więcej jednak niż czterokrotność wysokości stopy kredytu lombardowego Narodowego Banku Polskiego.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>6.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku przyjęcia na studia podyplomowe po upływie terminu płatności czesnego w danym miesiącu, Słuchacz zobowiązuje się do wpłaty zaległych rat czesnego w ciągu siedmiu dni licząc od daty podpisania niniejszej umowy.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>7.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Wpłaty  należy  dokonywać  przekazem pocztowym lub poprzez przelew na indywidualny numer konta:
                    </fo:inline>
                  </fo:block>
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />PKO BP nr rachunku <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline id="NrRORPM3" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />
                      <xsl:value-of select="NrRORPM3PKO"/>
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" start-indent="18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />Blankiety wpłaty czesnego powinny być wypełnione czytelnie z podaniem przeznaczenia (czesne, odsetki) wpłaconej kwoty oraz z dopiskiem <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />studia podyplomowe: <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="Kierunek_2" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="Kierunek"/>
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>8.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Za datę dokonania wpłaty czesnego uznaje się datę wpływu środków pieniężnych na rachunek bankowy Uczelni.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 3.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Nie później niż na pięć dni przed dniem, w którym zgodnie z ofertą AHE miały rozpocząć się na pierwszym semestrze studia podyplomowe objęte niniejszą umową, AHE ma prawo do odstąpienia od umowy w przypadku, gdy organizacja studiów będzie nieopłacalna i zostanie podjęta decyzja o nie prowadzeniu w danym semestrze studiów podyplomowych objętych niniejszą umową. Decyzję o nie prowadzeniu w danym semestrze studiów podyplomowych objętych niniejszą umową oraz o odstąpieniu od umowy podejmuje Kierownik Centrum Kształcenia Podyplomowego działając w ramach umocowania udzielonego przez właściwy organ AHE.<fo:leader leader-length="0pt" />
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>

            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Oświadczenie woli o odstąpieniu od umowy przez Uczelnię może być złożone Słuchaczowi na piśmie lub za pośrednictwem poczty elektronicznej na indywidualny adres e-mail Słuchacza podany w procesie rekrutacji.
                    </fo:inline>
                    <fo:inline font-size="9pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 4.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Słuchacz ma prawo do rozwiązania niniejszej umowy, przy czym oświadczenie o rozwiązaniu umowy należy złożyć pod rygorem nieważności na piśmie pracownikowi Centrum Kształcenia Podyplomowego.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku rozwiązania niniejszej umowy w trybie określonym w ust. 1, AHE stosuje przepisy Kodeksu Cywilnego, w szczególności art. 746 Kodeksu Cywilnego, dotyczące zwrotu wydatków poniesionych przez przyjmującego zlecenie na wykonanie umowy zlecenia i ewentualnych roszczeń odszkodowawczych.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>3.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Niezachowanie przez Słuchacza formy czynności, o której mowa w ust. 1, będzie skutkowało nieważnością oświadczenia o rozwiązaniu umowy i dalszym naliczaniem opłaty za studia podyplomowe w następnych miesiącach.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>4.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Postanowień ust. 3, odnośnie formy oświadczenia o rozwiązaniu umowy nie stosuje się, gdy rozwiązanie umowy przez Słuchacza nastąpiło po uiszczeniu całej kwoty czesnego za studia wskazanej w § 2 ust. 3.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>5.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku dyscyplinarnego skreślenia z listy Słuchaczy o zaprzestaniu naliczania opłat decyduje data decyzji dotyczącej skreślenia.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>6.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Uczelnia ma prawo rozwiązać niniejszą umowę w każdym czasie w przypadku naruszenia przez Słuchacza postanowień niniejszej umowy.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 5.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Wszelkie zmiany dokonane przez Słuchacza w odniesieniu do postanowień umowy nie są wiążące dla Uczelni i pociągają za sobą skutek w postaci niedojścia do skutku umowy w wersji zmodyfikowanej przez Słuchacza.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Wszelkie próby dokonywania przez Słuchacza zmian w treści umowy, bez złożenia odrębnego wyraźnego oświadczenia woli o fakcie dokonania modyfikacji niniejszej umowy, będą uznane za podstęp w rozumieniu przepisów Kodeksu Cywilnego.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
                  <fo:block>3.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku samowolnego dokonania przez Słuchacza modyfikacji treści umowy, bez złożenia odrębnego wyraźnego oświadczenia woli w tym zakresie i przesłania do siedziby Uczelni podpisanej zmodyfikowanej wersji umowy, przyjmuje się, iż Słuchacz wyraził zgodę na zawarcie niniejszej umowy w treści udostępnionej za pomocą internetu.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block page-break-before="always">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 6.
              </fo:inline>
            </fo:block>
            <fo:block text-align="justify">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />W sprawach nieuregulowanych w niniejszej umowie stosuje się przepisy Regulaminu Studiów Podyplomowych, Regulaminu Rekrutacji Online i przepisy Kodeksu Cywilnego.
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-indent="0pt" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            

  	    <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 7.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Słuchacz wyraża zgodę na wystawianie dokumentów finansowych drogą elektroniczną, bez jego podpisu oraz zamieszczenie ich do jego wglądu i pobrania w Wirtualnym Pokoju Studenta.
					</fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Zamieszczenie dokumentów, o których mowa w ust. 1 w Wirtualnym Pokoju Studenta jest równoznaczne z możliwością zapoznania się przez Słuchacza z ich treścią.
					</fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>



	    <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 8.
              </fo:inline>
            </fo:block>
			  <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
					  <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
						  <fo:block>1.</fo:block>
					  </fo:list-item-label>
					  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
						  <fo:block>
							  <fo:inline font-family="Tahoma" font-size="10pt">
								  <fo:leader leader-length="0pt" />Umowa została sporządzona w dwóch jednobrzmiących egzemplarzach, po jednym dla każdej ze stron.
							  </fo:inline>
						  </fo:block>
					  </fo:list-item-body>
				  </fo:list-item>
			  </fo:list-block>
			  <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
					  <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
						  <fo:block>2.</fo:block>
					  </fo:list-item-label>
					  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
						  <fo:block>
							  <fo:inline font-family="Tahoma" font-size="10pt">
								  <fo:leader leader-length="0pt" />Załącznik, o którym mowa w § 1 ust. 4b do umowy stanowi jej nierozerwalną całość.
							  </fo:inline>
						  </fo:block>
					  </fo:list-item-body>
				  </fo:list-item>
			  </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-pattern="space" leader-length="14.25pt" />
                <fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Times" font-size="12pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-pattern="space" leader-length="21pt" />
              </fo:inline>
              <fo:inline>
                <fo:leader leader-length="0pt" />_____________________
              </fo:inline>
              <fo:inline>
                <fo:leader leader-pattern="space" leader-length="15pt" />
              </fo:inline>
              <fo:inline>
                <fo:leader leader-pattern="space" leader-length="35.25pt" /><fo:leader leader-length="0pt" />______________________
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="9pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="9pt">
                <fo:leader leader-length="0pt" />                        AHE                                                             Słuchacz (czytelny podpis)
              </fo:inline>
            </fo:block>

			  <fo:block page-break-before="always"/>

			  <fo:block font-family="Arial" font-size="12pt" language="PL" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:leader />
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />ZAŁĄCZNIK 1 DO UMOWY O WARUNKACH ODPŁATNOŚCI ZA STUDIA Z DNIA
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline color="black" font-size="10pt" />
				  <fo:inline color="black" font-size="10pt">
					  <fo:leader leader-length="0pt" />
					  <xsl:value-of select="$dataWygenerowania"/>
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="12pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />zawartej pomiędzy Akademią Humanistyczno-Ekonomiczną w Łodzi a Słuchaczem
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="12pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-family="Tahoma" font-weight="bold" color="black" font-size="10pt" />
				  <fo:inline font-family="Tahoma" font-weight="bold" color="black" font-size="10pt">
					  <fo:leader leader-length="0pt" />
					  <xsl:value-of select="Student"/>
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" text-align="left" end-indent="-43.85pt" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:leader />
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />CENNIK OPŁAT ZA USŁUGI DYDAKTYCZNE NIEOBJĘTE CZESNYM
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />obowiązujący na studiach podyplomowych od 21 marca 2013 r.
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:leader />
			  </fo:block>
			  <fo:table font-family="Times" start-indent="-28.6pt" border-top-style="solid" border-top-color="black"
						border-top-width="0.5pt" border-left-style="solid" border-left-color="black" border-left-width="0.5pt"
						border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.5pt" border-right-style="solid"
						border-right-color="black" border-right-width="0.5pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
						table-layout="fixed" width="100.0%">
				  <fo:table-column column-number="1" column-width="27.5pt" />
				  <fo:table-column column-number="2" column-width="251.95pt" />
				  <fo:table-column column-number="3" column-width="80.55pt" />
				  <fo:table-column column-number="4" column-width="153pt" />
				  <fo:table-body start-indent="0pt" end-indent="0pt">
					  <fo:table-row>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  L.p.
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  WYSZCZEGÓLNIENIE
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  OPŁATY
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  UWAGI
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>1.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Reaktywacja – wznowienie studiów, wpisanie na semestr/uzupełnianie przedmiotów
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  100 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />1a
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Uzupełnianie przedmiotu w ramach reaktywacji
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  300 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Opłata jednorazowa za przedmiot
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" number-rows-spanned="3" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />1b
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Powtarzanie / wznowienie nauki na semestrze w ramach reaktywacji
								  </fo:inline>
							  </fo:block>
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="18pt" text-indent="0pt" font-family="Symbol">
										  <fo:block font-size="12pt" font-family="Arial">•</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="36pt" text-indent="0pt">
										  <fo:block>
											  <fo:inline font-size="10pt">
												  <fo:leader leader-length="0pt" />Dla studiów trwających 2 semestry opłata za semestr
											  </fo:inline>
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="5pt" padding-bottom="0pt" padding-right="5pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  1 opłaty za cały tok studiów *
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />opłata liczona od ceny studiów dla edycji do której dołącza słuchacz.
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="18pt" text-indent="0pt" font-family="Symbol">
										  <fo:block font-size="12pt" font-family="Arial">•</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="36pt" text-indent="0pt">
										  <fo:block>
											  <fo:inline font-size="10pt">
												  <fo:leader leader-length="0pt" />Dla studiów trwających 3 semestry opłata za każdy semestr pozostały do ukończenia studiów
											  </fo:inline>
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  1/3 opłaty za cały tok studiów *
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />opłata liczona od ceny studiów dla edycji do której dołącza słuchacz.
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="18pt" text-indent="0pt" font-family="Symbol">
										  <fo:block font-size="12pt" font-family="Arial">•</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="36pt" text-indent="0pt">
										  <fo:block>
											  <fo:inline font-size="10pt">
												  <fo:leader leader-length="0pt" />Dla studiów trwających 4 semestry opłata za każdy semestr pozostały do ukończenia studiów
											  </fo:inline>
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="5pt" padding-bottom="0pt" padding-right="5pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  1 opłaty za cały tok studiów *
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />opłata liczona od ceny studiów dla edycji do której dołącza słuchacz.
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />1c
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Reaktywacja – powołanie składu Komisji Egzaminacyjnej na wniosek słuchacza
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  200 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>2.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Zaświadczenie
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  10 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Obowiązuje osoby ze statusem X, R, A, N do 90 dnia po zmianie ze statusu słuchacza -S
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>3.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Zaświadczenie
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  35 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Obowiązuje osoby ze statusem  X, R, A, N od 91 dnia po zmianie ze statusu słuchacza -S
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>4.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Opłata za monit
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  20 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>5.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Opłata za dodatkowy przedmiot
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  300 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>6.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Przedłużanie okresu dyplomowania
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  50 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Opłata za każdy miesiąc przedłużenia
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>7.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Duplikat świadectwa ukończenia studiów podyplomowych
								  </fo:inline>
							  </fo:block>
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  <fo:leader />
							  </fo:block>
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  60 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
				  </fo:table-body>
			  </fo:table>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />* opłata liczona od ceny studiów dla edycji, do której dołącza słuchacz.
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" text-align="justify" end-indent="-43.85pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:leader />
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" text-align="justify" end-indent="-43.85pt" text-indent="35.45pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />Uczelnia
				  </fo:inline>
				  <fo:inline font-size="10pt">
					  <fo:leader leader-pattern="space" leader-length="33pt" />
					  <fo:leader leader-length="0pt" />
				  </fo:inline>
				  <fo:inline font-size="10pt">
					  <fo:leader leader-pattern="space" leader-length="34.5pt" />
				  </fo:inline>
				  <fo:inline font-size="10pt">
					  <fo:leader leader-pattern="space" leader-length="35.25pt" />
					  <fo:leader leader-length="0pt" />
				  </fo:inline>
				  <fo:inline font-size="10pt">
					  <fo:leader leader-pattern="space" leader-length="29.25pt" />
				  </fo:inline>
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="133pt" />Słuchacz
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="12pt" language="PL" line-height="1.7205" text-align="justify" start-indent="-18pt" end-indent="-43.85pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline>
					  <fo:leader leader-length="0pt" />
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Tahoma" font-size="12pt" language="" line-height="1.7205" text-align="justify" start-indent="-18pt" end-indent="-43.85pt" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline>
					  <fo:leader leader-length="0pt" />______________________
					  <fo:leader leader-length="133pt" />_________________________
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="12pt" language="PL" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:leader />
			  </fo:block>
			  
          </fo:block>
          <fo:block id="IDACIYWC" />
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>
</xsl:stylesheet>'
     
 WHERE Opis like 'Umowa podyplomowe PD4'
GO
UPDATE [dbo].[WUWydrukWersja]
   SET 
      [Szablon] = '<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:decimal-format name="pln"
decimal-separator="," grouping-separator="." NaN="" />
  <xsl:param name="dataWygenerowania"/>
  <xsl:template match="/Dane">
    <fo:root font-family="Times" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml">
      <fo:layout-master-set xmlns:rx="http://www.renderx.com/XSL/Extensions" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882">
        <fo:simple-page-master master-name="section1-first-page" page-width="8.268055555555555in" page-height="11.693055555555556in" margin-top="35.4pt" margin-bottom="14.05pt" margin-right="70.85pt" margin-left="70.85pt">
          <fo:region-body margin-top="55pt" margin-bottom="55pt"></fo:region-body>
          <fo:region-before region-name="first-page-header" extent="11in"></fo:region-before>
          <fo:region-after region-name="first-page-footer" extent="11in" display-align="after"></fo:region-after>
        </fo:simple-page-master>
        <fo:simple-page-master master-name="section1-odd-page" page-width="8.268055555555555in" page-height="11.693055555555556in" margin-top="35.4pt" margin-bottom="14.05pt" margin-right="70.85pt" margin-left="70.85pt">
          <fo:region-body margin-top="55pt" margin-bottom="55pt"></fo:region-body>
          <fo:region-before region-name="odd-page-header" extent="11in"></fo:region-before>
          <fo:region-after region-name="odd-page-footer" extent="11in" display-align="after"></fo:region-after>
        </fo:simple-page-master>
        <fo:simple-page-master master-name="section1-even-page" page-width="8.268055555555555in" page-height="11.693055555555556in" margin-top="35.4pt" margin-bottom="14.05pt" margin-right="70.85pt" margin-left="70.85pt">
          <fo:region-body margin-top="55pt" margin-bottom="55pt"></fo:region-body>
          <fo:region-before region-name="even-page-header" extent="11in"></fo:region-before>
          <fo:region-after region-name="even-page-footer" extent="11in" display-align="after"></fo:region-after>
        </fo:simple-page-master>
        <fo:page-sequence-master master-name="section1-page-sequence-master">
          <fo:repeatable-page-master-alternatives>
            <fo:conditional-page-master-reference odd-or-even="odd" master-reference="section1-odd-page" />
            <fo:conditional-page-master-reference odd-or-even="even" master-reference="section1-even-page" />
          </fo:repeatable-page-master-alternatives>
        </fo:page-sequence-master>
      </fo:layout-master-set>
      <fo:page-sequence master-reference="section1-page-sequence-master" format="1" xmlns:rx="http://www.renderx.com/XSL/Extensions" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882">
        <fo:static-content flow-name="first-page-header">
          <fo:retrieve-marker retrieve-class-name="first-page-header" retrieve-position="first-including-carryover" retrieve-boundary="page" />
        </fo:static-content>
        <fo:static-content flow-name="first-page-footer">
          <fo:retrieve-marker retrieve-class-name="first-page-footer" retrieve-position="first-including-carryover" retrieve-boundary="page" />
        </fo:static-content>
        <fo:static-content flow-name="odd-page-header">
          <fo:block font-family="Times" font-size="18pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
            <fo:inline>
              <fo:external-graphic content-width="186.1pt" content-height="51.45pt" src="https://www.ahe.lodz.pl/sites/default/files/logo_ckp.jpg" />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:external-graphic content-width="116.85pt" content-height="44.15pt" src="url(&quot;data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB&#xA;AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEB&#xA;AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wgARCAA7AJwDASIA&#xA;AhEBAxEB/8QAHgAAAQUBAQEBAQAAAAAAAAAAAAUGBwgJBAMKAQL/xAAaAQEAAwEBAQAAAAAAAAAA&#xA;AAAABQYHCAQD/9oADAMBAAIQAxAAAAHdiIK4xNR7roAZrL/N2p6FFeagw3ltFHlcqx9rw/07/sWt&#xA;714zOZW9QLAFXpTJOAAAACFsPforodnWqVQVK8z9z/v0eO+CXnZvtFeuUBX7m8zSVCtNJuhOc9V1&#xA;jJoNaXViW/TZ4x49jWNe+euRza9eyL10ObGnXDKHNNTspAK73cTbNDH9e3Fr83aS4tXLR1XGe7ha&#xA;DI7vxGUGikOEevLECaS9H3QonJK0M20Gg7wCEptKpJV5RbQFIncI7p6El/8AbXmWnYV2BANFgQAA&#xA;A8PcEBfAAD//xAAlEAACAgICAQQCAwAAAAAAAAAFBgMEAgcAARYIECA1EhQmMED/2gAIAQEAAQUC&#xA;JGRAaPrZK5NY80rc80rc80rc80rcat/iBOCj6ju7xr+zfeKwNr5JmvzUUi84r3AewKV6zzZTRPBy&#xA;gujUkQvAbbs9kjZCsYhcZL2FtjtXe6rrLcIDm3usUXredqz8n5JoP65aFuOsSYXaoyzGwdJDhWFv&#xA;lpfHKAOEXTNEybwf0+gjFYaUAXCLJ4gMwr2A6yJ5Mp17U8ikMlhHiq42T2vlBgrHkZQZKQ9ncoOE&#xA;TVVkk8EItXq9GuBCUGBr2zcJ9EFFcGAhqj9SbebILeiq7bCchdFyZi/VXcLBGPPbkfYAEzS9zB2+&#xA;xvBXwC7YtSWHE4dclBl2K++frNGxW9SnJpYq8UMnW02XDCKvEUK4sEOpIesoNvQR90A/5dCFH6k0&#xA;BVT+VcKoD8KdFNyqlAOtbzHXXEmoIqryEN7KjktjLuCGLbg6RrJOTlS8lJpOKZfA2L/GAV0dBLYh&#xA;rQ4Jghhgya60dRP1NPF+m7fyJu66666UfqWGsQiOC6JXOYetz2Mp1+MawZDy8dBiFnL05MQWn6tr&#xA;8sd4Ld7sH/gQVJ7l3wuxwjr6YiPFj3NNY0bVJalh4XY4GHZi6fzlijmjqChlDL/P/8QAKxEAAgIB&#xA;AgUDBAIDAAAAAAAAAQIDBBEFEgYHEyEiABRBICMxMkJRFTOx/9oACAEDAQE/AeAdJ0fXNUv1dbFj&#xA;28FESxNVyzpM08SK0q9avmPaz9lkLZ29vUvLXh7Y0tJPfwqNz9KzcSeMf3LWkcSAfnyj6qfO/wBU&#xA;+Xug3Z0gjqSZPdmNuzhEH7OfufHx+MnA+fXMDg7h/Q+GDqmi1bAnTVadFbktuWSOz1FnM6xQMxQo&#xA;pVF62B38ULYZvr5S2Xq69qHTrLbNrTPayQEbusjW67BNuGDHco/Kt/wi7BpVCeMo9mnfG0tXpzJZ&#xA;WJzjwYMrYb+LRGcjvjYFPqaGJrK0INtaS6os6g6Da/SRVUwRqC6xmVupI6BiBktl49oPNfVks8Ox&#xA;6fUiWKjW1Cts8fN2jjmVcbvJEXLEDs8jMZJfIhU+rlRLYi1fVmrPFFP/AIcrHJJJFE67rlYSdF5S&#xA;qpL0t43BlYIX2kN+aiRRWqyCVJ7U88MfUXLRVTLIil+oQOtON3iyZijP3Fd3C7J0MXFdhm3pHUzL&#xA;KyjOyGKmgLDv+pyAO4/f49cwm36IzYxu1CFsf1kTHH16Zq97R5ZJqEqxSSx9Jy0ccmU3K+MSKwHk&#xA;o7jv6TjriVGV1uxhkYOp9pV7MpBB/wBXwQPU3MvjCf3Jl1CAtb2idl06gjOqsXCbkrghdx8lUgNh&#xA;QRhQBqPEur6rX9rdsJJD1Fk2rBDGd6AhTujRW/ke2cfX/8QAKBEAAQQBAwMEAwEBAAAAAAAAAwEC&#xA;BAUGERITAAcUFSAhIwgiMVOC/9oACAECAQE/Ae7t2PC8OoL5tvfUy21yWtdYwMeqruGF448gmw4J&#xA;80C8SqNq7xPHIe9hGDG5jHKsvPe7hoUi2xPLqXMqqKzmlrVUsEFzXB/0ssflwh2IBouusiM2bERq&#xA;K5ZO1FXrDe6XeHNb+HQ1t3CG8+4sqWWlq1BAghRHSpp9IqfoJqojW7m8pniCjmuIi9do7iszAt1B&#xA;M+RcUdNDlMfmchkaGlraRk3zHVIIIY4nwK34Z5G1I5yO4mLM4ySV93dPObHGqGjp24/Hy6tvshbX&#xA;nx2QLkdKceEdUbE+mQ1SvcNn1mjnE7TcjRv+zrIqrtThd5CsKy8ybE8xGoykoMWPGyFtVKLt3QTq&#xA;ZnA57nO4TwH2ZBka7j8bx3N3W1DVzcmj4Hjbh43Z51FFkWfTBsHGmsqYwWItHWRgmlBhltjNl2Uy&#xA;EKSYQ0KrnmlwdBLgPc8FxnIcFxGH6RhdFQ2MYY1Yo5VmSIaINpTI77Axmu3vaF6+RJK90ueqme0I&#xA;Pd3gkGhYGeXSzqyFlbrSPDqj2FjX1R4kGREl+rSamXZnjRm2L4qLG3sMOSCISS+O5HuTrH4tTVXt&#xA;NHdYx7nJbO3rofkxCKepx7zpoQmm+d/La5Y0jljFiK+uryKkwUybMaJIp0kR/wAk7e0kPJHg46CR&#xA;dWMhE0bFooOIiZu0+GoEjCgjNRNE3HRP7r1+PZ/K7oS5O3Z5NRdH2a67OaVEJt/53ae/LMLx7Noc&#xA;aBkUQkuNEk+WBg5UmKrT8Tw7lfGIJzk4yPTa5Vb866aonUfsR21iyASo9PLGeMYcgBEuLXUZgvQg&#xA;3prLVNWvajk1RU+Or/DMNvx2aEx9IZ70EONdy4tnaNk2EeCRTBiIRZW6LC5lR5IoFRpUGAJHPAAQ&#xA;24v2qwrDrP1egrTxZ/jFicpLCdJbwGVjiN45ByD1VRs/bbuTT4X5X3//xABCEAACAgECBQEEBAkK&#xA;BwAAAAACAwEEBRESAAYTFCEiFSMxMgc0QVEIECRCYWJxk9EWICUwM0NSc4SxQERTcoGho//aAAgB&#xA;AQAGPwIHZfKY7FqZMitmRu1qQMKI1kQKyxcGUR5mB1mI4ZXoXaOR2FtE6WTpPhn6wQthzMTx9Sf+&#xA;8D+HH1J/7wP4cfUn/vA/hx9Sf+8D+HB1sHQjLZOD2F1HyGPr7SiD6jlrmbBfGBCtMhr87h02lSw3&#xA;NOGq0hyFhNZOTxj2dCsyywVJ7urbky6G4tXWQtaqHz25xrMf1mD5gzeOdlLxv9hUFsY/2dSr2WDc&#xA;yGROul9XuL6qqGLopdY7VrWiVmu9aPRFvABPYsge2uUrTlt02Dr1Ulqhb9fLVFVDYesdMR0Hjrcu&#xA;ZmctVDz7Kymkltj8xREUKn9PSOjM/Zu+32Zma54PLCXSJNrUUMb/AIBYyAJJl+at8RE6iINaUx+I&#xA;OWsWZ93bEe+JGsthTvCaS9nq6tnXcyI9XRkAjWHzxPMHMak3cs0duOxbIE1JeY7hExnUWOD5rDdJ&#xA;CuMTCt7ZAixOJAfOTvg68xahBaKYGdrIOgRjpgK6wNlYaCEnsVHzRwnDY3GIutZjSyG513s4gAeS&#xA;JWP5M6JnwMxrIx5+zTjGJx2M6mUyB3QOpYtQlFKcfOlmW2RUzqRrp0umrU4+bZPpnCCIuxtpHOFf&#xA;D5WquxvA9onJr6oQEPrNjSfUMeYmJidIKZpxVxlXTIFR6d7Mdvf0B3Tk4qTT0kyjytPW9R+7g9fP&#xA;GTxLnRZu2OasjXr99blFeljgJa1xDTFn95uXVqKHUz3eVxpJcxCR2T7XOWa49xY64gIgudtcekvo&#xA;I8+lOrdvn3k6/wA+xgL7SqyTVWqd5awa2ldRr03iB6QQyBsQ4INZMQ1oCxclBw6sRtXFZsom6gSd&#xA;ir4hPokt0dP1j5hbhCwrWdu2fVwIZpJUH+IJ1eCsVS+8unGtlX/ZA2P8z7OPOaxlbIAGlS8T113B&#xA;9sLcFiUk5Ez8VF6h1mVSBTOuTw+XILmRxa4DFWAbD1W4KQBYk8dYatQMGytmu5lYSUW1gRq/nXmd&#xA;mt6wB3RZZ9U1UN9UO0n/AJy3ujpiMbwAwSuINhjxA1UtZvLoY6kPnoIifmPzsEi06tp0ztH7S6aw&#xA;0dk5GLOeu61rd6fIqQGw+0pjP9mjqepp6dSyYgTNBBSl17wWrdCmvDHUO1j7CU2uvNomQr3i3T0i&#xA;At0lAfMI6FGnGPVUbeouxpPKtfqvGLu619Z6zGLat3Xn54NWkfKGwdR4wta9lhp2X8wrvUDyGSqJ&#xA;uZzOyJTCI7mBm69oaz2tMBbMDqER6plbbmVzNxa7S7cVX2a3bdRR71jMLprb0wnxEQ2J2+N3F1JM&#xA;taXszOcYyCR1QtSYH00lNedtfUPkmCZpJe988ZFqDcRZO8y+/qkEwLmwMEKtqw0X6fEHJl+vP465&#xA;5TI0MaFy2jH1Dv269MbV+1uitRrlYYuHW7EiXQrL3ObtLpgWk/isYlWRoMytRCrNrGLt1zyFas/w&#xA;ixYpCybKUOnwprFiDPzCn8eWt5Mh7brGHRkRYVoyj011KL+1Nn+H5YjUzkQEiht+tQpYHFbpGDTW&#xA;hSBGCn0rWuAm5Z/6px01QUSMknwvg3X33rEJWTbDm2BrpEFjJGe1KxIAgYmfLTmP8XC6dOp2+JqM&#xA;ZesKOSYztQJcLrOYySYROmEC4CL3THWoX6Q4p48zheK7YbFdKinRrYIgYyxG0Y3rn0KDUxBfrjaT&#xA;TjhB0h6ti5XS595gxDnQwBZAx8ekmNY2pGZj7Tkz9XH+pd/sHGP5fyvMFbE8pW/ous5QK2RfRpUX&#xA;8xL5lKvDQt2oWZWxxwzHbjY29KJZ0dfVx+Drjmc85jE2ufp+lv8AlHmMdVws3rf8l7L3Yj0W8ZYq&#xA;B0F1u293XDcphyW5m04+javnr9bN3cN+EVl+Spy17D4crlzHYlRdtZmOy6VDIbXbJuYwKlnYA+9k&#xA;pYZ8qqv85105SfwiXcscwIsNxKLaeSlW7Syq365qA6mOAQWJXiBRjp5ta68czPxOcJ9a1+ELnuSq&#xA;fMFf+T+mK5STXqWcfRxt/KCvAI7sTPtcvlmGhSuoxljyLl/R9jUc48y421zR9Ks8uXss/JfR7zFk&#xA;xxTcHVdCa9nletdwSlLsSTlosr9oC0mG78lbV45Sx2X54pXFLDP+17fLmZ5C5d5hyId7/QNp8c8V&#xA;sZRMFU5ivZDGV9IeLZcx59J5fQRcHmvMhy1n8v8ASRjObb/Nscv1WXLWJqR7Mr3r+NQrE9GtfKV4&#xA;2xj2pG2uAh0m7qBFXO5TnS6GPwn4TNjA07SIw8Y+lhQfXLEZfuipkv8AoFXWOhYa0qjAvNK9FqOh&#xA;K8zylT5qzXsTlrB8r+yb+JzP0YYO3zMOQxKLVnmW3kOcIRVycXnHO0eX19jW0iDXXIo6/Ms3udsj&#xA;Xda5V5fydehbZgQnPIsdbTBDtpjNqnitd6TxpRbKVbnWXL3fia9xipKVm1rTnaC1LGTYZlPiBAYk&#xA;imfhEcZTmO11Q5ap3WJxFPUgmzGuu9nwlZNVCXWtPee+XWFm1czIgsVoQkNoiMCtSlhHiIiNBABG&#xA;P0REcZHtNxcu4dTrWRux/Y5d1JZWIxlYvz6kGAzccOsNGOiBQB7mZu+c73vsoURz8dBFri8/rk7U&#xA;vv2j93GHs+Osu49A/wCW5O8//EEhf7Nf08YqD8H7Oo7o+4u2Vr/74/1Lv9g4UjmPDcv5o6i2WEKz&#xA;WOx2SKsopgWvUu8l0pWUgMMYECMyMQU+OMUePwvLqJwicjY5eCjj8Wk8cu8JFkywnSUHZDkNS74q&#xA;XSGzunuJPWeKFw8Hy7jSZkG8w1VWKOIS5OadOx+YXIhpGVbp73IpLuj8Qbp4irluT+UMjlcgibrs&#xA;lfwWBtNaySEQCxYs1zsMsughJe6SkwkZiZ1jiziamC5Vq4G9Z/K8bXxmJTiLlyekv8pprQNOxZno&#xA;JD3iyb7lQ/3YxFalSwfKFCaOS9p06lXGYarNPMSoEe0KyFJDt8lKVqT3ahG10gBe/aIxxbqcx8s4&#xA;DLWMJUrl32exOKvglF2d8KQ68lxpDf5MfQEnOvmeMbhsNn8Zy3gsLNhrcZj+XOTc3iD37CQZ0c5i&#xA;8jSodnttEs6qq+7u3y2S0HQ+TRirzDj8vft5y6vN18TYVlblyUsa8MWmqrGhUUKK0Vq9WpCK61L2&#xA;+fXNKvkuUuWcgjGVl0scm9gcVbVj6ao2qqUlvqmFWssYgVoRAKCI0EYjijlbGExD8njA6WNyLsbT&#xA;bfx6tCjp0bZpmxUDQzjYhix0MvHqn8WawhOKvGXxWQxkvHySe+qtrdWI1jd0+pu26xu02z8eH8uW&#xA;sQnKxXtOJdmhcBa5OYCPmeAz0z2RMdVaWBrOsFGmkRn7I08X8ZwmNYetj9W/eiRJoxPxBO1ReJ2g&#xA;cQXGWrUlhWSnHkC1JGBAEwQ9QYGPGkr37vtnWZnzxlakeHLdXeY/eLRYEF/84if2x9/GA5YT6grF&#xA;1r8j+YL9jnRP3EujX3j+mwI/HiIiNIiNIiPhER9nH+pd/sHB5GvjreQr2eWbeG/I+kbE2m2DaBMB&#xA;jVT0tCj1Dr9viZjScKDMRkKsYzl3JY9zLC1CtlhyS6cJkHHJQU+mN4hOv2ccrV8njSKrXwWSq3Yc&#xA;sCiu9zC6UTu3bHedyjiNQLQomJ4wVivhzyGORj/Z5sFVZ7k2FsXFS1Z60rIu3VAQL41NS16L+URk&#xA;cJOGyBMXzTGS71YoOiVTu+pvhkP6sFsnXbKfEfNMFqMZoSw57mWmdgdHCYhoPrCQyhzsqbRyK3mM&#xA;at2CUxPo+EyA80TFOww72DwSETprL7COl3AD58muYLfrxc7GiqlVtcjW6LWLBVWrOSYZaDYkdgdX&#xA;bpJNP4BGslpHHKNQqr67sVgrVN/UKuYMJdNaZYkq73aqkl+JZC58xpE/zbFobagh7JOBlZzMa/Zr&#xA;E8fXU/uz/jxeoHfSI3KlirJdI529dRL3/H4ju3R+zizjj5SyFrJeyioLimB2KmRPu91LIw9cbSri&#xA;oOh7v3mxexgpd1NljPZ+yiOYsxJNsrKOrNFRlv7feHo6h+mXQvVa4BaF+FzJ/XU/uz/jx2xsFs9U&#xA;2bhiRj1bfGk/s/qGJcsGqaBLapgwa2LONpgYFqJCQzMEMxpMeJ4I6OOo0zONpHVqoQZD8dpEsBmR&#xA;1iJ0mdNf+I//xAAhEAEBAQEBAAICAgMAAAAAAAABESEAMRBBIFEwYUBxof/aAAgBAQABPyECb1CS&#xA;tyEkBSPUoaEPJ4IRLv41q1as5p1nbjVM4KtPYJxzYXOjJZnpPdPP5FGxlcY7xWyW1J7GM6OG2hgx&#xA;F/7PI/cYZV5oJnJC+Q8LUr2LJk9u03yPRRiDHCaCxOzaRsuzjWu7ueqKW4JYKJ856isdQVqupGLV&#xA;KVvsR15kUUCdoybFNE24gaIu0j48BBPamFUmIZxEihmCUMUZ4gx8LMiDTvSirwH5NdlTrd+qXTVF&#xA;pRUpoS5emchiITqLM/a98CCrc5KLBF2rRHwqtHr+Gkp36IoSLvS8zn+IFWBwFeRUCsRUUx0S8h6V&#xA;YTYIzJPIwdK5za0xVXMJSUNCvI7laMMbMARVMAlAWjN1F/qiJtPV+hBRUUve3KRTchUwxS1QbBHJ&#xA;ylpgmbb8j/yplBU9peCJcvV3JqACFehEnzI1fqouqJB4epVS+y1GKsW90Y6HO/OAzgqFB10ELMSj&#xA;kMLn4fKwgo4w0CSQUTP6RJp93wkQtnzvsf8A9JKzj/iD0p4YPycX3VivVu8lcTYSlHrLQW3yLQsX&#xA;gFsksBgmPyCHMzt3ov7tUMSGGJcvXrAWfh3JPCXkJUXok/Vi63aguCoSZxXfgfHTd6McvLFyjGVh&#xA;bcrHLwZXCXs3GhC2IFLh33TTQlUmRGQV3g6jjw4bMgA30HDzBei4NEC0YB9ucDyNsf2PPFhjm5DS&#xA;ypb/AGxnonJ6IBAmxAi/6o+GW04OjVQBUspABdb1VCQSlzzc+QOdnBGRgaLODTOnAO9uTAcNjCn5&#xA;IjKevHFd46DYs+iTY+T7A35PuMhlBa8PJ3UEiVbpCPVEwfXO33Z/ovA2aGkwfUDkEDgwHGSGVkYQ&#xA;HFeHgdaF8dX09oAuXnwUW2wk/F/e2RYMoaTymOkAIXzourARF9nKVemfYYbX7lH/AIjt1VGsD/b2&#xA;agkNcCYYCAQAYAYH0fDONU/IK+MqzPPSE+n80SDZKeZ3mpsK8YAEuUhHFkEs8r8C3Zu4sDdA0bQV&#xA;5KcQRfO/peM78Lnci/TQe3NuGGjrxXGNulMIlOidPyLdoeGpT1J+AKK7DGoFz0+P9oEh2THtQH7H&#xA;CbRBjimov2NOV5oEzcfcOVEfj3/6jzuCClaff+BODgIIYnElII9v0LkB6PwSAy/5H//aAAwDAQAC&#xA;AAMAAAAQfAFVkIAAAAAE2gUkwEEg/XDrAUAMwgDzzgAAAAAA/8QAGhEBAQADAQEAAAAAAAAAAAAA&#xA;AREAITEgYf/aAAgBAwEBPxAbnseEhUjQEJdJThpZ5QodKIOgUPurpnUs6DBa7iHDwSC6TlX0NzR6&#xA;ZDInFFFJA+aJgh2NJyip1qwUgI0DAooYHpTyswbZadVUdlAVb2pn8rwf68IhAwMAVqK8XFHVRV/i&#xA;Tx0wPydq8NOqH1E9XrGuWWFg/fcqiCuiGCmBAlimC2ZNCHrKgUlMR2qCN6n1A7bKFzMfLhBAHoux&#xA;h7//xAAbEQEBAAMBAQEAAAAAAAAAAAABEQAgITFhgf/aAAgBAgEBPxAMnqIeyyHIewmJrHwgmxpo&#xA;OGO99xPLpeNRuc+EptwhuuTQncxDPmPoIz1iNIQ5T4VTkxKXd3lkD1MMYXPlrkZjcdDZbBrdpRBk&#xA;dkERCI1I9hbH4PHJM72jkL7mgKFFRsFSSWCGSKIcFeXhQFD8Nvs27Z6mikHQD4ORiNEBIggRKY9S&#xA;vyalTijOwkzEr05z5x6IApt//8QAIRABAAMAAgEEAwAAAAAAAAAAAQARIRAxMCBBUYFAYfD/2gAI&#xA;AQEAAT8QLTmtc8E78uCo1QK3oVCqLBBsQ5RIkSIqAarSHyw6d8Qsa7EkhtdvnCeVnOTCEYRAIKBE&#xA;RESxExE0TE8js7KkNdMjsrcBiqXQbA8KlUk2/wBfb2OTLdyz6fuTJxCkMD54AiI9JpkqCsOq2P4q&#xA;PnPTFEp7gOhiWPpVBYzg2ZHI+jAIgDoNGbgxzrypZsHgbnn9Luj+kdNo7xHm+M5RFA2lBqCtV+6J&#xA;UTH/AJp84vayK+BHsU+p/FgVng4LbU6tMxGcChb2K/GEdslglAroJa81oIIgeHnAun2majAlzDFR&#xA;DEThkQBkx++tpT9hk8ErykLD2cs0T5MRRLsZCNaqx+WKK13JgtOzIQWxsX7dg+Lke9BG0OJ2dWlC&#xA;ms5aDNGCppiH2+GEEhZU2KTehCIgcC1TbxBbD5xDgq5Dq1aiweQBw/QMKxLlX4x56vTZw0xNkgNF&#xA;X1zCCyT40pWBko7RGXqXVuXFKQk0hujO8k4hDfOR0kcOsXRAsn8y/d5oyNC7N8JoRPl1P2V78RKU&#xA;iZWAnRMMnxknmRyYoD32PzRYfutKKjXD/muPAkmOqeFoWquRB2xLvULDiyn1tLzMXWpT9IN/LmY2&#xA;hA243Sh5RlaZrpLyS8TKg99YZCagZxGLCm1QCuSfI01HEhIgCQw6IeIIFmZrb3d2dOPgtAEQPhwT&#xA;OhIyoAHcu0NjQxw9MNiCmdEWHRKdnR9P8/N8RhM1go9sZc0SO4d12RaWMJ/7zMOWw9YAq2tRKeuR&#xA;s11kbIF/K5Skvikz3FhaAP8AMDSzNYov3y6qECpEW7bgOylbpTjxkKs2lTqXX8x5ZMK4PsSWEMT9&#xA;cUXfkwIBxoLeraqtbg2lJYO58Dah3FAuKgW8LJLTAp3YaUPg/Pw41kID4anpaAzVQbwgoVZeiSZF&#xA;4NCDmpWmwx4QGwGAAAAAToi7tTnSzhdGxSTwyvgB7dCg9loZBFKUqRpAWgOU7P1GEXXoy+/Ysv6R&#xA;gwYMgYg2rBYG1tC0/wC3UAW4mDLVE70Z+LrAOO0eOEWwG8a824WR9ICexZwgk2kA3rh8RX1ATFna&#xA;oXuI+EdfBw3lvRQTTdSCjl+N3m9p9+9Tl3gHV2pEox8CJq5qsEQluUP0a8t3pIEVh/I//9k=&#xA;&quot;)" />
            </fo:inline>
          </fo:block>
        </fo:static-content>
        <fo:static-content flow-name="odd-page-footer">
          <fo:retrieve-marker retrieve-class-name="odd-page-footer" retrieve-position="first-including-carryover" retrieve-boundary="page" />
        </fo:static-content>
        <fo:static-content flow-name="even-page-header">
          <fo:block font-family="Times" font-size="18pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
            <fo:inline>
              <fo:external-graphic content-width="186.1pt" content-height="51.45pt" src="https://www.ahe.lodz.pl/sites/default/files/logo_ckp.jpg" />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:leader leader-pattern="space"  />
            </fo:inline>
            <fo:inline>
              <fo:external-graphic content-width="116.85pt" content-height="44.15pt" src="url(&quot;data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEB&#xA;AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEB&#xA;AQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wgARCAA7AJwDASIA&#xA;AhEBAxEB/8QAHgAAAQUBAQEBAQAAAAAAAAAAAAUGBwgJBAMKAQL/xAAaAQEAAwEBAQAAAAAAAAAA&#xA;AAAABQYHCAQD/9oADAMBAAIQAxAAAAHdiIK4xNR7roAZrL/N2p6FFeagw3ltFHlcqx9rw/07/sWt&#xA;714zOZW9QLAFXpTJOAAAACFsPforodnWqVQVK8z9z/v0eO+CXnZvtFeuUBX7m8zSVCtNJuhOc9V1&#xA;jJoNaXViW/TZ4x49jWNe+euRza9eyL10ObGnXDKHNNTspAK73cTbNDH9e3Fr83aS4tXLR1XGe7ha&#xA;DI7vxGUGikOEevLECaS9H3QonJK0M20Gg7wCEptKpJV5RbQFIncI7p6El/8AbXmWnYV2BANFgQAA&#xA;A8PcEBfAAD//xAAlEAACAgICAQQCAwAAAAAAAAAFBgMEAgcAARYIECA1EhQmMED/2gAIAQEAAQUC&#xA;JGRAaPrZK5NY80rc80rc80rc80rcat/iBOCj6ju7xr+zfeKwNr5JmvzUUi84r3AewKV6zzZTRPBy&#xA;gujUkQvAbbs9kjZCsYhcZL2FtjtXe6rrLcIDm3usUXredqz8n5JoP65aFuOsSYXaoyzGwdJDhWFv&#xA;lpfHKAOEXTNEybwf0+gjFYaUAXCLJ4gMwr2A6yJ5Mp17U8ikMlhHiq42T2vlBgrHkZQZKQ9ncoOE&#xA;TVVkk8EItXq9GuBCUGBr2zcJ9EFFcGAhqj9SbebILeiq7bCchdFyZi/VXcLBGPPbkfYAEzS9zB2+&#xA;xvBXwC7YtSWHE4dclBl2K++frNGxW9SnJpYq8UMnW02XDCKvEUK4sEOpIesoNvQR90A/5dCFH6k0&#xA;BVT+VcKoD8KdFNyqlAOtbzHXXEmoIqryEN7KjktjLuCGLbg6RrJOTlS8lJpOKZfA2L/GAV0dBLYh&#xA;rQ4Jghhgya60dRP1NPF+m7fyJu66666UfqWGsQiOC6JXOYetz2Mp1+MawZDy8dBiFnL05MQWn6tr&#xA;8sd4Ld7sH/gQVJ7l3wuxwjr6YiPFj3NNY0bVJalh4XY4GHZi6fzlijmjqChlDL/P/8QAKxEAAgIB&#xA;AgUDBAIDAAAAAAAAAQIDBBEFEgYHEyEiABRBICMxMkJRFTOx/9oACAEDAQE/AeAdJ0fXNUv1dbFj&#xA;28FESxNVyzpM08SK0q9avmPaz9lkLZ29vUvLXh7Y0tJPfwqNz9KzcSeMf3LWkcSAfnyj6qfO/wBU&#xA;+Xug3Z0gjqSZPdmNuzhEH7OfufHx+MnA+fXMDg7h/Q+GDqmi1bAnTVadFbktuWSOz1FnM6xQMxQo&#xA;pVF62B38ULYZvr5S2Xq69qHTrLbNrTPayQEbusjW67BNuGDHco/Kt/wi7BpVCeMo9mnfG0tXpzJZ&#xA;WJzjwYMrYb+LRGcjvjYFPqaGJrK0INtaS6os6g6Da/SRVUwRqC6xmVupI6BiBktl49oPNfVks8Ox&#xA;6fUiWKjW1Cts8fN2jjmVcbvJEXLEDs8jMZJfIhU+rlRLYi1fVmrPFFP/AIcrHJJJFE67rlYSdF5S&#xA;qpL0t43BlYIX2kN+aiRRWqyCVJ7U88MfUXLRVTLIil+oQOtON3iyZijP3Fd3C7J0MXFdhm3pHUzL&#xA;KyjOyGKmgLDv+pyAO4/f49cwm36IzYxu1CFsf1kTHH16Zq97R5ZJqEqxSSx9Jy0ccmU3K+MSKwHk&#xA;o7jv6TjriVGV1uxhkYOp9pV7MpBB/wBXwQPU3MvjCf3Jl1CAtb2idl06gjOqsXCbkrghdx8lUgNh&#xA;QRhQBqPEur6rX9rdsJJD1Fk2rBDGd6AhTujRW/ke2cfX/8QAKBEAAQQBAwMEAwEBAAAAAAAAAwEC&#xA;BAUGERITAAcUFSAhIwgiMVOC/9oACAECAQE/Ae7t2PC8OoL5tvfUy21yWtdYwMeqruGF448gmw4J&#xA;80C8SqNq7xPHIe9hGDG5jHKsvPe7hoUi2xPLqXMqqKzmlrVUsEFzXB/0ssflwh2IBouusiM2bERq&#xA;K5ZO1FXrDe6XeHNb+HQ1t3CG8+4sqWWlq1BAghRHSpp9IqfoJqojW7m8pniCjmuIi9do7iszAt1B&#xA;M+RcUdNDlMfmchkaGlraRk3zHVIIIY4nwK34Z5G1I5yO4mLM4ySV93dPObHGqGjp24/Hy6tvshbX&#xA;nx2QLkdKceEdUbE+mQ1SvcNn1mjnE7TcjRv+zrIqrtThd5CsKy8ybE8xGoykoMWPGyFtVKLt3QTq&#xA;ZnA57nO4TwH2ZBka7j8bx3N3W1DVzcmj4Hjbh43Z51FFkWfTBsHGmsqYwWItHWRgmlBhltjNl2Uy&#xA;EKSYQ0KrnmlwdBLgPc8FxnIcFxGH6RhdFQ2MYY1Yo5VmSIaINpTI77Axmu3vaF6+RJK90ueqme0I&#xA;Pd3gkGhYGeXSzqyFlbrSPDqj2FjX1R4kGREl+rSamXZnjRm2L4qLG3sMOSCISS+O5HuTrH4tTVXt&#xA;NHdYx7nJbO3rofkxCKepx7zpoQmm+d/La5Y0jljFiK+uryKkwUybMaJIp0kR/wAk7e0kPJHg46CR&#xA;dWMhE0bFooOIiZu0+GoEjCgjNRNE3HRP7r1+PZ/K7oS5O3Z5NRdH2a67OaVEJt/53ae/LMLx7Noc&#xA;aBkUQkuNEk+WBg5UmKrT8Tw7lfGIJzk4yPTa5Vb866aonUfsR21iyASo9PLGeMYcgBEuLXUZgvQg&#xA;3prLVNWvajk1RU+Or/DMNvx2aEx9IZ70EONdy4tnaNk2EeCRTBiIRZW6LC5lR5IoFRpUGAJHPAAQ&#xA;24v2qwrDrP1egrTxZ/jFicpLCdJbwGVjiN45ByD1VRs/bbuTT4X5X3//xABCEAACAgECBQEEBAkK&#xA;BwAAAAACAwEEBRESAAYTFCEiFSMxMgc0QVEIECRCYWJxk9EWICUwM0NSc4SxQERTcoGho//aAAgB&#xA;AQAGPwIHZfKY7FqZMitmRu1qQMKI1kQKyxcGUR5mB1mI4ZXoXaOR2FtE6WTpPhn6wQthzMTx9Sf+&#xA;8D+HH1J/7wP4cfUn/vA/hx9Sf+8D+HB1sHQjLZOD2F1HyGPr7SiD6jlrmbBfGBCtMhr87h02lSw3&#xA;NOGq0hyFhNZOTxj2dCsyywVJ7urbky6G4tXWQtaqHz25xrMf1mD5gzeOdlLxv9hUFsY/2dSr2WDc&#xA;yGROul9XuL6qqGLopdY7VrWiVmu9aPRFvABPYsge2uUrTlt02Dr1Ulqhb9fLVFVDYesdMR0Hjrcu&#xA;ZmctVDz7Kymkltj8xREUKn9PSOjM/Zu+32Zma54PLCXSJNrUUMb/AIBYyAJJl+at8RE6iINaUx+I&#xA;OWsWZ93bEe+JGsthTvCaS9nq6tnXcyI9XRkAjWHzxPMHMak3cs0duOxbIE1JeY7hExnUWOD5rDdJ&#xA;CuMTCt7ZAixOJAfOTvg68xahBaKYGdrIOgRjpgK6wNlYaCEnsVHzRwnDY3GIutZjSyG513s4gAeS&#xA;JWP5M6JnwMxrIx5+zTjGJx2M6mUyB3QOpYtQlFKcfOlmW2RUzqRrp0umrU4+bZPpnCCIuxtpHOFf&#xA;D5WquxvA9onJr6oQEPrNjSfUMeYmJidIKZpxVxlXTIFR6d7Mdvf0B3Tk4qTT0kyjytPW9R+7g9fP&#xA;GTxLnRZu2OasjXr99blFeljgJa1xDTFn95uXVqKHUz3eVxpJcxCR2T7XOWa49xY64gIgudtcekvo&#xA;I8+lOrdvn3k6/wA+xgL7SqyTVWqd5awa2ldRr03iB6QQyBsQ4INZMQ1oCxclBw6sRtXFZsom6gSd&#xA;ir4hPokt0dP1j5hbhCwrWdu2fVwIZpJUH+IJ1eCsVS+8unGtlX/ZA2P8z7OPOaxlbIAGlS8T113B&#xA;9sLcFiUk5Ez8VF6h1mVSBTOuTw+XILmRxa4DFWAbD1W4KQBYk8dYatQMGytmu5lYSUW1gRq/nXmd&#xA;mt6wB3RZZ9U1UN9UO0n/AJy3ujpiMbwAwSuINhjxA1UtZvLoY6kPnoIifmPzsEi06tp0ztH7S6aw&#xA;0dk5GLOeu61rd6fIqQGw+0pjP9mjqepp6dSyYgTNBBSl17wWrdCmvDHUO1j7CU2uvNomQr3i3T0i&#xA;At0lAfMI6FGnGPVUbeouxpPKtfqvGLu619Z6zGLat3Xn54NWkfKGwdR4wta9lhp2X8wrvUDyGSqJ&#xA;uZzOyJTCI7mBm69oaz2tMBbMDqER6plbbmVzNxa7S7cVX2a3bdRR71jMLprb0wnxEQ2J2+N3F1JM&#xA;taXszOcYyCR1QtSYH00lNedtfUPkmCZpJe988ZFqDcRZO8y+/qkEwLmwMEKtqw0X6fEHJl+vP465&#xA;5TI0MaFy2jH1Dv269MbV+1uitRrlYYuHW7EiXQrL3ObtLpgWk/isYlWRoMytRCrNrGLt1zyFas/w&#xA;ixYpCybKUOnwprFiDPzCn8eWt5Mh7brGHRkRYVoyj011KL+1Nn+H5YjUzkQEiht+tQpYHFbpGDTW&#xA;hSBGCn0rWuAm5Z/6px01QUSMknwvg3X33rEJWTbDm2BrpEFjJGe1KxIAgYmfLTmP8XC6dOp2+JqM&#xA;ZesKOSYztQJcLrOYySYROmEC4CL3THWoX6Q4p48zheK7YbFdKinRrYIgYyxG0Y3rn0KDUxBfrjaT&#xA;TjhB0h6ti5XS595gxDnQwBZAx8ekmNY2pGZj7Tkz9XH+pd/sHGP5fyvMFbE8pW/ous5QK2RfRpUX&#xA;8xL5lKvDQt2oWZWxxwzHbjY29KJZ0dfVx+Drjmc85jE2ufp+lv8AlHmMdVws3rf8l7L3Yj0W8ZYq&#xA;B0F1u293XDcphyW5m04+javnr9bN3cN+EVl+Spy17D4crlzHYlRdtZmOy6VDIbXbJuYwKlnYA+9k&#xA;pYZ8qqv85105SfwiXcscwIsNxKLaeSlW7Syq365qA6mOAQWJXiBRjp5ta68czPxOcJ9a1+ELnuSq&#xA;fMFf+T+mK5STXqWcfRxt/KCvAI7sTPtcvlmGhSuoxljyLl/R9jUc48y421zR9Ks8uXss/JfR7zFk&#xA;xxTcHVdCa9nletdwSlLsSTlosr9oC0mG78lbV45Sx2X54pXFLDP+17fLmZ5C5d5hyId7/QNp8c8V&#xA;sZRMFU5ivZDGV9IeLZcx59J5fQRcHmvMhy1n8v8ASRjObb/Nscv1WXLWJqR7Mr3r+NQrE9GtfKV4&#xA;2xj2pG2uAh0m7qBFXO5TnS6GPwn4TNjA07SIw8Y+lhQfXLEZfuipkv8AoFXWOhYa0qjAvNK9FqOh&#xA;K8zylT5qzXsTlrB8r+yb+JzP0YYO3zMOQxKLVnmW3kOcIRVycXnHO0eX19jW0iDXXIo6/Ms3udsj&#xA;Xda5V5fydehbZgQnPIsdbTBDtpjNqnitd6TxpRbKVbnWXL3fia9xipKVm1rTnaC1LGTYZlPiBAYk&#xA;imfhEcZTmO11Q5ap3WJxFPUgmzGuu9nwlZNVCXWtPee+XWFm1czIgsVoQkNoiMCtSlhHiIiNBABG&#xA;P0REcZHtNxcu4dTrWRux/Y5d1JZWIxlYvz6kGAzccOsNGOiBQB7mZu+c73vsoURz8dBFri8/rk7U&#xA;vv2j93GHs+Osu49A/wCW5O8//EEhf7Nf08YqD8H7Oo7o+4u2Vr/74/1Lv9g4UjmPDcv5o6i2WEKz&#xA;WOx2SKsopgWvUu8l0pWUgMMYECMyMQU+OMUePwvLqJwicjY5eCjj8Wk8cu8JFkywnSUHZDkNS74q&#xA;XSGzunuJPWeKFw8Hy7jSZkG8w1VWKOIS5OadOx+YXIhpGVbp73IpLuj8Qbp4irluT+UMjlcgibrs&#xA;lfwWBtNaySEQCxYs1zsMsughJe6SkwkZiZ1jiziamC5Vq4G9Z/K8bXxmJTiLlyekv8pprQNOxZno&#xA;JD3iyb7lQ/3YxFalSwfKFCaOS9p06lXGYarNPMSoEe0KyFJDt8lKVqT3ahG10gBe/aIxxbqcx8s4&#xA;DLWMJUrl32exOKvglF2d8KQ68lxpDf5MfQEnOvmeMbhsNn8Zy3gsLNhrcZj+XOTc3iD37CQZ0c5i&#xA;8jSodnttEs6qq+7u3y2S0HQ+TRirzDj8vft5y6vN18TYVlblyUsa8MWmqrGhUUKK0Vq9WpCK61L2&#xA;+fXNKvkuUuWcgjGVl0scm9gcVbVj6ao2qqUlvqmFWssYgVoRAKCI0EYjijlbGExD8njA6WNyLsbT&#xA;bfx6tCjp0bZpmxUDQzjYhix0MvHqn8WawhOKvGXxWQxkvHySe+qtrdWI1jd0+pu26xu02z8eH8uW&#xA;sQnKxXtOJdmhcBa5OYCPmeAz0z2RMdVaWBrOsFGmkRn7I08X8ZwmNYetj9W/eiRJoxPxBO1ReJ2g&#xA;cQXGWrUlhWSnHkC1JGBAEwQ9QYGPGkr37vtnWZnzxlakeHLdXeY/eLRYEF/84if2x9/GA5YT6grF&#xA;1r8j+YL9jnRP3EujX3j+mwI/HiIiNIiNIiPhER9nH+pd/sHB5GvjreQr2eWbeG/I+kbE2m2DaBMB&#xA;jVT0tCj1Dr9viZjScKDMRkKsYzl3JY9zLC1CtlhyS6cJkHHJQU+mN4hOv2ccrV8njSKrXwWSq3Yc&#xA;sCiu9zC6UTu3bHedyjiNQLQomJ4wVivhzyGORj/Z5sFVZ7k2FsXFS1Z60rIu3VAQL41NS16L+URk&#xA;cJOGyBMXzTGS71YoOiVTu+pvhkP6sFsnXbKfEfNMFqMZoSw57mWmdgdHCYhoPrCQyhzsqbRyK3mM&#xA;at2CUxPo+EyA80TFOww72DwSETprL7COl3AD58muYLfrxc7GiqlVtcjW6LWLBVWrOSYZaDYkdgdX&#xA;bpJNP4BGslpHHKNQqr67sVgrVN/UKuYMJdNaZYkq73aqkl+JZC58xpE/zbFobagh7JOBlZzMa/Zr&#xA;E8fXU/uz/jxeoHfSI3KlirJdI529dRL3/H4ju3R+zizjj5SyFrJeyioLimB2KmRPu91LIw9cbSri&#xA;oOh7v3mxexgpd1NljPZ+yiOYsxJNsrKOrNFRlv7feHo6h+mXQvVa4BaF+FzJ/XU/uz/jx2xsFs9U&#xA;2bhiRj1bfGk/s/qGJcsGqaBLapgwa2LONpgYFqJCQzMEMxpMeJ4I6OOo0zONpHVqoQZD8dpEsBmR&#xA;1iJ0mdNf+I//xAAhEAEBAQEBAAICAgMAAAAAAAABESEAMRBBIFEwYUBxof/aAAgBAQABPyECb1CS&#xA;tyEkBSPUoaEPJ4IRLv41q1as5p1nbjVM4KtPYJxzYXOjJZnpPdPP5FGxlcY7xWyW1J7GM6OG2hgx&#xA;F/7PI/cYZV5oJnJC+Q8LUr2LJk9u03yPRRiDHCaCxOzaRsuzjWu7ueqKW4JYKJ856isdQVqupGLV&#xA;KVvsR15kUUCdoybFNE24gaIu0j48BBPamFUmIZxEihmCUMUZ4gx8LMiDTvSirwH5NdlTrd+qXTVF&#xA;pRUpoS5emchiITqLM/a98CCrc5KLBF2rRHwqtHr+Gkp36IoSLvS8zn+IFWBwFeRUCsRUUx0S8h6V&#xA;YTYIzJPIwdK5za0xVXMJSUNCvI7laMMbMARVMAlAWjN1F/qiJtPV+hBRUUve3KRTchUwxS1QbBHJ&#xA;ylpgmbb8j/yplBU9peCJcvV3JqACFehEnzI1fqouqJB4epVS+y1GKsW90Y6HO/OAzgqFB10ELMSj&#xA;kMLn4fKwgo4w0CSQUTP6RJp93wkQtnzvsf8A9JKzj/iD0p4YPycX3VivVu8lcTYSlHrLQW3yLQsX&#xA;gFsksBgmPyCHMzt3ov7tUMSGGJcvXrAWfh3JPCXkJUXok/Vi63aguCoSZxXfgfHTd6McvLFyjGVh&#xA;bcrHLwZXCXs3GhC2IFLh33TTQlUmRGQV3g6jjw4bMgA30HDzBei4NEC0YB9ucDyNsf2PPFhjm5DS&#xA;ypb/AGxnonJ6IBAmxAi/6o+GW04OjVQBUspABdb1VCQSlzzc+QOdnBGRgaLODTOnAO9uTAcNjCn5&#xA;IjKevHFd46DYs+iTY+T7A35PuMhlBa8PJ3UEiVbpCPVEwfXO33Z/ovA2aGkwfUDkEDgwHGSGVkYQ&#xA;HFeHgdaF8dX09oAuXnwUW2wk/F/e2RYMoaTymOkAIXzourARF9nKVemfYYbX7lH/AIjt1VGsD/b2&#xA;agkNcCYYCAQAYAYH0fDONU/IK+MqzPPSE+n80SDZKeZ3mpsK8YAEuUhHFkEs8r8C3Zu4sDdA0bQV&#xA;5KcQRfO/peM78Lnci/TQe3NuGGjrxXGNulMIlOidPyLdoeGpT1J+AKK7DGoFz0+P9oEh2THtQH7H&#xA;CbRBjimov2NOV5oEzcfcOVEfj3/6jzuCClaff+BODgIIYnElII9v0LkB6PwSAy/5H//aAAwDAQAC&#xA;AAMAAAAQfAFVkIAAAAAE2gUkwEEg/XDrAUAMwgDzzgAAAAAA/8QAGhEBAQADAQEAAAAAAAAAAAAA&#xA;AREAITEgYf/aAAgBAwEBPxAbnseEhUjQEJdJThpZ5QodKIOgUPurpnUs6DBa7iHDwSC6TlX0NzR6&#xA;ZDInFFFJA+aJgh2NJyip1qwUgI0DAooYHpTyswbZadVUdlAVb2pn8rwf68IhAwMAVqK8XFHVRV/i&#xA;Tx0wPydq8NOqH1E9XrGuWWFg/fcqiCuiGCmBAlimC2ZNCHrKgUlMR2qCN6n1A7bKFzMfLhBAHoux&#xA;h7//xAAbEQEBAAMBAQEAAAAAAAAAAAABEQAgITFhgf/aAAgBAgEBPxAMnqIeyyHIewmJrHwgmxpo&#xA;OGO99xPLpeNRuc+EptwhuuTQncxDPmPoIz1iNIQ5T4VTkxKXd3lkD1MMYXPlrkZjcdDZbBrdpRBk&#xA;dkERCI1I9hbH4PHJM72jkL7mgKFFRsFSSWCGSKIcFeXhQFD8Nvs27Z6mikHQD4ORiNEBIggRKY9S&#xA;vyalTijOwkzEr05z5x6IApt//8QAIRABAAMAAgEEAwAAAAAAAAAAAQARIRAxMCBBUYFAYfD/2gAI&#xA;AQEAAT8QLTmtc8E78uCo1QK3oVCqLBBsQ5RIkSIqAarSHyw6d8Qsa7EkhtdvnCeVnOTCEYRAIKBE&#xA;RESxExE0TE8js7KkNdMjsrcBiqXQbA8KlUk2/wBfb2OTLdyz6fuTJxCkMD54AiI9JpkqCsOq2P4q&#xA;PnPTFEp7gOhiWPpVBYzg2ZHI+jAIgDoNGbgxzrypZsHgbnn9Luj+kdNo7xHm+M5RFA2lBqCtV+6J&#xA;UTH/AJp84vayK+BHsU+p/FgVng4LbU6tMxGcChb2K/GEdslglAroJa81oIIgeHnAun2majAlzDFR&#xA;DEThkQBkx++tpT9hk8ErykLD2cs0T5MRRLsZCNaqx+WKK13JgtOzIQWxsX7dg+Lke9BG0OJ2dWlC&#xA;ms5aDNGCppiH2+GEEhZU2KTehCIgcC1TbxBbD5xDgq5Dq1aiweQBw/QMKxLlX4x56vTZw0xNkgNF&#xA;X1zCCyT40pWBko7RGXqXVuXFKQk0hujO8k4hDfOR0kcOsXRAsn8y/d5oyNC7N8JoRPl1P2V78RKU&#xA;iZWAnRMMnxknmRyYoD32PzRYfutKKjXD/muPAkmOqeFoWquRB2xLvULDiyn1tLzMXWpT9IN/LmY2&#xA;hA243Sh5RlaZrpLyS8TKg99YZCagZxGLCm1QCuSfI01HEhIgCQw6IeIIFmZrb3d2dOPgtAEQPhwT&#xA;OhIyoAHcu0NjQxw9MNiCmdEWHRKdnR9P8/N8RhM1go9sZc0SO4d12RaWMJ/7zMOWw9YAq2tRKeuR&#xA;s11kbIF/K5Skvikz3FhaAP8AMDSzNYov3y6qECpEW7bgOylbpTjxkKs2lTqXX8x5ZMK4PsSWEMT9&#xA;cUXfkwIBxoLeraqtbg2lJYO58Dah3FAuKgW8LJLTAp3YaUPg/Pw41kID4anpaAzVQbwgoVZeiSZF&#xA;4NCDmpWmwx4QGwGAAAAAToi7tTnSzhdGxSTwyvgB7dCg9loZBFKUqRpAWgOU7P1GEXXoy+/Ysv6R&#xA;gwYMgYg2rBYG1tC0/wC3UAW4mDLVE70Z+LrAOO0eOEWwG8a824WR9ICexZwgk2kA3rh8RX1ATFna&#xA;oXuI+EdfBw3lvRQTTdSCjl+N3m9p9+9Tl3gHV2pEox8CJq5qsEQluUP0a8t3pIEVh/I//9k=&#xA;&quot;)" />
            </fo:inline>
          </fo:block>
        </fo:static-content>
        <fo:static-content flow-name="even-page-footer">
          <fo:retrieve-marker retrieve-class-name="odd-page-footer" retrieve-position="first-including-carryover" retrieve-boundary="page" />
        </fo:static-content>
        <fo:static-content flow-name="xsl-footnote-separator">
          <fo:block>
            <fo:leader leader-pattern="rule" leader-length="144pt" rule-thickness="0.5pt" rule-style="solid" color="gray" />
          </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">
          <fo:block widows="2" orphans="2" font-size="10pt" line-height="1.147" white-space-collapse="false">
            <fo:marker marker-class-name="first-page-header" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:marker marker-class-name="first-page-footer" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:marker marker-class-name="odd-page-header" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:block font-family="Arial" font-size="12pt" language="PL">
                <fo:inline>
                  <fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline>
                  <fo:block />
                </fo:inline>
                <fo:inline>
                  <fo:leader leader-pattern="space"  />
                </fo:inline>
              </fo:block>
            </fo:marker>
            <fo:marker marker-class-name="odd-page-footer" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:block font-family="Arial" font-size="7pt" language="PL" line-height="1.147" text-align="center">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="PL" line-height="1.147" text-align="center">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="PL" line-height="1.147" text-align="center" color="#999999">
                <fo:inline color="#999999" font-size="7pt">
                  <fo:leader leader-length="0pt" />Centrum Kształcenia Podyplomowego Akademii Humanistyczno-Ekonomicznej w Łodzi
                </fo:inline>
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="DE" line-height="1.147" text-align="center" color="#999999">
                <fo:inline color="#999999" font-size="7pt" language="DE">
                  <fo:leader leader-length="0pt" />ul. Sterlinga 26, 90-212 Łódź; Tel: (+48 42) 63 15 039, (+48 42) 29 95 637.
                </fo:inline>
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="EN-US" line-height="1.147" text-align="center" color="#999999">
                <fo:inline color="#999999" font-size="7pt" language="EN-US">
                  <fo:leader leader-length="0pt" />mail: ckp@ahe.lodz.pl,
                </fo:inline>
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="EN-US" line-height="1.147" text-align="center" color="#999999">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="EN-US" line-height="1.147" text-align="center">
                <fo:inline color="#999999" font-size="7pt" language="EN-US">
                  <fo:leader leader-length="0pt" />www.ckp-lodz.pl
                </fo:inline>
              </fo:block>
            </fo:marker>
            <fo:marker marker-class-name="even-page-header" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:marker marker-class-name="even-page-footer" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <!--><fo:block font-family="Times" font-size="18pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>-->
            <fo:block font-family="Tahoma" font-size="10pt" space-before="5pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />UMOWA<fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />dotycząca studiów podyplomowych<fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" /><xsl:value-of select="Kierunek"/><fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />zawarta w dniu <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="BiezacaData" />
              <fo:inline font-family="Arial" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="$dataWygenerowania"/>
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" /> pomiędzy
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />Akademią Humanistyczno - Ekonomiczną w Łodzi <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" /> wpisaną pod numerem 30 do Rejestru Uczelni Niepublicznych prowadzonego przez ministra właściwego do spraw szkolnictwa wyższego - zwaną dalej <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />AHE<fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />  lub<fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" /> Uczelnią<fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />,  reprezentowaną na podstawie stosownego pełnomocnictwa, przez: <fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Verdana" font-size="10pt" font-weight="bold">
                <fo:leader leader-length="0pt" />Dyrektora Centrum Kształcenia Podyplomowego Paulinę Gocała<fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />a
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />Panem/ią <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="Student" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="Student"/>
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" start-indent="72pt" text-indent="-72pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />zwanym/ą dalej Słuchaczem,
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" start-indent="72pt" text-indent="-72pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />zameldowanym/ą <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="AdresSP" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="AdresSP"/>
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block id="DOSeriaNumer" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:block id="DOTSNazwa" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="DOTSNazwa"/>
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" /> nr <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="DOTSSeriaNumer" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="DOTSSeriaNumer"/>,
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="-10pt" />PESEL<fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:inline color="white">-</fo:inline>
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="PESEL"/>.
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 1.
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="12pt" text-align="justify" language="PL" start-indent="18pt" text-indent="-18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />1.  AHE zobowiązuje  się  do  zorganizowania  kształcenia  zgodnie  z  wymaganiami  studiów <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="Kierunek_1" />
              <fo:inline font-family="Tahoma" font-size="10pt" >
                <fo:leader leader-length="0pt" />podyplomowych: <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" /><xsl:value-of select="Kierunek"/> <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />.
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />2.   Kształcenie, o którym mowa w niniejszej umowie, prowadzone jest:
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" start-indent="45pt">
                <fo:list-item-label font-size="10pt" start-indent="38pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>a.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="50pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />w formie z wykorzystaniem metod i technik nauczania na odległość,<fo:leader leader-length="0pt" />
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>         
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" start-indent="38pt">
                <fo:list-item-label font-size="10pt" start-indent="38pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>b.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="50pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />w trakcie zjazdów.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" start-indent="18pt" text-indent="-18pt" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />3.  Słuchacz wyraża zgodę na świadczenie przez Uczelnię usług określonych w ust. 1 drogą elektroniczną oraz na otrzymywanie tą drogą wszelkich informacji i materiałów związanych z realizacją studiów podyplomowych.
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="12pt" text-align="justify" language="PL" start-indent="18pt" text-indent="-18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />4.  Słuchacz oświadcza, iż przed podpisaniem niniejszej umowy zapoznał się z Regulaminem Studiów Podyplomowych wraz z załącznikami (które dostępne są w Centrum Kształcenia Podyplomowego i na stronie internetowej www.ckp-lodz.pl) oraz Regulaminem Rekrutacji Online, jeśli uczestniczył w rekrutacji online (który dostępny jest na stronie internetowej rekrutacji online www.rekrutacja.ahe.lodz.pl) oraz zobowiązuje się przestrzegać ich postanowień. Słuchacz oświadcza, iż zapoznał się z prawem odstąpienia od umowy zawartej na odległość lub poza lokalem Uczelni. <fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="12pt" text-align="justify" language="PL" start-indent="18pt" text-indent="-18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />5.  Słuchacz zobowiązany jest na mocy umowy wnieść określone opłaty.<fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="12pt" text-align="justify" language="PL" start-indent="18pt" text-indent="-18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />6.  Opłatami, o których mowa w ust. 5 są:<fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:list-block>
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" start-indent="38pt" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="38pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>a.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="50pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-size="10pt">
                      <fo:leader leader-length="0pt" />czesne<fo:leader leader-length="0pt" />
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block>
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" start-indent="38pt" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="38pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>b.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="50pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-size="10pt">
                      <fo:leader leader-length="0pt" />inne opłaty określone w cenniku opłat za usługi nieobjęte czesnym stanowiącym załącznik do Umowy.<fo:leader leader-length="0pt" />
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
			  <fo:block font-family="Tahoma" font-size="12pt" text-align="justify" language="PL" start-indent="18pt" text-indent="-18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-family="Tahoma" font-size="10pt">
					  <fo:leader leader-length="0pt" />7.  Opłaty, o których mowa w ust. 6b płatne są w przypadkach wskazanych w Cenniku i w terminach w nim podanych.<fo:leader leader-length="0pt" />
				  </fo:inline>
			  </fo:block>


            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 2.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Studia podyplomowe będące przedmiotem niniejszej umowy trwają <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline id="UmowaLiczbaSem" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" /><xsl:value-of select="UmowaLiczbaSem"/>
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> semestry akademickie. Czas trwania studiów podyplomowych może ulec zmianie w przypadku zmiany przepisów regulujących zasady organizacji i przeprowadzania studiów podyplomowych.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" start-indent="17.85pt" end-indent="-5.4pt" text-indent="-17.85pt" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Semestr zimowy rozpoczyna się w miesiącu październiku, natomiast semestr letni w miesiącu marcu.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" start-indent="17.85pt" end-indent="-5.4pt" text-indent="-17.85pt" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>3.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Zajęcia w semestrze zimowym rozpoczynają się najpóźniej w miesiącu listopadzie, natomiast                         w semestrze letnim najpóźniej w miesiącu kwietniu.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" start-indent="17.85pt" end-indent="-5.4pt" text-indent="-17.85pt" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>4.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Nieuczestniczenie w zajęciach dydaktycznych nie zwalnia Słuchacza z obowiązku uiszczania opłat za studia podyplomowe.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>5.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Słuchacz zobowiązany jest do<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> zapłaty czesnego<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> w wysokości <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" /> <xsl:value-of select="format-number(UmowaCzesne8Miesiec, &quot;##0,00&quot;, &quot;pln&quot;)"/> zł<fo:leader leader-length="0pt" />,<fo:leader leader-length="0pt" />
                  </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> za cały cykl studiów podyplomowych.<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" color="#3366FF" font-size="10pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> Płatność dokonywana jest w <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />8<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> równych miesięcznych ratach, po <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline id="UmowaCzesne" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" /><xsl:value-of select="format-number(UmowaCzesne, &quot;##0,00&quot;, &quot;pln&quot;)"/> zł<fo:leader leader-length="0pt" />
                  </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> każda.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>6.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Zapłaty czesnego należy dokonywać do<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" /> 5-go<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> dnia każdego miesiąca, począwszy od drugiego miesiąca każdego semestru, w którym rozpoczynają się studia tj. dla studiów rozpoczynających się w semestrze zimowym płatność następuje od listopada, a dla studiów rozpoczynających się w semestrze letnim od miesiąca kwietnia. W przypadku niedokonania wpłaty czesnego, oraz innych opłat, o których mowa w § 1 ust. 6b w terminie, wskazanym w ust. 6, Słuchacz zobowiązany jest do zapłaty zaległej kwoty powiększonej o odsetki umowne za opóźnienie. Odsetki umowne za opóźnienie wynoszą w stosunku rocznym dwukrotność odsetek ustawowych za opóźnienie od każdej zaległej kwoty, nie więcej jednak niż czterokrotność wysokości stopy kredytu lombardowego Narodowego Banku Polskiego. <fo:leader leader-length="0pt" />
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>7.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku przyjęcia na studia podyplomowe po upływie terminu płatności za studia w danym miesiącu, Słuchacz zobowiązuje się do wpłaty zaległych rat czesnego w ciągu siedmiu dni licząc od daty podpisania niniejszej umowy.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>8.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Wpłaty  należy  dokonywać  przekazem pocztowym lub przelewem na indywidualny numer konta
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />PKO BP nr rachunku <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline id="NrRORPM3" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" /><xsl:value-of select="NrRORPM3PKO"/>
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" start-indent="18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />Blankiety wpłaty czesnego powinny być wypełnione czytelnie z podaniem nazwiska i imienia, przeznaczenia (opłata za studia podyplomowe, odsetki) wpłaconej kwoty oraz z dopiskiem
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />studia podyplomowe: <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="Kierunek_2" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" /><xsl:value-of select="Kierunek"/><fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>9.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Za datę dokonania wpłaty za studia podyplomowe uznaje się datę wpływu wpłaty na rachunek bankowy Uczelni.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Times" font-size="12pt" language="PL" text-align="left" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline />
              <fo:inline />
              <fo:inline />
              <fo:inline />
              <fo:inline />
              <fo:inline />
              <fo:inline />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 3.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Nie później niż na pięć dni przed dniem, w którym zgodnie z ofertą <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />AHE miały rozpocząć się <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />zajęcia w ramach studiów objętych niniejszą umową <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-size="9pt">
                      <fo:leader leader-length="0pt" />, <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />AHE ma prawo do odstąpienia od umowy w przypadku, gdy organizacja studiów będzie nieopłacalna i zostanie podjęta decyzja <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />o nie prowadzeniu w danym roku akademickim zajęć w ramach studiów podyplomowych objętych niniejszą umową. <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-size="9.5pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Decyzję <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />o nie prowadzeniu w danym roku akademickim zajęć w ramach studiów podyplomowych objętych niniejszą umową <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-size="9pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />oraz o odstąpieniu od umowy podejmuje <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Kierownik <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Centrum Kształcenia Podyplomowego, działając w ramach umocowania udzielonego przez właściwy organ AHE<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Oświadczenie woli o odstąpieniu od umowy przez Uczelnię może być złożone Słuchaczowi na piśmie lub za pośrednictwem poczty elektronicznej na indywidualny adres e-mail Słuchacza podany w procesie rekrutacji.
                    </fo:inline>
                    <fo:inline font-size="9pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 4.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="21.3pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Słuchacz ma prawo do rozwiązania niniejszej umowy, przy czym oświadczenie o rozwiązaniu umowy należy złożyć pod rygorem nieważności na piśmie w Centrum Kształcenia Podyplomowego.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="21.3pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku rozwiązania niniejszej umowy w trybie określonym w ust. 1 AHE stosuje przepisy Kodeksu Cywilnego, w szczególności art. 746 Kodeksu Cywilnego, dotyczące zwrotu wydatków poniesionych przez przyjmującego zlecenie na wykonanie umowy zlecenia i ewentualnych roszczeń odszkodowawczych.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>3.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="21.3pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Niezachowanie formy czynności, o której mowa w ust. 1, będzie skutkowało dalszym naliczaniem opłaty za studia podyplomowe w następnych miesiącach.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>4.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="21.3pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Postanowień ust. 3, odnośnie formy oświadczenia o rozwiązaniu umowy, nie stosuje się, gdy rozwiązanie umowy przez Słuchacza nastąpiło po uiszczeniu całej kwoty czesnego za studia wskazanej w § 2 ust. 5.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>5.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="21.3pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku dyscyplinarnego skreślenia z listy Słuchaczy o zaprzestaniu naliczania opłat decyduje data decyzji dotyczącej skreślenia.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>6.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="21.3pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Uczelnia ma prawo rozwiązać niniejszą umowę w każdym czasie w przypadku naruszenia przez Słuchacza postanowień niniejszej umowy.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 5.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Słuchacz zobowiązany jest do przestrzegania zasad korzystania z zajęć świadczonych drogą elektroniczną z wykorzystaniem Internetu, w tym w szczególności do:
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-indent="-18pt" text-align="justify" start-indent="36pt">
                <fo:list-item-label font-size="10pt" start-indent="38pt" text-indent="0pt" font-family="Tahoma" font-weight="normal" font-style="normal">
                  <fo:block>a.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="50pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />dbania o ochronę prywatności innych osób (słuchaczy, prowadzących zajęcia, administracji), w tym danych przekazywanych bądź składowanych w systemie informatycznym,
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-indent="35.4pt" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="38pt" text-indent="0pt" font-family="Tahoma" font-weight="normal" font-style="normal">
                  <fo:block>b.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="50pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />respektowania praw autorskich — zarówno materiałów edukacyjnych, jak i udostępnionego oprogramowania.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-indent="-18pt" text-align="justify" start-indent="18pt">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18.75pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Za nieprzestrzeganie zasad wymienionych w ust. 1 Słuchacz może zostać skreślony z listy słuchaczy studiów podyplomowych.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-indent="0pt" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block page-break-before="always">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 6.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify" start-indent="18pt" text-indent="-18pt">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18.75pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Wszelkie zmiany dokonane przez Słuchacza w odniesieniu do postanowień umowy nie są wiążące dla Uczelni i pociągają za sobą skutek w postaci niedojścia do skutku umowy w wersji zmodyfikowanej przez Słuchacza.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify" start-indent="18pt" text-indent="-18pt">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18.75pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Wszelkie próby dokonywania przez Słuchacza zmian w treści umowy, bez złożenia odrębnego wyraźnego oświadczenia woli o fakcie dokonania modyfikacji niniejszej umowy, będą uznane za podstęp w rozumieniu przepisów Kodeksu Cywilnego.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify" start-indent="18pt" text-indent="-18pt">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>3.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18.75pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku samowolnego dokonania przez Słuchacza modyfikacji treści umowy, bez złożenia odrębnego wyraźnego oświadczenia woli w tym zakresie i przesłania do siedziby Uczelni podpisanej zmodyfikowanej wersji umowy, przyjmuje się, iż Słuchacz wyraził zgodę na zawarcie niniejszej umowy w treści udostępnionej za pomocą Internetu.
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-pattern="space"  />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-pattern="space"  />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-pattern="space"  />
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 7.
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />W sprawach nieuregulowanych w niniejszej umowie stosuje się przepisy Regulaminu Studiów Podyplomowych, Regulaminu Rekrutacji Online i przepisy Kodeksu Cywilnego.
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            


 	    <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 8.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Słuchacz wyraża zgodę na wystawianie dokumentów finansowych drogą elektroniczną, bez jego podpisu oraz zamieszczenie ich do jego wglądu i pobrania w Wirtualnym Pokoju Studenta.
					</fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Zamieszczenie dokumentów, o których mowa w ust. 1 w Wirtualnym Pokoju Studenta jest równoznaczne z możliwością zapoznania się przez Słuchacza z ich treścią.
					</fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>



	    <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 9.
              </fo:inline>
            </fo:block>
			  <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
					  <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
						  <fo:block>1.</fo:block>
					  </fo:list-item-label>
					  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
						  <fo:block>
							  <fo:inline font-family="Tahoma" font-size="10pt">
								  <fo:leader leader-length="0pt" />Umowa została sporządzona w dwóch jednobrzmiących egzemplarzach, po jednym dla każdej ze stron.
							  </fo:inline>
						  </fo:block>
					  </fo:list-item-body>
				  </fo:list-item>
			  </fo:list-block>
			  <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
					  <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
						  <fo:block>2.</fo:block>
					  </fo:list-item-label>
					  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
						  <fo:block>
							  <fo:inline font-family="Tahoma" font-size="10pt">
								  <fo:leader leader-length="0pt" />Załącznik, o którym mowa w § 1 ust. 6b do umowy stanowi jej nierozerwalną całość.
							  </fo:inline>
						  </fo:block>
					  </fo:list-item-body>
				  </fo:list-item>
			  </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-pattern="space" leader-length="14.25pt" />
                <fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Times" font-size="12pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-pattern="space" leader-length="21pt" />
              </fo:inline>
              <fo:inline>
                <fo:leader leader-length="0pt" />_____________________
              </fo:inline>
              <fo:inline>
                <fo:leader leader-pattern="space" leader-length="15pt" />
              </fo:inline>
              <fo:inline>
                <fo:leader leader-pattern="space" leader-length="35.25pt" /><fo:leader leader-length="0pt" />______________________
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="9pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="9pt">
                <fo:leader leader-length="0pt" />                        AHE                                                             Słuchacz (czytelny podpis)
              </fo:inline>
            </fo:block>

			  <fo:block page-break-before="always"/>

			  <fo:block font-family="Arial" font-size="12pt" language="PL" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:leader />
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />ZAŁĄCZNIK 1 DO UMOWY O WARUNKACH ODPŁATNOŚCI ZA STUDIA Z DNIA
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline color="black" font-size="10pt" />
				  <fo:inline color="black" font-size="10pt">
					  <fo:leader leader-length="0pt" />
					  <xsl:value-of select="$dataWygenerowania"/>
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="12pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />zawartej pomiędzy Akademią Humanistyczno-Ekonomiczną w Łodzi a Słuchaczem
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="12pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-family="Tahoma" font-weight="bold" color="black" font-size="10pt" />
				  <fo:inline font-family="Tahoma" font-weight="bold" color="black" font-size="10pt">
					  <fo:leader leader-length="0pt" />
					  <xsl:value-of select="Student"/>
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" text-align="left" end-indent="-43.85pt" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:leader />
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />CENNIK OPŁAT ZA USŁUGI DYDAKTYCZNE NIEOBJĘTE CZESNYM
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />obowiązujący na studiach podyplomowych od 21 marca 2013 r.
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:leader />
			  </fo:block>
			  <fo:table font-family="Times" start-indent="-28.6pt" border-top-style="solid" border-top-color="black"
						border-top-width="0.5pt" border-left-style="solid" border-left-color="black" border-left-width="0.5pt"
						border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.5pt" border-right-style="solid"
						border-right-color="black" border-right-width="0.5pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
						table-layout="fixed" width="100.0%">
				  <fo:table-column column-number="1" column-width="27.5pt" />
				  <fo:table-column column-number="2" column-width="251.95pt" />
				  <fo:table-column column-number="3" column-width="80.55pt" />
				  <fo:table-column column-number="4" column-width="153pt" />
				  <fo:table-body start-indent="0pt" end-indent="0pt">
					  <fo:table-row>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  L.p.
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  WYSZCZEGÓLNIENIE
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  OPŁATY
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  UWAGI
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>1.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Reaktywacja – wznowienie studiów, wpisanie na semestr/uzupełnianie przedmiotów
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  100 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />1a
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Uzupełnianie przedmiotu w ramach reaktywacji
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  300 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Opłata jednorazowa za przedmiot
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" number-rows-spanned="3" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />1b
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Powtarzanie / wznowienie nauki na semestrze w ramach reaktywacji
								  </fo:inline>
							  </fo:block>
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="18pt" text-indent="0pt" font-family="Symbol">
										  <fo:block font-size="12pt" font-family="Arial">•</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="36pt" text-indent="0pt">
										  <fo:block>
											  <fo:inline font-size="10pt">
												  <fo:leader leader-length="0pt" />Dla studiów trwających 2 semestry opłata za semestr
											  </fo:inline>
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="5pt" padding-bottom="0pt" padding-right="5pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  1 opłaty za cały tok studiów *
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />opłata liczona od ceny studiów dla edycji do której dołącza słuchacz.
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="18pt" text-indent="0pt" font-family="Symbol">
										  <fo:block font-size="12pt" font-family="Arial">•</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="36pt" text-indent="0pt">
										  <fo:block>
											  <fo:inline font-size="10pt">
												  <fo:leader leader-length="0pt" />Dla studiów trwających 3 semestry opłata za każdy semestr pozostały do ukończenia studiów
											  </fo:inline>
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  1/3 opłaty za cały tok studiów *
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />opłata liczona od ceny studiów dla edycji do której dołącza słuchacz.
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="18pt" text-indent="0pt" font-family="Symbol">
										  <fo:block font-size="12pt" font-family="Arial">•</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="36pt" text-indent="0pt">
										  <fo:block>
											  <fo:inline font-size="10pt">
												  <fo:leader leader-length="0pt" />Dla studiów trwających 4 semestry opłata za każdy semestr pozostały do ukończenia studiów
											  </fo:inline>
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="5pt" padding-bottom="0pt" padding-right="5pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  1 opłaty za cały tok studiów *
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />opłata liczona od ceny studiów dla edycji do której dołącza słuchacz.
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />1c
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Reaktywacja – powołanie składu Komisji Egzaminacyjnej na wniosek słuchacza
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  200 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>2.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Zaświadczenie
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  10 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Obowiązuje osoby ze statusem X, R, A, N do 90 dnia po zmianie ze statusu słuchacza -S
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>3.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Zaświadczenie
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  35 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Obowiązuje osoby ze statusem  X, R, A, N od 91 dnia po zmianie ze statusu słuchacza -S
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>4.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Opłata za monit
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  20 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>5.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Opłata za dodatkowy przedmiot
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  300 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>6.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Przedłużanie okresu dyplomowania
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  50 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Opłata za każdy miesiąc przedłużenia
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>7.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Duplikat świadectwa ukończenia studiów podyplomowych
								  </fo:inline>
							  </fo:block>
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  <fo:leader />
							  </fo:block>
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  60 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
				  </fo:table-body>
			  </fo:table>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />* opłata liczona od ceny studiów dla edycji, do której dołącza słuchacz.
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" text-align="justify" end-indent="-43.85pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:leader />
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" text-align="justify" end-indent="-43.85pt" text-indent="35.45pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />Uczelnia
				  </fo:inline>
				  <fo:inline font-size="10pt">
					  <fo:leader leader-pattern="space" leader-length="33pt" />
					  <fo:leader leader-length="0pt" />
				  </fo:inline>
				  <fo:inline font-size="10pt">
					  <fo:leader leader-pattern="space" leader-length="34.5pt" />
				  </fo:inline>
				  <fo:inline font-size="10pt">
					  <fo:leader leader-pattern="space" leader-length="35.25pt" />
					  <fo:leader leader-length="0pt" />
				  </fo:inline>
				  <fo:inline font-size="10pt">
					  <fo:leader leader-pattern="space" leader-length="29.25pt" />
				  </fo:inline>
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="133pt" />Słuchacz
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="12pt" language="PL" line-height="1.7205" text-align="justify" start-indent="-18pt" end-indent="-43.85pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline>
					  <fo:leader leader-length="0pt" />
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Tahoma" font-size="12pt" language="" line-height="1.7205" text-align="justify" start-indent="-18pt" end-indent="-43.85pt" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline>
					  <fo:leader leader-length="0pt" />______________________
					  <fo:leader leader-length="133pt" />_________________________
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="12pt" language="PL" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:leader />
			  </fo:block>
			  
          </fo:block>
          <fo:block id="IDACIYWC" />
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>
</xsl:stylesheet>'
     
 WHERE Opis like 'Umowa podyplomowe PD2 wirtualni'
GO
UPDATE [dbo].[WUWydrukWersja]
   SET 
      [Szablon] = '<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:decimal-format name="pln"
decimal-separator="," grouping-separator="." NaN="" />
  <xsl:param name="dataWygenerowania"/>
  <xsl:template match="/Dane">
    <fo:root font-family="Times" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml">
      <fo:layout-master-set xmlns:rx="http://www.renderx.com/XSL/Extensions" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882">
        <fo:simple-page-master master-name="section1-first-page" page-width="8.268055555555555in" page-height="11.693055555555556in" margin-top="35.4pt" margin-bottom="14.05pt" margin-right="70.85pt" margin-left="70.85pt">
          <fo:region-body margin-top="50pt" margin-bottom="45pt"></fo:region-body>
          <fo:region-before region-name="first-page-header" extent="11in"></fo:region-before>
          <fo:region-after region-name="first-page-footer" extent="11in" display-align="after"></fo:region-after>
        </fo:simple-page-master>
        <fo:simple-page-master master-name="section1-odd-page" page-width="8.268055555555555in" page-height="11.693055555555556in" margin-top="35.4pt" margin-bottom="14.05pt" margin-right="70.85pt" margin-left="70.85pt">
          <fo:region-body margin-top="50pt" margin-bottom="45pt"></fo:region-body>
          <fo:region-before region-name="odd-page-header" extent="11in"></fo:region-before>
          <fo:region-after region-name="odd-page-footer" extent="11in" display-align="after"></fo:region-after>
        </fo:simple-page-master>
        <fo:simple-page-master master-name="section1-even-page" page-width="8.268055555555555in" page-height="11.693055555555556in" margin-top="35.4pt" margin-bottom="14.05pt" margin-right="70.85pt" margin-left="70.85pt">
          <fo:region-body margin-top="50pt" margin-bottom="45pt"></fo:region-body>
          <fo:region-before region-name="even-page-header" extent="11in"></fo:region-before>
          <fo:region-after region-name="even-page-footer" extent="11in" display-align="after"></fo:region-after>
        </fo:simple-page-master>
        <fo:page-sequence-master master-name="section1-page-sequence-master">
          <fo:repeatable-page-master-alternatives>
            <fo:conditional-page-master-reference odd-or-even="odd" master-reference="section1-odd-page" />
            <fo:conditional-page-master-reference odd-or-even="even" master-reference="section1-even-page" />
          </fo:repeatable-page-master-alternatives>
        </fo:page-sequence-master>
      </fo:layout-master-set>
      <fo:page-sequence master-reference="section1-page-sequence-master" format="1" xmlns:rx="http://www.renderx.com/XSL/Extensions" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882">
        <fo:static-content flow-name="first-page-header">
          <fo:retrieve-marker retrieve-class-name="first-page-header" retrieve-position="first-including-carryover" retrieve-boundary="page" />
        </fo:static-content>
        <fo:static-content flow-name="first-page-footer">
          <fo:retrieve-marker retrieve-class-name="first-page-footer" retrieve-position="first-including-carryover" retrieve-boundary="page" />
        </fo:static-content>
        <fo:static-content flow-name="odd-page-header">
          <fo:block font-family="Times" font-size="18pt" language="PL" text-align="left" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
            <fo:inline>
              <fo:external-graphic content-width="186.1pt" content-height="51.45pt" src="https://www.ahe.lodz.pl/sites/default/files/logo_ckp.jpg" />
            </fo:inline>
          </fo:block>
        </fo:static-content>
        <fo:static-content flow-name="odd-page-footer">
          <fo:retrieve-marker retrieve-class-name="odd-page-footer" retrieve-position="first-including-carryover" retrieve-boundary="page" />
        </fo:static-content>
        <fo:static-content flow-name="even-page-header">
          <fo:block font-family="Times" font-size="18pt" language="PL" text-align="left" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
            <fo:inline>
              <fo:external-graphic content-width="186.1pt" content-height="51.45pt" src="https://www.ahe.lodz.pl/sites/default/files/logo_ckp.jpg" />
            </fo:inline>
          </fo:block>
        </fo:static-content>
        <fo:static-content flow-name="even-page-footer">
          <fo:retrieve-marker retrieve-class-name="odd-page-footer" retrieve-position="first-including-carryover" retrieve-boundary="page" />
        </fo:static-content>
        <fo:static-content flow-name="xsl-footnote-separator">
          <fo:block>
            <fo:leader leader-pattern="rule" leader-length="144pt" rule-thickness="0.5pt" rule-style="solid" color="gray" />
          </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">
          <fo:block widows="2" orphans="2" font-size="10pt" line-height="1.147" white-space-collapse="false">
            <fo:marker marker-class-name="first-page-header" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:marker marker-class-name="first-page-footer" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:marker marker-class-name="odd-page-header" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:block font-family="Arial" font-size="12pt" language="PL">
                <fo:inline>
                  <fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline>
                  <fo:block />
                </fo:inline>
                <fo:inline>
                  <fo:leader leader-pattern="space"  />
                </fo:inline>
              </fo:block>
            </fo:marker>
            <fo:marker marker-class-name="odd-page-footer" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:block font-family="Arial" font-size="7pt" language="PL" line-height="1.147" text-align="center">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="PL" line-height="1.147" text-align="center">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="PL" line-height="1.147" text-align="center" color="#999999">
                <fo:inline color="#999999" font-size="7pt">
                  <fo:leader leader-length="0pt" />Centrum Kształcenia Podyplomowego Akademii Humanistyczno-Ekonomicznej w Łodzi
                </fo:inline>
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="DE" line-height="1.147" text-align="center" color="#999999">
                <fo:inline color="#999999" font-size="7pt" language="DE">
                  <fo:leader leader-length="0pt" />ul. Sterlinga 26, 90-212 Łódź; Tel: (+48 42) 63 15 039, (+48 42) 29 95 637.
                </fo:inline>
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="EN-US" line-height="1.147" text-align="center" color="#999999">
                <fo:inline color="#999999" font-size="7pt" language="EN-US">
                  <fo:leader leader-length="0pt" />mail: ckp@ahe.lodz.pl,
                </fo:inline>
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="EN-US" line-height="1.147" text-align="center" color="#999999">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Arial" font-size="7pt" language="EN-US" line-height="1.147" text-align="center">
                <fo:inline color="#999999" font-size="7pt" language="EN-US">
                  <fo:leader leader-length="0pt" />www.ckp-lodz.pl
                </fo:inline>
              </fo:block>
            </fo:marker>
            <fo:marker marker-class-name="even-page-header" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:marker marker-class-name="even-page-footer" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />UMOWA<fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />dotycząca studiów podyplomowych<fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" /><xsl:value-of select="Kierunek"/><fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />zawarta w dniu <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="BiezacaData" />
              <fo:inline font-family="Arial" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="$dataWygenerowania"/>
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" /> pomiędzy <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />Akademią Humanistyczno - Ekonomiczną w Łodzi <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" /> wpisaną pod numerem 30 do Rejestru Uczelni Niepublicznych prowadzonego przez ministra właściwego do spraw szkolnictwa wyższego - zwaną dalej <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />AHE<fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />  lub<fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" /> Uczelnią<fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />, reprezentowaną na podstawie stosownego pełnomocnictwa, przez: <fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Verdana" font-size="10pt" font-weight="bold">
                <fo:leader leader-length="0pt" />Dyrektora Centrum Kształcenia Podyplomowego Paulinę Gocała<fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />a
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />Panem/ią <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="Student" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="Student"/>
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" start-indent="72pt" text-indent="-72pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />zwanym/ą dalej Słuchaczem,
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" start-indent="72pt" text-indent="-72pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />zameldowanym/ą <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="AdresSP" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="AdresSP"/>
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" start-indent="72pt" text-indent="-72pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" start-indent="48pt"  xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />powiat <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="AdresSPPowiat"/>
                <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />                     województwo <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="AdresSPWojewodztwo"/>
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block id="DOSeriaNumer" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:block id="DOTSNazwa" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />legitymującym/ą się dowodem osobistym <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" /><xsl:value-of select="DOTSSeriaNumer"/>,
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="-10pt" />PESEL<fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:inline color="white">-</fo:inline>
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />
                <xsl:value-of select="PESEL"/>.
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 1.
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="12pt" text-align="justify" language="PL" start-indent="18pt" text-indent="-18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />1.  AHE zobowiązuje się do prowadzenia zajęć dydaktycznych i organizowania zaliczeń i egzaminów zgodnie z wymaganiami studiów podyplomowych: <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="Kierunek_1" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" /> <xsl:value-of select="Kierunek"/> <fo:leader leader-length="0pt" />
              </fo:inline>


              <fo:block font-family="Tahoma" font-size="12pt" text-align="justify" language="PL" start-indent="18pt" text-indent="-18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
                <fo:inline font-family="Tahoma" font-size="10pt">
                  <fo:leader leader-length="0pt" />2.  Słuchacz oświadcza, iż przed podpisaniem niniejszej umowy zapoznał się z Regulaminem Studiów Podyplomowych wraz z załącznikami (które dostępne są w Centrum Kształcenia Podyplomowego i na stronie internetowej www.ckp-lodz.pl) oraz Regulaminem Rekrutacji Online, jeśli uczestniczył w rekrutacji online (który dostępny jest na stronie internetowej rekrutacji online www.rekrutacja.ahe.lodz.pl) oraz zobowiązuje się przestrzegać ich postanowień. Słuchacz oświadcza, iż zapoznał się z prawem odstąpienia od umowy zawartej na odległość lub poza lokalem Uczelni. <fo:leader leader-length="0pt" />
                </fo:inline>
              </fo:block>
              <fo:block font-family="Tahoma" font-size="12pt" text-align="justify" language="PL" start-indent="18pt" text-indent="-18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
                <fo:inline font-family="Tahoma" font-size="10pt">
                  <fo:leader leader-length="0pt" />3.  Słuchacz zobowiązany jest na mocy umowy wnieść określone opłaty.<fo:leader leader-length="0pt" />
                </fo:inline>
              </fo:block>
              <fo:block font-family="Tahoma" font-size="12pt" text-align="justify" language="PL" start-indent="18pt" text-indent="-18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
                <fo:inline font-family="Tahoma" font-size="10pt">
                  <fo:leader leader-length="0pt" />4.  Opłatami, o których mowa w ust. 3 są:<fo:leader leader-length="0pt" />
                </fo:inline>
              </fo:block>
              <fo:list-block>
                <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" start-indent="38pt" text-align="justify">
                  <fo:list-item-label font-size="10pt" start-indent="38pt" text-indent="0pt" font-family="Tahoma">
                    <fo:block>a.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="50pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="10pt">
                        <fo:leader leader-length="0pt" />czesne<fo:leader leader-length="0pt" />
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:list-block>
                <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" start-indent="38pt" text-align="justify">
                  <fo:list-item-label font-size="10pt" start-indent="38pt" text-indent="0pt" font-family="Tahoma">
                    <fo:block>b.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="50pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="10pt">
                        <fo:leader leader-length="0pt" />inne opłaty określone w cenniku opłat za usługi nieobjęte czesnym stanowiącym załącznik do Umowy.<fo:leader leader-length="0pt" />
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
				<fo:block font-family="Tahoma" font-size="12pt" text-align="justify" language="PL" start-indent="18pt" text-indent="-18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
					<fo:inline font-family="Tahoma" font-size="10pt">
						<fo:leader leader-length="0pt" />5.  Opłaty, o których mowa w ust. 4b płatne są w przypadkach wskazanych w Cenniku i w terminach w nim podanych.<fo:leader leader-length="0pt" />
					</fo:inline>
				</fo:block>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 2.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Studia podyplomowe będące przedmiotem niniejszej umowy trwają <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline id="UmowaLiczbaSem" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />3<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> semestry akademickie. Czas trwania studiów podyplomowych może ulec zmianie w przypadku zmiany przepisów regulujących zasady organizacji i przeprowadzania studiów podyplomowych.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" start-indent="17.85pt" end-indent="-5.4pt" text-indent="-17.85pt" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Nieuczestniczenie w zajęciach dydaktycznych nie zwalnia Słuchacza z obowiązku uiszczania opłat za studia podyplomowe.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>3.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Słuchacz zobowiązany jest do<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> zapłaty czesnego<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> w wysokości <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline id="UmowaCzesne10Miesiec" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" /> <xsl:value-of select="format-number(UmowaCzesne10Miesiec, &quot;##0,00&quot;, &quot;pln&quot;)"/> zł<fo:leader leader-length="0pt" />,<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> za cały cykl studiów podyplomowych.<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" color="#3366FF" font-size="10pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> Czesne płatne jest w <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt" font-weight="bold">
                      <fo:leader leader-length="0pt" />15<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> równych miesięcznych ratach, po <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline id="UmowaCzesne" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />
					  <xsl:variable name="UMCZ" select="translate(UmowaCzesne, '','', ''.'')" /> 
					  <xsl:value-of select="format-number(number(($UMCZ)*10) div 15, &quot;##0,00&quot;, &quot;pln&quot;)"/>
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> każda.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>4.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Zapłaty czesnego należy dokonywać do<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" /> 5-go<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> dnia każdego miesiąca, począwszy od miesiąca <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt" font-weight="bold">
                      <fo:leader leader-length="0pt" /> listopada<fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" /> dla studiów rozpoczynających się w semestrze zimowym, a dla studiów rozpoczynających się w semestrze letnim od miesiąca <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline font-family="Tahoma" font-size="10pt" font-weight="bold">
                      <fo:leader leader-length="0pt" />kwietnia.<fo:leader leader-length="0pt" />
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>5.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku niedokonania wpłaty czesnego, oraz innych opłat, o których mowa w § 1 ust. 4b w terminie, wskazanym w ust. 4, Słuchacz zobowiązany jest do zapłaty zaległej kwoty powiększonej o odsetki umowne za opóźnienie. Odsetki umowne za opóźnienie wynoszą w stosunku rocznym dwukrotność odsetek ustawowych za opóźnienie od każdej zaległej kwoty, nie więcej jednak niż czterokrotność wysokości stopy kredytu lombardowego Narodowego Banku Polskiego.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>6.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku przyjęcia na studia podyplomowe po upływie terminu płatności czesnego w danym miesiącu, Słuchacz zobowiązuje się do wpłaty zaległych rat czesnego w ciągu siedmiu dni licząc od daty podpisania niniejszej umowy.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>7.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Wpłaty  należy  dokonywać  przekazem pocztowym lub poprzez przelew na indywidualny numer konta:
                    </fo:inline>
                  </fo:block>
                    <fo:block>
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />PKO BP nr rachunku <fo:leader leader-length="0pt" />
                    </fo:inline>
                    <fo:inline id="NrRORPM3" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
                    <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                      <fo:leader leader-length="0pt" />
                      <xsl:value-of select="NrRORPM3PKO"/>
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="justify" start-indent="18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />Blankiety wpłaty czesnego powinny być wypełnione czytelnie z podaniem przeznaczenia (czesne, odsetki) wpłaconej kwoty oraz z dopiskiem <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />studia podyplomowe: <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline id="Kierunek_2" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt" />
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" /><xsl:value-of select="Kierunek"/><fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma" font-weight="normal">
                  <fo:block>8.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Za datę dokonania wpłaty czesnego uznaje się datę wpływu środków pieniężnych na rachunek bankowy Uczelni.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 3.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Nie później niż na pięć dni przed dniem, w którym zgodnie z ofertą AHE miały rozpocząć się na pierwszym semestrze studia podyplomowe objęte niniejszą umową, AHE ma prawo do odstąpienia od umowy w przypadku, gdy organizacja studiów będzie nieopłacalna i zostanie podjęta decyzja o nie prowadzeniu w danym semestrze studiów podyplomowych objętych niniejszą umową. Decyzję o nie prowadzeniu w danym semestrze studiów podyplomowych objętych niniejszą umową oraz o odstąpieniu od umowy podejmuje Kierownik Centrum Kształcenia Podyplomowego działając w ramach umocowania udzielonego przez właściwy organ AHE.<fo:leader leader-length="0pt" />
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>

            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Oświadczenie woli o odstąpieniu od umowy przez Uczelnię może być złożone Słuchaczowi na piśmie lub za pośrednictwem poczty elektronicznej na indywidualny adres e-mail Słuchacza podany w procesie rekrutacji.
                    </fo:inline>
                    <fo:inline font-size="9pt">
                      <fo:leader leader-length="0pt" />
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 4.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Słuchacz ma prawo do rozwiązania niniejszej umowy, przy czym oświadczenie o rozwiązaniu umowy należy złożyć pod rygorem nieważności na piśmie pracownikowi Centrum Kształcenia Podyplomowego.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku rozwiązania niniejszej umowy w trybie określonym w ust. 1, AHE stosuje przepisy Kodeksu Cywilnego, w szczególności art. 746 Kodeksu Cywilnego, dotyczące zwrotu wydatków poniesionych przez przyjmującego zlecenie na wykonanie umowy zlecenia i ewentualnych roszczeń odszkodowawczych.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>3.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Niezachowanie przez Słuchacza formy czynności, o której mowa w ust. 1, będzie skutkowało nieważnością oświadczenia o rozwiązaniu umowy i dalszym naliczaniem opłaty za studia podyplomowe w następnych miesiącach.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>4.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Postanowień ust. 3, odnośnie formy oświadczenia o rozwiązaniu umowy nie stosuje się, gdy rozwiązanie umowy przez Słuchacza nastąpiło po uiszczeniu całej kwoty czesnego za studia wskazanej w § 2 ust. 3.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>5.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku dyscyplinarnego skreślenia z listy Słuchaczy o zaprzestaniu naliczania opłat decyduje data decyzji dotyczącej skreślenia.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-family="Tahoma">
                  <fo:block>6.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Uczelnia ma prawo rozwiązać niniejszą umowę w każdym czasie w przypadku naruszenia przez Słuchacza postanowień niniejszej umowy.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 5.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Wszelkie zmiany dokonane przez Słuchacza w odniesieniu do postanowień umowy nie są wiążące dla Uczelni i pociągają za sobą skutek w postaci niedojścia do skutku umowy w wersji zmodyfikowanej przez Słuchacza.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Wszelkie próby dokonywania przez Słuchacza zmian w treści umowy, bez złożenia odrębnego wyraźnego oświadczenia woli o fakcie dokonania modyfikacji niniejszej umowy, będą uznane za podstęp w rozumieniu przepisów Kodeksu Cywilnego.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
                  <fo:block>3.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />W przypadku samowolnego dokonania przez Słuchacza modyfikacji treści umowy, bez złożenia odrębnego wyraźnego oświadczenia woli w tym zakresie i przesłania do siedziby Uczelni podpisanej zmodyfikowanej wersji umowy, przyjmuje się, iż Słuchacz wyraził zgodę na zawarcie niniejszej umowy w treści udostępnionej za pomocą internetu.
                    </fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block page-break-before="always">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 6.
              </fo:inline>
            </fo:block>
            <fo:block text-align="justify">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-length="0pt" />W sprawach nieuregulowanych w niniejszej umowie stosuje się przepisy Regulaminu Studiów Podyplomowych, Regulaminu Rekrutacji Online i przepisy Kodeksu Cywilnego.
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-indent="0pt" text-align="justify" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>

 	    <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 7.
              </fo:inline>
            </fo:block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
                  <fo:block>1.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Słuchacz wyraża zgodę na wystawianie dokumentów finansowych drogą elektroniczną, bez jego podpisu oraz zamieszczenie ich do jego wglądu i pobrania w Wirtualnym Pokoju Studenta.
					</fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
                <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
                  <fo:block>2.</fo:block>
                </fo:list-item-label>
                <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
                  <fo:block>
                    <fo:inline font-family="Tahoma" font-size="10pt">
                      <fo:leader leader-length="0pt" />Zamieszczenie dokumentów, o których mowa w ust. 1 w Wirtualnym Pokoju Studenta jest równoznaczne z możliwością zapoznania się przez Słuchacza z ich treścią.
					</fo:inline>
                  </fo:block>
                </fo:list-item-body>
              </fo:list-item>
            </fo:list-block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-weight="bold" font-size="10pt">
                <fo:leader leader-length="0pt" />§ 8.
              </fo:inline>
            </fo:block>
			  <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
					  <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
						  <fo:block>1.</fo:block>
					  </fo:list-item-label>
					  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
						  <fo:block>
							  <fo:inline font-family="Tahoma" font-size="10pt">
								  <fo:leader leader-length="0pt" />Umowa została sporządzona w dwóch jednobrzmiących egzemplarzach, po jednym dla każdej ze stron.
							  </fo:inline>
						  </fo:block>
					  </fo:list-item-body>
				  </fo:list-item>
			  </fo:list-block>
			  <fo:list-block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:list-item font-family="Tahoma" font-size="10pt" language="PL" line-height="1.147" text-align="justify">
					  <fo:list-item-label font-size="10pt" start-indent="0pt" text-indent="0pt" font-weight="normal" font-style="normal" font-family="Tahoma">
						  <fo:block>2.</fo:block>
					  </fo:list-item-label>
					  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
						  <fo:block>
							  <fo:inline font-family="Tahoma" font-size="10pt">
								  <fo:leader leader-length="0pt" />Załącznik, o którym mowa w § 1 ust. 4b do umowy stanowi jej nierozerwalną całość.
							  </fo:inline>
						  </fo:block>
					  </fo:list-item-body>
				  </fo:list-item>
			  </fo:list-block>

            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>

            <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Times" font-size="12pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="10pt">
                <fo:leader leader-pattern="space" leader-length="21pt" />
              </fo:inline>
              <fo:inline>
                <fo:leader leader-length="0pt" />_____________________
              </fo:inline>
              <fo:inline>
                <fo:leader leader-pattern="space" leader-length="15pt" />
              </fo:inline>
              <fo:inline>
                <fo:leader leader-pattern="space" leader-length="35.25pt" /><fo:leader leader-length="0pt" />______________________
              </fo:inline>
            </fo:block>
            <fo:block font-family="Tahoma" font-size="9pt" language="PL" text-align="left" start-indent="14.2pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-family="Tahoma" font-size="9pt">
                <fo:leader leader-length="0pt" />                        AHE                                                             Słuchacz (czytelny podpis)
              </fo:inline>
            </fo:block>

			  <fo:block page-break-before="always"/>

			  <fo:block font-family="Arial" font-size="12pt" language="PL" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:leader />
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />ZAŁĄCZNIK 1 DO UMOWY O WARUNKACH ODPŁATNOŚCI ZA STUDIA Z DNIA
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline color="black" font-size="10pt" />
				  <fo:inline color="black" font-size="10pt">
					  <fo:leader leader-length="0pt" />
					  <xsl:value-of select="$dataWygenerowania"/>
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="12pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />zawartej pomiędzy Akademią Humanistyczno-Ekonomiczną w Łodzi a Słuchaczem
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="12pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-family="Tahoma" font-weight="bold" color="black" font-size="10pt" />
				  <fo:inline font-family="Tahoma" font-weight="bold" color="black" font-size="10pt">
					  <fo:leader leader-length="0pt" />
					  <xsl:value-of select="Student"/>
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" text-align="left" end-indent="-43.85pt" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:leader />
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />CENNIK OPŁAT ZA USŁUGI DYDAKTYCZNE NIEOBJĘTE CZESNYM
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />obowiązujący na studiach podyplomowych od 21 marca 2013 r.
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Tahoma" font-size="10pt" language="PL" text-align="center" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:leader />
			  </fo:block>
			  <fo:table font-family="Times" start-indent="-28.6pt" border-top-style="solid" border-top-color="black"
						border-top-width="0.5pt" border-left-style="solid" border-left-color="black" border-left-width="0.5pt"
						border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.5pt" border-right-style="solid"
						border-right-color="black" border-right-width="0.5pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags"
						table-layout="fixed" width="100.0%">
				  <fo:table-column column-number="1" column-width="27.5pt" />
				  <fo:table-column column-number="2" column-width="251.95pt" />
				  <fo:table-column column-number="3" column-width="80.55pt" />
				  <fo:table-column column-number="4" column-width="153pt" />
				  <fo:table-body start-indent="0pt" end-indent="0pt">
					  <fo:table-row>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  L.p.
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  WYSZCZEGÓLNIENIE
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  OPŁATY
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  UWAGI
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>1.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Reaktywacja – wznowienie studiów, wpisanie na semestr/uzupełnianie przedmiotów
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  100 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />1a
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Uzupełnianie przedmiotu w ramach reaktywacji
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  300 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Opłata jednorazowa za przedmiot
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" number-rows-spanned="3" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />1b
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Powtarzanie / wznowienie nauki na semestrze w ramach reaktywacji
								  </fo:inline>
							  </fo:block>
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="18pt" text-indent="0pt" font-family="Symbol">
										  <fo:block font-size="12pt" font-family="Arial">•</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="36pt" text-indent="0pt">
										  <fo:block>
											  <fo:inline font-size="10pt">
												  <fo:leader leader-length="0pt" />Dla studiów trwających 2 semestry opłata za semestr
											  </fo:inline>
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="5pt" padding-bottom="0pt" padding-right="5pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  1 opłaty za cały tok studiów *
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />opłata liczona od ceny studiów dla edycji do której dołącza słuchacz.
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="18pt" text-indent="0pt" font-family="Symbol">
										  <fo:block font-size="12pt" font-family="Arial">•</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="36pt" text-indent="0pt">
										  <fo:block>
											  <fo:inline font-size="10pt">
												  <fo:leader leader-length="0pt" />Dla studiów trwających 3 semestry opłata za każdy semestr pozostały do ukończenia studiów
											  </fo:inline>
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  1/3 opłaty za cały tok studiów *
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />opłata liczona od ceny studiów dla edycji do której dołącza słuchacz.
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="3pt" padding-left="3.7pt" padding-bottom="3pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="18pt" text-indent="0pt" font-family="Symbol">
										  <fo:block font-size="12pt" font-family="Arial">•</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="36pt" text-indent="0pt">
										  <fo:block>
											  <fo:inline font-size="10pt">
												  <fo:leader leader-length="0pt" />Dla studiów trwających 4 semestry opłata za każdy semestr pozostały do ukończenia studiów
											  </fo:inline>
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="5pt" padding-bottom="0pt" padding-right="5pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  1 opłaty za cały tok studiów *
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />opłata liczona od ceny studiów dla edycji do której dołącza słuchacz.
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />1c
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Reaktywacja – powołanie składu Komisji Egzaminacyjnej na wniosek słuchacza
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  200 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>2.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Zaświadczenie
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  10 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Obowiązuje osoby ze statusem X, R, A, N do 90 dnia po zmianie ze statusu słuchacza -S
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>3.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Zaświadczenie
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  35 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Obowiązuje osoby ze statusem  X, R, A, N od 91 dnia po zmianie ze statusu słuchacza -S
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>4.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Opłata za monit
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  20 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>5.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Opłata za dodatkowy przedmiot
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  300 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>6.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Przedłużanie okresu dyplomowania
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  50 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt" display-align="center">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Opłata za każdy miesiąc przedłużenia
								  </fo:inline>
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
					  <fo:table-row>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt">
							  <fo:list-block>
								  <fo:list-item font-family="Arial" font-size="10pt" language="PL">
									  <fo:list-item-label font-size="12pt" start-indent="0pt" text-indent="0pt" font-family="Times">
										  <fo:block>7.</fo:block>
									  </fo:list-item-label>
									  <fo:list-item-body end-indent="inherit" start-indent="18pt" text-indent="0pt">
										  <fo:block>
											  <fo:leader />
										  </fo:block>
									  </fo:list-item-body>
								  </fo:list-item>
							  </fo:list-block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:inline font-size="10pt">
									  <fo:leader leader-length="0pt" />Duplikat świadectwa ukończenia studiów podyplomowych
								  </fo:inline>
							  </fo:block>
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt">
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  <fo:leader />
							  </fo:block>
							  <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center">
								  60 zł
							  </fo:block>
						  </fo:table-cell>
						  <fo:table-cell padding-top="0pt" padding-left="3.7pt" padding-bottom="0pt" padding-right="3.7pt" border-left-style="solid" border-right-style="solid" border-left-color="black" border-right-color="black" border-left-width="0.25pt" border-right-width="0.25pt" border-top-style="solid" border-bottom-style="solid" border-top-color="black" border-bottom-color="black" border-top-width="0.25pt" border-bottom-width="0.25pt">
							  <fo:block font-family="Arial" font-size="10pt" language="PL">
								  <fo:leader />
							  </fo:block>
						  </fo:table-cell>
					  </fo:table-row>
				  </fo:table-body>
			  </fo:table>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />* opłata liczona od ceny studiów dla edycji, do której dołącza słuchacz.
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" text-align="justify" end-indent="-43.85pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:leader />
			  </fo:block>
			  <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" text-align="justify" end-indent="-43.85pt" text-indent="35.45pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="0pt" />Uczelnia
				  </fo:inline>
				  <fo:inline font-size="10pt">
					  <fo:leader leader-pattern="space" leader-length="33pt" />
					  <fo:leader leader-length="0pt" />
				  </fo:inline>
				  <fo:inline font-size="10pt">
					  <fo:leader leader-pattern="space" leader-length="34.5pt" />
				  </fo:inline>
				  <fo:inline font-size="10pt">
					  <fo:leader leader-pattern="space" leader-length="35.25pt" />
					  <fo:leader leader-length="0pt" />
				  </fo:inline>
				  <fo:inline font-size="10pt">
					  <fo:leader leader-pattern="space" leader-length="29.25pt" />
				  </fo:inline>
				  <fo:inline font-size="10pt">
					  <fo:leader leader-length="133pt" />Słuchacz
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="12pt" language="PL" line-height="1.7205" text-align="justify" start-indent="-18pt" end-indent="-43.85pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline>
					  <fo:leader leader-length="0pt" />
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Tahoma" font-size="12pt" language="" line-height="1.7205" text-align="justify" start-indent="-18pt" end-indent="-43.85pt" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:inline>
					  <fo:leader leader-length="0pt" />______________________
					  <fo:leader leader-length="133pt" />_________________________
				  </fo:inline>
			  </fo:block>
			  <fo:block font-family="Arial" font-size="12pt" language="PL" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				  <fo:leader />
			  </fo:block>
			  
          </fo:block>
          <fo:block id="IDACIYWC" />
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>
</xsl:stylesheet>'
     
 WHERE Opis like 'UmowaPD3_Lodz_Mechatronika'
GO