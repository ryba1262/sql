UPDATE [dbo].[WUWydrukWersja]
   SET [Szablon] = '<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:param name="dataWygenerowania"/>
  <xsl:template match="/Dane">
    <fo:root font-family="Times" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml">
      <fo:layout-master-set xmlns:rx="http://www.renderx.com/XSL/Extensions" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882">
        <fo:simple-page-master master-name="section1-first-page" page-width="8.268055555555555in" page-height="11.693055555555556in" margin-top="25.4pt" margin-bottom="25.4pt" margin-right="70.85pt" margin-left="70.85pt">
          <fo:region-body margin-top="65pt" margin-bottom="9.55pt"></fo:region-body>
          <fo:region-before region-name="first-page-header" extent="11in"></fo:region-before>
          <fo:region-after region-name="first-page-footer" extent="11in" display-align="after"></fo:region-after>
        </fo:simple-page-master>
        <fo:simple-page-master master-name="section1-odd-page" page-width="8.268055555555555in" page-height="11.693055555555556in" margin-top="25.4pt" margin-bottom="25.4pt" margin-right="70.85pt" margin-left="70.85pt">
          <fo:region-body margin-top="65pt" margin-bottom="9.55pt"></fo:region-body>
          <fo:region-before region-name="odd-page-header" extent="11in"></fo:region-before>
          <fo:region-after region-name="odd-page-footer" extent="11in" display-align="after"></fo:region-after>
        </fo:simple-page-master>
        <fo:simple-page-master master-name="section1-even-page" page-width="8.268055555555555in" page-height="11.693055555555556in" margin-top="25.4pt" margin-bottom="25.4pt" margin-right="70.85pt" margin-left="70.85pt">
          <fo:region-body margin-top="90pt" margin-bottom="9.55pt"></fo:region-body>
          <fo:region-before region-name="even-page-header" extent="11in"></fo:region-before>
          <fo:region-after region-name="even-page-footer" extent="11in" display-align="after"></fo:region-after>
        </fo:simple-page-master>
        <fo:page-sequence-master master-name="section1-page-sequence-master">
          <fo:repeatable-page-master-alternatives>
            <fo:conditional-page-master-reference odd-or-even="odd" master-reference="section1-odd-page" />
            <fo:conditional-page-master-reference odd-or-even="even" master-reference="section1-even-page" />
          </fo:repeatable-page-master-alternatives>
        </fo:page-sequence-master>
      </fo:layout-master-set>
      <fo:page-sequence master-reference="section1-page-sequence-master" format="1" xmlns:rx="http://www.renderx.com/XSL/Extensions" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882">
        <fo:static-content flow-name="first-page-header">
          <fo:retrieve-marker retrieve-class-name="first-page-header" retrieve-position="first-including-carryover" retrieve-boundary="page" />
        </fo:static-content>
        <fo:static-content flow-name="first-page-footer">
          <fo:retrieve-marker retrieve-class-name="first-page-footer" retrieve-position="first-including-carryover" retrieve-boundary="page" />
        </fo:static-content>
        <fo:static-content flow-name="odd-page-header">
          <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
            <fo:block font-family="Tahoma" font-size="9pt" language="PL" keep-with-next.within-page="always" text-align="left" font-weight="bold" color="#993300">
              <fo:inline>
                <fo:leader leader-length="10pt" />
              </fo:inline>
              <fo:inline>
                <fo:external-graphic content-width="250pt" content-height="60pt" src="https://www.ahe.lodz.pl/sites/default/files/godlo_wersja2.jpg" />
              </fo:inline>
              <fo:inline>
                <fo:leader leader-pattern="space"  />
              </fo:inline>
              <fo:inline>
                <fo:leader leader-pattern="space"  />
              </fo:inline>
              <fo:inline>
                <fo:leader leader-pattern="space"  />
              </fo:inline>
              <fo:inline>
                <fo:leader leader-pattern="space"  />
              </fo:inline>
              <fo:inline>
                <fo:leader leader-pattern="space"  />
              </fo:inline>
              <fo:inline>
                <fo:leader leader-pattern="space"  />
              </fo:inline>
              <fo:inline font-size="9pt">
                <fo:external-graphic content-width="131.25pt" content-height="50.25pt" src="https://www.ahe.lodz.pl/sites/default/files/logo_ckp.jpg" />
              </fo:inline>
              <fo:inline>
                <fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Times" font-size="12pt" language="PL">
              <fo:leader />
            </fo:block>
            <fo:block border-bottom-width="medium" border-bottom-style="solid"
             border-bottom-color="#993300" font-weight="bold" text-align="outside">
            </fo:block>
            <fo:block font-family="Times" font-size="12pt" language="PL">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Times" font-size="12pt" language="PL">
              <fo:inline />
            </fo:block>
          </fo:block>
        </fo:static-content>
        <fo:static-content flow-name="odd-page-footer">
          <fo:block border-bottom-width="medium" border-bottom-style="solid"
 border-bottom-color="#993300" font-weight="bold" text-align="outside">
          </fo:block>
        </fo:static-content>
        <fo:static-content flow-name="even-page-header">
          <fo:block xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
            <fo:block font-family="Tahoma" font-size="9pt" language="PL" keep-with-next.within-page="always" text-align="left" font-weight="bold" color="#993300">
              <fo:inline>
                <fo:leader leader-length="10pt" />
              </fo:inline>
              <fo:inline>
                <fo:external-graphic content-width="250pt" content-height="60pt" src="https://www.ahe.lodz.pl/sites/default/files/godlo_wersja2.jpg" />
              </fo:inline>
              <fo:inline>
                <fo:leader leader-pattern="space"  />
              </fo:inline>
              <fo:inline>
                <fo:leader leader-pattern="space"  />
              </fo:inline>
              <fo:inline>
                <fo:leader leader-pattern="space"  />
              </fo:inline>
              <fo:inline>
                <fo:leader leader-pattern="space"  />
              </fo:inline>
              <fo:inline>
                <fo:leader leader-pattern="space"  />
              </fo:inline>
              <fo:inline>
                <fo:leader leader-pattern="space"  />
              </fo:inline>
              <fo:inline font-size="9pt">
                <fo:external-graphic content-width="131.25pt" content-height="50.25pt" src="https://www.ahe.lodz.pl/sites/default/files/logo_ckp.jpg" />
              </fo:inline>
              <fo:inline>
                <fo:leader leader-length="0pt" />
              </fo:inline>
            </fo:block>
            <fo:block font-family="Times" font-size="12pt" language="PL">
              <fo:leader />
            </fo:block>
            <fo:block border-bottom-width="medium" border-bottom-style="solid"
             border-bottom-color="#993300" font-weight="bold" text-align="outside">
            </fo:block>
            <fo:block font-family="Times" font-size="12pt" language="PL">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Times" font-size="12pt" language="PL">
              <fo:inline />
            </fo:block>
          </fo:block>
        </fo:static-content>
        <fo:static-content flow-name="even-page-footer">
          <fo:block border-bottom-width="medium" border-bottom-style="solid"
 border-bottom-color="#993300" font-weight="bold" text-align="outside">
          </fo:block>
        </fo:static-content>
        <fo:static-content flow-name="xsl-footnote-separator">
          <fo:block>
            <fo:leader leader-pattern="rule" leader-length="144pt" rule-thickness="0.5pt" rule-style="solid" color="gray" />
          </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">
          <fo:block widows="2" orphans="2" font-size="10pt" line-height="1.147" white-space-collapse="false">
            <fo:block font-family="Times" font-size="12pt" language="PL" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>

           
              <fo:block id="IDANOWDF">
               
                  <fo:block id="IDAPOWDF">
                    <fo:block id="IDAQOWDF">
                      <fo:block id="IDAROWDF">
                        <fo:block font-family="Arial" font-size="10pt" language="PL" keep-with-next.within-page="always" text-align="center" font-weight="bold" color="#993300">
                          <fo:inline font-family="Arial" font-size="10pt">
                            <fo:leader leader-length="0pt" />FORMULARZ ZG�OSZENIOWY<fo:leader leader-length="0pt" />
                          </fo:inline>
                        </fo:block>
                        <fo:block font-family="Arial" font-size="11pt" language="PL" text-align="center">
                          <fo:leader />
                        </fo:block>
                        <fo:block font-family="Arial" font-size="10pt" language="PL" text-align="center" space-after="2pt">
                          <fo:inline font-family="Arial" font-size="10pt">
                            <fo:leader leader-length="0pt" />Prosz� o przyj�cie na organizowane przez Akademi� Humanistyczno-Ekonomiczn�  w �odzi<fo:leader leader-length="0pt" />
                          </fo:inline>
                        </fo:block>
                      </fo:block>
                    </fo:block>
                    <fo:block id="IDA1QWDF">
                      <fo:block font-family="Arial" space-after="3pt" font-size="10pt" language="PL" keep-with-next.within-page="always" line-height="1.147" text-align="center" font-weight="bold">
                        <fo:inline font-family="Arial">
                          <fo:leader leader-length="0pt" />Studia Podyplomowe<fo:leader leader-length="0pt" />
                        </fo:inline>
                      </fo:block>
                      <fo:block font-family="Arial" font-size="13pt" language="PL" text-align="center" font-weight="bold" space-after="10pt">
                        <fo:inline id="Kierunek" />
                        <fo:inline font-weight="bold" font-size="13pt" />
                        <fo:inline font-weight="bold" font-size="13pt">
                          <fo:leader leader-length="0pt" />
                          <xsl:value-of select="Kierunek"/>
                        </fo:inline>
                      </fo:block>
                      <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" text-align="justify" font-weight="bold" color="#993300" text-decoration="underline">
                        <fo:inline font-family="Arial" font-weight="bold" color="#993300" font-size="10pt" text-decoration="underline">
                          <fo:leader leader-length="0pt" />DANE OSOBOWE:
                        </fo:inline>
                      </fo:block>
                      <fo:table table-layout="fixed" width="100%" display-align="center">
                        <fo:table-column column-width="40%"/>
                        <fo:table-column column-width="60%"/>
                        <fo:table-body>
                          <fo:table-row>
                            <fo:table-cell>
                              <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" end-indent="-5.5pt" text-align="justify">
                                <fo:inline font-family="Arial" font-size="10pt">
                                  <fo:leader leader-length="0pt" />Nazwisko: <fo:leader leader-length="0pt" />
                                </fo:inline>
                                <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                  <xsl:value-of select="Nazwisko"/>
                                </fo:inline>
                              </fo:block>
                            </fo:table-cell>
                            <fo:table-cell>
                              <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" end-indent="-5.5pt" text-align="justify">
                                <fo:inline font-family="Arial" font-size="10pt">
                                  <fo:leader leader-length="0pt" />Imi�: <fo:leader leader-length="0pt" />
                                </fo:inline>
                                <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                  <xsl:value-of select="Imie"/>
                                  <fo:inline color="white">-</fo:inline>
                                </fo:inline>
								<fo:inline font-family="Arial" font-size="10pt">
                                  <fo:leader leader-length="0pt" />Drugie imi�: <fo:leader leader-length="0pt" />
                                </fo:inline>
								 <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                  <xsl:value-of select="DrugieImie"/>
                                  <fo:inline color="white">-</fo:inline>
                                </fo:inline>
                              </fo:block>
                            </fo:table-cell>
                          </fo:table-row>
                          <fo:table-row>
                            <fo:table-cell>
                              <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" end-indent="-5.5pt" text-align="justify">
                                <fo:inline font-family="Arial" font-size="10pt">
                                  <fo:leader leader-length="0pt" />Data urodzenia: <fo:leader leader-length="0pt" />
                                </fo:inline>
                                <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                  <xsl:value-of select="substring(DataUrodzenia,1,10)"/>
                                </fo:inline>
                                <fo:inline font-family="Arial" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                </fo:inline>
                              </fo:block>>
                            </fo:table-cell>
                            <fo:table-cell>
                              <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" end-indent="-5.5pt" text-align="justify">
                                <fo:inline font-family="Arial" font-style="italic" font-size="10pt">
                                  <fo:leader leader-length="0pt" />Miejsce urodzenia: <fo:leader leader-length="0pt" />
                                </fo:inline>
                                <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                  <xsl:value-of select="MiejsceUrodzenia"/>
                                </fo:inline>
                              </fo:block>
                            </fo:table-cell>
                          </fo:table-row>
                          <fo:table-row>
                            <fo:table-cell>
                              <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" end-indent="-5.5pt" text-align="justify">
                                <fo:inline font-family="Arial" font-size="10pt">
                                  <fo:leader leader-length="0pt" />Imi� ojca: <fo:leader leader-length="0pt" />
                                </fo:inline>
                                <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                  <xsl:value-of select="ImieOjca"/>
                                </fo:inline>
                              </fo:block>
                            </fo:table-cell>
                            <fo:table-cell>
                              <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" end-indent="-5.5pt" text-align="justify">
                                <fo:inline font-weight="bold" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                </fo:inline>
                                <fo:inline font-family="Arial" font-size="10pt">
                                  <fo:leader leader-length="0pt" />Imi� matki: <fo:leader leader-length="0pt" />
                                </fo:inline>
                                <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                  <xsl:value-of select="ImieMatki"/>
                                </fo:inline>
                              </fo:block>
                            </fo:table-cell>
                          </fo:table-row>
                          <fo:table-row>
                            <fo:table-cell>
                              <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" end-indent="-5.5pt" text-align="justify">
                                <fo:inline font-family="Arial" font-size="10pt">
                                  <fo:leader leader-length="0pt" />PESEL: <fo:leader leader-length="0pt" />
                                </fo:inline>
                                <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                  <xsl:value-of select="PESEL"/>
                                </fo:inline>
                                <fo:inline font-weight="bold" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                </fo:inline>
                                <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                </fo:inline>
                              </fo:block>
                            </fo:table-cell>
                            <fo:table-cell>
                              <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" end-indent="-5.5pt" text-align="justify">
                                <fo:inline font-family="Arial" font-size="10pt">
                                  <fo:leader leader-length="0pt" />Nazwisko panie�skie: <fo:leader leader-length="0pt" />
                                </fo:inline>
                                <fo:inline font-family="Arial" color="#333333" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                  <xsl:value-of select="NazwiskoPanien"/>
                                </fo:inline>
                              </fo:block>
                            </fo:table-cell>
                          </fo:table-row>
                        </fo:table-body>
                      </fo:table>
                      <fo:table table-layout="fixed" width="100%" display-align="center">
                        <fo:table-column column-width="50%"/>
                        <fo:table-column column-width="50%"/>
                        <fo:table-body>
                          <fo:table-row>
                            <fo:table-cell>
                              <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" end-indent="-5.5pt" text-align="justify" font-style="italic">
                                <fo:inline font-family="Arial" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                  <xsl:value-of select="DOTSNazwa"/>
                                  <fo:inline color="white">-</fo:inline>
                                </fo:inline>
                                <fo:inline font-family="Arial" font-size="10pt">
                                  <fo:leader leader-length="0pt" />seria i numer: <fo:inline id="DOTSNazwaDop" />
                                </fo:inline>
                                <fo:inline font-family="Arial" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                </fo:inline>
                                <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                </fo:inline>
                                <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                  <xsl:value-of select="DOTSSeriaNumer"/>
                                  <fo:inline color="white">---</fo:inline>
                                </fo:inline>
                              </fo:block>
                            </fo:table-cell>
                            <fo:table-cell>
                              <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" end-indent="-5.5pt" text-align="justify" font-style="italic">
                                <fo:inline font-family="Arial" font-style="italic" font-size="10pt">
                                  <fo:leader leader-length="0pt" />wydany przez <fo:leader leader-length="0pt" />
                                </fo:inline>
                                <fo:inline font-family="Arial" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                </fo:inline>
                                <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                  <xsl:value-of select="DOTSWydanyPrzez"/>
                                </fo:inline>
                              </fo:block>
                            </fo:table-cell>
                          </fo:table-row>
                        </fo:table-body>
                      </fo:table>
                      <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" end-indent="-5.5pt" text-align="justify">
                        <fo:inline font-family="Arial" font-size="10pt">
                          <fo:leader leader-length="0pt" />Adres zameldowania: <fo:leader leader-length="0pt" />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                          <fo:leader leader-length="0pt" />
                          <xsl:value-of select="AdresSP"/>
                        </fo:inline>
                      </fo:block>
                      <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" end-indent="-5.5pt">
                        <fo:inline font-family="Arial" font-style="italic" font-size="10pt">
                          <fo:leader leader-length="0pt" />wojew�dztwo <fo:leader leader-length="0pt" />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                          <fo:leader leader-length="0pt" />
                          <xsl:value-of select="AdresSPWojewodztwo"/>
                        </fo:inline>
                        <fo:inline font-family="Arial" font-size="10pt">
                          <fo:leader leader-length="0pt" />
                        </fo:inline>
                      </fo:block>
                      <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" end-indent="-5.5pt">
                        <fo:inline font-family="Arial" font-size="10pt">
                          <fo:leader leader-length="0pt" />Adres do korespondencji: <fo:leader leader-length="0pt" />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                          <fo:leader leader-length="0pt" />
                          <xsl:value-of select="AdresKor"/>
                        </fo:inline>
                      </fo:block>
                      <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" end-indent="-5.5pt">
                        <fo:inline font-family="Arial" font-style="italic" font-size="10pt">
                          <fo:leader leader-length="0pt" />wojew�dztwo <fo:leader leader-length="0pt" />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-size="10pt" font-weight="bold">
                          <fo:leader leader-length="0pt" />
                          <xsl:value-of select="AdresKorWojewodztwo"/>
                        </fo:inline>
                        <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                          <fo:leader leader-length="0pt" />
                        </fo:inline>
                      </fo:block>
                      <fo:table table-layout="fixed" width="100%" display-align="center">
                        <fo:table-column column-width="50%"/>
                        <fo:table-column column-width="50%"/>
                        <fo:table-body>
                          <fo:table-row>
                            <fo:table-cell>
                              <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" end-indent="-5.5pt">
                                <fo:inline font-family="Arial" font-size="10pt">
                                  <fo:leader leader-length="0pt" />Nr telefonu: <fo:leader leader-length="0pt" />
                                </fo:inline>
                                <fo:inline font-family="Arial" font-style="italic" font-size="10pt">
                                  <fo:leader leader-length="0pt" />zameldowania <fo:leader leader-length="0pt" />
                                </fo:inline>
                                <fo:inline font-family="Arial" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                </fo:inline>
                                <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                  <xsl:value-of select="TelefonKontaktowy"/>
                                </fo:inline>
                              </fo:block>
                            </fo:table-cell>
                            <fo:table-cell>
                              <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" end-indent="-5.5pt">
                                <fo:inline font-family="Arial" font-style="italic" font-size="10pt">
                                  <fo:leader leader-length="0pt" />kom�rkowy <fo:leader leader-length="0pt" />
                                </fo:inline>
                                <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                  <xsl:value-of select="TelefonKomorkowy"/>
                                </fo:inline>
                              </fo:block>
                            </fo:table-cell>
                          </fo:table-row>
                        </fo:table-body>
                      </fo:table>
                      <fo:table table-layout="fixed" width="100%" display-align="center">
                        <fo:table-column column-width="40%"/>
                        <fo:table-column column-width="60%"/>
                        <fo:table-body>
                          <fo:table-row>
                            <fo:table-cell>
                              <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" end-indent="-5.5pt">
                                <fo:inline font-family="Arial" font-style="italic" font-size="10pt">
                                  <fo:leader leader-length="0pt" />do pracy <fo:leader leader-length="0pt" />
                                </fo:inline>
                                <fo:inline font-family="Arial" font-style="italic" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                </fo:inline>
                                <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                  <xsl:value-of select="ZakladPracyTelefon"/>
                                </fo:inline>
                              </fo:block>
                            </fo:table-cell>
                            <fo:table-cell>
                              <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" end-indent="-5.5pt">
                                <fo:inline font-family="Arial" font-style="italic" font-size="10pt">
                                  <fo:leader leader-length="0pt" />e-mail: <fo:leader leader-length="0pt" />
                                </fo:inline>
                                <fo:inline font-weight="bold">
                                  <fo:leader leader-length="0pt" />
                                </fo:inline>
                                <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                  <xsl:value-of select="Email1"/>
                                </fo:inline>
                              </fo:block>
                            </fo:table-cell>
                          </fo:table-row>
                        </fo:table-body>
                      </fo:table>
                      <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" font-weight="bold" color="#993300" text-decoration="underline">
                        <fo:inline font-family="Arial" font-weight="bold" color="#993300" font-size="10pt" text-decoration="underline">
                          <fo:leader leader-length="0pt" />WYKSZTA�CENIE:
                        </fo:inline>
                      </fo:block>
                      <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205">
                        <fo:inline font-family="Arial" font-size="10pt">
                          <fo:leader leader-length="0pt" />Uczelnia: <fo:leader leader-length="0pt" />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                          <fo:leader leader-length="0pt" />
                        </fo:inline>
                        <fo:inline id="UkonUczNazwa" />
                        <fo:inline font-family="Arial" font-weight="bold" font-size="10pt" />
                        <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                          <fo:leader leader-length="0pt" />
                          <xsl:value-of select="UkonUczNazwa"/>
                        </fo:inline>
                      </fo:block>
					    <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205">
                      <fo:inline font-family="Arial" font-size="10pt">
                        <fo:leader leader-length="0pt" />Wydzia�: <fo:leader leader-length="0pt" />
                      </fo:inline>
                      <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                        <fo:leader leader-length="0pt" />
                        <xsl:value-of select="UkonUczWydzial"/>
                      </fo:inline>
                    </fo:block>
                      <fo:table table-layout="fixed" width="100%" display-align="center">
                        <fo:table-column column-width="40%"/>
                        <fo:table-column column-width="60%"/>
                        <fo:table-body>
                          <fo:table-row>
                            <fo:table-cell>
                              <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205">
                                <fo:inline font-family="Arial" font-size="10pt">
                                  <fo:leader leader-length="0pt" />data uko�czenia: <fo:leader leader-length="0pt" />
                                </fo:inline>
                                <fo:inline id="UkonUczDataDyplomu" />
                                <fo:inline font-family="Arial" font-weight="bold" font-size="10pt" />
                                <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                  <xsl:value-of select="substring(UkonUczDataDyplomu,1,10)"/>
                                </fo:inline>
                                <fo:inline font-family="Arial" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                </fo:inline>
                              </fo:block>
                            </fo:table-cell>
                            <fo:table-cell>
                              <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205">
                                <fo:inline font-family="Arial" font-style="italic" font-size="10pt">
                                  <fo:leader leader-length="0pt" />tytu� <fo:leader leader-length="0pt" />
                                </fo:inline>
                                <fo:inline font-family="Arial" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                </fo:inline>
                                <fo:inline id="UkonUczUzTytul" />
                                <fo:inline font-family="Arial" font-weight="bold" font-size="10pt" />
                                <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                                  <fo:leader leader-length="0pt" />
                                  <xsl:value-of select="UkonUczUzTytul" />
                                </fo:inline>
                              </fo:block>
                            </fo:table-cell>
                          </fo:table-row>
                        </fo:table-body>
                      </fo:table>
                      <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205">
                        <fo:inline font-family="Arial" font-size="10pt">
                          <fo:leader leader-length="0pt" />kierunek studi�w <fo:leader leader-length="0pt" />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                          <fo:leader leader-length="0pt" />
                        </fo:inline>
                        <fo:inline id="UkonUczKierunek" />
                        <fo:inline font-family="Arial" font-weight="bold" font-size="10pt" />
                        <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                          <fo:leader leader-length="0pt" />
                          <xsl:value-of select="UkonUczKierunek"/>
                        </fo:inline>
                      </fo:block>
                    </fo:block>
                    <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" end-indent="-5.5pt" font-weight="bold" color="#993300" text-decoration="underline">
                      <fo:inline font-family="Arial" font-weight="bold" color="#993300" font-size="10pt" text-decoration="underline">
                        <fo:leader leader-length="0pt" />MIEJSCE PRACY:
                      </fo:inline>
                    </fo:block>
                    <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205">
                      <fo:inline font-family="Arial" font-size="10pt">
                        <fo:leader leader-length="0pt" />Nazwa firmy: <fo:leader leader-length="0pt" />
                      </fo:inline>
                      <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                        <fo:leader leader-length="0pt" />
                        <xsl:value-of select="ZakladPracyNazwa"/>
                      </fo:inline>
                    </fo:block>
                    <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205">
                      <fo:inline font-family="Arial" font-size="10pt">
                        <fo:leader leader-length="0pt" />Stanowisko: <fo:leader leader-length="0pt" />
                      </fo:inline>
                      <fo:inline font-family="Arial" font-size="7pt">
                        <fo:leader leader-length="0pt" />
                      </fo:inline>
                      <fo:inline font-family="Arial" font-weight="bold" color="#993300" font-size="10pt">
                        <fo:leader leader-length="0pt" />
                      </fo:inline>
                      <fo:inline id="ZakladPracyStanow" />
                      <fo:inline font-family="Arial" font-weight="bold" font-size="10pt" />
                      <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                        <fo:leader leader-length="0pt" />
                        <xsl:value-of select="ZakladPracyStanow"/>
                      </fo:inline>
                    </fo:block>
                    <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205">
                      <fo:inline font-family="Arial" font-size="10pt">
                        <fo:leader leader-length="0pt" />Miejscowo��: <fo:leader leader-length="0pt" />
                      </fo:inline>
                      <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                        <fo:leader leader-length="0pt" />
                        <xsl:value-of select="ZakladPracyMiejsc"/>
                      </fo:inline>
                    </fo:block>                
                    <fo:table table-layout="fixed" width="100%" display-align="center" space-before="5pt">
                      <fo:table-column column-width="60%"/>
                      <fo:table-column column-width="10%"/>
                      <fo:table-column column-width="10%"/>
                      <fo:table-column column-width="10%"/>
                      <fo:table-column column-width="10%"/>
                      <fo:table-body>
                        <fo:table-row>
                          <fo:table-cell>
                            <fo:block font-family="Arial" font-size="9pt" language="PL" text-align="left">
                              <fo:inline font-family="Arial" font-weight="bold" color="#993300" font-size="10pt" text-decoration="underline">
                                <fo:leader leader-length="0pt" />STUDIUJ�/UKO�CZY�AM (EM) w AHE w �odzi<fo:leader leader-length="0pt" />
                              </fo:inline>
                              <fo:inline font-family="Arial" font-weight="bold" color="#993300" font-size="10pt">
                                <fo:leader leader-length="0pt" />:               <fo:leader leader-length="0pt" />
                              </fo:inline>
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell azimuth="center" padding-bottom="2pt">
                            <fo:block font-weight="bold" space-before="6pt" font-family="Arial" font-size="7pt" border-width="2pt" border-style="solid" border-color="black" start-indent="0pt" end-indent="38pt">
                              <fo:leader leader-length="0pt" />
                            </fo:block>
                            <fo:block font-family="Arial" font-size="6pt" language="PL">
                              <fo:leader leader-length="0pt" />
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell azimuth="center">
                            <fo:block font-family="Arial" font-size="7pt" language="PL" space-before="6pt" start-indent="-30pt">
                              <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                                <fo:leader leader-length="0pt" />TAK<fo:leader leader-length="0pt" />
                              </fo:inline>
                            </fo:block>
                            <fo:block font-family="Arial" font-size="6pt" language="PL">
                              <fo:leader leader-length="0pt" />
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell azimuth="center" padding-bottom="2pt">
                            <fo:block font-weight="bold" space-before="6pt" font-family="Arial" font-size="7pt" border-width="2pt" border-style="solid" border-color="black" start-indent="0pt" end-indent="38pt">
                              <fo:leader leader-length="0pt" />
                            </fo:block>
                            <fo:block font-family="Arial" font-size="6pt" language="PL">
                              <fo:leader leader-length="0pt" />
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell azimuth="center">
                            <fo:block font-family="Arial" font-size="7pt" language="PL" space-before="6pt" start-indent="-30pt">
                              <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                                <fo:leader leader-length="0pt" />NIE<fo:leader leader-length="0pt" />
                              </fo:inline>
                            </fo:block>
                            <fo:block font-family="Arial" font-size="6pt" language="PL">
                              <fo:leader leader-length="0pt" />
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                        <fo:table-row>
                          <fo:table-cell>
                            <fo:block font-family="Arial" font-size="9pt" language="PL" text-align="left">
                              <fo:inline font-family="Arial" font-weight="bold" color="#993300" font-size="10pt" text-decoration="underline">
                                <fo:leader leader-length="0pt" />PRZYGOTOWANIE PEDAGOGICZNE<fo:leader leader-length="0pt" />
                              </fo:inline>
                              <fo:inline font-family="Arial" font-weight="bold" color="#993300" font-size="10pt">
                                <fo:leader leader-length="0pt" />:               <fo:leader leader-length="0pt" />
                              </fo:inline>
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell azimuth="center" padding-bottom="2pt">
                            <fo:block font-weight="bold" space-before="6pt" font-family="Arial" font-size="7pt" border-width="2pt" border-style="solid" border-color="black" start-indent="0pt" end-indent="38pt">
                              <fo:leader leader-length="0pt" />
                            </fo:block>
                            <fo:block font-family="Arial" font-size="6pt" language="PL">
                              <fo:leader leader-length="0pt" />
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell azimuth="center">
                            <fo:block font-family="Arial" font-size="7pt" language="PL" space-before="6pt" start-indent="-30pt">
                              <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                                <fo:leader leader-length="0pt" />TAK<fo:leader leader-length="0pt" />
                              </fo:inline>
                            </fo:block>
                            <fo:block font-family="Arial" font-size="6pt" language="PL">
                              <fo:leader leader-length="0pt" />
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell azimuth="center" padding-bottom="2pt">
                            <fo:block font-weight="bold" space-before="6pt" font-family="Arial" font-size="7pt" border-width="2pt" border-style="solid" border-color="black" start-indent="0pt" end-indent="38pt">
                              <fo:leader leader-length="0pt" />
                            </fo:block>
                            <fo:block font-family="Arial" font-size="6pt" language="PL">
                              <fo:leader leader-length="0pt" />
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell azimuth="center">
                            <fo:block font-family="Arial" font-size="7pt" language="PL" space-before="6pt" start-indent="-30pt">
                              <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                                <fo:leader leader-length="0pt" />NIE<fo:leader leader-length="0pt" />
                              </fo:inline>
                            </fo:block>
                            <fo:block font-family="Arial" font-size="6pt" language="PL">
                              <fo:leader leader-length="0pt" />
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </fo:table-body>
                    </fo:table>
                      <fo:block font-family="Arial" space-before="30pt" space-after="3pt" font-size="10pt" language="PL" keep-with-next.within-page="always" line-height="1.7205" text-align="center" font-weight="bold">
                        <fo:inline font-family="Arial">
                          <fo:leader leader-length="0pt" />Prawdziwo�� danych zawartych w formularzu potwierdzam w�asnor�cznym podpisem<fo:leader leader-length="0pt" />
                        </fo:inline>
                      </fo:block>
                      <fo:block font-family="Arial" space-after="60pt" font-size="7pt" language="PL" keep-with-next.within-page="always" line-height="1.7205" text-align="center">
                        <fo:inline font-family="Arial">
                          <fo:leader leader-length="0pt" />Osoby przyj�te na Studia Podyplomowe podpisuj� z AHE umow� o pobieraniu nauki.<fo:leader leader-length="0pt" />
                        </fo:inline>
                      </fo:block>
                      <fo:block font-family="Arial" font-size="11pt" language="PL" line-height="1.7205" text-align="justify" font-weight="bold">
                        <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                          <fo:leader leader-length="0pt" />
                          <xsl:value-of select="$dataWygenerowania"/>
                        </fo:inline>
                      </fo:block>
                      <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" text-align="justify">
                        <fo:inline font-family="Arial" font-size="10pt">
                          <fo:leader leader-length="0pt" />....................................
                        </fo:inline>
                        <fo:inline font-family="Arial" font-size="10pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-size="10pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-size="10pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-size="10pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-size="10pt">
                          <fo:leader leader-pattern="space"  /><fo:leader leader-length="0pt" />            ...................................................
                        </fo:inline>
                      </fo:block>
                      <fo:block font-family="Arial" font-size="9pt" language="PL" line-height="1.7205" text-align="justify">
                        <fo:inline font-family="Arial" font-style="italic" font-size="9pt">
                          <fo:leader leader-length="0pt" />   ��d�, data
                        </fo:inline>
                        <fo:inline font-family="Arial" font-style="italic" font-size="9pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-style="italic" font-size="9pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-style="italic" font-size="9pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-style="italic" font-size="9pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-style="italic" font-size="9pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-style="italic" font-size="9pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-style="italic" font-size="9pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-style="italic" font-size="9pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-style="italic" font-size="9pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-style="italic" font-size="9pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-style="italic" font-size="9pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-style="italic" font-size="9pt" >
                          <fo:leader leader-pattern="space"  /><fo:leader leader-length="0" />Czytelny podpis kandydata
                        </fo:inline>
                      </fo:block>
                      <fo:block font-family="Times" font-size="10pt" language="PL" text-align="justify" space-before="13pt" page-break-before="always" >
                        <fo:inline font-size="9pt">
                          <fo:leader leader-length="0pt" />Informujemy, �e Administratorem jest (podmiotem ustalaj�cym cele i sposoby przetwarzania danych osobowych) Akademia Humanistyczno-Ekonomiczna w �odzi, NIP 7251014115, adres korespondencyjny: 90-212 ��d�. ul. Sterlinga 26, adres poczty elektronicznej: uczelnia@ahe.lodz.pl. Dane kontaktowe Inspektora Danych Osobowych s� to�same z danymi kontaktowymi Administratora wskazanymi w zdaniu poprzednim. 
                        </fo:inline>
                      </fo:block>
					   
                      <fo:block font-family="Times" font-size="9pt" space-before="10pt" language="PL" text-align="justify">
                        <fo:inline font-size="9pt">
                          <fo:leader leader-length="0pt" />Dane osobowe s� gromadzone i przetwarzane w celu realizacji procesu rekrutacji, przebiegu studi�w i dzia�a� zwi�zanych z monitorowaniem i egzekucj� op�at, jak te� w celach archiwalnych oraz zgodnie tre�ci� udzielonych Administratorowi zg�d. Podanie danych jest obowi�zkowe i wynika z przepis�w Ustawy z dnia 27 lipca 2005 r. Prawo o szkolnictwie wy�szym (Dz.U.2017.2183 j.t. ze zm.) w zwi�zku z trybem i warunkami rekrutacji ustalonymi przez Akademi� Humanistyczno-Ekonomiczn� w �odzi. Konsekwencj� niepodania danych jest wstrzymanie post�powania rekrutacyjnego. Podstaw� prawn� przetwarzania s� czynno�ci zwi�zane z wykonaniem umowy, wype�nienie obowi�zku prawnego ci���cego na Administratorze wynikaj�ce z przepis�w o szkolnictwie wy�szym oraz mo�e by� zgoda osoby, kt�rej dane dotycz�.
                        </fo:inline>
                      </fo:block>
					  <fo:block font-family="Times" font-size="9pt" space-before="10pt" language="PL" text-align="justify">
                        <fo:inline font-size="9pt">
                          <fo:leader leader-length="0pt" />Dane dotycz�ce przebiegu studi�w b�d� przetwarzane zgodnie z terminami wskazanymi w przepisach o szkolnictwie wy�szym i powinny by� archiwizowane przez 50 lat. Dane dotycz�ce wykonania umowy stanowi�cej podstaw� do �wiadczenia us�ug edukacyjnych b�d� archiwizowane przez 10 lat (z uwagi na og�lne i szczeg�lne terminy przedawnienia roszcze�). Dane przetwarzane w celach marketingowych b�d� przetwarzane do czasu, w kt�rym ustanie cel dla kt�rego zosta�y zebrane lub sprzeciwu, co do przetwarzania danych osobowych osoby, kt�rej dane dotycz�, w zale�no�ci co nast�pi pierwsze. Ka�dy ma prawo ��dania od Administratora dost�pu do danych osobowych, ich sprostowania, usuni�cia, ograniczenia przetwarzania, a tak�e prawo do przenoszenia danych i wniesienia sprzeciwu wobec przetwarzania. Ka�dy ma prawo do cofni�cia zgody na przetwarzanie danych osobowych, kt�r� udzieli� Administratorowi. Wycofanie zgody nie ma wp�ywu na zgodno�� z prawem przetwarzania, kt�rego dokonano przed jej cofni�ciem. Ka�dy ma prawo do wniesienia skargi do Organu Nadzorczego, kt�rym od 25 maja 2018 r. jest Prezes Urz�du Ochrony Danych Osobowych.
                        </fo:inline>
                      </fo:block>
					         <fo:block font-family="Times" font-size="9pt" language="PL" text-align="justify" space-before="10pt" >
                        <fo:inline font-size="9pt">
                          <fo:leader leader-length="0pt" />Wszelkie uprawnienia osoby, kt�rej dane dotycz� mo�na wykona� przez z�o�enie o�wiadczenia wykorzystuj�c dane kontaktowe Administratora lub Inspektora Danych Osobowych wskazane we wst�pie.
                        </fo:inline>
                      </fo:block>
					     <fo:block font-family="Times" font-size="9pt" language="PL" text-align="justify" space-before="10pt">
                        <fo:inline font-size="9pt">
                          <fo:leader leader-length="0pt" />Wyra�one poni�ej zgody obowi�zuj� tak�e po zako�czeniu kszta�cenia obj�tego umow�.
                        </fo:inline>
                      </fo:block>
                      <fo:block font-family="Times" font-size="9pt" language="PL" text-align="justify">
                        <fo:inline font-size="9pt">
                          <fo:leader leader-length="0pt" />
                        </fo:inline>
                      </fo:block>
                    		<fo:block text-align="justify" font-size="9pt" space-before="10pt">
						[<xsl:value-of select="ZgodaRekrutacjaIStudia"/>] Wyra�am zgod� na przetwarzanie moich danych osobowych oraz wszystkich przekazanych przeze mnie informacji dla potrzeb rejestracji, post�powania rekrutacyjnego, a nast�pnie dokumentowania przebiegu studi�w, zgodnie z obowi�zuj�cymi przepisami prawa, a tak�e na potrzeby przysz�ych rekrutacji prowadzonych przez Akademi� Humanistyczno-Ekonomiczn� w �odzi.
					</fo:block>
                      <fo:block font-family="Times" font-size="9pt" language="PL" text-align="justify">
                      </fo:block>
                      <fo:block text-align="justify" font-size="9pt" space-before="10pt">
						[<xsl:value-of select="ZgodaPocztaISMS"/>] Wyra�am zgod� na otrzymywanie wszelkich informacji zwi�zanych z procesem elektronicznej rejestracji, rekrutacji oraz realizacji us�ug edukacyjnych przez Akademi� Humanistyczno-Ekonomiczn� w �odzi za po�rednictwem poczty elektronicznej, telefonu lub wiadomo�ci SMS (Short Message Service) zgodnie z ustaw� z dnia 18 lipca 2002 r. o �wiadczeniu us�ug drog� elektroniczn� (Dz.U.2013.1422 j.t.) oraz ustaw� z dnia 16 lipca 2004 r. Prawo telekomunikacyjne (Dz.U.2016.1489 j.t.)
					</fo:block>
					<fo:block text-align="justify" font-size="9pt" space-before="10pt">
						[<xsl:value-of select="ZgodaMarketingIPromocje"/>] Wyra�am zgod� na przetwarzanie moich danych osobowych w celach marketingowych i promocyjnych oraz otrzymywanie informacji handlowych (w szczeg�lno�ci oferty edukacyjnej) za po�rednictwem poczty elektronicznej, telefonu lub wiadomo�ci SMS (Short Message Service) zgodnie z ustaw� z dnia 18 lipca 2002 r. o �wiadczeniu us�ug drog� elektroniczn� (Dz.U.2013.1422 j.t.) oraz ustaw� z dnia 16 lipca 2004 r. Prawo telekomunikacyjne (Dz.U.2016.1489 j.t.) dotycz�cych Akademii Humanistyczno-Ekonomicznej w �odzi oraz Instytutu Post�powania Tw�rczego sp. z o.o. w �odzi (ul. Rewolucji 1905 r. nr 52, 90-213 ��d�).
					</fo:block>
				<fo:block text-align="justify" font-size="9pt" space-before="10pt">
						[<xsl:value-of select="ZgodaWizerunek"/>] Wyra�am na rzecz Akademii Humanistyczno-Ekonomicznej w �odzi zgod� na nieodp�atne wykorzystywanie przez AHE w �odzi lub przez inne osoby dzia�aj�ce na jej zlecenie mojego wizerunku oraz na utrwalanie, zapisywanie i zwielokrotnianie tego wizerunku wszelkimi dost�pnymi aktualnie technikami i metodami, rozpowszechnianie oraz publikowanie w materia�ach marketingowych s�u��cych reklamie lub promocji AHE w �odzi i wszelkiego rodzaju prowadzonej przez ni� dzia�alno�ci naukowej, edukacyjnej i dydaktycznej, na terenie Polski i za granic�, przez okres 5 lat od z�o�enia niniejszego o�wiadczenia (w tym poprzez zamieszczenie w postaci cyfrowej w sieci Internet, a tak�e w formie zdj��, film�w, plakat�w, folder�w reklamowych, ulotek, kalendarzy, banner�w, prezentacji multimedialnych,  og�osze�, reklam itp., emitowanych za pomoc� r�nych no�nik�w, w szczeg�lno�ci wydruk�w papierowych oraz sieci Internet i telewizji) - korzystanie z utrwalonego wizerunku we wskazanych wy�ej celach mo�e nast�pi� bez udost�pnienia mi ostatecznej wersji materia��w z moim wizerunkiem przez AHE w �odzi.
				</fo:block>
                      <fo:block font-family="Arial" font-size="9pt" language="PL" end-indent="18pt" text-align="justify">
                        <fo:leader />
                      </fo:block>
                      <fo:block font-family="Arial" font-size="9pt" language="PL" end-indent="18pt" text-align="justify">
                        <fo:leader />
                      </fo:block>
                      <fo:block font-family="Arial" font-size="9pt" language="PL" end-indent="18pt" text-align="justify">
                        <fo:leader />
                      </fo:block>
                      <fo:block id="BiezacaData" />
                      <fo:block font-family="Arial" font-size="11pt" language="PL" line-height="1.7205" text-align="justify" font-weight="bold">
                        <fo:inline font-family="Arial" font-weight="bold" font-size="10pt">
                          <fo:leader leader-length="0pt" />
                          <xsl:value-of select="$dataWygenerowania"/>
                        </fo:inline>
                      </fo:block>
                      <fo:block font-family="Arial" font-size="10pt" language="PL" line-height="1.7205" text-align="justify">
                        <fo:inline font-family="Arial" font-size="10pt">
                          <fo:leader leader-length="0pt" />....................................
                        </fo:inline>
                        <fo:inline font-family="Arial" font-size="10pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-size="10pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-size="10pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-size="10pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-size="10pt">
                          <fo:leader leader-pattern="space"  /><fo:leader leader-length="0pt" />            ...................................................
                        </fo:inline>
                      </fo:block>
                      <fo:block font-family="Arial" font-size="9pt" language="PL" line-height="1.7205" text-align="justify">
                        <fo:inline font-family="Arial" font-style="italic" font-size="9pt">
                          <fo:leader leader-length="0pt" />   ��d�, data
                        </fo:inline>
                        <fo:inline font-family="Arial" font-style="italic" font-size="9pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-style="italic" font-size="9pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-style="italic" font-size="9pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-style="italic" font-size="9pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-style="italic" font-size="9pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-style="italic" font-size="9pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-style="italic" font-size="9pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-style="italic" font-size="9pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-style="italic" font-size="9pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-style="italic" font-size="9pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-style="italic" font-size="9pt">
                          <fo:leader leader-pattern="space"  />
                        </fo:inline>
                        <fo:inline font-family="Arial" font-style="italic" font-size="9pt" >
                          <fo:leader leader-pattern="space"  /><fo:leader leader-length="0" />Czytelny podpis kandydata
                        </fo:inline>
                      </fo:block>
                     
                    
                  </fo:block>
                
              </fo:block>
            
          </fo:block>
        
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>
</xsl:stylesheet>'
 WHERE Opis like 'Kwestionaruisz podyplomowe'
GO


