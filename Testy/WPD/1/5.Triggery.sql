----------wdro1------2018_01_24_04_ESS_WPD_USR_Profil_trigger
----------wdro2------2018_01_24_08_ESS_WPD_USR_Rola_trigger
----------wdro3------2018_01_24_11_ESS_WPD_USR_ProfilToRola_trigger
----------wdro4------2018_01_24_14_ESS_WPD_USR_HistoriaLogowan_trigger
----------wdro5------2018_01_24_17_ESS_WPD_USR_HistoriaHasla_trigger
----------wdro6------2018_01_24_20_ESS_WPD_USR_HistoriaAkcji_trigger



--------------------------------------------------------wdro1-----------------------------------------------------


-- ******************* -- 2018_01_24_04_ESS_WPD_USR_Profil_trigger -- ******************* --



DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------USR_Profil-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_Profil_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER USR_Profil_INSERT
')

EXEC('
CREATE TRIGGER USR_Profil_INSERT ON ['+@BazaDanych+'].[dbo].[USR_Profil] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_Profil'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_Profil]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_Profil]
  ,[Org_Email]
  ,[Org_Login]
  ,[Org_HasloPlain]
  ,[Org_HasloHash]
  ,[Org_ZiarnoHasla]
  ,[Org_Imie]
  ,[Org_DrugieImie]
  ,[Org_NickName]
  ,[Org_Nazwisko]
  ,[Org_PrefiksTelefonu]
  ,[Org_Telefon]
  ,[Org_CzyHasloJednorazowe]
  ,[Org_CzyKontoAktywne]
  ,[Org_DataOstatniejZmianyHasla]
  ,[Org_Token]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDUSR_Profil
 ,ins.Email
 ,ins.Login
 ,ins.HasloPlain
 ,ins.HasloHash
 ,ins.ZiarnoHasla
 ,ins.Imie
 ,ins.DrugieImie
 ,ins.NickName
 ,ins.Nazwisko
 ,ins.PrefiksTelefonu
 ,ins.Telefon
 ,ins.CzyHasloJednorazowe
 ,ins.CzyKontoAktywne
 ,ins.DataOstatniejZmianyHasla
 ,ins.Token
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_Profil_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER USR_Profil_UPDATE
')

EXEC('
CREATE TRIGGER USR_Profil_UPDATE ON ['+@BazaDanych+'].[dbo].[USR_Profil] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_Profil'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_Profil]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_Profil]
  ,[Org_Email]
  ,[Org_Login]
  ,[Org_HasloPlain]
  ,[Org_HasloHash]
  ,[Org_ZiarnoHasla]
  ,[Org_Imie]
  ,[Org_DrugieImie]
  ,[Org_NickName]
  ,[Org_Nazwisko]
  ,[Org_PrefiksTelefonu]
  ,[Org_Telefon]
  ,[Org_CzyHasloJednorazowe]
  ,[Org_CzyKontoAktywne]
  ,[Org_DataOstatniejZmianyHasla]
  ,[Org_Token]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDUSR_Profil
 ,ins.Email
 ,ins.Login
 ,ins.HasloPlain
 ,ins.HasloHash
 ,ins.ZiarnoHasla
 ,ins.Imie
 ,ins.DrugieImie
 ,ins.NickName
 ,ins.Nazwisko
 ,ins.PrefiksTelefonu
 ,ins.Telefon
 ,ins.CzyHasloJednorazowe
 ,ins.CzyKontoAktywne
 ,ins.DataOstatniejZmianyHasla
 ,ins.Token
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_Profil_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER USR_Profil_DELETE
')

EXEC('
CREATE TRIGGER USR_Profil_DELETE ON ['+@BazaDanych+'].[dbo].[USR_Profil] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_Profil'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_Profil]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_Profil]
  ,[Org_Email]
  ,[Org_Login]
  ,[Org_HasloPlain]
  ,[Org_HasloHash]
  ,[Org_ZiarnoHasla]
  ,[Org_Imie]
  ,[Org_DrugieImie]
  ,[Org_NickName]
  ,[Org_Nazwisko]
  ,[Org_PrefiksTelefonu]
  ,[Org_Telefon]
  ,[Org_CzyHasloJednorazowe]
  ,[Org_CzyKontoAktywne]
  ,[Org_DataOstatniejZmianyHasla]
  ,[Org_Token]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDUSR_Profil
 ,del.Email
 ,del.Login
 ,del.HasloPlain
 ,del.HasloHash
 ,del.ZiarnoHasla
 ,del.Imie
 ,del.DrugieImie
 ,del.NickName
 ,del.Nazwisko
 ,del.PrefiksTelefonu
 ,del.Telefon
 ,del.CzyHasloJednorazowe
 ,del.CzyKontoAktywne
 ,del.DataOstatniejZmianyHasla
 ,del.Token
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'USR_Profil')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('USR_Profil', 1, 0,GetDate())

GO



--------------------------------------------------------wdro2-----------------------------------------------------


-- ******************* -- 2018_01_24_08_ESS_WPD_USR_Rola_trigger -- ******************* --


DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------USR_Rola-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_Rola_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER USR_Rola_INSERT
')

EXEC('
CREATE TRIGGER USR_Rola_INSERT ON ['+@BazaDanych+'].[dbo].[USR_Rola] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_Rola'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_Rola]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_Rola]
  ,[Org_Nazwa]
  ,[Org_Podmiot]
  ,[Org_Klucz]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDUSR_Rola
 ,ins.Nazwa
 ,ins.Podmiot
 ,ins.Klucz
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_Rola_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER USR_Rola_UPDATE
')

EXEC('
CREATE TRIGGER USR_Rola_UPDATE ON ['+@BazaDanych+'].[dbo].[USR_Rola] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_Rola'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_Rola]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_Rola]
  ,[Org_Nazwa]
  ,[Org_Podmiot]
  ,[Org_Klucz]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDUSR_Rola
 ,ins.Nazwa
 ,ins.Podmiot
 ,ins.Klucz
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_Rola_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER USR_Rola_DELETE
')

EXEC('
CREATE TRIGGER USR_Rola_DELETE ON ['+@BazaDanych+'].[dbo].[USR_Rola] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_Rola'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_Rola]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_Rola]
  ,[Org_Nazwa]
  ,[Org_Podmiot]
  ,[Org_Klucz]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDUSR_Rola
 ,del.Nazwa
 ,del.Podmiot
 ,del.Klucz
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'USR_Rola')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('USR_Rola', 1, 0,GetDate())

GO



--------------------------------------------------------wdro3-----------------------------------------------------


-- ******************* -- 2018_01_24_11_ESS_WPD_USR_ProfilToRola_trigger -- ******************* --



DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------USR_ProfilToRola-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_ProfilToRola_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER USR_ProfilToRola_INSERT
')

EXEC('
CREATE TRIGGER USR_ProfilToRola_INSERT ON ['+@BazaDanych+'].[dbo].[USR_ProfilToRola] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_ProfilToRola'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_ProfilToRola]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_ProfilToRola]
  ,[Org_USR_ProfilID]
  ,[Org_USR_RolaID]
  ,[Org_KluczZdalny]
  ,[Org_Aktywny]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDUSR_ProfilToRola
 ,ins.USR_ProfilID
 ,ins.USR_RolaID
 ,ins.KluczZdalny
 ,ins.Aktywny
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_ProfilToRola_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER USR_ProfilToRola_UPDATE
')

EXEC('
CREATE TRIGGER USR_ProfilToRola_UPDATE ON ['+@BazaDanych+'].[dbo].[USR_ProfilToRola] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_ProfilToRola'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_ProfilToRola]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_ProfilToRola]
  ,[Org_USR_ProfilID]
  ,[Org_USR_RolaID]
  ,[Org_KluczZdalny]
  ,[Org_Aktywny]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDUSR_ProfilToRola
 ,ins.USR_ProfilID
 ,ins.USR_RolaID
 ,ins.KluczZdalny
 ,ins.Aktywny
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_ProfilToRola_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER USR_ProfilToRola_DELETE
')

EXEC('
CREATE TRIGGER USR_ProfilToRola_DELETE ON ['+@BazaDanych+'].[dbo].[USR_ProfilToRola] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_ProfilToRola'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_ProfilToRola]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_ProfilToRola]
  ,[Org_USR_ProfilID]
  ,[Org_USR_RolaID]
  ,[Org_KluczZdalny]
  ,[Org_Aktywny]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDUSR_ProfilToRola
 ,del.USR_ProfilID
 ,del.USR_RolaID
 ,del.KluczZdalny
 ,del.Aktywny
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'USR_ProfilToRola')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('USR_ProfilToRola', 1, 0,GetDate())

GO



--------------------------------------------------------wdro4-----------------------------------------------------


-- ******************* -- 2018_01_24_14_ESS_WPD_USR_HistoriaLogowan_trigger -- ******************* --




DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------USR_HistoriaLogowan-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_HistoriaLogowan_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER USR_HistoriaLogowan_INSERT
')

EXEC('
CREATE TRIGGER USR_HistoriaLogowan_INSERT ON ['+@BazaDanych+'].[dbo].[USR_HistoriaLogowan] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_HistoriaLogowan'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_HistoriaLogowan]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_HistoriaLogowan]
  ,[Org_Login]
  ,[Org_AdresZdalny]
  ,[Org_Powodzenie]
  ,[Org_DataAkcji]
  ,[Org_UserAgent]
  ,[Org_SessionToken]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DadaModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDUSR_HistoriaLogowan
 ,ins.Login
 ,ins.AdresZdalny
 ,ins.Powodzenie
 ,ins.DataAkcji
 ,ins.UserAgent
 ,ins.SessionToken
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DadaModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_HistoriaLogowan_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER USR_HistoriaLogowan_UPDATE
')

EXEC('
CREATE TRIGGER USR_HistoriaLogowan_UPDATE ON ['+@BazaDanych+'].[dbo].[USR_HistoriaLogowan] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_HistoriaLogowan'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_HistoriaLogowan]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_HistoriaLogowan]
  ,[Org_Login]
  ,[Org_AdresZdalny]
  ,[Org_Powodzenie]
  ,[Org_DataAkcji]
  ,[Org_UserAgent]
  ,[Org_SessionToken]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DadaModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDUSR_HistoriaLogowan
 ,ins.Login
 ,ins.AdresZdalny
 ,ins.Powodzenie
 ,ins.DataAkcji
 ,ins.UserAgent
 ,ins.SessionToken
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DadaModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_HistoriaLogowan_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER USR_HistoriaLogowan_DELETE
')

EXEC('
CREATE TRIGGER USR_HistoriaLogowan_DELETE ON ['+@BazaDanych+'].[dbo].[USR_HistoriaLogowan] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_HistoriaLogowan'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_HistoriaLogowan]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_HistoriaLogowan]
  ,[Org_Login]
  ,[Org_AdresZdalny]
  ,[Org_Powodzenie]
  ,[Org_DataAkcji]
  ,[Org_UserAgent]
  ,[Org_SessionToken]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DadaModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDUSR_HistoriaLogowan
 ,del.Login
 ,del.AdresZdalny
 ,del.Powodzenie
 ,del.DataAkcji
 ,del.UserAgent
 ,del.SessionToken
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DadaModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'USR_HistoriaLogowan')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('USR_HistoriaLogowan', 1, 0,GetDate())

GO



--------------------------------------------------------wdro5-----------------------------------------------------


-- ******************* -- 2018_01_24_17_ESS_WPD_USR_HistoriaHasla_trigger -- ******************* --



DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------USR_HistoriaHasla-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_HistoriaHasla_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER USR_HistoriaHasla_INSERT
')

EXEC('
CREATE TRIGGER USR_HistoriaHasla_INSERT ON ['+@BazaDanych+'].[dbo].[USR_HistoriaHasla] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_HistoriaHasla'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_HistoriaHasla]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_HistoriaHasla]
  ,[Org_USR_ProfilID]
  ,[Org_HasloPlain]
  ,[Org_HasloHash]
  ,[Org_ZiarnoHasla]
  ,[Org_IP]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDUSR_HistoriaHasla
 ,ins.USR_ProfilID
 ,ins.HasloPlain
 ,ins.HasloHash
 ,ins.ZiarnoHasla
 ,ins.IP
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_HistoriaHasla_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER USR_HistoriaHasla_UPDATE
')

EXEC('
CREATE TRIGGER USR_HistoriaHasla_UPDATE ON ['+@BazaDanych+'].[dbo].[USR_HistoriaHasla] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_HistoriaHasla'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_HistoriaHasla]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_HistoriaHasla]
  ,[Org_USR_ProfilID]
  ,[Org_HasloPlain]
  ,[Org_HasloHash]
  ,[Org_ZiarnoHasla]
  ,[Org_IP]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDUSR_HistoriaHasla
 ,ins.USR_ProfilID
 ,ins.HasloPlain
 ,ins.HasloHash
 ,ins.ZiarnoHasla
 ,ins.IP
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_HistoriaHasla_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER USR_HistoriaHasla_DELETE
')

EXEC('
CREATE TRIGGER USR_HistoriaHasla_DELETE ON ['+@BazaDanych+'].[dbo].[USR_HistoriaHasla] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_HistoriaHasla'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_HistoriaHasla]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_HistoriaHasla]
  ,[Org_USR_ProfilID]
  ,[Org_HasloPlain]
  ,[Org_HasloHash]
  ,[Org_ZiarnoHasla]
  ,[Org_IP]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDUSR_HistoriaHasla
 ,del.USR_ProfilID
 ,del.HasloPlain
 ,del.HasloHash
 ,del.ZiarnoHasla
 ,del.IP
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'USR_HistoriaHasla')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('USR_HistoriaHasla', 1, 0,GetDate())

GO



--------------------------------------------------------wdro6-----------------------------------------------------


-- ******************* -- 2018_01_24_20_ESS_WPD_USR_HistoriaAkcji_trigger -- ******************* --



DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------USR_HistoriaAkcji-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_HistoriaAkcji_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER USR_HistoriaAkcji_INSERT
')

EXEC('
CREATE TRIGGER USR_HistoriaAkcji_INSERT ON ['+@BazaDanych+'].[dbo].[USR_HistoriaAkcji] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_HistoriaAkcji'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_HistoriaAkcji]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_HistoriaAkcji]
  ,[Org_Uri]
  ,[Org_USR_ProfilID]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDUSR_HistoriaAkcji
 ,ins.Uri
 ,ins.USR_ProfilID
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_HistoriaAkcji_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER USR_HistoriaAkcji_UPDATE
')

EXEC('
CREATE TRIGGER USR_HistoriaAkcji_UPDATE ON ['+@BazaDanych+'].[dbo].[USR_HistoriaAkcji] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_HistoriaAkcji'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_HistoriaAkcji]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_HistoriaAkcji]
  ,[Org_Uri]
  ,[Org_USR_ProfilID]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDUSR_HistoriaAkcji
 ,ins.Uri
 ,ins.USR_ProfilID
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_HistoriaAkcji_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER USR_HistoriaAkcji_DELETE
')

EXEC('
CREATE TRIGGER USR_HistoriaAkcji_DELETE ON ['+@BazaDanych+'].[dbo].[USR_HistoriaAkcji] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_HistoriaAkcji'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_HistoriaAkcji]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_HistoriaAkcji]
  ,[Org_Uri]
  ,[Org_USR_ProfilID]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDUSR_HistoriaAkcji
 ,del.Uri
 ,del.USR_ProfilID
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'USR_HistoriaAkcji')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('USR_HistoriaAkcji', 1, 0,GetDate())

GO

