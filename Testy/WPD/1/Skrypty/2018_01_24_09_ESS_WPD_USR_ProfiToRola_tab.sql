IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__USR_Profi__Aktyw__40BD0E90]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_ProfilToRola]'))
ALTER TABLE [dbo].[USR_ProfilToRola] DROP CONSTRAINT [DF__USR_Profi__Aktyw__40BD0E90]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__USR_Profi__USR_P__6B1D5CB0]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_ProfilToRola]'))
ALTER TABLE [dbo].[USR_ProfilToRola] DROP CONSTRAINT [FK__USR_Profi__USR_P__6B1D5CB0]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__USR_Profi__USR_R__6C1180E9]') AND parent_object_id = OBJECT_ID(N'[dbo].[USR_ProfilToRola]'))
ALTER TABLE [dbo].[USR_ProfilToRola] DROP CONSTRAINT [FK__USR_Profi__USR_R__6C1180E9]

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[USR_ProfilToRola]') AND name = N'PK_USR_ProfilToRola')
ALTER TABLE [dbo].[USR_ProfilToRola] DROP CONSTRAINT [PK_USR_ProfilToRola]



--------------------------------------
----------------USR_ProfilToRola---------------
--------------------------------------

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[USR_ProfilToRola]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
drop table [dbo].[USR_ProfilToRola]
GO

CREATE TABLE [dbo].[USR_ProfilToRola] (
   [IDUSR_ProfilToRola] [int]  IDENTITY(1,1) NOT NULL
  ,[USR_ProfilID] [int]  NOT NULL
  ,[USR_RolaID] [int]  NOT NULL
  ,[KluczZdalny] [int]  NULL
  ,[Aktywny] [bit]  NOT NULL
  ,[Dodal] [int]  NOT NULL
  ,[DataDodania] [datetime]  NOT NULL
  ,[Zmodyfikowal] [int]  NULL
  ,[DataModyfikacji] [datetime]  NULL
) ON [PRIMARY]

GO



ALTER TABLE [dbo].[USR_ProfilToRola] ADD  CONSTRAINT [PK_USR_ProfilToRola] PRIMARY KEY CLUSTERED( IDUSR_ProfilToRola ) ON [PRIMARY]

ALTER TABLE [dbo].[USR_ProfilToRola] WITH NOCHECK ADD CONSTRAINT [FK__USR_Profi__USR_R__6C1180E9] FOREIGN KEY([USR_RolaID]) REFERENCES [dbo].[USR_Rola] ([IDUSR_Rola])

ALTER TABLE [dbo].[USR_ProfilToRola] WITH NOCHECK ADD CONSTRAINT [FK__USR_Profi__USR_P__6B1D5CB0] FOREIGN KEY([USR_ProfilID]) REFERENCES [dbo].[USR_Profil] ([IDUSR_Profil])

ALTER TABLE [dbo].[USR_ProfilToRola] ADD  CONSTRAINT [DF__USR_Profi__Aktyw__40BD0E90]  DEFAULT ((1)) FOR [Aktywny]
GO
