DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------USR_Profil-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_Profil_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER USR_Profil_INSERT
')

EXEC('
CREATE TRIGGER USR_Profil_INSERT ON ['+@BazaDanych+'].[dbo].[USR_Profil] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_Profil'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_Profil]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_Profil]
  ,[Org_Email]
  ,[Org_Login]
  ,[Org_HasloPlain]
  ,[Org_HasloHash]
  ,[Org_ZiarnoHasla]
  ,[Org_Imie]
  ,[Org_DrugieImie]
  ,[Org_NickName]
  ,[Org_Nazwisko]
  ,[Org_PrefiksTelefonu]
  ,[Org_Telefon]
  ,[Org_CzyHasloJednorazowe]
  ,[Org_CzyKontoAktywne]
  ,[Org_DataOstatniejZmianyHasla]
  ,[Org_Token]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDUSR_Profil
 ,ins.Email
 ,ins.Login
 ,ins.HasloPlain
 ,ins.HasloHash
 ,ins.ZiarnoHasla
 ,ins.Imie
 ,ins.DrugieImie
 ,ins.NickName
 ,ins.Nazwisko
 ,ins.PrefiksTelefonu
 ,ins.Telefon
 ,ins.CzyHasloJednorazowe
 ,ins.CzyKontoAktywne
 ,ins.DataOstatniejZmianyHasla
 ,ins.Token
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_Profil_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER USR_Profil_UPDATE
')

EXEC('
CREATE TRIGGER USR_Profil_UPDATE ON ['+@BazaDanych+'].[dbo].[USR_Profil] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_Profil'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_Profil]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_Profil]
  ,[Org_Email]
  ,[Org_Login]
  ,[Org_HasloPlain]
  ,[Org_HasloHash]
  ,[Org_ZiarnoHasla]
  ,[Org_Imie]
  ,[Org_DrugieImie]
  ,[Org_NickName]
  ,[Org_Nazwisko]
  ,[Org_PrefiksTelefonu]
  ,[Org_Telefon]
  ,[Org_CzyHasloJednorazowe]
  ,[Org_CzyKontoAktywne]
  ,[Org_DataOstatniejZmianyHasla]
  ,[Org_Token]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDUSR_Profil
 ,ins.Email
 ,ins.Login
 ,ins.HasloPlain
 ,ins.HasloHash
 ,ins.ZiarnoHasla
 ,ins.Imie
 ,ins.DrugieImie
 ,ins.NickName
 ,ins.Nazwisko
 ,ins.PrefiksTelefonu
 ,ins.Telefon
 ,ins.CzyHasloJednorazowe
 ,ins.CzyKontoAktywne
 ,ins.DataOstatniejZmianyHasla
 ,ins.Token
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''USR_Profil_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER USR_Profil_DELETE
')

EXEC('
CREATE TRIGGER USR_Profil_DELETE ON ['+@BazaDanych+'].[dbo].[USR_Profil] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''USR_Profil'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_USR_Profil]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDUSR_Profil]
  ,[Org_Email]
  ,[Org_Login]
  ,[Org_HasloPlain]
  ,[Org_HasloHash]
  ,[Org_ZiarnoHasla]
  ,[Org_Imie]
  ,[Org_DrugieImie]
  ,[Org_NickName]
  ,[Org_Nazwisko]
  ,[Org_PrefiksTelefonu]
  ,[Org_Telefon]
  ,[Org_CzyHasloJednorazowe]
  ,[Org_CzyKontoAktywne]
  ,[Org_DataOstatniejZmianyHasla]
  ,[Org_Token]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDUSR_Profil
 ,del.Email
 ,del.Login
 ,del.HasloPlain
 ,del.HasloHash
 ,del.ZiarnoHasla
 ,del.Imie
 ,del.DrugieImie
 ,del.NickName
 ,del.Nazwisko
 ,del.PrefiksTelefonu
 ,del.Telefon
 ,del.CzyHasloJednorazowe
 ,del.CzyKontoAktywne
 ,del.DataOstatniejZmianyHasla
 ,del.Token
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'USR_Profil')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('USR_Profil', 1, 0,GetDate())

GO