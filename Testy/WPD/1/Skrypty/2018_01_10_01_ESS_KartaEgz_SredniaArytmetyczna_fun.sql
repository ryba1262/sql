
/****** Object:  UserDefinedFunction [dbo].[KartaEgzSredniaArytmetyczna]    Script Date: 01/10/2018 09:55:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[KartaEgzSredniaArytmetyczna]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[KartaEgzSredniaArytmetyczna]
GO

SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO

-- =============================================
-- Author:		Katarzyna Goc�awska
-- Create date: 2015-08-18
-- Description:	Funkcja do obliczania �redniej arytmetycznej semestralnej, rocznej i �redniej ze studi�w
-- =============================================

CREATE FUNCTION [dbo].[KartaEgzSredniaArytmetyczna] (@IndeksID int, @RokAkad int, @SemestrID int, @SemestrStudiow int, @CzyRoczna bit, @CzyPoczatkowa bit, @CzyDecyzja bit, @CzyKartaPoczatkowa bit)
RETURNS float
AS
BEGIN

  DECLARE @Srednia float, @KERokAkadOd int, @KESemestrIDOd int

  select @KERokAkadOd = Wartosc from UstawieniaProgramu where Nazwa = 'KERokAkadOd'
  select @KESemestrIDOd = Wartosc from UstawieniaProgramu where Nazwa = 'KESemestrOdID'
  
  IF(@CzyKartaPoczatkowa = 1)
  BEGIN
      SET @Srednia = NULL
      RETURN @Srednia
  END  

  DECLARE @CzyZatwierdzona bit
  DECLARE @RozliczonaWarunkowo int

  IF(@RokAkad is not NULL) -- �rednia sem lub roczna
   BEGIN
       SELECT @CzyZatwierdzona = Zatwierdzona, @RozliczonaWarunkowo = RozliczonaWarunkowo
       FROM KartaEgz WHERE RokAkad = @RokAkad AND SemestrID = @SemestrID AND SemestrStudiow = @SemestrStudiow AND IndeksID = @IndeksID
   END
  ELSE
   BEGIN  -- srednia ze studi�w
       SELECT @CzyZatwierdzona = Zatwierdzona, @RozliczonaWarunkowo = RozliczonaWarunkowo
       FROM KartaEgz 
       LEFT JOIN OkresRozliczeniowyPoz orp on SemestrID = IDOkresRozliczeniowyPoz
       WHERE IndeksID = @IndeksID and (RokAkad*10+orp.LP) = (SELECT Max(RokAkad*10+okp.LP) FROM KartaEgz LEFT JOIN OkresRozliczeniowyPoz okp on SemestrID = IDOkresRozliczeniowyPoz WHERE IndeksID = @IndeksID)
   END


  DECLARE @RokOd int , @RokDo int, @WhrRokAkad int, @WhrSemestrID int
  DECLARE @SemestrOd int, @SemestrDo int
  DECLARE @SemS int  -- semestr studiow w przypadku sredniej semestralnej ustawiony, w innych przypadkach NULL
  SET @SemS = NULL
  
  	-- ostatnie wyzerowanie historii rozlicze�
	select  top (1)
			@WhrRokAkad = RokAkademicki,
			@WhrSemestrID = SemestrAkadID
	from HistoriaStudenta
	where ZdarzenieID = 32
	and Anulowany = 0
	and IndeksID = @IndeksID
	and ((@RokAkad is null and @SemestrID is null) or (RokAkademicki * 100 + SemestrAkadID <= @RokAkad * 100 + @SemestrID))
	order by RokAkademicki * 100 + SemestrAkadID desc 

  IF(@RozliczonaWarunkowo = 0 AND @CzyZatwierdzona = 1)
    BEGIN  -- rejestracja pe�na - karta zatwierdzona
     
       -- Wyznaczenie zakresow pobierania przedmiotow
       IF(@CzyRoczna = 0 AND @RokAkad is not NULL) --srednia semestralna
         BEGIN
            SET @RokDo = @RokAkad
            SET @SemestrDo = @SemestrID
            SET @SemS = @SemestrStudiow
            
            SELECT TOP 1 @RokOd = RokAkad, @SemestrOd = SemestrID FROM KartaEgz
            LEFT JOIN OkresRozliczeniowyPoz orp on SemestrID = IDOkresRozliczeniowyPoz            
            WHERE SemestrStudiow = @SemestrStudiow AND IndeksID = @IndeksID
            and RokAkad * 100 + SemestrID >= coalesce(@WhrRokAkad, 0) * 100 + coalesce(@WhrSemestrID, 0)
            ORDER BY RokAkad*10+orp.LP ASC 
         END
       ELSE IF(@CzyRoczna = 1 AND @RokAkad is not NULL)  --srednia roczna
         BEGIN
            IF(@SemestrStudiow %2=0)   
              BEGIN
                  SET @RokDo = @RokAkad
                  SET @SemestrDo = @SemestrID
                  
                  SELECT TOP 1 @RokOd = RokAkad, @SemestrOd = SemestrID FROM KartaEgz 
                  LEFT JOIN OkresRozliczeniowyPoz orp on SemestrID = IDOkresRozliczeniowyPoz
                  WHERE SemestrStudiow = (@SemestrStudiow-1) AND IndeksID = @IndeksID
                  and RokAkad * 100 + SemestrID >= coalesce(@WhrRokAkad, 0) * 100 + coalesce(@WhrSemestrID, 0)
                  ORDER BY RokAkad*10+orp.LP ASC 
                  
                  IF(((@SemestrStudiow-2)/2)+((@SemestrStudiow-2)%2) = 0) 
                  BEGIN
                     SELECT TOP 1 @RokOd = RokAkad, @SemestrOd = SemestrID FROM KartaEgz 
                     LEFT JOIN OkresRozliczeniowyPoz orp on SemestrID = IDOkresRozliczeniowyPoz
                     WHERE SemestrStudiow = (@SemestrStudiow-2) AND IndeksID = @IndeksID
                     and RokAkad * 100 + SemestrID >= coalesce(@WhrRokAkad, 0) * 100 + coalesce(@WhrSemestrID, 0)
                     ORDER BY RokAkad*10+orp.LP ASC 
                  END 
              END
            ELSE  -- srednia roczna z semestru nieparzystego = NULL
              BEGIN
                 SET @Srednia = NULL              
                 RETURN @Srednia
              END
         END
       ELSE  --srednia ze studiow
         BEGIN
            SELECT TOP 1 @RokOd = RokAkad, @SemestrOd = SemestrID FROM KartaEgz
            LEFT JOIN OkresRozliczeniowyPoz orp on SemestrID = IDOkresRozliczeniowyPoz
            WHERE IndeksID = @IndeksID
            and RokAkad * 100 + SemestrID >= coalesce(@WhrRokAkad, 0) * 100 + coalesce(@WhrSemestrID, 0)
            ORDER BY RokAkad*10+orp.LP ASC 

            SELECT TOP 1 @RokDo = RokAkad, @SemestrDo = SemestrID FROM KartaEgz 
            LEFT JOIN OkresRozliczeniowyPoz orp on SemestrID = IDOkresRozliczeniowyPoz
            WHERE IndeksID = @IndeksID
            and RokAkad * 100 + SemestrID >= coalesce(@WhrRokAkad, 0) * 100 + coalesce(@WhrSemestrID, 0)
            ORDER BY RokAkad*10+orp.LP DESC
         END  
      
      -- pobiera przedmioty z kart w zawezonym okresie
      DECLARE @TabelaPrzedmiotow TABLE (IDSemReal int ) 
      INSERT @TabelaPrzedmiotow  
      SELECT IDSemReal FROM [dbo].[KartaEgzHistoriaRozliczen] (@IndeksID, @RokOd,@SemestrOd, @RokDo, @SemestrDo,@CzyPoczatkowa, @SemS)      

      DECLARE @TabelaObliczenia TABLE(IDSemReal int, Punkty int, Ocena float, CzyZalicza bit, CzyOcena bit, NumerWiersza int, RokAkad int, SemestrID int, FormaWymiarID int, 
      CzyZaliczaNowe bit, CzyNoweKarty bit, OcenaCz float) 
      
      IF(@CzyPoczatkowa = 1)
      BEGIN
        INSERT @TabelaObliczenia
         SELECT IDSemestrRealizacji, LiczbaPunktow, SkalaOcenPoz.Wartosc,SkalaOcenPoz.CzyZalicza, SkalaOcenPoz.CzyOcena, ROW_NUMBER() over (partition by IDSemestrRealizacji order by FormaWymiarID asc), RokAkad, SemestrID, FormaWymiarID,
        case when (select COUNT(*) from KartaEgzForma 
		           left join SkalaOcenPoz on OcenaID = IDSkalaOcenPoz
		           JOIN FormaWymiar ON KartaEgzForma.FormaWymiarID = FormaWymiar.IDFormaWymiar
                   LEFT JOIN FormaZajec ON FormaWymiar.FormaZajecID = FormaZajec.IDFormaZajec
		           where KartaEgzForma.IDKartaEgzForma = kaf.IDKartaEgzForma	and CzyDrukowacNaKarcieEgz = 1 and (CzyZalicza <> 0 or CzyZalicza  is not null) and CzyPoprawkowy = 0)>0 then 1 else 0 end CzyZaliczaNowe, 
		case when RokAkad*10+SemestrID >=@KERokAkadOd*10+@KESemestrIDOd then 1 else 0 end CzyNoweKarty, skocz.Wartosc        
        FROM KartaEgzPoz
        join KartaEgzForma kaf on kaf.KartaEgzPozID = IDKartaEgzPoz
        LEFT JOIN KartaEgz ON KartaEgzID = IDKartaEgz
        LEFT JOIN SemestrRealizacji  ON SemestrRealizacjiID = IDSemestrRealizacji
        LEFT JOIN SkalaOcenPoz ON OcenaKoncowaID = IDSkalaOcenPoz
        left join SkalaOcenPoz skocz on kaf.OcenaID = skocz.IDSkalaOcenPoz
        WHERE IndeksID = @IndeksID AND Zatwierdzona = 1 /*AND CzyZalicza = 1 AND CzyOcena=1*/ AND SemestrRealizacjiID IN (SELECT IDSemReal FROM @TabelaPrzedmiotow) AND 
              ((RokAkad*10 + SemestrID) BETWEEN (@RokOd * 10 + @SemestrOd) AND (@RokDo * 10 + @SemestrDo))           
      END
      ELSE
      BEGIN
        INSERT @TabelaObliczenia
         SELECT IDSemestrRealizacji, LiczbaPunktow, SkalaOcenPoz.Wartosc,SkalaOcenPoz.CzyZalicza, SkalaOcenPoz.CzyOcena, ROW_NUMBER() over (partition by IDSemestrRealizacji order by FormaWymiarID asc), RokAkad, SemestrID, FormaWymiarID ,  
        case when (select COUNT(*) from KartaEgzForma 
		           left join SkalaOcenPoz on OcenaID = IDSkalaOcenPoz
		           JOIN FormaWymiar ON KartaEgzForma.FormaWymiarID = FormaWymiar.IDFormaWymiar
                   LEFT JOIN FormaZajec ON FormaWymiar.FormaZajecID = FormaZajec.IDFormaZajec
		           where KartaEgzForma.IDKartaEgzForma = kaf.IDKartaEgzForma and CzyDrukowacNaKarcieEgz = 1 and (CzyZalicza<> 0 or CzyZalicza  is not null) and CzyPoprawkowy = 0)>0 then 1 else 0 end CzyZaliczaNowe, 
		case when RokAkad*10+SemestrID >=@KERokAkadOd*10+@KESemestrIDOd then 1 else 0 end CzyNoweKarty, skocz.Wartosc     
        FROM KartaEgzPoz
        join KartaEgzForma kaf on kaf.KartaEgzPozID = IDKartaEgzPoz
        LEFT JOIN KartaEgz ON KartaEgzID = IDKartaEgz
        LEFT JOIN SemestrRealizacji  ON SemestrRealizacjiID = IDSemestrRealizacji
        LEFT JOIN SkalaOcenPoz ON OcenaKoncowaID = IDSkalaOcenPoz
        left join SkalaOcenPoz skocz on kaf.OcenaID = skocz.IDSkalaOcenPoz
        WHERE IndeksID = @IndeksID AND Zatwierdzona = 1 /*AND CzyZalicza = 1 AND CzyOcena=1*/ AND SemestrRealizacjiID IN (SELECT IDSemReal FROM @TabelaPrzedmiotow) AND 
              ((RokAkad*10 + SemestrID) BETWEEN (@RokOd * 10 + @SemestrOd) AND (@RokDo * 10 + @SemestrDo)) AND              
              (KartaPoczatkowa is NULL or KartaPoczatkowa = 0)
      END 

      IF(@CzyDecyzja = 1)
      BEGIN
         DECLARE @Korekta TABLE(ObowiazujacaLPunktow int, SemestrRealizacjiID int, KRokAkad int, KSemestrID int, Data datetime)
         INSERT @Korekta
         SELECT ObowiazujacaLPunktow,SemestrRealizacjiID,RokAkad, SemestrID, Data FROM KorektaLiczbyPunktowPoz
         JOIN KorektaLiczbyPunktow ON (KorektaLiczbyPunktowID=IDKorektaLiczbyPunktow AND KorektaLiczbyPunktow.IndeksID = @IndeksID)
         JOIN HistoriaStudenta ON (TabelaID=IDKorektaLiczbyPunktow AND ZdarzenieID=33 AND HistoriaStudenta.IndeksID = @IndeksID)
         WHERE SemestrRealizacjiID IN (SELECT IDSemReal FROM @TabelaPrzedmiotow) AND HistoriaStudenta.Anulowany = 0 
		 AND ((RokAkad*10 + SemestrID) BETWEEN (@RokOd * 10 + @SemestrOd) AND (@RokDo * 10 + @SemestrDo))

         UPDATE @TabelaObliczenia 
		 SET Punkty =
		 (
			SELECT TOP 1 ObowiazujacaLPunktow 
			FROM @Korekta 
            WHERE SemestrRealizacjiID = IDSemReal AND KRokAkad = RokAkad AND KSemestrID = SemestrID 
            ORDER BY Data DESC
		 )
         WHERE IDSemReal in (select SemestrRealizacjiID from @Korekta) 
      END

	  declare @SumaOceny float, @SumaIlosc int
	  select @SumaOceny = SUM(Ocena) from @TabelaObliczenia where CzyZalicza = 1 and CzyOcena = 1 and CzyNoweKarty = 0 and NumerWiersza = 1
	  select @SumaIlosc = Count(distinct IDSemReal) from @TabelaObliczenia where CzyZalicza = 1 and CzyOcena = 1 and CzyNoweKarty = 0

      declare @SumaOcenyN float, @SumaIloscN int
	  select @SumaOcenyN = SUM(OcenaCz) from @TabelaObliczenia where CzyNoweKarty = 1 and CzyZaliczaNowe = 1
	  select @SumaIloscN = Count(distinct FormaWymiarID) from @TabelaObliczenia where CzyNoweKarty = 1 and CzyZaliczaNowe = 1

	  set @Srednia = case when (@SumaIlosc + @SumaIloscN) > 0 then (coalesce(@SumaOceny,0)+ coalesce(@SumaOcenyN,0)) / (@SumaIlosc+ @SumaIloscN) else 0 end                
   END
  ELSE
   BEGIN
      SET @Srednia = NULL
   END

	
  RETURN ROUND(COALESCE(@Srednia,0),3)

END



GO


grant execute on dbo.KartaEgzSredniaArytmetyczna to [user]