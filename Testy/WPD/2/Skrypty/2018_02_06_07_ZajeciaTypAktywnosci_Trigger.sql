DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------ZajeciaTypUczestnictwa-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ZajeciaTypUczestnictwa_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER ZajeciaTypUczestnictwa_INSERT
')

EXEC('
CREATE TRIGGER ZajeciaTypUczestnictwa_INSERT ON ['+@BazaDanych+'].[dbo].[ZajeciaTypUczestnictwa] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ZajeciaTypUczestnictwa'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ZajeciaTypUczestnictwa]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDZajeciaTypUczestnictwa]
  ,[Org_TypUczestnictwaID]
  ,[Org_Podmiot]
  ,[Org_PodmiotID]
  ,[Org_FormaWymiarID]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDZajeciaTypUczestnictwa
 ,ins.TypUczestnictwaID
 ,ins.Podmiot
 ,ins.PodmiotID
 ,ins.FormaWymiarID
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ZajeciaTypUczestnictwa_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER ZajeciaTypUczestnictwa_UPDATE
')

EXEC('
CREATE TRIGGER ZajeciaTypUczestnictwa_UPDATE ON ['+@BazaDanych+'].[dbo].[ZajeciaTypUczestnictwa] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ZajeciaTypUczestnictwa'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ZajeciaTypUczestnictwa]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDZajeciaTypUczestnictwa]
  ,[Org_TypUczestnictwaID]
  ,[Org_Podmiot]
  ,[Org_PodmiotID]
  ,[Org_FormaWymiarID]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDZajeciaTypUczestnictwa
 ,ins.TypUczestnictwaID
 ,ins.Podmiot
 ,ins.PodmiotID
 ,ins.FormaWymiarID
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''ZajeciaTypUczestnictwa_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER ZajeciaTypUczestnictwa_DELETE
')

EXEC('
CREATE TRIGGER ZajeciaTypUczestnictwa_DELETE ON ['+@BazaDanych+'].[dbo].[ZajeciaTypUczestnictwa] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''ZajeciaTypUczestnictwa'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_ZajeciaTypUczestnictwa]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDZajeciaTypUczestnictwa]
  ,[Org_TypUczestnictwaID]
  ,[Org_Podmiot]
  ,[Org_PodmiotID]
  ,[Org_FormaWymiarID]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDZajeciaTypUczestnictwa
 ,del.TypUczestnictwaID
 ,del.Podmiot
 ,del.PodmiotID
 ,del.FormaWymiarID
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'ZajeciaTypUczestnictwa')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('ZajeciaTypUczestnictwa', 1, 0,GetDate())

