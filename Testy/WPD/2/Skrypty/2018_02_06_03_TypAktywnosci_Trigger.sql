DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr  varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych

----------------------------TypUczestnictwa-----------------------------------
-------------------------------------------------------------------------

----------------------------INSERT-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''TypUczestnictwa_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER TypUczestnictwa_INSERT
')

EXEC('
CREATE TRIGGER TypUczestnictwa_INSERT ON ['+@BazaDanych+'].[dbo].[TypUczestnictwa] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''TypUczestnictwa'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_TypUczestnictwa]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDTypUczestnictwa]
  ,[Org_NazwaTypu]
  ,[Org_Opis]
  ,[Org_CzyFormaWymiar]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
1
	,@ID
 ,ins.IDTypUczestnictwa
 ,ins.NazwaTypu
 ,ins.Opis
 ,ins.CzyFormaWymiar
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------UPDATE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''TypUczestnictwa_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER TypUczestnictwa_UPDATE
')

EXEC('
CREATE TRIGGER TypUczestnictwa_UPDATE ON ['+@BazaDanych+'].[dbo].[TypUczestnictwa] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''TypUczestnictwa'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_TypUczestnictwa]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDTypUczestnictwa]
  ,[Org_NazwaTypu]
  ,[Org_Opis]
  ,[Org_CzyFormaWymiar]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
2
	,@ID
 ,ins.IDTypUczestnictwa
 ,ins.NazwaTypu
 ,ins.Opis
 ,ins.CzyFormaWymiar
 ,ins.Dodal
 ,ins.DataDodania
 ,ins.Zmodyfikowal
 ,ins.DataModyfikacji
FROM  inserted ins 
END
END
')

----------------------------DELETE-----------------------------------
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''TypUczestnictwa_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER TypUczestnictwa_DELETE
')

EXEC('
CREATE TRIGGER TypUczestnictwa_DELETE ON ['+@BazaDanych+'].[dbo].[TypUczestnictwa] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''TypUczestnictwa'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_TypUczestnictwa]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDTypUczestnictwa]
  ,[Org_NazwaTypu]
  ,[Org_Opis]
  ,[Org_CzyFormaWymiar]
  ,[Org_Dodal]
  ,[Org_DataDodania]
  ,[Org_Zmodyfikowal]
  ,[Org_DataModyfikacji]
)
SELECT
3
	,@ID
 ,del.IDTypUczestnictwa
 ,del.NazwaTypu
 ,del.Opis
 ,del.CzyFormaWymiar
 ,del.Dodal
 ,del.DataDodania
 ,del.Zmodyfikowal
 ,del.DataModyfikacji
FROM  deleted del 
END
END
')
if not exists (select * from LOG_LogowanaTabela where Tabela = 'TypUczestnictwa')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('TypUczestnictwa', 1, 0,GetDate())

