-- =============================================
-- Author:		Agata Stelcer-Matuszczak
-- Create date: 2018-02-05
-- Description:	Dodanie nowej kolumny "Kolor" - TRIGGERY
-- =============================================

DECLARE @BazaDanych varchar(128)
DECLARE @BazaLog varchar(128)
DECLARE @ReplikacjaStr varchar(128)
SET @BazaDanych = (Select DB_NAME())
SET @BazaLog = 'LOG_'+@BazaDanych
EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''FormaZajec_INSERT''
AND 	  type = ''TR'')
DROP TRIGGER FormaZajec_INSERT
')

EXEC('
CREATE TRIGGER FormaZajec_INSERT ON ['+@BazaDanych+'].[dbo].[FormaZajec] FOR INSERT
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''FormaZajec'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_FormaZajec]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDFormaZajec]
  ,[Org_Symbol]
  ,[Org_Nazwa]
  ,[Org_Opis]
  ,[Org_Symbol6Znakow]
  ,[Org_CzyDrukowacNaKarcieEgz]
  ,[Org_Kolor]
  ,[Org_DataModyfikacji]
  ,[Org_Zmodyfikowal]
  ,[Org_DataDodania]
  ,[Org_Dodal]
)
SELECT
1
  ,@ID
 ,ins.IDFormaZajec
 ,ins.Symbol
 ,ins.Nazwa
 ,ins.Opis
 ,ins.Symbol6Znakow
 ,ins.CzyDrukowacNaKarcieEgz
 ,ins.Kolor
 ,ins.DataModyfikacji
 ,ins.Zmodyfikowal
 ,ins.DataDodania
 ,ins.Dodal
FROM  inserted ins 
END
END
')

EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''FormaZajec_UPDATE''
AND 	  type = ''TR'')
DROP TRIGGER FormaZajec_UPDATE
')

EXEC('
CREATE TRIGGER FormaZajec_UPDATE ON ['+@BazaDanych+'].[dbo].[FormaZajec] FOR UPDATE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''FormaZajec'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_FormaZajec]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDFormaZajec]
  ,[Org_Symbol]
  ,[Org_Nazwa]
  ,[Org_Opis]
  ,[Org_Symbol6Znakow]
  ,[Org_CzyDrukowacNaKarcieEgz]
  ,[Org_Kolor]
  ,[Org_DataModyfikacji]
  ,[Org_Zmodyfikowal]
  ,[Org_DataDodania]
  ,[Org_Dodal]
)
SELECT
2
  ,@ID
 ,ins.IDFormaZajec
 ,ins.Symbol
 ,ins.Nazwa
 ,ins.Opis
 ,ins.Symbol6Znakow
 ,ins.CzyDrukowacNaKarcieEgz
 ,ins.Kolor
 ,ins.DataModyfikacji
 ,ins.Zmodyfikowal
 ,ins.DataDodania
 ,ins.Dodal
FROM  inserted ins 
END
END
')

EXEC(
'IF EXISTS (SELECT name
FROM   sysobjects
WHERE  name = N''FormaZajec_DELETE''
AND 	  type = ''TR'')
DROP TRIGGER FormaZajec_DELETE
')

EXEC('
CREATE TRIGGER FormaZajec_DELETE ON ['+@BazaDanych+'].[dbo].[FormaZajec] FOR DELETE
AS 
BEGIN
IF (SELECT CzyLogowana FROM ['+@BazaDanych+'].[dbo].[Log_LogowanaTabela]
WHERE Tabela=''FormaZajec'')=1 
BEGIN
	DECLARE @IDUser VARBINARY(128), @ID int
SELECT @IDUser = CONTEXT_INFO FROM master.dbo.sysprocesses WHERE spid=@@spid
 
SET @ID=CAST(SUBSTRING(@IDUser,1,4) AS int)
INSERT INTO ['+@BazaLog+'].[dbo].[LOG_FormaZajec]
(
   [Log_Operacja]
  ,[Log_UzytkownikID]
  ,[Org_IDFormaZajec]
  ,[Org_Symbol]
  ,[Org_Nazwa]
  ,[Org_Opis]
  ,[Org_Symbol6Znakow]
  ,[Org_CzyDrukowacNaKarcieEgz]
  ,[Org_Kolor]
  ,[Org_DataModyfikacji]
  ,[Org_Zmodyfikowal]
  ,[Org_DataDodania]
  ,[Org_Dodal]
)
SELECT
3
  ,@ID
 ,del.IDFormaZajec
 ,del.Symbol
 ,del.Nazwa
 ,del.Opis
 ,del.Symbol6Znakow
 ,del.CzyDrukowacNaKarcieEgz
 ,del.Kolor
 ,del.DataModyfikacji
 ,del.Zmodyfikowal
 ,del.DataDodania
 ,del.Dodal
FROM  deleted del 
END
END
')

if not exists (select * from LOG_LogowanaTabela where Tabela = 'FormaZajec')
insert into Log_LogowanaTabela(Tabela,CzyLogowana,Dodal,DataDodania) values('FormaZajec', 1, 0,GetDate())
GO

