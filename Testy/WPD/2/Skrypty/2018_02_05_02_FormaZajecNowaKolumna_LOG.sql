-- =============================================
-- Author:		Agata Stelcer-Matuszczak
-- Create date: 2018-02-05
-- Description:	Dodanie nowej kolumny "Kolor" do Logow
-- =============================================

--------------------------------------
----------------FormaZajec---------------
--------------------------------------

if not exists (select * from sys.objects where object_id = object_id(N'[dbo].[Log_FormaZajec]') and OBJECTPROPERTY(object_id, N'IsUserTable') = 1)
BEGIN

CREATE TABLE [dbo].[Log_FormaZajec] (
   [Log_FormaZajecID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDFormaZajec] [int] 
  ,[Org_Symbol] [varchar] (2)
  ,[Org_Nazwa] [varchar] (50)
  ,[Org_Opis] [varchar] (200)
  ,[Org_Symbol6Znakow] [varchar] (6)
  ,[Org_CzyDrukowacNaKarcieEgz] [bit] 
  ,[Org_Kolor] [varchar] (10)
  ,[Org_DataModyfikacji] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Dodal] [int] 
) ON [PRIMARY]
ALTER TABLE [dbo].[LOG_FormaZajec] WITH NOCHECK ADD 
CONSTRAINT [PK_LOG_FormaZajec] PRIMARY KEY  CLUSTERED
(
   [Log_FormaZajecID]
) ON [PRIMARY]

END
ELSE
BEGIN

CREATE TABLE [dbo].[Tmp_Log_FormaZajec] (
   [Log_FormaZajecID] [int] IDENTITY (1, 1) NOT NULL
  ,[Log_Operacja] [tinyint] NOT NULL
  ,[Log_UzytkownikID] [int] NOT NULL
  ,[Log_Data] [datetime] DEFAULT GETDATE()
  ,[Org_IDFormaZajec] [int] 
  ,[Org_Symbol] [varchar] (2)
  ,[Org_Nazwa] [varchar] (50)
  ,[Org_Opis] [varchar] (200)
  ,[Org_Symbol6Znakow] [varchar] (6)
  ,[Org_CzyDrukowacNaKarcieEgz] [bit] 
  ,[Org_Kolor] [varchar] (10)
  ,[Org_DataModyfikacji] [datetime] 
  ,[Org_Zmodyfikowal] [int] 
  ,[Org_DataDodania] [datetime] 
  ,[Org_Dodal] [int] 
) ON [PRIMARY]
SET IDENTITY_INSERT dbo.Tmp_LOG_FormaZajec ON

IF EXISTS (SELECT * FROM dbo.LOG_FormaZajec)
EXEC('INSERT INTO dbo.Tmp_LOG_FormaZajec (Log_FormaZajecID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDFormaZajec, Org_Symbol, Org_Nazwa, Org_Opis, Org_Symbol6Znakow, Org_CzyDrukowacNaKarcieEgz, Org_DataModyfikacji, Org_Zmodyfikowal, Org_DataDodania, Org_Dodal)
  SELECT Log_FormaZajecID, Log_Operacja, Log_UzytkownikID, Log_Data , Org_IDFormaZajec, Org_Symbol, Org_Nazwa, Org_Opis, Org_Symbol6Znakow, Org_CzyDrukowacNaKarcieEgz, Org_DataModyfikacji, Org_Zmodyfikowal, Org_DataDodania, Org_Dodal FROM dbo.Log_FormaZajec WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_LOG_FormaZajec OFF

DROP TABLE dbo.LOG_FormaZajec

EXECUTE sp_rename N'dbo.Tmp_LOG_FormaZajec', N'LOG_FormaZajec', 'OBJECT'

ALTER TABLE [dbo].[LOG_FormaZajec] WITH NOCHECK ADD
CONSTRAINT [PK_LOG_FormaZajec] PRIMARY KEY  CLUSTERED
(
   [Log_FormaZajecID]
) ON [PRIMARY]

END
GO


