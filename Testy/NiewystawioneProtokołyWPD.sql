/****** Script for SelectTopNRows command from SSMS  ******/
SELECT Imie, Nazwisko, Email 
      ,[ProtokolID], pt.Nazwa
      ,[LiczbaWszystkichPoz] as Wszystkie
      ,[LiczbaZatwierdzonychPoz] as Zatwierdzone 
      ,[LiczbaWystawionychOcen] as WystawioneOceny
      ,[DataAktualizacji]
  FROM [ESS].[dbo].[ProtokolStatystyka] prstat
  join Pracownik pr on IDPracownik = prstat.PracownikID
  join Protokol pt on prstat.ProtokolID  = IDProtokol
  where prstat.RokAkad = 2017 and prstat.SemestrID = 2 and LiczbaWszystkichPoz <> LiczbaWystawionychOcen