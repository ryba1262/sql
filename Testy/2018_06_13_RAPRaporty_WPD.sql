--------------------------------------2018_13_06_ESS_RAPRaportyWPD--------------------------------------



SET IDENTITY_INSERT RAPRaportGrupa ON
INSERT INTO [dbo].[RAPRaportGrupa]
           ([IDRAPRaportGrupa]
		   ,[RAPRaportGrupaID]
           ,[Nazwa]
           ,[Opis]
           ,[Dodal]
           ,[DataDodania]
           ,[Zmodyfikowal]
           ,[DataModyfikacji])
     VALUES
           (27
		   ,NULL
           ,'WPD'
           ,'Raporty dotycz�ce WPD.'
           ,1611
           ,GETDATE()
           ,NULL
           ,NULL)
GO

INSERT INTO [dbo].[RAPRaportGrupa]
           ([IDRAPRaportGrupa]
		   ,[RAPRaportGrupaID]
           ,[Nazwa]
           ,[Opis]
           ,[Dodal]
           ,[DataDodania]
           ,[Zmodyfikowal]
           ,[DataModyfikacji])
     VALUES
           (28
		   ,27
           ,'Raporty aktywno�ci w WPD'
           ,'Raporty aktywno�ci w WPD.'
           ,1611
           ,GETDATE()
           ,NULL
           ,NULL)
GO
SET IDENTITY_INSERT RAPRaportGrupa OFF
SET IDENTITY_INSERT RAPRaport ON
INSERT INTO [dbo].[RAPRaport]
           ([IDRAPRaport]
		   ,[RAPRaportGrupaID]
           ,[Nazwa]
           ,[Opis]
           ,[Kwerenda]
           ,[TimeOut]
           ,[LiczbaBand]
           ,[Dodal]
           ,[DataDodania]
           ,[Zmodyfikowal]
           ,[DataModyfikacji])
     VALUES
           (107
		   ,28
           ,'Historia logowa� WPD'
           ,'Historia logowa� dydaktyka w WPD'
           ,
'declare @IDUSR_Profil int
set @IDUSR_Profil = :IDUSR_Profil
select [USR_Profil].IDUSR_Profil, [USR_Profil].Imie, [USR_Profil].Nazwisko, [USR_HistoriaLogowan].[Login] 
, [USR_HistoriaLogowan].AdresZdalny as [Adres IP],
[USR_HistoriaLogowan].Powodzenie, [USR_HistoriaLogowan].DataAkcji 
from USR_HistoriaLogowan
join USR_Profil on [USR_HistoriaLogowan].[Login]  = USR_Profil.Email
where IDUSR_Profil = @IDUSR_Profil'
           ,1800
           ,5
           ,1611
           ,GETDATE()
           ,NULL
           ,NULL)
GO

INSERT INTO [dbo].[RAPRaport]
           ([IDRAPRaport]
		   ,[RAPRaportGrupaID]
           ,[Nazwa]
           ,[Opis]
           ,[Kwerenda]
           ,[TimeOut]
           ,[LiczbaBand]
           ,[Dodal]
           ,[DataDodania]
           ,[Zmodyfikowal]
           ,[DataModyfikacji])
     VALUES
           (108
		   ,28
           ,'Historia Akcji WPD'
           ,'Historia aktywno�ci dydaktyka w WPD'
           ,
'

declare @IDUSR_Profil int
set @IDUSR_Profil = :IDUSR_Profil

select USR_HistoriaAkcji.USR_ProfilID, USR_Profil.Imie, USR_Profil.Nazwisko, USR_HistoriaAkcji.Uri, USR_HistoriaAkcji.DaneWejsciowe, USR_HistoriaAkcji.DaneWyjsciowe, USR_HistoriaAkcji.DataDodania
from USR_HistoriaAkcji
join USR_Profil on USR_ProfilID  = IDUSR_Profil
where USR_ProfilID = @IDUSR_Profil
Order by DataDodania desc
'
           ,1800
           ,5
           ,1611
           ,GETDATE()
           ,NULL
           ,NULL)
GO
SET IDENTITY_INSERT RAPRaport OFF
INSERT INTO [dbo].[RAPRaportParametr]
           ([RAPRaportID]
           ,[LP]
           ,[Etykieta]
           ,[Nazwa]
           ,[Typ]
           ,[CzyWymagany]
           ,[CzyWielokrotny]
           ,[WartoscDomyslna]
           ,[WartoscMin]
           ,[WartoscMax]
           ,[DWOWystepowanieID]
           ,[WyborKwerenda]
           ,[SygnaturaIdentyfikator]
           ,[SygnaturyOpis]
           ,[Dodal]
           ,[DataDodania]
           ,[Zmodyfikowal]
           ,[DataModyfikacji])
     VALUES
           (108
           ,1
           ,'Dydaktyk'
           ,':IDUSR_Profil'
           ,'INT'
           ,1
           ,0
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,'select IDUSR_Profil, Nazwisko + '' '' + Imie as DydaktykNazwa  from USR_Profil'
           ,'IDUSR_Profil'
           ,'DydaktykNazwa'
           ,1611
           ,GETDATE()
           ,NULL
           ,NULL)
GO

INSERT INTO [dbo].[RAPRaportParametr]
           ([RAPRaportID]
           ,[LP]
           ,[Etykieta]
           ,[Nazwa]
           ,[Typ]
           ,[CzyWymagany]
           ,[CzyWielokrotny]
           ,[WartoscDomyslna]
           ,[WartoscMin]
           ,[WartoscMax]
           ,[DWOWystepowanieID]
           ,[WyborKwerenda]
           ,[SygnaturaIdentyfikator]
           ,[SygnaturyOpis]
           ,[Dodal]
           ,[DataDodania]
           ,[Zmodyfikowal]
           ,[DataModyfikacji])
     VALUES
			(107
           ,1
           ,'Dydaktyk'
           ,':IDUSR_Profil'
           ,'INT'
           ,1
           ,0
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,'select IDUSR_Profil, Nazwisko + '' '' + Imie as DydaktykNazwa  from USR_Profil'
           ,'IDUSR_Profil'
           ,'DydaktykNazwa'
           ,0
           ,GETDATE()
           ,NULL
           ,NULL)
GO