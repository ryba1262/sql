/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [IDUSR_ProfilToRola]
      ,[USR_ProfilID]
      ,[USR_RolaID]
      ,[KluczZdalny]
	  ,pr.IDPracownik
	  ,pr.Imie
	  ,pr.Nazwisko
	  ,PESEL
  FROM [ESS].[dbo].[USR_ProfilToRola]

  join Pracownik pr on KluczZdalny = IDPracownik
    Where pr.IDPracownik in (
	SELECT
IDPracownik
  FROM [ESS].[dbo].[Pracownik]
  where PESEL in (
  SELECT 
      [Pesel]
  FROM [ESS].[dbo].[Pracownik]
  group by Pesel 
  having count(*) >1
  )
  and PESEL <>''
	)