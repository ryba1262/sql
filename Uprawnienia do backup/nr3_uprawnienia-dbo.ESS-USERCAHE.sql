GRANT Select on Bank TO [userCAHE]
GRANT Select on Blok TO [userCAHE]
GRANT Execute on BlokiPathString TO [userCAHE]
GRANT Select on Budynek TO [userCAHE]
GRANT Select on ComaIntegerStringToTable TO [userCAHE]
GRANT Execute on DatyZKalendarza TO [userCAHE]
GRANT Execute on DatyZKalendarzaDzien TO [userCAHE]
GRANT Select on DrzewoSiatki TO [userCAHE]
GRANT Execute on DWODodajUprawnieniaDlaGrupy TO [userCAHE]
GRANT Execute on DWODodajUprawnieniaDoObiektu TO [userCAHE]
GRANT Execute on DWODodajUzytkownikaDoGrupy TO [userCAHE]
GRANT Select on DWOFiltrPola TO [userCAHE]
GRANT Select on DWOGrupa TO [userCAHE]
GRANT Select on DWOGrupaUzytkownik TO [userCAHE]
GRANT Select on DWOJoin TO [userCAHE]
GRANT Execute on DWOKopiujUprawnienia TO [userCAHE]
GRANT Select on DWOObiekt TO [userCAHE]
GRANT Select on DWOObiektDlaWariantu TO [userCAHE]
GRANT Select on DWOPole TO [userCAHE]
GRANT Select on DWOPoziomUprawnien TO [userCAHE]
GRANT Select on DWOSprawdzPoziomUprawnien TO [userCAHE]
GRANT Select on DWOStworzDrzewka TO [userCAHE]
GRANT Select on DWOUprawnienie TO [userCAHE]
GRANT Select on DWOUprawnienieDoObiektu TO [userCAHE]
GRANT Select on DWOUstawienia TO [userCAHE]
GRANT Execute on DWOUstawSplywanieUprawnien TO [userCAHE]
GRANT Execute on DWOUsunGrupeUzytkownikow TO [userCAHE]
GRANT Execute on DWOUsunUprawnieniaDlaGrupy TO [userCAHE]
GRANT Execute on DWOUsunUprawnieniaDoObiektu TO [userCAHE]
GRANT Select on DWOWariant TO [userCAHE]
GRANT Select on DWOWystepowanie TO [userCAHE]
GRANT Execute on DWOWyswietlUprawnienia TO [userCAHE]
GRANT Select on DWOWyszukiwanie TO [userCAHE]
GRANT Select on DWOZestawPoziom TO [userCAHE]
GRANT Select on DWOZestawUprawnien TO [userCAHE]
GRANT Execute on DWOZmienPoziomUprawnien TO [userCAHE]
GRANT Select on DzienTygodnia TO [userCAHE]
GRANT Execute on FormatCzas TO [userCAHE]
GRANT Select on FormaZajec TO [userCAHE]
GRANT Execute on GenerujIBAN TO [userCAHE]
GRANT Select on Gmina TO [userCAHE]
GRANT Select on GrupaDziekanska TO [userCAHE]
GRANT Select on GrupaZajeciowa TO [userCAHE]
GRANT Select on GrupaZajIndeks TO [userCAHE]
GRANT Execute on GrupyStudenckieStr TO [userCAHE]
GRANT Select on HierarchiaBlokowDlaProgramPoz TO [userCAHE]
GRANT Select on HistoriaStudenta TO [userCAHE]
GRANT Select on Indeks TO [userCAHE]
GRANT Select on IndeksROR TO [userCAHE]
GRANT Select on Jezyk TO [userCAHE]
GRANT Select on JezykPrzedmiot TO [userCAHE]
GRANT Select on KartaEgz TO [userCAHE]
GRANT Select on KartaEgzForma TO [userCAHE]
GRANT Select on KartaEgzPoz TO [userCAHE]
GRANT Select on KartaEgzPracownik TO [userCAHE]
GRANT Execute on KartaEgzSrednia TO [userCAHE]
GRANT Select on KartaUzup TO [userCAHE]
GRANT Select on Kierunek TO [userCAHE]
GRANT Select on KontoBankowe TO [userCAHE]
GRANT Select on Kurs TO [userCAHE]
GRANT Select on KursModul TO [userCAHE]
GRANT Select on KursPoz TO [userCAHE]
GRANT Select on KursPozData TO [userCAHE]
GRANT Select on Miejscowosc TO [userCAHE]
GRANT Execute on NazwaSpec TO [userCAHE]
GRANT Select on OkregowaKomisjaEgzaminacyjna TO [userCAHE]
GRANT Select on OkresRozliczeniowyPoz TO [userCAHE]
GRANT Select on Panstwo TO [userCAHE]
GRANT Select on PlanPozGrupa TO [userCAHE]
GRANT Select on PlanPozPracownik TO [userCAHE]
GRANT Select on PlanZajec TO [userCAHE]
GRANT Select on PlanZajecPoz TO [userCAHE]
GRANT Select on Powiat TO [userCAHE]
GRANT Select on Pracownik TO [userCAHE]
GRANT Select on Program TO [userCAHE]
GRANT Select on ProgramPoz TO [userCAHE]
GRANT Select on Przedmiot TO [userCAHE]
GRANT Select on PublikacjaZalacznik TO [userCAHE]
GRANT Select on PublikacjaZalacznikOdbiorca TO [userCAHE]
GRANT Select on PublikacjaZalacznikTypMime TO [userCAHE]
GRANT Select on PublikowanaDana TO [userCAHE]
GRANT Select on PublikowanaDanaOdbiorca TO [userCAHE]
GRANT Select on PublikowanaDanaPoz TO [userCAHE]
GRANT Select on PublikowanaDanaPublikacja TO [userCAHE]
GRANT Select on PublikowanaDanaZalacznik TO [userCAHE]
GRANT Select on RachunekBiezacy TO [userCAHE]
GRANT Select on RejestracjaWarunkowaKryterium TO [userCAHE]
GRANT Select on RejestracjaWarunkowaKryteriumPoz TO [userCAHE]
GRANT Select on RSAktywacja TO [userCAHE]
GRANT Select on RSBilans TO [userCAHE]
GRANT Select on RSKasa TO [userCAHE]
GRANT Select on RSKwitKasowy TO [userCAHE]
GRANT Select on RSKwitKasowyPoz TO [userCAHE]
GRANT Select on RSRaport TO [userCAHE]
GRANT Select on RSRaportPoz TO [userCAHE]
GRANT Select on RSRejestr TO [userCAHE]
GRANT Select on Sala TO [userCAHE]
GRANT Select on Sekcja TO [userCAHE]
GRANT Select on SemestrInfo TO [userCAHE]
GRANT Select on SemestrRealizacji TO [userCAHE]
GRANT Select on Siatka TO [userCAHE]
GRANT Select on SiatkaPoz TO [userCAHE]
GRANT Select on SkalaOcenPoz TO [userCAHE]
GRANT Execute on sp_KartaEgzPUW TO [userCAHE]
GRANT Execute on sp_ListaAktualnychPublikacjiDlaIndeksu TO [userCAHE]
GRANT Select on Specjalizacja TO [userCAHE]
GRANT Select on Specjalnosc TO [userCAHE]
GRANT Select on SposobRozliczenia TO [userCAHE]
GRANT Execute on SprawdzNumerIBAN TO [userCAHE]
GRANT Select on SprawdzRejestracje TO [userCAHE]
GRANT Select on StanCywilny TO [userCAHE]
GRANT Select on StatusStudenta TO [userCAHE]
GRANT Select on Student TO [userCAHE]
GRANT Select on Studia TO [userCAHE]
GRANT Select on sync_WPS_PUW TO [userCAHE]
GRANT Select on SystemStudiow TO [userCAHE]
GRANT Select on TrybStudiow TO [userCAHE]
GRANT Select on Tytul TO [userCAHE]
GRANT Select on Uzytkownik TO [userCAHE]
GRANT Select on Wojewodztwo TO [userCAHE]
GRANT Select on Wojewodztwo TO [userCAHE]
GRANT Select on WPSZmianaHasla TO [userCAHE]
GRANT Select on WUGrupaWydrukow TO [userCAHE]
GRANT Select on WUParametry TO [userCAHE]
GRANT Select on WUWydruk TO [userCAHE]
GRANT Select on WUWydrukWersja TO [userCAHE]
GRANT Select on WUZestawParametrow TO [userCAHE]
GRANT Execute on WybraniWykladowcy TO [userCAHE]
GRANT Select on Wydzial TO [userCAHE]
GRANT Select on WzorzecNumIndeksu TO [userCAHE]
GRANT Select on ZajeciaStatus TO [userCAHE]
GRANT Select on ZajeciaWspolne TO [userCAHE]
GRANT Select on ZajeciaWspolnePoz TO [userCAHE]
GRANT Select on Zatrudnienie TO [userCAHE]




































