-- -- WUWydrukWersja -- -- 
UPDATE [dbo].[WUWydrukWersja] SET [Szablon] = '<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                exclude-result-prefixes="fo">
  <xsl:decimal-format name="pln"
  decimal-separator="," grouping-separator="." NaN=""/>
  <xsl:param name="dataWygenerowania"/>
  <xsl:template match="/Dane">
    <fo:root font-family="Times" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:w="http://schemas.microsoft.com/office/word/2003/wordml">
      <fo:layout-master-set xmlns:rx="http://www.renderx.com/XSL/Extensions" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882">
        <fo:simple-page-master master-name="section1-first-page" page-width="8.268055555555555in" page-height="11.693055555555556in" margin-top="35.4pt" margin-bottom="35.4pt" margin-right="50pt" margin-left="65pt">
          <fo:region-body margin-top="0.55pt"></fo:region-body>
          <fo:region-before region-name="first-page-header" extent="11in"></fo:region-before>
          <fo:region-after region-name="first-page-footer" extent="11in" display-align="after"></fo:region-after>
        </fo:simple-page-master>
        <fo:simple-page-master master-name="section1-odd-page" page-width="8.268055555555555in" page-height="11.693055555555556in" margin-top="35.4pt" margin-bottom="35.4pt" margin-right="50pt" margin-left="65pt">
          <fo:region-body margin-top="0.55pt"></fo:region-body>
          <fo:region-before region-name="odd-page-header" extent="11in"></fo:region-before>
          <fo:region-after region-name="odd-page-footer" extent="11in" display-align="after"></fo:region-after>
        </fo:simple-page-master>
        <fo:simple-page-master master-name="section1-even-page" page-width="8.268055555555555in" page-height="11.693055555555556in" margin-top="35.4pt" margin-bottom="35.4pt" margin-right="50pt" margin-left="65pt">
          <fo:region-body margin-top="0.55pt"></fo:region-body>
          <fo:region-before region-name="even-page-header" extent="11in"></fo:region-before>
          <fo:region-after region-name="even-page-footer" extent="11in" display-align="after"></fo:region-after>
        </fo:simple-page-master>
        <fo:page-sequence-master master-name="section1-page-sequence-master">
          <fo:repeatable-page-master-alternatives>
            <fo:conditional-page-master-reference odd-or-even="odd" master-reference="section1-odd-page" />
            <fo:conditional-page-master-reference odd-or-even="even" master-reference="section1-even-page" />
          </fo:repeatable-page-master-alternatives>
        </fo:page-sequence-master>
      </fo:layout-master-set>
      <fo:page-sequence master-reference="section1-page-sequence-master" format="1" xmlns:rx="http://www.renderx.com/XSL/Extensions" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wx="http://schemas.microsoft.com/office/word/2003/auxHint" xmlns:aml="http://schemas.microsoft.com/aml/2001/core" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:dt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882">
        <fo:static-content flow-name="xsl-footnote-separator">
          <fo:block>
            <fo:leader leader-pattern="rule" leader-length="144pt" rule-thickness="0.5pt" rule-style="solid" color="gray" />
          </fo:block>
        </fo:static-content>
        <fo:flow flow-name="xsl-region-body">
          <fo:block widows="2" orphans="2" font-size="10pt" line-height="1.147" white-space-collapse="false">
            <fo:marker marker-class-name="first-page-header" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:marker marker-class-name="first-page-footer" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:marker marker-class-name="odd-page-header" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:marker marker-class-name="odd-page-footer" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:marker marker-class-name="even-page-header" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:marker marker-class-name="even-page-footer" xmlns:st1="urn:schemas-microsoft-com:office:smarttags" />
            <fo:block font-family="Times" font-size="9pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-size="10pt">
                <fo:leader leader-length="0pt" />UMOWA O WARUNKACH ODP�ATNO�CI ZA STUDIA
			  </fo:inline>
            </fo:block>
            <fo:block font-family="Times" font-size="9pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Times" font-size="9pt" language="PL" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:inline font-size="9pt">
                <fo:leader leader-length="0pt" />zawarta w dniu: <fo:leader leader-length="0pt" />
              </fo:inline>
              <fo:inline font-weight="bold">
                <fo:leader leader-length="0pt" /><xsl:value-of select="$dataWygenerowania"/>
              </fo:inline>
              <fo:inline font-size="9pt">
                <fo:leader leader-length="0pt" /> pomi�dzy:
              </fo:inline>
            </fo:block>
            <fo:block font-family="Times" font-size="9pt" language="PL" end-indent="-50.4pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
              <fo:leader />
            </fo:block>
            <fo:block>
              <fo:block font-family="Times" font-size="9pt" start-indent="-18pt" text-align="justify" line-height="1.5">
                Akademi� Humanistyczno-Ekonomiczn� w �odzi, ul. Sterlinga 26, 90-212 ��d�, wpisan� pod liczb� porz�dkow� 30 do rejestru uczelni niepublicznych i zwi�zk�w uczelni niepublicznych, prowadzonego przez ministra w�a�ciwego do spraw szkolnictwa wy�szego, zwan� dalej Uczelni�, reprezentowan� przez pe�nomocnika �������������������������...
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-9pt">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" space-after="2pt">
                <fo:inline>
                  a <fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline>
                  <fo:leader leader-length="0pt" />
                      <xsl:value-of select="ODMpanem"/>
                </fo:inline>
                <fo:inline color="white">-</fo:inline>
                <fo:inline font-weight="bold">
                      <xsl:value-of select="Student"/>
                </fo:inline>
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-9pt">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL">
                <xsl:value-of select="ODMzameldowanym"/>
                <fo:inline color="white">--</fo:inline>
                <fo:inline font-size="9pt" font-weight="bold">
                  <xsl:value-of select="AdresSP"/>
                </fo:inline>
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL">
                <fo:inline font-size="9pt">
                  <fo:leader leader-length="-34pt" />               powiat   <fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline font-size="9pt" font-weight="bold">
                  <fo:leader leader-length="0pt" />
                  <xsl:value-of select="AdresSPPowiat"/>
                </fo:inline>
                <fo:inline font-size="9pt">
                  <fo:leader leader-pattern="space" leader-length="43pt" /><fo:leader leader-length="0pt" />                wojew�dztwo   <fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline font-size="9pt" font-weight="bold">
                  <fo:leader leader-length="0pt" />
                  <xsl:value-of select="AdresSPWojewodztwo"/>
                </fo:inline>
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL">
                <fo:inline font-size="9pt">
                  <xsl:value-of select="DOTSNazwa"/> nr<fo:inline color="white">-</fo:inline>
                  <fo:inline font-size="9pt" font-weight="bold">
                    <fo:leader leader-length="0pt" />
                    <xsl:value-of select="DOTSSeriaNumer"/>
                  </fo:inline>
                  <fo:inline font-size="9pt">
                    <fo:leader leader-pattern="space" leader-length="11.25pt" /><fo:leader leader-length="0pt" />                PESEL   <fo:leader leader-length="0pt" />
                    <fo:leader leader-length="0pt" />
                  </fo:inline><fo:inline font-size="9pt" font-weight="bold">
                    <xsl:value-of select="PESEL"/>
                  </fo:inline>
                </fo:inline>
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-27pt" space-before="5pt">
                <fo:inline font-size="9pt">
                  <fo:leader leader-length="0pt" />  decyzj� Dziekana <fo:leader leader-length="0pt" />
                  <xsl:value-of select="ODMwpisanym"/>
                  <fo:leader leader-length="0pt" /> na list� student�w pierwszego semestru studi�w pierwszego stopnia
                </fo:inline>
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" font-weight="bold">
                <fo:inline font-weight="bold" font-size="9pt">
                  <fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline font-weight="bold" font-size="9pt">
                  <fo:leader leader-pattern="space" leader-length="259.5pt" />
                </fo:inline>
              </fo:block>
              <fo:table font-family="Times" start-indent="0pt" table-layout="fixed" width="100%">
                <fo:table-column column-number="1" column-width="238.05pt" />
                <fo:table-column column-number="2" column-width="238.1pt" />
                <fo:table-body start-indent="0pt" end-indent="0pt">
                  <fo:table-row>
                    <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="1pt" padding-right="3.5pt">
                      <fo:block font-family="Times" font-size="9pt" language="PL" font-weight="bold">
                        <fo:leader leader-length="0pt" />
                        <xsl:value-of select="Wydzial"/>
                        <fo:leader leader-length="0pt" />
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="1pt" padding-right="3.5pt">
                      <fo:block font-family="Times" font-size="9pt" language="PL">
                        <fo:inline font-size="9pt">
                          <fo:leader leader-length="0pt" />studia <fo:leader leader-length="0pt" />
                        </fo:inline>
                        <fo:inline font-size="9pt" font-weight="bold">
                          <fo:leader leader-length="0pt" />
                          <xsl:value-of select="TrStudNJakie"/>
                        </fo:inline>
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                  <fo:table-row>
                    <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="1pt" padding-right="3.5pt">
                      <fo:block font-family="Times" font-size="9pt" language="PL">
                        <fo:inline font-size="9pt">
                          <fo:leader leader-length="0pt" />kierunek <fo:leader leader-length="0pt" />
                        </fo:inline>
                        <fo:inline font-size="9pt" font-weight="bold">
                          <fo:leader leader-length="0pt" />
                          <xsl:value-of select="Kierunek"/>
                        </fo:inline>
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="1pt" padding-right="3.5pt">
                      <fo:block font-family="Times" font-size="9pt" language="PL" >
                        <fo:inline font-size="9pt">
                          <fo:leader leader-length="0pt" />spec. <fo:leader leader-length="0pt" />
                        </fo:inline>
                        <fo:inline font-size="9pt" font-weight="bold">
                          <fo:leader leader-length="0pt" />
                          <xsl:value-of select="SpecjalnoscDruk"/>
                        </fo:inline>
                      </fo:block>
                    </fo:table-cell>
                  </fo:table-row>
                </fo:table-body>
              </fo:table>
              <fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt">
                <fo:inline font-size="9pt">
                  <fo:leader leader-length="0pt" />
                </fo:inline>
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" end-indent="1.15pt">
                <xsl:value-of select="ODMktory"/>
                <fo:inline font-size="9pt">
                  <fo:leader leader-length="0pt" /> pe�ne prawa studenckie uzyska z chwil� immatrykulacji oraz z�o�enia �lubowania, <fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline color="#000000" font-size="9pt">
                  <xsl:value-of select="ODMzwanym"/>
                </fo:inline>
                <fo:inline font-size="9pt">
                  <fo:leader leader-length="0pt" /> dalej Studentem.
                </fo:inline>
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" font-weight="bold">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" text-align="center" font-weight="bold">
                <fo:inline font-weight="bold" font-size="9pt">
                  <fo:leader leader-length="0pt" />� 1
                </fo:inline>
              </fo:block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" text-align="justify" color="#000000">
                  <fo:list-item-label font-size="9pt" start-indent="-18pt" text-indent="0pt" color="black" font-family="Times">
                    <fo:block>1.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="0pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline color="#000000" font-size="9pt">
                        <fo:leader leader-length="0pt" />Uczelnia zobowi�zuje si� do zorganizowania kszta�cenia zgodnie z programem i form� studi�w.
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" text-align="justify" color="#000000">
                  <fo:list-item-label font-size="9pt" start-indent="-18pt" text-indent="0pt" color="black" font-family="Times">
                    <fo:block>2.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="0pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline color="#000000" font-size="9pt">
                        <fo:leader leader-length="0pt" />Kszta�cenie mo�e odbywa� si� z wykorzystaniem metod i technik nauczania na odleg�o��.
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" text-align="justify" color="#000000">
                  <fo:list-item-label font-size="9pt" start-indent="-18pt" text-indent="0pt" color="black" font-family="Times">
                    <fo:block>3.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="0pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline color="#000000" font-size="9pt">
                        <fo:leader leader-length="0pt" />Uczelnia rozwija nowe metody nauczania, kt�re mog� by� wprowadzone w toku studi�w w celu podnoszenia jako�ci kszta�cenia.<fo:leader leader-length="0pt" />
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" text-align="justify" color="#000000">
                  <fo:list-item-label font-size="9pt" start-indent="-18pt" text-indent="0pt" color="black" font-family="Times">
                    <fo:block>4.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="0pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline color="#000000" font-size="9pt">
                        <fo:leader leader-length="0pt" />Obowi�zkiem Studenta jest zapoznanie si� ze Statutem Uczelni, Regulaminem Studi�w, Regulaminem Rekrutacji Online (dost�pnymi w Dziekanacie, wirtualnym pokoju studenta <fo:leader leader-length="0pt" />
                        <fo:inline color="#0000FF" text-decoration="underline" font-size="9pt">
                          <fo:leader leader-length="0pt" />http://student.ahe.lodz.pl<fo:leader leader-length="0pt" />
                        </fo:inline>
                        <fo:leader leader-length="0pt" />) oraz przestrzeganie tych przepis�w. Student o�wiadcza, i� zapozna� si� z prawem odst�pienia od umowy zawartej na odleg�o�� lub poza lokalem Uczelni.
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" text-align="center" font-weight="bold">
                <fo:inline font-weight="bold" font-size="9pt">
                  <fo:leader leader-length="0pt" />
                </fo:inline>
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" text-align="center" font-weight="bold">
                <fo:inline font-weight="bold" font-size="9pt">
                  <fo:leader leader-length="0pt" />� 2
                </fo:inline>
              </fo:block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" text-align="justify">
                  <fo:list-item-label font-size="9pt" start-indent="-18pt" text-indent="0pt" font-family="Times">
                    <fo:block>1.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />Student zobowi�zany jest na mocy umowy wnie�� okre�lone op�aty.
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" text-align="justify">
                  <fo:list-item-label font-size="9pt" start-indent="-18pt" text-indent="0pt" font-family="Times">
                    <fo:block>2.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />Op�atami, o kt�rych mowa w ust. 1 s�:
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" start-indent="18pt" text-align="justify">
                  <fo:list-item-label font-size="9pt" start-indent="0pt" text-indent="0pt" font-family="Times">
                    <fo:block>a)</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="20.25pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />op�ata za post�powanie rekrutacyjne w wysoko�ci <fo:leader leader-length="0pt" />
                      </fo:inline>
                      <fo:inline font-size="9pt" font-weight="bold">
                        <fo:leader leader-length="0pt" />
                        <xsl:value-of select="format-number(UmowaPostepowanieRek, &quot;##0,00&quot;, &quot;pln&quot;)"/> z�<fo:leader leader-length="0pt" />,
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" start-indent="18pt" text-align="justify">
                  <fo:list-item-label font-size="9pt" start-indent="0pt" text-indent="0pt" font-family="Times">
                    <fo:block>b)</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="19.5pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />wpisowe w wysoko�ci <fo:leader leader-length="0pt" />
                      </fo:inline>
                      <fo:inline font-size="9pt" font-weight="bold">
                        <fo:leader leader-length="0pt" />
                        <xsl:value-of select="format-number(UmowaWpisowe2, &quot;##0,00&quot;, &quot;pln&quot;)"/> z�<fo:leader leader-length="0pt" />,
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" start-indent="18pt" text-align="justify">
                  <fo:list-item-label font-size="9pt" start-indent="0pt" text-indent="0pt" font-family="Times">
                    <fo:block>c)</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="20.25pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />op�ata za elektroniczn� legitymacj� studenck� w wysoko�ci <fo:leader leader-length="0pt" />
                      </fo:inline>
                      <fo:inline font-size="9pt" font-weight="bold">
                        <fo:leader leader-length="0pt" />
                        <xsl:value-of select="format-number(UmowaLegitymacjaEl, &quot;##0,00&quot;, &quot;pln&quot;)"/> z�<fo:leader leader-length="0pt" />,
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" start-indent="18pt" text-align="justify">
                  <fo:list-item-label font-size="9pt" start-indent="0pt" text-indent="0pt" font-family="Times">
                    <fo:block>d)</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="19.5pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />czesne,
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" start-indent="18pt" text-align="justify">
                  <fo:list-item-label font-size="9pt" start-indent="0pt" text-indent="0pt" font-family="Times">
                    <fo:block>e)</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="20.25pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />inne op�aty okre�lone w Cenniku op�at za us�ugi nieobj�te czesnym stanowi�cym za��cznik do umowy.
					  </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL">
                  <fo:list-item-label font-size="9pt" start-indent="-18pt" text-indent="0pt" font-family="Times">
                    <fo:block>3.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />Op�aty wymienione 2a, 2b, 2c powinny by� uiszczone nie p�niej ni� w dniu z�o�enia dokument�w na studia  na numer konta bankowego zawarty w � 3 ust. 7 niniejszej umowy.
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
				<fo:list-block>
					<fo:list-item font-family="Times" font-size="9pt" language="PL">
						<fo:list-item-label font-size="9pt" start-indent="-18pt" text-indent="0pt" font-family="Times">
							<fo:block>4.</fo:block>
						</fo:list-item-label>
						<fo:list-item-body end-indent="inherit" text-indent="0pt">
							<fo:block>
								<fo:inline font-size="9pt">
									<fo:leader leader-length="0pt" />Op�aty, o kt�rych mowa w ust. 2 e p�atne s� w przypadkach wskazanych w Cenniku i w terminach w nim podanych.
								</fo:inline>
							</fo:block>
						</fo:list-item-body>
					</fo:list-item>
				</fo:list-block>
              <fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-9pt" text-align="center" font-weight="bold">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" text-align="center" font-weight="bold">
                <fo:inline font-weight="bold" font-size="9pt">
                  <fo:leader leader-length="0pt" /> � 3
                </fo:inline>
              </fo:block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL">
                  <fo:list-item-label font-size="9pt" start-indent="-18.75pt" text-indent="0pt" font-family="Times">
                    <fo:block>1.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />Studia trwaj� 7 semestr�w.
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" text-align="justify">
                  <fo:list-item-label font-size="9pt" start-indent="-18.75pt" text-indent="0pt" font-family="Times">
                    <fo:block>2.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="-0.7pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />Nieuczestniczenie w zaj�ciach dydaktycznych nie zwalnia Studenta z obowi�zku uiszczania czesnego.
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" text-align="justify">
                  <fo:list-item-label font-size="9pt" start-indent="-18.75pt" text-indent="0pt" font-family="Times">
                    <fo:block>3.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="-0.7pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />Okresem rozliczeniowym jest jeden miesi�c
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" text-align="justify">
                  <fo:list-item-label font-size="9pt" start-indent="-18.75pt" text-indent="0pt" font-family="Times">
                    <fo:block>4.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="-0.7pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />Student zobowi�zany jest do zap�aty czesnego, w nast�puj�cy spos�b:
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:block font-family="Times" font-size="9pt" language="PL" text-align="justify">
                <fo:inline font-size="9pt">
                  <fo:leader leader-length="0pt" />a) dla student�w I, II roku czesne za rok akademicki wynosi <fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline font-size="9pt" font-weight="bold">
                  <xsl:value-of select="format-number(UmowaCzesneRocznie, &quot;##0,00&quot;, &quot;pln&quot;)"/> z�<fo:leader leader-length="0pt" />
                  <fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline font-size="9pt">
                  <fo:leader leader-length="0pt" /> roz�o�one jest na 12  miesi�cznych  rat  po <fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline font-size="9pt" font-weight="bold">
                  <xsl:value-of select="format-number(UmowaCzesne, &quot;##0,00&quot;, &quot;pln&quot;)"/> z�<fo:leader leader-length="0pt" />
                  <fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline font-size="9pt">
                  <fo:leader leader-length="0pt" /> ka�da,
                </fo:inline>
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" text-align="justify">
                <fo:inline font-size="9pt">
                  <fo:leader leader-length="0pt" />b) dla student�w III roku czesne za rok akademicki wynosi <fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline font-size="9pt" font-weight="bold">
                  <xsl:value-of select="format-number (floor(((UmowaCzesne * 18 + 500) div 17))*12, &quot;##0,00&quot;, &quot;pln&quot;)"/> z�<fo:leader leader-length="0pt" />
                  <fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline font-size="9pt">
                  <fo:leader leader-length="0pt" /> roz�o�one jest na 12  miesi�cznych  rat  po <fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline font-size="9pt" font-weight="bold">
                  <xsl:value-of select="format-number(floor(((UmowaCzesne * 18 + 500) div 17)), &quot;##0,00&quot;, &quot;pln&quot;)"/> z�<fo:leader leader-length="0pt" />
                  <fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline font-size="9pt">
                  <fo:leader leader-length="0pt" /> ka�da,
                </fo:inline>
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" text-align="justify">
                <fo:inline font-size="9pt">
                  <fo:leader leader-length="0pt" />c) dla student�w IV roku VII semestru czesne wynosi <fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline font-size="9pt" font-weight="bold">
                  <xsl:value-of select="format-number (floor(((UmowaCzesne * 18 + 500) div 17))*5, &quot;##0,00&quot;, &quot;pln&quot;)"/> z�<fo:leader leader-length="0pt" />
                  <fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline font-size="9pt">
                  <fo:leader leader-length="0pt" />  i  roz�o�one jest na 5  miesi�cznych rat po <fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline font-size="9pt" font-weight="bold">
                  <xsl:value-of select="format-number (floor(((UmowaCzesne * 18 + 500) div 17)), &quot;##0,00&quot;, &quot;pln&quot;)"/> z�<fo:leader leader-length="0pt" />
                  <fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline font-size="9pt">
                  <fo:leader leader-length="0pt" /> ka�da,
                </fo:inline>
              </fo:block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" start-indent="0pt" text-align="justify">
                  <fo:list-item-label font-size="9pt" start-indent="-18pt" text-indent="0pt" font-family="Times">
                    <fo:block>5.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />Termin p�atno�ci czesnego up�ywa 5-go dnia ka�dego miesi�ca, pocz�wszy od pierwszego miesi�ca semestru.<fo:leader leader-length="0pt" />
                      </fo:inline>
                    </fo:block>
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />W przypadku niedokonania wp�aty czesnego oraz innych op�at, o kt�rych mowa w � 2 ust. 2 w terminie Student zobowi�zany jest do zap�aty zaleg�ej kwoty powi�kszonej o odsetki umowne za op�nienie. Odsetki umowne za op�nienie wynosz� w stosunku rocznym dwukrotno�� odsetek ustawowych za op�nienie od ka�dej zaleg�ej kwoty, nie wi�cej jednak ni� czterokrotno�� wysoko�ci stopy kredytu lombardowego Narodowego Banku Polskiego.
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" start-indent="0pt" text-align="justify">
                  <fo:list-item-label font-size="9pt" start-indent="-18pt" text-indent="0pt" font-family="Times">
                    <fo:block>6.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />W przypadku przyj�cia na studia po up�ywie terminu p�atno�ci czesnego w danym miesi�cu, Student zobowi�zuje si� do wp�aty zaleg�ych rat czesnego w ci�gu siedmiu dni licz�c od daty podpisania niniejszej umowy.
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" text-indent="-36pt" text-align="justify">
                  <fo:list-item-label font-size="9pt" start-indent="-18pt" text-indent="0pt" font-family="Times">
                    <fo:block>7.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />Wp�aty nale�y dokona� na indywidualny nr rachunku bankowego <fo:leader leader-length="0pt" />
                      </fo:inline>
                    </fo:block>
                    <fo:block text-align="center">
                      <fo:inline font-weight="bold" font-size="9pt">
                        <fo:leader leader-length="0pt" />
                        <xsl:value-of select="NrRORPM1PKO"/>
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" text-indent="-36pt" text-align="justify">
                  <fo:list-item-label font-size="9pt" start-indent="-18pt" text-indent="0pt" font-family="Times">
                    <fo:block>8.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="2.25pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />Za dat� dokonania wp�aty czesnego uznaje si� dat� wp�ywu wp�aty na rachunek bankowy Uczelni.
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" end-indent="-43.85pt" font-weight="bold">
                <fo:inline font-weight="bold" font-size="9pt">
                  <fo:leader leader-length="0pt" />
                </fo:inline>
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" text-align="center" font-weight="bold">
                <fo:inline font-weight="bold" font-size="9pt">
                  <fo:leader leader-length="0pt" />� 4
                </fo:inline>
              </fo:block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" end-indent="1.15pt" text-align="justify">
                  <fo:list-item-label font-size="9pt" start-indent="-18pt" text-indent="0pt" font-family="Times">
                    <fo:block>1.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="0pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />Strony umowy zgodnie postanawiaj�, �e w przypadku zwi�kszenia koszt�w utrzymania Uczelnia mo�e dokona� z pocz�tkiem kolejnego semestru zmiany wysoko�ci czesnego (waloryzacja umowna), przy czym ka�da zmiana wysoko�ci czesnego nie mo�e przekroczy� kwoty uzyskanej z przemno�enia wysoko�ci czesnego wskazanej w � 3 ust. 4 przez wska�nik inflacji za ubieg�y rok liczony do miesi�ca poprzedzaj�cego zmian� wysoko�ci czesnego og�aszany przez Prezesa G��wnego Urz�du Statystycznego powi�kszony o 10 % wysoko�ci czesnego wskazanej w � 3 ust. 4.
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:block font-family="Times" font-size="9pt" language="PL" end-indent="1.15pt" text-align="justify">
                <fo:inline font-size="9pt">
                  <fo:leader leader-length="0pt" />W przypadku nadzwyczajnej zmiany okoliczno�ci ka�da zmiana wysoko�ci czesnego nie mo�e przekroczy� 20 % wysoko�ci czesnego wskazanej w � 3 ust. 4. Zmiana wysoko�ci czesnego wymaga wydania stosownego Zarz�dzenia Kanclerza AHE, przekazania jego tre�ci Studentowi oraz og�oszenia tre�ci Zarz�dzenia w gablocie w holu Uczelni, w Dziekanacie i w wirtualnym pokoju studenta na stronie <fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline color="#0000FF" font-size="9pt" text-decoration="underline">
                  <fo:leader leader-length="0pt" />http://student.ahe.lodz.pl<fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline font-size="9pt">
                  <fo:leader leader-length="0pt" /> , z zastrze�eniem ust.2.
                </fo:inline>
              </fo:block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" end-indent="1.15pt" text-align="justify">
                  <fo:list-item-label font-size="9pt" start-indent="-18pt" text-indent="0pt" font-family="Times">
                    <fo:block>2.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="0pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />Student, kt�ry nie wyra�a zgody na zmian� czesnego, ma prawo w ci�gu 7 dni od powzi�cia wiadomo�ci o zmianie wysoko�ci czesnego rozwi�za� niniejsz� umow�. W takim przypadku umowa rozwi�zuje si� z ostatnim dniem semestru poprzedzaj�cego nowy semestr, kt�rego dotyczy zmiana wysoko�ci czesnego.
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" end-indent="1.15pt" text-align="justify">
                  <fo:list-item-label font-size="9pt" start-indent="-18pt" text-indent="0pt" font-family="Times">
                    <fo:block>3.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="0pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />O�wiadczenie Studenta o rozwi�zaniu umowy powinno by� z�o�one w formie pisemnej w Dziekanacie Uczelni. W przypadku niezachowania przez Studenta formy oraz terminu z�o�enia o�wiadczenia o rozwi�zaniu umowy przyjmuje si�, �e Student wyra�a zgod� na zmian� wysoko�ci czesnego.
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" end-indent="1.15pt" text-align="justify">
                  <fo:list-item-label font-size="9pt" start-indent="-18pt" text-indent="0pt" font-family="Times">
                    <fo:block>4.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="0pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />Uczelnia mo�e dokona� w oznaczonym okresie obni�enia wysoko�ci czesnego okre�lonego w � 3 ust. 4. Obni�enie wysoko�ci czesnego dokonane b�dzie na podstawie Zarz�dzenia Kanclerza AHE, kt�rego tre�� zostanie przekazana Studentowi oraz b�dzie og�oszona w gablocie w holu Uczelni i w Dziekanacie, a tak�e w wirtualnym pokoju studenta na stronie <fo:leader leader-length="0pt" />
                      </fo:inline>
                      <fo:basic-link external-destination="url(&quot;http://student.ahe.lodz.pl&quot;)">
                        <fo:inline color="#0000FF" text-decoration="underline" font-size="9pt">
                          <fo:leader leader-length="0pt" />http://student.ahe.lodz.pl<fo:leader leader-length="0pt" />
                        </fo:inline>
                      </fo:basic-link>
                      <fo:inline color="#0000FF" font-size="9pt">
                        <fo:leader leader-length="0pt" />, <fo:leader leader-length="0pt" />
                      </fo:inline>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />co b�dzie skutkowa� zmian� tre�ci umowy - bez potrzeby podpisania aneksu.
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" end-indent="1.15pt" text-align="justify">
                  <fo:list-item-label font-size="9pt" start-indent="-18pt" text-indent="0pt" font-family="Times">
                    <fo:block>5.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="0pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />W przypadku zwi�kszania innych op�at, o kt�rych stanowi � 2 ust. 2 lit. e umowy, stosuje si� ust. 1 i 2 powy�ej, z tym, �e podstaw� przemno�enia jest, zamiast kwoty czesnego wskazanej w � 3 ust. 4, kwota zwi�kszanej op�aty wskazanej w cenniku.<fo:leader leader-length="0pt" />
                      </fo:inline>
                    </fo:block>
                      <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />Zdanie pierwsze nie dotyczy op�at za legalizacj� dokument�w, wydanie dyplomu, wydanie odpis�w w j�zykach obcych, �wiadectwa, indeksu, legitymacji studenckiej oraz za wydanie duplikat�w tych dokument�w, wskazanych jednocze�nie w rozporz�dzeniu wydanym na podstawie art. 192 ust. 1 Prawa o szkolnictwie wy�szym.<fo:leader leader-length="0pt" />
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" end-indent="1.15pt" text-align="justify">
                  <fo:list-item-label font-size="9pt" start-indent="-18pt" text-indent="0pt" font-family="Times">
                    <fo:block>6.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="0pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />Studentowi przys�uguje prawo wyboru specjalizacji co skutkowa� mo�e zmian� wysoko�ci czesnego zgodnie z obowi�zuj�cym cennikiem. W przypadku nie uruchomienia specjalizacji, o kt�rej mowa we wst�pnej cz�ci umowy, student zobowi�zany jest do zap�aty czesnego zgodnie z obowi�zuj�cym cennikiem za t� specjalizacj�, kt�ra jest dost�pna dla danego roku studi�w w ofercie uczelni. W sytuacjach opisanych powy�ej nie stosuje si� ust. 2.<fo:leader leader-length="0pt" />
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" end-indent="-43.85pt" font-weight="bold">
                <fo:inline font-weight="bold" font-size="9pt">
                  <fo:leader leader-length="0pt" />
                </fo:inline>
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" text-align="center" font-weight="bold">
                <fo:inline font-weight="bold" font-size="9pt">
                  <fo:leader leader-length="0pt" />� 5
                </fo:inline>
              </fo:block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" start-indent="0pt" text-align="justify">
                  <fo:list-item-label font-size="9pt" start-indent="-18pt" text-indent="0pt" font-family="Times">
                    <fo:block>1.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="2.25pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />Nie p�niej ni� na pi�� dni przed dniem, w kt�rym zgodnie z ofert� Uczelni mia�y rozpocz�� si� zaj�cia w ramach studi�w obj�tych niniejsz� umow�, Uczelnia  ma prawo do odst�pienia od umowy w przypadku, gdy organizacja studi�w b�dzie nieop�acalna i zostanie podj�ta decyzja o nie prowadzeniu w danym roku akademickim zaj�� w ramach studi�w obj�tych niniejsz� umow�. Decyzj� o nie prowadzeniu w danym roku akademickim zaj�� w ramach studi�w obj�tych niniejsz� umow� oraz o odst�pieniu od umowy podejmuje Rektor AHE.
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" start-indent="0pt" text-align="justify">
                  <fo:list-item-label font-size="9pt" start-indent="-18pt" text-indent="0pt" font-family="Times">
                    <fo:block>2.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="2.25pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />O�wiadczenie woli o odst�pieniu od umowy przez Uczelni� powinno by� z�o�one Studentowi na pi�mie.
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:block font-family="Times" font-size="9pt" language="PL" end-indent="-43.85pt">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" text-align="center" font-weight="bold">
                <fo:inline font-weight="bold" font-size="9pt">
                  <fo:leader leader-length="0pt" />� 6
                </fo:inline>
              </fo:block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" end-indent="1.15pt" text-align="justify">
                  <fo:list-item-label font-size="9pt" start-indent="-18pt" text-indent="0pt" font-family="Times">
                    <fo:block>1.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="0pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />Z zastrze�eniem � 4 ust. 2 i 3, Student ma prawo do rozwi�zania umowy, przy czym dla jego skuteczno�ci, o�wiadczenie takie nale�y z�o�y� na pi�mie w Dziekanacie Uczelni.
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" end-indent="1.15pt" text-align="justify">
                  <fo:list-item-label font-size="9pt" start-indent="-18pt" text-indent="0pt" font-family="Times">
                    <fo:block>2.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="0pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />W przypadku rozwi�zania niniejszej umowy w trybie okre�lonym w ust. 1 Uczelnia stosuje przepisy Kodeksu cywilnego, w szczeg�lno�ci art. 746 Kodeksu cywilnego, dotycz�ce zwrotu wydatk�w poniesionych przez przyjmuj�cego zlecenie na wykonanie umowy zlecenia i ewentualnych roszcze� odszkodowawczych.
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" end-indent="1.15pt" text-align="justify">
                  <fo:list-item-label font-size="9pt" start-indent="-18pt" text-indent="0pt" font-family="Times">
                    <fo:block>3.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="0pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />Niezachowanie formy czynno�ci, o kt�rej mowa w ust. 1, b�dzie skutkowa�o dalszym naliczaniem czesnego w nast�pnych miesi�cach do ko�ca trwania semestru.
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:block font-family="Times" font-size="9pt" language="PL" end-indent="1.15pt" text-align="justify">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" text-align="center" font-weight="bold">
                <fo:inline font-weight="bold" font-size="9pt">
                  <fo:leader leader-length="0pt" />� 7
                </fo:inline>
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" end-indent="1.15pt" text-indent="-18pt" text-align="justify">
                <fo:inline font-size="9pt">
                  <fo:leader leader-length="0pt" />       W sprawach nieuregulowanych w niniejszej umowie stosuje si� przepisy Regulaminu Studi�w, Regulaminu Rekrutacji Online i przepisy Kodeksu cywilnego.
                </fo:inline>
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" end-indent="1.15pt" text-align="justify">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" text-align="center">
                <fo:inline font-weight="bold" font-size="9pt">
                  <fo:leader leader-length="0pt" />� 8
                </fo:inline>
              </fo:block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" end-indent="1.15pt" text-align="justify">
                  <fo:list-item-label font-size="9pt" start-indent="-18pt" text-indent="0pt" font-family="Times">
                    <fo:block>1.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="0pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />Student wyra�a zgod� na wystawianie dokument�w finansowych drog� elektroniczn�, bez jego podpisu oraz zamieszczenie ich do jego wgl�du i pobrania w Wirtualnym Pokoju Studenta.
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:list-block>
                <fo:list-item font-family="Times" font-size="9pt" language="PL" end-indent="1.15pt" text-align="justify">
                  <fo:list-item-label font-size="9pt" start-indent="-18pt" text-indent="0pt" font-family="Times">
                    <fo:block>2.</fo:block>
                  </fo:list-item-label>
                  <fo:list-item-body end-indent="inherit" start-indent="0pt" text-indent="0pt">
                    <fo:block>
                      <fo:inline font-size="9pt">
                        <fo:leader leader-length="0pt" />Zamieszczenie dokument�w, o kt�rych mowa w ust. 1 w Wirtualnym Pokoju Studenta jest r�wnoznaczne z mo�liwo�ci� zapoznania si� przez Studenta z ich tre�ci�.
                      </fo:inline>
                    </fo:block>
                  </fo:list-item-body>
                </fo:list-item>
              </fo:list-block>
              <fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-9pt" end-indent="1.15pt" text-align="justify">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" text-align="center">
                <fo:inline font-weight="bold" font-size="9pt">
                  <fo:leader leader-length="0pt" />� 9
                </fo:inline>
              </fo:block>
				<fo:list-block>
					<fo:list-item font-family="Times" font-size="9pt" language="PL" end-indent="1.15pt" text-align="justify">
						<fo:list-item-label font-size="9pt" start-indent="-18pt" text-indent="0pt" font-family="Times">
							<fo:block>1.</fo:block>
						</fo:list-item-label>
						<fo:list-item-body end-indent="inherit" start-indent="0pt" text-indent="0pt">
							<fo:block>
								<fo:inline font-size="9pt">
									<fo:leader leader-length="0pt" />Umowa zosta�a sporz�dzona w dw�ch jednobrzmi�cych egzemplarzach, po jednym dla ka�dej ze stron.
								</fo:inline>
							</fo:block>
						</fo:list-item-body>
					</fo:list-item>
				</fo:list-block>
				<fo:list-block>
					<fo:list-item font-family="Times" font-size="9pt" language="PL" end-indent="1.15pt" text-align="justify">
						<fo:list-item-label font-size="9pt" start-indent="-18pt" text-indent="0pt" font-family="Times">
							<fo:block>2.</fo:block>
						</fo:list-item-label>
						<fo:list-item-body end-indent="inherit" start-indent="0pt" text-indent="0pt">
							<fo:block>
								<fo:inline font-size="9pt">
									<fo:leader leader-length="0pt" />Za��cznik, o kt�rym mowa w � 2 ust. 2 e stanowi integraln� cz�� umowy.
								</fo:inline>
							</fo:block>
						</fo:list-item-body>
					</fo:list-item>
				</fo:list-block>
              <fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" end-indent="-43.85pt">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" end-indent="-43.85pt">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" end-indent="-43.85pt">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" end-indent="-43.85pt">
                <fo:inline font-size="9pt">
                  <fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline font-size="9pt">
                  <fo:leader leader-pattern="space" leader-length="2.25pt" />
                </fo:inline>
                <fo:inline font-size="9pt">
                  <fo:leader leader-pattern="space" leader-length="35.25pt" /><fo:leader leader-length="0pt" />Uczelnia
                </fo:inline>
                <fo:inline font-size="9pt">
                  <fo:leader leader-pattern="space" leader-length="3.75pt" />
                  <fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline font-size="9pt">
                  <fo:leader leader-pattern="space" leader-length="2.25pt" />
                </fo:inline>
                <fo:inline font-size="9pt">
                  <fo:leader leader-pattern="space" leader-length="45.25pt" />
                </fo:inline>
                <fo:inline font-size="9pt">
                  <fo:leader leader-pattern="space" leader-length="35.25pt" /><fo:leader leader-length="0pt" />                                                           Student
                </fo:inline>
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" end-indent="-43.85pt">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" end-indent="-43.85pt">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" end-indent="-43.85pt">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" end-indent="-43.85pt" font-weight="bold">
                <fo:inline font-weight="bold" font-size="9pt">
                  <fo:leader leader-length="0pt" />     _____________________________________                                                           ______________________________________
                </fo:inline>
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" end-indent="-43.85pt" font-weight="bold">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" end-indent="-43.85pt">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" end-indent="-43.85pt">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" end-indent="-43.85pt">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" end-indent="-43.85pt">
                <fo:leader />
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" end-indent="-43.85pt">
                <fo:inline font-size="9pt">
                  <fo:leader leader-length="0pt" />                                                                                                                                         ��d�, dn. <fo:leader leader-length="0pt" />
                </fo:inline>
                <fo:inline font-weight="bold" font-size="9pt">
                  <fo:leader leader-length="0pt" /><xsl:value-of select="$dataWygenerowania"/>
                </fo:inline>
              </fo:block>
              <fo:block font-family="Times" font-size="9pt" language="PL" end-indent="1.15pt" text-align="center" font-weight="bold">
                <fo:leader />
              </fo:block>
            </fo:block>
          </fo:block>

			<fo:block page-break-before="always"/>
			
			<fo:block font-family="Times" font-size="9pt" language="PL" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				<fo:inline font-size="10pt">
					<fo:leader leader-length="0pt" />ZA��CZNIK 1 DO UMOWY O WARUNKACH ODP�ATNO�CI ZA STUDIA Z DNIA <xsl:value-of select="$dataWygenerowania"/>
				</fo:inline>
			</fo:block>
			<fo:block font-family="Times" font-size="10pt" language="" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				<fo:leader />
			</fo:block>
			<fo:block font-family="Times" font-size="10pt" language="" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				<fo:inline font-size="10pt" language="">
					<fo:leader leader-length="0pt" />zawartej pomi�dzy Akademi� Humanistyczno-Ekonomiczn� w �odzi a Studentem
				</fo:inline>
				<fo:inline font-size="9pt">
					<fo:leader leader-length="0pt" /><xsl:value-of select="Student"/>.
				</fo:inline>
			</fo:block>
			<fo:block font-family="Times" font-size="10pt" language="" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				<fo:leader />
			</fo:block>
			<fo:block font-family="Times" font-size="10pt" language="" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				<fo:inline font-weight="bold" font-size="10pt" language="">
					<fo:leader leader-length="0pt" />OP�ATY ZA US�UGI NIEOBJ�TE CZESNYM
				</fo:inline>
			</fo:block>
			<fo:block font-family="Times" font-size="10pt" language="" text-align="center" font-weight="bold" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				<fo:leader />
			</fo:block>
			<fo:table font-family="Times" start-indent="-39.05pt"
					  table-layout="fixed"
					  margin-right="2.0px" margin-left="2.0px"
					  border-left-width="2.0px" border-right-width="2.0px"
					  width="100.0%"
					  xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				<fo:table-column column-number="1" column-width="19.05pt" />
				<fo:table-column column-number="2" column-width="218.9pt" />
				<fo:table-column column-number="3" column-width="68.8pt" />
				<fo:table-column column-number="4" column-width="240.05pt" />
				<fo:table-body start-indent="0pt" end-indent="0pt">
					<fo:table-row>
						<fo:table-cell padding-top="0.5pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" font-weight="bold" language="PL" text-align="center">
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />LP.
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0.5pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" font-weight="bold" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />WYSZCZEG�LNIENIE
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0.5pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" font-weight="bold" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />OP�ATY
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0.5pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" font-weight="bold" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />UWAGI
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />1
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Duplikat dyplomu
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />90 z� **
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:leader />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />2
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Duplikat elektronicznej legitymacji studenckiej
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />25,50 z� **
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:leader />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />3
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Duplikat indeksu
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="none" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />6 z� **
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:leader />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />4
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Duplikat karty okresowych osi�gni�� studenta
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />55 z� **
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:leader />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />5
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Duplikat legitymacji studenckiej
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />7,50 z� **
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:leader />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />6
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Dyplom
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />60 z�
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />termin p�atno�ci - w dniu z�o�enia pracy dyplomowej do
								</fo:inline>
							</fo:block>
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />dziekanatu
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />7
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Dyplom uko�czenia specjalistycznego szkolenia z wybranej dziedziny
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />60 z� **
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />za ka�d� wersj� j�zykow�
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />8
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Elektroniczna legitymacja studencka
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="none" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />17 z�
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />termin p�atno�ci zgodnie z umow�
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />9
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Grupy certyfikatowe z j�zyka obcego (30 godz.)
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />390 z�
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />op�ata semestralna*
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />10
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Indywidualny tok studi�w (ITS)
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />200% czesnego
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="#000000" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />na danym kierunku liczone od poczatku semestru w kt�rym nastapi�o z�o�enie podania o ITS - p�atno�� do 5 dnia ka�dego miesi�ca
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />11
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Indywidulana Organizacja studi�w (IOS)
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />110% czesnego
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="#000000" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />na danym kierunku liczone od poczatku semestru w kt�rym nastapi�o z�o�enie podania o IOS - p�atno�� do 5 dnia ka�dego miesi�ca
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />12
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Koszty za przesy�k� pocztow�- Polska - list zwyk�y
                </fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />2,50 z�**
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />13
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Koszty za przesy�k� pocztow�- Polska - list polecony
                </fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />4,50 z� **
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

          <fo:table-row>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />14
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />Koszty za przesy�k� pocztow�- Polska - list za potwierdzeniem odbioru
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />7 z� **
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />
                </fo:inline>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>

          <fo:table-row>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />15
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />Koszty za przesy�k� pocztow�- poza granicami Polski - list polecony
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />18 z� **
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />
                </fo:inline>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>

          <fo:table-row>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />16
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />Koszty za przesy�k� pocztow�- Polska - list za potwierdzeniem odbioru
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />21 z� **
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />
                </fo:inline>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>
          <fo:table-row>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />17
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />Kserowanie dokument�w
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />5 z� **
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />
                </fo:inline>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>

          <fo:table-row>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />18
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />Legitymacja studencka
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />5 z�
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />
                </fo:inline>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>

          <fo:table-row>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />19
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />Odpis dyplomu w j�zyku obcym
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />40 z� **
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />
                </fo:inline>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>

          <fo:table-row>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />20
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />Odtworzenie danych z toku studi�w w duplikacie indeksu
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt"  >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />120 z� **
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />
                </fo:inline>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>



          <fo:table-row>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" number-rows-spanned="4" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />21
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />Op�ata za dodatkowe zaj�cia/modu� kierunki:
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />
                </fo:inline>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>

          <fo:table-row>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />Administracja, Filologia polska, Kulturoznawstwo, Pedagogika, Politologia, Transport
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt"  >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />70 z�
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" number-rows-spanned="3" border-top-style="none" border-left-style="none" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt"  display-align="center">
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />op�ata z 1 pkt ECTS
                </fo:inline>
              </fo:block>
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />- iloczyn liczby pkt ECTS obowi�zuj�cej na danym kierunku i
                </fo:inline>
              </fo:block>
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />stawki za punkty obowiazuj�cej na danym kierunku (op�ata
                </fo:inline>
              </fo:block>
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />semestralna)*
                </fo:inline>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>
          <fo:table-row>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt"  >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />Dziennikarstwo, Filologia obca, Piel�gniarstwo
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt"  >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />80 z�
                </fo:inline>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>
          <fo:table-row>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />Grafika, Taniec
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt"  >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />110 z�
                </fo:inline>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>
          
          
					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />22
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt">
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Op�ata za monit
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />20 z�
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:leader />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" number-rows-spanned="2" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />23
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" number-rows-spanned="2" border-left-style="none" border-top-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Op�ata za przed�u�enie terminu rozliczenia semestru
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />50  z� **
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />za przed�u�enie do 7 dni po up�yni�ciu terminu wskazanego
								</fo:inline>
							</fo:block>
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />w harmonogramie roku akademickiego
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />150 z� **
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />za przed�u�enie do 14 dni po up�yni�ciu terminu wskazanego
								</fo:inline>
							</fo:block>
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />w harmonogramie roku akademickiego
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />24
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Op�ata za przed�u�enie terminu z�o�enia dokument�w zwi�zanych z zako�czeniem studi�w
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />50% czesnego
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="#000000" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />obowi�zuj�cego na ostatnim semestrze studi�w - op�ata
								</fo:inline>
							</fo:block>
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />miesi�czna p�atna do 5 dnia ka�dego miesi�ca
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />25
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Powtarzanie roku/semestru
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />100% czesnego
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />obowi�zuj�cego na danym kierunku*
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />26
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Reaktywacja - wznowienie studi�w
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />270 z� **
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:leader />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />27
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />R�nice programowe
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />280 z�
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />op�ata semestralna za 1 zaj�cia/przedmiot *
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />28
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt">
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Studia na dodatkowej specjalno�ci/specjalizacji
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />25% czesnego
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="#000000" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />na danym kierunku za ka�d� dodatkow� wybieran�
								</fo:inline>
							</fo:block>
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />specjalno��/specjalizacj� (w przypadku ro�nych cen 25% liczone
								</fo:inline>
							</fo:block>
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />jest od ta�szej spacjalizacji) *
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />29
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Studia na drugim kierunku
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="#000000" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />50% czesnego
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="#000000" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />na danym kierunku (zni�ka obowi�zuje do czasu posiadnia
								</fo:inline>
							</fo:block>
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />statusu studenta na dw�ch kierunkach)*
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />30
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Urlop dzieka�ski - d�ugoterminowy (semestralny, roczny)
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="#000000" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />10% czesnego
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="#000000" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />na danym kierunku (p�atne przed rozpocz�ciem urlopu)**
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />31
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Urlop dzieka�ski � kr�tkoterminowy (do miesi�ca)
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="#000000" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />100% czesnego
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="#000000" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />na danym kierunku za ka�dy miesi�c (p�atne przed
								</fo:inline>
							</fo:block>
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />rozpocz�ciem urlopu)**
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />32
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Uzupe�nianie/powtarzanie przedmiotu (brakuj�ce zaj�cia w
								</fo:inline>
							</fo:block>
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />module, realizacja przedmiotu w czasie urlopu, dodatkowy
								</fo:inline>
							</fo:block>
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />lektorat)
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" background-color="#FFFFFF">
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />350 z�
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />op�ata semestralna za 1 zaj�cia*
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />33
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Weksel
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />7 z�
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:leader />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />34
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Za�wiadczenie (w tym wype�nianie druk�w) w j�z. obcym
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />35 z� **
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />dot. student�w i absolw. do 30 dni po egzaminie dyplomowym
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />35
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Za�wiadczenie (w tym wype�nianie druk�w) w j�z. obcym
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />60 z� **
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />dot. absolw. od 31 dni po egzaminie dyplomowym i by�ych
								</fo:inline>
							</fo:block>
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />student�w
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />36
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Za�wiadczenie (w  tym wype�nianie druk�w) w j�z. polskim
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />10 z� **
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />dot. student�w i absolw. do 30 dni po egzaminie dyplomowym
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />37
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Za�wiadczenie (w  tym wype�nianie druk�w) w j�z. polskim
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />35 z� **
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />dot. absolw. od 31 dni po egzaminie dyplomowym i by�ych
								</fo:inline>
							</fo:block>
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />student�w
								</fo:inline>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

          <fo:table-row>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />38
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />Za�wiadczenie- TRANSKRYPCJA - w j�z. obcym
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />45 z� **
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />dot. student�w i absolw. do 30 dni po egzaminie dyplomowym
                </fo:inline>
              </fo:block>
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />
                </fo:inline>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>

          <fo:table-row>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />39
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />Za�wiadczenie- TRANSKRYPCJA - w j�z. obcym
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />70 z� **
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />dot. absolw. od 31 dni po egzaminie dyplomowym i by�ych
                </fo:inline>
              </fo:block>
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">student�w
                  <fo:leader leader-length="0pt" />
                </fo:inline>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>


          <fo:table-row>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />40
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />Za�wiadczenie- TRANSKRYPCJA - w j�z. polskim
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />20 z� **
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />dot. student�w i absolw. do 30 dni po egzaminie dyplomowym
                </fo:inline>
              </fo:block>
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />
                </fo:inline>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>


          <fo:table-row>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />41
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />Za�wiadczenie- TRANSKRYPCJA - w j�z. polskim
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />45 z� **
                </fo:inline>
              </fo:block>
            </fo:table-cell>
            <fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="solid" border-top-color="black" border-top-width="0.25pt" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">
                  <fo:leader leader-length="0pt" />dot. absolw. od 31 dni po egzaminie dyplomowym i by�ych
                </fo:inline>
              </fo:block>
              <fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
                <fo:inline font-family="Arial" font-size="8pt">student�w
                  <fo:leader leader-length="0pt" />
                </fo:inline>
              </fo:block>
            </fo:table-cell>
          </fo:table-row>
          
    
          
          
          
          
					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />42
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Zmiana grupy lektorskiej lub pierwotnego wyboru j�zyka
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />50 z� **
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:leader />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
					
					<fo:table-row>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />43
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="none" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="left" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />Zmiana kierunku
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="solid" border-left-color="black" border-left-width="0.25pt" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:inline font-family="Arial" font-size="8pt">
									<fo:leader leader-length="0pt" />60  z� **
								</fo:inline>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell padding-top="0pt" padding-left="3.5pt" padding-bottom="0pt" padding-right="3.5pt" border-top-style="none" border-left-style="none" border-bottom-style="solid" border-bottom-color="black" border-bottom-width="0.25pt" border-right-style="solid" border-right-color="black" border-right-width="0.25pt" >
							<fo:block font-family="Arial" font-size="8pt" language="PL" text-align="center" >
								<fo:leader />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

				</fo:table-body>
			</fo:table>
			<fo:block font-family="Times" font-size="8pt" font-style="italic" language="PL" start-indent="-18pt" end-indent="-43.85pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				<fo:inline font-size="8pt" language="PL">
					<fo:leader leader-length="0pt" />*termin p�atno�ci do 5 dnia miesi�ca rozpoczynaj�cego semestr
				</fo:inline>
			</fo:block>
			<fo:block font-family="Times" font-size="8pt" font-style="italic" language="PL" start-indent="-18pt" end-indent="-43.85pt" xmlns:st1="urn:schemas-microsoft-com:office:smarttags">
				<fo:inline font-size="8pt" language="PL">
					<fo:leader leader-length="0pt" />** termin p�atno�ci w dniu z�o�enia podania/wniosku
				</fo:inline>
			</fo:block>
			<fo:block font-family="Times" font-size="9pt" language="PL" end-indent="-43.85pt">
				<fo:leader />
			</fo:block>
			<fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" end-indent="-43.85pt">
				<fo:leader />
			</fo:block>
			<fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" end-indent="-43.85pt">
				<fo:inline font-size="9pt">
					<fo:leader leader-length="0pt" />
				</fo:inline>
				<fo:inline font-size="9pt">
					<fo:leader leader-pattern="space" leader-length="2.25pt" />
				</fo:inline>
				<fo:inline font-size="9pt">
					<fo:leader leader-pattern="space" leader-length="35.25pt" /><fo:leader leader-length="0pt" />Uczelnia
				</fo:inline>
				<fo:inline font-size="9pt">
					<fo:leader leader-pattern="space" leader-length="3.75pt" />
					<fo:leader leader-length="0pt" />
				</fo:inline>
				<fo:inline font-size="9pt">
					<fo:leader leader-pattern="space" leader-length="2.25pt" />
				</fo:inline>
				<fo:inline font-size="9pt">
					<fo:leader leader-pattern="space" leader-length="45.25pt" />
				</fo:inline>
				<fo:inline font-size="9pt">
					<fo:leader leader-pattern="space" leader-length="35.25pt" /><fo:leader leader-length="168pt" />Student<fo:leader leader-length="0pt" />
				</fo:inline>
			</fo:block>
			<fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" end-indent="-43.85pt">
				<fo:leader />
			</fo:block>
			<fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" end-indent="-43.85pt">
				<fo:leader />
			</fo:block>
			<fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" end-indent="-43.85pt">
				<fo:leader />
			</fo:block>
			<fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" end-indent="-43.85pt" font-weight="bold">
				<fo:inline font-weight="bold" font-size="9pt">
					<fo:leader leader-length="10pt" />_____________________________________<fo:leader leader-length="133pt" />______________________________________<fo:leader leader-length="0pt" />
				</fo:inline>
			</fo:block>
			<fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" end-indent="-43.85pt" font-weight="bold">
				<fo:leader />
			</fo:block>
			<fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" end-indent="-43.85pt">
				<fo:leader />
			</fo:block>
			<fo:block font-family="Times" font-size="9pt" language="PL" start-indent="-18pt" end-indent="-43.85pt">
				<fo:inline font-size="9pt">
					<fo:leader leader-length="308pt" />��d�, dn. <fo:leader leader-length="0pt" />
				</fo:inline>
				<fo:inline font-weight="bold" font-size="9pt">
					<fo:leader leader-length="0" />
					<xsl:value-of select="$dataWygenerowania"/>
				</fo:inline>
			</fo:block>
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>
</xsl:stylesheet>' 
Where opis = 'in�ynierskie'